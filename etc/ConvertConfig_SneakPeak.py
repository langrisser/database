import os,json
from lib import proto_spy, CollectProtoData,processUnityFile, listFiles, CollectProtoData_Old, MultiThreadHandler, ConvertConfigData
import unitypack
from unitypack.export import AssetExporter
from PIL import Image

SPY=True
PROTO=True
PATH = os.path.dirname(os.path.realpath(__file__))
config_path = os.path.join(PATH,*['assets','sneak_peak'])

debug = True

def Convert():
	raw_path = config_path
	dest_path = config_path
	proto_path=os.path.join(PATH,*['res','ProtoBuffer'])
	####	Images	###########################################
	originFolder=config_path
	destFolder = config_path
	for asset in listFiles(originFolder, False):
		fp = open(os.path.join(originFolder,asset),'rb')
		dest=modifiedPath(destFolder,asset)
		processUnityFile(fp,modifiedPath(destFolder,asset),destFolder)

	#Music - CriRes
	#StreamingAssetsConvertion(join(dest_path,'CriRes'))

def modifiedPath(destfolder,asset, fullPath=False):
	#abs.b/en/fr/de fix ~ b is normal, en/de/fr language specific
	asset=asset.rsplit('.',1)
	if len(asset)==2:
		if asset[0][-4:]=='_abs':
			asset[0]=asset[0][:-4]
		if asset[1] != 'b':
			asset[0]+='_%s'%asset[1]
	asset=asset[0]

	#remove useless clutter and parse the rest into a path
	asset=asset.replace('assets_gameproject_runtimeassets_','',1).split('_')
	asset=[val for val in asset if val]	#remove possible empty parts
	lpath=destfolder
	os.makedirs(lpath,exist_ok=True)
	#generate sub folders
	if len(asset)>1:
		for folder in asset[:-1]:
			lpath=os.path.join(lpath,folder)
			os.makedirs(lpath, exist_ok=True)
		
		lpath=os.path.join(lpath,asset[-1])
	else:
		lpath=os.path.join(lpath,asset[0])
	#make relativ path ~ stuff before was to ensure that the path exists
	if not fullPath:
		lpath=lpath.replace(str(destfolder),'')
	#print(asset,'\n',lpath)
	return lpath
	
def convertConfigs():
	MultiThread = MultiThreadHandler(8)

	p_raw = os.path.join(config_path,*['configdata','configdata'])
	cpath = os.path.dirname(p_raw)
	#	1.	convert via spy
	if SPY:
		lpath=os.path.join(cpath,'Spy')
		os.makedirs(lpath,exist_ok=True)
		for fp in os.listdir(p_raw):
			name=fp.rsplit('.',1)[0]
			def ConfigSpyToJson(path_raw,path_dest):
				data=open(path_raw,'rb')
				obj = proto_spy.read_proto_message(data)
				open(path_dest,'wb').write(json.dumps(obj,ensure_ascii=False, indent='\t').encode('utf8'))
			MultiThread.queue.put((
				ConfigSpyToJson,
				{
					'path_raw':os.path.join(p_raw,fp),
					'path_dest':os.path.join(lpath,name+'.json')
				}
			))

	#	2.	via Protos
	if PROTO:
		protopy_path=os.path.join(PATH,*['res','ProtoPy'])
		for v in ['Global','China','Old']:
			print(v)
			lpath			=	os.path.join(cpath,v)
			lprotopy_path	=	os.path.join(protopy_path,v)
			os.makedirs(lpath,exist_ok=True)
			for fp in os.listdir(p_raw):
				name=fp.split('.',1)[0]
				#print(name)
				if 0:
					ConvertConfigData(os.path.join(p_raw,fp),os.path.join(cpath,name+'.json'),lprotopy_path)
				if 1:
					MultiThread.queue.put((
						ConvertConfigData,
						{
							'p_raw':os.path.join(p_raw,fp),
							'p_fin':os.path.join(lpath,name+'.json'),
							'protopy_path':lprotopy_path
						}
					))
	print('\n\n')
		#	End Threads
	MultiThread.RunThreads()

def FindAlphas():
	files = listFiles(config_path,True)
	for f in files:
		if f[-4:] == '.png' and f[:-4]+'_a.png' in files:
			ApplyAlphaImage(f,f[:-4]+'_a.png')
			os.unlink(f[:-4]+'_a.png')


def ApplyAlphaImage(target,alpha):
	# Alpha -> IMG images
	t=Image.open(target)
	a=Image.open(alpha).resize((t.width, t.height))
	channels=(*t.split()[:3],a.getchannel('R'))
	Image.merge("RGBA", channels).save(target[:-4]+'.png')

if __name__ == '__main__':
	Convert()
	convertConfigs()
	#FindAlphas()