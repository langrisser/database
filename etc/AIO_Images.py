import os
import shutil


path=os.path.dirname(os.path.realpath(__file__))
path_assets = os.path.join(path,'assets')
pathAIO = os.path.join(path_assets,'images')
versions=['china','global','images']   #JP first to prefer files from there

def main():
	files=listFiles(pathAIO,['png'],True)

	for version in versions:
		directory=os.path.join(path_assets,version)
		for fp in listFiles(directory,['png'],True):
			if fp not in files:
				files.append(fp)
				os.makedirs(os.path.join(pathAIO,os.path.dirname(fp)), exist_ok=True)
				shutil.copy2(
					os.path.join(directory,fp),
					os.path.join(pathAIO,fp)
					)
			elif os.path.getsize(os.path.join(directory,fp)) > os.path.getsize(os.path.join(pathAIO,fp)):
				
				shutil.copy2(
					os.path.join(directory,fp),
					os.path.join(pathAIO,fp)
					)


def compareFolder(source,dest,path,replace=False):
	for name,item in source.items():
		if name=='path':
			continue
		npath=os.path.join(path,name)
		if type(item)==str:	#file
			if name not in dest:
				print(name)
				dest[name]=npath
				shutil.copy2(item,npath)
			elif replace:
				if os.path.getsize(npath) != os.path.getsize(item):
					print(name)
					shutil.copy2(item,npath)
		else:   #folder
			if name not in dest:
				os.makedirs(npath,exist_ok=True)
				dest[name]={'path':npath}
			compareFolder(source[name],dest[name],npath,replace)


def listFiles(directory,filetypes=[],relative=False):
	files = [
		val for sublist in [
			[
				os.path.join(dirpath, filename).split('resources')[-1]
				for filename in filenames
				if not filetypes or filename.split('.')[-1] in filetypes
			] 
			for (dirpath, dirnames, filenames) in os.walk(directory)
			if '.git' not in dirpath
		] 
		for val in sublist
	]
	if relative:
		x=len(directory)
		files=[fp[x+1:] for fp in files]
	return files

main()