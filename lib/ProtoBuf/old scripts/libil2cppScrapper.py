import os

def ProtoScrapper(fpath):
	with open(fpath,'rt',encoding='utf8') as f:
		data=f.read()

	sections=[section for section in data.split('int __fastcall sub_') if len(section)>300 and len(section)<800]

	def Between(string,pre,post):
		try:
			p_pre=string.index(pre)+len(pre)
			p_post=string[p_pre:].index(post)+p_pre
			return string[p_pre:p_post]
		except ValueError:
			return False

	current=''
	types={}
	for section in sections:
		#main
		if 	'v3 = j_il2cpp_string_new_wrapper_0("' in section or 'v4 = j_il2cpp_string_new_wrapper_0("' in section:
			current=Between(section, ' = j_il2cpp_string_new_wrapper_0("', '");')
			if 'ConfigData' in current:
				types[current]={}
			else:
				current=''
		#sub
		elif	current and 'v2 = j_il2cpp_string_new_wrapper_0("' in section:
			try:
				index	=Between(section, 'j_ProtoMemberAttribute___ctor(**(_DWORD **)(a1 + 4), ', ');')
				if index:
					types[current][index]={
						'index':index,
						'name'	:Between(section, 'v2 = j_il2cpp_string_new_wrapper_0(', ');').replace('"',''),
						'required'	: 'j_ProtoMemberAttribute__set_IsRequired' in section,
						'format'	: Between(section, 'j_ProtoMemberAttribute__set_DataFormat(v1, ',');')
					}
			except:
				pass

	return {key:var for key,var in types.items() if var}