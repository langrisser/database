import os
import subprocess
import copy
from .EnumSearcher import CollectEnums

PATH=os.path.dirname(os.path.realpath(__file__))
config_path=os.path.join(PATH,*[os.pardir,'Assembly-CSharp','BlackJack','ConfigData'])
enums = CollectEnums(config_path)
types={}
def generateProtos(_types,fpath):
	global types
	types=_types
	for key,items in types.items():
		generateProto(key,items,fpath)

def generateProto(name,items,fpath,syntax=2):
	open(
		file=os.path.join(fpath,'%s.proto'%name),
		mode='wt',
		encoding='utf8'
	).write(
		'\n'.join([
			'syntax = "proto%s";'%syntax,
			'package %s;'%name,
			generateProtoMessage(name,items,indent=1),
			''
			'message Items {',
			'	repeated %s items = 1;'%name,
			'}'
		])
	)

def checkVarType(var):
	global types
	required='required' if var['required'] else 'optional'
	typ='variant'
	styp=False
	if 'typ' in var:
		typ = var['typ']

		#list?
		if typ[:5]=='List<':
			required='repeated'
			typ=typ[5:-1]
		#typ fix
		if typ in ['string','bool']:
			pass
		elif typ=='int':
			typ='int32'
		elif typ in enums: #~enum ~ atm as int
			styp='enum'
		elif typ in types:
			styp='typ'
	return (required,typ,styp)

def checkName(name,names):
	if name not in names:
		names.append(name)
		return name
	else:
		for i in range(1,99):
			tname='%s%s'%(name,i)
			if tname not in names:
				names.append(tname)
				return tname


def protoEnum(ename,names,indent):
	used_names=names
	def generateEName(name):
		for i in range(99):
			cname='%s%s'%(name,'_'*i)
			if cname not in used_names:
				used_names.append(cname)
				return cname

	return ('\n%s'%('\t'*indent)).join([
		'enum %s {'%ename,
		*[
			'	{name} = {index};'.format(
				name=generateEName(sename[len(ename):]),#'ArmyTag_None' -> None
				index=str(index)
			)
			for index,sename in enums[ename].items()
		],
		'}',
		''
		])

def checkName(name,names):
	if name not in names:
		names.append(name)
		return name
	else:
		for i in range(1,99):
			tname='%s%s'%(name,i)
			if tname not in names:
				names.append(tname)
				return tname

def generateProtoMessage(key,items,indent,names=[],syntax=2):
	ptext=[
		'message %s {'%key,
	]
	u_enums=[]
	u_typs=[]

	for index,item in sorted(list(items.items()), key=lambda item:int(item[0])):
		#check type
		req,typ,styp=checkVarType(item)
		#if enum
		if styp=='enum':
			if typ not in u_enums:
				ptext.append(protoEnum(typ,names,indent+1))
				u_enums.append(typ)
		if styp=='typ':
			if typ not in u_typs:
				ptext.append(generateProtoMessage(typ,types[typ],indent+1,names,syntax))
				u_typs.append(typ)
		#define var
		ptext.append('	{req} {typ} {name} = {index};'.format(req=req,typ=typ,name=checkName(item['name'],names),index=item['index']))
	ptext.append('}')
	return ('\n%s'%('\t'*indent)).join(ptext)