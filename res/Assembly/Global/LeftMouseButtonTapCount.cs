﻿// Decompiled with JetBrains decompiler
// Type: LeftMouseButtonTapCount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;
using UnityEngine;

public class LeftMouseButtonTapCount
{
  private static float _tabInterval = 0.35f;
  private static int _leftMouseButtonDownCount;
  private static float _leftMouseButtonDownTime;
  private static int _lastFrameLeftMouseButtonDownCount;

  public static float TabInterval
  {
    set
    {
      LeftMouseButtonTapCount._tabInterval = value;
    }
  }

  public static int Count
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Update()
  {
    if ((double) Time.time - (double) LeftMouseButtonTapCount._leftMouseButtonDownTime > (double) LeftMouseButtonTapCount._tabInterval)
      LeftMouseButtonTapCount._leftMouseButtonDownCount = 0;
    LeftMouseButtonTapCount._lastFrameLeftMouseButtonDownCount = LeftMouseButtonTapCount._leftMouseButtonDownCount;
    if ((double) Time.time <= (double) LeftMouseButtonTapCount._leftMouseButtonDownTime || !Input.GetMouseButtonDown(0))
      return;
    ++LeftMouseButtonTapCount._leftMouseButtonDownCount;
    LeftMouseButtonTapCount._leftMouseButtonDownTime = Time.time;
  }
}
