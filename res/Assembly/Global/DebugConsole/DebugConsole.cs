﻿// Decompiled with JetBrains decompiler
// Type: DebugConsole.DebugConsole
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace DebugConsole
{
  public class DebugConsole : MonoBehaviour
  {
    private DebugConsoleView _consoleView;
    public static DebugConsole.DebugConsole instance;

    [MethodImpl((MethodImplOptions) 32768)]
    private static void LogPlatformDefine()
    {
      Debug.Log(string.Format("Application.platform = {0}", (object) Application.platform));
      Debug.Log("UNITY_STANDALONE define.");
    }

    private void Awake()
    {
      DebugConsole.DebugConsole.instance = this;
      this.Init();
    }

    private void Start()
    {
      DebugConsole.DebugConsole.LogPlatformDefine();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      this._consoleView = DebugConsoleView.Create();
      DebugConsoleCtrl.Create(this._consoleView, DebugConsoleMode.Create());
    }

    protected void OnGUI()
    {
      this._consoleView.Show();
    }

    private void OnDestroy()
    {
      DebugConsole.DebugConsole.instance = (DebugConsole.DebugConsole) null;
    }
  }
}
