﻿// Decompiled with JetBrains decompiler
// Type: OrderedDictionaryEnumerator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

internal class OrderedDictionaryEnumerator : IDictionaryEnumerator, IEnumerator
{
  private IEnumerator<KeyValuePair<string, JsonData>> list_enumerator;

  public OrderedDictionaryEnumerator(
    IEnumerator<KeyValuePair<string, JsonData>> enumerator)
  {
    this.list_enumerator = enumerator;
  }

  public object Current
  {
    get
    {
      return (object) this.Entry;
    }
  }

  public DictionaryEntry Entry
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      KeyValuePair<string, JsonData> current = this.list_enumerator.Current;
      return new DictionaryEntry((object) current.Key, (object) current.Value);
    }
  }

  public object Key
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public object Value
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public bool MoveNext()
  {
    return this.list_enumerator.MoveNext();
  }

  public void Reset()
  {
    this.list_enumerator.Reset();
  }
}
