﻿// Decompiled with JetBrains decompiler
// Type: ThreeDTouchEventListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

public class ThreeDTouchEventListener : MonoBehaviour, IUpdateSelectedHandler, IEventSystemHandler
{
  public float ThreeDTouchThreshhold;
  public bool IsThreeDTouchTriggered;

  [MethodImpl((MethodImplOptions) 32768)]
  public ThreeDTouchEventListener()
  {
  }

  public void OnDisable()
  {
    this.IsThreeDTouchTriggered = false;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void OnUpdateSelected(BaseEventData data)
  {
    // ISSUE: unable to decompile the method.
  }

  public event Action EventOnThreeDTouchTriggered
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
    }
  }
}
