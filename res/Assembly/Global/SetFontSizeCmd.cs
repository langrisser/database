﻿// Decompiled with JetBrains decompiler
// Type: SetFontSizeCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;

public class SetFontSizeCmd : IDebugCmd
{
  private const string _NAME = "sfs";
  private const string _DESC = "sfs [int]: set the font size of debug console.";
  private const string _SCHEMA = "i";

  [MethodImpl((MethodImplOptions) 32768)]
  public void Execute(string strParams)
  {
    // ISSUE: unable to decompile the method.
  }

  public string GetHelpDesc()
  {
    return "sfs [int]: set the font size of debug console.";
  }

  public string GetName()
  {
    return "sfs";
  }
}
