﻿// Decompiled with JetBrains decompiler
// Type: AddCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;

public class AddCmd : IDebugCmd
{
  private const string _NAME = "intadd";
  private const string _DESC = "intadd [int] [int]: add two int num.";
  private const string _SCHEMA = "i i";

  [MethodImpl((MethodImplOptions) 32768)]
  public void Execute(string strParams)
  {
    // ISSUE: unable to decompile the method.
  }

  public string GetHelpDesc()
  {
    return "intadd [int] [int]: add two int num.";
  }

  public string GetName()
  {
    return "intadd";
  }
}
