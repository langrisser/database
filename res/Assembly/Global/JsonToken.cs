﻿// Decompiled with JetBrains decompiler
// Type: JsonToken
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

public enum JsonToken
{
  None,
  ObjectStart,
  PropertyName,
  ObjectEnd,
  ArrayStart,
  ArrayEnd,
  Int,
  Long,
  Double,
  String,
  Boolean,
  Null,
}
