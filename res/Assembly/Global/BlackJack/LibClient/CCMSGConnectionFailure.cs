﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.LibClient.CCMSGConnectionFailure
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.LibClient
{
  [ProtoContract(Name = "CCMSGConnectionFailure")]
  [Serializable]
  public class CCMSGConnectionFailure
  {
    private int _MessageId;

    [MethodImpl((MethodImplOptions) 32768)]
    public CCMSGConnectionFailure()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "MessageId")]
    public int MessageId
    {
      get
      {
        return this._MessageId;
      }
      set
      {
        this._MessageId = value;
      }
    }
  }
}
