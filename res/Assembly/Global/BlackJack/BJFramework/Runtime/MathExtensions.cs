﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.MathExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  public static class MathExtensions
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static bool AlmostEquals(
      this Vector3 target,
      Vector3 second,
      float sqrMagnitudePrecision)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool AlmostEquals(
      this Vector2 target,
      Vector2 second,
      float sqrMagnitudePrecision)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool AlmostEquals(this Quaternion target, Quaternion second, float maxAngle)
    {
      return (double) Quaternion.Angle(target, second) < (double) maxAngle;
    }

    public static bool AlmostEquals(this float target, float second, float floatDiff)
    {
      return (double) Mathf.Abs(target - second) < (double) floatDiff;
    }
  }
}
