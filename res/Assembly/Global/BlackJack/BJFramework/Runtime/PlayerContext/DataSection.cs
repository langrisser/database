﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.PlayerContext.DataSection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace BlackJack.BJFramework.Runtime.PlayerContext
{
  [Serializable]
  public class DataSection
  {
    [NonSerialized]
    protected ushort m_version;
    protected ushort m_persistentVersion;

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitWithPersistentVersion(ushort version)
    {
    }

    public void SetVersion(ushort version)
    {
      this.m_version = version;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool NeedFlushPersistent()
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnFlushed()
    {
      this.m_persistentVersion = this.m_version;
    }

    public ushort Version
    {
      get
      {
        return this.m_version;
      }
    }

    public ushort PersistentVersion
    {
      get
      {
        return this.m_persistentVersion;
      }
    }

    [OnDeserialized]
    private void OnDeserialized(StreamingContext context)
    {
      this.InitWithPersistentVersion(this.m_persistentVersion);
    }
  }
}
