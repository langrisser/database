﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ClassLoader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  public class ClassLoader
  {
    private Dictionary<string, Assembly> m_assembleDict = new Dictionary<string, Assembly>();
    private Dictionary<string, System.Type> m_typeDict = new Dictionary<string, System.Type>();
    private static ClassLoader m_instance;

    [MethodImpl((MethodImplOptions) 32768)]
    private ClassLoader()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ClassLoader CreateClassLoader()
    {
      if (ClassLoader.m_instance == null)
        ClassLoader.m_instance = new ClassLoader();
      return ClassLoader.m_instance;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object CreateInstance(TypeDNName typeDNName, params object[] args)
    {
      System.Type type = this.LoadType(typeDNName);
      if (type == null)
        return (object) null;
      return Activator.CreateInstance(type, args);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public System.Type LoadType(TypeDNName typeDNName)
    {
      System.Type type;
      if (!this.m_typeDict.TryGetValue(typeDNName.m_typeFullName, out type))
      {
        Assembly assembly;
        if (string.IsNullOrEmpty(typeDNName.m_assemblyName))
          assembly = this.m_assembleDict["Assembly-CSharp"];
        else if (!this.m_assembleDict.TryGetValue(typeDNName.m_assemblyName, out assembly))
          return (System.Type) null;
        type = assembly.GetType(typeDNName.m_typeFullName);
        if (type == null)
        {
          Debug.LogError(string.Format("Can not find type {0}", (object) typeDNName.m_typeFullName));
          return (System.Type) null;
        }
        this.m_typeDict[typeDNName.m_typeFullName] = type;
      }
      return type;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAssembly(Assembly assembly)
    {
      this.m_assembleDict[assembly.GetName().Name] = assembly;
    }

    public static ClassLoader Instance
    {
      get
      {
        return ClassLoader.m_instance;
      }
    }
  }
}
