﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Scene.UISceneLayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Scene
{
  [CustomLuaClass]
  public class UISceneLayer : SceneLayerBase
  {
    private Transform m_layerParent;
    private Camera m_layerCamera;
    private Canvas m_layerCanvas;

    public override Camera LayerCamera
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        if (this.State == SceneLayerBase.LayerState.InStack)
        {
          if ((Object) this.m_layerParent != (Object) this.transform.parent)
          {
            this.m_layerParent = this.transform.parent;
            this.m_layerCamera = (Camera) null;
          }
          if ((Object) this.m_layerCamera == (Object) null)
          {
            Camera[] componentsInParent = this.GetComponentsInParent<Camera>(true);
            if (componentsInParent.Length != 0)
              this.m_layerCamera = componentsInParent[0];
          }
        }
        else
          this.m_layerCamera = (Camera) null;
        return this.m_layerCamera;
      }
    }

    public Canvas LayerCanvas
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
