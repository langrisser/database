﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TypeDNName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  [CustomLuaClass]
  [Serializable]
  public class TypeDNName
  {
    public string m_assemblyName;
    public string m_typeFullName;

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeDNName(string typeDNName)
    {
      int length = typeDNName.IndexOf('@');
      if (length == -1)
      {
        this.m_assemblyName = "Assembly-CSharp";
        this.m_typeFullName = typeDNName;
      }
      else
      {
        this.m_assemblyName = typeDNName.Substring(0, length);
        this.m_typeFullName = typeDNName.Substring(length + 1);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
