﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.RandomAudioClipPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [RequireComponent(typeof (AudioSource))]
  [Serializable]
  public class RandomAudioClipPlayer : MonoBehaviour
  {
    private AudioSource m_audioSource;
    [Header("播放延时")]
    public float DelayTime;
    [Header("播放速度范围最小值")]
    public float MinPitch;
    [Header("播放速度范围最大值")]
    public float MaxPitch;
    [Header("待播放的AudioClip列表")]
    public List<AudioClip> AudioClipList;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomAudioClipPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    private void Awake()
    {
      this.m_audioSource = this.GetComponent<AudioSource>();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Play(float volume = -1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayWithDelay()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
