﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.IAudioManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  public interface IAudioManager : ITickable
  {
    bool Initlize();

    void Uninitlize();

    void Pause(bool pause);

    IEnumerator Start(Action<bool> onEnd, string initAssetPath);

    void StopAll();

    void SetMute(
      bool muteBackGroundMusic,
      bool muteSound,
      bool muteMovieBackGroundMusic,
      bool mutePlayerVoice,
      bool muteSpeech);

    void SetBackGroundMusicMute(bool muteBackGroundMusic);

    void SetVolume(
      float backGroundMusicVolume,
      float soundVolume,
      float playerVoiceVolume,
      float speechVolume);

    void SetVolume(string category, float volume, bool isSmooth);

    float GetVolume(string category);

    void PlayBackGroundMusic(string music);

    void StopBackGroundMusic();

    void PlaySpeech(string music, float volume = 1f);

    void StopSpeech();

    IAudioPlayback PlaySound(string sound, float volume = 1f);

    void PlaySound(AudioClip ac, float volume = 1f);

    void PlaySound(string sound, AudioClip audioClip, float volume = 1f, bool allowRepeatedPlaying = false);

    List<string> GetLanguages();

    void SetLanguage(string language);

    string GetLanguage();

    void StopSound();

    void StopSound(string sound);

    void PlayPlayerVoice(AudioClip sound, float volume = 1f);

    void StopPlayerVoice();

    bool IsSoundPlaying();

    bool IsPlayerVoicePlaying();

    bool IsSpeechPlaying();
  }
}
