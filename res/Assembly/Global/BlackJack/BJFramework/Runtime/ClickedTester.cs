﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ClickedTester
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.BJFramework.Runtime
{
  public class ClickedTester : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
  {
    private bool m_isClicked;

    public bool Clicked
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        bool isClicked = this.m_isClicked;
        this.m_isClicked = false;
        return isClicked;
      }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
      this.m_isClicked = true;
    }
  }
}
