﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.GameManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.ConfigData;
using BlackJack.BJFramework.Runtime.Log;
using BlackJack.BJFramework.Runtime.Lua;
using BlackJack.BJFramework.Runtime.PlayerContext;
using BlackJack.BJFramework.Runtime.Resource;
using BlackJack.BJFramework.Runtime.Scene;
using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.Utils;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [CustomLuaClass]
  public abstract class GameManager : ITickable
  {
    private TinyCorutineHelper m_corutineHelper = new TinyCorutineHelper();
    protected Dictionary<string, object> m_gameManagerParamDict = new Dictionary<string, object>();
    private static GameManager m_instance;
    protected GameManager.GMState m_state;
    protected GameClientSetting m_gameClientSetting;
    protected LogManager m_logManager;
    protected TaskManager m_taskManager;
    protected ResourceManager m_resourceManager;
    protected IAudioManager m_audioManager;
    protected SceneManager m_sceneManager;
    protected UIManager m_uiManager;
    protected ClassLoader m_classLoader;
    protected LuaManager m_luaManager;
    protected ClientConfigDataLoaderBase m_configDataLoader;
    protected StringTableManagerBase m_stringTableManager;
    protected IPlayerContextNetworkClient m_networkClient;
    protected PlayerContextBase m_playerContext;
    protected string m_currLocalization;
    private const string LocalizationPrefKey = "Localization";

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    protected GameManager()
    {
      this.m_state = GameManager.GMState.None;
      this.m_playerContext = (PlayerContextBase) null;
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public static T CreateAndInitGameManager<T>() where T : GameManager, new()
    {
      if (GameManager.m_instance != null)
        return GameManager.m_instance as T;
      GameManager.m_instance = (GameManager) new T();
      if (GameManager.m_instance.Initlize())
        return (T) GameManager.m_instance;
      Debug.LogError("CreateAndInitGameManager fail");
      return (T) null;
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    private Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAssemblyLoad(object sender, AssemblyLoadEventArgs args)
    {
      Debug.LogWarning(string.Format("{0} {1}", sender, (object) args.LoadedAssembly));
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Initlize()
    {
      UnityEngine.Debug.Log((object) "GameManager.Initlize start");
      AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(this.OnAssemblyResolve);
      AppDomain.CurrentDomain.AssemblyLoad += new AssemblyLoadEventHandler(this.OnAssemblyLoad);
      AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(this.OnUnhandledException);
      if (!this.InitlizeGameSetting())
      {
        UnityEngine.Debug.LogError((object) "GameManager.Initlize fail");
        return false;
      }
      Application.targetFrameRate = this.m_gameClientSetting.ResolutionSetting.RefreshRate;
      Screen.sleepTimeout = -1;
      Debug.m_mainThread = Thread.CurrentThread;
      string str = !this.m_gameClientSetting.Log2Persistent ? Application.temporaryCachePath : Application.persistentDataPath;
      this.m_logManager = LogManager.CreateLogManager();
      if (!this.m_logManager.Initlize(!this.m_gameClientSetting.DisableUnityEngineLog, true, str + "/Log/", "Log"))
      {
        UnityEngine.Debug.LogError((object) "GameManager.Initlize fail for m_logManager.Initlize");
        return false;
      }
      this.m_taskManager = TaskManager.CreateTaskManager();
      if (!this.m_taskManager.Initlize())
      {
        UnityEngine.Debug.LogError((object) "GameManager.Initlize fail for m_taskManager.Initlize()");
        return false;
      }
      this.InitLocalizationInfo();
      this.m_resourceManager = GameManager.CreateResourceManagerHandler == null ? ResourceManager.CreateResourceManager() : GameManager.CreateResourceManagerHandler();
      if (!this.m_resourceManager.Initlize(this.m_gameClientSetting.ResourcesSetting, this.m_currLocalization))
      {
        Debug.LogError("GameManager.Initlize fail for m_resourceManager.Initlize()");
        return false;
      }
      this.m_sceneManager = SceneManager.CreateSceneManager();
      if (!this.m_sceneManager.Initlize(this.m_gameClientSetting.SceneSetting.DesignScreenWidth, this.m_gameClientSetting.SceneSetting.DesignScreenHeight, this.m_gameClientSetting.SceneSetting.TrigerWidth2ShrinkScale, this.m_gameClientSetting.SceneSetting.TrigerHeight2ShrinkScale, this.m_gameClientSetting.SceneSetting.SceneLayerOffset, false, this.m_gameClientSetting.SceneSetting.UseOrthographicForUILayer))
      {
        Debug.LogError("GameManager.Initlize fail for m_sceneManager.Initlize()");
        return false;
      }
      this.m_uiManager = UIManager.CreateUIManager();
      if (!this.m_uiManager.Initlize())
      {
        Debug.LogError("GameManager.Initlize fail for m_uiManager.Initlize()");
        return false;
      }
      this.m_classLoader = ClassLoader.CreateClassLoader();
      foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
        this.m_classLoader.AddAssembly(assembly);
      foreach (string dynamicAssembly in this.m_gameClientSetting.DynamicAssemblySetting.DynamicAssemblyList)
        this.m_classLoader.AddAssembly(Assembly.Load(dynamicAssembly));
      if (this.m_gameClientSetting.LuaSetting.EnableLua)
      {
        if (this.m_gameClientSetting.DynamicAssemblySetting.EnableDynamicAssembly)
        {
          Debug.LogError("Can not enable DynamicAssembly and Lua in same time");
          return false;
        }
        this.m_luaManager = LuaManager.CreateLuaManager();
        if (!this.m_luaManager.Initlize(this.m_gameClientSetting.LuaSetting.LuaRootPath, this.m_gameClientSetting.LuaSetting.LuaInitModule, false))
        {
          Debug.LogError("GameManager.Initlize fail for m_luaManager.Initlize()");
          return false;
        }
      }
      this.m_state = GameManager.GMState.Inited;
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitlizeAudioManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public virtual void OnApplicationQuit()
    {
      this.Uninitlize();
      GameManager.m_instance = (GameManager) null;
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool InitlizeGameSetting()
    {
      this.m_gameClientSetting = Resources.Load("GameClientSetting", typeof (GameClientSetting)) as GameClientSetting;
      if (!((UnityEngine.Object) this.m_gameClientSetting == (UnityEngine.Object) null))
        return true;
      UnityEngine.Debug.LogError((object) "InitlizeGameSetting fail");
      return false;
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IAudioManager CreateAudioManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartLoadDynamicAssemblys(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    protected virtual void OnLoadDynamicAssemblysCompleted(bool ret, Action<bool> onEnd)
    {
      onEnd(ret);
    }

    [DebuggerHidden]
    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator LoadDynamicAssemblysWorker(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartLuaManager(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartLoadConfigData(Action<bool> onEnd, out int initLoadDataCount)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool OnLoadConfigDataEnd(Action<bool> onEnd)
    {
      return false;
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool InitStringTableManager(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public T GetConfigData<T>() where T : ClientConfigDataLoaderBase
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartAudioManager(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool InitlizeBeforeGameAuthLogin(string loginUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public virtual bool InitGameNetworkClient()
    {
      throw new NotImplementedException();
    }

    public virtual bool InitPlayerContext()
    {
      throw new NotImplementedException();
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public T GetPlayerContext<T>() where T : PlayerContextBase
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public T GetGameManagerParam<T>(string key) where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetGameManagerParam(string key, object value)
    {
      this.m_gameManagerParamDict[key] = value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Return2Login(bool raiseCriticalDataCacheDirty)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Clear4Return2Login(bool isCacheDataDirty)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void Clear4Relogin()
    {
    }

    public void StartCorutine(Func<IEnumerator> corutine)
    {
      this.m_corutineHelper.StartCorutine(corutine);
    }

    public void StartCorutine(IEnumerator corutine)
    {
      this.m_corutineHelper.StartCorutine(corutine);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitLocalizationInfo()
    {
      if (PlayerPrefs.HasKey("Localization"))
      {
        this.m_currLocalization = PlayerPrefs.GetString("Localization");
      }
      else
      {
        this.m_currLocalization = this.GameClientSetting.StringTableSetting.LocalizationDefault;
        PlayerPrefs.SetString("Localization", this.m_currLocalization);
        PlayerPrefs.Save();
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocalization(string localization, Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick()
    {
      try
      {
        this.m_corutineHelper.Tick();
        Timer.Tick();
        if (this.m_playerContext != null)
          this.m_playerContext.Tick();
        this.m_taskManager.Tick();
        this.m_resourceManager.Tick();
        if (this.m_audioManager != null)
          this.m_audioManager.Tick();
        this.m_sceneManager.Tick();
        this.m_uiManager.Tick();
      }
      catch (Exception ex)
      {
        Debug.LogError(ex.Message + "\n" + ex.StackTrace);
        throw;
      }
    }

    public static GameManager Instance
    {
      get
      {
        return GameManager.m_instance;
      }
    }

    public GameClientSetting GameClientSetting
    {
      get
      {
        return this.m_gameClientSetting;
      }
    }

    public static Func<ResourceManager> CreateResourceManagerHandler { get; set; }

    public IAudioManager AudioManager
    {
      get
      {
        return this.m_audioManager;
      }
    }

    public ClientConfigDataLoaderBase ConfigDataLoader
    {
      get
      {
        return this.m_configDataLoader;
      }
    }

    public StringTableManagerBase StringTableManager
    {
      get
      {
        return this.m_stringTableManager;
      }
    }

    public IPlayerContextNetworkClient NetworkClient
    {
      get
      {
        return this.m_networkClient;
      }
    }

    public PlayerContextBase PlayerContext
    {
      get
      {
        return this.m_playerContext;
      }
    }

    public string LoginUserId { get; protected set; }

    public enum GMState
    {
      None = 0,
      Inited = 1,
      DynamicAssemblyLoading = 2,
      DynamicAssemblyLoadEnd = 3,
      LuaLoading = 4,
      LuaLoadEnd = 5,
      ConfigDataLoading = 6,
      ConfigDataLoadEnd = 7,
      AudioManagerLoading = 8,
      AudioManagerLoadEnd = 9,
      Ready = 9,
    }
  }
}
