﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Resource.StreamingAssetsFileList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Resource
{
  public class StreamingAssetsFileList : ScriptableObject
  {
    public List<StreamingAssetsFileList.ListItem> m_fileList = new List<StreamingAssetsFileList.ListItem>();
    public int m_version;

    [MethodImpl((MethodImplOptions) 32768)]
    public StreamingAssetsFileList()
    {
    }

    [Serializable]
    public class ListItem
    {
      public string m_bundleName;
      public int m_bundleVersion;
      public string m_filePath;

      [MethodImpl((MethodImplOptions) 32768)]
      public ListItem(string path, int version)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
