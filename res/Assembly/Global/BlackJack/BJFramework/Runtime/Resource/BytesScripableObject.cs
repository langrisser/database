﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Resource.BytesScripableObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Resource
{
  public class BytesScripableObject : ScriptableObject
  {
    public int m_size;
    [SerializeField]
    protected byte[] m_bytes;

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetBytes(byte[] bytes)
    {
    }

    public byte[] GetBytes()
    {
      return this.m_bytes;
    }
  }
}
