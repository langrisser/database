﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Resource.BundleData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Resource
{
  public class BundleData : ScriptableObject
  {
    public int m_version = 1;
    public int m_basicVersion = 1;
    public List<BundleData.SingleBundleData> m_bundleList = new List<BundleData.SingleBundleData>();

    [MethodImpl((MethodImplOptions) 32768)]
    public BundleData()
    {
    }

    [Serializable]
    public class SingleBundleData
    {
      public int m_version = 1;
      public List<string> m_assetList = new List<string>();
      public string m_bundleName;
      public string m_bundleHash;
      public uint m_bundleCRC;
      public long m_size;
      public bool m_isInStreamingAssets;
      public bool m_isNeedPreUpdateByDefault;
      public bool m_isResaveFileBundle;
      public bool m_isLazyUpdate;
      public string m_localizationKey;

      [MethodImpl((MethodImplOptions) 32768)]
      public SingleBundleData()
      {
      }
    }
  }
}
