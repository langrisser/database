﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ResourcesSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class ResourcesSettings
  {
    public bool SkipStreamingAssetsFileProcessing = true;
    public bool DisableAssetBundle = true;
    public bool DisableAssetBundleDownload = true;
    public bool SkipAssetBundlePreUpdateing = true;
    public int PreUpdateWorkerCount = 4;
    public int UnloadUnusedAssetTimeInterval = 10;
    public string ResaveAssetRoot = "GameProject/RuntimeAssets/ResaveFiles";
    public string ResaveFileDestDir = "Resave";
    [Header("Bundle预更新重试次数")]
    public int BundlePreUpdateRetryCount = 10;
    [Header("Bundle预更新重试时间间隔(秒)")]
    public float BundlePreUpdateRetryTimeDelay = 1f;
    public string AssetBundleDownloadUrlRoot;
    public bool LoadAssetFromBundleInEditor;
    public bool AssetPathIgnoreCase;
    public List<string> ResaveFileDirRoots;
    [Header("是否开启资源管理器的详细log")]
    public bool EnableDetailResourceManagerLog;

    [MethodImpl((MethodImplOptions) 32768)]
    public ResourcesSettings()
    {
    }
  }
}
