﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIStateGradientColorDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.UI
{
  [Serializable]
  public class UIStateGradientColorDesc
  {
    public Gradient GradientComptent;
    public Color Color1;
    public Color Color2;
  }
}
