﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UITaskBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.Scene;
using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.Utils;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;

namespace BlackJack.BJFramework.Runtime.UI
{
  [HotFixManually]
  public class UITaskBase : Task
  {
    protected bool m_isUIInputEnable = true;
    protected bool m_enableUIInputLog = true;
    protected List<UITaskBase.PlayingUpdateViewEffectItem> m_playingUpdateViewEffectList = new List<UITaskBase.PlayingUpdateViewEffectItem>();
    protected Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict = new Dictionary<string, UnityEngine.Object>();
    protected TinyCorutineHelper m_corutineHelper = new TinyCorutineHelper();
    private LinkedList<UITaskBase.DelayTimeExecItem> m_delayTimeExecList = new LinkedList<UITaskBase.DelayTimeExecItem>();
    protected List<UITaskBase.PiplineQueueItem> m_piplineQueue = new List<UITaskBase.PiplineQueueItem>();
    protected List<string> m_modeDefine;
    protected UIIntent m_currIntent;
    protected string m_currMode;
    protected int m_loadingStaticResCorutineCount;
    protected int m_loadingDynamicResCorutineCount;
    protected SceneLayerBase[] m_layerArray;
    protected UIControllerBase[] m_uiCtrlArray;
    protected UITaskPipeLineCtx m_currPipeLineCtx;
    protected bool m_blockPipeLine;
    private ulong m_lastStartUpdatePipeLineTime;
    protected List<string> m_tagList;
    [DoNotToLua]
    private UITaskBase.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_InitlizeBeforeManagerStartIt_hotfix;
    private LuaFunction m_PrepareForStartOrResumeUIIntentAction`1_hotfix;
    private LuaFunction m_OnStartObject_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_GetPipeLineCtx_hotfix;
    private LuaFunction m_CreatePipeLineCtx_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_OnPause_hotfix;
    private LuaFunction m_OnResumeObject_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_OnNewIntentUIIntent_hotfix;
    private LuaFunction m_StartUpdatePipeLineUIIntentBooleanBooleanBoolean_hotfix;
    private LuaFunction m_NeedSkipUpdatePipeLineUIIntentBoolean_hotfix;
    private LuaFunction m_IsNeedUpdateDataCache_hotfix;
    private LuaFunction m_UpdateDataCache_hotfix;
    private LuaFunction m_IsNeedLoadStaticRes_hotfix;
    private LuaFunction m_StartLoadStaticRes_hotfix;
    private LuaFunction m_CheckLayerDescArrayList`1_hotfix;
    private LuaFunction m_OnLoadStaticResCompleted_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_StartLoadDynamicRes_hotfix;
    private LuaFunction m_CollectAllDynamicResForLoad_hotfix;
    private LuaFunction m_CollectAllStaticResDescForLoad_hotfix;
    private LuaFunction m_CalculateDynamicResReallyNeedLoadList`1_hotfix;
    private LuaFunction m_OnLoadDynamicResCompleted_hotfix;
    private LuaFunction m_RedirectPipLineOnAllResReadyAction_hotfix;
    private LuaFunction m_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_ReturnFromRedirectPipLineOnLoadAllResCompleted_hotfix;
    private LuaFunction m_InitLayerStateOnLoadAllResCompleted_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_PostOnLoadAllResCompleted_hotfix;
    private LuaFunction m_IsLoadAllResCompleted_hotfix;
    private LuaFunction m_StartUpdateView_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_RegUpdateViewPlayingEffectStringInt32Action`1_hotfix;
    private LuaFunction m_UnregUpdateViewPlayingEffectStringBoolean_hotfix;
    private LuaFunction m_PostUpdateView_hotfix;
    private LuaFunction m_PostUpdateViewBeforeClearContext_hotfix;
    private LuaFunction m_HideAllView_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_SaveContextInIntentOnPause_hotfix;
    private LuaFunction m_ClearContextOnPause_hotfix;
    private LuaFunction m_ClearContextOnIntentChangeUIIntent_hotfix;
    private LuaFunction m_ClearContextOnUpdateViewEnd_hotfix;
    private LuaFunction m_EnableUIInputBooleanNullable`1_hotfix;
    private LuaFunction m_GetLayerDescByNameString_hotfix;
    private LuaFunction m_GetLayerByNameString_hotfix;
    private LuaFunction m_RegisterModesDefineStringStringbe_hotfix;
    private LuaFunction m_SetCurrentModeString_hotfix;
    private LuaFunction m_SetIsNeedPauseTimeOutBoolean_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_TickForDelayTimeExecuteActionList_hotfix;
    private LuaFunction m_PostDelayTimeExecuteActionActionSingle_hotfix;
    private LuaFunction m_PostDelayTicksExecuteActionActionUInt64_hotfix;
    private LuaFunction m_SetTagString_hotfix;
    private LuaFunction m_HasTagString_hotfix;
    private LuaFunction m_add_EventOnPostUpdateViewAction`1_hotfix;
    private LuaFunction m_remove_EventOnPostUpdateViewAction`1_hotfix;
    private LuaFunction m_get_CurrentIntent_hotfix;
    private LuaFunction m_get_IsUIInputEnable_hotfix;
    private LuaFunction m_get_MainLayer_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;
    private LuaFunction m_get_IsNeedPauseTimeOut_hotfix;
    private LuaFunction m_set_IsNeedPauseTimeOutBoolean_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase(string name)
      : base(name)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_ctorString_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        this.m_ctorString_hotfix.call((object) this, (object) name);
      else
        BJLuaObjHelper.IsSkipLuaHotfix = false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void InitlizeBeforeManagerStartIt()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_InitlizeBeforeManagerStartIt_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_InitlizeBeforeManagerStartIt_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        if (this.LayerDescArray == null)
          return;
        for (int index = 0; index < this.LayerDescArray.Length; ++index)
          this.LayerDescArray[index].m_index = index;
        this.m_layerArray = new SceneLayerBase[this.LayerDescArray.Length];
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override sealed bool OnStart(object param)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_OnStartObject_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return Convert.ToBoolean(this.m_OnStartObject_hotfix.call((object) this, param));
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      return this.OnStart(param as UIIntent);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool OnStart(UIIntent intent)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_OnStartUIIntent_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return Convert.ToBoolean(this.m_OnStartUIIntent_hotfix.call((object) this, (object) intent));
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      this.m_currPipeLineCtx = this.GetPipeLineCtx();
      return this.StartUpdatePipeLine(intent, false, false, true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected UITaskPipeLineCtx GetPipeLineCtx()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_GetPipeLineCtx_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return (UITaskPipeLineCtx) this.m_GetPipeLineCtx_hotfix.call((object) this);
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      if (this.m_currPipeLineCtx == null)
        this.m_currPipeLineCtx = this.CreatePipeLineCtx();
      return this.m_currPipeLineCtx;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual UITaskPipeLineCtx CreatePipeLineCtx()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_CreatePipeLineCtx_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return (UITaskPipeLineCtx) this.m_CreatePipeLineCtx_hotfix.call((object) this);
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      return new UITaskPipeLineCtx();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override sealed bool OnResume(object param = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool StartUpdatePipeLine(
      UIIntent intent = null,
      bool onlyUpdateView = false,
      bool canBeSkip = false,
      bool enableQueue = true)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_StartUpdatePipeLineUIIntentBooleanBooleanBoolean_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return Convert.ToBoolean(this.m_StartUpdatePipeLineUIIntentBooleanBooleanBoolean_hotfix.call((object) this, (object) intent, (object) onlyUpdateView, (object) canBeSkip, (object) enableQueue));
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      if (this.State != Task.TaskState.Running)
      {
        Debug.LogError(string.Format("{0}.UITaskBase.StartUpdatePipeLine Error: State {1} != TaskState.Running", (object) this.GetType().Name, (object) this.State));
        return false;
      }
      if (this.m_blockPipeLine)
      {
        Debug.LogError(string.Format("UITaskBase.StartUpdatePipeLine {0} m_blockPipeLine=true", (object) this.Name));
        return false;
      }
      if (this.m_currPipeLineCtx.m_isRuning)
      {
        if (enableQueue)
          this.m_piplineQueue.Add(new UITaskBase.PiplineQueueItem()
          {
            m_canBeSkip = canBeSkip,
            m_intent = intent,
            m_onlyUpdateView = onlyUpdateView
          });
        return false;
      }
      if (canBeSkip && this.NeedSkipUpdatePipeLine(intent, onlyUpdateView))
      {
        Debug.Log("UITaskBase.StartUpdatePipeLine Skiped");
        return true;
      }
      if (intent != null && (this.m_currIntent != intent || this.m_currMode != intent.TargetMode))
      {
        if (!this.SetCurrentMode(intent.TargetMode))
        {
          Debug.LogError(string.Format("UITaskBase.StartUpdatePipeLine fail error mode {0}", (object) intent.TargetMode));
          return false;
        }
        this.ClearContextOnIntentChange(intent);
        this.m_currIntent = intent;
      }
      this.m_currPipeLineCtx.m_isRuning = true;
      this.m_lastStartUpdatePipeLineTime = BlackJack.BJFramework.Runtime.Timer.m_currTick;
      this.EnableUIInput(false, new bool?(false));
      if (onlyUpdateView)
      {
        this.StartUpdateView();
        return true;
      }
      if (this.IsNeedUpdateDataCache())
        this.UpdateDataCache();
      bool flag1 = this.IsNeedLoadStaticRes();
      bool flag2 = this.IsNeedLoadDynamicRes();
      if (flag1 || flag2)
      {
        if (flag1)
          this.StartLoadStaticRes();
        if (flag2)
          this.StartLoadDynamicRes();
        return true;
      }
      if (this.m_currPipeLineCtx.m_redirectPipLineOnAllResReady != null)
      {
        this.m_blockPipeLine = true;
        this.m_currPipeLineCtx.m_redirectPipLineOnAllResReady();
        return true;
      }
      this.StartUpdateView();
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool NeedSkipUpdatePipeLine(UIIntent intent, bool onlyUpdateView)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsNeedUpdateDataCache()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_IsNeedUpdateDataCache_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return Convert.ToBoolean(this.m_IsNeedUpdateDataCache_hotfix.call((object) this));
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      return false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void UpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsNeedLoadStaticRes()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_IsNeedLoadStaticRes_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return Convert.ToBoolean(this.m_IsNeedLoadStaticRes_hotfix.call((object) this));
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      return (UnityEngine.Object) this.MainLayer == (UnityEngine.Object) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartLoadStaticRes()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_StartLoadStaticRes_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_StartLoadStaticRes_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        List<UITaskBase.LayerDesc> layerDescArray = this.CollectAllStaticResDescForLoad();
        this.CheckLayerDescArray(layerDescArray);
        for (int index = layerDescArray.Count - 1; index >= 0; --index)
        {
          if ((UnityEngine.Object) this.m_layerArray[layerDescArray[index].m_index] != (UnityEngine.Object) null)
            layerDescArray.RemoveAt(index);
          else
            ++this.m_loadingStaticResCorutineCount;
        }
        if (layerDescArray.Count == 0)
        {
          this.OnLoadStaticResCompleted();
        }
        else
        {
          foreach (UITaskBase.LayerDesc layerDesc in layerDescArray)
          {
            string layerName = layerDesc.m_layerName;
            string layerResPath = layerDesc.m_layerResPath;
            bool isUiLayer = layerDesc.m_isUILayer;
            int index = layerDesc.m_index;
            if (isUiLayer)
            {
              this.m_currPipeLineCtx.m_layerLoadedInPipe = true;
              SceneManager.Instance.CreateLayer(typeof (UISceneLayer), layerName, layerResPath, (Action<SceneLayerBase>) (layer =>
              {
                if ((UnityEngine.Object) layer == (UnityEngine.Object) null)
                {
                  Debug.LogError(string.Format("Load Layer fail task={0} layer={1}", (object) this.ToString(), (object) layerName));
                }
                else
                {
                  this.m_layerArray[index] = layer;
                  --this.m_loadingStaticResCorutineCount;
                  this.OnLoadStaticResCompleted();
                }
              }), false);
            }
            else if (layerResPath.EndsWith(".unity"))
            {
              SceneManager.Instance.CreateLayer(typeof (UnitySceneLayer), layerName, layerResPath, (Action<SceneLayerBase>) (layer =>
              {
                if ((UnityEngine.Object) layer == (UnityEngine.Object) null)
                {
                  Debug.LogError(string.Format("Load Layer fail task={0} layer={1}", (object) this.ToString(), (object) layerName));
                }
                else
                {
                  this.m_layerArray[index] = layer;
                  --this.m_loadingStaticResCorutineCount;
                  this.OnLoadStaticResCompleted();
                }
              }), false);
            }
            else
            {
              this.m_currPipeLineCtx.m_layerLoadedInPipe = true;
              SceneManager.Instance.CreateLayer(typeof (ThreeDSceneLayer), layerName, layerResPath, (Action<SceneLayerBase>) (layer =>
              {
                if ((UnityEngine.Object) layer == (UnityEngine.Object) null)
                {
                  Debug.LogError(string.Format("Load Layer fail task={0} layer={1}", (object) this.ToString(), (object) layerName));
                }
                else
                {
                  this.m_layerArray[index] = layer;
                  --this.m_loadingStaticResCorutineCount;
                  this.OnLoadStaticResCompleted();
                }
              }), false);
            }
          }
        }
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void CheckLayerDescArray(List<UITaskBase.LayerDesc> layerDescArray)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_CheckLayerDescArrayList`1_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_CheckLayerDescArrayList`1_hotfix.call((object) this, (object) layerDescArray);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        if (layerDescArray == null || layerDescArray.Count <= 0)
        {
          Debug.LogError("StartLoadStaticRes fail LayerDescArray size error");
          throw new Exception("StartLoadStaticRes fail LayerDescArray size error");
        }
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLoadStaticResCompleted()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_OnLoadStaticResCompleted_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_OnLoadStaticResCompleted_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        if (!this.IsLoadAllResCompleted() || this.State == Task.TaskState.Paused)
          return;
        if (this.State == Task.TaskState.Stopped)
          this.ClearAllContextAndRes();
        else
          this.OnLoadAllResCompleted();
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsNeedLoadDynamicRes()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_IsNeedLoadDynamicRes_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return Convert.ToBoolean(this.m_IsNeedLoadDynamicRes_hotfix.call((object) this));
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      return false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual List<string> CollectAllDynamicResForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_CollectAllStaticResDescForLoad_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return (List<UITaskBase.LayerDesc>) this.m_CollectAllStaticResDescForLoad_hotfix.call((object) this);
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      if (!((UnityEngine.Object) this.MainLayer == (UnityEngine.Object) null))
        return (List<UITaskBase.LayerDesc>) null;
      List<UITaskBase.LayerDesc> layerDescList = new List<UITaskBase.LayerDesc>();
      foreach (UITaskBase.LayerDesc layerDesc in this.LayerDescArray)
      {
        if (!layerDesc.m_isLazyLoad)
          layerDescList.Add(layerDesc);
      }
      return layerDescList;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected HashSet<string> CalculateDynamicResReallyNeedLoad(List<string> resPathList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLoadDynamicResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RedirectPipLineOnAllResReady(Action callBack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLoadAllResCompleted()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_OnLoadAllResCompleted_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_OnLoadAllResCompleted_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        Debug.Log(string.Format("OnLoadAllResCompleted task={0}", (object) this.ToString()));
        if (this.m_currPipeLineCtx.m_redirectPipLineOnAllResReady != null)
        {
          this.m_blockPipeLine = true;
          this.m_currPipeLineCtx.m_redirectPipLineOnAllResReady();
        }
        else
        {
          if (this.m_currPipeLineCtx.m_layerLoadedInPipe)
          {
            this.InitLayerStateOnLoadAllResCompleted();
            this.InitAllUIControllers();
          }
          this.PostOnLoadAllResCompleted();
        }
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReturnFromRedirectPipLineOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void InitLayerStateOnLoadAllResCompleted()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_InitLayerStateOnLoadAllResCompleted_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_InitLayerStateOnLoadAllResCompleted_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        if (this.MainLayer.State == SceneLayerBase.LayerState.InStack)
          return;
        SceneManager.Instance.PushLayer(this.MainLayer);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void InitAllUIControllers()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_InitAllUIControllers_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_InitAllUIControllers_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        if (this.UICtrlDescArray == null || this.UICtrlDescArray.Length == 0)
          return;
        if (this.m_uiCtrlArray == null)
          this.m_uiCtrlArray = new UIControllerBase[this.UICtrlDescArray.Length];
        List<UIControllerBase> uiControllerBaseList = new List<UIControllerBase>();
        for (int index = 0; index < this.UICtrlDescArray.Length; ++index)
        {
          if (!((UnityEngine.Object) this.m_uiCtrlArray[index] != (UnityEngine.Object) null))
          {
            UITaskBase.UIControllerDesc uiCtrlDesc = this.UICtrlDescArray[index];
            UITaskBase.LayerDesc layerDescByName = this.GetLayerDescByName(uiCtrlDesc.m_attachLayerName);
            SceneLayerBase layerByName = this.GetLayerByName(uiCtrlDesc.m_attachLayerName);
            if ((UnityEngine.Object) layerByName == (UnityEngine.Object) null)
            {
              if (layerDescByName == null || !layerDescByName.m_isLazyLoad)
                Debug.LogError(string.Format("InitAllUIControllers fail for ctrl={0} can not find layer={1} in task={2}", (object) uiCtrlDesc.m_ctrlName, (object) uiCtrlDesc.m_attachLayerName, (object) this.ToString()));
              else
                continue;
            }
            if ((UnityEngine.Object) layerByName != (UnityEngine.Object) null)
            {
              PrefabControllerBase gameObject = PrefabControllerBase.AddControllerToGameObject(layerByName.LayerPrefabRoot, uiCtrlDesc.m_attachPath, uiCtrlDesc.m_ctrlTypeDNName, uiCtrlDesc.m_ctrlName, uiCtrlDesc.m_luaModuleName, false);
              if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
                Debug.LogError(string.Format("InitAllUIControllers AddControllerToGameObject fail for ctrl={0} layer={1} in task={2}", (object) uiCtrlDesc.m_ctrlName, (object) uiCtrlDesc.m_attachLayerName, (object) this.ToString()));
              this.m_uiCtrlArray[index] = gameObject as UIControllerBase;
              uiControllerBaseList.Add(gameObject as UIControllerBase);
            }
          }
        }
        foreach (UIControllerBase uiControllerBase in uiControllerBaseList)
        {
          if ((UnityEngine.Object) uiControllerBase != (UnityEngine.Object) null)
            uiControllerBase.BindFields();
        }
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void PostOnLoadAllResCompleted()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_PostOnLoadAllResCompleted_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_PostOnLoadAllResCompleted_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        this.StartUpdateView();
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsLoadAllResCompleted()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_IsLoadAllResCompleted_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return Convert.ToBoolean(this.m_IsLoadAllResCompleted_hotfix.call((object) this));
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      if (this.m_loadingStaticResCorutineCount == 0)
        return this.m_loadingDynamicResCorutineCount == 0;
      return false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartUpdateView()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_StartUpdateView_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_StartUpdateView_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        this.m_playingUpdateViewEffectList.Add((UITaskBase.PlayingUpdateViewEffectItem) null);
        this.UpdateView();
        this.m_playingUpdateViewEffectList.Remove((UITaskBase.PlayingUpdateViewEffectItem) null);
        if (this.m_playingUpdateViewEffectList.Count != 0)
          return;
        this.PostUpdateViewBeforeClearContext();
        this.ClearContextOnUpdateViewEnd();
        this.EnableUIInput(true, new bool?(true));
        this.PostUpdateView();
        if (this.EventOnPostUpdateView == null)
          return;
        this.EventOnPostUpdateView(this);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void RegUpdateViewPlayingEffect(string name = null, int timeout = 0, Action<string> onTimeOut = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UnregUpdateViewPlayingEffect(string name = null, bool isTimeOut = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void PostUpdateViewBeforeClearContext()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_PostUpdateViewBeforeClearContext_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        this.m_PostUpdateViewBeforeClearContext_hotfix.call((object) this);
      else
        BJLuaObjHelper.IsSkipLuaHotfix = false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void HideAllView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void SaveContextInIntentOnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ClearContextOnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ClearContextOnIntentChange(UIIntent newIntent)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_ClearContextOnIntentChangeUIIntent_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        this.m_ClearContextOnIntentChangeUIIntent_hotfix.call((object) this, (object) newIntent);
      else
        BJLuaObjHelper.IsSkipLuaHotfix = false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ClearContextOnUpdateViewEnd()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_ClearContextOnUpdateViewEnd_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_ClearContextOnUpdateViewEnd_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        this.m_currPipeLineCtx.Clear();
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void EnableUIInput(bool isEnable, bool? isGlobalEnable = null)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_EnableUIInputBooleanNullable`1_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_EnableUIInputBooleanNullable`1_hotfix.call((object) this, (object) isEnable, (object) isGlobalEnable);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        this.m_isUIInputEnable = isEnable;
        if (!isGlobalEnable.HasValue)
          return;
        if (isGlobalEnable.Value)
          UIManager.Instance.GlobalUIInputEnable(this.Name, true);
        else
          UIManager.Instance.GlobalUIInputEnable(this.Name, false);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected UITaskBase.LayerDesc GetLayerDescByName(string name)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_GetLayerDescByNameString_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return (UITaskBase.LayerDesc) this.m_GetLayerDescByNameString_hotfix.call((object) this, (object) name);
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      foreach (UITaskBase.LayerDesc layerDesc in this.LayerDescArray)
      {
        if (layerDesc.m_layerName == name)
          return layerDesc;
      }
      return (UITaskBase.LayerDesc) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected SceneLayerBase GetLayerByName(string name)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_GetLayerByNameString_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return (SceneLayerBase) this.m_GetLayerByNameString_hotfix.call((object) this, (object) name);
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      foreach (SceneLayerBase layer in this.m_layerArray)
      {
        if ((UnityEngine.Object) layer != (UnityEngine.Object) null && layer.LayerName == name)
          return layer;
      }
      return (SceneLayerBase) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void RegisterModesDefine(string defaultMode, params string[] modes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool SetCurrentMode(string mode)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_SetCurrentModeString_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return Convert.ToBoolean(this.m_SetCurrentModeString_hotfix.call((object) this, (object) mode));
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      if (this.m_currMode == mode || this.m_modeDefine == null)
        return true;
      if (string.IsNullOrEmpty(mode))
        this.m_currMode = this.m_modeDefine[0];
      else if (this.m_modeDefine.Contains(mode))
      {
        this.m_currMode = mode;
      }
      else
      {
        Debug.LogError(string.Format("UITaskBase.SetCurrentMode fail error mode {0}", (object) mode));
        return false;
      }
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetIsNeedPauseTimeOut(bool isNeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickForDelayTimeExecuteActionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostDelayTimeExecuteAction(Action action, float delaySeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostDelayTicksExecuteAction(Action action, ulong delayTickCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetTag(string tag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasTag(string tag)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<UITaskBase> EventOnPostUpdateView
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_add_EventOnPostUpdateViewAction`1_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        {
          this.m_add_EventOnPostUpdateViewAction`1_hotfix.call((object) this, (object) value);
        }
        else
        {
          BJLuaObjHelper.IsSkipLuaHotfix = false;
          Action<UITaskBase> comparand = this.EventOnPostUpdateView;
          Action<UITaskBase> action;
          do
          {
            action = comparand;
            comparand = Interlocked.CompareExchange<Action<UITaskBase>>(ref this.EventOnPostUpdateView, action + value, comparand);
          }
          while (comparand != action);
        }
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_remove_EventOnPostUpdateViewAction`1_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        {
          this.m_remove_EventOnPostUpdateViewAction`1_hotfix.call((object) this, (object) value);
        }
        else
        {
          BJLuaObjHelper.IsSkipLuaHotfix = false;
          Action<UITaskBase> comparand = this.EventOnPostUpdateView;
          Action<UITaskBase> action;
          do
          {
            action = comparand;
            comparand = Interlocked.CompareExchange<Action<UITaskBase>>(ref this.EventOnPostUpdateView, action - value, comparand);
          }
          while (comparand != action);
        }
      }
    }

    public UIIntent CurrentIntent
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected bool IsUIInputEnable
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected static bool IsGlobalUIInputEnable
    {
      get
      {
        return UIManager.Instance.IsGlobalUIInputEnable();
      }
    }

    protected virtual SceneLayerBase MainLayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_get_MainLayer_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
          return (SceneLayerBase) this.m_get_MainLayer_hotfix.call((object) this);
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        if (this.m_layerArray == null || this.m_layerArray.Length == 0)
          return (SceneLayerBase) null;
        return this.m_layerArray[0];
      }
    }

    protected virtual UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected virtual UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNeedPauseTimeOut
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_get_IsNeedPauseTimeOut_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
          return Convert.ToBoolean(this.m_get_IsNeedPauseTimeOut_hotfix.call((object) this));
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        return this.\u003CIsNeedPauseTimeOut\u003Ek__BackingField;
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public UITaskBase.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_Start(object param)
    {
      return this.Start(param);
    }

    private void __callBase_Stop()
    {
      this.Stop();
    }

    private void __callBase_Pause()
    {
      this.Pause();
    }

    private bool __callBase_Resume(object param)
    {
      return this.Resume(param);
    }

    private void __callBase_ClearOnStopEvent()
    {
      this.ClearOnStopEvent();
    }

    private void __callBase_ExecAfterTicks(Action action, ulong delayTickCount)
    {
      this.ExecAfterTicks(action, delayTickCount);
    }

    private bool __callBase_OnStart(object param)
    {
      return base.OnStart(param);
    }

    private void __callBase_OnPause()
    {
      base.OnPause();
    }

    private bool __callBase_OnResume(object param)
    {
      return base.OnResume(param);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnTick()
    {
      base.OnTick();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPostUpdateView(UITaskBase obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPostUpdateView(UITaskBase obj)
    {
      this.EventOnPostUpdateView = (Action<UITaskBase>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class PlayingUpdateViewEffectItem
    {
      public string m_name;
      public DateTime? m_timeOutTime;
      public Action<string> m_onTimeOut;
    }

    [CustomLuaClass]
    public class LayerDesc
    {
      public string m_layerName;
      public string m_layerResPath;
      public bool m_isUILayer;
      public bool m_isLazyLoad;
      public int m_index;
      public bool m_isReserve;
    }

    [CustomLuaClass]
    public class UIControllerDesc
    {
      public string m_ctrlName;
      public string m_luaModuleName;
      public TypeDNName m_ctrlTypeDNName;
      public string m_attachLayerName;
      public string m_attachPath;
    }

    public class DelayTimeExecItem
    {
      public DateTime m_execTargetTime;
      public Action m_action;
    }

    public class PiplineQueueItem
    {
      public UIIntent m_intent;
      public bool m_onlyUpdateView;
      public bool m_canBeSkip;
    }

    public class LuaExportHelper
    {
      private UITaskBase m_owner;

      public LuaExportHelper(UITaskBase owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_Start(object param)
      {
        return this.m_owner.__callBase_Start(param);
      }

      public void __callBase_Stop()
      {
        this.m_owner.__callBase_Stop();
      }

      public void __callBase_Pause()
      {
        this.m_owner.__callBase_Pause();
      }

      public bool __callBase_Resume(object param)
      {
        return this.m_owner.__callBase_Resume(param);
      }

      public void __callBase_ClearOnStopEvent()
      {
        this.m_owner.__callBase_ClearOnStopEvent();
      }

      public void __callBase_ExecAfterTicks(Action action, ulong delayTickCount)
      {
        this.m_owner.__callBase_ExecAfterTicks(action, delayTickCount);
      }

      public bool __callBase_OnStart(object param)
      {
        return this.m_owner.__callBase_OnStart(param);
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(object param)
      {
        return this.m_owner.__callBase_OnResume(param);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnTick()
      {
        this.m_owner.__callBase_OnTick();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnPostUpdateView(UITaskBase obj)
      {
        this.m_owner.__callDele_EventOnPostUpdateView(obj);
      }

      public void __clearDele_EventOnPostUpdateView(UITaskBase obj)
      {
        this.m_owner.__clearDele_EventOnPostUpdateView(obj);
      }

      public List<string> m_modeDefine
      {
        get
        {
          return this.m_owner.m_modeDefine;
        }
        set
        {
          this.m_owner.m_modeDefine = value;
        }
      }

      public UIIntent m_currIntent
      {
        get
        {
          return this.m_owner.m_currIntent;
        }
        set
        {
          this.m_owner.m_currIntent = value;
        }
      }

      public string m_currMode
      {
        get
        {
          return this.m_owner.m_currMode;
        }
        set
        {
          this.m_owner.m_currMode = value;
        }
      }

      public bool m_isUIInputEnable
      {
        get
        {
          return this.m_owner.m_isUIInputEnable;
        }
        set
        {
          this.m_owner.m_isUIInputEnable = value;
        }
      }

      public bool m_enableUIInputLog
      {
        get
        {
          return this.m_owner.m_enableUIInputLog;
        }
        set
        {
          this.m_owner.m_enableUIInputLog = value;
        }
      }

      public int m_loadingStaticResCorutineCount
      {
        get
        {
          return this.m_owner.m_loadingStaticResCorutineCount;
        }
        set
        {
          this.m_owner.m_loadingStaticResCorutineCount = value;
        }
      }

      public int m_loadingDynamicResCorutineCount
      {
        get
        {
          return this.m_owner.m_loadingDynamicResCorutineCount;
        }
        set
        {
          this.m_owner.m_loadingDynamicResCorutineCount = value;
        }
      }

      public List<UITaskBase.PlayingUpdateViewEffectItem> m_playingUpdateViewEffectList
      {
        get
        {
          return this.m_owner.m_playingUpdateViewEffectList;
        }
        set
        {
          this.m_owner.m_playingUpdateViewEffectList = value;
        }
      }

      public SceneLayerBase[] m_layerArray
      {
        get
        {
          return this.m_owner.m_layerArray;
        }
        set
        {
          this.m_owner.m_layerArray = value;
        }
      }

      public UIControllerBase[] m_uiCtrlArray
      {
        get
        {
          return this.m_owner.m_uiCtrlArray;
        }
        set
        {
          this.m_owner.m_uiCtrlArray = value;
        }
      }

      public UITaskPipeLineCtx m_currPipeLineCtx
      {
        get
        {
          return this.m_owner.m_currPipeLineCtx;
        }
        set
        {
          this.m_owner.m_currPipeLineCtx = value;
        }
      }

      public Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict
      {
        get
        {
          return this.m_owner.m_dynamicResCacheDict;
        }
        set
        {
          this.m_owner.m_dynamicResCacheDict = value;
        }
      }

      public TinyCorutineHelper m_corutineHelper
      {
        get
        {
          return this.m_owner.m_corutineHelper;
        }
        set
        {
          this.m_owner.m_corutineHelper = value;
        }
      }

      public bool m_blockPipeLine
      {
        get
        {
          return this.m_owner.m_blockPipeLine;
        }
        set
        {
          this.m_owner.m_blockPipeLine = value;
        }
      }

      public ulong m_lastStartUpdatePipeLineTime
      {
        get
        {
          return this.m_owner.m_lastStartUpdatePipeLineTime;
        }
        set
        {
          this.m_owner.m_lastStartUpdatePipeLineTime = value;
        }
      }

      public LinkedList<UITaskBase.DelayTimeExecItem> m_delayTimeExecList
      {
        get
        {
          return this.m_owner.m_delayTimeExecList;
        }
        set
        {
          this.m_owner.m_delayTimeExecList = value;
        }
      }

      public List<UITaskBase.PiplineQueueItem> m_piplineQueue
      {
        get
        {
          return this.m_owner.m_piplineQueue;
        }
        set
        {
          this.m_owner.m_piplineQueue = value;
        }
      }

      public List<string> m_tagList
      {
        get
        {
          return this.m_owner.m_tagList;
        }
        set
        {
          this.m_owner.m_tagList = value;
        }
      }

      public bool IsUIInputEnable
      {
        get
        {
          return this.m_owner.IsUIInputEnable;
        }
      }

      public static bool IsGlobalUIInputEnable
      {
        get
        {
          return UITaskBase.IsGlobalUIInputEnable;
        }
      }

      public SceneLayerBase MainLayer
      {
        get
        {
          return this.m_owner.MainLayer;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool IsNeedPauseTimeOut
      {
        set
        {
          this.m_owner.IsNeedPauseTimeOut = value;
        }
      }

      public bool OnStart(object param)
      {
        return this.m_owner.OnStart(param);
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public UITaskPipeLineCtx GetPipeLineCtx()
      {
        return this.m_owner.GetPipeLineCtx();
      }

      public UITaskPipeLineCtx CreatePipeLineCtx()
      {
        return this.m_owner.CreatePipeLineCtx();
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void OnPause()
      {
        this.m_owner.OnPause();
      }

      public bool OnResume(object param)
      {
        return this.m_owner.OnResume(param);
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public bool StartUpdatePipeLine(
        UIIntent intent,
        bool onlyUpdateView,
        bool canBeSkip,
        bool enableQueue)
      {
        return this.m_owner.StartUpdatePipeLine(intent, onlyUpdateView, canBeSkip, enableQueue);
      }

      public bool NeedSkipUpdatePipeLine(UIIntent intent, bool onlyUpdateView)
      {
        return this.m_owner.NeedSkipUpdatePipeLine(intent, onlyUpdateView);
      }

      public bool IsNeedUpdateDataCache()
      {
        return this.m_owner.IsNeedUpdateDataCache();
      }

      public void UpdateDataCache()
      {
        this.m_owner.UpdateDataCache();
      }

      public bool IsNeedLoadStaticRes()
      {
        return this.m_owner.IsNeedLoadStaticRes();
      }

      public void StartLoadStaticRes()
      {
        this.m_owner.StartLoadStaticRes();
      }

      public void CheckLayerDescArray(List<UITaskBase.LayerDesc> layerDescArray)
      {
        this.m_owner.CheckLayerDescArray(layerDescArray);
      }

      public void OnLoadStaticResCompleted()
      {
        this.m_owner.OnLoadStaticResCompleted();
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void StartLoadDynamicRes()
      {
        this.m_owner.StartLoadDynamicRes();
      }

      public List<string> CollectAllDynamicResForLoad()
      {
        return this.m_owner.CollectAllDynamicResForLoad();
      }

      public List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
      {
        return this.m_owner.CollectAllStaticResDescForLoad();
      }

      public HashSet<string> CalculateDynamicResReallyNeedLoad(List<string> resPathList)
      {
        return this.m_owner.CalculateDynamicResReallyNeedLoad(resPathList);
      }

      public void OnLoadDynamicResCompleted()
      {
        this.m_owner.OnLoadDynamicResCompleted();
      }

      public void OnLoadAllResCompleted()
      {
        this.m_owner.OnLoadAllResCompleted();
      }

      public void InitLayerStateOnLoadAllResCompleted()
      {
        this.m_owner.InitLayerStateOnLoadAllResCompleted();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void PostOnLoadAllResCompleted()
      {
        this.m_owner.PostOnLoadAllResCompleted();
      }

      public bool IsLoadAllResCompleted()
      {
        return this.m_owner.IsLoadAllResCompleted();
      }

      public void StartUpdateView()
      {
        this.m_owner.StartUpdateView();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void RegUpdateViewPlayingEffect(string name, int timeout, Action<string> onTimeOut)
      {
        this.m_owner.RegUpdateViewPlayingEffect(name, timeout, onTimeOut);
      }

      public void UnregUpdateViewPlayingEffect(string name, bool isTimeOut)
      {
        this.m_owner.UnregUpdateViewPlayingEffect(name, isTimeOut);
      }

      public void PostUpdateView()
      {
        this.m_owner.PostUpdateView();
      }

      public void PostUpdateViewBeforeClearContext()
      {
        this.m_owner.PostUpdateViewBeforeClearContext();
      }

      public void HideAllView()
      {
        this.m_owner.HideAllView();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void SaveContextInIntentOnPause()
      {
        this.m_owner.SaveContextInIntentOnPause();
      }

      public void ClearContextOnPause()
      {
        this.m_owner.ClearContextOnPause();
      }

      public void ClearContextOnIntentChange(UIIntent newIntent)
      {
        this.m_owner.ClearContextOnIntentChange(newIntent);
      }

      public void ClearContextOnUpdateViewEnd()
      {
        this.m_owner.ClearContextOnUpdateViewEnd();
      }

      public UITaskBase.LayerDesc GetLayerDescByName(string name)
      {
        return this.m_owner.GetLayerDescByName(name);
      }

      public SceneLayerBase GetLayerByName(string name)
      {
        return this.m_owner.GetLayerByName(name);
      }

      public void RegisterModesDefine(string defaultMode, string[] modes)
      {
        this.m_owner.RegisterModesDefine(defaultMode, modes);
      }

      public bool SetCurrentMode(string mode)
      {
        return this.m_owner.SetCurrentMode(mode);
      }

      public void SetIsNeedPauseTimeOut(bool isNeed)
      {
        this.m_owner.SetIsNeedPauseTimeOut(isNeed);
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void TickForDelayTimeExecuteActionList()
      {
        this.m_owner.TickForDelayTimeExecuteActionList();
      }

      public void SetTag(string tag)
      {
        this.m_owner.SetTag(tag);
      }
    }
  }
}
