﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Lua;
using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.Utils;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  [CustomLuaClassWithProtected]
  public class UIManager
  {
    protected Dictionary<string, int> m_globalUIInputBlockDict = new Dictionary<string, int>();
    private List<UIIntent> m_uiIntentStack = new List<UIIntent>();
    private Dictionary<string, UITaskBase> m_uiTaskDict = new Dictionary<string, UITaskBase>();
    private Dictionary<string, UIManager.UITaskRegItem> m_uiTaskRegDict = new Dictionary<string, UIManager.UITaskRegItem>();
    private List<UITaskBase> m_taskList4Stop = new List<UITaskBase>();
    private List<List<int>> m_uiTaskGroupConflictList = new List<List<int>>();
    protected DateTime m_lastTickPauseTimeOutTime = DateTime.MinValue;
    protected DelayExecHelper m_delayExecHelper = new DelayExecHelper();
    protected List<UIManager.UIProcessQueueItem> m_uiProcessQueue = new List<UIManager.UIProcessQueueItem>();
    private List<Func<UIManager.UIProcessQueueItem, bool>> m_uiProcessQueueBlockerList = new List<Func<UIManager.UIProcessQueueItem, bool>>();
    private static UIManager m_instance;
    public const double UITaskPauseTimeOut = 120.0;
    protected UIManager.UIProcessQueueItem m_currRuningUIProcess;
    protected DateTime? m_currRuningUIProcessTimeOut;
    private bool m_blockGlobalUIInputForWaitingItem;
    [DoNotToLua]
    private UIManager.LuaExportHelper luaExportHelper;

    [MethodImpl((MethodImplOptions) 32768)]
    private UIManager()
    {
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public static UIManager CreateUIManager()
    {
      if (UIManager.m_instance == null)
        UIManager.m_instance = new UIManager();
      return UIManager.m_instance;
    }

    [DoNotToLua]
    public bool Initlize()
    {
      Debug.Log("UIManager.Initlize start");
      return true;
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase StartUITask(
      UIIntent intent,
      bool pushIntentToStack = false,
      bool clearIntentStack = false,
      Action redirectPipLineOnLoadAllResCompleted = null)
    {
      Debug.Log(string.Format("StartUITask task={0}", (object) intent.TargetTaskName));
      if (!this.m_uiTaskRegDict.ContainsKey(intent.TargetTaskName))
      {
        Debug.LogError(string.Format("StartUITask fail for Unregisted TargetTaskName={0}", (object) intent.TargetTaskName));
        return (UITaskBase) null;
      }
      UITaskBase uiTask = this.GetOrCreateUITask(intent.TargetTaskName);
      if (uiTask == null)
      {
        Debug.LogError(string.Format("StartUITask fail for GetOrCreateUITask null TargetTaskName={0}", (object) intent.TargetTaskName));
        return (UITaskBase) null;
      }
      if (clearIntentStack)
        this.m_uiIntentStack.Clear();
      if (redirectPipLineOnLoadAllResCompleted != null)
        uiTask.RedirectPipLineOnAllResReady(redirectPipLineOnLoadAllResCompleted);
      if (this.StartUITaskInternal<UITaskBase>(uiTask, intent, pushIntentToStack))
        return uiTask;
      Debug.LogError(string.Format("StartUITask fail for StartUITaskInternal fail TargetTaskName={0}", (object) intent.TargetTaskName));
      return (UITaskBase) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartUITaskWithPrepare(
      UIIntent intent,
      Action<bool> onPrepareEnd,
      bool pushIntentToStack = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase ReturnUITask(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase ReturnUITaskWithTaskName(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    public UITaskBase ReturnUITaskToLast()
    {
      return this.ReturnUITaskByStackIndex(-1);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase ReturnUITaskByStackIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReturnUITaskWithPrepare(UIIntent intent, Action<bool> onPrepareEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PauseUITask(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopUITask(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UITaskBase GetOrCreateUITask(string taskName)
    {
      UITaskBase uiTaskBase;
      if (this.m_uiTaskDict.TryGetValue(taskName, out uiTaskBase))
        return uiTaskBase;
      UIManager.UITaskRegItem uiTaskRegItem;
      if (!this.m_uiTaskRegDict.TryGetValue(taskName, out uiTaskRegItem))
      {
        Debug.LogError(string.Format("GetOrCreateUITask fail taskName={0} not registed", (object) taskName));
        return (UITaskBase) null;
      }
      UITaskBase instance = ClassLoader.Instance.CreateInstance(uiTaskRegItem.m_taskTypeDnName, (object) taskName) as UITaskBase;
      if (instance == null)
      {
        Debug.LogError(string.Format("GetOrCreateUITask CreateInstance fail taskName={0}", (object) taskName));
        return (UITaskBase) null;
      }
      if (!string.IsNullOrEmpty(uiTaskRegItem.m_luaModuleName) && !LuaManager.TryInitHotfixForObj((object) instance, uiTaskRegItem.m_luaModuleName, (System.Type) null))
      {
        Debug.LogError(string.Format("GetOrCreateUITask fail LuaManager.TryInitHotfixForObj for taskName={0} luaModuleName={1}", (object) taskName, (object) uiTaskRegItem.m_luaModuleName));
        return (UITaskBase) null;
      }
      instance.InitlizeBeforeManagerStartIt();
      this.m_uiTaskDict[taskName] = instance;
      return instance;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase FindUITaskWithName(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    private bool StartUITaskInternal<TTaskType>(
      TTaskType targetTask,
      UIIntent intent,
      bool pushIntentToStack)
      where TTaskType : UITaskBase
    {
      this.CloseAllConflictUITask(intent.TargetTaskName);
      if (pushIntentToStack)
        this.m_uiIntentStack.Add(intent);
      if (this.StartOrResumeTask((UITaskBase) targetTask, intent))
        return true;
      Debug.LogError(string.Format("UIManager.StartUITaskInternal fail task={0}", (object) targetTask.Name));
      return false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool StartOrResumeTask(UITaskBase targetTask, UIIntent intent)
    {
      switch (targetTask.State)
      {
        case Task.TaskState.Init:
          targetTask.EventOnStop += new Action<Task>(this.OnUITaskStop);
          targetTask.EventOnPostUpdateView += new Action<UITaskBase>(this.OnUITaskPostUpdateView);
          return targetTask.Start((object) intent);
        case Task.TaskState.Running:
          targetTask.EventOnPostUpdateView += new Action<UITaskBase>(this.OnUITaskPostUpdateView);
          return targetTask.OnNewIntent(intent);
        case Task.TaskState.Paused:
          targetTask.EventOnPostUpdateView += new Action<UITaskBase>(this.OnUITaskPostUpdateView);
          return targetTask.Resume((object) intent);
        case Task.TaskState.Stopped:
          Debug.LogError(string.Format("UIManager.StartOrResumeTask fail in TaskState.Stopped task={0}", (object) targetTask.Name));
          return false;
        default:
          return false;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool PopIntentUntilReturnTarget(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveIntentFromStack(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUITaskStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUITaskPostUpdateView(UITaskBase task)
    {
      task.EventOnPostUpdateView -= new Action<UITaskBase>(this.OnUITaskPostUpdateView);
      if (this.EventOnUITaskShow == null)
        return;
      this.EventOnUITaskShow(task.Name);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAllConflictUITask(string taskName)
    {
      UIManager.UITaskRegItem uiTaskRegItem1;
      if (!this.m_uiTaskRegDict.TryGetValue(taskName, out uiTaskRegItem1))
        return;
      int taskGroup1 = uiTaskRegItem1.m_taskGroup;
      List<int> intList = (List<int>) null;
      if (this.m_uiTaskGroupConflictList.Count > taskGroup1)
        intList = this.m_uiTaskGroupConflictList[taskGroup1];
      if (intList == null || intList.Count == 0)
        return;
      foreach (KeyValuePair<string, UITaskBase> keyValuePair in this.m_uiTaskDict)
      {
        UIManager.UITaskRegItem uiTaskRegItem2;
        if (this.m_uiTaskRegDict.TryGetValue(keyValuePair.Value.Name, out uiTaskRegItem2))
        {
          int taskGroup2 = uiTaskRegItem2.m_taskGroup;
          if (intList.Contains(taskGroup2))
            this.m_taskList4Stop.Add(keyValuePair.Value);
        }
      }
      if (this.m_taskList4Stop.Count == 0)
        return;
      foreach (Task task in this.m_taskList4Stop)
        task.Stop();
      this.m_taskList4Stop.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopUITaskByGroup(int group)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PauseUITaskByGroup(int group)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PauseUITaskByTag(string tag, bool isExcept = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopUITaskByTag(string tag, bool isExcept)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterUITaskWithGroup(
      string taskName,
      TypeDNName taskTypeDNName,
      int group,
      string luaModuleName = null)
    {
      UIManager.UITaskRegItem uiTaskRegItem;
      if (!this.m_uiTaskRegDict.TryGetValue(taskName, out uiTaskRegItem))
      {
        uiTaskRegItem = new UIManager.UITaskRegItem();
        this.m_uiTaskRegDict.Add(taskName, uiTaskRegItem);
      }
      uiTaskRegItem.m_taskName = taskName;
      uiTaskRegItem.m_taskTypeDnName = taskTypeDNName;
      uiTaskRegItem.m_taskGroup = group;
      uiTaskRegItem.m_luaModuleName = luaModuleName;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUITaskGroupConflict(uint group1, uint group2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReturnToLoginUI()
    {
    }

    public bool IsGlobalUIInputEnable()
    {
      return this.m_globalUIInputBlockDict.Count == 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GlobalUIInputEnable(string srcName, bool enable)
    {
      if (enable)
      {
        int num;
        if (!this.m_globalUIInputBlockDict.TryGetValue(srcName, out num))
          return;
        if (num == 1)
          this.m_globalUIInputBlockDict.Remove(srcName);
        else
          this.m_globalUIInputBlockDict[srcName] = num - 1;
      }
      else
      {
        int num;
        if (this.m_globalUIInputBlockDict.TryGetValue(srcName, out num))
          this.m_globalUIInputBlockDict[srcName] = num + 1;
        else
          this.m_globalUIInputBlockDict[srcName] = 1;
      }
    }

    public void GlobalUIInputBlockClear(string srcName)
    {
      this.m_globalUIInputBlockDict.Remove(srcName);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GlobalUIInputBlockForTicks(int ticks)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      this.m_delayExecHelper.Tick((uint) BlackJack.BJFramework.Runtime.Timer.m_currTick);
      this.TickPauseTimeOut();
      this.TickUIProcessQueue();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickPauseTimeOut()
    {
      if ((BlackJack.BJFramework.Runtime.Timer.m_currTime - this.m_lastTickPauseTimeOutTime).TotalSeconds < 5.0)
        return;
      this.m_lastTickPauseTimeOutTime = BlackJack.BJFramework.Runtime.Timer.m_currTime;
      DateTime dateTime = BlackJack.BJFramework.Runtime.Timer.m_currTime.AddSeconds(-120.0);
      foreach (KeyValuePair<string, UITaskBase> keyValuePair in this.m_uiTaskDict)
      {
        UITaskBase task = keyValuePair.Value;
        if (task.IsNeedPauseTimeOut && task.State == Task.TaskState.Paused && (task.PauseStartTime <= dateTime && this.m_uiIntentStack.Find((Predicate<UIIntent>) (intent => intent.TargetTaskName == task.Name)) == null))
          this.m_taskList4Stop.Add(keyValuePair.Value);
      }
      if (this.m_taskList4Stop.Count == 0)
        return;
      foreach (Task task in this.m_taskList4Stop)
        task.Stop();
      this.m_taskList4Stop.Clear();
    }

    public event Action<string> EventOnUITaskShow
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<bool> EventReturnToLoginUI
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public static UIManager Instance
    {
      get
      {
        return UIManager.m_instance;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterUIProcess(UIManager.UIProcessQueueItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnregisterUIProcess(UIManager.UIProcessQueueItem item)
    {
    }

    public void RegisterUIProcessQueueBlocker(Func<UIManager.UIProcessQueueItem, bool> blocker)
    {
      this.m_uiProcessQueueBlockerList.Add(blocker);
    }

    public void UnregisterUIProcessQueueBlocker(Func<UIManager.UIProcessQueueItem, bool> blocker)
    {
      this.m_uiProcessQueueBlockerList.Remove(blocker);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickUIProcessQueue()
    {
      bool flag = false;
      DateTime currTime = BlackJack.BJFramework.Runtime.Timer.m_currTime;
      if (this.m_currRuningUIProcess != null)
      {
        if (!this.m_currRuningUIProcessTimeOut.HasValue || !(this.m_currRuningUIProcessTimeOut.Value < currTime))
          return;
        this.m_currRuningUIProcess.OnEnd();
      }
      else
      {
        if (this.m_uiProcessQueue.Count == 0)
          return;
        for (int index = this.m_uiProcessQueue.Count - 1; index >= 0; --index)
        {
          this.m_currRuningUIProcess = this.m_uiProcessQueue[index];
          bool onlyTryOnce = this.m_currRuningUIProcess.m_onlyTryOnce;
          if (this.m_currRuningUIProcess.m_waitTimeOutTime.HasValue)
          {
            DateTime? waitTimeOutTime = this.m_currRuningUIProcess.m_waitTimeOutTime;
            if ((!waitTimeOutTime.HasValue ? 0 : (waitTimeOutTime.GetValueOrDefault() < currTime ? 1 : 0)) != 0)
            {
              this.m_currRuningUIProcess = (UIManager.UIProcessQueueItem) null;
              this.m_uiProcessQueue.RemoveAt(index);
              flag = true;
              continue;
            }
          }
          foreach (Func<UIManager.UIProcessQueueItem, bool> processQueueBlocker in this.m_uiProcessQueueBlockerList)
          {
            if (processQueueBlocker(this.m_currRuningUIProcess))
            {
              this.m_currRuningUIProcess = (UIManager.UIProcessQueueItem) null;
              break;
            }
          }
          if (this.m_currRuningUIProcess != null && this.m_currRuningUIProcess.m_cbCanStart != null && !this.m_currRuningUIProcess.m_cbCanStart())
          {
            this.m_currRuningUIProcess = (UIManager.UIProcessQueueItem) null;
            if (onlyTryOnce)
            {
              this.m_uiProcessQueue.RemoveAt(index);
              flag = true;
            }
          }
          else
          {
            this.m_uiProcessQueue.RemoveAt(index);
            flag = true;
          }
          if (this.m_currRuningUIProcess != null)
          {
            if (this.m_currRuningUIProcess.m_processTimeOut != 0)
              this.m_currRuningUIProcessTimeOut = new DateTime?(BlackJack.BJFramework.Runtime.Timer.m_currTime.AddMilliseconds((double) this.m_currRuningUIProcess.m_processTimeOut));
            this.m_currRuningUIProcess.m_cbOnStart();
            if (this.m_currRuningUIProcess != null && this.m_currRuningUIProcess.m_blockGlobalUIInputForProcessing)
            {
              this.GlobalUIInputEnable(this.m_currRuningUIProcess.ToString(), false);
              break;
            }
            break;
          }
        }
        if (!flag)
          return;
        this.UpdateBlockGlobalUIInputForProcessQueue();
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnUIProcessEnd(UIManager.UIProcessQueueItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBlockGlobalUIInputForProcessQueue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUIProcessQueueItemExist(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int UIProcessQueueComparsion(
      UIManager.UIProcessQueueItem x,
      UIManager.UIProcessQueueItem y)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public UIManager.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnUITaskShow(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnUITaskShow(string obj)
    {
      this.EventOnUITaskShow = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventReturnToLoginUI(bool obj)
    {
    }

    private void __clearDele_EventReturnToLoginUI(bool obj)
    {
      this.EventReturnToLoginUI = (Action<bool>) null;
    }

    public class UITaskRegItem
    {
      public int m_taskGroup;
      public string m_taskName;
      public TypeDNName m_taskTypeDnName;
      public string m_luaModuleName;
    }

    public class UIProcessQueueItem
    {
      public bool m_onlyTryOnce = true;
      public int m_priority;
      public int m_typeId;
      public string m_name;
      public object m_ctx;
      public Func<bool> m_cbCanStart;
      public Action m_cbOnStart;
      public Action m_cbOnEnd;
      public int m_waitTimeOut;
      public DateTime? m_waitTimeOutTime;
      public int m_processTimeOut;
      public bool m_blockGlobalUIInputForWaiting;
      public bool m_blockGlobalUIInputForProcessing;
      public bool m_ignoreBlockGlobalUIInputForWaitingInProcessing;

      public void OnEnd()
      {
        UIManager.Instance.OnUIProcessEnd(this);
        if (this.m_cbOnEnd == null)
          return;
        this.m_cbOnEnd();
      }
    }

    public class LuaExportHelper
    {
      private UIManager m_owner;

      public LuaExportHelper(UIManager owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_EventOnUITaskShow(string obj)
      {
        this.m_owner.__callDele_EventOnUITaskShow(obj);
      }

      public void __clearDele_EventOnUITaskShow(string obj)
      {
        this.m_owner.__clearDele_EventOnUITaskShow(obj);
      }

      public void __callDele_EventReturnToLoginUI(bool obj)
      {
        this.m_owner.__callDele_EventReturnToLoginUI(obj);
      }

      public void __clearDele_EventReturnToLoginUI(bool obj)
      {
        this.m_owner.__clearDele_EventReturnToLoginUI(obj);
      }

      public static UIManager m_instance
      {
        get
        {
          return UIManager.m_instance;
        }
        set
        {
          UIManager.m_instance = value;
        }
      }

      public Dictionary<string, int> m_globalUIInputBlockDict
      {
        get
        {
          return this.m_owner.m_globalUIInputBlockDict;
        }
        set
        {
          this.m_owner.m_globalUIInputBlockDict = value;
        }
      }

      public List<UIIntent> m_uiIntentStack
      {
        get
        {
          return this.m_owner.m_uiIntentStack;
        }
        set
        {
          this.m_owner.m_uiIntentStack = value;
        }
      }

      public Dictionary<string, UITaskBase> m_uiTaskDict
      {
        get
        {
          return this.m_owner.m_uiTaskDict;
        }
        set
        {
          this.m_owner.m_uiTaskDict = value;
        }
      }

      public Dictionary<string, UIManager.UITaskRegItem> m_uiTaskRegDict
      {
        get
        {
          return this.m_owner.m_uiTaskRegDict;
        }
        set
        {
          this.m_owner.m_uiTaskRegDict = value;
        }
      }

      public List<UITaskBase> m_taskList4Stop
      {
        get
        {
          return this.m_owner.m_taskList4Stop;
        }
        set
        {
          this.m_owner.m_taskList4Stop = value;
        }
      }

      public List<List<int>> m_uiTaskGroupConflictList
      {
        get
        {
          return this.m_owner.m_uiTaskGroupConflictList;
        }
        set
        {
          this.m_owner.m_uiTaskGroupConflictList = value;
        }
      }

      public DateTime m_lastTickPauseTimeOutTime
      {
        get
        {
          return this.m_owner.m_lastTickPauseTimeOutTime;
        }
        set
        {
          this.m_owner.m_lastTickPauseTimeOutTime = value;
        }
      }

      public DelayExecHelper m_delayExecHelper
      {
        get
        {
          return this.m_owner.m_delayExecHelper;
        }
        set
        {
          this.m_owner.m_delayExecHelper = value;
        }
      }

      public List<UIManager.UIProcessQueueItem> m_uiProcessQueue
      {
        get
        {
          return this.m_owner.m_uiProcessQueue;
        }
        set
        {
          this.m_owner.m_uiProcessQueue = value;
        }
      }

      public UIManager.UIProcessQueueItem m_currRuningUIProcess
      {
        get
        {
          return this.m_owner.m_currRuningUIProcess;
        }
        set
        {
          this.m_owner.m_currRuningUIProcess = value;
        }
      }

      public DateTime? m_currRuningUIProcessTimeOut
      {
        get
        {
          return this.m_owner.m_currRuningUIProcessTimeOut;
        }
        set
        {
          this.m_owner.m_currRuningUIProcessTimeOut = value;
        }
      }

      public List<Func<UIManager.UIProcessQueueItem, bool>> m_uiProcessQueueBlockerList
      {
        get
        {
          return this.m_owner.m_uiProcessQueueBlockerList;
        }
        set
        {
          this.m_owner.m_uiProcessQueueBlockerList = value;
        }
      }

      public bool m_blockGlobalUIInputForWaitingItem
      {
        get
        {
          return this.m_owner.m_blockGlobalUIInputForWaitingItem;
        }
        set
        {
          this.m_owner.m_blockGlobalUIInputForWaitingItem = value;
        }
      }

      public UITaskBase GetOrCreateUITask(string taskName)
      {
        return this.m_owner.GetOrCreateUITask(taskName);
      }

      public bool StartOrResumeTask(UITaskBase targetTask, UIIntent intent)
      {
        return this.m_owner.StartOrResumeTask(targetTask, intent);
      }

      public bool PopIntentUntilReturnTarget(UIIntent intent)
      {
        return this.m_owner.PopIntentUntilReturnTarget(intent);
      }

      public void OnUITaskStop(Task task)
      {
        this.m_owner.OnUITaskStop(task);
      }

      public void OnUITaskPostUpdateView(UITaskBase task)
      {
        this.m_owner.OnUITaskPostUpdateView(task);
      }

      public void CloseAllConflictUITask(string taskName)
      {
        this.m_owner.CloseAllConflictUITask(taskName);
      }

      public void TickPauseTimeOut()
      {
        this.m_owner.TickPauseTimeOut();
      }

      public void TickUIProcessQueue()
      {
        this.m_owner.TickUIProcessQueue();
      }

      public void OnUIProcessEnd(UIManager.UIProcessQueueItem item)
      {
        this.m_owner.OnUIProcessEnd(item);
      }

      public void UpdateBlockGlobalUIInputForProcessQueue()
      {
        this.m_owner.UpdateBlockGlobalUIInputForProcessQueue();
      }

      public static int UIProcessQueueComparsion(
        UIManager.UIProcessQueueItem x,
        UIManager.UIProcessQueueItem y)
      {
        return UIManager.UIProcessQueueComparsion(x, y);
      }
    }
  }
}
