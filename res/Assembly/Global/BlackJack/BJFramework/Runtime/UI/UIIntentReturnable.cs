﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIIntentReturnable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  [CustomLuaClassWithProtected]
  public class UIIntentReturnable : UIIntentCustom
  {
    [DoNotToLua]
    private UIIntentReturnable.LuaExportHelper luaExportHelper;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIIntentReturnable(UIIntent prevTaskIntent, string targetTaskName, string targetMode = null)
    {
    }

    public UIIntent PrevTaskIntent { set; get; }

    [DoNotToLua]
    public UIIntentReturnable.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_SetParam(string key, object value)
    {
      this.SetParam(key, value);
    }

    private bool __callBase_TryGetParam(string key, out object value)
    {
      return this.TryGetParam(key, out value);
    }

    public class LuaExportHelper
    {
      private UIIntentReturnable m_owner;

      public LuaExportHelper(UIIntentReturnable owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_SetParam(string key, object value)
      {
        this.m_owner.__callBase_SetParam(key, value);
      }

      public bool __callBase_TryGetParam(string key, out object value)
      {
        return this.m_owner.__callBase_TryGetParam(key, out value);
      }
    }
  }
}
