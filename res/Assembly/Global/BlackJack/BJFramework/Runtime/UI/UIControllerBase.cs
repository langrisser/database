﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIControllerBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [HotFixManually]
  public class UIControllerBase : PrefabControllerBase
  {
    private static int m_lastButtonClickFrameCount = -1;
    public bool AutoInitLocalizedString = true;
    protected Dictionary<string, Action<UIControllerBase>> m_buttonClickListenerDict;
    protected Dictionary<string, Action<UIControllerBase>> m_buttonDoubleClickListenerDict;
    protected Dictionary<string, Action<UIControllerBase>> m_buttonLongPressStartListenerDict;
    protected Dictionary<string, Action<UIControllerBase>> m_buttonLongPressingListenerDict;
    protected Dictionary<string, Action<UIControllerBase>> m_buttonLongPressEndListenerDict;
    protected Dictionary<string, Action<UIControllerBase, bool>> m_toggleValueChangedListenerDict;
    [DoNotToLua]
    private UIControllerBase.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_InitlizeStringBoolean_hotfix;
    private LuaFunction m_BindFields_hotfix;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_BindFieldImplTypeStringInitStateStringStringBoolean_hotfix;
    private LuaFunction m_Clear_hotfix;
    private LuaFunction m_OnButtonClickButtonString_hotfix;
    private LuaFunction m_OnButtonDoubleClickButtonString_hotfix;
    private LuaFunction m_OnButtonLongPressStartButtonString_hotfix;
    private LuaFunction m_OnButtonLongPressingButtonString_hotfix;
    private LuaFunction m_OnButtonLongPressEndButtonString_hotfix;
    private LuaFunction m_SetButtonClickListenerStringAction`1_hotfix;
    private LuaFunction m_SetButtonClickListenerStringbeAction`1_hotfix;
    private LuaFunction m_SetButtonDoubleClickListenerStringAction`1_hotfix;
    private LuaFunction m_SetButtonLongPressStartListenerStringAction`1_hotfix;
    private LuaFunction m_SetButtonLongPressingListenerStringAction`1_hotfix;
    private LuaFunction m_SetButtonLongPressEndListenerStringAction`1_hotfix;
    private LuaFunction m_OnToggleValueChangedToggleStringBoolean_hotfix;
    private LuaFunction m_SetToggleValueChangedListenerStringAction`2_hotfix;
    private LuaFunction m_SetToggleValueChangedListenerStringbeAction`2_hotfix;
    private LuaFunction m_OnDestroy_hotfix;
    private LuaFunction m_ToString_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Initlize(string ctrlName, bool bindNow)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_InitlizeStringBoolean_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_InitlizeStringBoolean_hotfix.call((object) this, (object) ctrlName, (object) bindNow);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        base.Initlize(ctrlName, bindNow);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void BindFields()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_BindFields_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_BindFields_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        base.BindFields();
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_OnBindFiledsCompleted_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
      {
        this.m_OnBindFiledsCompleted_hotfix.call((object) this);
      }
      else
      {
        BJLuaObjHelper.IsSkipLuaHotfix = false;
        base.OnBindFiledsCompleted();
        if (!this.AutoInitLocalizedString)
          return;
        UIControllerBase.InitLocalizedString(this.gameObject);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override UnityEngine.Object BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName = null,
      bool optional = false)
    {
      if (!BJLuaObjHelper.IsSkipLuaHotfix && (LuaVar) this.m_BindFieldImplTypeStringInitStateStringStringBoolean_hotfix != (LuaVar) null && !this.IsLuaObjHelperDisposed())
        return (UnityEngine.Object) this.m_BindFieldImplTypeStringInitStateStringStringBoolean_hotfix.call((object) this, (object) fieldType, (object) path, (object) initState, (object) fieldName, (object) ctrlName, (object) optional);
      BJLuaObjHelper.IsSkipLuaHotfix = false;
      GameObject childByPath = this.GetChildByPath(path);
      if ((UnityEngine.Object) childByPath == (UnityEngine.Object) null)
      {
        Debug.LogError(string.Format("BindFields fail can not found child {0}", (object) path));
        return (UnityEngine.Object) null;
      }
      UnityEngine.Object @object;
      if (fieldType.IsSubclassOf(typeof (Selectable)))
      {
        Component component = childByPath.GetComponent(fieldType);
        if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        {
          Debug.LogError(string.Format("BindFields fail can not found comp in child {0} {1}", (object) path, (object) fieldType.Name));
          return (UnityEngine.Object) null;
        }
        @object = (UnityEngine.Object) component;
        switch (fieldType.Name)
        {
          case "Button":
            Button button1 = component as Button;
            if ((UnityEngine.Object) button1 != (UnityEngine.Object) null)
            {
              button1.onClick.AddListener((UnityAction) (() => this.OnButtonClick(button1, fieldName)));
              break;
            }
            break;
          case "ButtonEx":
            ButtonEx button2 = component as ButtonEx;
            if ((UnityEngine.Object) button2 != (UnityEngine.Object) null)
            {
              button2.onClick.AddListener((UnityAction) (() => this.OnButtonClick((Button) button2, fieldName)));
              button2.onDoubleClick.AddListener((UnityAction) (() => this.OnButtonDoubleClick((Button) button2, fieldName)));
              button2.onLongPressStart.AddListener((UnityAction) (() => this.OnButtonLongPressStart((Button) button2, fieldName)));
              button2.onLongPressing.AddListener((UnityAction) (() => this.OnButtonLongPressing((Button) button2, fieldName)));
              button2.onLongPressEnd.AddListener((UnityAction) (() => this.OnButtonLongPressEnd((Button) button2, fieldName)));
              break;
            }
            break;
          case "Toggle":
            Toggle toggle1 = component as Toggle;
            if ((UnityEngine.Object) toggle1 != (UnityEngine.Object) null)
            {
              toggle1.onValueChanged.AddListener((UnityAction<bool>) (value => this.OnToggleValueChanged(toggle1, fieldName, value)));
              break;
            }
            break;
          case "ToggleEx":
            ToggleEx toggle2 = component as ToggleEx;
            if ((UnityEngine.Object) toggle2 != (UnityEngine.Object) null)
            {
              toggle2.onValueChanged.AddListener((UnityAction<bool>) (value => this.OnToggleValueChanged((Toggle) toggle2, fieldName, value)));
              break;
            }
            break;
        }
      }
      else
        @object = base.BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      switch (initState)
      {
        case AutoBindAttribute.InitState.Active:
          childByPath.SetActive(true);
          break;
        case AutoBindAttribute.InitState.Inactive:
          childByPath.SetActive(false);
          break;
      }
      return @object;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnButtonClick(Button button, string fieldName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool CheckAndUpdateCurrFrameButtonClickStateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonDoubleClick(Button button, string fieldName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonLongPressStart(Button button, string fieldName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonLongPressing(Button button, string fieldName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonLongPressEnd(Button button, string fieldName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonClickListener(string fieldName, Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonClickListener(string[] fieldNames, Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonDoubleClickListener(string fieldName, Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonLongPressStartListener(string fieldName, Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonLongPressingListener(string fieldName, Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonLongPressEndListener(string fieldName, Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleValueChanged(Toggle toggle, string fieldName, bool value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnDestroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void InitLocalizedString(GameObject goRoot)
    {
      foreach (UITextLocalizationHelper componentsInChild in goRoot.GetComponentsInChildren<UITextLocalizationHelper>(true))
        componentsInChild.LoadLocatizedString();
    }

    [DoNotToLua]
    public UIControllerBase.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      base.Initlize(ctrlName, bindNow);
    }

    private UnityEngine.Object __callBase_GetAssetInContainer(string resName)
    {
      return this.GetAssetInContainer(resName);
    }

    private void __callBase_BindResContainer()
    {
      this.BindResContainer();
    }

    private void __callBase_BindFields()
    {
      base.BindFields();
    }

    private PrefabControllerNextUpdateExecutor __callBase_GetNextUpdateExecutor()
    {
      return this.GetNextUpdateExecutor();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private GameObject __callBase_GetChildByPath(string path)
    {
      return this.GetChildByPath(path);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private UIControllerBase m_owner;

      public LuaExportHelper(UIControllerBase owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public UnityEngine.Object __callBase_GetAssetInContainer(string resName)
      {
        return this.m_owner.__callBase_GetAssetInContainer(resName);
      }

      public void __callBase_BindResContainer()
      {
        this.m_owner.__callBase_BindResContainer();
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public PrefabControllerNextUpdateExecutor __callBase_GetNextUpdateExecutor()
      {
        return this.m_owner.__callBase_GetNextUpdateExecutor();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public GameObject __callBase_GetChildByPath(string path)
      {
        return this.m_owner.__callBase_GetChildByPath(path);
      }

      public Dictionary<string, Action<UIControllerBase>> m_buttonClickListenerDict
      {
        get
        {
          return this.m_owner.m_buttonClickListenerDict;
        }
        set
        {
          this.m_owner.m_buttonClickListenerDict = value;
        }
      }

      public Dictionary<string, Action<UIControllerBase>> m_buttonDoubleClickListenerDict
      {
        get
        {
          return this.m_owner.m_buttonDoubleClickListenerDict;
        }
        set
        {
          this.m_owner.m_buttonDoubleClickListenerDict = value;
        }
      }

      public Dictionary<string, Action<UIControllerBase>> m_buttonLongPressStartListenerDict
      {
        get
        {
          return this.m_owner.m_buttonLongPressStartListenerDict;
        }
        set
        {
          this.m_owner.m_buttonLongPressStartListenerDict = value;
        }
      }

      public Dictionary<string, Action<UIControllerBase>> m_buttonLongPressingListenerDict
      {
        get
        {
          return this.m_owner.m_buttonLongPressingListenerDict;
        }
        set
        {
          this.m_owner.m_buttonLongPressingListenerDict = value;
        }
      }

      public Dictionary<string, Action<UIControllerBase>> m_buttonLongPressEndListenerDict
      {
        get
        {
          return this.m_owner.m_buttonLongPressEndListenerDict;
        }
        set
        {
          this.m_owner.m_buttonLongPressEndListenerDict = value;
        }
      }

      public Dictionary<string, Action<UIControllerBase, bool>> m_toggleValueChangedListenerDict
      {
        get
        {
          return this.m_owner.m_toggleValueChangedListenerDict;
        }
        set
        {
          this.m_owner.m_toggleValueChangedListenerDict = value;
        }
      }

      public static int m_lastButtonClickFrameCount
      {
        get
        {
          return UIControllerBase.m_lastButtonClickFrameCount;
        }
        set
        {
          UIControllerBase.m_lastButtonClickFrameCount = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.OnButtonClick(button, fieldName);
      }

      public void OnButtonDoubleClick(Button button, string fieldName)
      {
        this.m_owner.OnButtonDoubleClick(button, fieldName);
      }

      public void OnButtonLongPressStart(Button button, string fieldName)
      {
        this.m_owner.OnButtonLongPressStart(button, fieldName);
      }

      public void OnButtonLongPressing(Button button, string fieldName)
      {
        this.m_owner.OnButtonLongPressing(button, fieldName);
      }

      public void OnButtonLongPressEnd(Button button, string fieldName)
      {
        this.m_owner.OnButtonLongPressEnd(button, fieldName);
      }

      public void OnToggleValueChanged(Toggle toggle, string fieldName, bool value)
      {
        this.m_owner.OnToggleValueChanged(toggle, fieldName, value);
      }

      public void OnDestroy()
      {
        this.m_owner.OnDestroy();
      }
    }
  }
}
