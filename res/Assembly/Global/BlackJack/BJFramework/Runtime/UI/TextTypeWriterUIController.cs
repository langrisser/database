﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.TextTypeWriterUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [AddComponentMenu("UI/BlackJack/TextTypeWriterUIController")]
  public class TextTypeWriterUIController : MonoBehaviour
  {
    public int charsPerSecond;
    public float fadeInTime;
    public bool AutoStart;
    private Text mText;
    protected Color mTextColor;
    private string mFullText;
    private int mCurrentOffset;
    private float mNextChar;
    private bool mReset;
    private bool mActive;
    private List<TextTypeWriterUIController.FadeEntry> mFade;

    [MethodImpl((MethodImplOptions) 32768)]
    public TextTypeWriterUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool isActive
    {
      get
      {
        return this.mActive;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetToBeginning()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Finish()
    {
    }

    public void StartTypeWriter()
    {
      this.mReset = true;
      this.mActive = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnable()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetAlphaColor(float alpha)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    private struct FadeEntry
    {
      public int index;
      public string text;
      public float alpha;
    }
  }
}
