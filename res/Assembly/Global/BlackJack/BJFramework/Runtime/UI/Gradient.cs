﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.Gradient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [AddComponentMenu("UI/Effects/Gradient")]
  public class Gradient : BaseMeshEffect
  {
    public GradientDirection m_gradientDir;
    public Color m_color1;
    public Color m_color2;

    [MethodImpl((MethodImplOptions) 32768)]
    public Gradient()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ModifyMesh(VertexHelper vh)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
