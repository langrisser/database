﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.LoginUITaskBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Scene;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  [HotFix]
  public abstract class LoginUITaskBase : UITaskBase
  {
    protected LoginUITaskBase.LoginState m_loginTaskState;
    protected string m_authLoginServerAddress;
    protected int m_authLoginServerPort;
    protected string m_authtoken;
    protected string m_gameUserId;
    protected int m_loginChannelId;
    protected int m_bornChannelId;
    protected string m_authLoginServerDomain;
    protected string m_lastSessionToken;
    protected bool m_isWaitingForMsgAck;
    protected DateTime m_timeOutTime;
    protected float m_timeoutDelayTime;
    [DoNotToLua]
    private LoginUITaskBase.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_PostUpdateView_hotfix;
    private LuaFunction m_IsSDKLogin_hotfix;
    private LuaFunction m_IsEditorTestLogin_hotfix;
    private LuaFunction m_StartInitSDK_hotfix;
    private LuaFunction m_OnSDKInitEndBoolean_hotfix;
    private LuaFunction m_StartSDKLogin_hotfix;
    private LuaFunction m_OnSDKLoginEndBoolean_hotfix;
    private LuaFunction m_PostSDKLogin_hotfix;
    private LuaFunction m_IsNeedSelectServer_hotfix;
    private LuaFunction m_LaunchServerListUI_hotfix;
    private LuaFunction m_DownloadServerListAction`1_hotfix;
    private LuaFunction m_OnDownloadServerListEndBoolean_hotfix;
    private LuaFunction m_ShowServerListUI_hotfix;
    private LuaFunction m_OnGameServerConfirmedString_hotfix;
    private LuaFunction m_InitServerCtxByServerIDString_hotfix;
    private LuaFunction m_GetLastLoginedServerID_hotfix;
    private LuaFunction m_LaunchEnterGameUI_hotfix;
    private LuaFunction m_LaunchEnterGameUIWithGameSettingTokenAndServer_hotfix;
    private LuaFunction m_LaunchEnterGameUIWithUIInputAccountAndServer_hotfix;
    private LuaFunction m_LoadLastLoginWithUIInputInfoString_String_Int32__hotfix;
    private LuaFunction m_DownloadGameServerLoginAnnouncementAction`1_hotfix;
    private LuaFunction m_OnDownloadGameServerLoginAnnouncementEndBoolean_hotfix;
    private LuaFunction m_ShowGameServerLoginAnnouncementUI_hotfix;
    private LuaFunction m_ShowEnterGameUI_hotfix;
    private LuaFunction m_OnEnterGameConfirmed_hotfix;
    private LuaFunction m_StartLoginAgentLoginAction`3_hotfix;
    private LuaFunction m_LoginAgentLoginEndInt32StringString_hotfix;
    private LuaFunction m_StartAuthLogin_hotfix;
    private LuaFunction m_StartSessionLogin_hotfix;
    private LuaFunction m_StartPlayerInfoInitReq_hotfix;
    private LuaFunction m_LauncheMainUI_hotfix;
    private LuaFunction m_OnWaitingMsgAckOutTime_hotfix;
    private LuaFunction m_StartWaitingMsgAckSingle_hotfix;
    private LuaFunction m_StopWaitingMsgAck_hotfix;
    private LuaFunction m_ClearGameServerLoginState_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_OnGameServerConnected_hotfix;
    private LuaFunction m_OnGameServerDisconnected_hotfix;
    private LuaFunction m_OnGameServerNetworkErrorInt32_hotfix;
    private LuaFunction m_OnLoginByAuthTokenAckInt32StringBoolean_hotfix;
    private LuaFunction m_OnLoginBySessionTokenAckInt32_hotfix;
    private LuaFunction m_OnConfigDataMD5InfoNtfStringStringString_hotfix;
    private LuaFunction m_OnPlayerInfoInitEnd_hotfix;
    private LuaFunction m_RegisterNetEvent_hotfix;
    private LuaFunction m_UnRegisterNetEvent_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginUITaskBase(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsSDKLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsEditorTestLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartInitSDK()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnSDKInitEnd(bool isSuccess)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartSDKLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnSDKLoginEnd(bool isSuccess)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void PostSDKLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsNeedSelectServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LaunchServerListUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual IEnumerator DownloadServerList(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnDownloadServerListEnd(bool isSuccess)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ShowServerListUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGameServerConfirmed(string serverId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void InitServerCtxByServerID(string serverId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual string GetLastLoginedServerID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LaunchEnterGameUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LaunchEnterGameUIWithGameSettingTokenAndServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LaunchEnterGameUIWithUIInputAccountAndServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool LoadLastLoginWithUIInputInfo(
      out string loginUserId,
      out string authLoginServerAddress,
      out int authLoginServerPort)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual IEnumerator DownloadGameServerLoginAnnouncement(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnDownloadGameServerLoginAnnouncementEnd(bool isSuccess)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ShowGameServerLoginAnnouncementUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ShowEnterGameUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnEnterGameConfirmed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual IEnumerator StartLoginAgentLogin(Action<int, string, string> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LoginAgentLoginEnd(int errCode, string loginUserId, string authToken)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartAuthLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartSessionLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartPlayerInfoInitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LauncheMainUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnWaitingMsgAckOutTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StartWaitingMsgAck(float waitTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StopWaitingMsgAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ClearGameServerLoginState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGameServerConnected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGameServerDisconnected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGameServerNetworkError(int err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool OnLoginByAuthTokenAck(int ret, string sessionToken, bool needRedirect)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool OnLoginBySessionTokenAck(int ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnConfigDataMD5InfoNtf(
      string fileNameWithErrorMD5,
      string localMD5,
      string serverMD5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegisterNetEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnRegisterNetEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public LoginUITaskBase.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_InitlizeBeforeManagerStartIt()
    {
      this.InitlizeBeforeManagerStartIt();
    }

    private void __callBase_PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
    {
      this.PrepareForStartOrResume(intent, onPrepareEnd);
    }

    private bool __callBase_OnStart(object param)
    {
      return this.OnStart(param);
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return this.OnStart(intent);
    }

    private UITaskPipeLineCtx __callBase_GetPipeLineCtx()
    {
      return this.GetPipeLineCtx();
    }

    private UITaskPipeLineCtx __callBase_CreatePipeLineCtx()
    {
      return this.CreatePipeLineCtx();
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(object param)
    {
      return this.OnResume(param);
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return this.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private bool __callBase_StartUpdatePipeLine(
      UIIntent intent,
      bool onlyUpdateView,
      bool canBeSkip,
      bool enableQueue)
    {
      return this.StartUpdatePipeLine(intent, onlyUpdateView, canBeSkip, enableQueue);
    }

    private bool __callBase_NeedSkipUpdatePipeLine(UIIntent intent, bool onlyUpdateView)
    {
      return this.NeedSkipUpdatePipeLine(intent, onlyUpdateView);
    }

    private bool __callBase_IsNeedUpdateDataCache()
    {
      return this.IsNeedUpdateDataCache();
    }

    private void __callBase_UpdateDataCache()
    {
      this.UpdateDataCache();
    }

    private bool __callBase_IsNeedLoadStaticRes()
    {
      return this.IsNeedLoadStaticRes();
    }

    private void __callBase_StartLoadStaticRes()
    {
      this.StartLoadStaticRes();
    }

    private void __callBase_CheckLayerDescArray(List<UITaskBase.LayerDesc> layerDescArray)
    {
      this.CheckLayerDescArray(layerDescArray);
    }

    private void __callBase_OnLoadStaticResCompleted()
    {
      this.OnLoadStaticResCompleted();
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return this.IsNeedLoadDynamicRes();
    }

    private void __callBase_StartLoadDynamicRes()
    {
      this.StartLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private List<UITaskBase.LayerDesc> __callBase_CollectAllStaticResDescForLoad()
    {
      return this.CollectAllStaticResDescForLoad();
    }

    private HashSet<string> __callBase_CalculateDynamicResReallyNeedLoad(
      List<string> resPathList)
    {
      return this.CalculateDynamicResReallyNeedLoad(resPathList);
    }

    private void __callBase_OnLoadDynamicResCompleted()
    {
      this.OnLoadDynamicResCompleted();
    }

    private void __callBase_RedirectPipLineOnAllResReady(Action callBack)
    {
      this.RedirectPipLineOnAllResReady(callBack);
    }

    private void __callBase_OnLoadAllResCompleted()
    {
      this.OnLoadAllResCompleted();
    }

    private void __callBase_ReturnFromRedirectPipLineOnLoadAllResCompleted()
    {
      this.ReturnFromRedirectPipLineOnLoadAllResCompleted();
    }

    private void __callBase_InitLayerStateOnLoadAllResCompleted()
    {
      this.InitLayerStateOnLoadAllResCompleted();
    }

    private void __callBase_InitAllUIControllers()
    {
      this.InitAllUIControllers();
    }

    private void __callBase_PostOnLoadAllResCompleted()
    {
      this.PostOnLoadAllResCompleted();
    }

    private bool __callBase_IsLoadAllResCompleted()
    {
      return this.IsLoadAllResCompleted();
    }

    private void __callBase_StartUpdateView()
    {
      this.StartUpdateView();
    }

    private void __callBase_UpdateView()
    {
      this.UpdateView();
    }

    private void __callBase_RegUpdateViewPlayingEffect(
      string name,
      int timeout,
      Action<string> onTimeOut)
    {
      this.RegUpdateViewPlayingEffect(name, timeout, onTimeOut);
    }

    private void __callBase_UnregUpdateViewPlayingEffect(string name, bool isTimeOut)
    {
      this.UnregUpdateViewPlayingEffect(name, isTimeOut);
    }

    private void __callBase_PostUpdateView()
    {
      base.PostUpdateView();
    }

    private void __callBase_PostUpdateViewBeforeClearContext()
    {
      this.PostUpdateViewBeforeClearContext();
    }

    private void __callBase_HideAllView()
    {
      this.HideAllView();
    }

    private void __callBase_ClearAllContextAndRes()
    {
      this.ClearAllContextAndRes();
    }

    private void __callBase_SaveContextInIntentOnPause()
    {
      this.SaveContextInIntentOnPause();
    }

    private void __callBase_ClearContextOnPause()
    {
      this.ClearContextOnPause();
    }

    private void __callBase_ClearContextOnIntentChange(UIIntent newIntent)
    {
      this.ClearContextOnIntentChange(newIntent);
    }

    private void __callBase_ClearContextOnUpdateViewEnd()
    {
      this.ClearContextOnUpdateViewEnd();
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private UITaskBase.LayerDesc __callBase_GetLayerDescByName(string name)
    {
      return this.GetLayerDescByName(name);
    }

    private SceneLayerBase __callBase_GetLayerByName(string name)
    {
      return this.GetLayerByName(name);
    }

    private void __callBase_RegisterModesDefine(string defaultMode, string[] modes)
    {
      this.RegisterModesDefine(defaultMode, modes);
    }

    private bool __callBase_SetCurrentMode(string mode)
    {
      return this.SetCurrentMode(mode);
    }

    private void __callBase_SetIsNeedPauseTimeOut(bool isNeed)
    {
      this.SetIsNeedPauseTimeOut(isNeed);
    }

    private void __callBase_OnTick()
    {
      base.OnTick();
    }

    private void __callBase_PostDelayTimeExecuteAction(Action action, float delaySeconds)
    {
      this.PostDelayTimeExecuteAction(action, delaySeconds);
    }

    private void __callBase_PostDelayTicksExecuteAction(Action action, ulong delayTickCount)
    {
      this.PostDelayTicksExecuteAction(action, delayTickCount);
    }

    private void __callBase_SetTag(string tag)
    {
      this.SetTag(tag);
    }

    private bool __callBase_HasTag(string tag)
    {
      return this.HasTag(tag);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum LoginState
    {
      None,
      Inited,
      StartSDKInit,
      SDKInitEnd,
      StartSDKLogin,
      SDKLoginEnd,
      ReadyForEnterGame,
      StartLoginAgentLogin,
      LoginAgentLoginEnd,
      StartAuthLogin,
      AuthLoginEnd,
      RedirectWaitDisconnect,
      StartSessionLogin,
      SessionLoginEnd,
      StartPlayerInfoInit,
      ReadyForCharacterUI,
    }

    public class LuaExportHelper
    {
      private LoginUITaskBase m_owner;

      public LuaExportHelper(LoginUITaskBase owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_InitlizeBeforeManagerStartIt()
      {
        this.m_owner.__callBase_InitlizeBeforeManagerStartIt();
      }

      public void __callBase_PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
      {
        this.m_owner.__callBase_PrepareForStartOrResume(intent, onPrepareEnd);
      }

      public bool __callBase_OnStart(object param)
      {
        return this.m_owner.__callBase_OnStart(param);
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public UITaskPipeLineCtx __callBase_GetPipeLineCtx()
      {
        return this.m_owner.__callBase_GetPipeLineCtx();
      }

      public UITaskPipeLineCtx __callBase_CreatePipeLineCtx()
      {
        return this.m_owner.__callBase_CreatePipeLineCtx();
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(object param)
      {
        return this.m_owner.__callBase_OnResume(param);
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public bool __callBase_StartUpdatePipeLine(
        UIIntent intent,
        bool onlyUpdateView,
        bool canBeSkip,
        bool enableQueue)
      {
        return this.m_owner.__callBase_StartUpdatePipeLine(intent, onlyUpdateView, canBeSkip, enableQueue);
      }

      public bool __callBase_NeedSkipUpdatePipeLine(UIIntent intent, bool onlyUpdateView)
      {
        return this.m_owner.__callBase_NeedSkipUpdatePipeLine(intent, onlyUpdateView);
      }

      public bool __callBase_IsNeedUpdateDataCache()
      {
        return this.m_owner.__callBase_IsNeedUpdateDataCache();
      }

      public void __callBase_UpdateDataCache()
      {
        this.m_owner.__callBase_UpdateDataCache();
      }

      public bool __callBase_IsNeedLoadStaticRes()
      {
        return this.m_owner.__callBase_IsNeedLoadStaticRes();
      }

      public void __callBase_StartLoadStaticRes()
      {
        this.m_owner.__callBase_StartLoadStaticRes();
      }

      public void __callBase_CheckLayerDescArray(List<UITaskBase.LayerDesc> layerDescArray)
      {
        this.m_owner.__callBase_CheckLayerDescArray(layerDescArray);
      }

      public void __callBase_OnLoadStaticResCompleted()
      {
        this.m_owner.__callBase_OnLoadStaticResCompleted();
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public void __callBase_StartLoadDynamicRes()
      {
        this.m_owner.__callBase_StartLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public List<UITaskBase.LayerDesc> __callBase_CollectAllStaticResDescForLoad()
      {
        return this.m_owner.__callBase_CollectAllStaticResDescForLoad();
      }

      public HashSet<string> __callBase_CalculateDynamicResReallyNeedLoad(
        List<string> resPathList)
      {
        return this.m_owner.__callBase_CalculateDynamicResReallyNeedLoad(resPathList);
      }

      public void __callBase_OnLoadDynamicResCompleted()
      {
        this.m_owner.__callBase_OnLoadDynamicResCompleted();
      }

      public void __callBase_RedirectPipLineOnAllResReady(Action callBack)
      {
        this.m_owner.__callBase_RedirectPipLineOnAllResReady(callBack);
      }

      public void __callBase_OnLoadAllResCompleted()
      {
        this.m_owner.__callBase_OnLoadAllResCompleted();
      }

      public void __callBase_ReturnFromRedirectPipLineOnLoadAllResCompleted()
      {
        this.m_owner.__callBase_ReturnFromRedirectPipLineOnLoadAllResCompleted();
      }

      public void __callBase_InitLayerStateOnLoadAllResCompleted()
      {
        this.m_owner.__callBase_InitLayerStateOnLoadAllResCompleted();
      }

      public void __callBase_InitAllUIControllers()
      {
        this.m_owner.__callBase_InitAllUIControllers();
      }

      public void __callBase_PostOnLoadAllResCompleted()
      {
        this.m_owner.__callBase_PostOnLoadAllResCompleted();
      }

      public bool __callBase_IsLoadAllResCompleted()
      {
        return this.m_owner.__callBase_IsLoadAllResCompleted();
      }

      public void __callBase_StartUpdateView()
      {
        this.m_owner.__callBase_StartUpdateView();
      }

      public void __callBase_UpdateView()
      {
        this.m_owner.__callBase_UpdateView();
      }

      public void __callBase_RegUpdateViewPlayingEffect(
        string name,
        int timeout,
        Action<string> onTimeOut)
      {
        this.m_owner.__callBase_RegUpdateViewPlayingEffect(name, timeout, onTimeOut);
      }

      public void __callBase_UnregUpdateViewPlayingEffect(string name, bool isTimeOut)
      {
        this.m_owner.__callBase_UnregUpdateViewPlayingEffect(name, isTimeOut);
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_PostUpdateViewBeforeClearContext()
      {
        this.m_owner.__callBase_PostUpdateViewBeforeClearContext();
      }

      public void __callBase_HideAllView()
      {
        this.m_owner.__callBase_HideAllView();
      }

      public void __callBase_ClearAllContextAndRes()
      {
        this.m_owner.__callBase_ClearAllContextAndRes();
      }

      public void __callBase_SaveContextInIntentOnPause()
      {
        this.m_owner.__callBase_SaveContextInIntentOnPause();
      }

      public void __callBase_ClearContextOnPause()
      {
        this.m_owner.__callBase_ClearContextOnPause();
      }

      public void __callBase_ClearContextOnIntentChange(UIIntent newIntent)
      {
        this.m_owner.__callBase_ClearContextOnIntentChange(newIntent);
      }

      public void __callBase_ClearContextOnUpdateViewEnd()
      {
        this.m_owner.__callBase_ClearContextOnUpdateViewEnd();
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public UITaskBase.LayerDesc __callBase_GetLayerDescByName(string name)
      {
        return this.m_owner.__callBase_GetLayerDescByName(name);
      }

      public SceneLayerBase __callBase_GetLayerByName(string name)
      {
        return this.m_owner.__callBase_GetLayerByName(name);
      }

      public void __callBase_RegisterModesDefine(string defaultMode, string[] modes)
      {
        this.m_owner.__callBase_RegisterModesDefine(defaultMode, modes);
      }

      public bool __callBase_SetCurrentMode(string mode)
      {
        return this.m_owner.__callBase_SetCurrentMode(mode);
      }

      public void __callBase_SetIsNeedPauseTimeOut(bool isNeed)
      {
        this.m_owner.__callBase_SetIsNeedPauseTimeOut(isNeed);
      }

      public void __callBase_OnTick()
      {
        this.m_owner.__callBase_OnTick();
      }

      public void __callBase_PostDelayTimeExecuteAction(Action action, float delaySeconds)
      {
        this.m_owner.__callBase_PostDelayTimeExecuteAction(action, delaySeconds);
      }

      public void __callBase_PostDelayTicksExecuteAction(Action action, ulong delayTickCount)
      {
        this.m_owner.__callBase_PostDelayTicksExecuteAction(action, delayTickCount);
      }

      public void __callBase_SetTag(string tag)
      {
        this.m_owner.__callBase_SetTag(tag);
      }

      public bool __callBase_HasTag(string tag)
      {
        return this.m_owner.__callBase_HasTag(tag);
      }

      public LoginUITaskBase.LoginState m_loginTaskState
      {
        get
        {
          return this.m_owner.m_loginTaskState;
        }
        set
        {
          this.m_owner.m_loginTaskState = value;
        }
      }

      public string m_authLoginServerAddress
      {
        get
        {
          return this.m_owner.m_authLoginServerAddress;
        }
        set
        {
          this.m_owner.m_authLoginServerAddress = value;
        }
      }

      public int m_authLoginServerPort
      {
        get
        {
          return this.m_owner.m_authLoginServerPort;
        }
        set
        {
          this.m_owner.m_authLoginServerPort = value;
        }
      }

      public string m_authtoken
      {
        get
        {
          return this.m_owner.m_authtoken;
        }
        set
        {
          this.m_owner.m_authtoken = value;
        }
      }

      public string m_gameUserId
      {
        get
        {
          return this.m_owner.m_gameUserId;
        }
        set
        {
          this.m_owner.m_gameUserId = value;
        }
      }

      public int m_loginChannelId
      {
        get
        {
          return this.m_owner.m_loginChannelId;
        }
        set
        {
          this.m_owner.m_loginChannelId = value;
        }
      }

      public int m_bornChannelId
      {
        get
        {
          return this.m_owner.m_bornChannelId;
        }
        set
        {
          this.m_owner.m_bornChannelId = value;
        }
      }

      public string m_authLoginServerDomain
      {
        get
        {
          return this.m_owner.m_authLoginServerDomain;
        }
        set
        {
          this.m_owner.m_authLoginServerDomain = value;
        }
      }

      public string m_lastSessionToken
      {
        get
        {
          return this.m_owner.m_lastSessionToken;
        }
        set
        {
          this.m_owner.m_lastSessionToken = value;
        }
      }

      public bool m_isWaitingForMsgAck
      {
        get
        {
          return this.m_owner.m_isWaitingForMsgAck;
        }
        set
        {
          this.m_owner.m_isWaitingForMsgAck = value;
        }
      }

      public DateTime m_timeOutTime
      {
        get
        {
          return this.m_owner.m_timeOutTime;
        }
        set
        {
          this.m_owner.m_timeOutTime = value;
        }
      }

      public float m_timeoutDelayTime
      {
        get
        {
          return this.m_owner.m_timeoutDelayTime;
        }
        set
        {
          this.m_owner.m_timeoutDelayTime = value;
        }
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void PostUpdateView()
      {
        this.m_owner.PostUpdateView();
      }

      public bool IsSDKLogin()
      {
        return this.m_owner.IsSDKLogin();
      }

      public bool IsEditorTestLogin()
      {
        return this.m_owner.IsEditorTestLogin();
      }

      public void StartInitSDK()
      {
        this.m_owner.StartInitSDK();
      }

      public void OnSDKInitEnd(bool isSuccess)
      {
        this.m_owner.OnSDKInitEnd(isSuccess);
      }

      public void StartSDKLogin()
      {
        this.m_owner.StartSDKLogin();
      }

      public void OnSDKLoginEnd(bool isSuccess)
      {
        this.m_owner.OnSDKLoginEnd(isSuccess);
      }

      public void PostSDKLogin()
      {
        this.m_owner.PostSDKLogin();
      }

      public bool IsNeedSelectServer()
      {
        return this.m_owner.IsNeedSelectServer();
      }

      public void LaunchServerListUI()
      {
        this.m_owner.LaunchServerListUI();
      }

      public IEnumerator DownloadServerList(Action<bool> onEnd)
      {
        return this.m_owner.DownloadServerList(onEnd);
      }

      public void OnDownloadServerListEnd(bool isSuccess)
      {
        this.m_owner.OnDownloadServerListEnd(isSuccess);
      }

      public void ShowServerListUI()
      {
        this.m_owner.ShowServerListUI();
      }

      public void OnGameServerConfirmed(string serverId)
      {
        this.m_owner.OnGameServerConfirmed(serverId);
      }

      public void InitServerCtxByServerID(string serverId)
      {
        this.m_owner.InitServerCtxByServerID(serverId);
      }

      public string GetLastLoginedServerID()
      {
        return this.m_owner.GetLastLoginedServerID();
      }

      public void LaunchEnterGameUI()
      {
        this.m_owner.LaunchEnterGameUI();
      }

      public void LaunchEnterGameUIWithGameSettingTokenAndServer()
      {
        this.m_owner.LaunchEnterGameUIWithGameSettingTokenAndServer();
      }

      public void LaunchEnterGameUIWithUIInputAccountAndServer()
      {
        this.m_owner.LaunchEnterGameUIWithUIInputAccountAndServer();
      }

      public bool LoadLastLoginWithUIInputInfo(
        out string loginUserId,
        out string authLoginServerAddress,
        out int authLoginServerPort)
      {
        return this.m_owner.LoadLastLoginWithUIInputInfo(out loginUserId, out authLoginServerAddress, out authLoginServerPort);
      }

      public IEnumerator DownloadGameServerLoginAnnouncement(Action<bool> onEnd)
      {
        return this.m_owner.DownloadGameServerLoginAnnouncement(onEnd);
      }

      public void OnDownloadGameServerLoginAnnouncementEnd(bool isSuccess)
      {
        this.m_owner.OnDownloadGameServerLoginAnnouncementEnd(isSuccess);
      }

      public void ShowGameServerLoginAnnouncementUI()
      {
        this.m_owner.ShowGameServerLoginAnnouncementUI();
      }

      public void ShowEnterGameUI()
      {
        this.m_owner.ShowEnterGameUI();
      }

      public void OnEnterGameConfirmed()
      {
        this.m_owner.OnEnterGameConfirmed();
      }

      public IEnumerator StartLoginAgentLogin(Action<int, string, string> onEnd)
      {
        return this.m_owner.StartLoginAgentLogin(onEnd);
      }

      public void LoginAgentLoginEnd(int errCode, string loginUserId, string authToken)
      {
        this.m_owner.LoginAgentLoginEnd(errCode, loginUserId, authToken);
      }

      public void StartAuthLogin()
      {
        this.m_owner.StartAuthLogin();
      }

      public void StartSessionLogin()
      {
        this.m_owner.StartSessionLogin();
      }

      public void StartPlayerInfoInitReq()
      {
        this.m_owner.StartPlayerInfoInitReq();
      }

      public void LauncheMainUI()
      {
        this.m_owner.LauncheMainUI();
      }

      public void OnWaitingMsgAckOutTime()
      {
        this.m_owner.OnWaitingMsgAckOutTime();
      }

      public void StartWaitingMsgAck(float waitTime)
      {
        this.m_owner.StartWaitingMsgAck(waitTime);
      }

      public void StopWaitingMsgAck()
      {
        this.m_owner.StopWaitingMsgAck();
      }

      public void ClearGameServerLoginState()
      {
        this.m_owner.ClearGameServerLoginState();
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void OnGameServerConnected()
      {
        this.m_owner.OnGameServerConnected();
      }

      public void OnGameServerDisconnected()
      {
        this.m_owner.OnGameServerDisconnected();
      }

      public void OnGameServerNetworkError(int err)
      {
        this.m_owner.OnGameServerNetworkError(err);
      }

      public bool OnLoginByAuthTokenAck(int ret, string sessionToken, bool needRedirect)
      {
        return this.m_owner.OnLoginByAuthTokenAck(ret, sessionToken, needRedirect);
      }

      public bool OnLoginBySessionTokenAck(int ret)
      {
        return this.m_owner.OnLoginBySessionTokenAck(ret);
      }

      public void OnConfigDataMD5InfoNtf(
        string fileNameWithErrorMD5,
        string localMD5,
        string serverMD5)
      {
        this.m_owner.OnConfigDataMD5InfoNtf(fileNameWithErrorMD5, localMD5, serverMD5);
      }

      public void OnPlayerInfoInitEnd()
      {
        this.m_owner.OnPlayerInfoInitEnd();
      }

      public void RegisterNetEvent()
      {
        this.m_owner.RegisterNetEvent();
      }

      public void UnRegisterNetEvent()
      {
        this.m_owner.UnRegisterNetEvent();
      }
    }
  }
}
