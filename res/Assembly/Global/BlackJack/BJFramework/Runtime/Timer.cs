﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Timer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  public static class Timer
  {
    public static DateTime m_lastTickTime = DateTime.MaxValue;
    public static DateTime m_currTime;
    public static ulong m_currTick;

    [MethodImpl((MethodImplOptions) 32768)]
    public static uint GetLastFrameDeltaTimeMs()
    {
      // ISSUE: unable to decompile the method.
    }

    public static int GetCurrFrameCount()
    {
      return Time.frameCount;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Tick()
    {
      Timer.m_lastTickTime = Timer.m_currTime;
      Timer.m_currTime = DateTime.Now;
      ++Timer.m_currTick;
    }
  }
}
