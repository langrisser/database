﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.LoginSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class LoginSettings
  {
    public bool LoginUseSettings = true;
    public string GameServerAddress = "192.168.1.111";
    public int GameServerPort = 16001;
    public string LoginAccount = "testUser1";
    public string ClientVersion = string.Empty;
    public int AndroidBasicVersion = 1;
    public int IOSBasicVersion = 1;
    public bool LoginUseSDK;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginSettings()
    {
    }

    public int BasicVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXEditor)
          return this.IOSBasicVersion;
        return this.AndroidBasicVersion;
      }
    }
  }
}
