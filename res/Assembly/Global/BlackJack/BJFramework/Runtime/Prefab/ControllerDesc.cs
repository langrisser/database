﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Prefab.ControllerDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;

namespace BlackJack.BJFramework.Runtime.Prefab
{
  [Serializable]
  public struct ControllerDesc
  {
    public string m_ctrlName;
    public TypeDNName m_ctrlTypeDNName;
    public string m_luaModuleName;
    public string m_attachPath;
  }
}
