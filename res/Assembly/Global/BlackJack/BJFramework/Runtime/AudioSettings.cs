﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.AudioSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections.Generic;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class AudioSettings
  {
    public string AudioMixerAssetPath;
    public string AudioMixerBGMGroupSubPath;
    public string AudioMixerSoundEffectGroupSubPath;
    public string AudioMixerPlayerVoiceGroupSubPath;
    public string AudioMixerSpeechGroupSubPath;
    public string AudioMixerBGMVolumeParamName;
    public string AudioMixerMovieBGMVolumeParamName;
    public string AudioMixerSoundEffectParamName;
    public string AudioMixerPlayerVoiceVolumeParamName;
    public string AudioMixerSpeechVolumeParamName;
    public bool EnableCRI;
    public string CRIAudioManagerAsset;
    public List<string> Languages;
    public int DefaultLanguageIndex;
    public string CRIVideoAssetPathRoot;
    public string CRIVideoAssetPathRootForEditor;
    public bool EnableDownload;
  }
}
