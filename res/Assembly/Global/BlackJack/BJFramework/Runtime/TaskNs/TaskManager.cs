﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TaskNs.TaskManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.TaskNs
{
  public class TaskManager : ITickable
  {
    private List<Task> m_taskList = new List<Task>();
    private List<Task> m_taskList4TickLoop = new List<Task>();
    private Dictionary<string, Task> m_taskRegDict = new Dictionary<string, Task>();
    private static TaskManager m_instance;

    [MethodImpl((MethodImplOptions) 32768)]
    private TaskManager()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TaskManager CreateTaskManager()
    {
      if (TaskManager.m_instance == null)
        TaskManager.m_instance = new TaskManager();
      return TaskManager.m_instance;
    }

    public bool Initlize()
    {
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RegisterTask(Task task)
    {
      if (!string.IsNullOrEmpty(task.Name) && this.m_taskRegDict.ContainsKey(task.Name))
      {
        if (this.m_taskRegDict[task.Name] != task)
          Debug.LogError(string.Format("Task name collision. Name: {0}.", (object) task.Name));
        else
          Debug.LogError(string.Format("Readding task. Name: {0}.", (object) task.Name));
        return false;
      }
      if (this.m_taskRegDict.ContainsValue(task))
      {
        Debug.LogError(string.Format("Re-adding same task with different name. Task name {0}", (object) task.Name));
        return false;
      }
      this.m_taskList.Add(task);
      if (!string.IsNullOrEmpty(task.Name))
        this.m_taskRegDict.Add(task.Name, task);
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnregisterTask(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopTaskWithTaskName(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T FindTaskByName<T>(string taskName) where T : Task
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      this.m_taskList4TickLoop.AddRange((IEnumerable<Task>) this.m_taskList);
      foreach (ITickable tickable in this.m_taskList4TickLoop)
        tickable.Tick();
      this.m_taskList4TickLoop.Clear();
    }

    public static TaskManager Instance
    {
      get
      {
        return TaskManager.m_instance;
      }
    }
  }
}
