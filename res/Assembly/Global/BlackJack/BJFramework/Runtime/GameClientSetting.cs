﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.GameClientSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  public class GameClientSetting : ScriptableObject
  {
    public static string defaultClientPrefPath = "Assets/GameProject/Resources/GameClientSetting";
    public string UITaskRegisterTypeDNName = "Assembly-CSharp@BlackJack.ProjectSample.SampleGameUITaskRegister";
    public bool DisableUnityEngineLog;
    public bool Log2Persistent;
    public ResourcesSettings ResourcesSetting;
    public ResolutionSettings ResolutionSetting;
    public SceneSettings SceneSetting;
    public DynamicAssemblySettings DynamicAssemblySetting;
    public LuaSettings LuaSetting;
    public ConfigDataSettings ConfigDataSetting;
    public AudioSettings AudioSetting;
    public StringTableSettings StringTableSetting;
    public bool DisableUserGuide;
    public LoginSettings LoginSetting;

    [MethodImpl((MethodImplOptions) 32768)]
    public GameClientSetting()
    {
    }
  }
}
