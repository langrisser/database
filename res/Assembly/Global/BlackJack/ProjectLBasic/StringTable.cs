﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.StringTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectLBasic
{
  [CustomLuaClass]
  public class StringTable
  {
    private static Dictionary<string, string> m_stringDictionary = (Dictionary<string, string>) null;
    private static string m_filepath = string.Empty;

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Load()
    {
      StringTable.m_filepath = MultiLanguageManager.IsDefaultLanguage() ? "Config/StringTable.txt" : string.Format("Config/StringTable_{0}.txt", (object) MultiLanguageManager.CurrentLanguage);
      WWW www = new WWW(Util.GetFileStreamingAssetsPath4WWW(StringTable.m_filepath));
      do
        ;
      while (!www.isDone);
      if (!string.IsNullOrEmpty(www.error))
      {
        Debug.LogError(string.Format("StringTable.Load DefaultServerAddress.Load {0} Error: {1}", (object) StringTable.m_filepath, (object) www.error));
        return false;
      }
      string[] strArray1 = Array.ConvertAll<string, string>(www.text.Split('\n'), (Converter<string, string>) (s => s.Trim()));
      StringTable.m_stringDictionary = new Dictionary<string, string>();
      string empty = string.Empty;
      foreach (string str in strArray1)
      {
        string[] separator = new string[1]{ ",=" };
        string[] strArray2 = str.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        if (strArray2.Length == 2 && !string.IsNullOrEmpty(strArray2[0]) && !string.IsNullOrEmpty(strArray2[1]))
        {
          if (StringTable.m_stringDictionary.TryGetValue(strArray2[0], out empty))
          {
            Debug.LogError(string.Format("StringTable.Load() key {0} repeated.", (object) strArray2[0]));
            return false;
          }
          StringTable.m_stringDictionary.Add(strArray2[0], strArray2[1].Replace("\\n", "\n"));
        }
      }
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsKeyExist(string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string Get(string key)
    {
      if (string.IsNullOrEmpty(key))
      {
        Debug.LogError("StringTable.Get() key is null.");
        return string.Empty;
      }
      if (StringTable.m_stringDictionary == null)
      {
        Debug.LogError("StringTable.Get() m_stringDictionary == null.");
        return string.Empty;
      }
      string empty = string.Empty;
      if (StringTable.m_stringDictionary.TryGetValue(key, out empty))
        return empty;
      Debug.LogError(string.Format("StringTable.Get No key={0} string in {1}", (object) key, (object) StringTable.m_filepath));
      return string.Empty;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static StringTable()
    {
    }
  }
}
