﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.Utility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectLBasic
{
  [CustomLuaClass]
  public class Utility
  {
    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator GetUpdateClientURL(GameObject parentObj = null)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new Utility.\u003CGetUpdateClientURL\u003Ec__Iterator0()
      {
        parentObj = parentObj
      };
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Texture2D ResizeTexture(
      Texture2D pSource,
      ImageFilterMode pFilterMode,
      float pScale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIStateOpen(
      CommonUIStateController ctrl,
      string stateName,
      Action onEnd = null,
      Action<bool> EnableInput = null,
      bool allowToRefreshSameState = true)
    {
      if ((UnityEngine.Object) ctrl == (UnityEngine.Object) null || string.IsNullOrEmpty(stateName))
        return;
      if (EnableInput != null)
        EnableInput(false);
      ctrl.gameObject.SetActive(true);
      ctrl.SetActionForUIStateFinshed(stateName, (Action) (() =>
      {
        if (EnableInput != null)
          EnableInput(true);
        if (onEnd == null)
          return;
        onEnd();
      }));
      ctrl.SetToUIState(stateName, false, allowToRefreshSameState);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIStateClose(
      CommonUIStateController ctrl,
      string stateName,
      Action onEnd = null,
      Action<bool> EnableInput = null,
      bool allowToRefreshSameState = true)
    {
      if ((UnityEngine.Object) ctrl == (UnityEngine.Object) null || string.IsNullOrEmpty(stateName) || !ctrl.gameObject.activeSelf)
        return;
      if (EnableInput != null)
        EnableInput(false);
      ctrl.SetActionForUIStateFinshed(stateName, (Action) (() =>
      {
        MonoBehaviour monoBehaviour = (MonoBehaviour) null;
        if (onEnd != null && (UnityEngine.Object) ctrl.transform.parent != (UnityEngine.Object) null)
          monoBehaviour = ctrl.transform.parent.GetComponentInParent<MonoBehaviour>();
        if ((UnityEngine.Object) monoBehaviour != (UnityEngine.Object) null)
        {
          monoBehaviour.StartCoroutine((IEnumerator) Utility.Co_UIStateCloseFinished(ctrl, onEnd, EnableInput));
        }
        else
        {
          ctrl.gameObject.SetActive(false);
          if (EnableInput != null)
            EnableInput(true);
          if (onEnd == null)
            return;
          onEnd();
        }
      }));
      ctrl.SetToUIState(stateName, false, allowToRefreshSameState);
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator<object> Co_UIStateCloseFinished(
      CommonUIStateController ctrl,
      Action onEnd,
      Action<bool> EnableInput)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator<object>) new Utility.\u003CCo_UIStateCloseFinished\u003Ec__Iterator1()
      {
        ctrl = ctrl,
        EnableInput = EnableInput,
        onEnd = onEnd
      };
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T FindChildComponent<T>(GameObject go, string name, bool warning = true) where T : Component
    {
      if ((UnityEngine.Object) go == (UnityEngine.Object) null)
        return (T) null;
      Transform transform = go.transform.Find(name);
      if ((UnityEngine.Object) transform == (UnityEngine.Object) null)
      {
        if (warning)
          Debug.LogError(name + " not found.");
        return (T) null;
      }
      T component = transform.GetComponent<T>();
      if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
        return component;
      if (warning)
        Debug.LogError(typeof (T).Name + " componnent not found.");
      return (T) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject FindChildGameObject(
      GameObject go,
      string name,
      bool warning = true)
    {
      if ((UnityEngine.Object) go == (UnityEngine.Object) null)
        return (GameObject) null;
      Transform transform = go.transform.Find(name);
      if (!((UnityEngine.Object) transform == (UnityEngine.Object) null))
        return transform.gameObject;
      if (warning)
        Debug.LogError(name + " not found.");
      return (GameObject) null;
    }
  }
}
