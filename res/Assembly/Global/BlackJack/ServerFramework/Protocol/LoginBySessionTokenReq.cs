﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ServerFramework.Protocol.LoginBySessionTokenReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ServerFramework.Protocol
{
  [ProtoContract(Name = "LoginBySessionTokenReq")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class LoginBySessionTokenReq : IExtensible
  {
    private string _SessionToken;
    private string _ClientVersion;
    private string _Localization;
    private int _CurrChannelId;
    private int _BornChannelId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginBySessionTokenReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "SessionToken")]
    public string SessionToken
    {
      get
      {
        return this._SessionToken;
      }
      set
      {
        this._SessionToken = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "ClientVersion")]
    public string ClientVersion
    {
      get
      {
        return this._ClientVersion;
      }
      set
      {
        this._ClientVersion = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Localization")]
    public string Localization
    {
      get
      {
        return this._Localization;
      }
      set
      {
        this._Localization = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "CurrChannelId")]
    [DefaultValue(0)]
    public int CurrChannelId
    {
      get
      {
        return this._CurrChannelId;
      }
      set
      {
        this._CurrChannelId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BornChannelId")]
    public int BornChannelId
    {
      get
      {
        return this._BornChannelId;
      }
      set
      {
        this._BornChannelId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
