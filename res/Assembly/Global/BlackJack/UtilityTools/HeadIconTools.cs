﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.UtilityTools.HeadIconTools
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;

namespace BlackJack.UtilityTools
{
  [CustomLuaClass]
  public class HeadIconTools
  {
    public static int GetHeadPortrait(int headIcon)
    {
      return headIcon >> 16;
    }

    public static int GetHeadFrame(int headIcon)
    {
      return headIcon & (int) ushort.MaxValue;
    }

    public static int GetHeadIcon(int headPortraitId, int headFrameId)
    {
      return headPortraitId << 16 | headFrameId;
    }
  }
}
