﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelfRankingListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SelfRankingListItemUIController : RankingListItemUIController
  {
    [AutoBind("./ComparePanel", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController ComparePanelUIStateCtrl;
    [AutoBind("./ComparePanel/LastRankLevelText", AutoBindAttribute.InitState.NotInit, false)]
    public Text LastRankLevelText;

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateCompareInfo(int lastRankLevel, int curRankLevel)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
