﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RankingUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RankingUIController : UIControllerBase
  {
    protected RankingUIController.MainMenuSelectState m_mainMenuSelectState = RankingUIController.MainMenuSelectState.Power;
    protected bool m_isDuringUpdate;
    protected bool m_isSubMenuPanelPowerShow;
    protected bool m_isSubMenuPanelRiftShow;
    protected bool m_isSubMenuPanelUnchartedShow;
    protected RankingListType m_currRankingType;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ReturnButton;
    [AutoBind("./SubMenumGroup/NoneSubMenuArea", AutoBindAttribute.InitState.NotInit, false)]
    public RankingSubMenuUIController SubMenuUICtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Power", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx MainMenuPowerButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Power", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController MainMenuPowerStateCtrl;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/TopHero", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuTopHeroButton;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/TopHero", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuTopHeroStateCtrl;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/AllHero", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuAllHeroButton;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/AllHero", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuAllHeroStateCtrl;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/ChampionHero", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuChampionHeroButton;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/ChampionHero", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuChampionHeroStateCtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Rift", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx MainMenuRiftButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Rift", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController MainMenuRiftStateCtrl;
    [AutoBind("./SubMenumGroup/RiftMenuList/BGImage/BGImage1/Content/RiftChapterStar", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuRiftChapterStarButton;
    [AutoBind("./SubMenumGroup/RiftMenuList/BGImage/BGImage1/Content/RiftChapterStar", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuRiftChapterStarStateCtrl;
    [AutoBind("./SubMenumGroup/RiftMenuList/BGImage/BGImage1/Content/RiftAchievement", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuRiftAchievementButton;
    [AutoBind("./SubMenumGroup/RiftMenuList/BGImage/BGImage1/Content/RiftAchievement", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuRiftAchievementStateCtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Uncharted", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx MainMenuUnchartedButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Uncharted", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController MainMenuUnchartedStateCtrl;
    [AutoBind("./SubMenumGroup/UnchartedMenuList/BGImage/BGImage1/Content/ClimbTower", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuClimbTowerButton;
    [AutoBind("./SubMenumGroup/UnchartedMenuList/BGImage/BGImage1/Content/ClimbTower", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuClimbTowerStateCtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Stage", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx MainMenuStageButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Stage", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController MainMenuStageStateCtrl;
    [AutoBind("./SubMenumGroup/PowerMenulList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelPowerStateCtrl;
    [AutoBind("./SubMenumGroup/RiftMenuList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelRiftStateCtrl;
    [AutoBind("./SubMenumGroup/UnchartedMenuList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelUnchartedStateCtrl;
    [AutoBind("./SubMenumGroup/StageMenuList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelStageStateCtrl;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRankingUICtrl(RankingListType rankingType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected RankingUIController.MainMenuSelectState GetMainMenuSelectStateFromRankingType(
      RankingListType rankingType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetMainMenuSelectState(RankingUIController.MainMenuSelectState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetAllMainMenuUnselect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetSubMenuSelectState(RankingListType rankType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetAllSubMenuToggleUnselect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowPowerSubMenuPanel(bool isshow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowRiftSubMenuPanel(bool isshow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowUnchartedSubMenuPanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void HideAllSubMenuPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnNoneSubMenuAreaClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMainMenuPowerClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMainMenuRiftClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMainMenuUnchartedClick()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnMainMenuStageClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRankingTypeMenuClick(RankingListType rankListType)
    {
    }

    protected void OnSubMenuTopHeroClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.TopHero);
    }

    protected void OnSubMenuAllHeroClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.AllHero);
    }

    protected void OnSubMenuChampionHeroClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.ChampionHero);
    }

    protected void OnSubMenuRiftChapterStarClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.RiftChapterStar);
    }

    protected void OnSubMenuRiftAchievementClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.RiftAchievement);
    }

    protected void OnSubMenuClimbTowerClick()
    {
      this.OnRankingTypeMenuClick(RankingListType.TowerFloor);
    }

    public event Action<RankingListType> EventOnRankingTypeMenuClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnResetRankingType
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnRetunButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public enum MainMenuSelectState
    {
      None,
      Power,
      Rift,
      Uncharted,
      Stage,
    }
  }
}
