﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SoldierShowItemController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Scene;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class SoldierShowItemController
  {
    public UISpineGraphic m_soldierSkinGraphic;
    public Text m_nameText;
    public GameObject m_spineObj;
    public CommonUIStateController m_itemStateCtrl;
    private GameObject m_item;
    [DoNotToLua]
    private SoldierShowItemController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorGameObject_hotfix;
    private LuaFunction m_SetActiveBoolean_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public SoldierShowItemController(GameObject soldierItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActive(bool active)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public SoldierShowItemController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private SoldierShowItemController m_owner;

      public LuaExportHelper(SoldierShowItemController owner)
      {
        this.m_owner = owner;
      }

      public GameObject m_item
      {
        get
        {
          return this.m_owner.m_item;
        }
        set
        {
          this.m_owner.m_item = value;
        }
      }
    }
  }
}
