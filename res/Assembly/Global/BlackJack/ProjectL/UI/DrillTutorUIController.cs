﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DrillTutorUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class DrillTutorUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./LeftPanel/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_recommendHeroGroup;
    [AutoBind("./Detail/HeroGroupButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroGroupButton;
    [AutoBind("./Detail/HeroGroup/HeroEmptyButton1/HeroItemDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chooseHeroDummy1;
    [AutoBind("./Detail/HeroGroup/HeroEmptyButton2/HeroItemDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chooseHeroDummy2;
    [AutoBind("./Detail/HeroGroup/HeroEmptyButton3/HeroItemDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chooseHeroDummy3;
    [AutoBind("./Detail/AchivementValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroDetailAchivementValueText;
    [AutoBind("./HeroListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroListPanelStateCtrl;
    [AutoBind("./HeroListPanel/BGBackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroListPanelBGBackButton;
    [AutoBind("./HeroListPanel/LeftPanel/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroListPanelHeroGroup;
    [AutoBind("./HeroListPanel/HeroList/HeroListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_heroListScrollRect;
    [AutoBind("./HeroListPanel/HeroList/HeroListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroListScrollViewContent;
    [AutoBind("./HeroListPanel/HeroList/ResultDetailPanel/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroListConfirmButton;
    [AutoBind("./HeroListPanel/HeroList/ResultDetailPanel/ChooseValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroListChooseCountValueText;
    [AutoBind("./HeroListPanel/HeroList/ResultDetailPanel/AchivementValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroListAchivementValueText;
    [AutoBind("./HeroListPanel/HeroList/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroListSortButton;
    [AutoBind("./HeroListPanel/HeroList/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroListSortButtonTypeText;
    [AutoBind("./HeroListPanel/HeroList/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroListSortTypesStateCtrl;
    [AutoBind("./HeroListPanel/HeroList/SortTypes/GridLayout/Level", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_heroListSortTypesLevelToggle;
    [AutoBind("./HeroListPanel/HeroList/SortTypes/GridLayout/Star", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_heroListSortTypesStarLvToggle;
    [AutoBind("./HeroListPanel/HeroList/SortTypes/GridLayout/Achievement", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_heroListSortTypesAchievementToggle;
    [AutoBind("./Detail/DifficultyGroup/LevelBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_detailDifficultyLevelBGImage;
    [AutoBind("./Detail/TeamNumber/TeamNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamNumberText;
    [AutoBind("./Detail/DifficultyGroup/DifficultyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_detailDifficultyGroupScrollRect;
    [AutoBind("./Detail/DifficultyGroup/DifficultyGroup/Viewport/Group", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_detailDifficultyGroupContent;
    [AutoBind("./Detail/TodayRewardGroup/RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_detailRewardGroup;
    [AutoBind("./Detail/TeachingTimeGroup/Time1", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_detailTeachingTimeToggle1;
    [AutoBind("./Detail/TeachingTimeGroup/Time2", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_detailTeachingTimeToggle2;
    [AutoBind("./Detail/TeachingTimeGroup/Time3", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_detailTeachingTimeToggle3;
    [AutoBind("./Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detailConfirmButton;
    [AutoBind("./Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_detailConfirmButtonStateCtrl;
    [AutoBind("./TeachStart", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teachStartPanelStateCtrl;
    [AutoBind("./Prefab/HeroItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroItemprefab;
    private DrillTutorUIController.HeroSortType m_curHeroSortType;
    private List<int> m_curChooseHeroIds;
    private List<int> m_tempChooseHeroIds;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private HeroAssistantsTask m_curHeroAssistantsTask;
    private int m_curDifficultLevel;
    private int m_curWorkSeconds;
    private List<HeroAssistantsTask> m_allTaskList;
    private int m_slot;
    private bool m_isFirstIn;

    [MethodImpl((MethodImplOptions) 32768)]
    public DrillTutorUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInDrillTutor(int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDifficultScrollViewPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetConfirmButtonState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDifficultBgBySlot()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDifficultInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDifficultItemClick(TrainingDifficultItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetWorkHour()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRecommendHeroGroup(GameObject parentGameObject)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurChooseHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRewardGroupList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> GetRewardsList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetChooseHeroListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroListItemComparator(Hero h1, Hero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTrainingHeroItemClick(TrainingHeroItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroListResultDetailPanel()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CalcHeroTotalAchievementvalue(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChooseHeroButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroListConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroListPanelBGBackButtonClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDetailConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayStartTeachEffect(Action OnEffectFinish)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelToggleChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStarLvToggleChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementToggleChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroSortButtonClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSortTypesPanel()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnShowTutorHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<int, List<int>, int, int> EventOnConfirmAssistant
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public enum HeroSortType
    {
      Level,
      StarLv,
      Achievement,
    }
  }
}
