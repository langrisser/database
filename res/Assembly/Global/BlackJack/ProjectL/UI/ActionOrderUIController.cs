﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ActionOrderUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ActionOrderUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Panel/Slots", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actionOrdersGameObject;
    [AutoBind("./Panel/OkButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_okButton;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/ActionOrderButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actionOrderButtonPrefab;
    private ArenaActionOrderButton[] m_actionOrderButtons;
    private ArenaActionOrderButton m_draggingActionOrderButton;
    private int m_draggingActionOrderButtonIndex;
    private Camera m_camera;

    [MethodImpl((MethodImplOptions) 32768)]
    private ActionOrderUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationPause(bool isPause)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationFocus(bool focus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeros(List<BattleHero> heros)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroActionOrderIndex(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ArenaActionOrderButton CreateActionOrderButton(
      BattleHero hero,
      Transform parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateDraggingActionOrderButton(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyDragginActionOrderButton()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveDraggingActionOrderButton(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DropDraggingActionOrderButton(Vector3 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOkButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void ActionOrderButton_OnClick(ArenaActionOrderButton b)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionOrderButton_OnBeginDrag(ArenaActionOrderButton b, PointerEventData eventData)
    {
    }

    private void ActionOrderButton_OnEndDrag(ArenaActionOrderButton b, PointerEventData eventData)
    {
      this.DestroyDragginActionOrderButton();
    }

    private void ActionOrderButton_OnDrag(PointerEventData eventData)
    {
      this.MoveDraggingActionOrderButton(eventData.position);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionOrderButton_OnDrop(PointerEventData eventData)
    {
    }

    public event Action EventOnConfirm
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
