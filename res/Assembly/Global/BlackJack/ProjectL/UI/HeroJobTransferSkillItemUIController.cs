﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroJobTransferSkillItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroJobTransferSkillItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillIconImg;
    [AutoBind("./Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_passiveSkillIconObj;
    private GameObject m_descParent;
    public ConfigDataSkillInfo m_skillInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSkillItem(ConfigDataSkillInfo skillInfo, GameObject descParent)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
