﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.VoiceRecordUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class VoiceRecordUIController : UIControllerBase
  {
    [AutoBind("./SoundMessage/Send/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text voiceTimeText;
    [AutoBind("./SoundMessage", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController voiceRecordStateUICtrl;
    [AutoBind("./SoundMessage/Send/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    public Image recordTimeProgressBar;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public VoiceRecordUIController()
    {
    }

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceRecordTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideVoiceRecordTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceCancelTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceShortTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateVoiceRecordTime(float time)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
