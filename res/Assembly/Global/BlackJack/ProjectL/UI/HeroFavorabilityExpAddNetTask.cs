﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroFavorabilityExpAddNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroFavorabilityExpAddNetTask : UINetTask
  {
    private int m_heroId;
    private GoodsType m_goodsType;
    private int m_itemId;
    private int m_count;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroFavorabilityExpAddNetTask(int heroId, GoodsType goodsType, int itemId, int count)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroFavorabilityExpAddAck(int result, List<Goods> rewards)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<Goods> Rewards { private set; get; }
  }
}
