﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildInviteItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GuildInviteItemUIController : UIControllerBase
  {
    [AutoBind("./Char/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_charLevelText;
    [AutoBind("./Char/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_charIcon;
    [AutoBind("./Char/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameDummy;
    [AutoBind("./Char/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_charName;
    [AutoBind("./Char/SociatyNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteGuildName;
    [AutoBind("./InfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detialButton;
    [AutoBind("./AgreeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_acceptButton;
    [AutoBind("./RefuseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_refuseButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private GuildJoinInvitation m_guildJoinInvitation;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Init(GuildJoinInvitation guildJoinInvitation)
    {
      this.m_guildJoinInvitation = guildJoinInvitation;
      this.Refresh();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAcceptClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefuseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDetailClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action ItemRefreshEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
