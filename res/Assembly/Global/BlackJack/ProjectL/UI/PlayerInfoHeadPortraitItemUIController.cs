﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerInfoHeadPortraitItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class PlayerInfoHeadPortraitItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_toggle;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headPortraitImage;
    public int HeadPortraitId;
    public string HeadPortraitName;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItemInfo(int headPortraitId, string headPortraitName, bool isSelected)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectToggleValueChanged(bool isOn)
    {
    }

    public event Action<bool, PlayerInfoHeadPortraitItemUIController> EventOnSelectToggleValueChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
