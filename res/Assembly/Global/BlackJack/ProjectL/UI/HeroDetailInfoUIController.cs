﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroDetailInfoUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ArmyImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroArmyImg;
    [AutoBind("./Lv/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroLvText;
    [AutoBind("./Lv/ValueText/MaxText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroLvMaxText;
    [AutoBind("./Exp/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroExpText;
    [AutoBind("./Exp/ProgressImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroExpImg;
    [AutoBind("./Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoHeroGraphic;
    [AutoBind("./PropertyMsgButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_propertyMsgButton;
    [AutoBind("./Explanation", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_explanationStateCtrl;
    [AutoBind("./Explanation/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_explanationBackgroundButton;
    [AutoBind("./Explanation/Panel/BGImage/FrameImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_explanationText;
    [AutoBind("./Keyword", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tagButtonsContent;
    [AutoBind("./KeywordPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_tagPanelStateCtrl;
    [AutoBind("./KeywordPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tagPanelBGButton;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/InfoPanel/TitleText1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tagPanelTitleText;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/InfoPanel/Scrollrect/Viewport/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tagPanelDescText;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/InfoPanel/KeywordIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_tagPanelIconImage;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tagPanelListScrollViewContent;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_tagPanelListScrollRect;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/Prefab/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tagPanelHeroIconImagePrefab;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/CloseBth", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tagPanelCloseBtn;
    [AutoBind("./Property/HP/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropHPImg;
    [AutoBind("./Property/DF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropDFImg;
    [AutoBind("./Property/AT/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropATImg;
    [AutoBind("./Property/MagicDF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropMagicDFImg;
    [AutoBind("./Property/Magic/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropMagicImg;
    [AutoBind("./Property/DEX/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropDEXImg;
    [AutoBind("./Property/HP/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropHPValueText;
    [AutoBind("./Property/DF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDFValueText;
    [AutoBind("./Property/AT/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropATValueText;
    [AutoBind("./Property/MagicDF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicDFValueText;
    [AutoBind("./Property/Magic/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicValueText;
    [AutoBind("./Property/DEX/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDEXValueText;
    [AutoBind("./Property/HP/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropHPAddText;
    [AutoBind("./Property/DF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDFAddText;
    [AutoBind("./Property/AT/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropATAddText;
    [AutoBind("./Property/MagicDF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicDFAddText;
    [AutoBind("./Property/Magic/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicAddText;
    [AutoBind("./Property/DEX/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDEXAddText;
    [AutoBind("./Talent/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroTalentIcon;
    [AutoBind("./Talent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroTalentNameText;
    [AutoBind("./Talent/DescScrollView/Viewport/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroTalentDescText;
    [AutoBind("./Skill/Costs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoHeroCurSkillCostsObj;
    [AutoBind("./Skill/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoHeroCurSkillContent;
    [AutoBind("./Skill/SkillBg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoHeroCurSkillContentBg;
    [AutoBind("./Skill/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoHeroCurSkillChangeButton;
    [AutoBind("./Skill/SkillItemDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoCurSkillDescPanel;
    [AutoBind("./AddExpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoAddExpButton;
    [AutoBind("./SkinInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skinInfoButton;
    private Hero m_hero;
    private UISpineGraphic m_playerHeroGraphic;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private HeroDetailInfoUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_UpdateViewInInfoStateHero_hotfix;
    private LuaFunction m_ShowHeroInfo_hotfix;
    private LuaFunction m_SetHeroTagInfo_hotfix;
    private LuaFunction m_OpenTagPanelConfigDataHeroTagInfo_hotfix;
    private LuaFunction m_SetTagPanelInfoConfigDataHeroTagInfo_hotfix;
    private LuaFunction m_SortRelatedHeroIdByRankInt32Int32_hotfix;
    private LuaFunction m_CloseTagPanel_hotfix;
    private LuaFunction m_SetHeroPropertyHero_hotfix;
    private LuaFunction m_SetHeroTalentHero_hotfix;
    private LuaFunction m_SetCurSelectSkills_hotfix;
    private LuaFunction m_OnSkillItemClickHeroSkillItemUIController_hotfix;
    private LuaFunction m_OnAddExpButtonClick_hotfix;
    private LuaFunction m_OnSkinInfoButtonClcik_hotfix;
    private LuaFunction m_OnJobUpButtonClick_hotfix;
    private LuaFunction m_OnChangeSkillButtonClick_hotfix;
    private LuaFunction m_SetCommonUIStateString_hotfix;
    private LuaFunction m_OnPropertyMsgButtonClick_hotfix;
    private LuaFunction m_OnExplanationBackgroundButtonClick_hotfix;
    private LuaFunction m_IsShowHeroLittleSpineBoolean_hotfix;
    private LuaFunction m_add_EventOnSetDetailStateAction`1_hotfix;
    private LuaFunction m_remove_EventOnSetDetailStateAction`1_hotfix;
    private LuaFunction m_add_EventOnSkinInfoButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnSkinInfoButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnJobUpButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnJobUpButtonClickAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInInfoState(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowHeroInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroTagInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OpenTagPanel(ConfigDataHeroTagInfo tagInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTagPanelInfo(ConfigDataHeroTagInfo tagInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortRelatedHeroIdByRank(int heroId1, int heroId2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseTagPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroProperty(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroTalent(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurSelectSkills()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemClick(HeroSkillItemUIController skillCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddExpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkinInfoButtonClcik()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeSkillButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonUIState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPropertyMsgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExplanationBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IsShowHeroLittleSpine(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<string> EventOnSetDetailState
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSkinInfoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnJobUpButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroDetailInfoUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSetDetailState(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSetDetailState(string obj)
    {
      this.EventOnSetDetailState = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSkinInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSkinInfoButtonClick()
    {
      this.EventOnSkinInfoButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnJobUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnJobUpButtonClick()
    {
      this.EventOnJobUpButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroDetailInfoUIController m_owner;

      public LuaExportHelper(HeroDetailInfoUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnSetDetailState(string obj)
      {
        this.m_owner.__callDele_EventOnSetDetailState(obj);
      }

      public void __clearDele_EventOnSetDetailState(string obj)
      {
        this.m_owner.__clearDele_EventOnSetDetailState(obj);
      }

      public void __callDele_EventOnSkinInfoButtonClick()
      {
        this.m_owner.__callDele_EventOnSkinInfoButtonClick();
      }

      public void __clearDele_EventOnSkinInfoButtonClick()
      {
        this.m_owner.__clearDele_EventOnSkinInfoButtonClick();
      }

      public void __callDele_EventOnJobUpButtonClick()
      {
        this.m_owner.__callDele_EventOnJobUpButtonClick();
      }

      public void __clearDele_EventOnJobUpButtonClick()
      {
        this.m_owner.__clearDele_EventOnJobUpButtonClick();
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Image m_infoHeroArmyImg
      {
        get
        {
          return this.m_owner.m_infoHeroArmyImg;
        }
        set
        {
          this.m_owner.m_infoHeroArmyImg = value;
        }
      }

      public Text m_infoHeroLvText
      {
        get
        {
          return this.m_owner.m_infoHeroLvText;
        }
        set
        {
          this.m_owner.m_infoHeroLvText = value;
        }
      }

      public Text m_infoHeroLvMaxText
      {
        get
        {
          return this.m_owner.m_infoHeroLvMaxText;
        }
        set
        {
          this.m_owner.m_infoHeroLvMaxText = value;
        }
      }

      public Text m_infoHeroExpText
      {
        get
        {
          return this.m_owner.m_infoHeroExpText;
        }
        set
        {
          this.m_owner.m_infoHeroExpText = value;
        }
      }

      public Image m_infoHeroExpImg
      {
        get
        {
          return this.m_owner.m_infoHeroExpImg;
        }
        set
        {
          this.m_owner.m_infoHeroExpImg = value;
        }
      }

      public GameObject m_infoHeroGraphic
      {
        get
        {
          return this.m_owner.m_infoHeroGraphic;
        }
        set
        {
          this.m_owner.m_infoHeroGraphic = value;
        }
      }

      public Button m_propertyMsgButton
      {
        get
        {
          return this.m_owner.m_propertyMsgButton;
        }
        set
        {
          this.m_owner.m_propertyMsgButton = value;
        }
      }

      public CommonUIStateController m_explanationStateCtrl
      {
        get
        {
          return this.m_owner.m_explanationStateCtrl;
        }
        set
        {
          this.m_owner.m_explanationStateCtrl = value;
        }
      }

      public Button m_explanationBackgroundButton
      {
        get
        {
          return this.m_owner.m_explanationBackgroundButton;
        }
        set
        {
          this.m_owner.m_explanationBackgroundButton = value;
        }
      }

      public Text m_explanationText
      {
        get
        {
          return this.m_owner.m_explanationText;
        }
        set
        {
          this.m_owner.m_explanationText = value;
        }
      }

      public GameObject m_tagButtonsContent
      {
        get
        {
          return this.m_owner.m_tagButtonsContent;
        }
        set
        {
          this.m_owner.m_tagButtonsContent = value;
        }
      }

      public CommonUIStateController m_tagPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_tagPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_tagPanelStateCtrl = value;
        }
      }

      public Button m_tagPanelBGButton
      {
        get
        {
          return this.m_owner.m_tagPanelBGButton;
        }
        set
        {
          this.m_owner.m_tagPanelBGButton = value;
        }
      }

      public Text m_tagPanelTitleText
      {
        get
        {
          return this.m_owner.m_tagPanelTitleText;
        }
        set
        {
          this.m_owner.m_tagPanelTitleText = value;
        }
      }

      public Text m_tagPanelDescText
      {
        get
        {
          return this.m_owner.m_tagPanelDescText;
        }
        set
        {
          this.m_owner.m_tagPanelDescText = value;
        }
      }

      public Image m_tagPanelIconImage
      {
        get
        {
          return this.m_owner.m_tagPanelIconImage;
        }
        set
        {
          this.m_owner.m_tagPanelIconImage = value;
        }
      }

      public GameObject m_tagPanelListScrollViewContent
      {
        get
        {
          return this.m_owner.m_tagPanelListScrollViewContent;
        }
        set
        {
          this.m_owner.m_tagPanelListScrollViewContent = value;
        }
      }

      public ScrollRect m_tagPanelListScrollRect
      {
        get
        {
          return this.m_owner.m_tagPanelListScrollRect;
        }
        set
        {
          this.m_owner.m_tagPanelListScrollRect = value;
        }
      }

      public GameObject m_tagPanelHeroIconImagePrefab
      {
        get
        {
          return this.m_owner.m_tagPanelHeroIconImagePrefab;
        }
        set
        {
          this.m_owner.m_tagPanelHeroIconImagePrefab = value;
        }
      }

      public Button m_tagPanelCloseBtn
      {
        get
        {
          return this.m_owner.m_tagPanelCloseBtn;
        }
        set
        {
          this.m_owner.m_tagPanelCloseBtn = value;
        }
      }

      public Image m_infoHeroPropHPImg
      {
        get
        {
          return this.m_owner.m_infoHeroPropHPImg;
        }
        set
        {
          this.m_owner.m_infoHeroPropHPImg = value;
        }
      }

      public Image m_infoHeroPropDFImg
      {
        get
        {
          return this.m_owner.m_infoHeroPropDFImg;
        }
        set
        {
          this.m_owner.m_infoHeroPropDFImg = value;
        }
      }

      public Image m_infoHeroPropATImg
      {
        get
        {
          return this.m_owner.m_infoHeroPropATImg;
        }
        set
        {
          this.m_owner.m_infoHeroPropATImg = value;
        }
      }

      public Image m_infoHeroPropMagicDFImg
      {
        get
        {
          return this.m_owner.m_infoHeroPropMagicDFImg;
        }
        set
        {
          this.m_owner.m_infoHeroPropMagicDFImg = value;
        }
      }

      public Image m_infoHeroPropMagicImg
      {
        get
        {
          return this.m_owner.m_infoHeroPropMagicImg;
        }
        set
        {
          this.m_owner.m_infoHeroPropMagicImg = value;
        }
      }

      public Image m_infoHeroPropDEXImg
      {
        get
        {
          return this.m_owner.m_infoHeroPropDEXImg;
        }
        set
        {
          this.m_owner.m_infoHeroPropDEXImg = value;
        }
      }

      public Text m_infoHeroPropHPValueText
      {
        get
        {
          return this.m_owner.m_infoHeroPropHPValueText;
        }
        set
        {
          this.m_owner.m_infoHeroPropHPValueText = value;
        }
      }

      public Text m_infoHeroPropDFValueText
      {
        get
        {
          return this.m_owner.m_infoHeroPropDFValueText;
        }
        set
        {
          this.m_owner.m_infoHeroPropDFValueText = value;
        }
      }

      public Text m_infoHeroPropATValueText
      {
        get
        {
          return this.m_owner.m_infoHeroPropATValueText;
        }
        set
        {
          this.m_owner.m_infoHeroPropATValueText = value;
        }
      }

      public Text m_infoHeroPropMagicDFValueText
      {
        get
        {
          return this.m_owner.m_infoHeroPropMagicDFValueText;
        }
        set
        {
          this.m_owner.m_infoHeroPropMagicDFValueText = value;
        }
      }

      public Text m_infoHeroPropMagicValueText
      {
        get
        {
          return this.m_owner.m_infoHeroPropMagicValueText;
        }
        set
        {
          this.m_owner.m_infoHeroPropMagicValueText = value;
        }
      }

      public Text m_infoHeroPropDEXValueText
      {
        get
        {
          return this.m_owner.m_infoHeroPropDEXValueText;
        }
        set
        {
          this.m_owner.m_infoHeroPropDEXValueText = value;
        }
      }

      public Text m_infoHeroPropHPAddText
      {
        get
        {
          return this.m_owner.m_infoHeroPropHPAddText;
        }
        set
        {
          this.m_owner.m_infoHeroPropHPAddText = value;
        }
      }

      public Text m_infoHeroPropDFAddText
      {
        get
        {
          return this.m_owner.m_infoHeroPropDFAddText;
        }
        set
        {
          this.m_owner.m_infoHeroPropDFAddText = value;
        }
      }

      public Text m_infoHeroPropATAddText
      {
        get
        {
          return this.m_owner.m_infoHeroPropATAddText;
        }
        set
        {
          this.m_owner.m_infoHeroPropATAddText = value;
        }
      }

      public Text m_infoHeroPropMagicDFAddText
      {
        get
        {
          return this.m_owner.m_infoHeroPropMagicDFAddText;
        }
        set
        {
          this.m_owner.m_infoHeroPropMagicDFAddText = value;
        }
      }

      public Text m_infoHeroPropMagicAddText
      {
        get
        {
          return this.m_owner.m_infoHeroPropMagicAddText;
        }
        set
        {
          this.m_owner.m_infoHeroPropMagicAddText = value;
        }
      }

      public Text m_infoHeroPropDEXAddText
      {
        get
        {
          return this.m_owner.m_infoHeroPropDEXAddText;
        }
        set
        {
          this.m_owner.m_infoHeroPropDEXAddText = value;
        }
      }

      public Image m_infoHeroTalentIcon
      {
        get
        {
          return this.m_owner.m_infoHeroTalentIcon;
        }
        set
        {
          this.m_owner.m_infoHeroTalentIcon = value;
        }
      }

      public Text m_infoHeroTalentNameText
      {
        get
        {
          return this.m_owner.m_infoHeroTalentNameText;
        }
        set
        {
          this.m_owner.m_infoHeroTalentNameText = value;
        }
      }

      public Text m_infoHeroTalentDescText
      {
        get
        {
          return this.m_owner.m_infoHeroTalentDescText;
        }
        set
        {
          this.m_owner.m_infoHeroTalentDescText = value;
        }
      }

      public GameObject m_infoHeroCurSkillCostsObj
      {
        get
        {
          return this.m_owner.m_infoHeroCurSkillCostsObj;
        }
        set
        {
          this.m_owner.m_infoHeroCurSkillCostsObj = value;
        }
      }

      public GameObject m_infoHeroCurSkillContent
      {
        get
        {
          return this.m_owner.m_infoHeroCurSkillContent;
        }
        set
        {
          this.m_owner.m_infoHeroCurSkillContent = value;
        }
      }

      public GameObject m_infoHeroCurSkillContentBg
      {
        get
        {
          return this.m_owner.m_infoHeroCurSkillContentBg;
        }
        set
        {
          this.m_owner.m_infoHeroCurSkillContentBg = value;
        }
      }

      public Button m_infoHeroCurSkillChangeButton
      {
        get
        {
          return this.m_owner.m_infoHeroCurSkillChangeButton;
        }
        set
        {
          this.m_owner.m_infoHeroCurSkillChangeButton = value;
        }
      }

      public GameObject m_infoCurSkillDescPanel
      {
        get
        {
          return this.m_owner.m_infoCurSkillDescPanel;
        }
        set
        {
          this.m_owner.m_infoCurSkillDescPanel = value;
        }
      }

      public Button m_infoAddExpButton
      {
        get
        {
          return this.m_owner.m_infoAddExpButton;
        }
        set
        {
          this.m_owner.m_infoAddExpButton = value;
        }
      }

      public Button m_skinInfoButton
      {
        get
        {
          return this.m_owner.m_skinInfoButton;
        }
        set
        {
          this.m_owner.m_skinInfoButton = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public UISpineGraphic m_playerHeroGraphic
      {
        get
        {
          return this.m_owner.m_playerHeroGraphic;
        }
        set
        {
          this.m_owner.m_playerHeroGraphic = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ShowHeroInfo()
      {
        this.m_owner.ShowHeroInfo();
      }

      public void SetHeroTagInfo()
      {
        this.m_owner.SetHeroTagInfo();
      }

      public void OpenTagPanel(ConfigDataHeroTagInfo tagInfo)
      {
        this.m_owner.OpenTagPanel(tagInfo);
      }

      public void SetTagPanelInfo(ConfigDataHeroTagInfo tagInfo)
      {
        this.m_owner.SetTagPanelInfo(tagInfo);
      }

      public int SortRelatedHeroIdByRank(int heroId1, int heroId2)
      {
        return this.m_owner.SortRelatedHeroIdByRank(heroId1, heroId2);
      }

      public void CloseTagPanel()
      {
        this.m_owner.CloseTagPanel();
      }

      public void SetHeroProperty(Hero hero)
      {
        this.m_owner.SetHeroProperty(hero);
      }

      public void SetHeroTalent(Hero hero)
      {
        this.m_owner.SetHeroTalent(hero);
      }

      public void SetCurSelectSkills()
      {
        this.m_owner.SetCurSelectSkills();
      }

      public void OnSkillItemClick(HeroSkillItemUIController skillCtrl)
      {
        this.m_owner.OnSkillItemClick(skillCtrl);
      }

      public void OnAddExpButtonClick()
      {
        this.m_owner.OnAddExpButtonClick();
      }

      public void OnSkinInfoButtonClcik()
      {
        this.m_owner.OnSkinInfoButtonClcik();
      }

      public void OnJobUpButtonClick()
      {
        this.m_owner.OnJobUpButtonClick();
      }

      public void OnChangeSkillButtonClick()
      {
        this.m_owner.OnChangeSkillButtonClick();
      }

      public void OnPropertyMsgButtonClick()
      {
        this.m_owner.OnPropertyMsgButtonClick();
      }

      public void OnExplanationBackgroundButtonClick()
      {
        this.m_owner.OnExplanationBackgroundButtonClick();
      }
    }
  }
}
