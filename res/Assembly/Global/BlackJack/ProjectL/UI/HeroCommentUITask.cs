﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroCommentUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroCommentUITask : UITask
  {
    protected RankingListInfo m_currRankList;
    public static string UIMode_HeroComment;
    public static string UIModel_RankingList;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private Hero m_hero;
    private bool m_isNeedResetScrollViewPos;
    private HeroCommentUIController m_heroCommentUIController;
    private bool m_isNeedShowHeroDetailPanel;
    private Hero m_playerHero;
    private HeroCommentEntry m_playerComment;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroCommentUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
    }

    protected override bool OnStart(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnStart(intent);
    }

    protected override bool OnResume(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnResume(intent);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedUpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateDataCache()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<string> CollectAllDynamicResForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCommentUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCommentUIController_OnCommentGet(int heroId, bool isNeedReset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCommentUIController_OnRankingQuery(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCommentUIController_OnRankingListLockClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCommentUIController_OnCommentSend(int heroId, string commentStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCommentUIController_OnCommentPraised(int heroId, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCommentUIController_OnCommenterHeroView(int heroId, HeroCommentEntry comment)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static HeroCommentUITask()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
