﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerInfoUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class PlayerInfoUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ProjectLPlayerContext m_playerContext;
    private PlayerInfoUIController m_playerInfoUIController;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerInfoUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    protected override void OnStop()
    {
      base.OnStop();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUIController_OnClose()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUIController_OnShowChangeIconPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUIController_OnChangeName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUIController_OnWatchMyBusinessCard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUIController_OnChangeHeadPortraitAndHeadFrame(
      int headPortraitId,
      int headFreameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUIController_OnRedemptionCodeUse(string key, Action<int> OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUIController_OnShowFanpage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUIController_OnShowSign()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SignUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUIController_OnShowPlayerSetting()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUIController_OnChangeRECState(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUIController_OnCrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    private void PlayerInfoUIController_OnOpenChangeLanguage()
    {
      LanguageChangeUITask.StartUITask(this.CurrentIntent);
    }

    private void PDSDK_OnBindGuestSuccess()
    {
      this.UpdateView();
    }

    private void PlayerInfoUIController_OnUserBindClick()
    {
      UserBindUITask.StartUITask(this.CurrentIntent);
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
