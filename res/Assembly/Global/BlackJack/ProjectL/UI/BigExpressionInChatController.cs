﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BigExpressionInChatController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [ControllerDescription("UI/Chat_ABS/Prefab/ExpressionGroupInChatPrefab.prefab", "ExpressionGroupInChatPrefab")]
  [CustomLuaClass]
  public class BigExpressionInChatController : UIControllerBase
  {
    private float m_scrollRectLastPos;
    private ChatChannel m_chatChannel;
    private int m_pageCapacity;
    private List<BigExpressionItem> m_bigExpressionItemList;
    private List<GameObject> m_pageList;
    private List<CommonUIStateController> m_pagePointStateControllerList;
    [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject Content;
    [AutoBind("./Scroll View/Viewport/Expression", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject Expression;
    [AutoBind("./Scroll View/Viewport/Page", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject Page;
    [AutoBind("./PagePoint", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject PagePointNode;
    [AutoBind("./PagePoint/PagePoint", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject PagePoint;
    [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
    public ScrollRect ScrollView;

    [MethodImpl((MethodImplOptions) 32768)]
    public BigExpressionInChatController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetChannel(ChatChannel channel)
    {
      this.m_chatChannel = channel;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator DelayInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ConfigDataBigExpressionInfo> GetChatExpressionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionClick(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEndDrag(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnValueChange(Vector2 rectEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPagePointActivity(int page)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnExpressionClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
