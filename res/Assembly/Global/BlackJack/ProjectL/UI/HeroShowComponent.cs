﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroShowComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroShowComponent
  {
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private GameObject m_root;
    private Dictionary<string, CommonUIStateController> heroDictionary;
    [DoNotToLua]
    private HeroShowComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitGameObject_hotfix;
    private LuaFunction m_Dispose_hotfix;
    private LuaFunction m_ShowComponentBoolean_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroShowComponent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(GameObject root)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowComponent(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public HeroShowComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroShowComponent m_owner;

      public LuaExportHelper(HeroShowComponent owner)
      {
        this.m_owner = owner;
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public GameObject m_root
      {
        get
        {
          return this.m_owner.m_root;
        }
        set
        {
          this.m_owner.m_root = value;
        }
      }

      public Dictionary<string, CommonUIStateController> heroDictionary
      {
        get
        {
          return this.m_owner.heroDictionary;
        }
        set
        {
          this.m_owner.heroDictionary = value;
        }
      }
    }
  }
}
