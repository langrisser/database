﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArchiveItemDetailInfoController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ArchiveItemDetailInfoController : UIControllerBase
  {
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemIamge;
    [AutoBind("./EquipInfo/EquipGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipLimitGroupAnimation;
    [AutoBind("./EquipInfo/EquipGroup/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipLimitContent;
    [AutoBind("./EquipInfo/EquipGroup/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./EquipInfo/ExplainFrontToggle/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentExplain;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_HPGameObject;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_HPText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ATGameObject;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ATText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_DFGameObject;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_DFText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_MagicDFGameObject;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_MagicDFText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_MagicGameObject;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_MagicText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_DexGameObject;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_DexText;
    [AutoBind("./EquipInfo/PropertyFrontToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillAnimation;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillContentAnimation;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/SkillContent/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillBelongText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/SkillContent/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillBelongBGText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/SkillContent/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillUnlockConditionText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/SkillContent/DescScrollView/Mask/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDescText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/SkillContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillNameText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private EquipmentArchiveUIController.EquipmentInfoWrap m_equipmentInfoWrap;
    [DoNotToLua]
    private ArchiveItemDetailInfoController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetDataEquipmentInfoWrap_hotfix;
    private LuaFunction m_Refresh_hotfix;
    private LuaFunction m_ClosePropDisplay_hotfix;
    private LuaFunction m_SetEquipmentPropItemPropertyModifyTypeInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetData(
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentInfoWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePropDisplay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentPropItem(PropertyModifyType type, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public ArchiveItemDetailInfoController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ArchiveItemDetailInfoController m_owner;

      public LuaExportHelper(ArchiveItemDetailInfoController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public Image m_itemIamge
      {
        get
        {
          return this.m_owner.m_itemIamge;
        }
        set
        {
          this.m_owner.m_itemIamge = value;
        }
      }

      public CommonUIStateController m_equipLimitGroupAnimation
      {
        get
        {
          return this.m_owner.m_equipLimitGroupAnimation;
        }
        set
        {
          this.m_owner.m_equipLimitGroupAnimation = value;
        }
      }

      public GameObject m_equipLimitContent
      {
        get
        {
          return this.m_owner.m_equipLimitContent;
        }
        set
        {
          this.m_owner.m_equipLimitContent = value;
        }
      }

      public Text m_descEquipUnlimitText
      {
        get
        {
          return this.m_owner.m_descEquipUnlimitText;
        }
        set
        {
          this.m_owner.m_descEquipUnlimitText = value;
        }
      }

      public Text m_equipmentExplain
      {
        get
        {
          return this.m_owner.m_equipmentExplain;
        }
        set
        {
          this.m_owner.m_equipmentExplain = value;
        }
      }

      public GameObject m_HPGameObject
      {
        get
        {
          return this.m_owner.m_HPGameObject;
        }
        set
        {
          this.m_owner.m_HPGameObject = value;
        }
      }

      public Text m_HPText
      {
        get
        {
          return this.m_owner.m_HPText;
        }
        set
        {
          this.m_owner.m_HPText = value;
        }
      }

      public GameObject m_ATGameObject
      {
        get
        {
          return this.m_owner.m_ATGameObject;
        }
        set
        {
          this.m_owner.m_ATGameObject = value;
        }
      }

      public Text m_ATText
      {
        get
        {
          return this.m_owner.m_ATText;
        }
        set
        {
          this.m_owner.m_ATText = value;
        }
      }

      public GameObject m_DFGameObject
      {
        get
        {
          return this.m_owner.m_DFGameObject;
        }
        set
        {
          this.m_owner.m_DFGameObject = value;
        }
      }

      public Text m_DFText
      {
        get
        {
          return this.m_owner.m_DFText;
        }
        set
        {
          this.m_owner.m_DFText = value;
        }
      }

      public GameObject m_MagicDFGameObject
      {
        get
        {
          return this.m_owner.m_MagicDFGameObject;
        }
        set
        {
          this.m_owner.m_MagicDFGameObject = value;
        }
      }

      public Text m_MagicDFText
      {
        get
        {
          return this.m_owner.m_MagicDFText;
        }
        set
        {
          this.m_owner.m_MagicDFText = value;
        }
      }

      public GameObject m_MagicGameObject
      {
        get
        {
          return this.m_owner.m_MagicGameObject;
        }
        set
        {
          this.m_owner.m_MagicGameObject = value;
        }
      }

      public Text m_MagicText
      {
        get
        {
          return this.m_owner.m_MagicText;
        }
        set
        {
          this.m_owner.m_MagicText = value;
        }
      }

      public GameObject m_DexGameObject
      {
        get
        {
          return this.m_owner.m_DexGameObject;
        }
        set
        {
          this.m_owner.m_DexGameObject = value;
        }
      }

      public Text m_DexText
      {
        get
        {
          return this.m_owner.m_DexText;
        }
        set
        {
          this.m_owner.m_DexText = value;
        }
      }

      public CommonUIStateController m_skillAnimation
      {
        get
        {
          return this.m_owner.m_skillAnimation;
        }
        set
        {
          this.m_owner.m_skillAnimation = value;
        }
      }

      public CommonUIStateController m_skillContentAnimation
      {
        get
        {
          return this.m_owner.m_skillContentAnimation;
        }
        set
        {
          this.m_owner.m_skillContentAnimation = value;
        }
      }

      public Text m_skillBelongText
      {
        get
        {
          return this.m_owner.m_skillBelongText;
        }
        set
        {
          this.m_owner.m_skillBelongText = value;
        }
      }

      public GameObject m_skillBelongBGText
      {
        get
        {
          return this.m_owner.m_skillBelongBGText;
        }
        set
        {
          this.m_owner.m_skillBelongBGText = value;
        }
      }

      public Text m_skillUnlockConditionText
      {
        get
        {
          return this.m_owner.m_skillUnlockConditionText;
        }
        set
        {
          this.m_owner.m_skillUnlockConditionText = value;
        }
      }

      public Text m_skillDescText
      {
        get
        {
          return this.m_owner.m_skillDescText;
        }
        set
        {
          this.m_owner.m_skillDescText = value;
        }
      }

      public Text m_skillNameText
      {
        get
        {
          return this.m_owner.m_skillNameText;
        }
        set
        {
          this.m_owner.m_skillNameText = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public EquipmentArchiveUIController.EquipmentInfoWrap m_equipmentInfoWrap
      {
        get
        {
          return this.m_owner.m_equipmentInfoWrap;
        }
        set
        {
          this.m_owner.m_equipmentInfoWrap = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ClosePropDisplay()
      {
        this.m_owner.ClosePropDisplay();
      }

      public void SetEquipmentPropItem(PropertyModifyType type, int value)
      {
        this.m_owner.SetEquipmentPropItem(type, value);
      }
    }
  }
}
