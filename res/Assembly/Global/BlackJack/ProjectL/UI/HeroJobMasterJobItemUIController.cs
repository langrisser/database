﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroJobMasterJobItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroJobMasterJobItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./JobIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_icon;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Property", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyContent;
    [AutoBind("./Property/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyHP;
    [AutoBind("./Property/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propertyHPValueText;
    [AutoBind("./Property/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyAT;
    [AutoBind("./Property/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propertyATValueText;
    [AutoBind("./Property/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyDF;
    [AutoBind("./Property/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propertyDFValueText;
    [AutoBind("./Property/DEX", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyDEX;
    [AutoBind("./Property/DEX/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propertyDEXValueText;
    [AutoBind("./Property/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyMagicDF;
    [AutoBind("./Property/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propertyMagicDFValueText;
    [AutoBind("./Property/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyMagic;
    [AutoBind("./Property/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propertyMagicValueText;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitJobMasterItem(ConfigDataJobInfo jobInfo, bool isMaster)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMasterRewardProperty(PropertyModifyType type, int value)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
