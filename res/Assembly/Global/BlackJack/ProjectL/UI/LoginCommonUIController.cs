﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LoginCommonUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class LoginCommonUIController : UIControllerBase
  {
    [AutoBind("./Fade", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_fadeImage;
    [AutoBind("./DisableInput", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_disableInputGameObject;
    private ScreenFade m_screenFade;
    private TouchFx m_touchFx;

    private LoginCommonUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~LoginCommonUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTouchFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DisposeTouchFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableInput(bool enable)
    {
    }

    public void FadeIn(float time, Color color, Action onEnd = null)
    {
      this.m_screenFade.FadeIn(time, color, onEnd);
    }

    public void FadeOut(float time, Color color, Action onEnd = null)
    {
      this.m_screenFade.FadeOut(time, color, onEnd);
    }
  }
}
