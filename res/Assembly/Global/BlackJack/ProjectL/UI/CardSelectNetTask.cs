﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CardSelectNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class CardSelectNetTask : UINetTask
  {
    private int m_cardPoolId;
    private bool m_isSingleSlect;
    private bool m_isUsingTicket;

    [MethodImpl((MethodImplOptions) 32768)]
    public CardSelectNetTask(int cardPoolId, bool isSingleSlect, bool isUsingTicket = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCardSelectAck(int result, List<Goods> rewards)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<Goods> Rewards { private set; get; }
  }
}
