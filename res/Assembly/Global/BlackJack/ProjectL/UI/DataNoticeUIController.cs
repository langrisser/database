﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DataNoticeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class DataNoticeUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Detail/Scroll View/Viewport/Content/LinkTextButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_privacyWebLinkButton;
    [AutoBind("./Detail/DisagreeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_disagreeButton;
    [AutoBind("./Detail/AgreeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_agreeButton;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private DataNoticeUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_Finalize_hotfix;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnCloseButtonClicked_hotfix;
    private LuaFunction m_OnPrivacyWebLinkButtonClicked_hotfix;
    private LuaFunction m_OnDisagreeButtonClicked_hotfix;
    private LuaFunction m_OnAgreeButtonClicked_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_Close_hotfix;
    private LuaFunction m_OnUIClosed_hotfix;
    private LuaFunction m_add_EventOnClosedAction_hotfix;
    private LuaFunction m_remove_EventOnClosedAction_hotfix;
    private LuaFunction m_add_EventOnPrivacyWebLinkButtonClickedAction_hotfix;
    private LuaFunction m_remove_EventOnPrivacyWebLinkButtonClickedAction_hotfix;
    private LuaFunction m_add_EventOnDisagreeButtonClickedAction_hotfix;
    private LuaFunction m_remove_EventOnDisagreeButtonClickedAction_hotfix;
    private LuaFunction m_add_EventOnAgreeButtonClickedAction_hotfix;
    private LuaFunction m_remove_EventOnAgreeButtonClickedAction_hotfix;

    private DataNoticeUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~DataNoticeUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPrivacyWebLinkButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisagreeButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAgreeButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIClosed()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClosed
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPrivacyWebLinkButtonClicked
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnDisagreeButtonClicked
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAgreeButtonClicked
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public DataNoticeUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClosed()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClosed()
    {
      this.EventOnClosed = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPrivacyWebLinkButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPrivacyWebLinkButtonClicked()
    {
      this.EventOnPrivacyWebLinkButtonClicked = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnDisagreeButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnDisagreeButtonClicked()
    {
      this.EventOnDisagreeButtonClicked = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAgreeButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAgreeButtonClicked()
    {
      this.EventOnAgreeButtonClicked = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private DataNoticeUIController m_owner;

      public LuaExportHelper(DataNoticeUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClosed()
      {
        this.m_owner.__callDele_EventOnClosed();
      }

      public void __clearDele_EventOnClosed()
      {
        this.m_owner.__clearDele_EventOnClosed();
      }

      public void __callDele_EventOnPrivacyWebLinkButtonClicked()
      {
        this.m_owner.__callDele_EventOnPrivacyWebLinkButtonClicked();
      }

      public void __clearDele_EventOnPrivacyWebLinkButtonClicked()
      {
        this.m_owner.__clearDele_EventOnPrivacyWebLinkButtonClicked();
      }

      public void __callDele_EventOnDisagreeButtonClicked()
      {
        this.m_owner.__callDele_EventOnDisagreeButtonClicked();
      }

      public void __clearDele_EventOnDisagreeButtonClicked()
      {
        this.m_owner.__clearDele_EventOnDisagreeButtonClicked();
      }

      public void __callDele_EventOnAgreeButtonClicked()
      {
        this.m_owner.__callDele_EventOnAgreeButtonClicked();
      }

      public void __clearDele_EventOnAgreeButtonClicked()
      {
        this.m_owner.__clearDele_EventOnAgreeButtonClicked();
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_backgroundButton
      {
        get
        {
          return this.m_owner.m_backgroundButton;
        }
        set
        {
          this.m_owner.m_backgroundButton = value;
        }
      }

      public ButtonEx m_privacyWebLinkButton
      {
        get
        {
          return this.m_owner.m_privacyWebLinkButton;
        }
        set
        {
          this.m_owner.m_privacyWebLinkButton = value;
        }
      }

      public Button m_disagreeButton
      {
        get
        {
          return this.m_owner.m_disagreeButton;
        }
        set
        {
          this.m_owner.m_disagreeButton = value;
        }
      }

      public Button m_agreeButton
      {
        get
        {
          return this.m_owner.m_agreeButton;
        }
        set
        {
          this.m_owner.m_agreeButton = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnCloseButtonClicked()
      {
        this.m_owner.OnCloseButtonClicked();
      }

      public void OnPrivacyWebLinkButtonClicked()
      {
        this.m_owner.OnPrivacyWebLinkButtonClicked();
      }

      public void OnDisagreeButtonClicked()
      {
        this.m_owner.OnDisagreeButtonClicked();
      }

      public void OnAgreeButtonClicked()
      {
        this.m_owner.OnAgreeButtonClicked();
      }

      public void OnUIClosed()
      {
        this.m_owner.OnUIClosed();
      }
    }
  }
}
