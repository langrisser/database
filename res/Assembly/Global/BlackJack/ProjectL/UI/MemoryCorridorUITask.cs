﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MemoryCorridorUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class MemoryCorridorUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private MemoryCorridorUIController m_memoryCorridorUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private ConfigDataMemoryCorridorInfo m_memoryCorridorInfo;
    [DoNotToLua]
    private MemoryCorridorUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_Finalize_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_InitDataFromUIIntentUIIntent_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_InitMemoryCorridorUIController_hotfix;
    private LuaFunction m_UninitMemoryCorridorUIController_hotfix;
    private LuaFunction m_OnMemoryWarning_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_MemoryCorridorUIController_OnReturn_hotfix;
    private LuaFunction m_MemoryCorridorUIController_OnShowHelp_hotfix;
    private LuaFunction m_MemoryCorridorUIController_OnAddTicket_hotfix;
    private LuaFunction m_MemoryCorridorUIController_OnShowTeam_hotfix;
    private LuaFunction m_TeamUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_MemoryCorridorUIController_OnStartMemoryCorridorLevelConfigDataMemoryCorridorLevelInfo_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public MemoryCorridorUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~MemoryCorridorUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitMemoryCorridorUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitMemoryCorridorUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MemoryCorridorUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MemoryCorridorUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MemoryCorridorUIController_OnAddTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MemoryCorridorUIController_OnShowTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MemoryCorridorUIController_OnStartMemoryCorridorLevel(
      ConfigDataMemoryCorridorLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public MemoryCorridorUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      base.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private MemoryCorridorUITask m_owner;

      public LuaExportHelper(MemoryCorridorUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public MemoryCorridorUIController m_memoryCorridorUIController
      {
        get
        {
          return this.m_owner.m_memoryCorridorUIController;
        }
        set
        {
          this.m_owner.m_memoryCorridorUIController = value;
        }
      }

      public PlayerResourceUIController m_playerResourceUIController
      {
        get
        {
          return this.m_owner.m_playerResourceUIController;
        }
        set
        {
          this.m_owner.m_playerResourceUIController = value;
        }
      }

      public ConfigDataMemoryCorridorInfo m_memoryCorridorInfo
      {
        get
        {
          return this.m_owner.m_memoryCorridorInfo;
        }
        set
        {
          this.m_owner.m_memoryCorridorInfo = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void InitDataFromUIIntent(UIIntent intent)
      {
        this.m_owner.InitDataFromUIIntent(intent);
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void InitMemoryCorridorUIController()
      {
        this.m_owner.InitMemoryCorridorUIController();
      }

      public void UninitMemoryCorridorUIController()
      {
        this.m_owner.UninitMemoryCorridorUIController();
      }

      public void OnMemoryWarning()
      {
        this.m_owner.OnMemoryWarning();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void MemoryCorridorUIController_OnReturn()
      {
        this.m_owner.MemoryCorridorUIController_OnReturn();
      }

      public void MemoryCorridorUIController_OnShowHelp()
      {
        this.m_owner.MemoryCorridorUIController_OnShowHelp();
      }

      public void MemoryCorridorUIController_OnAddTicket()
      {
        this.m_owner.MemoryCorridorUIController_OnAddTicket();
      }

      public void MemoryCorridorUIController_OnShowTeam()
      {
        this.m_owner.MemoryCorridorUIController_OnShowTeam();
      }

      public void TeamUITask_OnLoadAllResCompleted()
      {
        this.m_owner.TeamUITask_OnLoadAllResCompleted();
      }

      public void MemoryCorridorUIController_OnStartMemoryCorridorLevel(
        ConfigDataMemoryCorridorLevelInfo levelInfo)
      {
        this.m_owner.MemoryCorridorUIController_OnStartMemoryCorridorLevel(levelInfo);
      }
    }
  }
}
