﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class StoreItemUIController : UIControllerBase
  {
    private PDSDKGoodType m_goodPDSDKType;
    private const string PriceState_OtherItem = "OtherItem";
    private const string PriceState_Crystal = "Crystal";
    private const string PriceState_RMBIcon = "RMB";
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_storeItemButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_itemUIStateController;
    [AutoBind("./Item/GeneralGoods", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_generalGoodObj;
    [AutoBind("./Item/GeneralGoods/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_generalGoodIconImage;
    [AutoBind("./Item/GeneralGoods/FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_generalGoodFrameImage;
    [AutoBind("./Item/GeneralGoods/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_ssrEffectImage;
    [AutoBind("./Item/GeneralGoods/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_generalGoodCountText;
    [AutoBind("./Item/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_spineAnimObj;
    [AutoBind("./Item/Item", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noFrameItemObj;
    [AutoBind("./Item/Item", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_noFrameItemIconImage;
    [AutoBind("./Item/Crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rechargeItemObj;
    [AutoBind("./Item/Crystal", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rechargeItemIconImage;
    [AutoBind("./LimitPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_limitObj;
    [AutoBind("./LimitPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_limitText;
    [AutoBind("./DisablePanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_disableBuyObj;
    [AutoBind("./DisablePanel/SoldOut&RefreshTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_disableBuyRefreshObj;
    [AutoBind("./DisablePanel/SoldOut&RefreshTime/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_refreshOrRemoveTimeText;
    [AutoBind("./DisablePanel/OnlySoldOut", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_disableBuyOnlyObj;
    [AutoBind("./ExtraPresentPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_extraPresentGameObj;
    [AutoBind("./ExtraPresentPanel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_extraPresentText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemNameText;
    [AutoBind("./PresentedPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_presentedPanelObj;
    [AutoBind("./PresentedPanel/RemoveTimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_presentedText;
    [AutoBind("./PricePanel/Item", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_priceIconImage;
    [AutoBind("./PricePanel/CrystalIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_priceCrystalImage;
    [AutoBind("./PricePanel/RMBIcon ", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_priceRMBImage;
    [AutoBind("./PricePanel/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_priceText;
    [AutoBind("./PricePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_pricePanelStateCtrl;
    [AutoBind("./LabelPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_labelObj;
    [AutoBind("./LabelPanel/CommendLabel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_commendLabelObj;
    [AutoBind("./LabelPanel/DiscountLabel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_discountLabelObj;
    [AutoBind("./LabelPanel/FirstDiscountLabel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_firstDiscountLabelObj;
    [AutoBind("./LabelPanel/LimitTimeLabel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_limitTimeLabelObj;
    [AutoBind("./LabelPanel/FirstLabel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_firstLabelObj;
    [AutoBind("./BestValue", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_isBestValueObj;
    [AutoBind("./Tape", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_LeftDayGameobj;
    [AutoBind("./Tape/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_LeftDayText;
    [AutoBind("./Tape/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_LeftTitleGameobj;
    private string m_countString;
    private bool m_isSSR;
    private DateTime m_giftStoreItemEndTime;
    private int m_nowSeconds;
    public StoreType m_storeType;
    public GoodsType m_goodsType;
    public int m_fixedStoreItemGoodsID;
    public int m_goodsID;
    public int m_index;
    public bool m_haveFirstBuyReward;
    public GoodsType m_extraCurrencyType;
    public GoodsType m_currentGoodCurrencyType;
    public string m_extraCurrencyTypeIconString;
    public int m_extraCurrencyCount;
    public GiftStoreItem m_giftStoreItem;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private ClientConfigDataLoader m_clientConfigDataLoader;
    [DoNotToLua]
    private StoreItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnStoreItemClick_hotfix;
    private LuaFunction m_SetFixedStoreItemInfoFixedStoreItem_hotfix;
    private LuaFunction m_SetRandomStoreItemInfoRandomStoreItemInt32_hotfix;
    private LuaFunction m_SetRechargeStoreItemInfoInt32StoreTypeBooleanDoubleStringInt32StringBoolean_hotfix;
    private LuaFunction m_SetGiftStoreItemInfoGiftStoreItemDoublePDSDKGoodType_hotfix;
    private LuaFunction m_SetGiftStoreMothCardItemInfoGiftStoreItemDoublePDSDKGoodType_hotfix;
    private LuaFunction m_SetLabelConfigDataFixedStoreItemInfo_hotfix;
    private LuaFunction m_SetLabel_RMB_RechargeBoolean_hotfix;
    private LuaFunction m_SetLabel_hotfix;
    private LuaFunction m_SetPriceFixedStoreItem_hotfix;
    private LuaFunction m_SetPriceConfigDataRandomStoreItemInfo_hotfix;
    private LuaFunction m_SetPrice_RMBDouble_hotfix;
    private LuaFunction m_SetLimitFixedStoreItemConfigDataFixedStoreItemInfo_hotfix;
    private LuaFunction m_SetLimitGiftStoreItemConfigDataGiftStoreItemInfo_hotfix;
    private LuaFunction m_HaveFirstBuyRewardConfigDataFixedStoreItemInfo_hotfix;
    private LuaFunction m_SetLimit_hotfix;
    private LuaFunction m_SetLimit_RMBBooleanInt32_hotfix;
    private LuaFunction m_SetSellOutFixedStoreItem_hotfix;
    private LuaFunction m_SetSellOutRandomStoreItem_hotfix;
    private LuaFunction m_SetSellOutGiftStoreItem_hotfix;
    private LuaFunction m_SetSellOut_hotfix;
    private LuaFunction m_UpdateRemoveTimeDateTime_hotfix;
    private LuaFunction m_UpdateIconGoodsTypeInt32Int32_hotfix;
    private LuaFunction m_UpdateIcon_RMBString_hotfix;
    private LuaFunction m_SetNoFrameInfoGoodsTypeInt32_hotfix;
    private LuaFunction m_SetGeneralGoodInfoInt32_hotfix;
    private LuaFunction m_GetFrameNameAndSetSSR_hotfix;
    private LuaFunction m_SetRandomStoreItemNotShow_hotfix;
    private LuaFunction m_SetLeftDaysString_hotfix;
    private LuaFunction m_SetItemDefaultState_hotfix;
    private LuaFunction m_SetIsBestValueBoolean_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_UpdateRemoveTime_hotfix;
    private LuaFunction m_get_Count_hotfix;
    private LuaFunction m_set_CountString_hotfix;
    private LuaFunction m_get_PriceIcon_hotfix;
    private LuaFunction m_set_PriceIconImage_hotfix;
    private LuaFunction m_get_PriceText_hotfix;
    private LuaFunction m_set_PriceTextText_hotfix;
    private LuaFunction m_get_ItemNameText_hotfix;
    private LuaFunction m_set_ItemNameTextText_hotfix;
    private LuaFunction m_get_LimitText_hotfix;
    private LuaFunction m_set_LimitTextText_hotfix;
    private LuaFunction m_get_GoodPDSDKType_hotfix;
    private LuaFunction m_add_EventOnClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStoreItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFixedStoreItemInfo(FixedStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomStoreItemInfo(RandomStoreItem item, int itemIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRechargeStoreItemInfo(
      int goodsId,
      StoreType storeType,
      bool isGoodsBought,
      double goodsPrice,
      string goodsName,
      int cystalRewardNums,
      string icon,
      bool isBestValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGiftStoreItemInfo(GiftStoreItem item, double price, PDSDKGoodType goodPDSDKType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGiftStoreMothCardItemInfo(
      GiftStoreItem item,
      double price,
      PDSDKGoodType goodPDSDKType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLabel(ConfigDataFixedStoreItemInfo itemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLabel_RMB_Recharge(bool isGoodsBought)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLabel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPrice(FixedStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPrice(ConfigDataRandomStoreItemInfo itemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPrice_RMB(double goodPrice)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLimit(FixedStoreItem item, ConfigDataFixedStoreItemInfo fixedStoreItemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLimit(GiftStoreItem item, ConfigDataGiftStoreItemInfo giftStoreItemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HaveFirstBuyReward(ConfigDataFixedStoreItemInfo fixedStoreItemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLimit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLimit_RMB(bool isGoodsBought, int crystalNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSellOut(FixedStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSellOut(RandomStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSellOut(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSellOut()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRemoveTime(DateTime showEndTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateIcon(GoodsType goodType, int itemId, int num)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateIcon_RMB(string icon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetNoFrameInfo(GoodsType goodType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGeneralGoodInfo(int count = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetFrameNameAndSetSSR()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRandomStoreItemNotShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLeftDays(string remainTime = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetItemDefaultState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetIsBestValue(bool isBestValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRemoveTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public string Count
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Image PriceIcon
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Text PriceText
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Text ItemNameText
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Text LimitText
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public PDSDKGoodType GoodPDSDKType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public StoreItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClick(StoreItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClick(StoreItemUIController obj)
    {
      this.EventOnClick = (Action<StoreItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private StoreItemUIController m_owner;

      public LuaExportHelper(StoreItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClick(StoreItemUIController obj)
      {
        this.m_owner.__callDele_EventOnClick(obj);
      }

      public void __clearDele_EventOnClick(StoreItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnClick(obj);
      }

      public PDSDKGoodType m_goodPDSDKType
      {
        get
        {
          return this.m_owner.m_goodPDSDKType;
        }
        set
        {
          this.m_owner.m_goodPDSDKType = value;
        }
      }

      public static string PriceState_OtherItem
      {
        get
        {
          return "OtherItem";
        }
      }

      public static string PriceState_Crystal
      {
        get
        {
          return "Crystal";
        }
      }

      public static string PriceState_RMBIcon
      {
        get
        {
          return "RMB";
        }
      }

      public Button m_storeItemButton
      {
        get
        {
          return this.m_owner.m_storeItemButton;
        }
        set
        {
          this.m_owner.m_storeItemButton = value;
        }
      }

      public CommonUIStateController m_itemUIStateController
      {
        get
        {
          return this.m_owner.m_itemUIStateController;
        }
        set
        {
          this.m_owner.m_itemUIStateController = value;
        }
      }

      public GameObject m_generalGoodObj
      {
        get
        {
          return this.m_owner.m_generalGoodObj;
        }
        set
        {
          this.m_owner.m_generalGoodObj = value;
        }
      }

      public Image m_generalGoodIconImage
      {
        get
        {
          return this.m_owner.m_generalGoodIconImage;
        }
        set
        {
          this.m_owner.m_generalGoodIconImage = value;
        }
      }

      public Image m_generalGoodFrameImage
      {
        get
        {
          return this.m_owner.m_generalGoodFrameImage;
        }
        set
        {
          this.m_owner.m_generalGoodFrameImage = value;
        }
      }

      public Image m_ssrEffectImage
      {
        get
        {
          return this.m_owner.m_ssrEffectImage;
        }
        set
        {
          this.m_owner.m_ssrEffectImage = value;
        }
      }

      public Text m_generalGoodCountText
      {
        get
        {
          return this.m_owner.m_generalGoodCountText;
        }
        set
        {
          this.m_owner.m_generalGoodCountText = value;
        }
      }

      public GameObject m_spineAnimObj
      {
        get
        {
          return this.m_owner.m_spineAnimObj;
        }
        set
        {
          this.m_owner.m_spineAnimObj = value;
        }
      }

      public GameObject m_noFrameItemObj
      {
        get
        {
          return this.m_owner.m_noFrameItemObj;
        }
        set
        {
          this.m_owner.m_noFrameItemObj = value;
        }
      }

      public Image m_noFrameItemIconImage
      {
        get
        {
          return this.m_owner.m_noFrameItemIconImage;
        }
        set
        {
          this.m_owner.m_noFrameItemIconImage = value;
        }
      }

      public GameObject m_rechargeItemObj
      {
        get
        {
          return this.m_owner.m_rechargeItemObj;
        }
        set
        {
          this.m_owner.m_rechargeItemObj = value;
        }
      }

      public Image m_rechargeItemIconImage
      {
        get
        {
          return this.m_owner.m_rechargeItemIconImage;
        }
        set
        {
          this.m_owner.m_rechargeItemIconImage = value;
        }
      }

      public GameObject m_limitObj
      {
        get
        {
          return this.m_owner.m_limitObj;
        }
        set
        {
          this.m_owner.m_limitObj = value;
        }
      }

      public Text m_limitText
      {
        get
        {
          return this.m_owner.m_limitText;
        }
        set
        {
          this.m_owner.m_limitText = value;
        }
      }

      public GameObject m_disableBuyObj
      {
        get
        {
          return this.m_owner.m_disableBuyObj;
        }
        set
        {
          this.m_owner.m_disableBuyObj = value;
        }
      }

      public GameObject m_disableBuyRefreshObj
      {
        get
        {
          return this.m_owner.m_disableBuyRefreshObj;
        }
        set
        {
          this.m_owner.m_disableBuyRefreshObj = value;
        }
      }

      public Text m_refreshOrRemoveTimeText
      {
        get
        {
          return this.m_owner.m_refreshOrRemoveTimeText;
        }
        set
        {
          this.m_owner.m_refreshOrRemoveTimeText = value;
        }
      }

      public GameObject m_disableBuyOnlyObj
      {
        get
        {
          return this.m_owner.m_disableBuyOnlyObj;
        }
        set
        {
          this.m_owner.m_disableBuyOnlyObj = value;
        }
      }

      public GameObject m_extraPresentGameObj
      {
        get
        {
          return this.m_owner.m_extraPresentGameObj;
        }
        set
        {
          this.m_owner.m_extraPresentGameObj = value;
        }
      }

      public Text m_extraPresentText
      {
        get
        {
          return this.m_owner.m_extraPresentText;
        }
        set
        {
          this.m_owner.m_extraPresentText = value;
        }
      }

      public Text m_itemNameText
      {
        get
        {
          return this.m_owner.m_itemNameText;
        }
        set
        {
          this.m_owner.m_itemNameText = value;
        }
      }

      public GameObject m_presentedPanelObj
      {
        get
        {
          return this.m_owner.m_presentedPanelObj;
        }
        set
        {
          this.m_owner.m_presentedPanelObj = value;
        }
      }

      public Text m_presentedText
      {
        get
        {
          return this.m_owner.m_presentedText;
        }
        set
        {
          this.m_owner.m_presentedText = value;
        }
      }

      public Image m_priceIconImage
      {
        get
        {
          return this.m_owner.m_priceIconImage;
        }
        set
        {
          this.m_owner.m_priceIconImage = value;
        }
      }

      public Image m_priceCrystalImage
      {
        get
        {
          return this.m_owner.m_priceCrystalImage;
        }
        set
        {
          this.m_owner.m_priceCrystalImage = value;
        }
      }

      public Image m_priceRMBImage
      {
        get
        {
          return this.m_owner.m_priceRMBImage;
        }
        set
        {
          this.m_owner.m_priceRMBImage = value;
        }
      }

      public Text m_priceText
      {
        get
        {
          return this.m_owner.m_priceText;
        }
        set
        {
          this.m_owner.m_priceText = value;
        }
      }

      public CommonUIStateController m_pricePanelStateCtrl
      {
        get
        {
          return this.m_owner.m_pricePanelStateCtrl;
        }
        set
        {
          this.m_owner.m_pricePanelStateCtrl = value;
        }
      }

      public GameObject m_labelObj
      {
        get
        {
          return this.m_owner.m_labelObj;
        }
        set
        {
          this.m_owner.m_labelObj = value;
        }
      }

      public GameObject m_commendLabelObj
      {
        get
        {
          return this.m_owner.m_commendLabelObj;
        }
        set
        {
          this.m_owner.m_commendLabelObj = value;
        }
      }

      public GameObject m_discountLabelObj
      {
        get
        {
          return this.m_owner.m_discountLabelObj;
        }
        set
        {
          this.m_owner.m_discountLabelObj = value;
        }
      }

      public GameObject m_firstDiscountLabelObj
      {
        get
        {
          return this.m_owner.m_firstDiscountLabelObj;
        }
        set
        {
          this.m_owner.m_firstDiscountLabelObj = value;
        }
      }

      public GameObject m_limitTimeLabelObj
      {
        get
        {
          return this.m_owner.m_limitTimeLabelObj;
        }
        set
        {
          this.m_owner.m_limitTimeLabelObj = value;
        }
      }

      public GameObject m_firstLabelObj
      {
        get
        {
          return this.m_owner.m_firstLabelObj;
        }
        set
        {
          this.m_owner.m_firstLabelObj = value;
        }
      }

      public GameObject m_isBestValueObj
      {
        get
        {
          return this.m_owner.m_isBestValueObj;
        }
        set
        {
          this.m_owner.m_isBestValueObj = value;
        }
      }

      public GameObject m_LeftDayGameobj
      {
        get
        {
          return this.m_owner.m_LeftDayGameobj;
        }
        set
        {
          this.m_owner.m_LeftDayGameobj = value;
        }
      }

      public Text m_LeftDayText
      {
        get
        {
          return this.m_owner.m_LeftDayText;
        }
        set
        {
          this.m_owner.m_LeftDayText = value;
        }
      }

      public GameObject m_LeftTitleGameobj
      {
        get
        {
          return this.m_owner.m_LeftTitleGameobj;
        }
        set
        {
          this.m_owner.m_LeftTitleGameobj = value;
        }
      }

      public string m_countString
      {
        get
        {
          return this.m_owner.m_countString;
        }
        set
        {
          this.m_owner.m_countString = value;
        }
      }

      public bool m_isSSR
      {
        get
        {
          return this.m_owner.m_isSSR;
        }
        set
        {
          this.m_owner.m_isSSR = value;
        }
      }

      public DateTime m_giftStoreItemEndTime
      {
        get
        {
          return this.m_owner.m_giftStoreItemEndTime;
        }
        set
        {
          this.m_owner.m_giftStoreItemEndTime = value;
        }
      }

      public int m_nowSeconds
      {
        get
        {
          return this.m_owner.m_nowSeconds;
        }
        set
        {
          this.m_owner.m_nowSeconds = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ClientConfigDataLoader m_clientConfigDataLoader
      {
        get
        {
          return this.m_owner.m_clientConfigDataLoader;
        }
        set
        {
          this.m_owner.m_clientConfigDataLoader = value;
        }
      }

      public string Count
      {
        set
        {
          this.m_owner.Count = value;
        }
      }

      public Image PriceIcon
      {
        set
        {
          this.m_owner.PriceIcon = value;
        }
      }

      public Text PriceText
      {
        set
        {
          this.m_owner.PriceText = value;
        }
      }

      public Text ItemNameText
      {
        set
        {
          this.m_owner.ItemNameText = value;
        }
      }

      public Text LimitText
      {
        set
        {
          this.m_owner.LimitText = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnStoreItemClick()
      {
        this.m_owner.OnStoreItemClick();
      }

      public void SetLabel(ConfigDataFixedStoreItemInfo itemInfo)
      {
        this.m_owner.SetLabel(itemInfo);
      }

      public void SetLabel_RMB_Recharge(bool isGoodsBought)
      {
        this.m_owner.SetLabel_RMB_Recharge(isGoodsBought);
      }

      public void SetLabel()
      {
        this.m_owner.SetLabel();
      }

      public void SetPrice(FixedStoreItem item)
      {
        this.m_owner.SetPrice(item);
      }

      public void SetPrice(ConfigDataRandomStoreItemInfo itemInfo)
      {
        this.m_owner.SetPrice(itemInfo);
      }

      public void SetPrice_RMB(double goodPrice)
      {
        this.m_owner.SetPrice_RMB(goodPrice);
      }

      public void SetLimit(FixedStoreItem item, ConfigDataFixedStoreItemInfo fixedStoreItemInfo)
      {
        this.m_owner.SetLimit(item, fixedStoreItemInfo);
      }

      public void SetLimit(GiftStoreItem item, ConfigDataGiftStoreItemInfo giftStoreItemInfo)
      {
        this.m_owner.SetLimit(item, giftStoreItemInfo);
      }

      public bool HaveFirstBuyReward(ConfigDataFixedStoreItemInfo fixedStoreItemInfo)
      {
        return this.m_owner.HaveFirstBuyReward(fixedStoreItemInfo);
      }

      public void SetLimit()
      {
        this.m_owner.SetLimit();
      }

      public void SetLimit_RMB(bool isGoodsBought, int crystalNums)
      {
        this.m_owner.SetLimit_RMB(isGoodsBought, crystalNums);
      }

      public void SetSellOut(FixedStoreItem item)
      {
        this.m_owner.SetSellOut(item);
      }

      public void SetSellOut(RandomStoreItem item)
      {
        this.m_owner.SetSellOut(item);
      }

      public void SetSellOut(GiftStoreItem item)
      {
        this.m_owner.SetSellOut(item);
      }

      public void SetSellOut()
      {
        this.m_owner.SetSellOut();
      }

      public void UpdateRemoveTime(DateTime showEndTime)
      {
        this.m_owner.UpdateRemoveTime(showEndTime);
      }

      public void UpdateIcon(GoodsType goodType, int itemId, int num)
      {
        this.m_owner.UpdateIcon(goodType, itemId, num);
      }

      public void UpdateIcon_RMB(string icon)
      {
        this.m_owner.UpdateIcon_RMB(icon);
      }

      public void SetNoFrameInfo(GoodsType goodType, int goodsId)
      {
        this.m_owner.SetNoFrameInfo(goodType, goodsId);
      }

      public void SetGeneralGoodInfo(int count)
      {
        this.m_owner.SetGeneralGoodInfo(count);
      }

      public string GetFrameNameAndSetSSR()
      {
        return this.m_owner.GetFrameNameAndSetSSR();
      }

      public void SetRandomStoreItemNotShow()
      {
        this.m_owner.SetRandomStoreItemNotShow();
      }

      public void SetLeftDays(string remainTime)
      {
        this.m_owner.SetLeftDays(remainTime);
      }

      public void SetItemDefaultState()
      {
        this.m_owner.SetItemDefaultState();
      }

      public void SetIsBestValue(bool isBestValue)
      {
        this.m_owner.SetIsBestValue(isBestValue);
      }

      public void Update()
      {
        this.m_owner.Update();
      }

      public void UpdateRemoveTime()
      {
        this.m_owner.UpdateRemoveTime();
      }
    }
  }
}
