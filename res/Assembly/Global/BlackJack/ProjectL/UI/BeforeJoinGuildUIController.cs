﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BeforeJoinGuildUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BeforeJoinGuildUIController : UIControllerBase
  {
    [AutoBind("./MainPanel/InfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoPanel;
    [AutoBind("./MainPanel/RankingPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rankingPanel;
    [AutoBind("./MainPanel/ListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listPanel;
    [AutoBind("./MessagePanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messagePanel;
    [AutoBind("./GreatSociatyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_createGuildPanel;
    [AutoBind("./MainPanel/MessageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_messageButton;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./InfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoButton;
    [AutoBind("./MainPanel/MessageButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageRedPoint;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    public BeforeJoinGuildListUIController m_guildListUIController;
    public BeforeJoinGuildInfoUIController m_guildInfoUIController;
    public BeforeJoinGuildMsgUIController m_receiveMsgUIController;
    public CreateGuildUIController m_createGuildUIController;
    private GuildUITask m_guildUITask;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetInviteMsgList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshMessageRedPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessageClick()
    {
      this.m_receiveMsgUIController.Show();
    }

    private void OnInfoClick()
    {
      CommonUIController.Instance.ShowExplanation(ExplanationId.ExplanationId_Guild);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildUpdateInfoNtf(GuildLog log)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
