﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildFixedStoreUIComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class GuildFixedStoreUIComponent
  {
    private StoreId m_fixedStoreId;
    private GameObject m_itemPrefab;
    private GameObject m_content;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private CurrencyUIController m_currencyUIController;
    private GameObjectPool<StoreItemUIController> m_fixedStoreItemPool;
    [DoNotToLua]
    private GuildFixedStoreUIComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitGameObjectGameObjectCurrencyUIController_hotfix;
    private LuaFunction m_SetFixedStoreInfoStoreId_hotfix;
    private LuaFunction m_SetCurrencyStateList`1_hotfix;
    private LuaFunction m_ClearFiexdStoreItem_hotfix;
    private LuaFunction m_OnStoreItemClickStoreItemUIController_hotfix;
    private LuaFunction m_OnBuyItemSuccess_hotfix;
    private LuaFunction m_add_EventOnBuyItemSuccessAction_hotfix;
    private LuaFunction m_remove_EventOnBuyItemSuccessAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildFixedStoreUIComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(
      GameObject itemPrefab,
      GameObject itemContent,
      CurrencyUIController currencyUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFixedStoreInfo(StoreId fixedStoreID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurrencyState(List<GoodsType> currencyList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearFiexdStoreItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStoreItemClick(StoreItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyItemSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnBuyItemSuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public GuildFixedStoreUIComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBuyItemSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBuyItemSuccess()
    {
      this.EventOnBuyItemSuccess = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private GuildFixedStoreUIComponent m_owner;

      public LuaExportHelper(GuildFixedStoreUIComponent owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_EventOnBuyItemSuccess()
      {
        this.m_owner.__callDele_EventOnBuyItemSuccess();
      }

      public void __clearDele_EventOnBuyItemSuccess()
      {
        this.m_owner.__clearDele_EventOnBuyItemSuccess();
      }

      public StoreId m_fixedStoreId
      {
        get
        {
          return this.m_owner.m_fixedStoreId;
        }
        set
        {
          this.m_owner.m_fixedStoreId = value;
        }
      }

      public GameObject m_itemPrefab
      {
        get
        {
          return this.m_owner.m_itemPrefab;
        }
        set
        {
          this.m_owner.m_itemPrefab = value;
        }
      }

      public GameObject m_content
      {
        get
        {
          return this.m_owner.m_content;
        }
        set
        {
          this.m_owner.m_content = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public CurrencyUIController m_currencyUIController
      {
        get
        {
          return this.m_owner.m_currencyUIController;
        }
        set
        {
          this.m_owner.m_currencyUIController = value;
        }
      }

      public GameObjectPool<StoreItemUIController> m_fixedStoreItemPool
      {
        get
        {
          return this.m_owner.m_fixedStoreItemPool;
        }
        set
        {
          this.m_owner.m_fixedStoreItemPool = value;
        }
      }

      public void SetCurrencyState(List<GoodsType> currencyList)
      {
        this.m_owner.SetCurrencyState(currencyList);
      }

      public void OnBuyItemSuccess()
      {
        this.m_owner.OnBuyItemSuccess();
      }
    }
  }
}
