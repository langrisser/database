﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ClimbTowerLevelInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ClimbTowerLevelInfoUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Panel/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    [AutoBind("./Panel/Hard/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_hardText;
    [AutoBind("./Panel/StartButton/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_costEnergyText;
    [AutoBind("./Panel/WinCondition/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_winConditionGroupTransform;
    [AutoBind("./Panel/Rule/Scrollrect/Viewport/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_ruleConditionGroupTransform;
    [AutoBind("./Panel/RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_rewardGroupTransform;
    [AutoBind("./RecommendPanel/HeroScrollView/Viewport/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_recommendHeroGroupTransform;
    [AutoBind("./RecommendPanel/InfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_recommendHeroDescText;
    [AutoBind("./Panel/StartButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_conditionPrefab;
    [AutoBind("./Prefabs/RecommendHeroItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_recommendHeroItemPrefab;
    [DoNotToLua]
    private ClimbTowerLevelInfoUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OpenBoolean_hotfix;
    private LuaFunction m_CloseAction_hotfix;
    private LuaFunction m_SetTowerLevelConfigDataTowerFloorInfoConfigDataTowerLevelInfoConfigDataTowerBattleRuleInfoConfigDataTowerBonusHeroGroupInfo_hotfix;
    private LuaFunction m_SetRecommendHerosList`1_hotfix;
    private LuaFunction m_SetConditionsTransformStringInt32_hotfix;
    private LuaFunction m_AddConditionTransformStringInt32_hotfix;
    private LuaFunction m_OnBackgroundButtonClick_hotfix;
    private LuaFunction m_OnStartButtonClick_hotfix;
    private LuaFunction m_add_EventOnStartBattleAction_hotfix;
    private LuaFunction m_remove_EventOnStartBattleAction_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;

    private ClimbTowerLevelInfoUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(bool hasRecommendHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTowerLevel(
      ConfigDataTowerFloorInfo floorInfo,
      ConfigDataTowerLevelInfo levelInfo,
      ConfigDataTowerBattleRuleInfo ruleInfo,
      ConfigDataTowerBonusHeroGroupInfo bonusHeroGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRecommendHeros(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetConditions(Transform parent, string conditionStrs, int stateType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddCondition(Transform parent, string str, int stateType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnStartBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ClimbTowerLevelInfoUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStartBattle()
    {
      this.EventOnStartBattle = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ClimbTowerLevelInfoUIController m_owner;

      public LuaExportHelper(ClimbTowerLevelInfoUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnStartBattle()
      {
        this.m_owner.__callDele_EventOnStartBattle();
      }

      public void __clearDele_EventOnStartBattle()
      {
        this.m_owner.__clearDele_EventOnStartBattle();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public Image m_image
      {
        get
        {
          return this.m_owner.m_image;
        }
        set
        {
          this.m_owner.m_image = value;
        }
      }

      public Text m_hardText
      {
        get
        {
          return this.m_owner.m_hardText;
        }
        set
        {
          this.m_owner.m_hardText = value;
        }
      }

      public Text m_costEnergyText
      {
        get
        {
          return this.m_owner.m_costEnergyText;
        }
        set
        {
          this.m_owner.m_costEnergyText = value;
        }
      }

      public Transform m_winConditionGroupTransform
      {
        get
        {
          return this.m_owner.m_winConditionGroupTransform;
        }
        set
        {
          this.m_owner.m_winConditionGroupTransform = value;
        }
      }

      public Transform m_ruleConditionGroupTransform
      {
        get
        {
          return this.m_owner.m_ruleConditionGroupTransform;
        }
        set
        {
          this.m_owner.m_ruleConditionGroupTransform = value;
        }
      }

      public Transform m_rewardGroupTransform
      {
        get
        {
          return this.m_owner.m_rewardGroupTransform;
        }
        set
        {
          this.m_owner.m_rewardGroupTransform = value;
        }
      }

      public Transform m_recommendHeroGroupTransform
      {
        get
        {
          return this.m_owner.m_recommendHeroGroupTransform;
        }
        set
        {
          this.m_owner.m_recommendHeroGroupTransform = value;
        }
      }

      public Text m_recommendHeroDescText
      {
        get
        {
          return this.m_owner.m_recommendHeroDescText;
        }
        set
        {
          this.m_owner.m_recommendHeroDescText = value;
        }
      }

      public Button m_startButton
      {
        get
        {
          return this.m_owner.m_startButton;
        }
        set
        {
          this.m_owner.m_startButton = value;
        }
      }

      public Button m_backgroundButton
      {
        get
        {
          return this.m_owner.m_backgroundButton;
        }
        set
        {
          this.m_owner.m_backgroundButton = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_conditionPrefab
      {
        get
        {
          return this.m_owner.m_conditionPrefab;
        }
        set
        {
          this.m_owner.m_conditionPrefab = value;
        }
      }

      public GameObject m_recommendHeroItemPrefab
      {
        get
        {
          return this.m_owner.m_recommendHeroItemPrefab;
        }
        set
        {
          this.m_owner.m_recommendHeroItemPrefab = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetRecommendHeros(List<int> heroIds)
      {
        this.m_owner.SetRecommendHeros(heroIds);
      }

      public void SetConditions(Transform parent, string conditionStrs, int stateType)
      {
        this.m_owner.SetConditions(parent, conditionStrs, stateType);
      }

      public void AddCondition(Transform parent, string str, int stateType)
      {
        this.m_owner.AddCondition(parent, str, stateType);
      }

      public void OnBackgroundButtonClick()
      {
        this.m_owner.OnBackgroundButtonClick();
      }

      public void OnStartButtonClick()
      {
        this.m_owner.OnStartButtonClick();
      }
    }
  }
}
