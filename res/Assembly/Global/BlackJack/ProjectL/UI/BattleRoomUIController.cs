﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleRoomUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleRoomUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("/PlayerGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerGroupGameObject;
    [AutoBind("./ExpressionGroup/Empty", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chatEmptyGameObject;
    [AutoBind("./ExpressionGroup/ExpressionButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_expressionButton;
    [AutoBind("./ExpressionGroup/TalkButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button TalkButton;
    [AutoBind("./ExpressionGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_expressionGroup;
    [AutoBind("./ExpressionGroupDummy/BGMask", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx ButtonBGMask;
    [AutoBind("./TimeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject;
    [AutoBind("./TimeGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./BigNumberEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bigTimeUIStateController;
    [AutoBind("./BigNumberEffect/Number", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bigTimeText;
    [AutoBind("./ActionPlayer", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actionPlayerUIStateController;
    [AutoBind("./ActionPlayer/Image", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actionPlayerEnemyUIStateController;
    [AutoBind("./ActionPlayer/PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actionPlayerTagImage;
    [AutoBind("./ActionPlayer/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actionPlayerTimeText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/BattleTeamPlayer", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleTeamPlayerPrefab;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.NotInit, false)]
    private SmallExpressionParseDesc m_expressionParseDesc;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_expressionResContainer;
    private List<BattleTeamPlayerUIController> m_players;
    private BigExpressionController m_bigExpressionCtrl;
    [DoNotToLua]
    private BattleRoomUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_StartBattle_hotfix;
    private LuaFunction m_AddPlayersBattleRoom_hotfix;
    private LuaFunction m_AddPlayerBattleRoomPlayerInt32Boolean_hotfix;
    private LuaFunction m_ClearPlayers_hotfix;
    private LuaFunction m_SetPlayerStatusInt32PlayerBattleStatusBoolean_hotfix;
    private LuaFunction m_SetPlayerActionInt32Boolean_hotfix;
    private LuaFunction m_SetPlayerHeroCountInt32Int32_hotfix;
    private LuaFunction m_SetPlayerHeroAliveInt32Int32Boolean_hotfix;
    private LuaFunction m_ShowPlayerChatInt32String_hotfix;
    private LuaFunction m_ShowPlayerExpressionInt32Int32_hotfix;
    private LuaFunction m_ShowPlayerVoiceInt32ChatVoiceMessage_hotfix;
    private LuaFunction m_GetPlayerUIControllerInt32_hotfix;
    private LuaFunction m_SetOtherActionAsTeammateInt32_hotfix;
    private LuaFunction m_SetOtherActionAsEnemy_hotfix;
    private LuaFunction m_ShowOtherActionCountdown_hotfix;
    private LuaFunction m_HideOtherActionCountdown_hotfix;
    private LuaFunction m_SetOtherActionCountdownTimeSpan_hotfix;
    private LuaFunction m_ShowMyActionCountdown_hotfix;
    private LuaFunction m_HideMyActionCountdown_hotfix;
    private LuaFunction m_SetMyActionCountdownTimeSpan_hotfix;
    private LuaFunction m_SetCanUseChatBoolean_hotfix;
    private LuaFunction m_OnExpressionButtonClick_hotfix;
    private LuaFunction m_OnBigExpressionClickInt32_hotfix;
    private LuaFunction m_OnBigExpressionBGClick_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleRoomUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddPlayers(BattleRoom battleRoom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPlayer(BattleRoomPlayer player, int heroCount, bool showPlayerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearPlayers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerStatus(int playerIndex, PlayerBattleStatus status, bool isOffline)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerAction(int playerIndex, bool isAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerHeroCount(int playerIndex, int heroCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerHeroAlive(int playerIndex, int heroIndex, bool isAlive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerChat(int playerIndex, string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerExpression(int playerIndex, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerVoice(int playerIndex, ChatVoiceMessage voiceMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleTeamPlayerUIController GetPlayerUIController(
      int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOtherActionAsTeammate(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOtherActionAsEnemy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOtherActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideOtherActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOtherActionCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMyActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideMyActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMyActionCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanUseChat(bool canChat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBigExpressionClick(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBigExpressionBGClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleRoomUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleRoomUIController m_owner;

      public LuaExportHelper(BattleRoomUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public GameObject m_playerGroupGameObject
      {
        get
        {
          return this.m_owner.m_playerGroupGameObject;
        }
        set
        {
          this.m_owner.m_playerGroupGameObject = value;
        }
      }

      public GameObject m_chatEmptyGameObject
      {
        get
        {
          return this.m_owner.m_chatEmptyGameObject;
        }
        set
        {
          this.m_owner.m_chatEmptyGameObject = value;
        }
      }

      public ButtonEx m_expressionButton
      {
        get
        {
          return this.m_owner.m_expressionButton;
        }
        set
        {
          this.m_owner.m_expressionButton = value;
        }
      }

      public GameObject m_expressionGroup
      {
        get
        {
          return this.m_owner.m_expressionGroup;
        }
        set
        {
          this.m_owner.m_expressionGroup = value;
        }
      }

      public ButtonEx ButtonBGMask
      {
        get
        {
          return this.m_owner.ButtonBGMask;
        }
        set
        {
          this.m_owner.ButtonBGMask = value;
        }
      }

      public GameObject m_timeGameObject
      {
        get
        {
          return this.m_owner.m_timeGameObject;
        }
        set
        {
          this.m_owner.m_timeGameObject = value;
        }
      }

      public Text m_timeText
      {
        get
        {
          return this.m_owner.m_timeText;
        }
        set
        {
          this.m_owner.m_timeText = value;
        }
      }

      public CommonUIStateController m_bigTimeUIStateController
      {
        get
        {
          return this.m_owner.m_bigTimeUIStateController;
        }
        set
        {
          this.m_owner.m_bigTimeUIStateController = value;
        }
      }

      public Text m_bigTimeText
      {
        get
        {
          return this.m_owner.m_bigTimeText;
        }
        set
        {
          this.m_owner.m_bigTimeText = value;
        }
      }

      public CommonUIStateController m_actionPlayerUIStateController
      {
        get
        {
          return this.m_owner.m_actionPlayerUIStateController;
        }
        set
        {
          this.m_owner.m_actionPlayerUIStateController = value;
        }
      }

      public CommonUIStateController m_actionPlayerEnemyUIStateController
      {
        get
        {
          return this.m_owner.m_actionPlayerEnemyUIStateController;
        }
        set
        {
          this.m_owner.m_actionPlayerEnemyUIStateController = value;
        }
      }

      public Image m_actionPlayerTagImage
      {
        get
        {
          return this.m_owner.m_actionPlayerTagImage;
        }
        set
        {
          this.m_owner.m_actionPlayerTagImage = value;
        }
      }

      public Text m_actionPlayerTimeText
      {
        get
        {
          return this.m_owner.m_actionPlayerTimeText;
        }
        set
        {
          this.m_owner.m_actionPlayerTimeText = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_battleTeamPlayerPrefab
      {
        get
        {
          return this.m_owner.m_battleTeamPlayerPrefab;
        }
        set
        {
          this.m_owner.m_battleTeamPlayerPrefab = value;
        }
      }

      public SmallExpressionParseDesc m_expressionParseDesc
      {
        get
        {
          return this.m_owner.m_expressionParseDesc;
        }
        set
        {
          this.m_owner.m_expressionParseDesc = value;
        }
      }

      public PrefabResourceContainer m_expressionResContainer
      {
        get
        {
          return this.m_owner.m_expressionResContainer;
        }
        set
        {
          this.m_owner.m_expressionResContainer = value;
        }
      }

      public List<BattleTeamPlayerUIController> m_players
      {
        get
        {
          return this.m_owner.m_players;
        }
        set
        {
          this.m_owner.m_players = value;
        }
      }

      public BigExpressionController m_bigExpressionCtrl
      {
        get
        {
          return this.m_owner.m_bigExpressionCtrl;
        }
        set
        {
          this.m_owner.m_bigExpressionCtrl = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void AddPlayer(BattleRoomPlayer player, int heroCount, bool showPlayerIndex)
      {
        this.m_owner.AddPlayer(player, heroCount, showPlayerIndex);
      }

      public void ClearPlayers()
      {
        this.m_owner.ClearPlayers();
      }

      public BattleTeamPlayerUIController GetPlayerUIController(
        int playerIndex)
      {
        return this.m_owner.GetPlayerUIController(playerIndex);
      }

      public void OnExpressionButtonClick()
      {
        this.m_owner.OnExpressionButtonClick();
      }

      public void OnBigExpressionClick(int id)
      {
        this.m_owner.OnBigExpressionClick(id);
      }

      public void OnBigExpressionBGClick()
      {
        this.m_owner.OnBigExpressionBGClick();
      }
    }
  }
}
