﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildManagementUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class GuildManagementUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public const string ParamKey_JumpUI = "JumpUI";
    private GuildManagementUIController m_guildManagementUIController;
    private static string m_guildId;
    private PlayerSimpleInfoUITask m_playerSimpleInfoUITask;
    private float m_lastUpdateTime;
    private static Guild m_guild;
    [DoNotToLua]
    private GuildManagementUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_InitDataFromUIIntentUIIntent_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_PostUpdateView_hotfix;
    private LuaFunction m_GuildManagementUIController_OnClose_hotfix;
    private LuaFunction m_PlayerContext_EventOnGuildUpdateInfoGuildLog_hotfix;
    private LuaFunction m_GuildManagementUIController_OnQuitGuildAction_hotfix;
    private LuaFunction m_GuildManagementUIController_OnGetCanInvitePlayerListAction`1Boolean_hotfix;
    private LuaFunction m_GuildManagementUIController_OnGuildHiringDeclarationSetStringAction_hotfix;
    private LuaFunction m_GuildManagementUIController_OnGuildInfoSetBooleanInt32StringAction_hotfix;
    private LuaFunction m_GuildManagementUIController_OnGuildAnnouncementSetStringAction_hotfix;
    private LuaFunction m_GuildManagementUIController_OnGetGuildJoinApplyAction`1_hotfix;
    private LuaFunction m_GuildManagementUIController_OnGuildJoinConfirmOrRefuseStringBooleanAction_hotfix;
    private LuaFunction m_GuildManagementUIController_OnChangeGuildNameStringAction_hotfix;
    private LuaFunction m_GuildManagementUIController_OnGetGuildJournalAction`1_hotfix;
    private LuaFunction m_GuildManagementUIController_OnGuildInviteMemberStringAction_hotfix;
    private LuaFunction m_GuildManagementUIController_OnAllRefuseButtonClickAction_hotfix;
    private LuaFunction m_GuildManagementUIController_OnGuildMemberClickStringVector3PostionType_hotfix;
    private LuaFunction m_GuildManagementUIController_OnGotoGuildStore_hotfix;
    private LuaFunction m_GuildManagementUIController_OnGotoGuildGameListPanel_hotfix;
    private LuaFunction m_GuildGameListUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_PlayerSimpleInfoUITask_OnClose_hotfix;
    private LuaFunction m_PlayerSimpleInfoUITask_OnPrivateChatButtonClickBusinessCard_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_SendRefreshGuildReq_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildManagementUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GuildManagementUITask StartUITask(
      string guildId = null,
      UIIntent intent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void GuildManagementUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_EventOnGuildUpdateInfo(GuildLog log)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnQuitGuild(Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGetCanInvitePlayerList(
      Action<List<UserSummary>> OnSucceed,
      bool isShowSucceedTip)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGuildHiringDeclarationSet(
      string hiringDeclaration,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGuildInfoSet(
      bool autoJoin,
      int joinLevel,
      string announcement,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGuildAnnouncementSet(
      string announcement,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGetGuildJoinApply(Action<List<UserSummary>> OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGuildJoinConfirmOrRefuse(
      string userId,
      bool isAccept,
      Action OnEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnChangeGuildName(string name, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGetGuildJournal(Action<List<GuildLog>> OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGuildInviteMember(string userId, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnAllRefuseButtonClick(Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGuildMemberClick(
      string userId,
      Vector3 pos,
      PlayerSimpleInfoUITask.PostionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGotoGuildStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGotoGuildGameListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildGameListUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_OnPrivateChatButtonClick(BusinessCard playerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendRefreshGuildReq()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public GuildManagementUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      base.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private GuildManagementUITask m_owner;

      public LuaExportHelper(GuildManagementUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public GuildManagementUIController m_guildManagementUIController
      {
        get
        {
          return this.m_owner.m_guildManagementUIController;
        }
        set
        {
          this.m_owner.m_guildManagementUIController = value;
        }
      }

      public static string m_guildId
      {
        get
        {
          return GuildManagementUITask.m_guildId;
        }
        set
        {
          GuildManagementUITask.m_guildId = value;
        }
      }

      public PlayerSimpleInfoUITask m_playerSimpleInfoUITask
      {
        get
        {
          return this.m_owner.m_playerSimpleInfoUITask;
        }
        set
        {
          this.m_owner.m_playerSimpleInfoUITask = value;
        }
      }

      public float m_lastUpdateTime
      {
        get
        {
          return this.m_owner.m_lastUpdateTime;
        }
        set
        {
          this.m_owner.m_lastUpdateTime = value;
        }
      }

      public static Guild m_guild
      {
        get
        {
          return GuildManagementUITask.m_guild;
        }
        set
        {
          GuildManagementUITask.m_guild = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void InitDataFromUIIntent(UIIntent intent)
      {
        this.m_owner.InitDataFromUIIntent(intent);
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void PostUpdateView()
      {
        this.m_owner.PostUpdateView();
      }

      public void GuildManagementUIController_OnClose()
      {
        this.m_owner.GuildManagementUIController_OnClose();
      }

      public void PlayerContext_EventOnGuildUpdateInfo(GuildLog log)
      {
        this.m_owner.PlayerContext_EventOnGuildUpdateInfo(log);
      }

      public void GuildManagementUIController_OnQuitGuild(Action OnSucceed)
      {
        this.m_owner.GuildManagementUIController_OnQuitGuild(OnSucceed);
      }

      public void GuildManagementUIController_OnGetCanInvitePlayerList(
        Action<List<UserSummary>> OnSucceed,
        bool isShowSucceedTip)
      {
        this.m_owner.GuildManagementUIController_OnGetCanInvitePlayerList(OnSucceed, isShowSucceedTip);
      }

      public void GuildManagementUIController_OnGuildHiringDeclarationSet(
        string hiringDeclaration,
        Action OnSucceed)
      {
        this.m_owner.GuildManagementUIController_OnGuildHiringDeclarationSet(hiringDeclaration, OnSucceed);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void GuildManagementUIController_OnGuildInfoSet(
        bool autoJoin,
        int joinLevel,
        string announcement,
        Action OnSucceed)
      {
        // ISSUE: unable to decompile the method.
      }

      public void GuildManagementUIController_OnGuildAnnouncementSet(
        string announcement,
        Action OnSucceed)
      {
        this.m_owner.GuildManagementUIController_OnGuildAnnouncementSet(announcement, OnSucceed);
      }

      public void GuildManagementUIController_OnGetGuildJoinApply(
        Action<List<UserSummary>> OnSucceed)
      {
        this.m_owner.GuildManagementUIController_OnGetGuildJoinApply(OnSucceed);
      }

      public void GuildManagementUIController_OnGuildJoinConfirmOrRefuse(
        string userId,
        bool isAccept,
        Action OnEnd)
      {
        this.m_owner.GuildManagementUIController_OnGuildJoinConfirmOrRefuse(userId, isAccept, OnEnd);
      }

      public void GuildManagementUIController_OnChangeGuildName(string name, Action OnSucceed)
      {
        this.m_owner.GuildManagementUIController_OnChangeGuildName(name, OnSucceed);
      }

      public void GuildManagementUIController_OnGetGuildJournal(Action<List<GuildLog>> OnSucceed)
      {
        this.m_owner.GuildManagementUIController_OnGetGuildJournal(OnSucceed);
      }

      public void GuildManagementUIController_OnGuildInviteMember(string userId, Action OnSucceed)
      {
        this.m_owner.GuildManagementUIController_OnGuildInviteMember(userId, OnSucceed);
      }

      public void GuildManagementUIController_OnAllRefuseButtonClick(Action OnSucceed)
      {
        this.m_owner.GuildManagementUIController_OnAllRefuseButtonClick(OnSucceed);
      }

      public void GuildManagementUIController_OnGuildMemberClick(
        string userId,
        Vector3 pos,
        PlayerSimpleInfoUITask.PostionType posType)
      {
        this.m_owner.GuildManagementUIController_OnGuildMemberClick(userId, pos, posType);
      }

      public void GuildManagementUIController_OnGotoGuildStore()
      {
        this.m_owner.GuildManagementUIController_OnGotoGuildStore();
      }

      public void GuildManagementUIController_OnGotoGuildGameListPanel()
      {
        this.m_owner.GuildManagementUIController_OnGotoGuildGameListPanel();
      }

      public void GuildGameListUITask_OnLoadAllResCompleted()
      {
        this.m_owner.GuildGameListUITask_OnLoadAllResCompleted();
      }

      public void PlayerSimpleInfoUITask_OnClose()
      {
        this.m_owner.PlayerSimpleInfoUITask_OnClose();
      }

      public void PlayerSimpleInfoUITask_OnPrivateChatButtonClick(BusinessCard playerInfo)
      {
        this.m_owner.PlayerSimpleInfoUITask_OnPrivateChatButtonClick(playerInfo);
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void SendRefreshGuildReq()
      {
        this.m_owner.SendRefreshGuildReq();
      }
    }
  }
}
