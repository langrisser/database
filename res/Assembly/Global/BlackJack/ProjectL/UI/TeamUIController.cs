﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TeamUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./ToggleList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_gameFunctionTypeListScrollRect;
    [AutoBind("./ToggleList/DisableButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_gameFunctionTypeListDisableButton;
    [AutoBind("./LevelListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_locationListUiStateController;
    [AutoBind("./LevelListPanel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_locationListScrollRect;
    [AutoBind("./LevelListPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_locationListCloseButton;
    [AutoBind("./LevelListPanel/DisableButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_locationListDisableButton;
    [AutoBind("./TeamRoomList/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    [AutoBind("./TeamRoomList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_roomListScrollRect;
    [AutoBind("./TeamRoomList/CreateTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_createTeamRoomButton;
    [AutoBind("./TeamRoomList/RefreshButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_refreshTeamRoomButton;
    [AutoBind("./TeamRoomList/AutoMatchingButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoMatchButton;
    [AutoBind("./TeamRoomList/NoTeamPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_noTeamRoomUIStateController;
    [AutoBind("./TeamRoomList/NoTeamPanel/MatchingCancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoMatchCancelButton;
    [AutoBind("./TeamRoomList/NoTeamPanel/MatchingCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_autoMatchWaitCountText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/GameFunctionTypeListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_gameFunctionTypeListItemPrefab;
    [AutoBind("./Prefabs/LocationListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_locationListItemPrefab;
    [AutoBind("./Prefabs/RoomListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_roomListItemPrefab;
    private List<TeamGameFunctionTypeListItemUIController> m_gameFunctionTypeListItems;
    private List<TeamLocationListItemUIController> m_locationListItems;
    private List<TeamRoomListItemUIController> m_roomListItems;
    private bool m_isIgnoreFireSelectEvent;
    [DoNotToLua]
    private TeamUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OpenGameFunctionTypeInt32Int32_hotfix;
    private LuaFunction m_OpenCollectionActivityInt32_hotfix;
    private LuaFunction m_AddGameFunctionTypeListItems_hotfix;
    private LuaFunction m_AddCollectionActivityListItemsInt32_hotfix;
    private LuaFunction m_ClearGameFunctionTypeListItems_hotfix;
    private LuaFunction m_AddGameFunctionTypeListItemStringGameFunctionTypeInt32Boolean_hotfix;
    private LuaFunction m_AddLocationListItemsGameFunctionTypeInt32_hotfix;
    private LuaFunction m_ClearLocationListItems_hotfix;
    private LuaFunction m_AddLocationListItemStringInt32Int32Boolean_hotfix;
    private LuaFunction m_AddTeamRoomListItemTeamRoom_hotfix;
    private LuaFunction m_ClearTeamRoomListItems_hotfix;
    private LuaFunction m_SetTeamRoomsList`1_hotfix;
    private LuaFunction m_SetAutoMatchModeBoolean_hotfix;
    private LuaFunction m_SetAutoMatchWaitCountInt32_hotfix;
    private LuaFunction m_IsAutoMatching_hotfix;
    private LuaFunction m_UpdateTitle_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnRefreshTeamRoomButtonClick_hotfix;
    private LuaFunction m_OnShowCreateTeamRoomButtonClick_hotfix;
    private LuaFunction m_OnAutoMatchButtonClick_hotfix;
    private LuaFunction m_OnAutoMatchCancelButtonClick_hotfix;
    private LuaFunction m_OnLocationListCloseButtonClick_hotfix;
    private LuaFunction m_TeamGameFunctionTypeListItem_OnToggleValueChangedBooleanTeamGameFunctionTypeListItemUIController_hotfix;
    private LuaFunction m_GetSelectedGameFunctionTypeListItem_hotfix;
    private LuaFunction m_GetSelectedLocationListItem_hotfix;
    private LuaFunction m_TeamLocationListItem_OnToggleValueChangedBooleanTeamLocationListItemUIController_hotfix;
    private LuaFunction m_TeamRoomListItem_OnJoinButtonClickTeamRoomListItemUIController_hotfix;
    private LuaFunction m_GetGameFunctionType_hotfix;
    private LuaFunction m_GetChapterId_hotfix;
    private LuaFunction m_GetLocationId_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnRefreshTeamRoomAction_hotfix;
    private LuaFunction m_remove_EventOnRefreshTeamRoomAction_hotfix;
    private LuaFunction m_add_EventOnShowCreateTeamRoomAction_hotfix;
    private LuaFunction m_remove_EventOnShowCreateTeamRoomAction_hotfix;
    private LuaFunction m_add_EventOnAutoMatchAction_hotfix;
    private LuaFunction m_remove_EventOnAutoMatchAction_hotfix;
    private LuaFunction m_add_EventOnAutoMatchCancelAction_hotfix;
    private LuaFunction m_remove_EventOnAutoMatchCancelAction_hotfix;
    private LuaFunction m_add_EventOnSelectGameFunctionTypeAndLocationAction`2_hotfix;
    private LuaFunction m_remove_EventOnSelectGameFunctionTypeAndLocationAction`2_hotfix;
    private LuaFunction m_add_EventOnJoinTeamRoomAction`3_hotfix;
    private LuaFunction m_remove_EventOnJoinTeamRoomAction`3_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(GameFunctionType gameFunctionType, int chapterId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenCollectionActivity(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddGameFunctionTypeListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddCollectionActivityListItems(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearGameFunctionTypeListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddGameFunctionTypeListItem(
      string name,
      GameFunctionType gameFunctionType,
      int chapterId,
      bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLocationListItems(GameFunctionType gameFunctionType, int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearLocationListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLocationListItem(string name, int locationId, int energy, bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddTeamRoomListItem(TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearTeamRoomListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamRooms(List<TeamRoom> rooms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAutoMatchMode(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAutoMatchWaitCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAutoMatching()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTitle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefreshTeamRoomButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowCreateTeamRoomButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoMatchButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoMatchCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLocationListCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamGameFunctionTypeListItem_OnToggleValueChanged(
      bool isOn,
      TeamGameFunctionTypeListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamGameFunctionTypeListItemUIController GetSelectedGameFunctionTypeListItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamLocationListItemUIController GetSelectedLocationListItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamLocationListItem_OnToggleValueChanged(
      bool isOn,
      TeamLocationListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomListItem_OnJoinButtonClick(TeamRoomListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameFunctionType GetGameFunctionType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChapterId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLocationId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGameFunctionName(GameFunctionType gameFunctionType, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetLocationName(GameFunctionType gameFunctionType, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetEnergy(GameFunctionType gameFunctionType, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRefreshTeamRoom
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowCreateTeamRoom
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAutoMatch
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAutoMatchCancel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GameFunctionType, int> EventOnSelectGameFunctionTypeAndLocation
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, GameFunctionType, int> EventOnJoinTeamRoom
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TeamUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRefreshTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRefreshTeamRoom()
    {
      this.EventOnRefreshTeamRoom = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowCreateTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowCreateTeamRoom()
    {
      this.EventOnShowCreateTeamRoom = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAutoMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAutoMatch()
    {
      this.EventOnAutoMatch = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAutoMatchCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAutoMatchCancel()
    {
      this.EventOnAutoMatchCancel = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSelectGameFunctionTypeAndLocation(
      GameFunctionType arg1,
      int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSelectGameFunctionTypeAndLocation(
      GameFunctionType arg1,
      int arg2)
    {
      this.EventOnSelectGameFunctionTypeAndLocation = (Action<GameFunctionType, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnJoinTeamRoom(int arg1, GameFunctionType arg2, int arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnJoinTeamRoom(int arg1, GameFunctionType arg2, int arg3)
    {
      this.EventOnJoinTeamRoom = (Action<int, GameFunctionType, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TeamUIController m_owner;

      public LuaExportHelper(TeamUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnRefreshTeamRoom()
      {
        this.m_owner.__callDele_EventOnRefreshTeamRoom();
      }

      public void __clearDele_EventOnRefreshTeamRoom()
      {
        this.m_owner.__clearDele_EventOnRefreshTeamRoom();
      }

      public void __callDele_EventOnShowCreateTeamRoom()
      {
        this.m_owner.__callDele_EventOnShowCreateTeamRoom();
      }

      public void __clearDele_EventOnShowCreateTeamRoom()
      {
        this.m_owner.__clearDele_EventOnShowCreateTeamRoom();
      }

      public void __callDele_EventOnAutoMatch()
      {
        this.m_owner.__callDele_EventOnAutoMatch();
      }

      public void __clearDele_EventOnAutoMatch()
      {
        this.m_owner.__clearDele_EventOnAutoMatch();
      }

      public void __callDele_EventOnAutoMatchCancel()
      {
        this.m_owner.__callDele_EventOnAutoMatchCancel();
      }

      public void __clearDele_EventOnAutoMatchCancel()
      {
        this.m_owner.__clearDele_EventOnAutoMatchCancel();
      }

      public void __callDele_EventOnSelectGameFunctionTypeAndLocation(
        GameFunctionType arg1,
        int arg2)
      {
        this.m_owner.__callDele_EventOnSelectGameFunctionTypeAndLocation(arg1, arg2);
      }

      public void __clearDele_EventOnSelectGameFunctionTypeAndLocation(
        GameFunctionType arg1,
        int arg2)
      {
        this.m_owner.__clearDele_EventOnSelectGameFunctionTypeAndLocation(arg1, arg2);
      }

      public void __callDele_EventOnJoinTeamRoom(int arg1, GameFunctionType arg2, int arg3)
      {
        this.m_owner.__callDele_EventOnJoinTeamRoom(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnJoinTeamRoom(int arg1, GameFunctionType arg2, int arg3)
      {
        this.m_owner.__clearDele_EventOnJoinTeamRoom(arg1, arg2, arg3);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public ScrollRect m_gameFunctionTypeListScrollRect
      {
        get
        {
          return this.m_owner.m_gameFunctionTypeListScrollRect;
        }
        set
        {
          this.m_owner.m_gameFunctionTypeListScrollRect = value;
        }
      }

      public Button m_gameFunctionTypeListDisableButton
      {
        get
        {
          return this.m_owner.m_gameFunctionTypeListDisableButton;
        }
        set
        {
          this.m_owner.m_gameFunctionTypeListDisableButton = value;
        }
      }

      public CommonUIStateController m_locationListUiStateController
      {
        get
        {
          return this.m_owner.m_locationListUiStateController;
        }
        set
        {
          this.m_owner.m_locationListUiStateController = value;
        }
      }

      public ScrollRect m_locationListScrollRect
      {
        get
        {
          return this.m_owner.m_locationListScrollRect;
        }
        set
        {
          this.m_owner.m_locationListScrollRect = value;
        }
      }

      public Button m_locationListCloseButton
      {
        get
        {
          return this.m_owner.m_locationListCloseButton;
        }
        set
        {
          this.m_owner.m_locationListCloseButton = value;
        }
      }

      public Button m_locationListDisableButton
      {
        get
        {
          return this.m_owner.m_locationListDisableButton;
        }
        set
        {
          this.m_owner.m_locationListDisableButton = value;
        }
      }

      public Text m_titleText
      {
        get
        {
          return this.m_owner.m_titleText;
        }
        set
        {
          this.m_owner.m_titleText = value;
        }
      }

      public ScrollRect m_roomListScrollRect
      {
        get
        {
          return this.m_owner.m_roomListScrollRect;
        }
        set
        {
          this.m_owner.m_roomListScrollRect = value;
        }
      }

      public Button m_createTeamRoomButton
      {
        get
        {
          return this.m_owner.m_createTeamRoomButton;
        }
        set
        {
          this.m_owner.m_createTeamRoomButton = value;
        }
      }

      public Button m_refreshTeamRoomButton
      {
        get
        {
          return this.m_owner.m_refreshTeamRoomButton;
        }
        set
        {
          this.m_owner.m_refreshTeamRoomButton = value;
        }
      }

      public Button m_autoMatchButton
      {
        get
        {
          return this.m_owner.m_autoMatchButton;
        }
        set
        {
          this.m_owner.m_autoMatchButton = value;
        }
      }

      public CommonUIStateController m_noTeamRoomUIStateController
      {
        get
        {
          return this.m_owner.m_noTeamRoomUIStateController;
        }
        set
        {
          this.m_owner.m_noTeamRoomUIStateController = value;
        }
      }

      public Button m_autoMatchCancelButton
      {
        get
        {
          return this.m_owner.m_autoMatchCancelButton;
        }
        set
        {
          this.m_owner.m_autoMatchCancelButton = value;
        }
      }

      public Text m_autoMatchWaitCountText
      {
        get
        {
          return this.m_owner.m_autoMatchWaitCountText;
        }
        set
        {
          this.m_owner.m_autoMatchWaitCountText = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_gameFunctionTypeListItemPrefab
      {
        get
        {
          return this.m_owner.m_gameFunctionTypeListItemPrefab;
        }
        set
        {
          this.m_owner.m_gameFunctionTypeListItemPrefab = value;
        }
      }

      public GameObject m_locationListItemPrefab
      {
        get
        {
          return this.m_owner.m_locationListItemPrefab;
        }
        set
        {
          this.m_owner.m_locationListItemPrefab = value;
        }
      }

      public GameObject m_roomListItemPrefab
      {
        get
        {
          return this.m_owner.m_roomListItemPrefab;
        }
        set
        {
          this.m_owner.m_roomListItemPrefab = value;
        }
      }

      public List<TeamGameFunctionTypeListItemUIController> m_gameFunctionTypeListItems
      {
        get
        {
          return this.m_owner.m_gameFunctionTypeListItems;
        }
        set
        {
          this.m_owner.m_gameFunctionTypeListItems = value;
        }
      }

      public List<TeamLocationListItemUIController> m_locationListItems
      {
        get
        {
          return this.m_owner.m_locationListItems;
        }
        set
        {
          this.m_owner.m_locationListItems = value;
        }
      }

      public List<TeamRoomListItemUIController> m_roomListItems
      {
        get
        {
          return this.m_owner.m_roomListItems;
        }
        set
        {
          this.m_owner.m_roomListItems = value;
        }
      }

      public bool m_isIgnoreFireSelectEvent
      {
        get
        {
          return this.m_owner.m_isIgnoreFireSelectEvent;
        }
        set
        {
          this.m_owner.m_isIgnoreFireSelectEvent = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void AddGameFunctionTypeListItems()
      {
        this.m_owner.AddGameFunctionTypeListItems();
      }

      public void AddCollectionActivityListItems(int collectionActivityId)
      {
        this.m_owner.AddCollectionActivityListItems(collectionActivityId);
      }

      public void ClearGameFunctionTypeListItems()
      {
        this.m_owner.ClearGameFunctionTypeListItems();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void AddGameFunctionTypeListItem(
        string name,
        GameFunctionType gameFunctionType,
        int chapterId,
        bool isLocked)
      {
        // ISSUE: unable to decompile the method.
      }

      public void AddLocationListItems(GameFunctionType gameFunctionType, int chapterId)
      {
        this.m_owner.AddLocationListItems(gameFunctionType, chapterId);
      }

      public void ClearLocationListItems()
      {
        this.m_owner.ClearLocationListItems();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void AddLocationListItem(string name, int locationId, int energy, bool isLocked)
      {
        // ISSUE: unable to decompile the method.
      }

      public void AddTeamRoomListItem(TeamRoom room)
      {
        this.m_owner.AddTeamRoomListItem(room);
      }

      public void ClearTeamRoomListItems()
      {
        this.m_owner.ClearTeamRoomListItems();
      }

      public void UpdateTitle()
      {
        this.m_owner.UpdateTitle();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnRefreshTeamRoomButtonClick()
      {
        this.m_owner.OnRefreshTeamRoomButtonClick();
      }

      public void OnShowCreateTeamRoomButtonClick()
      {
        this.m_owner.OnShowCreateTeamRoomButtonClick();
      }

      public void OnAutoMatchButtonClick()
      {
        this.m_owner.OnAutoMatchButtonClick();
      }

      public void OnAutoMatchCancelButtonClick()
      {
        this.m_owner.OnAutoMatchCancelButtonClick();
      }

      public void OnLocationListCloseButtonClick()
      {
        this.m_owner.OnLocationListCloseButtonClick();
      }

      public void TeamGameFunctionTypeListItem_OnToggleValueChanged(
        bool isOn,
        TeamGameFunctionTypeListItemUIController ctrl)
      {
        this.m_owner.TeamGameFunctionTypeListItem_OnToggleValueChanged(isOn, ctrl);
      }

      public TeamGameFunctionTypeListItemUIController GetSelectedGameFunctionTypeListItem()
      {
        return this.m_owner.GetSelectedGameFunctionTypeListItem();
      }

      public TeamLocationListItemUIController GetSelectedLocationListItem()
      {
        return this.m_owner.GetSelectedLocationListItem();
      }

      public void TeamLocationListItem_OnToggleValueChanged(
        bool isOn,
        TeamLocationListItemUIController ctrl)
      {
        this.m_owner.TeamLocationListItem_OnToggleValueChanged(isOn, ctrl);
      }

      public void TeamRoomListItem_OnJoinButtonClick(TeamRoomListItemUIController ctrl)
      {
        this.m_owner.TeamRoomListItem_OnJoinButtonClick(ctrl);
      }
    }
  }
}
