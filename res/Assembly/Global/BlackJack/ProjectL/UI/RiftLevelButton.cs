﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftLevelButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class RiftLevelButton : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./NameNum/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameNumText;
    [AutoBind("./ChallengeCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeCountText;
    [AutoBind("./StarsandCup/Star1/Star", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star1GameObject;
    [AutoBind("./StarsandCup/Star2/Star", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star2GameObject;
    [AutoBind("./StarsandCup/Star3/Star", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star3GameObject;
    [AutoBind("./StarsandCup/Cup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementCountText;
    [AutoBind("./StoryImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    [AutoBind("./StoryNormalImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image1;
    private ConfigDataRiftLevelInfo m_riftLevelInfo;
    private RiftLevelStatus m_status;
    [DoNotToLua]
    private RiftLevelButton.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetRiftLevelInfoConfigDataRiftLevelInfo_hotfix;
    private LuaFunction m_GetRiftLevel_hotfix;
    private LuaFunction m_SetStatusRiftLevelStatusBooleanBooleanBoolean_hotfix;
    private LuaFunction m_GetStatus_hotfix;
    private LuaFunction m_SetStarInt32_hotfix;
    private LuaFunction m_SetChallengeCountInt32Int32_hotfix;
    private LuaFunction m_SetAchievementCountInt32Int32_hotfix;
    private LuaFunction m_OnClick_hotfix;
    private LuaFunction m_add_EventOnClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRiftLevelInfo(ConfigDataRiftLevelInfo riftLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRiftLevelInfo GetRiftLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStatus(RiftLevelStatus status, bool isNew, bool isClear, bool isAllTreasureGot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevelStatus GetStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStar(int star)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChallengeCount(int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAchievementCount(int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<RiftLevelButton> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public RiftLevelButton.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClick(RiftLevelButton obj)
    {
    }

    private void __clearDele_EventOnClick(RiftLevelButton obj)
    {
      this.EventOnClick = (Action<RiftLevelButton>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private RiftLevelButton m_owner;

      public LuaExportHelper(RiftLevelButton owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClick(RiftLevelButton obj)
      {
        this.m_owner.__callDele_EventOnClick(obj);
      }

      public void __clearDele_EventOnClick(RiftLevelButton obj)
      {
        this.m_owner.__clearDele_EventOnClick(obj);
      }

      public Button m_button
      {
        get
        {
          return this.m_owner.m_button;
        }
        set
        {
          this.m_owner.m_button = value;
        }
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Text m_nameNumText
      {
        get
        {
          return this.m_owner.m_nameNumText;
        }
        set
        {
          this.m_owner.m_nameNumText = value;
        }
      }

      public Text m_challengeCountText
      {
        get
        {
          return this.m_owner.m_challengeCountText;
        }
        set
        {
          this.m_owner.m_challengeCountText = value;
        }
      }

      public GameObject m_star1GameObject
      {
        get
        {
          return this.m_owner.m_star1GameObject;
        }
        set
        {
          this.m_owner.m_star1GameObject = value;
        }
      }

      public GameObject m_star2GameObject
      {
        get
        {
          return this.m_owner.m_star2GameObject;
        }
        set
        {
          this.m_owner.m_star2GameObject = value;
        }
      }

      public GameObject m_star3GameObject
      {
        get
        {
          return this.m_owner.m_star3GameObject;
        }
        set
        {
          this.m_owner.m_star3GameObject = value;
        }
      }

      public Text m_achievementCountText
      {
        get
        {
          return this.m_owner.m_achievementCountText;
        }
        set
        {
          this.m_owner.m_achievementCountText = value;
        }
      }

      public Image m_image
      {
        get
        {
          return this.m_owner.m_image;
        }
        set
        {
          this.m_owner.m_image = value;
        }
      }

      public Image m_image1
      {
        get
        {
          return this.m_owner.m_image1;
        }
        set
        {
          this.m_owner.m_image1 = value;
        }
      }

      public ConfigDataRiftLevelInfo m_riftLevelInfo
      {
        get
        {
          return this.m_owner.m_riftLevelInfo;
        }
        set
        {
          this.m_owner.m_riftLevelInfo = value;
        }
      }

      public RiftLevelStatus m_status
      {
        get
        {
          return this.m_owner.m_status;
        }
        set
        {
          this.m_owner.m_status = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnClick()
      {
        this.m_owner.OnClick();
      }
    }
  }
}
