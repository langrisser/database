﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BigExpressionItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BigExpressionItem
  {
    private Action<int> m_onClick;
    public ConfigDataBigExpressionInfo m_bigExpressionInfo;
    public GameObject m_expression;
    private Image imageIcon;

    [MethodImpl((MethodImplOptions) 32768)]
    public BigExpressionItem(
      ConfigDataBigExpressionInfo bigExpressionInfo,
      GameObject expression,
      Action<int> click)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
