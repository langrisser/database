﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroArchiveUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroArchiveUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prefabAnimation;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./Margin/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroShowButton;
    [AutoBind("./HeroListGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_heroScrollRect;
    [AutoBind("./HeroListGroup/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroContent;
    [AutoBind("./HeroListGroup/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private InfinityGridLayoutGroup m_heroContentInfinityGrid;
    [AutoBind("./Prefab/HeroHeadPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroHeadPrefab;
    [AutoBind("./CountLimit/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ownHeroCountText;
    [AutoBind("./HeroGroup/HeroEnglishNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroEnglishNameText;
    [AutoBind("./HeroGroup/PaperRightImage/JobPatternTitleBgImage/SpineGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_spineJobDummy;
    [AutoBind("./HeroGroup/PaperRightImage/HeroPatternTitle/HeroDescScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_introTextscrollRect;
    [AutoBind("./HeroGroup/PaperRightImage/HeroPatternTitle/HeroDescScrollView/Viewport/Content/HeroMoreText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroIntroText;
    [AutoBind("./HeroGroup/PaperRightImage/ApproachPatternTitleBgImage/ExplainText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroObtainText;
    [AutoBind("./HeroGroup/HeroDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroDrawShowDummy;
    [AutoBind("./HeroGroup/HeroMessageGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroMessageGroup;
    [AutoBind("./HeroGroup/GradeToggle/All", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charALLToggle;
    [AutoBind("./HeroGroup/GradeToggle/SSR", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charSSRToggle;
    [AutoBind("./HeroGroup/GradeToggle/SR", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charSRToggle;
    [AutoBind("./HeroGroup/GradeToggle/R", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charRToggle;
    private HeroMessageGroupUIComponent m_messageGroupUIComponent;
    private ArchiveUITask m_task;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private List<HeroArchiveUIController.HeroWrap> m_selectHeroList;
    private List<HeroArchiveUIController.HeroWrap> m_heroAllList;
    private List<HeroArchiveUIController.HeroWrap> m_heroSSRList;
    private List<HeroArchiveUIController.HeroWrap> m_heroSRList;
    private List<HeroArchiveUIController.HeroWrap> m_heroRList;
    private List<HeroHeadUIController> m_heroHeadUIControllerList;
    private HeroArchiveUIController.HeroWrap m_selectHeroWrap;
    private GameObject m_selectHeroPainting;
    private List<UISpineGraphic> m_jobSpineGraphicList;
    private int m_allOwnHeroCount;
    private int m_ssrOwnHeroCount;
    private int m_srOwnHeroCount;
    private int m_rOwnHeroCount;
    private const int FinalJobRank = 4;
    [DoNotToLua]
    private HeroArchiveUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetTaskArchiveUITask_hotfix;
    private LuaFunction m_EnterController_hotfix;
    private LuaFunction m_HeroListCompareHeroWrapHeroWrap_hotfix;
    private LuaFunction m_GetHeroFinalJobListConfigDataHeroInfo_hotfix;
    private LuaFunction m_HeroCountRefreshInt32Int32_hotfix;
    private LuaFunction m_HeroDrawRefreshHeroHeadUIController_hotfix;
    private LuaFunction m_CreateHeroGraphicHeroWrap_hotfix;
    private LuaFunction m_CreateHeroHeadListList`1_hotfix;
    private LuaFunction m_DestroyHeroHeadList_hotfix;
    private LuaFunction m_RefreshHeroHeadWithHeroDataHeroWrap_hotfix;
    private LuaFunction m_OnReturnClick_hotfix;
    private LuaFunction m_OnCharFilterClickGameObject_hotfix;
    private LuaFunction m_OnHeroHeadClickHeroHeadUIController_hotfix;
    private LuaFunction m_OnHeroShowClick_hotfix;
    private LuaFunction m_OnHeroHeadListRefreshInt32Transform_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroArchiveUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTask(ArchiveUITask task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnterController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroListCompare(
      HeroArchiveUIController.HeroWrap heroWrap1,
      HeroArchiveUIController.HeroWrap heroWrap2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ConfigDataJobConnectionInfo> GetHeroFinalJobList(
      ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroCountRefresh(int ownCount, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDrawRefresh(HeroHeadUIController heroHeadUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateHeroGraphic(HeroArchiveUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateHeroHeadList(List<HeroArchiveUIController.HeroWrap> heroList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyHeroHeadList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshHeroHeadWithHeroData(HeroArchiveUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCharFilterClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroHeadClick(HeroHeadUIController heroHeadUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroShowClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroHeadListRefresh(int index, Transform trans)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public HeroArchiveUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [HotFix]
    public class HeroWrap
    {
      public ConfigDataHeroInfo hero;
      public bool isUnlocked;
      public bool isSelect;
    }

    public class LuaExportHelper
    {
      private HeroArchiveUIController m_owner;

      public LuaExportHelper(HeroArchiveUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_prefabAnimation
      {
        get
        {
          return this.m_owner.m_prefabAnimation;
        }
        set
        {
          this.m_owner.m_prefabAnimation = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_heroShowButton
      {
        get
        {
          return this.m_owner.m_heroShowButton;
        }
        set
        {
          this.m_owner.m_heroShowButton = value;
        }
      }

      public ScrollRect m_heroScrollRect
      {
        get
        {
          return this.m_owner.m_heroScrollRect;
        }
        set
        {
          this.m_owner.m_heroScrollRect = value;
        }
      }

      public GameObject m_heroContent
      {
        get
        {
          return this.m_owner.m_heroContent;
        }
        set
        {
          this.m_owner.m_heroContent = value;
        }
      }

      public InfinityGridLayoutGroup m_heroContentInfinityGrid
      {
        get
        {
          return this.m_owner.m_heroContentInfinityGrid;
        }
        set
        {
          this.m_owner.m_heroContentInfinityGrid = value;
        }
      }

      public GameObject m_heroHeadPrefab
      {
        get
        {
          return this.m_owner.m_heroHeadPrefab;
        }
        set
        {
          this.m_owner.m_heroHeadPrefab = value;
        }
      }

      public Text m_ownHeroCountText
      {
        get
        {
          return this.m_owner.m_ownHeroCountText;
        }
        set
        {
          this.m_owner.m_ownHeroCountText = value;
        }
      }

      public Text m_heroEnglishNameText
      {
        get
        {
          return this.m_owner.m_heroEnglishNameText;
        }
        set
        {
          this.m_owner.m_heroEnglishNameText = value;
        }
      }

      public GameObject m_spineJobDummy
      {
        get
        {
          return this.m_owner.m_spineJobDummy;
        }
        set
        {
          this.m_owner.m_spineJobDummy = value;
        }
      }

      public ScrollRect m_introTextscrollRect
      {
        get
        {
          return this.m_owner.m_introTextscrollRect;
        }
        set
        {
          this.m_owner.m_introTextscrollRect = value;
        }
      }

      public Text m_heroIntroText
      {
        get
        {
          return this.m_owner.m_heroIntroText;
        }
        set
        {
          this.m_owner.m_heroIntroText = value;
        }
      }

      public Text m_heroObtainText
      {
        get
        {
          return this.m_owner.m_heroObtainText;
        }
        set
        {
          this.m_owner.m_heroObtainText = value;
        }
      }

      public GameObject m_heroDrawShowDummy
      {
        get
        {
          return this.m_owner.m_heroDrawShowDummy;
        }
        set
        {
          this.m_owner.m_heroDrawShowDummy = value;
        }
      }

      public GameObject m_heroMessageGroup
      {
        get
        {
          return this.m_owner.m_heroMessageGroup;
        }
        set
        {
          this.m_owner.m_heroMessageGroup = value;
        }
      }

      public Toggle m_charALLToggle
      {
        get
        {
          return this.m_owner.m_charALLToggle;
        }
        set
        {
          this.m_owner.m_charALLToggle = value;
        }
      }

      public Toggle m_charSSRToggle
      {
        get
        {
          return this.m_owner.m_charSSRToggle;
        }
        set
        {
          this.m_owner.m_charSSRToggle = value;
        }
      }

      public Toggle m_charSRToggle
      {
        get
        {
          return this.m_owner.m_charSRToggle;
        }
        set
        {
          this.m_owner.m_charSRToggle = value;
        }
      }

      public Toggle m_charRToggle
      {
        get
        {
          return this.m_owner.m_charRToggle;
        }
        set
        {
          this.m_owner.m_charRToggle = value;
        }
      }

      public HeroMessageGroupUIComponent m_messageGroupUIComponent
      {
        get
        {
          return this.m_owner.m_messageGroupUIComponent;
        }
        set
        {
          this.m_owner.m_messageGroupUIComponent = value;
        }
      }

      public ArchiveUITask m_task
      {
        get
        {
          return this.m_owner.m_task;
        }
        set
        {
          this.m_owner.m_task = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public List<HeroArchiveUIController.HeroWrap> m_selectHeroList
      {
        get
        {
          return this.m_owner.m_selectHeroList;
        }
        set
        {
          this.m_owner.m_selectHeroList = value;
        }
      }

      public List<HeroArchiveUIController.HeroWrap> m_heroAllList
      {
        get
        {
          return this.m_owner.m_heroAllList;
        }
        set
        {
          this.m_owner.m_heroAllList = value;
        }
      }

      public List<HeroArchiveUIController.HeroWrap> m_heroSSRList
      {
        get
        {
          return this.m_owner.m_heroSSRList;
        }
        set
        {
          this.m_owner.m_heroSSRList = value;
        }
      }

      public List<HeroArchiveUIController.HeroWrap> m_heroSRList
      {
        get
        {
          return this.m_owner.m_heroSRList;
        }
        set
        {
          this.m_owner.m_heroSRList = value;
        }
      }

      public List<HeroArchiveUIController.HeroWrap> m_heroRList
      {
        get
        {
          return this.m_owner.m_heroRList;
        }
        set
        {
          this.m_owner.m_heroRList = value;
        }
      }

      public List<HeroHeadUIController> m_heroHeadUIControllerList
      {
        get
        {
          return this.m_owner.m_heroHeadUIControllerList;
        }
        set
        {
          this.m_owner.m_heroHeadUIControllerList = value;
        }
      }

      public HeroArchiveUIController.HeroWrap m_selectHeroWrap
      {
        get
        {
          return this.m_owner.m_selectHeroWrap;
        }
        set
        {
          this.m_owner.m_selectHeroWrap = value;
        }
      }

      public GameObject m_selectHeroPainting
      {
        get
        {
          return this.m_owner.m_selectHeroPainting;
        }
        set
        {
          this.m_owner.m_selectHeroPainting = value;
        }
      }

      public List<UISpineGraphic> m_jobSpineGraphicList
      {
        get
        {
          return this.m_owner.m_jobSpineGraphicList;
        }
        set
        {
          this.m_owner.m_jobSpineGraphicList = value;
        }
      }

      public int m_allOwnHeroCount
      {
        get
        {
          return this.m_owner.m_allOwnHeroCount;
        }
        set
        {
          this.m_owner.m_allOwnHeroCount = value;
        }
      }

      public int m_ssrOwnHeroCount
      {
        get
        {
          return this.m_owner.m_ssrOwnHeroCount;
        }
        set
        {
          this.m_owner.m_ssrOwnHeroCount = value;
        }
      }

      public int m_srOwnHeroCount
      {
        get
        {
          return this.m_owner.m_srOwnHeroCount;
        }
        set
        {
          this.m_owner.m_srOwnHeroCount = value;
        }
      }

      public int m_rOwnHeroCount
      {
        get
        {
          return this.m_owner.m_rOwnHeroCount;
        }
        set
        {
          this.m_owner.m_rOwnHeroCount = value;
        }
      }

      public static int FinalJobRank
      {
        get
        {
          return 4;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public int HeroListCompare(
        HeroArchiveUIController.HeroWrap heroWrap1,
        HeroArchiveUIController.HeroWrap heroWrap2)
      {
        return this.m_owner.HeroListCompare(heroWrap1, heroWrap2);
      }

      public List<ConfigDataJobConnectionInfo> GetHeroFinalJobList(
        ConfigDataHeroInfo heroInfo)
      {
        return this.m_owner.GetHeroFinalJobList(heroInfo);
      }

      public void HeroDrawRefresh(HeroHeadUIController heroHeadUIController)
      {
        this.m_owner.HeroDrawRefresh(heroHeadUIController);
      }

      public void CreateHeroHeadList(List<HeroArchiveUIController.HeroWrap> heroList)
      {
        this.m_owner.CreateHeroHeadList(heroList);
      }

      public void DestroyHeroHeadList()
      {
        this.m_owner.DestroyHeroHeadList();
      }

      public void RefreshHeroHeadWithHeroData(HeroArchiveUIController.HeroWrap heroWrap)
      {
        this.m_owner.RefreshHeroHeadWithHeroData(heroWrap);
      }

      public void OnReturnClick()
      {
        this.m_owner.OnReturnClick();
      }

      public void OnCharFilterClick(GameObject obj)
      {
        this.m_owner.OnCharFilterClick(obj);
      }

      public void OnHeroHeadClick(HeroHeadUIController heroHeadUIController)
      {
        this.m_owner.OnHeroHeadClick(heroHeadUIController);
      }

      public void OnHeroShowClick()
      {
        this.m_owner.OnHeroShowClick();
      }

      public void OnHeroHeadListRefresh(int index, Transform trans)
      {
        this.m_owner.OnHeroHeadListRefresh(index, trans);
      }
    }
  }
}
