﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerChatItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class PlayerChatItemUIController : UIControllerBase
  {
    private bool m_isUIEventInited;
    public bool m_isEmojiTextInit;
    public ChatComponent.ChatMessageClient m_currChatInfo;
    [AutoBind("./TimeAndPlayerName/PlayerNameText", AutoBindAttribute.InitState.Active, false)]
    public Text PlayerNameText;
    [AutoBind("./TimeAndPlayerName/SendTimeText", AutoBindAttribute.InitState.Active, false)]
    public Text SendTimeText;
    [AutoBind("./PlayerIcon/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image PlayerIconImage;
    [AutoBind("./PlayerIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    public Transform PlayerHeadFrameTransform;
    [AutoBind("./PlayerIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerLevelText;
    [AutoBind("./PlayerIcon/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    public Button PlayerIconButton;
    [AutoBind("./ContentBG/ContentText", AutoBindAttribute.InitState.Active, false)]
    public EmojiText ContentText;
    [AutoBind("./ContentBG", AutoBindAttribute.InitState.Active, false)]
    public GameObject ContentGo;
    [AutoBind("./Voice/Voice/SpeakImage", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController VoicePlayerStateCtrl;
    [AutoBind("./Voice", AutoBindAttribute.InitState.NotInit, false)]
    public Button VoiceButton;
    [AutoBind("./Voice/Voice/TimeButton/ContentText", AutoBindAttribute.InitState.Active, false)]
    public Text VoiceLengthText;
    [AutoBind("./Voice/ContentText", AutoBindAttribute.InitState.Active, false)]
    public Text VoiceContentText;
    [AutoBind("./Voice", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject VoiceRoot;
    [AutoBind("./Face", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject FaceRoot;
    [AutoBind("./Face/Image", AutoBindAttribute.InitState.NotInit, false)]
    public Image FaceIcon;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController itemStateCtrl;
    public ButtonEx TranslateButton;
    public ButtonEx TransBackButton;
    public Text TranslateCompleteText;

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateChatInfo(ChatComponent.ChatMessageClient chatClientInfo, bool isPlaying = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEmojiText(int fontSize, Image image)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlaying()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayState(bool play)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegClickEvent(
      Action<ChatComponent.ChatMessageClient> voiceAction,
      Action<ChatMessage, GameObject> playeIconAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetToVoiceMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetToTextMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetToBigExpressionMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string SetChatMsgTime(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnVoiceButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerHeadClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTranslateButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTransBackButtonClick()
    {
    }

    public event Action<ChatComponent.ChatMessageClient> EventOnVoiceButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<ChatMessage, GameObject> EventOnPlayerHeadClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CommonUIStateController TranslateButtonStateCtrl { get; private set; }
  }
}
