﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerVoiceHandleThread
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class PlayerVoiceHandleThread
  {
    public bool m_startHandle;
    public Thread m_voiceHandleThread;
    private Queue<PlayerVoiceHandleThread.VoicePacket> m_inputQueue;
    private Queue<PlayerVoiceHandleThread.VoicePacket> m_outputQueue;
    private List<int> m_validIdList;
    private readonly object m_lock;
    private readonly object m_seclock;
    private readonly object m_idListLock;
    private static PlayerVoiceHandleThread m_instance;

    [MethodImpl((MethodImplOptions) 32768)]
    private PlayerVoiceHandleThread()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Stop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddDataToInputBuffer(PlayerVoiceHandleThread.VoicePacket packet)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDataContentToInputBuffer(float[] audioData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDataHeadToInputBuffer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDataEndToInputBuffer(ChatVoiceInfo chatInfo, bool isValid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private PlayerVoiceHandleThread.VoicePacket GetInputBufferData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddDataToOutputBuffer(PlayerVoiceHandleThread.VoicePacket packet)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerVoiceHandleThread.VoicePacket GetOutputBufferData()
    {
      // ISSUE: unable to decompile the method.
    }

    private int GetTickTime()
    {
      return 250;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void VoiceThreadProc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private byte[] ByteArrayListToByteArray(List<byte[]> list)
    {
      // ISSUE: unable to decompile the method.
    }

    public static PlayerVoiceHandleThread Instance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [CustomLuaClass]
    public class VoicePacket
    {
      public bool m_isValid = true;
      public float[] m_srcData;
      public ChatVoiceInfo m_chatInfo;
      public bool m_isCompressed;
      public DataType m_type;
    }
  }
}
