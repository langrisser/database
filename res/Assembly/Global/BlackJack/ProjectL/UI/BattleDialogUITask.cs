﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleDialogUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BattleDialogUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private BattleDialogUIController m_battleDialogUIController;
    private ConfigDataBattleDialogInfo m_battleDialogInfo;
    private bool m_isSkipClose;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleDialogUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool OnStart(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnStart(intent);
    }

    protected override bool OnResume(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnResume(intent);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void InitAllUIControllers()
    {
      base.InitAllUIControllers();
      this.InitBattleDialogUIController();
    }

    protected override void ClearAllContextAndRes()
    {
      base.ClearAllContextAndRes();
      this.UninitBattleDialogUIController();
    }

    protected override void OnMemoryWarning()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattleDialogUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitBattleDialogUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUIController_OnSkip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SkipDialogDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUIController_OnNext()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool> EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnUserGuideClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
