﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TreasureMapUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TreasureMapUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./PlayerResource/Ticket/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ticketText;
    [AutoBind("./PlayerResource/Ticket/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addTicketButton;
    [AutoBind("./LevelList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelListScrollRect;
    [AutoBind("./Background/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char0GameObject;
    [AutoBind("./Background/Char1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char1GameObject;
    [AutoBind("./Background/Char2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char2GameObject;
    [AutoBind("./Background/Char3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char3GameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/TreasureLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_treasureLevelListItemPrefab;
    private List<TreasureLevelListItemUIController> m_treasureLevelListItems;
    private TreasureMapBackgroundActor[] m_actors;
    [DoNotToLua]
    private TreasureMapUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_ResetScrollViewToTop_hotfix;
    private LuaFunction m_SetTicketCountInt32Int32_hotfix;
    private LuaFunction m_SetScrollViewPosition_hotfix;
    private LuaFunction m_AddTreasureLevelButtonConfigDataTreasureLevelInfoBoolean_hotfix;
    private LuaFunction m_ClearTreasureLevelButtons_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnAddTicketButtonClick_hotfix;
    private LuaFunction m_ThreasureLevelListItem_OnStartButtonClickTreasureLevelListItemUIController_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnAddTicketAction_hotfix;
    private LuaFunction m_remove_EventOnAddTicketAction_hotfix;
    private LuaFunction m_add_EventOnStartTreasureLevelAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartTreasureLevelAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private TreasureMapUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTicketCount(int haveCount, int allCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScrollViewPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddTreasureLevelButton(ConfigDataTreasureLevelInfo levelnfo, bool opened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearTreasureLevelButtons()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddTicketButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ThreasureLevelListItem_OnStartButtonClick(TreasureLevelListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataTreasureLevelInfo> EventOnStartTreasureLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TreasureMapUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddTicket()
    {
      this.EventOnAddTicket = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartTreasureLevel(ConfigDataTreasureLevelInfo obj)
    {
    }

    private void __clearDele_EventOnStartTreasureLevel(ConfigDataTreasureLevelInfo obj)
    {
      this.EventOnStartTreasureLevel = (Action<ConfigDataTreasureLevelInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TreasureMapUIController m_owner;

      public LuaExportHelper(TreasureMapUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnAddTicket()
      {
        this.m_owner.__callDele_EventOnAddTicket();
      }

      public void __clearDele_EventOnAddTicket()
      {
        this.m_owner.__clearDele_EventOnAddTicket();
      }

      public void __callDele_EventOnStartTreasureLevel(ConfigDataTreasureLevelInfo obj)
      {
        this.m_owner.__callDele_EventOnStartTreasureLevel(obj);
      }

      public void __clearDele_EventOnStartTreasureLevel(ConfigDataTreasureLevelInfo obj)
      {
        this.m_owner.__clearDele_EventOnStartTreasureLevel(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Text m_ticketText
      {
        get
        {
          return this.m_owner.m_ticketText;
        }
        set
        {
          this.m_owner.m_ticketText = value;
        }
      }

      public Button m_addTicketButton
      {
        get
        {
          return this.m_owner.m_addTicketButton;
        }
        set
        {
          this.m_owner.m_addTicketButton = value;
        }
      }

      public ScrollRect m_levelListScrollRect
      {
        get
        {
          return this.m_owner.m_levelListScrollRect;
        }
        set
        {
          this.m_owner.m_levelListScrollRect = value;
        }
      }

      public GameObject m_char0GameObject
      {
        get
        {
          return this.m_owner.m_char0GameObject;
        }
        set
        {
          this.m_owner.m_char0GameObject = value;
        }
      }

      public GameObject m_char1GameObject
      {
        get
        {
          return this.m_owner.m_char1GameObject;
        }
        set
        {
          this.m_owner.m_char1GameObject = value;
        }
      }

      public GameObject m_char2GameObject
      {
        get
        {
          return this.m_owner.m_char2GameObject;
        }
        set
        {
          this.m_owner.m_char2GameObject = value;
        }
      }

      public GameObject m_char3GameObject
      {
        get
        {
          return this.m_owner.m_char3GameObject;
        }
        set
        {
          this.m_owner.m_char3GameObject = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_treasureLevelListItemPrefab
      {
        get
        {
          return this.m_owner.m_treasureLevelListItemPrefab;
        }
        set
        {
          this.m_owner.m_treasureLevelListItemPrefab = value;
        }
      }

      public List<TreasureLevelListItemUIController> m_treasureLevelListItems
      {
        get
        {
          return this.m_owner.m_treasureLevelListItems;
        }
        set
        {
          this.m_owner.m_treasureLevelListItems = value;
        }
      }

      public TreasureMapBackgroundActor[] m_actors
      {
        get
        {
          return this.m_owner.m_actors;
        }
        set
        {
          this.m_owner.m_actors = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ResetScrollViewToTop()
      {
        this.m_owner.ResetScrollViewToTop();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnAddTicketButtonClick()
      {
        this.m_owner.OnAddTicketButtonClick();
      }

      public void ThreasureLevelListItem_OnStartButtonClick(TreasureLevelListItemUIController ctrl)
      {
        this.m_owner.ThreasureLevelListItem_OnStartButtonClick(ctrl);
      }
    }
  }
}
