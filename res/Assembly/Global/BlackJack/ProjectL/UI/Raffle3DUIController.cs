﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.Raffle3DUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class Raffle3DUIController : UIControllerBase
  {
    protected Action m_onAnimationEnd;
    protected static string AniParam_Init;
    protected static string AniParam_Level0;
    protected static string AniParam_Level1;
    protected static string AniParam_Level2;
    protected static string AniParam_Level3;
    [AutoBind("./3DBgImage", AutoBindAttribute.InitState.NotInit, false)]
    public Button ThreeDBgButton;
    [AutoBind("./3DViewImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image ThreeDViewImage;
    [AutoBind("./3DCamera", AutoBindAttribute.InitState.NotInit, false)]
    public Camera ThreeDViewCamera;
    [AutoBind("./Mesh_FX_Xionggui", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController UIEffectStateCtrl;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Clear()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRaffle3DModel(bool isShow)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDrawIdleAnimation()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRaffeDrawingAnimation(int level, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetAniNameByLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnDrawAnimationEnd(GameObject go, string aniName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnDrawAnimationEndImp()
    {
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator WaitForTime(Action action, float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void On3DBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Rect CalcThreeDCameraViewRect()
    {
      // ISSUE: unable to decompile the method.
    }

    public Camera LayerCamera { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    static Raffle3DUIController()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
