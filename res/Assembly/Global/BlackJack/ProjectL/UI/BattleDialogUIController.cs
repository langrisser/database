﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleDialogUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Misc;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BattleDialogUIController : UIControllerBase
  {
    [AutoBind("./SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skipButton;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./WordPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_wordsGameObject;
    [AutoBind("./Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char0GameObject;
    [AutoBind("./Char/1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char1GameObject;
    private DialogCharUIController[] m_dialogCharUIControllers;
    private BattleDialogBoxUIController m_dialogBoxUIController;
    private IAudioPlayback m_currentAudio;
    private ConfigDataBattleDialogInfo m_dialogInfo;
    private float m_showDialogBoxTime;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleDialogUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public void CloseDialog()
    {
      this.StartCoroutine(this.Co_CloseDialog());
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_CloseDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDialogBox()
    {
      // ISSUE: unable to decompile the method.
    }

    private IAudioPlayback PlayVoice(string name)
    {
      return AudioUtility.PlaySound(name);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopVoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_NextDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
    }

    public event Action EventOnSkip
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnNext
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
