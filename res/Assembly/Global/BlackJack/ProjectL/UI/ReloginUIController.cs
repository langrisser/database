﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ReloginUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ReloginUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./DialogBox", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dialogBoxGameObject;
    [AutoBind("./DialogBox/Buttons/RetryButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button RetryLoginButton;
    [AutoBind("./DialogBox/Buttons/LoginButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button ReturnToLoginButton;
    [AutoBind("./Waiting", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_waitingGameObject;

    private ReloginUIController()
    {
    }

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitForReloginConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitForReloginProcessing()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitReturnToLoginConfirm()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
