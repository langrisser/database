﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroAssistantAssignToTaskNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroAssistantAssignToTaskNetTask : UINetTask
  {
    private int m_slot;
    private int m_taskIndex;
    private List<int> m_heroIds;
    private int m_workSeconds;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAssistantAssignToTaskNetTask(
      int taskIndex,
      List<int> heroIds,
      int workSeconds,
      int slot)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnHeroAssistantAssignToTaskAck(int result)
    {
      this.Result = result;
      this.OnTransactionComplete();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
