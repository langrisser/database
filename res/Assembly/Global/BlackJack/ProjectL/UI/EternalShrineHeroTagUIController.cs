﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EternalShrineHeroTagUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EternalShrineHeroTagUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    private ConfigDataHeroTagInfo m_heroTagInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroTagInfo(ConfigDataHeroTagInfo heroTagInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataHeroTagInfo GetHeroTagInfo()
    {
      return this.m_heroTagInfo;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
    }

    public event Action<EternalShrineHeroTagUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
