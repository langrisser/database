﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LoginUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using PD.SDK;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class LoginUITask : LoginUITaskBase
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private Coroutine m_loginAgentCoroutine;
    private LoginUIController m_loginUIController;
    private ServerListUIController m_serverListUIController;
    private DataNoticeUIController m_dataNoticeUIController;
    private DataNoticeDisagreeUIController m_dataNoticeDisagreeUIController;
    private CreateCharacterUIController m_createCharaterUIController;
    private LoginCommonUIController m_loginCommonUIController;
    private int m_retryLoginByAuthTokenCount;
    private int m_retryLoginBySessionTokenCount;
    private List<string> m_assets;
    private List<string> m_randomNameHead;
    private List<string> m_randomNameMiddle;
    private List<string> m_randomNameTail;
    private System.Random m_randomNameRandom;
    private ClientConfigDataLoader m_configDataLoader;
    private static bool s_isGlobalInitialized;
    private static string s_localConfigPath;
    private static string m_loadConfigFailedMessage;
    private List<UITaskBase.LayerDesc> m_curLoadingLayers;
    private static string m_serverListText;
    private static List<LoginUITask.ServerInfo> m_serverlist;
    private static bool m_isRecentLoginServerIDInServerlist;
    private static int m_curSelectServerID;
    private static int m_recommendServerIndex;
    private static float m_totalRecommandWeight;
    private List<LoginUITask.ExistCharInfo> m_exsitCharsInfo;
    private bool m_isOpeningUI;
    private string m_curLoginAgnetUrl;
    public const string CustomParams = "menghuanmonizhan";
    public const int m_messageDuration = 3;
    private bool m_ignoreNetworkErrorOnce;
    private Dictionary<string, HashSet<string>> m_gmUserIDs;
    private static bool m_isGMUser;
    private bool m_isGettingSDKPlatformUserID;
    private string m_pdsdkLoginReturnData;
    private string m_pdsdkLoginReturnOpcode;
    private string m_pdsdkLoginReturnChannelID;
    private string m_pdsdkLoginReturnCustomParams;
    private static bool m_isAutoRelogin;
    private static bool m_isReturnToLoginAndSwitchUser;
    private static bool m_isReturnToLoginAndOnLoginSuccess;
    private static LoginSuccessMsg m_onSwitchUserSuccessMsg;
    private bool m_isNewRole;
    private Coroutine m_downloadServerListCoroutine;
    private bool m_isClearSessionFailed;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDialogBox(StringTableId strID)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Relogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ReturnToLoginAndSwitchUser()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void InitializeGlobals()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void InitLocalConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void LogSystemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public static List<LoginUITask.ServerInfo> ServerList
    {
      get
      {
        return LoginUITask.m_serverlist;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LoginUITask.ServerInfo GetServerInfoByBornChannelID(int bornChannelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override List<string> CollectAllDynamicResForLoad()
    {
      return this.m_assets;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostOnLoadAllResCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
    }

    private static bool ShouldUsePDSDK
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCoroutineKeepUpdatingServerList()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ClearLocalPushNotifications()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetLocalPushNotifications()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static DateTime GetLocalPushNotificationTime(ConfigDataDailyPushNotification cfg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static List<LoginUITask.ServerInfo> GetRecentLoginServerList()
    {
      // ISSUE: unable to decompile the method.
    }

    private void DataNoticeUIController_OnClosed()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DataNoticeUIController_OnAgreeButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DataNoticeUIController_OnPrivacyWebLinkButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    private void DataNoticeUIController_OnDisagreeButtonClicked()
    {
      this.ShowDataNoticeUI(false);
      this.ShowDataNoticeDisagreeUI(true);
    }

    private void DataNoticeDisagreeUIController_OnExitButtonClicked()
    {
      Application.Quit();
    }

    private void DataNoticeDisagreeUIController_OnBackButtonClicked()
    {
      this.ShowDataNoticeDisagreeUI(false);
      this.ShowDataNoticeUI(true);
    }

    private void DataNoticeDisagreeUIController_OnClosed()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ServerListUIController_OnServerListClosed(int selectedServerID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnTick()
    {
      base.OnTick();
      UIUtility.CheckLongFrame();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DeleteExpiredLogFiles(string logFolder, int days)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator CheckClientVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopEntryUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    public static int RecommendServerIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LoginUITask.ServerInfo GetCurrentSelectServerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DownloadServerListFile()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DownloadAnnounceFile(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DownloadGMUserIDs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ParseGMUserIDsText(string text)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsGMUser
    {
      get
      {
        return LoginUITask.m_isGMUser;
      }
    }

    public void UpdateLoginPCClientButton()
    {
    }

    private bool IsSdkIDCanLoginPCClient(string sdkChannelID)
    {
      return false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGMUserFlag(LoginUITask.ServerInfo si = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateLocalConfigGMUser()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator GetSDKPlatformUserID(LoginUITask.ServerInfo si)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReqExistChars()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitingNet(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void LaunchEnterGameUIWithGameSettingTokenAndServer()
    {
      this.LaunchEnterGameUI();
    }

    protected override void LaunchEnterGameUIWithUIInputAccountAndServer()
    {
      this.LaunchEnterGameUI();
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator DownloadGameServerLoginAnnouncement(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void ShowGameServerLoginAnnouncementUI()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ShowEnterGameUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator StartLoginAgentLogin(Action<int, string, string> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnLoginByAuthTokenAck(int ret, string sessionToken, bool needRedirect)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayEnableInput(bool isEnable, float delaySeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(StringTableId id, int time = 0, bool isOverride = true)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnLoginBySessionTokenAck(int ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator LoginBySessionTokenAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void StartPlayerInfoInitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string GetStartGameOrCreateRoleJsonString(string rolenameProperty)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string PDSDKStartGameJsonString
    {
      get
      {
        return LoginUITask.GetStartGameOrCreateRoleJsonString("GameName");
      }
    }

    public static string PDSDKGameRoleJsonString
    {
      get
      {
        return LoginUITask.GetStartGameOrCreateRoleJsonString("RoleName");
      }
    }

    private void PlayerContext_OnGameServerNetworkError()
    {
      LoginUITask.NetWorkTransactionTask_OnReLoginBySession((Action) null);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void LauncheMainUI()
    {
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator Co_UnloadAssetsAndStartPreloadUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnWaitingMsgAckOutTime()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnGameServerConnected()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnGameServerNetworkError(int err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendLanguageToServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCreateCharacterUI()
    {
      // ISSUE: unable to decompile the method.
    }

    private bool IsEuropeanUnion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCreateCharacterReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveSessionTokenCache(string sessionToken)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetSessionTokenCache()
    {
      // ISSUE: unable to decompile the method.
    }

    private string SessionTokenCacheFileName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearSessionTokenCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsSessionTokenValid(string sessionToken)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveLoginInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CreateRandomName()
    {
      // ISSUE: unable to decompile the method.
    }

    private void EnableInput(bool isEnable)
    {
      this.m_loginCommonUIController.EnableInput(isEnable);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnLoginPCClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSDK_OnQRLoginSuccess(string msg)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSDK_OnQRLoginFailed(string msg)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSDK_OnQRLoginCancel(string msg)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSK_OnLoginFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDSDK_OnInitFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDSDK_OnInitSucess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearSDKUserIDOfAllServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PDSDK_OnLogoutSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PDSDK_OnSwitchUserSuccess(LoginSuccessMsg msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSDK_OnLoginSuccess(LoginSuccessMsg msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ParseLoginAgentAck(
      string ackText,
      ref string status,
      ref string platformName,
      ref string userId,
      ref string token,
      ref string error)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator LoginAgent(
      string svrUrl,
      string data,
      string opcode,
      string channel_id,
      string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPlatformInfoToServerList(
      string loginAgentUrl,
      string platformName,
      string platformUserID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnOpenUserCenter()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator SwitchUserAccount()
    {
      // ISSUE: unable to decompile the method.
    }

    private void LoginUIController_OnAccountTextChanged(string text)
    {
      this.UpdateGMUserFlag((LoginUITask.ServerInfo) null);
    }

    private void LoginUIController_OnSaveServerConfig()
    {
      this.SaveLoginInfo();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnCloseAnnouncePanel()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnOpenAnnouncePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void LoginUIController_OnSelectServerClick()
    {
      this.ShowServerListUI(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowServerListUI(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDataNoticeUI(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDataNoticeDisagreeUI(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string SessionAccountInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator KeepUpdatingServerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCharacterUIController_OnCreate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCharacterUIController_OnAutoName()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckServerMaitainState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void NetWorkTransactionTask_OnReLoginBySession(Action action)
    {
      // ISSUE: unable to decompile the method.
    }

    public static LoginUITask FindAInstance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void NetWorkTransactionTask_ReturnToLoginUI(bool isDataDirty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void ParseServerListText(string serverListText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateServerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ParseServerOpenDateTime(string dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void NetWorkTransactionTask_OnEventShowUIWaiting(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static LoginUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [CustomLuaClass]
    public class ExistCharInfo
    {
      public string m_roleListURL;
      public int m_channelId;
      public int m_playerLevel;
      public int m_headIcon;
      public int m_lastLoginHours;
      public string m_charName;

      [MethodImpl((MethodImplOptions) 32768)]
      public ExistCharInfo(
        string roleListURL,
        int channelId,
        int playerId,
        int headIcon,
        int lastLoginHours,
        string charName)
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [CustomLuaClass]
    public class ServerInfo
    {
      public LoginUITask.ServerInfo.State m_state = LoginUITask.ServerInfo.State.Normal;
      public int m_id;
      public string m_name;
      public bool m_isNew;
      public string m_ip;
      public string m_domain;
      public int m_port;
      public string m_loginAgentUrl;
      public int m_channelId;
      public int m_bornChannelId;
      public float m_recommendWeight;
      public int m_initialIndex;
      public string m_sdkPlatformName;
      public string m_sdkPlatformUserID;
      public string m_roleListURL;
      public int m_realmID;
      public bool m_isAppleReview;
      public string m_serverOpenDateTime;
      public bool m_isRefuseNewPlayer;
      public string m_areaName;

      public ServerInfo(
        int initialIndex,
        int id,
        string name,
        LoginUITask.ServerInfo.State state,
        bool isNew,
        string ip,
        string domain,
        int port,
        string loginAgentUrl,
        int channelId,
        int bornChannelId,
        float recommendWeight,
        string roleListURL,
        int realmID,
        bool isAppleReview,
        string serverOpenDateTime,
        bool isRefuseNewPlayer,
        string areaName)
      {
        this.m_initialIndex = initialIndex;
        this.m_id = id;
        this.m_name = name;
        this.m_state = state;
        this.m_isNew = isNew;
        this.m_ip = ip;
        this.m_domain = domain;
        this.m_port = port;
        this.m_loginAgentUrl = loginAgentUrl;
        this.m_channelId = channelId;
        this.m_bornChannelId = bornChannelId;
        this.m_recommendWeight = recommendWeight;
        this.m_roleListURL = roleListURL.ToLower();
        this.m_realmID = realmID;
        this.m_isAppleReview = isAppleReview;
        this.m_serverOpenDateTime = serverOpenDateTime;
        this.m_isRefuseNewPlayer = isRefuseNewPlayer;
        this.m_areaName = areaName;
      }

      public override string ToString()
      {
        return this.m_ip + (object) this.m_port + this.m_loginAgentUrl + (object) this.m_channelId + (object) this.m_bornChannelId;
      }

      public enum State
      {
        Hot = 1,
        Crowded = 2,
        Maintain = 3,
        Normal = 4,
      }
    }
  }
}
