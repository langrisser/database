﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SkillDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class SkillDesc : MonoBehaviour
  {
    private Image m_icon;
    private Text m_title;
    private Text m_desc;
    private Text m_cd;
    private Text m_range;
    private Text m_distance;
    [DoNotToLua]
    private SkillDesc.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_Awake_hotfix;
    private LuaFunction m_SetSkillConfigDataSkillInfo_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkill(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public SkillDesc.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private SkillDesc m_owner;

      public LuaExportHelper(SkillDesc owner)
      {
        this.m_owner = owner;
      }

      public Image m_icon
      {
        get
        {
          return this.m_owner.m_icon;
        }
        set
        {
          this.m_owner.m_icon = value;
        }
      }

      public Text m_title
      {
        get
        {
          return this.m_owner.m_title;
        }
        set
        {
          this.m_owner.m_title = value;
        }
      }

      public Text m_desc
      {
        get
        {
          return this.m_owner.m_desc;
        }
        set
        {
          this.m_owner.m_desc = value;
        }
      }

      public Text m_cd
      {
        get
        {
          return this.m_owner.m_cd;
        }
        set
        {
          this.m_owner.m_cd = value;
        }
      }

      public Text m_range
      {
        get
        {
          return this.m_owner.m_range;
        }
        set
        {
          this.m_owner.m_range = value;
        }
      }

      public Text m_distance
      {
        get
        {
          return this.m_owner.m_distance;
        }
        set
        {
          this.m_owner.m_distance = value;
        }
      }

      public void Awake()
      {
        this.m_owner.Awake();
      }
    }
  }
}
