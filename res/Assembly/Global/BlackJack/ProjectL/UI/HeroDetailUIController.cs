﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroDetailUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./Margin/LeftButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_leftButton;
    [AutoBind("./Margin/RightButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rightButton;
    [AutoBind("./Margin/FilterToggles/Info", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_infoToggle;
    [AutoBind("./Margin/FilterToggles/Job", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_jobToggle;
    [AutoBind("./Margin/FilterToggles/Job/Click/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobToggleClickRedMark;
    [AutoBind("./Margin/FilterToggles/Job/UnClick/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobToggleUnClickRedMark;
    [AutoBind("./Margin/FilterToggles/Job/JobUpButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobUpTip;
    [AutoBind("./Margin/FilterToggles/Soldier", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_soldierToggle;
    [AutoBind("./Margin/FilterToggles/Equip", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_equipToggle;
    [AutoBind("./Margin/FilterToggles/Equip/Click/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipToggleClickRedMark;
    [AutoBind("./Margin/FilterToggles/Equip/UnClick/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipToggleUnClickRedMark;
    [AutoBind("./Life", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_lifeToggle;
    [AutoBind("./Life/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lifeToggleClickRedMark;
    [AutoBind("./Margin/FilterToggles/Equip/EquipMask", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipMaskButton;
    [AutoBind("./Margin/JobTransferButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobTransferButton;
    [AutoBind("./Margin/JobTransferButton/RedIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobTransferButtonRedIcon;
    [AutoBind("./Margin/JobTransferButton/U_SummonButton_ Press", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobTransferButtonPressEffect;
    [AutoBind("./Margin/JobTransferButton/U_Button_ready", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobTransferButtonReadyEffect;
    private Hero m_hero;
    public int m_curHeroNum;
    public List<Hero> m_curHeroList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private HeroDetailUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_PassHeroInfoList`1Int32_hotfix;
    private LuaFunction m_UpdateViewInHeroDetail_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnLeftButtonClick_hotfix;
    private LuaFunction m_OnRightButtonClick_hotfix;
    private LuaFunction m_OnJobTransferButtonClick_hotfix;
    private LuaFunction m_OnJobTransferButtonClickEffect_hotfix;
    private LuaFunction m_OnMaskButtonForUserGuideClick_hotfix;
    private LuaFunction m_HeroInfoToogleIsOn_hotfix;
    private LuaFunction m_HeroJobToggleIsOn_hotfix;
    private LuaFunction m_HeroEquipToggleIsOn_hotfix;
    private LuaFunction m_SetToggleToInfo_hotfix;
    private LuaFunction m_SetToggleToJob_hotfix;
    private LuaFunction m_SetToggleToSoldier_hotfix;
    private LuaFunction m_SetToggleToEquip_hotfix;
    private LuaFunction m_SetToggleToLife_hotfix;
    private LuaFunction m_OnInfoToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnJobToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnSoldierToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnEquipToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnLifeToggleValueChangedBoolean_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnJobTransferAction_hotfix;
    private LuaFunction m_remove_EventOnJobTransferAction_hotfix;
    private LuaFunction m_add_EventOnSetDetailStateAction`1_hotfix;
    private LuaFunction m_remove_EventOnSetDetailStateAction`1_hotfix;
    private LuaFunction m_add_EventOnUpdateViewInListAndDetailAction`5_hotfix;
    private LuaFunction m_remove_EventOnUpdateViewInListAndDetailAction`5_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDetailUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PassHeroInfo(List<Hero> hList, int num)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInHeroDetail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLeftButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRightButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobTransferButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator OnJobTransferButtonClickEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMaskButtonForUserGuideClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HeroInfoToogleIsOn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HeroJobToggleIsOn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HeroEquipToggleIsOn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleToInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleToJob()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleToSoldier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleToEquip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleToLife()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CreateSpineGraphic(
      ref UISpineGraphic graphic,
      string assetName,
      GameObject parent,
      int direction,
      Vector2 offset,
      float scale,
      List<ReplaceAnim> anims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DestroySpineGraphic(ref UISpineGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLifeToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnJobTransfer
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnSetDetailState
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event HeroDetailUIController.Action<int, bool, bool, int, bool> EventOnUpdateViewInListAndDetail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroDetailUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnJobTransfer()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnJobTransfer()
    {
      this.EventOnJobTransfer = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSetDetailState(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSetDetailState(string obj)
    {
      this.EventOnSetDetailState = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnUpdateViewInListAndDetail(
      int arg1,
      bool arg2,
      bool arg3,
      int arg4,
      bool arg5)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnUpdateViewInListAndDetail(
      int arg1,
      bool arg2,
      bool arg3,
      int arg4,
      bool arg5)
    {
      this.EventOnUpdateViewInListAndDetail = (HeroDetailUIController.Action<int, bool, bool, int, bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public delegate void Action<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);

    public class LuaExportHelper
    {
      private HeroDetailUIController m_owner;

      public LuaExportHelper(HeroDetailUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnJobTransfer()
      {
        this.m_owner.__callDele_EventOnJobTransfer();
      }

      public void __clearDele_EventOnJobTransfer()
      {
        this.m_owner.__clearDele_EventOnJobTransfer();
      }

      public void __callDele_EventOnSetDetailState(string obj)
      {
        this.m_owner.__callDele_EventOnSetDetailState(obj);
      }

      public void __clearDele_EventOnSetDetailState(string obj)
      {
        this.m_owner.__clearDele_EventOnSetDetailState(obj);
      }

      public void __callDele_EventOnUpdateViewInListAndDetail(
        int arg1,
        bool arg2,
        bool arg3,
        int arg4,
        bool arg5)
      {
        this.m_owner.__callDele_EventOnUpdateViewInListAndDetail(arg1, arg2, arg3, arg4, arg5);
      }

      public void __clearDele_EventOnUpdateViewInListAndDetail(
        int arg1,
        bool arg2,
        bool arg3,
        int arg4,
        bool arg5)
      {
        this.m_owner.__clearDele_EventOnUpdateViewInListAndDetail(arg1, arg2, arg3, arg4, arg5);
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_leftButton
      {
        get
        {
          return this.m_owner.m_leftButton;
        }
        set
        {
          this.m_owner.m_leftButton = value;
        }
      }

      public Button m_rightButton
      {
        get
        {
          return this.m_owner.m_rightButton;
        }
        set
        {
          this.m_owner.m_rightButton = value;
        }
      }

      public Toggle m_infoToggle
      {
        get
        {
          return this.m_owner.m_infoToggle;
        }
        set
        {
          this.m_owner.m_infoToggle = value;
        }
      }

      public Toggle m_jobToggle
      {
        get
        {
          return this.m_owner.m_jobToggle;
        }
        set
        {
          this.m_owner.m_jobToggle = value;
        }
      }

      public GameObject m_jobToggleClickRedMark
      {
        get
        {
          return this.m_owner.m_jobToggleClickRedMark;
        }
        set
        {
          this.m_owner.m_jobToggleClickRedMark = value;
        }
      }

      public GameObject m_jobToggleUnClickRedMark
      {
        get
        {
          return this.m_owner.m_jobToggleUnClickRedMark;
        }
        set
        {
          this.m_owner.m_jobToggleUnClickRedMark = value;
        }
      }

      public GameObject m_jobUpTip
      {
        get
        {
          return this.m_owner.m_jobUpTip;
        }
        set
        {
          this.m_owner.m_jobUpTip = value;
        }
      }

      public Toggle m_soldierToggle
      {
        get
        {
          return this.m_owner.m_soldierToggle;
        }
        set
        {
          this.m_owner.m_soldierToggle = value;
        }
      }

      public Toggle m_equipToggle
      {
        get
        {
          return this.m_owner.m_equipToggle;
        }
        set
        {
          this.m_owner.m_equipToggle = value;
        }
      }

      public GameObject m_equipToggleClickRedMark
      {
        get
        {
          return this.m_owner.m_equipToggleClickRedMark;
        }
        set
        {
          this.m_owner.m_equipToggleClickRedMark = value;
        }
      }

      public GameObject m_equipToggleUnClickRedMark
      {
        get
        {
          return this.m_owner.m_equipToggleUnClickRedMark;
        }
        set
        {
          this.m_owner.m_equipToggleUnClickRedMark = value;
        }
      }

      public Toggle m_lifeToggle
      {
        get
        {
          return this.m_owner.m_lifeToggle;
        }
        set
        {
          this.m_owner.m_lifeToggle = value;
        }
      }

      public GameObject m_lifeToggleClickRedMark
      {
        get
        {
          return this.m_owner.m_lifeToggleClickRedMark;
        }
        set
        {
          this.m_owner.m_lifeToggleClickRedMark = value;
        }
      }

      public Button m_equipMaskButton
      {
        get
        {
          return this.m_owner.m_equipMaskButton;
        }
        set
        {
          this.m_owner.m_equipMaskButton = value;
        }
      }

      public Button m_jobTransferButton
      {
        get
        {
          return this.m_owner.m_jobTransferButton;
        }
        set
        {
          this.m_owner.m_jobTransferButton = value;
        }
      }

      public GameObject m_jobTransferButtonRedIcon
      {
        get
        {
          return this.m_owner.m_jobTransferButtonRedIcon;
        }
        set
        {
          this.m_owner.m_jobTransferButtonRedIcon = value;
        }
      }

      public GameObject m_jobTransferButtonPressEffect
      {
        get
        {
          return this.m_owner.m_jobTransferButtonPressEffect;
        }
        set
        {
          this.m_owner.m_jobTransferButtonPressEffect = value;
        }
      }

      public GameObject m_jobTransferButtonReadyEffect
      {
        get
        {
          return this.m_owner.m_jobTransferButtonReadyEffect;
        }
        set
        {
          this.m_owner.m_jobTransferButtonReadyEffect = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnLeftButtonClick()
      {
        this.m_owner.OnLeftButtonClick();
      }

      public void OnRightButtonClick()
      {
        this.m_owner.OnRightButtonClick();
      }

      public void OnJobTransferButtonClick()
      {
        this.m_owner.OnJobTransferButtonClick();
      }

      public IEnumerator OnJobTransferButtonClickEffect()
      {
        return this.m_owner.OnJobTransferButtonClickEffect();
      }

      public void OnMaskButtonForUserGuideClick()
      {
        this.m_owner.OnMaskButtonForUserGuideClick();
      }

      public void OnInfoToggleValueChanged(bool on)
      {
        this.m_owner.OnInfoToggleValueChanged(on);
      }

      public void OnJobToggleValueChanged(bool on)
      {
        this.m_owner.OnJobToggleValueChanged(on);
      }

      public void OnSoldierToggleValueChanged(bool on)
      {
        this.m_owner.OnSoldierToggleValueChanged(on);
      }

      public void OnEquipToggleValueChanged(bool on)
      {
        this.m_owner.OnEquipToggleValueChanged(on);
      }

      public void OnLifeToggleValueChanged(bool on)
      {
        this.m_owner.OnLifeToggleValueChanged(on);
      }
    }
  }
}
