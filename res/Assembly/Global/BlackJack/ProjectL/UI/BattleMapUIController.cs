﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleMapUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.Scene;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BattleMapUIController : UIControllerBase
  {
    private ClientBattle m_clientBattle;
    private GameObjectPool<RegionGridEffect> m_moveGridPool;
    private GameObjectPool<RegionGridEffect> m_attackGridPool;
    private GameObjectPool<RegionGridEffect> m_skillGridPool;
    private GameObjectPool<RegionGridEffect> m_teleportGridPool;
    private GameObjectPool<RegionGridEffect> m_fadeGridPool;
    private GameObjectPool m_dangerEdgeGrid0Pool;
    private GameObjectPool m_dangerEdgeGrid1Pool;
    private GameObjectPool m_dangerEdgeGrid1_1Pool;
    private GameObjectPool m_dangerEdgeGrid1_2Pool;
    private GameObjectPool m_dangerEdgeGrid1_3Pool;
    private GameObjectPool m_dangerEdgeGrid2Pool;
    private GameObjectPool m_dangerEdgeGrid2_1Pool;
    private GameObjectPool m_dangerEdgeGrid3Pool;
    private GameObjectPool m_dangerEdgeGrid4Pool;
    private GameObjectPool m_dangerEdgeGrid5Pool;
    private GameObjectPool m_dangerCornerGrid1Pool;
    private GameObjectPool m_dangerCornerGrid2Pool;
    private GameObjectPool m_dangerCornerGrid3Pool;
    private GameObjectPool m_dangerCornerGrid4Pool;
    private GameObjectPool m_dangerCornerGrid5Pool;
    private GameObjectPool m_reachGridPool;
    private GameObjectPool m_stagePosition0Pool;
    private GameObjectPool m_stagePosition1Pool;
    private GameObjectPool m_stagePosition2Pool;
    private GameObjectPool m_arrowHeadPool;
    private GameObjectPool m_arrowEndPool;
    private GameObjectPool m_arrowCornerPool;
    private GameObjectPool m_arrowLinePool;
    private bool m_isFadeIn;
    private float m_fadeCurTime;
    private float m_fadeDuration;
    private Material m_fadeMaterial;
    private Color m_fadeColor;
    private bool m_isZoomFadeIn;
    private float m_zoomFadeCurTime;
    private float m_zoomFadeDuration;
    [AutoBind("./Grid", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_gridGameObject;
    [AutoBind("./Fade", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_fadeGameObject;
    [AutoBind("./ZoomFade", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_zoomFadeGameObject;
    [AutoBind("./ZoomFade/Full", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_zoomFadeFullGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/MoveGrid", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_moveGridPrefab;
    [AutoBind("./Prefabs/AttackGrid", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_attackGridPrefab;
    [AutoBind("./Prefabs/SkillGrid", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillGridPrefab;
    [AutoBind("./Prefabs/TeleportGrid", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teleportGridPrefab;
    [AutoBind("./Prefabs/FadeGrid", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_fadeGridPrefab;
    [AutoBind("./Prefabs/ReachGrid", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_reachGridPrefab;
    [AutoBind("./Prefabs/ArrowHead", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arrowHeadPrefab;
    [AutoBind("./Prefabs/ArrowEnd", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arrowEndPrefab;
    [AutoBind("./Prefabs/ArrowCorner", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arrowCornerPrefab;
    [AutoBind("./Prefabs/ArrowLine", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arrowLinePrefab;
    [AutoBind("./Prefabs/DangerEdgeGrid0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerEdgeGrid0Prefab;
    [AutoBind("./Prefabs/DangerEdgeGrid1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerEdgeGrid1Prefab;
    [AutoBind("./Prefabs/DangerEdgeGrid1_1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerEdgeGrid1_1Prefab;
    [AutoBind("./Prefabs/DangerEdgeGrid1_2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerEdgeGrid1_2Prefab;
    [AutoBind("./Prefabs/DangerEdgeGrid1_3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerEdgeGrid1_3Prefab;
    [AutoBind("./Prefabs/DangerEdgeGrid2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerEdgeGrid2Prefab;
    [AutoBind("./Prefabs/DangerEdgeGrid2_1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerEdgeGrid2_1Prefab;
    [AutoBind("./Prefabs/DangerEdgeGrid3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerEdgeGrid3Prefab;
    [AutoBind("./Prefabs/DangerEdgeGrid4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerEdgeGrid4Prefab;
    [AutoBind("./Prefabs/DangerEdgeGrid5", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerEdgeGrid5Prefab;
    [AutoBind("./Prefabs/DangerCornerGrid1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerCornerGrid1Prefab;
    [AutoBind("./Prefabs/DangerCornerGrid2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerCornerGrid2Prefab;
    [AutoBind("./Prefabs/DangerCornerGrid3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerCornerGrid3Prefab;
    [AutoBind("./Prefabs/DangerCornerGrid4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerCornerGrid4Prefab;
    [AutoBind("./Prefabs/DangerCornerGrid5", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dangerCornerGrid5Prefab;

    private BattleMapUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientBattle clientBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AddChildPrefab(GameObject go, string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PrepareBattle()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattle()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateZoomFade(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRegion(GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDangerRegion(HashSet<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    private static bool IsInRegion(int x, int y, HashSet<GridPosition> region)
    {
      return region.Contains(new GridPosition(x, y));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideDangerRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowReachRegion(List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    public void HideReachRegion()
    {
      this.m_reachGridPool.Deactive();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMovePath(List<GridPosition> path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideMovePath()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowStagePosition(StagePositionType posType, GridPosition pos, bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideStagePositions()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayOnStageFx(StagePositionType posType, GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SkillFadeIn(float time, float alpha)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SkillFadeOut(float time)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideFade()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetFade(float a)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFade(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowZoomFade(GridPosition startPos, float fadeDuration, bool fadeIn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FullZoomFade()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideZoomFade()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsShowZoomFade()
    {
      return this.m_zoomFadeGameObject.activeSelf;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 StagePositionToWorldPosition(GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GridPositionToWorldPosition(GridPosition p, float z)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
