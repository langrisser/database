﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaDefendUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ArenaDefendUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./SaveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_saveTeamButton;
    [AutoBind("./Margin/ActionOrderButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_actionOrderButton;
    [AutoBind("./Margin/MapButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_mapButton;
    [AutoBind("./Margin/DefendRuleButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_defendRuleButton;
    [AutoBind("./Title/ActorCount/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_stageActorCountText;
    [AutoBind("./ActorList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_actorListScrollRect;
    [AutoBind("./BattlePower", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battlePowerUIStateController;
    [AutoBind("./BattlePower/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battlePowerText;
    [AutoBind("./Panels", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_panelGameObject;
    [AutoBind("./Panels/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_panelBackgroundButton;
    [AutoBind("./Panels/ActionOrder", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actionOrderPanelUIStateController;
    [AutoBind("./Panels/ActionOrder", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actionOrderPanelGameObject;
    [AutoBind("./Panels/ActionOrder/Slots", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actionOrdersGameObject;
    [AutoBind("./Panels/ActionOrder/OkButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_actionOrderOkButton;
    [AutoBind("./Panels/Map", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_mapPanelUIStateController;
    [AutoBind("./Panels/Map", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapPanelGameObject;
    [AutoBind("./Panels/Map/Maps", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapsGameObject;
    [AutoBind("./Panels/Map/OkButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_mapOkButton;
    [AutoBind("./Panels/DefendRule", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_defendRulePanelUIStateController;
    [AutoBind("./Panels/DefendRule", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_defendRulePanelGameObject;
    [AutoBind("./Panels/DefendRule/Rules", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_defendRulesGameObject;
    [AutoBind("./Panels/DefendRule/OkButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_defendRuleOkButton;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/HeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroButtonPrefab;
    [AutoBind("./Prefabs/ActionOrderButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actionOrderButtonPrefab;
    private Camera m_camera;
    private bool m_isIgnorePointerClick;
    private ArenaDefendActor m_pointerDownStageActor;
    private int m_stageActorCountMax;
    private bool m_isStageActorChanged;
    private List<HeroDragButton> m_heroButtons;
    private List<ArenaDefendActor> m_stageActors;
    private List<GridPosition> m_stagePositions;
    private List<int> m_stageDirs;
    private ArenaActionOrderButton[] m_actionOrderButtons;
    private ArenaDefendMapToggle[] m_defendMapToggles;
    private ArenaDefendRuleToggle[] m_defendRuleToggles;
    private HeroDragButton m_draggingHeroButton;
    private ArenaActionOrderButton m_draggingActionOrderButton;
    private int m_draggingActionOrderButtonIndex;
    private ArenaDefendBattle m_arenaDefendBattle;
    private int m_battlePowerValue;
    private Coroutine m_setBattlePowerValueCoroutine;

    [MethodImpl((MethodImplOptions) 32768)]
    private ArenaDefendUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ArenaDefendBattle battle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnApplicationPause(bool isPause)
    {
      if (!isPause)
        return;
      this.CancelDragging();
    }

    private void OnApplicationFocus(bool focus)
    {
      if (focus)
        return;
      this.CancelDragging();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CancelDragging()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroActionOrders(List<BattleHero> heros)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroActionOrderIndex(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMaps(List<ConfigDataArenaBattleInfo> battleInfos, int currentIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefendRules(
      List<ConfigDataArenaDefendRuleInfo> defendRuleInfos,
      int currentIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattlePower(int battlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetBattlePowerValue(int newValue, int oldValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckStageAtorChange()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetStageActorCountMax(int c)
    {
      this.m_stageActorCountMax = c;
      this.UpdateStageActorCount();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStageActorCount()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearStagePositions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddStagePosition(GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<GridPosition> GetStagePositions()
    {
      return this.m_stagePositions;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetStageDirection(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private HeroDragButton CreateHeroButton(BattleHero hero, Transform parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendActor HeroOnStage(BattleHero hero, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActorOffStage(ArenaDefendActor sa)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActorOnStageMove(ArenaDefendActor sa, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActorOnStageExchange(ArenaDefendActor sa1, ArenaDefendActor sa2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearStageActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendActor GetStageActor(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<ArenaDefendActor> GetStageActors()
    {
      return this.m_stageActors;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateStageActor(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroDragButton_OnBeginDrag(HeroDragButton b, PointerEventData eventData)
    {
    }

    public void HeroDragButton_OnEndDrag(HeroDragButton b, PointerEventData eventData)
    {
      this.DestroyDragginHeroButton();
    }

    public void HeroDragButton_OnDrag(PointerEventData eventData)
    {
      this.MoveDraggingHeroButton(eventData.position);
    }

    public void HeroDragButton_OnDrop(PointerEventData eventData)
    {
      this.DropHeroButton(eventData.position);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroDragButton_OnClick(HeroDragButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearPointerDownStageActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneEndDrag(PointerEventData eventData)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateDraggingHeroButton(BattleHero hero)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyDragginHeroButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveDraggingHeroButton(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DropHeroButton(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowActionOrderPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMapPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDefendRulePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ArenaActionOrderButton CreateActionOrderButton(
      BattleHero hero,
      Transform parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateDraggingActionOrderButton(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyDragginActionOrderButton()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveDraggingActionOrderButton(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DropDraggingActionOrderButton(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSaveTeamButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowActionOrderButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowMapButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowDefendRuleButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPanelBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActionOrderOkButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void ActionOrderButton_OnClick(ArenaActionOrderButton b)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionOrderButton_OnBeginDrag(ArenaActionOrderButton b, PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    private void ActionOrderButton_OnEndDrag(ArenaActionOrderButton b, PointerEventData eventData)
    {
      this.DestroyDragginActionOrderButton();
    }

    private void ActionOrderButton_OnDrag(PointerEventData eventData)
    {
      this.MoveDraggingActionOrderButton(eventData.position);
    }

    private void ActionOrderButton_OnDrop(PointerEventData eventData)
    {
      this.DropDraggingActionOrderButton(eventData.position);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMapOkButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDefendRuleOkButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnSave
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnShowActionOrderPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnShowMapPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnShowDefendRulePanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnConfirmActionOrder
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<int> EventOnConfirmMap
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<int> EventOnConfirmDefendRule
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<BattleHero> EventOnShowMyActorInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHideActorInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnStageActorChange
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<BattleHero, GridPosition> EventOnHeroOnStage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<ArenaDefendActor> EventOnActorOffStage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<ArenaDefendActor, GridPosition> EventOnStageActorMove
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ArenaDefendActor, ArenaDefendActor> EventOnStageActorSwap
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerDown
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
