﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RankingListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RankingListItemUIController : UIControllerBase
  {
    public ScrollItemBaseUIController ScrollItemBaseUICtrl;
    [AutoBind("./Rank/RankValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text RankValueText;
    [AutoBind("./Rank/RankValueImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image RankValueImage;
    [AutoBind("./HeadIconImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image HeadIconImage;
    [AutoBind("./LevelValue", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerLevelText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerNameText;
    [AutoBind("./DetailGroup", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController DetailGroupStateCtrl;
    [AutoBind("./DetailGroup/TopHero/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text TopHeroScoreValueText;
    [AutoBind("./DetailGroup/AllHero/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text AllHeroScoreValueText;
    [AutoBind("./DetailGroup/ChampionHero/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    public Text ChampionHeroScoreTitleText;
    [AutoBind("./DetailGroup/ChampionHero/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text ChampionHeroScoreValueText;
    [AutoBind("./DetailGroup/RiftChapterStar/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text RiftChapterStarScoreValueText;
    [AutoBind("./DetailGroup/RiftAchievement/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text RiftAchievementScoreValueText;
    [AutoBind("./DetailGroup/ClimbTower/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text ClimbTowerScoreValueText;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Init()
    {
      this.ScrollItemBaseUICtrl.Init((UIControllerBase) this, true);
    }

    public void RegisterItemClickEvent(Action<UIControllerBase> action)
    {
      this.ScrollItemBaseUICtrl.EventOnUIItemClick = action;
    }

    public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
    {
      this.ScrollItemBaseUICtrl.EventOnUIItemNeedFill = action;
    }

    public int GetItemIndex()
    {
      return this.ScrollItemBaseUICtrl.ItemIndex;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItemInfo(
      RankingListType rankType,
      int headIconId,
      int rankLevel,
      int lv,
      string playerName,
      int score,
      string heroName = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Sprite GetRankLevelSprite(int rankLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateDetailInfo(RankingListType rankType, string heroName, int score)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
