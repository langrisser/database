﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SoldierItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class SoldierItemUIController : UIControllerBase
  {
    [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./AttackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackButton;
    [AutoBind("./Attacking", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_attackingObj;
    [AutoBind("./NotGet", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dontGetObj;
    [AutoBind("./Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGrapgic;
    [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_frameImage;
    [AutoBind("./SkinIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skinIconImage;
    private bool m_isSoldierGet;
    private int m_soldierSkinId;
    private UISpineGraphic m_graphic;
    public ConfigDataJobConnectionInfo JobConnectionInfo;
    public ConfigDataSoldierInfo SoldierInfo;
    [DoNotToLua]
    private SoldierItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitSoldierItemConfigDataSoldierInfoConfigDataJobConnectionInfoBooleanInt32_hotfix;
    private LuaFunction m_OnSoldierItemClick_hotfix;
    private LuaFunction m_OnAttackButtonClick_hotfix;
    private LuaFunction m_SetAttackingPanelActiveBoolean_hotfix;
    private LuaFunction m_SetAttackButtonActiveBoolean_hotfix;
    private LuaFunction m_SetFrameImgActiveBoolean_hotfix;
    private LuaFunction m_CreateSpineGraphicConfigDataSoldierInfoBooleanInt32_hotfix;
    private LuaFunction m_DestroySpineGraphic_hotfix;
    private LuaFunction m_OnDestroy_hotfix;
    private LuaFunction m_add_EventOnSoldierItemClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnSoldierItemClickAction`1_hotfix;
    private LuaFunction m_add_EventOnAttackButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnAttackButtonClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierItem(
      ConfigDataSoldierInfo si,
      ConfigDataJobConnectionInfo jci,
      bool isSoliderGet,
      int soldierSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAttackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAttackingPanelActive(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAttackButtonActive(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFrameImgActive(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(
      ConfigDataSoldierInfo soldierInfo,
      bool isGet,
      int soldierSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnDestroy()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<SoldierItemUIController> EventOnSoldierItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataSoldierInfo> EventOnAttackButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public SoldierItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      base.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSoldierItemClick(SoldierItemUIController obj)
    {
    }

    private void __clearDele_EventOnSoldierItemClick(SoldierItemUIController obj)
    {
      this.EventOnSoldierItemClick = (Action<SoldierItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAttackButtonClick(ConfigDataSoldierInfo obj)
    {
    }

    private void __clearDele_EventOnAttackButtonClick(ConfigDataSoldierInfo obj)
    {
      this.EventOnAttackButtonClick = (Action<ConfigDataSoldierInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private SoldierItemUIController m_owner;

      public LuaExportHelper(SoldierItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnSoldierItemClick(SoldierItemUIController obj)
      {
        this.m_owner.__callDele_EventOnSoldierItemClick(obj);
      }

      public void __clearDele_EventOnSoldierItemClick(SoldierItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnSoldierItemClick(obj);
      }

      public void __callDele_EventOnAttackButtonClick(ConfigDataSoldierInfo obj)
      {
        this.m_owner.__callDele_EventOnAttackButtonClick(obj);
      }

      public void __clearDele_EventOnAttackButtonClick(ConfigDataSoldierInfo obj)
      {
        this.m_owner.__clearDele_EventOnAttackButtonClick(obj);
      }

      public Text m_soldierNameText
      {
        get
        {
          return this.m_owner.m_soldierNameText;
        }
        set
        {
          this.m_owner.m_soldierNameText = value;
        }
      }

      public Button m_attackButton
      {
        get
        {
          return this.m_owner.m_attackButton;
        }
        set
        {
          this.m_owner.m_attackButton = value;
        }
      }

      public GameObject m_attackingObj
      {
        get
        {
          return this.m_owner.m_attackingObj;
        }
        set
        {
          this.m_owner.m_attackingObj = value;
        }
      }

      public GameObject m_dontGetObj
      {
        get
        {
          return this.m_owner.m_dontGetObj;
        }
        set
        {
          this.m_owner.m_dontGetObj = value;
        }
      }

      public GameObject m_soldierGrapgic
      {
        get
        {
          return this.m_owner.m_soldierGrapgic;
        }
        set
        {
          this.m_owner.m_soldierGrapgic = value;
        }
      }

      public GameObject m_frameImage
      {
        get
        {
          return this.m_owner.m_frameImage;
        }
        set
        {
          this.m_owner.m_frameImage = value;
        }
      }

      public GameObject m_skinIconImage
      {
        get
        {
          return this.m_owner.m_skinIconImage;
        }
        set
        {
          this.m_owner.m_skinIconImage = value;
        }
      }

      public bool m_isSoldierGet
      {
        get
        {
          return this.m_owner.m_isSoldierGet;
        }
        set
        {
          this.m_owner.m_isSoldierGet = value;
        }
      }

      public int m_soldierSkinId
      {
        get
        {
          return this.m_owner.m_soldierSkinId;
        }
        set
        {
          this.m_owner.m_soldierSkinId = value;
        }
      }

      public UISpineGraphic m_graphic
      {
        get
        {
          return this.m_owner.m_graphic;
        }
        set
        {
          this.m_owner.m_graphic = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnSoldierItemClick()
      {
        this.m_owner.OnSoldierItemClick();
      }

      public void OnAttackButtonClick()
      {
        this.m_owner.OnAttackButtonClick();
      }

      public void CreateSpineGraphic(
        ConfigDataSoldierInfo soldierInfo,
        bool isGet,
        int soldierSkinId)
      {
        this.m_owner.CreateSpineGraphic(soldierInfo, isGet, soldierSkinId);
      }

      public void DestroySpineGraphic()
      {
        this.m_owner.DestroySpineGraphic();
      }

      public void OnDestroy()
      {
        this.m_owner.OnDestroy();
      }
    }
  }
}
