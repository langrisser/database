﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ArenaUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ArenaUIController m_arenaUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private ArenaDefendUITask m_arenaDefendUITask;
    private ChestUITask m_chestUITask;
    private List<ConfigDataRealTimePVPDanInfo> m_realTimePVPDans;
    private List<ArenaOpponent> m_arenaOpponents;
    private List<ArenaBattleReport> m_offlineArenaBattleReport;
    private List<RealTimePVPBattleReport> m_onlineArenaBattleReport;
    private List<RealTimePVPBattleReport> m_onlineArenaPromotionBattleReport;
    private OfflineArenaPanelType m_offlinePanelType;
    private OnlineArenaPanelType m_onlinePanelType;
    private ArenaUIType m_arenaUIType;
    private int m_curArenaOpponentIndex;
    private ArenaBattleReport m_curArenaBattleReport;
    private bool m_canFlushOfflineTopRankPlayers;
    private bool m_canFlushOnlineGlobalTopRankPlayers;
    private bool m_canFlushOnlineLocalTopRankPlayers;
    private bool m_isSwitchingOfflineOnline;
    private int m_nowSeconds;
    private DateTime m_matchingUIBeginTime;
    private DateTime m_matchingReqSendTime;
    private BattleMode m_matchingBattleMode;
    private bool m_isNeedCheckOnline;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool OnStart(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnStart(intent);
    }

    protected override bool OnResume(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnResume(intent);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPlayerHeadAssets(int playerHeadIconId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeroModelAssets(List<BattleHero> heros)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void InitAllUIControllers()
    {
      base.InitAllUIControllers();
      this.InitArenaUIController();
    }

    protected override void ClearAllContextAndRes()
    {
      base.ClearAllContextAndRes();
      this.UninitArenaUIController();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitArenaUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitArenaUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnMemoryWarning()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    private static int CompareHeroBattlePower(Hero h1, Hero h2)
    {
      return h2.BattlePower - h1.BattlePower;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaBattleReportBasicDataGetNetTask(ArenaUIType arenaUIType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddCrystal()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnDefend()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnShowBuyArenaTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnBuyArenaTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnSwitchOnlineOffline()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChestUITask_OnStop(Task task)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChestUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool> EventOnStartArenaBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<ArenaBattleReport> EventOnStartArenaBattleReplay
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<RealTimePVPBattleReport> EventOnStartRealTimePVPBattleReplay
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectOfflineArenaAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateOfflineArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateOfflineArenaOpponentRefreshTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareArenaBattleReportsByCreateTime(
      ArenaBattleReport r0,
      ArenaBattleReport r1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareRealTimePVPBattleReportsByCreateTime(
      RealTimePVPBattleReport r0,
      RealTimePVPBattleReport r1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsBattleReportAttackerGiveup(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlushOfflineTopRankPlayers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutoGetOfflineVictoryPointReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnShowOfflinePanel(OfflineArenaPanelType panelType)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnGainOfflineVictoryPointsReward(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnShowOfflineArenaOpponent(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnAttackOfflineArenaOpponent(bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnShowRevengeOfflineArenaOpponent(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnRevengeOfflineArenaOpponent(bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnOfflineBattleReportReplay(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnArenaFlushOpponentsNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectOnlineArenaAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateOnlineArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateOnlineMatchingTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateLadderMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsBattleReportPlayerGiveup(
      RealTimePVPBattleReport battleReport,
      out int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlushOnlineTopRankPlayers(bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRealTimePVPGetInfoNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRealTimePVPGetTopPlayersNetTask(bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutoGetOnlineWeekWinReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRealTimePVPWaitingForOpponentNetTask(BattleMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMatchiing(BattleMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnShowOnlinePanel(OnlineArenaPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnGainOnlineWeekWinReward(int bonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnOnlineBattleReportReplay(RealTimePVPBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void ArenaUIController_OnLadderChallenge()
    {
      this.StartMatchiing(BattleMode.Default);
    }

    private void ArenaUIController_OnCasualChallenge()
    {
      this.StartMatchiing(BattleMode.Friendly);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnMatchingCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnRealTimePVPMatchupNtf(int result)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
