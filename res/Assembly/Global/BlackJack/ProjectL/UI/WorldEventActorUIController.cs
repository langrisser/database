﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.WorldEventActorUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class WorldEventActorUIController : UIControllerBase, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IEventSystemHandler
  {
    [AutoBind("./TestText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_testText;
    [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_iconGameObject;
    [AutoBind("./WorldHitImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_hitGameObject;
    private ConfigDataEventInfo m_eventInfo;
    private bool m_isPointerDown;
    private bool m_ignoreClick;

    private WorldEventActorUIController()
    {
    }

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEvent(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEnergy(int energy)
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataEventInfo GetEventInfo()
    {
      return this.m_eventInfo;
    }

    public void SetCanClick(bool canClick)
    {
      this.m_hitGameObject.SetActive(canClick);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IgnoreClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPointerDown
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnPointerUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
