﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RewardGoodsDescUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RewardGoodsDescUITask : UITask
  {
    private const string ParamKey_Type = "Type";
    private const string ParamKey_Id = "Id";
    private const string ParamKey_Ctrl = "Ctrl";
    private const string ParamKey_AlignType = "AlignType";
    private const string ParamKey_GameObjectForPosScale = "GameObjectForPosScale";
    private const string ParamKey_IsNeedAutoClose = "IsNeedAutoClose ";
    private PrefabControllerBase m_controller;
    private GoodsType m_goodsType;
    private int m_goodsId;
    private int m_alignType;
    private GameObject m_gameObjectForPosCalc;
    private bool m_isNeedAutoClose;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private RewardGoodsDescUIController m_rewardGoodsDescUIController;

    [MethodImpl((MethodImplOptions) 32768)]
    public RewardGoodsDescUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RewardGoodsDescUITask StartUITask(
      PrefabControllerBase ctrl,
      GoodsType goodsType,
      int goodsId,
      int alignType = 0,
      GameObject gameObjectForPosCalc = null,
      bool isNeedAutoClose = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
    }

    protected override bool OnStart(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnStart(intent);
    }

    protected override bool OnResume(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnResume(intent);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RewardGoodsDescUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetData()
    {
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
