﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AppScoreUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class AppScoreUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateCtrl;
    [AutoBind("./Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scorePanelCloseButton;
    [AutoBind("./Detail/ScoreButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scorePanelScoreButton;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayOpenTween()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScorePanelCloseButtonClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScorePanelScoreButtonClick()
    {
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnGoScore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
