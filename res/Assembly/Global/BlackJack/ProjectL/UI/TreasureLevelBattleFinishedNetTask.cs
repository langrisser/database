﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TreasureLevelBattleFinishedNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TreasureLevelBattleFinishedNetTask : UINetTask
  {
    private int m_treasureLevelID;
    private ProBattleReport m_battleReport;

    [MethodImpl((MethodImplOptions) 32768)]
    public TreasureLevelBattleFinishedNetTask(int treasureLevelID, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnTreasureLevelBattleFinishedAck(int result, bool isWin, BattleReward reward)
    {
    }

    public bool IsWin { private set; get; }

    public BattleReward Reward { private set; get; }
  }
}
