﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroListUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroListUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private HeroListUIController m_heroListUIController;
    private HeroDetailUIController m_heroDetailUIController;
    private HeroDetailJobUIController m_heroDetailJobUIController;
    private HeroDetailInfoUIController m_heroDetailInfoUIController;
    private HeroDetailAddExpUIController m_heroDetailAddExpUIController;
    private HeroDetailSoldierUIController m_heroDetailSoldierUIController;
    private HeroDetailLifeUIController m_heroDetailLifeUIController;
    private HeroDetailEquipmentUIController m_heroDetailEquipmentUIController;
    private HeroDetailSelectSkillUIController m_heroDetailSelectSkillUIController;
    private HeroJobTransferUITask m_heroJobTransferUITask;
    private HeroSkinChangeUITask m_heroCharChangeTask;
    private StoreSoldierSkinDetailUITask m_storeSoldierSkinDetailUITask;
    public const string ListMode = "List";
    public const string DetailInfoMode = "DetailInfo";
    public const string DetailJobMode = "DetailJob";
    public const string DetailSoldierMode = "DetailSoldier";
    public const string DetailEquipmentMode = "DetailEquipment";
    public const string DetailLifeMode = "DetailLife";
    public const string DetailSelectSkillMode = "DetailSelectSkill";
    public const string DetailAddExpMode = "DetailAddExp";
    private string m_stateName;
    private string m_lastMode;
    private string m_curMode;
    private List<Hero> m_lockedList;
    private List<Hero> m_curHeroList;
    private List<Hero> m_unlockedList;
    private int m_curHeroPos;
    private int m_curLayerDescIndex;
    private int m_lastHeroId;
    private bool m_isUnlockHero;
    private bool m_isDetailLayerOpen;
    private bool m_isNeedRefreshHeroList;
    private HeroListUIController.HeroSortType m_curHeroSortType;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private HeroListUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_SaveUIStateToIntent_hotfix;
    private LuaFunction m_GetUIStateFromIntentUIIntent_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_UpdateHeroListData_hotfix;
    private LuaFunction m_UpdateHeroCardData_hotfix;
    private LuaFunction m_UpdateCurrentHeroData_hotfix;
    private LuaFunction m_UpdateSoldierModeData_hotfix;
    private LuaFunction m_UpdateJobModeData_hotfix;
    private LuaFunction m_HeroListItemCompareHeroHero_hotfix;
    private LuaFunction m_IsNeedLoadStaticRes_hotfix;
    private LuaFunction m_CollectAllStaticResDescForLoad_hotfix;
    private LuaFunction m_CreateLayerDescByIndexInt32_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_UpdateViewOnHeroChangedInt32BooleanBooleanInt32Boolean_hotfix;
    private LuaFunction m_StartUpdatePiplineInHeroListTask_hotfix;
    private LuaFunction m_OnMemoryWarning_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_PushLayerByCurState_hotfix;
    private LuaFunction m_PushLayerByIndexInt32_hotfix;
    private LuaFunction m_CheckConflictBetweenLayerInt32_hotfix;
    private LuaFunction m_HeroDetailUIController_OnSetDetailStateString_hotfix;
    private LuaFunction m_SetTabCommonUIStateByNameStringString_hotfix;
    private LuaFunction m_HeroListUIController_SetHeroListList`1List`1_hotfix;
    private LuaFunction m_HeroListUIController_OnReturn_hotfix;
    private LuaFunction m_HeroListUIController_OnAddHeroString_hotfix;
    private LuaFunction m_HeroListUIController_OnHeroComposeInt32Action_hotfix;
    private LuaFunction m_HeroDetailLifeUIController_OnVoiceItemClickInt32_hotfix;
    private LuaFunction m_HeroDetailSelectSkillUIController_OnHeroSkillsSelectInt32List`1Boolean_hotfix;
    private LuaFunction m_HeroDetailSoldierUIController_OnHeroSoldierSelectInt32Int32Action_hotfix;
    private LuaFunction m_HeroDetailSoldierUIController_OnGotoDrillInt32_hotfix;
    private LuaFunction m_HeroDetailSoldierUIController_OnGotoJobTransferConfigDataJobConnectionInfo_hotfix;
    private LuaFunction m_HeroDetailJobUIController_OnJobLvUpgradeInt32Int32Action_hotfix;
    private LuaFunction m_HeroDetailAddExpUIController_OnHeroAddExpInt32BagItemBaseInt32Action_hotfix;
    private LuaFunction m_HeroDetailAddExpUIController_OnHeroMaxLevelUseExpItem_hotfix;
    private LuaFunction m_HeroDetailAddExpUIController_OnReturn_hotfix;
    private LuaFunction m_HeroListUIController_OnDetail_hotfix;
    private LuaFunction m_HeroDetailUIController_OnReturn_hotfix;
    private LuaFunction m_HeroListUIController_OnHeroBreakInt32_hotfix;
    private LuaFunction m_HeroDetailUIController_OnJobTransfer_hotfix;
    private LuaFunction m_HeroJobTransferUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_HeroListUIController_OnComment_hotfix;
    private LuaFunction m_HeroCommentUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_HeroDetailEquipmentUIController_OnGotoEquipmentDepotInt32UInt64_hotfix;
    private LuaFunction m_EquipmentDepotUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_HeroDetailEquipmentUIController_OnGotoEquipmentForgeInt32UInt64_hotfix;
    private LuaFunction m_EquipmentForgeUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_HeroDetailEquipmentUIController_OnEquipmentTakeOffInt32Int32_hotfix;
    private LuaFunction m_HeroDetailEquipmentUIController_OnLockButtonClickUInt64Int32Action_hotfix;
    private LuaFunction m_HeroDetailEquipmentUIController_OnAutoEquipInt32Action_hotfix;
    private LuaFunction m_HeroDetailEquipmentUIController_OnAutoRemoveInt32_hotfix;
    private LuaFunction m_Hero_OnShowGetPathGoodsTypeInt32Int32_hotfix;
    private LuaFunction m_HeroListUIController_OnGotoEquipmentTab_hotfix;
    private LuaFunction m_HeroListUIController_OnGotoJobTab_hotfix;
    private LuaFunction m_HeroListUIController_OnSortToggleClickHeroSortType_hotfix;
    private LuaFunction m_CloseHeroCharChangeTaskAction_hotfix;
    private LuaFunction m_HeroListUIController_OnHeroCharSkinChangeButtonClick_hotfix;
    private LuaFunction m_HeroListUIController_OnHeroCharClick_hotfix;
    private LuaFunction m_HeroListUIController_OnGoToMemoryExtractionStore_hotfix;
    private LuaFunction m_HeroCharChangeTask_OnSkinChangedPreviewStringInt32_hotfix;
    private LuaFunction m_HeroCharChangeTask_EventOnClose_hotfix;
    private LuaFunction m_HeroCharChangeTask_OnAddSkinTicket_hotfix;
    private LuaFunction m_Hero_OnGotoGetPathGetPathDataNeedGoods_hotfix;
    private LuaFunction m_HeroDetailSoldierUIController_OnSkinInfoButtonClickConfigDataSoldierInfo_hotfix;
    private LuaFunction m_HeroDetailInfoUIController_OnSkinInfoButtonClick_hotfix;
    private LuaFunction m_HeroSoldierSkinUITask_EventOnReturn_hotfix;
    private LuaFunction m_HeroDetailInfoUIController_OnJobUpButtonClick_hotfix;
    private LuaFunction m_StoreSoldierSkinDetailUITask_EventOnBuySuccessEnd_hotfix;
    private LuaFunction m_StoreSoldierSkinDetailUITask_EventOnClose_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroListUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveUIStateToIntent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetUIStateFromIntent(UIIntent uiIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroListData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroCardData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCurrentHeroData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSoldierModeData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateJobModeData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroListItemCompare(Hero h1, Hero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UITaskBase.LayerDesc CreateLayerDescByIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewOnHeroChanged(
      int pos,
      bool isUnlockHero,
      bool isNeedStopCoroutine,
      int lastHeroId,
      bool isNeedRefreshHeroList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUpdatePiplineInHeroListTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PushLayerByCurState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PushLayerByIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckConflictBetweenLayer(int layerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailUIController_OnSetDetailState(string mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTabCommonUIStateByName(string modeName, string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_SetHeroList(List<Hero> unlockedList, List<Hero> lockedList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnAddHero(string str)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnHeroCompose(int heroId, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailLifeUIController_OnVoiceItemClick(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSelectSkillUIController_OnHeroSkillsSelect(
      int heroId,
      List<int> skillIds,
      bool isSkillChanged)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnHeroSoldierSelect(
      int heroId,
      int soldierId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnGotoDrill(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnGotoJobTransfer(
      ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailJobUIController_OnJobLvUpgrade(
      int heroId,
      int jobConnectionId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailAddExpUIController_OnHeroAddExp(
      int heroId,
      BagItemBase bagItem,
      int count,
      Action OnFinished)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailAddExpUIController_OnHeroMaxLevelUseExpItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailAddExpUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnDetail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnHeroBreak(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailUIController_OnJobTransfer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroJobTransferUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnComment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCommentUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnGotoEquipmentDepot(
      int slot,
      ulong equipmentInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentDepotUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnGotoEquipmentForge(
      int slot,
      ulong equipmentInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnEquipmentTakeOff(int heroId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnLockButtonClick(
      ulong instanceId,
      int slot,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnAutoEquip(int heroId, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnAutoRemove(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Hero_OnShowGetPath(GoodsType goodsType, int goodsId, int needCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnGotoEquipmentTab()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnGotoJobTab()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnSortToggleClick(HeroListUIController.HeroSortType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseHeroCharChangeTask(Action closeFinisdhAction = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnHeroCharSkinChangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnHeroCharClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnGoToMemoryExtractionStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroCharChangeTask_OnSkinChangedPreview(string spinePath, int heroSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroCharChangeTask_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroCharChangeTask_OnAddSkinTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Hero_OnGotoGetPath(GetPathData getPath, NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnSkinInfoButtonClick(
      ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailInfoUIController_OnSkinInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSoldierSkinUITask_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailInfoUIController_OnJobUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StoreSoldierSkinDetailUITask_EventOnBuySuccessEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StoreSoldierSkinDetailUITask_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroListUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return this.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      base.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroListUITask m_owner;

      public LuaExportHelper(HeroListUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public HeroListUIController m_heroListUIController
      {
        get
        {
          return this.m_owner.m_heroListUIController;
        }
        set
        {
          this.m_owner.m_heroListUIController = value;
        }
      }

      public HeroDetailUIController m_heroDetailUIController
      {
        get
        {
          return this.m_owner.m_heroDetailUIController;
        }
        set
        {
          this.m_owner.m_heroDetailUIController = value;
        }
      }

      public HeroDetailJobUIController m_heroDetailJobUIController
      {
        get
        {
          return this.m_owner.m_heroDetailJobUIController;
        }
        set
        {
          this.m_owner.m_heroDetailJobUIController = value;
        }
      }

      public HeroDetailInfoUIController m_heroDetailInfoUIController
      {
        get
        {
          return this.m_owner.m_heroDetailInfoUIController;
        }
        set
        {
          this.m_owner.m_heroDetailInfoUIController = value;
        }
      }

      public HeroDetailAddExpUIController m_heroDetailAddExpUIController
      {
        get
        {
          return this.m_owner.m_heroDetailAddExpUIController;
        }
        set
        {
          this.m_owner.m_heroDetailAddExpUIController = value;
        }
      }

      public HeroDetailSoldierUIController m_heroDetailSoldierUIController
      {
        get
        {
          return this.m_owner.m_heroDetailSoldierUIController;
        }
        set
        {
          this.m_owner.m_heroDetailSoldierUIController = value;
        }
      }

      public HeroDetailLifeUIController m_heroDetailLifeUIController
      {
        get
        {
          return this.m_owner.m_heroDetailLifeUIController;
        }
        set
        {
          this.m_owner.m_heroDetailLifeUIController = value;
        }
      }

      public HeroDetailEquipmentUIController m_heroDetailEquipmentUIController
      {
        get
        {
          return this.m_owner.m_heroDetailEquipmentUIController;
        }
        set
        {
          this.m_owner.m_heroDetailEquipmentUIController = value;
        }
      }

      public HeroDetailSelectSkillUIController m_heroDetailSelectSkillUIController
      {
        get
        {
          return this.m_owner.m_heroDetailSelectSkillUIController;
        }
        set
        {
          this.m_owner.m_heroDetailSelectSkillUIController = value;
        }
      }

      public HeroJobTransferUITask m_heroJobTransferUITask
      {
        get
        {
          return this.m_owner.m_heroJobTransferUITask;
        }
        set
        {
          this.m_owner.m_heroJobTransferUITask = value;
        }
      }

      public HeroSkinChangeUITask m_heroCharChangeTask
      {
        get
        {
          return this.m_owner.m_heroCharChangeTask;
        }
        set
        {
          this.m_owner.m_heroCharChangeTask = value;
        }
      }

      public StoreSoldierSkinDetailUITask m_storeSoldierSkinDetailUITask
      {
        get
        {
          return this.m_owner.m_storeSoldierSkinDetailUITask;
        }
        set
        {
          this.m_owner.m_storeSoldierSkinDetailUITask = value;
        }
      }

      public string m_stateName
      {
        get
        {
          return this.m_owner.m_stateName;
        }
        set
        {
          this.m_owner.m_stateName = value;
        }
      }

      public string m_lastMode
      {
        get
        {
          return this.m_owner.m_lastMode;
        }
        set
        {
          this.m_owner.m_lastMode = value;
        }
      }

      public string m_curMode
      {
        get
        {
          return this.m_owner.m_curMode;
        }
        set
        {
          this.m_owner.m_curMode = value;
        }
      }

      public List<Hero> m_lockedList
      {
        get
        {
          return this.m_owner.m_lockedList;
        }
        set
        {
          this.m_owner.m_lockedList = value;
        }
      }

      public List<Hero> m_curHeroList
      {
        get
        {
          return this.m_owner.m_curHeroList;
        }
        set
        {
          this.m_owner.m_curHeroList = value;
        }
      }

      public List<Hero> m_unlockedList
      {
        get
        {
          return this.m_owner.m_unlockedList;
        }
        set
        {
          this.m_owner.m_unlockedList = value;
        }
      }

      public int m_curHeroPos
      {
        get
        {
          return this.m_owner.m_curHeroPos;
        }
        set
        {
          this.m_owner.m_curHeroPos = value;
        }
      }

      public int m_curLayerDescIndex
      {
        get
        {
          return this.m_owner.m_curLayerDescIndex;
        }
        set
        {
          this.m_owner.m_curLayerDescIndex = value;
        }
      }

      public int m_lastHeroId
      {
        get
        {
          return this.m_owner.m_lastHeroId;
        }
        set
        {
          this.m_owner.m_lastHeroId = value;
        }
      }

      public bool m_isUnlockHero
      {
        get
        {
          return this.m_owner.m_isUnlockHero;
        }
        set
        {
          this.m_owner.m_isUnlockHero = value;
        }
      }

      public bool m_isDetailLayerOpen
      {
        get
        {
          return this.m_owner.m_isDetailLayerOpen;
        }
        set
        {
          this.m_owner.m_isDetailLayerOpen = value;
        }
      }

      public bool m_isNeedRefreshHeroList
      {
        get
        {
          return this.m_owner.m_isNeedRefreshHeroList;
        }
        set
        {
          this.m_owner.m_isNeedRefreshHeroList = value;
        }
      }

      public HeroListUIController.HeroSortType m_curHeroSortType
      {
        get
        {
          return this.m_owner.m_curHeroSortType;
        }
        set
        {
          this.m_owner.m_curHeroSortType = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void SaveUIStateToIntent()
      {
        this.m_owner.SaveUIStateToIntent();
      }

      public void GetUIStateFromIntent(UIIntent uiIntent)
      {
        this.m_owner.GetUIStateFromIntent(uiIntent);
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void UpdateHeroListData()
      {
        this.m_owner.UpdateHeroListData();
      }

      public void UpdateHeroCardData()
      {
        this.m_owner.UpdateHeroCardData();
      }

      public void UpdateCurrentHeroData()
      {
        this.m_owner.UpdateCurrentHeroData();
      }

      public void UpdateSoldierModeData()
      {
        this.m_owner.UpdateSoldierModeData();
      }

      public void UpdateJobModeData()
      {
        this.m_owner.UpdateJobModeData();
      }

      public int HeroListItemCompare(Hero h1, Hero h2)
      {
        return this.m_owner.HeroListItemCompare(h1, h2);
      }

      public bool IsNeedLoadStaticRes()
      {
        return this.m_owner.IsNeedLoadStaticRes();
      }

      public List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
      {
        return this.m_owner.CollectAllStaticResDescForLoad();
      }

      public UITaskBase.LayerDesc CreateLayerDescByIndex(int index)
      {
        return this.m_owner.CreateLayerDescByIndex(index);
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void UpdateViewOnHeroChanged(
        int pos,
        bool isUnlockHero,
        bool isNeedStopCoroutine,
        int lastHeroId,
        bool isNeedRefreshHeroList)
      {
        // ISSUE: unable to decompile the method.
      }

      public void StartUpdatePiplineInHeroListTask()
      {
        this.m_owner.StartUpdatePiplineInHeroListTask();
      }

      public void OnMemoryWarning()
      {
        this.m_owner.OnMemoryWarning();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void PushLayerByCurState()
      {
        this.m_owner.PushLayerByCurState();
      }

      public void PushLayerByIndex(int index)
      {
        this.m_owner.PushLayerByIndex(index);
      }

      public void CheckConflictBetweenLayer(int layerIndex)
      {
        this.m_owner.CheckConflictBetweenLayer(layerIndex);
      }

      public void HeroDetailUIController_OnSetDetailState(string mode)
      {
        this.m_owner.HeroDetailUIController_OnSetDetailState(mode);
      }

      public void SetTabCommonUIStateByName(string modeName, string stateName)
      {
        this.m_owner.SetTabCommonUIStateByName(modeName, stateName);
      }

      public void HeroListUIController_SetHeroList(List<Hero> unlockedList, List<Hero> lockedList)
      {
        this.m_owner.HeroListUIController_SetHeroList(unlockedList, lockedList);
      }

      public void HeroListUIController_OnReturn()
      {
        this.m_owner.HeroListUIController_OnReturn();
      }

      public void HeroListUIController_OnAddHero(string str)
      {
        this.m_owner.HeroListUIController_OnAddHero(str);
      }

      public void HeroListUIController_OnHeroCompose(int heroId, Action OnSucceed)
      {
        this.m_owner.HeroListUIController_OnHeroCompose(heroId, OnSucceed);
      }

      public void HeroDetailLifeUIController_OnVoiceItemClick(int heroPerformanceId)
      {
        this.m_owner.HeroDetailLifeUIController_OnVoiceItemClick(heroPerformanceId);
      }

      public void HeroDetailSelectSkillUIController_OnHeroSkillsSelect(
        int heroId,
        List<int> skillIds,
        bool isSkillChanged)
      {
        this.m_owner.HeroDetailSelectSkillUIController_OnHeroSkillsSelect(heroId, skillIds, isSkillChanged);
      }

      public void HeroDetailSoldierUIController_OnHeroSoldierSelect(
        int heroId,
        int soldierId,
        Action OnSucceed)
      {
        this.m_owner.HeroDetailSoldierUIController_OnHeroSoldierSelect(heroId, soldierId, OnSucceed);
      }

      public void HeroDetailSoldierUIController_OnGotoDrill(int techId)
      {
        this.m_owner.HeroDetailSoldierUIController_OnGotoDrill(techId);
      }

      public void HeroDetailSoldierUIController_OnGotoJobTransfer(
        ConfigDataJobConnectionInfo jobConnectionInfo)
      {
        this.m_owner.HeroDetailSoldierUIController_OnGotoJobTransfer(jobConnectionInfo);
      }

      public void HeroDetailJobUIController_OnJobLvUpgrade(
        int heroId,
        int jobConnectionId,
        Action OnSucceed)
      {
        this.m_owner.HeroDetailJobUIController_OnJobLvUpgrade(heroId, jobConnectionId, OnSucceed);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void HeroDetailAddExpUIController_OnHeroAddExp(
        int heroId,
        BagItemBase bagItem,
        int count,
        Action OnFinished)
      {
        // ISSUE: unable to decompile the method.
      }

      public void HeroDetailAddExpUIController_OnHeroMaxLevelUseExpItem()
      {
        this.m_owner.HeroDetailAddExpUIController_OnHeroMaxLevelUseExpItem();
      }

      public void HeroDetailAddExpUIController_OnReturn()
      {
        this.m_owner.HeroDetailAddExpUIController_OnReturn();
      }

      public void HeroListUIController_OnDetail()
      {
        this.m_owner.HeroListUIController_OnDetail();
      }

      public void HeroDetailUIController_OnReturn()
      {
        this.m_owner.HeroDetailUIController_OnReturn();
      }

      public void HeroListUIController_OnHeroBreak(int id)
      {
        this.m_owner.HeroListUIController_OnHeroBreak(id);
      }

      public void HeroDetailUIController_OnJobTransfer()
      {
        this.m_owner.HeroDetailUIController_OnJobTransfer();
      }

      public void HeroJobTransferUITask_OnLoadAllResCompleted()
      {
        this.m_owner.HeroJobTransferUITask_OnLoadAllResCompleted();
      }

      public void HeroListUIController_OnComment()
      {
        this.m_owner.HeroListUIController_OnComment();
      }

      public void HeroCommentUITask_OnLoadAllResCompleted()
      {
        this.m_owner.HeroCommentUITask_OnLoadAllResCompleted();
      }

      public void HeroDetailEquipmentUIController_OnGotoEquipmentDepot(
        int slot,
        ulong equipmentInstanceId)
      {
        this.m_owner.HeroDetailEquipmentUIController_OnGotoEquipmentDepot(slot, equipmentInstanceId);
      }

      public void EquipmentDepotUITask_OnLoadAllResCompleted()
      {
        this.m_owner.EquipmentDepotUITask_OnLoadAllResCompleted();
      }

      public void HeroDetailEquipmentUIController_OnGotoEquipmentForge(
        int slot,
        ulong equipmentInstanceId)
      {
        this.m_owner.HeroDetailEquipmentUIController_OnGotoEquipmentForge(slot, equipmentInstanceId);
      }

      public void EquipmentForgeUITask_OnLoadAllResCompleted()
      {
        this.m_owner.EquipmentForgeUITask_OnLoadAllResCompleted();
      }

      public void HeroDetailEquipmentUIController_OnEquipmentTakeOff(int heroId, int slot)
      {
        this.m_owner.HeroDetailEquipmentUIController_OnEquipmentTakeOff(heroId, slot);
      }

      public void HeroDetailEquipmentUIController_OnLockButtonClick(
        ulong instanceId,
        int slot,
        Action OnSucceed)
      {
        this.m_owner.HeroDetailEquipmentUIController_OnLockButtonClick(instanceId, slot, OnSucceed);
      }

      public void HeroDetailEquipmentUIController_OnAutoEquip(int heroId, Action OnSucceed)
      {
        this.m_owner.HeroDetailEquipmentUIController_OnAutoEquip(heroId, OnSucceed);
      }

      public void HeroDetailEquipmentUIController_OnAutoRemove(int heroId)
      {
        this.m_owner.HeroDetailEquipmentUIController_OnAutoRemove(heroId);
      }

      public void Hero_OnShowGetPath(GoodsType goodsType, int goodsId, int needCount)
      {
        this.m_owner.Hero_OnShowGetPath(goodsType, goodsId, needCount);
      }

      public void HeroListUIController_OnGotoEquipmentTab()
      {
        this.m_owner.HeroListUIController_OnGotoEquipmentTab();
      }

      public void HeroListUIController_OnGotoJobTab()
      {
        this.m_owner.HeroListUIController_OnGotoJobTab();
      }

      public void HeroListUIController_OnSortToggleClick(HeroListUIController.HeroSortType type)
      {
        this.m_owner.HeroListUIController_OnSortToggleClick(type);
      }

      public void CloseHeroCharChangeTask(Action closeFinisdhAction)
      {
        this.m_owner.CloseHeroCharChangeTask(closeFinisdhAction);
      }

      public void HeroListUIController_OnHeroCharSkinChangeButtonClick()
      {
        this.m_owner.HeroListUIController_OnHeroCharSkinChangeButtonClick();
      }

      public void HeroListUIController_OnHeroCharClick()
      {
        this.m_owner.HeroListUIController_OnHeroCharClick();
      }

      public void HeroListUIController_OnGoToMemoryExtractionStore()
      {
        this.m_owner.HeroListUIController_OnGoToMemoryExtractionStore();
      }

      public void Hero_OnGotoGetPath(GetPathData getPath, NeedGoods needGoods)
      {
        this.m_owner.Hero_OnGotoGetPath(getPath, needGoods);
      }

      public void HeroDetailSoldierUIController_OnSkinInfoButtonClick(
        ConfigDataSoldierInfo soldierInfo)
      {
        this.m_owner.HeroDetailSoldierUIController_OnSkinInfoButtonClick(soldierInfo);
      }

      public void HeroDetailInfoUIController_OnSkinInfoButtonClick()
      {
        this.m_owner.HeroDetailInfoUIController_OnSkinInfoButtonClick();
      }

      public void HeroSoldierSkinUITask_EventOnReturn()
      {
        this.m_owner.HeroSoldierSkinUITask_EventOnReturn();
      }

      public void HeroDetailInfoUIController_OnJobUpButtonClick()
      {
        this.m_owner.HeroDetailInfoUIController_OnJobUpButtonClick();
      }
    }
  }
}
