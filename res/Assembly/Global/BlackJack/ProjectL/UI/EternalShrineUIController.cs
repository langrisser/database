﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EternalShrineUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class EternalShrineUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./PlayerResource/Times/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dailyChallengeCountText;
    [AutoBind("./LevelList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelListScrollRect;
    [AutoBind("./BossInfoPanel/TitleGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_chapterNameText;
    [AutoBind("./BossInfoPanel/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGo;
    [AutoBind("./RecommendPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroTagPanelGameObject;
    [AutoBind("./RecommendPanel/FrameImage/CampGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_heroTagPanelGroup;
    [AutoBind("./KeywordPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_tagPanelStateCtrl;
    [AutoBind("./KeywordPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tagPanelBGButton;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/InfoPanel/TitleText1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tagPanelTitleText;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/InfoPanel/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tagPanelDescText;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/InfoPanel/RuleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tagPanelRuleText;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/InfoPanel/KeywordIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_tagPanelIconImage;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_tagPanelListScrollRect;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/Prefab/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tagPanelHeroIconPrefab;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/CloseBth", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tagPanelCloseBtn;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/EternalShrineLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_eternalShrineLevelListItemPrefab;
    [AutoBind("./Prefabs/CampIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroTagPrefab;
    private List<EternalShrineLevelListItemUIController> m_eternalShrineLevelListItems;
    private UISpineGraphic m_graphic;
    [DoNotToLua]
    private EternalShrineUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_ResetScrollViewToTop_hotfix;
    private LuaFunction m_SetEternalShrineConfigDataEternalShrineInfo_hotfix;
    private LuaFunction m_SetAllEternalShrineLevelListItemsList`1_hotfix;
    private LuaFunction m_AddEternalShrineLevelListItemConfigDataEternalShrineLevelInfoBoolean_hotfix;
    private LuaFunction m_ClearEternalShrineLevelListItems_hotfix;
    private LuaFunction m_ShowHeroTagPanelConfigDataHeroTagInfo_hotfix;
    private LuaFunction m_CloseHeroTagPanel_hotfix;
    private LuaFunction m_SortHeroIdByRankInt32Int32_hotfix;
    private LuaFunction m_EternalShrineHeroTagUIController_OnClickEternalShrineHeroTagUIController_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnAddTicketButtonClick_hotfix;
    private LuaFunction m_EternalShrineLevelListItem_OnStartButtonClickEternalShrineLevelListItemUIController_hotfix;
    private LuaFunction m_CreateSpineGraphicConfigDataEternalShrineInfo_hotfix;
    private LuaFunction m_DestroySpineGraphic_hotfix;
    private LuaFunction m_SetDailyChallengeCountInt32Int32_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnAddTicketAction_hotfix;
    private LuaFunction m_remove_EventOnAddTicketAction_hotfix;
    private LuaFunction m_add_EventOnStartEternalShrineLevelAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartEternalShrineLevelAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private EternalShrineUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEternalShrine(ConfigDataEternalShrineInfo eternalShrineInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAllEternalShrineLevelListItems(List<ConfigDataEternalShrineLevelInfo> levelInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddEternalShrineLevelListItem(
      ConfigDataEternalShrineLevelInfo levelnfo,
      bool opened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearEternalShrineLevelListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowHeroTagPanel(ConfigDataHeroTagInfo tagInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseHeroTagPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortHeroIdByRank(int heroId1, int heroId2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EternalShrineHeroTagUIController_OnClick(EternalShrineHeroTagUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddTicketButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EternalShrineLevelListItem_OnStartButtonClick(
      EternalShrineLevelListItemUIController b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(ConfigDataEternalShrineInfo eternalShrineInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDailyChallengeCount(int restCount, int allCount)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataEternalShrineLevelInfo> EventOnStartEternalShrineLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public EternalShrineUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddTicket()
    {
      this.EventOnAddTicket = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartEternalShrineLevel(ConfigDataEternalShrineLevelInfo obj)
    {
    }

    private void __clearDele_EventOnStartEternalShrineLevel(ConfigDataEternalShrineLevelInfo obj)
    {
      this.EventOnStartEternalShrineLevel = (Action<ConfigDataEternalShrineLevelInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private EternalShrineUIController m_owner;

      public LuaExportHelper(EternalShrineUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnAddTicket()
      {
        this.m_owner.__callDele_EventOnAddTicket();
      }

      public void __clearDele_EventOnAddTicket()
      {
        this.m_owner.__clearDele_EventOnAddTicket();
      }

      public void __callDele_EventOnStartEternalShrineLevel(ConfigDataEternalShrineLevelInfo obj)
      {
        this.m_owner.__callDele_EventOnStartEternalShrineLevel(obj);
      }

      public void __clearDele_EventOnStartEternalShrineLevel(ConfigDataEternalShrineLevelInfo obj)
      {
        this.m_owner.__clearDele_EventOnStartEternalShrineLevel(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Text m_dailyChallengeCountText
      {
        get
        {
          return this.m_owner.m_dailyChallengeCountText;
        }
        set
        {
          this.m_owner.m_dailyChallengeCountText = value;
        }
      }

      public ScrollRect m_levelListScrollRect
      {
        get
        {
          return this.m_owner.m_levelListScrollRect;
        }
        set
        {
          this.m_owner.m_levelListScrollRect = value;
        }
      }

      public Text m_chapterNameText
      {
        get
        {
          return this.m_owner.m_chapterNameText;
        }
        set
        {
          this.m_owner.m_chapterNameText = value;
        }
      }

      public GameObject m_charGo
      {
        get
        {
          return this.m_owner.m_charGo;
        }
        set
        {
          this.m_owner.m_charGo = value;
        }
      }

      public GameObject m_heroTagPanelGameObject
      {
        get
        {
          return this.m_owner.m_heroTagPanelGameObject;
        }
        set
        {
          this.m_owner.m_heroTagPanelGameObject = value;
        }
      }

      public Transform m_heroTagPanelGroup
      {
        get
        {
          return this.m_owner.m_heroTagPanelGroup;
        }
        set
        {
          this.m_owner.m_heroTagPanelGroup = value;
        }
      }

      public CommonUIStateController m_tagPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_tagPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_tagPanelStateCtrl = value;
        }
      }

      public Button m_tagPanelBGButton
      {
        get
        {
          return this.m_owner.m_tagPanelBGButton;
        }
        set
        {
          this.m_owner.m_tagPanelBGButton = value;
        }
      }

      public Text m_tagPanelTitleText
      {
        get
        {
          return this.m_owner.m_tagPanelTitleText;
        }
        set
        {
          this.m_owner.m_tagPanelTitleText = value;
        }
      }

      public Text m_tagPanelDescText
      {
        get
        {
          return this.m_owner.m_tagPanelDescText;
        }
        set
        {
          this.m_owner.m_tagPanelDescText = value;
        }
      }

      public Text m_tagPanelRuleText
      {
        get
        {
          return this.m_owner.m_tagPanelRuleText;
        }
        set
        {
          this.m_owner.m_tagPanelRuleText = value;
        }
      }

      public Image m_tagPanelIconImage
      {
        get
        {
          return this.m_owner.m_tagPanelIconImage;
        }
        set
        {
          this.m_owner.m_tagPanelIconImage = value;
        }
      }

      public ScrollRect m_tagPanelListScrollRect
      {
        get
        {
          return this.m_owner.m_tagPanelListScrollRect;
        }
        set
        {
          this.m_owner.m_tagPanelListScrollRect = value;
        }
      }

      public GameObject m_tagPanelHeroIconPrefab
      {
        get
        {
          return this.m_owner.m_tagPanelHeroIconPrefab;
        }
        set
        {
          this.m_owner.m_tagPanelHeroIconPrefab = value;
        }
      }

      public Button m_tagPanelCloseBtn
      {
        get
        {
          return this.m_owner.m_tagPanelCloseBtn;
        }
        set
        {
          this.m_owner.m_tagPanelCloseBtn = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_eternalShrineLevelListItemPrefab
      {
        get
        {
          return this.m_owner.m_eternalShrineLevelListItemPrefab;
        }
        set
        {
          this.m_owner.m_eternalShrineLevelListItemPrefab = value;
        }
      }

      public GameObject m_heroTagPrefab
      {
        get
        {
          return this.m_owner.m_heroTagPrefab;
        }
        set
        {
          this.m_owner.m_heroTagPrefab = value;
        }
      }

      public List<EternalShrineLevelListItemUIController> m_eternalShrineLevelListItems
      {
        get
        {
          return this.m_owner.m_eternalShrineLevelListItems;
        }
        set
        {
          this.m_owner.m_eternalShrineLevelListItems = value;
        }
      }

      public UISpineGraphic m_graphic
      {
        get
        {
          return this.m_owner.m_graphic;
        }
        set
        {
          this.m_owner.m_graphic = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ResetScrollViewToTop()
      {
        this.m_owner.ResetScrollViewToTop();
      }

      public void AddEternalShrineLevelListItem(
        ConfigDataEternalShrineLevelInfo levelnfo,
        bool opened)
      {
        this.m_owner.AddEternalShrineLevelListItem(levelnfo, opened);
      }

      public void ClearEternalShrineLevelListItems()
      {
        this.m_owner.ClearEternalShrineLevelListItems();
      }

      public void ShowHeroTagPanel(ConfigDataHeroTagInfo tagInfo)
      {
        this.m_owner.ShowHeroTagPanel(tagInfo);
      }

      public void CloseHeroTagPanel()
      {
        this.m_owner.CloseHeroTagPanel();
      }

      public int SortHeroIdByRank(int heroId1, int heroId2)
      {
        return this.m_owner.SortHeroIdByRank(heroId1, heroId2);
      }

      public void EternalShrineHeroTagUIController_OnClick(EternalShrineHeroTagUIController ctrl)
      {
        this.m_owner.EternalShrineHeroTagUIController_OnClick(ctrl);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnAddTicketButtonClick()
      {
        this.m_owner.OnAddTicketButtonClick();
      }

      public void EternalShrineLevelListItem_OnStartButtonClick(
        EternalShrineLevelListItemUIController b)
      {
        this.m_owner.EternalShrineLevelListItem_OnStartButtonClick(b);
      }

      public void CreateSpineGraphic(ConfigDataEternalShrineInfo eternalShrineInfo)
      {
        this.m_owner.CreateSpineGraphic(eternalShrineInfo);
      }

      public void DestroySpineGraphic()
      {
        this.m_owner.DestroySpineGraphic();
      }
    }
  }
}
