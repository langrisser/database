﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ManualUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ManualUIController : UIControllerBase
  {
    [AutoBind("./EquipmentPanel/EquipmentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipmentButton;
    [AutoBind("./HeroPanel/HeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroButton;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./DetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detailButton;
    private ArchiveUITask m_task;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetTask(ArchiveUITask task)
    {
      this.m_task = task;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnEquipmentClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnReturnClick()
    {
    }

    protected void OnDetailClick()
    {
      CommonUIController.Instance.ShowExplanation(ExplanationId.ExplanationId_Archive);
    }
  }
}
