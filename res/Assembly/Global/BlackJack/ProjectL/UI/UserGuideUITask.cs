﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UserGuideUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class UserGuideUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private Coroutine m_initCoroutine;
    private UserGuideUIController m_userGuideUIController;
    private ConfigDataUserGuideStep m_userGuideStepInfo;
    private const float m_waitForClickObjectTime = 0.2f;
    private const float m_maxWaitObjectTime = 20f;
    private ConfigDataBattleDialogInfo m_battleDialogInfo;
    private BattleDialogUITask m_battleDialogUITask;
    private UserGuideDialogUITask m_userGuideDialogUITask;
    private int m_pageIndex;
    private int m_nUserGuideId;
    private bool m_isDoingUpdateViewAsync;
    private List<GameObject> m_temporaryDisableObjects;
    private List<GameObject> m_temporaryDeactiveObjects;
    private bool m_isFinished;
    private bool m_isTemporaryDisableMoveBattleCamera;
    private bool m_isEnableSkip;
    private static Dictionary<int, int> m_triggerUserGuideCounts;
    private static Dictionary<int, ConfigDataUserGuide> m_userGuideConfigs;
    private static List<string[]> m_userGuideShowHideEventObjectPaths;
    private static bool m_isEnable;
    private static int m_dragHeroToBattleUserGuideID;
    [DoNotToLua]
    private UserGuideUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_TriggerUserGuideUserGuideTriggerString_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_ForceStop_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_InitDataFromUIIntentUIIntent_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_AddShowHideEventForUserGuideStep_hotfix;
    private LuaFunction m_DoUpdateView_hotfix;
    private LuaFunction m_EnableInputLatelySingle_hotfix;
    private LuaFunction m_DoUpdateViewAsync_hotfix;
    private LuaFunction m_OnFunctionOpenAnimEnd_hotfix;
    private LuaFunction m_IsClickableGameObject_hotfix;
    private LuaFunction m_PrepareForGetUIPositionGameObject_hotfix;
    private LuaFunction m_ClickObjectStringbe_hotfix;
    private LuaFunction m_ClickGameObject_hotfix;
    private LuaFunction m_ArrayToPathStringStringbe_hotfix;
    private LuaFunction m_DoActionUserGuideActionString_hotfix;
    private LuaFunction m_EnableBattleCameraTouchMoveBoolean_hotfix;
    private LuaFunction m_FinishCurrentUserGuide_hotfix;
    private LuaFunction m_WaitForObjectReadyString_hotfix;
    private LuaFunction m_ScrollToItemStringString_hotfix;
    private LuaFunction m_EnableObjectGameObjectBoolean_hotfix;
    private LuaFunction m_ClickBattleGridInt32Int32_hotfix;
    private LuaFunction m_ShowCurrentPage_hotfix;
    private LuaFunction m_InitUserGuideUIController_hotfix;
    private LuaFunction m_UninitUserGuideUIController_hotfix;
    private LuaFunction m_StartBattleDialogUITaskConfigDataBattleDialogInfo_hotfix;
    private LuaFunction m_BattleDialogUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartUserGuideDialogUITaskConfigDataUserGuideDialogInfo_hotfix;
    private LuaFunction m_UserGuideDialogUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_OnNextButtonClicked_hotfix;
    private LuaFunction m_OnSkipUserGuide_hotfix;
    private LuaFunction m_FinishBooleanBoolean_hotfix;
    private LuaFunction m_Next_hotfix;
    private LuaFunction m_UserGuideUIController_OnNextPage_hotfix;
    private LuaFunction m_UserGuideUIController_OnPrevPage_hotfix;
    private LuaFunction m_BattleDialogUITask_OnUserGuideClose_hotfix;
    private LuaFunction m_UserGuideDialogUITask_OnUserGuideClose_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public UserGuideUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Initialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AddUserGuideTriggerCount(int userGuideId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetUserGuideTriggerCount(int userGuideId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SaveUserGuideProgress()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void LoadUserGuideProgress()
    {
      // ISSUE: unable to decompile the method.
    }

    private static string UserGuideProgressFileName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsUserGuideBattle(int battleID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool NeedSkipBattlePrepareForUserGuide(int battleID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<int> GetEnforceHeros(int battleID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void CollectUserGuideShowHideEventObjectPaths(List<string[]> paths)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsArrayEqual<T>(T[] arr1, T[] arr2, Comparison<T> compare)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void OnShowObject(string objectPath)
    {
      UserGuideUITask.Trigger(UserGuideTrigger.UserGuideTrigger_ShowObject, objectPath);
    }

    public static void OnHideObject(string objectPath)
    {
      UserGuideUITask.Trigger(UserGuideTrigger.UserGuideTrigger_HideObject, objectPath);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnUIStateEnd(GameObject obj, string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnSelectBattleActor(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void OnWorldUIGetReady()
    {
      UserGuideUITask.Trigger(UserGuideTrigger.UserGuideTrigger_WorldUIGetReady, string.Empty);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnServerFinishUserGuide(int guideID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnDeselectBattleActor(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnGiftStoreGoodsBuy(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnUITaskShow(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void OnReturnToLoginUI(bool obj)
    {
      UserGuideUITask.m_triggerUserGuideCounts = (Dictionary<int, int>) null;
    }

    public static void OnPrefabAwake(GameObject awakeObj)
    {
      UserGuideUITask.AddShowHideEventForUserGuide(awakeObj);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AddShowHideEventForUserGuide(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsUserGuideTriggerObject(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TriggerUserGuide(UserGuideTrigger trigger, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool Trigger(UserGuideTrigger trigger, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool MatchTrigger(
      UserGuideTrigger trigger1,
      string param1,
      UserGuideTrigger trigger2,
      string param2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool CheckCondition(UserGuideCondition c, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartStep(int userGuideId, int userGuideStepId, bool bSkipEnable = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ForceStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddShowHideEventForUserGuideStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator EnableInputLately(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DoUpdateViewAsync()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFunctionOpenAnimEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool GetCenterScreenPosition(RectTransform rt, ref Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsInsideScreen(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsClickable(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsChildTransform(Transform parent, Transform child)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PrepareForGetUIPosition(GameObject uiObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClickObject(string[] objPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool Click(GameObject o)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ArrayToPathString(string[] path)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DoAction(UserGuideAction action, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnableBattleCameraTouchMove(bool isEnable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FinishCurrentUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator WaitForObjectReady(string objPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ScrollToItem(string scrollObjPath, string itemObjPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnableObject(GameObject obj, bool isEnable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClickBattleGrid(int x, int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCurrentPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitUserGuideUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitUserGuideUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleDialogUITask(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator StartUserGuideDialogUITask(
      ConfigDataUserGuideDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UserGuideDialogUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNextButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkipUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Finish(bool isClick = true, bool bForce = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Next()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UserGuideUIController_OnNextPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UserGuideUIController_OnPrevPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUITask_OnUserGuideClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UserGuideDialogUITask_OnUserGuideClose()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static bool Enable
    {
      get
      {
        return UserGuideUITask.m_isEnable;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public UserGuideUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static UserGuideUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private UserGuideUITask m_owner;

      public LuaExportHelper(UserGuideUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public Coroutine m_initCoroutine
      {
        get
        {
          return this.m_owner.m_initCoroutine;
        }
        set
        {
          this.m_owner.m_initCoroutine = value;
        }
      }

      public UserGuideUIController m_userGuideUIController
      {
        get
        {
          return this.m_owner.m_userGuideUIController;
        }
        set
        {
          this.m_owner.m_userGuideUIController = value;
        }
      }

      public ConfigDataUserGuideStep m_userGuideStepInfo
      {
        get
        {
          return this.m_owner.m_userGuideStepInfo;
        }
        set
        {
          this.m_owner.m_userGuideStepInfo = value;
        }
      }

      public static float m_waitForClickObjectTime
      {
        get
        {
          return 0.2f;
        }
      }

      public static float m_maxWaitObjectTime
      {
        get
        {
          return 20f;
        }
      }

      public ConfigDataBattleDialogInfo m_battleDialogInfo
      {
        get
        {
          return this.m_owner.m_battleDialogInfo;
        }
        set
        {
          this.m_owner.m_battleDialogInfo = value;
        }
      }

      public BattleDialogUITask m_battleDialogUITask
      {
        get
        {
          return this.m_owner.m_battleDialogUITask;
        }
        set
        {
          this.m_owner.m_battleDialogUITask = value;
        }
      }

      public UserGuideDialogUITask m_userGuideDialogUITask
      {
        get
        {
          return this.m_owner.m_userGuideDialogUITask;
        }
        set
        {
          this.m_owner.m_userGuideDialogUITask = value;
        }
      }

      public int m_pageIndex
      {
        get
        {
          return this.m_owner.m_pageIndex;
        }
        set
        {
          this.m_owner.m_pageIndex = value;
        }
      }

      public int m_nUserGuideId
      {
        get
        {
          return this.m_owner.m_nUserGuideId;
        }
        set
        {
          this.m_owner.m_nUserGuideId = value;
        }
      }

      public bool m_isDoingUpdateViewAsync
      {
        get
        {
          return this.m_owner.m_isDoingUpdateViewAsync;
        }
        set
        {
          this.m_owner.m_isDoingUpdateViewAsync = value;
        }
      }

      public List<GameObject> m_temporaryDisableObjects
      {
        get
        {
          return this.m_owner.m_temporaryDisableObjects;
        }
        set
        {
          this.m_owner.m_temporaryDisableObjects = value;
        }
      }

      public List<GameObject> m_temporaryDeactiveObjects
      {
        get
        {
          return this.m_owner.m_temporaryDeactiveObjects;
        }
        set
        {
          this.m_owner.m_temporaryDeactiveObjects = value;
        }
      }

      public bool m_isFinished
      {
        get
        {
          return this.m_owner.m_isFinished;
        }
        set
        {
          this.m_owner.m_isFinished = value;
        }
      }

      public bool m_isTemporaryDisableMoveBattleCamera
      {
        get
        {
          return this.m_owner.m_isTemporaryDisableMoveBattleCamera;
        }
        set
        {
          this.m_owner.m_isTemporaryDisableMoveBattleCamera = value;
        }
      }

      public bool m_isEnableSkip
      {
        get
        {
          return this.m_owner.m_isEnableSkip;
        }
        set
        {
          this.m_owner.m_isEnableSkip = value;
        }
      }

      public static Dictionary<int, int> m_triggerUserGuideCounts
      {
        get
        {
          return UserGuideUITask.m_triggerUserGuideCounts;
        }
        set
        {
          UserGuideUITask.m_triggerUserGuideCounts = value;
        }
      }

      public static Dictionary<int, ConfigDataUserGuide> m_userGuideConfigs
      {
        get
        {
          return UserGuideUITask.m_userGuideConfigs;
        }
        set
        {
          UserGuideUITask.m_userGuideConfigs = value;
        }
      }

      public static List<string[]> m_userGuideShowHideEventObjectPaths
      {
        get
        {
          return UserGuideUITask.m_userGuideShowHideEventObjectPaths;
        }
        set
        {
          UserGuideUITask.m_userGuideShowHideEventObjectPaths = value;
        }
      }

      public static bool m_isEnable
      {
        get
        {
          return UserGuideUITask.m_isEnable;
        }
        set
        {
          UserGuideUITask.m_isEnable = value;
        }
      }

      public static int m_dragHeroToBattleUserGuideID
      {
        get
        {
          return UserGuideUITask.m_dragHeroToBattleUserGuideID;
        }
        set
        {
          UserGuideUITask.m_dragHeroToBattleUserGuideID = value;
        }
      }

      public static string UserGuideProgressFileName
      {
        get
        {
          return UserGuideUITask.UserGuideProgressFileName;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public static void AddUserGuideTriggerCount(int userGuideId)
      {
        UserGuideUITask.AddUserGuideTriggerCount(userGuideId);
      }

      public static int GetUserGuideTriggerCount(int userGuideId)
      {
        return UserGuideUITask.GetUserGuideTriggerCount(userGuideId);
      }

      public static void LoadUserGuideProgress()
      {
        UserGuideUITask.LoadUserGuideProgress();
      }

      public static void CollectUserGuideShowHideEventObjectPaths(List<string[]> paths)
      {
        UserGuideUITask.CollectUserGuideShowHideEventObjectPaths(paths);
      }

      public static void AddShowHideEventForUserGuide(GameObject obj)
      {
        UserGuideUITask.AddShowHideEventForUserGuide(obj);
      }

      public static bool IsUserGuideTriggerObject(GameObject obj)
      {
        return UserGuideUITask.IsUserGuideTriggerObject(obj);
      }

      public bool TriggerUserGuide(UserGuideTrigger trigger, string param)
      {
        return this.m_owner.TriggerUserGuide(trigger, param);
      }

      public static bool Trigger(UserGuideTrigger trigger, string param)
      {
        return UserGuideUITask.Trigger(trigger, param);
      }

      public static bool MatchTrigger(
        UserGuideTrigger trigger1,
        string param1,
        UserGuideTrigger trigger2,
        string param2)
      {
        return UserGuideUITask.MatchTrigger(trigger1, param1, trigger2, param2);
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public void ForceStop()
      {
        this.m_owner.ForceStop();
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void InitDataFromUIIntent(UIIntent intent)
      {
        this.m_owner.InitDataFromUIIntent(intent);
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void AddShowHideEventForUserGuideStep()
      {
        this.m_owner.AddShowHideEventForUserGuideStep();
      }

      public void DoUpdateView()
      {
        this.m_owner.DoUpdateView();
      }

      public IEnumerator EnableInputLately(float time)
      {
        return this.m_owner.EnableInputLately(time);
      }

      public IEnumerator DoUpdateViewAsync()
      {
        return this.m_owner.DoUpdateViewAsync();
      }

      public void OnFunctionOpenAnimEnd()
      {
        this.m_owner.OnFunctionOpenAnimEnd();
      }

      public static bool GetCenterScreenPosition(RectTransform rt, ref Vector2 pos)
      {
        return UserGuideUITask.GetCenterScreenPosition(rt, ref pos);
      }

      public static bool IsInsideScreen(Vector2 pos)
      {
        return UserGuideUITask.IsInsideScreen(pos);
      }

      public bool IsClickable(GameObject obj)
      {
        return this.m_owner.IsClickable(obj);
      }

      public void PrepareForGetUIPosition(GameObject uiObj)
      {
        this.m_owner.PrepareForGetUIPosition(uiObj);
      }

      public void ClickObject(string[] objPath)
      {
        this.m_owner.ClickObject(objPath);
      }

      public bool Click(GameObject o)
      {
        return this.m_owner.Click(o);
      }

      public string ArrayToPathString(string[] path)
      {
        return this.m_owner.ArrayToPathString(path);
      }

      public IEnumerator DoAction(UserGuideAction action, string param)
      {
        return this.m_owner.DoAction(action, param);
      }

      public void EnableBattleCameraTouchMove(bool isEnable)
      {
        this.m_owner.EnableBattleCameraTouchMove(isEnable);
      }

      public void FinishCurrentUserGuide()
      {
        this.m_owner.FinishCurrentUserGuide();
      }

      public IEnumerator WaitForObjectReady(string objPath)
      {
        return this.m_owner.WaitForObjectReady(objPath);
      }

      public IEnumerator ScrollToItem(string scrollObjPath, string itemObjPath)
      {
        return this.m_owner.ScrollToItem(scrollObjPath, itemObjPath);
      }

      public void EnableObject(GameObject obj, bool isEnable)
      {
        this.m_owner.EnableObject(obj, isEnable);
      }

      public void ClickBattleGrid(int x, int y)
      {
        this.m_owner.ClickBattleGrid(x, y);
      }

      public void ShowCurrentPage()
      {
        this.m_owner.ShowCurrentPage();
      }

      public void InitUserGuideUIController()
      {
        this.m_owner.InitUserGuideUIController();
      }

      public void UninitUserGuideUIController()
      {
        this.m_owner.UninitUserGuideUIController();
      }

      public void StartBattleDialogUITask(ConfigDataBattleDialogInfo dialogInfo)
      {
        this.m_owner.StartBattleDialogUITask(dialogInfo);
      }

      public void BattleDialogUITask_OnLoadAllResCompleted()
      {
        this.m_owner.BattleDialogUITask_OnLoadAllResCompleted();
      }

      public IEnumerator StartUserGuideDialogUITask(
        ConfigDataUserGuideDialogInfo dialogInfo)
      {
        return this.m_owner.StartUserGuideDialogUITask(dialogInfo);
      }

      public void UserGuideDialogUITask_OnLoadAllResCompleted()
      {
        this.m_owner.UserGuideDialogUITask_OnLoadAllResCompleted();
      }

      public void OnNextButtonClicked()
      {
        this.m_owner.OnNextButtonClicked();
      }

      public void OnSkipUserGuide()
      {
        this.m_owner.OnSkipUserGuide();
      }

      public void Finish(bool isClick, bool bForce)
      {
        this.m_owner.Finish(isClick, bForce);
      }

      public void Next()
      {
        this.m_owner.Next();
      }

      public void UserGuideUIController_OnNextPage()
      {
        this.m_owner.UserGuideUIController_OnNextPage();
      }

      public void UserGuideUIController_OnPrevPage()
      {
        this.m_owner.UserGuideUIController_OnPrevPage();
      }

      public void BattleDialogUITask_OnUserGuideClose()
      {
        this.m_owner.BattleDialogUITask_OnUserGuideClose();
      }

      public void UserGuideDialogUITask_OnUserGuideClose()
      {
        this.m_owner.UserGuideDialogUITask_OnUserGuideClose();
      }
    }
  }
}
