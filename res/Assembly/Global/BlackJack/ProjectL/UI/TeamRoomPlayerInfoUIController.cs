﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomPlayerInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TeamRoomPlayerInfoUIController : UIControllerBase, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler, IEventSystemHandler
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./PlayerIn", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerButton;
    [AutoBind("./PlayerIn/PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerTagImage;
    [AutoBind("./PlayerIn/Name/NameWord/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./PlayerIn/Name/NameWord/LVText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./PlayerIn/Name/NameBG/HeadImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIconImage;
    [AutoBind("./PlayerIn/Name/NameBG/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    [AutoBind("./PlayerIn/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_graphicGameObject;
    [AutoBind("./PlayerIn/SecretBlessing", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_blessingButton;
    [AutoBind("./NoPlayer/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteButton;
    [AutoBind("./Chat", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_chatUIStateController;
    [AutoBind("./Chat/BGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private EmojiText m_dialogText;
    [AutoBind("./Chat/BGImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_dialogBigExpression;
    private int m_index;
    private TeamRoomPlayer m_player;
    private UISpineGraphic m_spineGraphic;
    private float m_hideChatTime;
    [DoNotToLua]
    private TeamRoomPlayerInfoUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitEmojiTextInt32Image_hotfix;
    private LuaFunction m_SetIndexInt32_hotfix;
    private LuaFunction m_GetIndex_hotfix;
    private LuaFunction m_SetPlayerIndexInt32_hotfix;
    private LuaFunction m_SetPlayerTeamRoomPlayer_hotfix;
    private LuaFunction m_GetPlayer_hotfix;
    private LuaFunction m_SetNameString_hotfix;
    private LuaFunction m_SetLevelInt32_hotfix;
    private LuaFunction m_SetHeadIconInt32_hotfix;
    private LuaFunction m_SetBlessingBoolean_hotfix;
    private LuaFunction m_SetOnBoolean_hotfix;
    private LuaFunction m_SetEditOnBoolean_hotfix;
    private LuaFunction m_ShowChatString_hotfix;
    private LuaFunction m_ShowBigExpressionInt32_hotfix;
    private LuaFunction m_HideChat_hotfix;
    private LuaFunction m_CreateSpineGraphicConfigDataJobConnectionInfoConfigDataModelSkinResourceInfo_hotfix;
    private LuaFunction m_DestroySpineGraphic_hotfix;
    private LuaFunction m_SetAnimationTimeSingle_hotfix;
    private LuaFunction m_GetAnimationTime_hotfix;
    private LuaFunction m_OnInviteButtonClick_hotfix;
    private LuaFunction m_OnPlayerButtonClick_hotfix;
    private LuaFunction m_OnBlessingButtonClick_hotfix;
    private LuaFunction m_OnBeginDragPointerEventData_hotfix;
    private LuaFunction m_OnEndDragPointerEventData_hotfix;
    private LuaFunction m_OnDragPointerEventData_hotfix;
    private LuaFunction m_OnDropPointerEventData_hotfix;
    private LuaFunction m_ClearEvents_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_add_EventOnInviteButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnInviteButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnPlayerButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnPlayerButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnBlessingButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnBlessingButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnBeginDragAction`2_hotfix;
    private LuaFunction m_remove_EventOnBeginDragAction`2_hotfix;
    private LuaFunction m_add_EventOnEndDragAction`2_hotfix;
    private LuaFunction m_remove_EventOnEndDragAction`2_hotfix;
    private LuaFunction m_add_EventOnDragAction`1_hotfix;
    private LuaFunction m_remove_EventOnDragAction`1_hotfix;
    private LuaFunction m_add_EventOnDropAction`1_hotfix;
    private LuaFunction m_remove_EventOnDropAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEmojiText(int fontSize, Image image)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIndex(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerIndex(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayer GetPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeadIcon(int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBlessing(bool blessing)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOn(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEditOn(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChat(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBigExpression(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ConfigDataModelSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnimationTime(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAnimationTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBlessingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrop(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<TeamRoomPlayerInfoUIController> EventOnInviteButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomPlayerInfoUIController> EventOnPlayerButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomPlayerInfoUIController> EventOnBlessingButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomPlayerInfoUIController, PointerEventData> EventOnBeginDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomPlayerInfoUIController, PointerEventData> EventOnEndDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnDrop
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TeamRoomPlayerInfoUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnInviteButtonClick(TeamRoomPlayerInfoUIController obj)
    {
    }

    private void __clearDele_EventOnInviteButtonClick(TeamRoomPlayerInfoUIController obj)
    {
      this.EventOnInviteButtonClick = (Action<TeamRoomPlayerInfoUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPlayerButtonClick(TeamRoomPlayerInfoUIController obj)
    {
    }

    private void __clearDele_EventOnPlayerButtonClick(TeamRoomPlayerInfoUIController obj)
    {
      this.EventOnPlayerButtonClick = (Action<TeamRoomPlayerInfoUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBlessingButtonClick(TeamRoomPlayerInfoUIController obj)
    {
    }

    private void __clearDele_EventOnBlessingButtonClick(TeamRoomPlayerInfoUIController obj)
    {
      this.EventOnBlessingButtonClick = (Action<TeamRoomPlayerInfoUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBeginDrag(
      TeamRoomPlayerInfoUIController arg1,
      PointerEventData arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBeginDrag(
      TeamRoomPlayerInfoUIController arg1,
      PointerEventData arg2)
    {
      this.EventOnBeginDrag = (Action<TeamRoomPlayerInfoUIController, PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEndDrag(
      TeamRoomPlayerInfoUIController arg1,
      PointerEventData arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEndDrag(
      TeamRoomPlayerInfoUIController arg1,
      PointerEventData arg2)
    {
      this.EventOnEndDrag = (Action<TeamRoomPlayerInfoUIController, PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnDrag(PointerEventData obj)
    {
    }

    private void __clearDele_EventOnDrag(PointerEventData obj)
    {
      this.EventOnDrag = (Action<PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnDrop(PointerEventData obj)
    {
    }

    private void __clearDele_EventOnDrop(PointerEventData obj)
    {
      this.EventOnDrop = (Action<PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TeamRoomPlayerInfoUIController m_owner;

      public LuaExportHelper(TeamRoomPlayerInfoUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnInviteButtonClick(TeamRoomPlayerInfoUIController obj)
      {
        this.m_owner.__callDele_EventOnInviteButtonClick(obj);
      }

      public void __clearDele_EventOnInviteButtonClick(TeamRoomPlayerInfoUIController obj)
      {
        this.m_owner.__clearDele_EventOnInviteButtonClick(obj);
      }

      public void __callDele_EventOnPlayerButtonClick(TeamRoomPlayerInfoUIController obj)
      {
        this.m_owner.__callDele_EventOnPlayerButtonClick(obj);
      }

      public void __clearDele_EventOnPlayerButtonClick(TeamRoomPlayerInfoUIController obj)
      {
        this.m_owner.__clearDele_EventOnPlayerButtonClick(obj);
      }

      public void __callDele_EventOnBlessingButtonClick(TeamRoomPlayerInfoUIController obj)
      {
        this.m_owner.__callDele_EventOnBlessingButtonClick(obj);
      }

      public void __clearDele_EventOnBlessingButtonClick(TeamRoomPlayerInfoUIController obj)
      {
        this.m_owner.__clearDele_EventOnBlessingButtonClick(obj);
      }

      public void __callDele_EventOnBeginDrag(
        TeamRoomPlayerInfoUIController arg1,
        PointerEventData arg2)
      {
        this.m_owner.__callDele_EventOnBeginDrag(arg1, arg2);
      }

      public void __clearDele_EventOnBeginDrag(
        TeamRoomPlayerInfoUIController arg1,
        PointerEventData arg2)
      {
        this.m_owner.__clearDele_EventOnBeginDrag(arg1, arg2);
      }

      public void __callDele_EventOnEndDrag(
        TeamRoomPlayerInfoUIController arg1,
        PointerEventData arg2)
      {
        this.m_owner.__callDele_EventOnEndDrag(arg1, arg2);
      }

      public void __clearDele_EventOnEndDrag(
        TeamRoomPlayerInfoUIController arg1,
        PointerEventData arg2)
      {
        this.m_owner.__clearDele_EventOnEndDrag(arg1, arg2);
      }

      public void __callDele_EventOnDrag(PointerEventData obj)
      {
        this.m_owner.__callDele_EventOnDrag(obj);
      }

      public void __clearDele_EventOnDrag(PointerEventData obj)
      {
        this.m_owner.__clearDele_EventOnDrag(obj);
      }

      public void __callDele_EventOnDrop(PointerEventData obj)
      {
        this.m_owner.__callDele_EventOnDrop(obj);
      }

      public void __clearDele_EventOnDrop(PointerEventData obj)
      {
        this.m_owner.__clearDele_EventOnDrop(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_playerButton
      {
        get
        {
          return this.m_owner.m_playerButton;
        }
        set
        {
          this.m_owner.m_playerButton = value;
        }
      }

      public Image m_playerTagImage
      {
        get
        {
          return this.m_owner.m_playerTagImage;
        }
        set
        {
          this.m_owner.m_playerTagImage = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public Text m_levelText
      {
        get
        {
          return this.m_owner.m_levelText;
        }
        set
        {
          this.m_owner.m_levelText = value;
        }
      }

      public Image m_headIconImage
      {
        get
        {
          return this.m_owner.m_headIconImage;
        }
        set
        {
          this.m_owner.m_headIconImage = value;
        }
      }

      public Transform m_headFrameTransform
      {
        get
        {
          return this.m_owner.m_headFrameTransform;
        }
        set
        {
          this.m_owner.m_headFrameTransform = value;
        }
      }

      public GameObject m_graphicGameObject
      {
        get
        {
          return this.m_owner.m_graphicGameObject;
        }
        set
        {
          this.m_owner.m_graphicGameObject = value;
        }
      }

      public Button m_blessingButton
      {
        get
        {
          return this.m_owner.m_blessingButton;
        }
        set
        {
          this.m_owner.m_blessingButton = value;
        }
      }

      public Button m_inviteButton
      {
        get
        {
          return this.m_owner.m_inviteButton;
        }
        set
        {
          this.m_owner.m_inviteButton = value;
        }
      }

      public CommonUIStateController m_chatUIStateController
      {
        get
        {
          return this.m_owner.m_chatUIStateController;
        }
        set
        {
          this.m_owner.m_chatUIStateController = value;
        }
      }

      public EmojiText m_dialogText
      {
        get
        {
          return this.m_owner.m_dialogText;
        }
        set
        {
          this.m_owner.m_dialogText = value;
        }
      }

      public Image m_dialogBigExpression
      {
        get
        {
          return this.m_owner.m_dialogBigExpression;
        }
        set
        {
          this.m_owner.m_dialogBigExpression = value;
        }
      }

      public int m_index
      {
        get
        {
          return this.m_owner.m_index;
        }
        set
        {
          this.m_owner.m_index = value;
        }
      }

      public TeamRoomPlayer m_player
      {
        get
        {
          return this.m_owner.m_player;
        }
        set
        {
          this.m_owner.m_player = value;
        }
      }

      public UISpineGraphic m_spineGraphic
      {
        get
        {
          return this.m_owner.m_spineGraphic;
        }
        set
        {
          this.m_owner.m_spineGraphic = value;
        }
      }

      public float m_hideChatTime
      {
        get
        {
          return this.m_owner.m_hideChatTime;
        }
        set
        {
          this.m_owner.m_hideChatTime = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetName(string name)
      {
        this.m_owner.SetName(name);
      }

      public void SetLevel(int level)
      {
        this.m_owner.SetLevel(level);
      }

      public void SetHeadIcon(int headIconId)
      {
        this.m_owner.SetHeadIcon(headIconId);
      }

      public void SetBlessing(bool blessing)
      {
        this.m_owner.SetBlessing(blessing);
      }

      public void HideChat()
      {
        this.m_owner.HideChat();
      }

      public void CreateSpineGraphic(
        ConfigDataJobConnectionInfo jobConnectionInfo,
        ConfigDataModelSkinResourceInfo skinResInfo)
      {
        this.m_owner.CreateSpineGraphic(jobConnectionInfo, skinResInfo);
      }

      public void OnInviteButtonClick()
      {
        this.m_owner.OnInviteButtonClick();
      }

      public void OnPlayerButtonClick()
      {
        this.m_owner.OnPlayerButtonClick();
      }

      public void OnBlessingButtonClick()
      {
        this.m_owner.OnBlessingButtonClick();
      }

      public void Update()
      {
        this.m_owner.Update();
      }
    }
  }
}
