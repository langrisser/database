﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.WorldUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class WorldUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Fog", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_fogImage;
    [AutoBind("./Player", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerButton;
    [AutoBind("./Player/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./Player/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./Player/Vip/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerVipText;
    [AutoBind("./Player/Exp/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerExpText;
    [AutoBind("./Player/Exp/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerExpImage;
    [AutoBind("./Player/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconImage;
    [AutoBind("./CompassButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_compassButton;
    [AutoBind("./CurrentScenario", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currentScenarioButton;
    [AutoBind("./CurrentScenario/Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currentScenarioNameText;
    [AutoBind("./NewScenario", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_newScenarioUIStateController;
    [AutoBind("./NewScenario/NameGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_newScenarioNameText;
    [AutoBind("./EnterScenario", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enterScenarioUIStateController;
    [AutoBind("./EnterScenario/Image/Chapter", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enterScenarioChapterText;
    [AutoBind("./EnterScenario/Image/Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enterScenarioNameText;
    [AutoBind("./EnterMonster", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enterMonsterUIStateController;
    [AutoBind("./EventList", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_eventListTransform;
    [AutoBind("./EventList", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_eventListUIStateController;
    [AutoBind("./EventList/RandomEventPanel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_randomEventScrollRect;
    [AutoBind("./EventList/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_eventListBackgroundButton;
    [AutoBind("./PastScenarioList", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_pastScenarioListUIStateController;
    [AutoBind("./PastScenarioList/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_pastScenarioListBackgroundButton;
    [AutoBind("./PastScenarioList/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_pastScenarioListScrollRect;
    [AutoBind("./UnlockScenario", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockScenarioUIStateController;
    [AutoBind("./UnlockScenario/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockScenarioBackgroundButton;
    [AutoBind("./UnlockScenario/Frame/Title/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockScenarioTitleText;
    [AutoBind("./UnlockScenario/Frame/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockScenarioText;
    [AutoBind("./UnlockScenario/Frame/GotoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockScenarioGotoButton;
    [AutoBind("./MainButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_mainButton;
    [AutoBind("./MainButton/ON", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mainButtonOnGameObject;
    [AutoBind("./MainButton/OFF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mainButtonOffGameObject;
    [AutoBind("./Margin1", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_margin1Transform;
    [AutoBind("./Margin1/MainButtonBar", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_mainButtonBarUIStateController;
    [AutoBind("./Margin1/MainButtonBar/HeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroButton;
    [AutoBind("./Margin1/MainButtonBar/HeroButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroButtonRedMarkGameObject;
    [AutoBind("./Margin1/MainButtonBar/BagButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bagButton;
    [AutoBind("./Margin1/MainButtonBar/MissionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_missionButton;
    [AutoBind("./Margin1/MainButtonBar/MissionButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_missionButtonRedMarkGameObject;
    [AutoBind("./Margin1/MainButtonBar/FetterButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fetterButton;
    [AutoBind("./Margin1/MainButtonBar/FetterButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_fetterButtonRedMarkGameObject;
    [AutoBind("./Margin1/MainButtonBar/StoreButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_storeButton;
    [AutoBind("./Margin1/MainButtonBar/SelectCardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectCardButton;
    [AutoBind("./Margin1/MainButtonBar/TrainingHouseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_drillButton;
    [AutoBind("./Margin1/MainButtonBar/TrainingHouseButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_drillButtonRedMark;
    [AutoBind("./Margin1/MainButtonBar/SociatyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildButton;
    [AutoBind("./Margin1/MainButtonBar/SociatyButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sociatyButtonRedMark;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/Right/", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightGameObject;
    [AutoBind("./Margin/Right/EventButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_eventButton;
    [AutoBind("./Margin/Right/EventButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_eventButtonRedMarkGameObject;
    [AutoBind("./Margin/Right/UnchartedButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unchartedButton;
    [AutoBind("./Margin/Right/ArenaButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_arenaButton;
    [AutoBind("./Margin/Right/RiftButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_riftButton;
    [AutoBind("./Margin/Right/TestButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_testButton;
    [AutoBind("./Margin/Right/TestButton2", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_testButton2;
    [AutoBind("./Margin/Right/ActiveButtonGroup/CooperateBattleButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cooperateBattleButton;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TimeLimitPackageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recommendGiftBoxButton;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TimeLimitPackageButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_recommendGiftBoxButtonAnimation;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TimeLimitPackageButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_recommendGiftBoxRedMark;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TimeLimitPackageButton/LimitPackage/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_recommendGiftBoxTimeText;
    [AutoBind("./Margin/Left/", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftGameObject;
    [AutoBind("./Margin/Left/FriendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_friendButton;
    [AutoBind("./Margin/Left/FriendButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendButtonRedMark;
    [AutoBind("./Margin/Left/MailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_mailButton;
    [AutoBind("./Margin/Left/MailButton/CountPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unreadMailCountText;
    [AutoBind("./Margin/Left/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./Margin/Left/ChatButton/CountPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_newChatCountText;
    [AutoBind("./Margin/Left/RankButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankButton;
    [AutoBind("./Margin/Left/ActivityButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_activityButton;
    [AutoBind("./Margin/Left/ActivityButton/CountPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityRedMarkGameObject;
    [AutoBind("./Margin/Left/InvestigationButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_investigationButton;
    [AutoBind("./Margin/Left/InvestigationButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_investigationButtonRedMark;
    [AutoBind("./Margin/Left/WebInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_noticeCenterButton;
    [AutoBind("./Margin/Left/WebInfoButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noticeCenterButtonRedMark;
    [AutoBind("./Margin/YYBButton/", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_YYBButton;
    [AutoBind("./Margin/OppoButton/", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_OppoButton;
    [AutoBind("./OpenServiceButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_openServiceActivityButton;
    [AutoBind("./OpenServiceButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_openServiceActivityButtonRedMark;
    [AutoBind("./BackflowButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backflowButtonActivityButton;
    [AutoBind("./BackflowButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_backflowButtonActivityButtonRedMark;
    [AutoBind("./MonthCardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_monthCardButton;
    [AutoBind("./MonthCardButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_monthCardButtonStateCtrl;
    [AutoBind("./MonthCardButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_monthCardRedMarkObj;
    [AutoBind("./MonthCardBuyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_monthCardPanelUIStateController;
    [AutoBind("./MonthCardBuyPanel/Detail/FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_monthCardItemGroupObj;
    [AutoBind("./MonthCardBuyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_monthCardCloseButton;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/RandomEventListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_randomEventListItemPrefab;
    [AutoBind("./Prefabs/PastScenarioListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_pastScenarioListItemPrefab;
    [AutoBind("./Prefabs/MonthCardDetail", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_monthCardItemPrefab;
    [AutoBind("./WorldTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ServerTimeGO;
    [AutoBind("./WorldTime/TextTime", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ServerTimeText;
    private ConfigDataRiftLevelInfo m_gotoRiftLevelInfo;
    private int m_developerClickCount;
    private Color m_fogColor;
    private List<WorldEventListItemUIController> m_randomEventListItems;
    private List<PastScenarioListItemUIController> m_pastScenarioListItems;
    private List<GameObject> m_userGuideDialogHideGameObjects;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private bool m_alwaysShowMonthCardButton;
    private List<ConfigDataMonthCardInfo> m_allMonthCardList;
    private ConfigDataGiftStoreItemInfo m_strongRecommendGiftStoreItem;
    private DateTime m_strongRecommendGiftStoreItemEndTime;
    [DoNotToLua]
    private WorldUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnEnable_hotfix;
    private LuaFunction m_UpdateDeveloperMode_hotfix;
    private LuaFunction m_ShowOrHideBoolean_hotfix;
    private LuaFunction m_UpdateGameFunctionOpen_hotfix;
    private LuaFunction m_SetPlayerNameString_hotfix;
    private LuaFunction m_SetPlayerLevelInt32_hotfix;
    private LuaFunction m_SetPlayerVipInt32_hotfix;
    private LuaFunction m_SetPlayerExpInt32Int32_hotfix;
    private LuaFunction m_SetPlayerHeadIconInt32_hotfix;
    private LuaFunction m_ShowMainButtonBarBoolean_hotfix;
    private LuaFunction m_SetActiveScenarioConfigDataScenarioInfo_hotfix;
    private LuaFunction m_ShowNewScenarioConfigDataScenarioInfoAction_hotfix;
    private LuaFunction m_ShowUnlockScenarioConfigDataScenarioInfo_hotfix;
    private LuaFunction m_ShowEnterScenarioConfigDataScenarioInfoAction_hotfix;
    private LuaFunction m_ShowEnterMonsterConfigDataBattleInfoAction_hotfix;
    private LuaFunction m_Co_EnterMonsterAction_hotfix;
    private LuaFunction m_SetFogSingle_hotfix;
    private LuaFunction m_AddRandomEventConfigDataWaypointInfoRandomEvent_hotfix;
    private LuaFunction m_ClearRandomEventList_hotfix;
    private LuaFunction m_ShowEventList_hotfix;
    private LuaFunction m_HideEventListAction_hotfix;
    private LuaFunction m_IsEventListVisible_hotfix;
    private LuaFunction m_AddPastScenarioConfigDataScenarioInfo_hotfix;
    private LuaFunction m_ClearPastScenarioList_hotfix;
    private LuaFunction m_ShowPastScenarioList_hotfix;
    private LuaFunction m_HidePastScenarioList_hotfix;
    private LuaFunction m_ShowCooperateBattleButtonBoolean_hotfix;
    private LuaFunction m_ShowHeroRedMarkBoolean_hotfix;
    private LuaFunction m_ShowDrillRedMarkBoolean_hotfix;
    private LuaFunction m_ShowFetterRedMarkBoolean_hotfix;
    private LuaFunction m_ShowNoticeCenterButtonRedMarkBoolean_hotfix;
    private LuaFunction m_ShowNoticeCenterButtonBoolean_hotfix;
    private LuaFunction m_ShowGuildRedMarkBoolean_hotfix;
    private LuaFunction m_ShowEventRedMarkBoolean_hotfix;
    private LuaFunction m_ShowMissionRedMarkBoolean_hotfix;
    private LuaFunction m_ShowActivityRedMarkBoolean_hotfix;
    private LuaFunction m_ShowFriendRedMarkBoolean_hotfix;
    private LuaFunction m_UpdateNewChatCountInt32_hotfix;
    private LuaFunction m_UpdateUnreadMailCountInt32_hotfix;
    private LuaFunction m_GetUserGuideDialogHideGameObjects_hotfix;
    private LuaFunction m_UpdateMonthCardOpen_hotfix;
    private LuaFunction m_SetMonthCardRedMarkBoolean_hotfix;
    private LuaFunction m_UpdateRecommendGiftBoxRedMark_hotfix;
    private LuaFunction m_UpdateRecommendGiftBoxTime_hotfix;
    private LuaFunction m_UpdateRecommendGiftBoxIconState_hotfix;
    private LuaFunction m_UpdateSystemTime_hotfix;
    private LuaFunction m_SetMonthCardButtonState_hotfix;
    private LuaFunction m_OpenMonthCardPanelList`1_hotfix;
    private LuaFunction m_OnMonthCardSortMonthCardMonthCard_hotfix;
    private LuaFunction m_CloseMonthCardPanel_hotfix;
    private LuaFunction m_DeveloperModeClick_hotfix;
    private LuaFunction m_OnCompassButtonClick_hotfix;
    private LuaFunction m_OnCurrentScenarioButtonClick_hotfix;
    private LuaFunction m_OnUnlockScenarioBackgroundButtonClick_hotfix;
    private LuaFunction m_OnUnlockScenarioGotoButtonClick_hotfix;
    private LuaFunction m_OnMainButtonClick_hotfix;
    private LuaFunction m_OnPlayerButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnHeroButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnBagButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnSelectCardButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnMissionButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnFetterButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnStoreButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnDrillButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnFriendButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnGuildButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnEventButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnUnchartedButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnArenaButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnRiftButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnTestButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnTestButton2ClickUIControllerBase_hotfix;
    private LuaFunction m_OnCooperateBattleButtonClick_hotfix;
    private LuaFunction m_OnRecommendGiftBoxButtonClick_hotfix;
    private LuaFunction m_OnMailButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnChatButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnRankButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnActivityButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnOpenServiceActivityButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnBackflowButtonActivityButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnOppoButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnYingYongBaoButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnNoticeCenterButtonClickUIControllerBase_hotfix;
    private LuaFunction m_OnInvestigationButtonClickUIControllerBase_hotfix;
    private LuaFunction m_UpdateInvestigationButton_hotfix;
    private LuaFunction m_UpdateOpenServiceActivityButton_hotfix;
    private LuaFunction m_UpdateBackFlowActivityButton_hotfix;
    private LuaFunction m_SaveState_hotfix;
    private LuaFunction m_WorldEventListItem_OnButtonClickWorldEventListItemUIController_hotfix;
    private LuaFunction m_OnEventListBackgroundButtonClick_hotfix;
    private LuaFunction m_OnPastScenarioListBackgroundButtonClick_hotfix;
    private LuaFunction m_PastScenarioListItem_OnStartButtonClickPastScenarioListItemUIController_hotfix;
    private LuaFunction m_OnMonthCardButtonClick_hotfix;
    private LuaFunction m_OnMonthCardCloseButtonClick_hotfix;
    private LuaFunction m_OnMonthCardItemItemClickInt32_hotfix;
    private LuaFunction m_OnMonthCardBuyClickInt32_hotfix;
    private LuaFunction m_add_EventOnShowPlayerInfoAction_hotfix;
    private LuaFunction m_remove_EventOnShowPlayerInfoAction_hotfix;
    private LuaFunction m_add_EventOnCompassAction_hotfix;
    private LuaFunction m_remove_EventOnCompassAction_hotfix;
    private LuaFunction m_add_EventOnCurrentScenarioAction_hotfix;
    private LuaFunction m_remove_EventOnCurrentScenarioAction_hotfix;
    private LuaFunction m_add_EventOnShowMainButtonBarAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowMainButtonBarAction`1_hotfix;
    private LuaFunction m_add_EventOnUnlockScenarioGotoRiftLevelAction`1_hotfix;
    private LuaFunction m_remove_EventOnUnlockScenarioGotoRiftLevelAction`1_hotfix;
    private LuaFunction m_add_EventOnGotoEventAction`2_hotfix;
    private LuaFunction m_remove_EventOnGotoEventAction`2_hotfix;
    private LuaFunction m_add_EventOnStartPastScenarioAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartPastScenarioAction`1_hotfix;
    private LuaFunction m_add_EventOnClosePastScenarioListAction_hotfix;
    private LuaFunction m_remove_EventOnClosePastScenarioListAction_hotfix;
    private LuaFunction m_add_EventOnShowHeroAction_hotfix;
    private LuaFunction m_remove_EventOnShowHeroAction_hotfix;
    private LuaFunction m_add_EventOnShowBagAction_hotfix;
    private LuaFunction m_remove_EventOnShowBagAction_hotfix;
    private LuaFunction m_add_EventOnShowSelectCardAction_hotfix;
    private LuaFunction m_remove_EventOnShowSelectCardAction_hotfix;
    private LuaFunction m_add_EventOnShowMisisionAction_hotfix;
    private LuaFunction m_remove_EventOnShowMisisionAction_hotfix;
    private LuaFunction m_add_EventOnShowFetterAction_hotfix;
    private LuaFunction m_remove_EventOnShowFetterAction_hotfix;
    private LuaFunction m_add_EventOnShowStoreAction_hotfix;
    private LuaFunction m_remove_EventOnShowStoreAction_hotfix;
    private LuaFunction m_add_EventOnShowDrillAction_hotfix;
    private LuaFunction m_remove_EventOnShowDrillAction_hotfix;
    private LuaFunction m_add_EventOnShowFriendAction_hotfix;
    private LuaFunction m_remove_EventOnShowFriendAction_hotfix;
    private LuaFunction m_add_EventOnShowGuildAction_hotfix;
    private LuaFunction m_remove_EventOnShowGuildAction_hotfix;
    private LuaFunction m_add_EventOnShowEventAction_hotfix;
    private LuaFunction m_remove_EventOnShowEventAction_hotfix;
    private LuaFunction m_add_EventOnShowUnchartedAction_hotfix;
    private LuaFunction m_remove_EventOnShowUnchartedAction_hotfix;
    private LuaFunction m_add_EventOnShowArenaAction_hotfix;
    private LuaFunction m_remove_EventOnShowArenaAction_hotfix;
    private LuaFunction m_add_EventOnShowRiftAction_hotfix;
    private LuaFunction m_remove_EventOnShowRiftAction_hotfix;
    private LuaFunction m_add_EventOnShowTestAction_hotfix;
    private LuaFunction m_remove_EventOnShowTestAction_hotfix;
    private LuaFunction m_add_EventOnShowTest2Action_hotfix;
    private LuaFunction m_remove_EventOnShowTest2Action_hotfix;
    private LuaFunction m_add_EventOnShowCooperateBattleAction_hotfix;
    private LuaFunction m_remove_EventOnShowCooperateBattleAction_hotfix;
    private LuaFunction m_add_EventOnRecommendGiftBoxAction_hotfix;
    private LuaFunction m_remove_EventOnRecommendGiftBoxAction_hotfix;
    private LuaFunction m_add_EventOnShowMailAction_hotfix;
    private LuaFunction m_remove_EventOnShowMailAction_hotfix;
    private LuaFunction m_add_EventOnShowChatAction_hotfix;
    private LuaFunction m_remove_EventOnShowChatAction_hotfix;
    private LuaFunction m_add_EventOnShowRankingAction_hotfix;
    private LuaFunction m_remove_EventOnShowRankingAction_hotfix;
    private LuaFunction m_add_EventOnShowAnnouncementAction_hotfix;
    private LuaFunction m_remove_EventOnShowAnnouncementAction_hotfix;
    private LuaFunction m_add_EventOnOpenWebInvestigationAction_hotfix;
    private LuaFunction m_remove_EventOnOpenWebInvestigationAction_hotfix;
    private LuaFunction m_add_EventOnNoticeCenterButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnNoticeCenterButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnShowOpenServiceActivityAction_hotfix;
    private LuaFunction m_remove_EventOnShowOpenServiceActivityAction_hotfix;
    private LuaFunction m_add_EventOnShowBackFlowActivityAction_hotfix;
    private LuaFunction m_remove_EventOnShowBackFlowActivityAction_hotfix;
    private LuaFunction m_add_EventOnMonthCardButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnMonthCardButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnRefreshMonthCardPanelAction`1_hotfix;
    private LuaFunction m_remove_EventOnRefreshMonthCardPanelAction`1_hotfix;
    private LuaFunction m_add_EventOnMonthCardItemClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnMonthCardItemClickAction`1_hotfix;
    private LuaFunction m_add_EventOnMonthCardItemBuyClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnMonthCardItemBuyClickAction`1_hotfix;
    private LuaFunction m_add_EventOnYYBButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnYYBButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnOppoButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnOppoButtonClickAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private WorldUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateDeveloperMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOrHide(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGameFunctionOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerVip(int vip)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerExp(int exp, int expMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerHeadIcon(int playerHeadIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMainButtonBar(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActiveScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNewScenario(ConfigDataScenarioInfo scenarioInfo, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowUnlockScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnterScenario(ConfigDataScenarioInfo scenarioInfo, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnterMonster(ConfigDataBattleInfo battleInfo, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_EnterMonster(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFog(float fog)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRandomEvent(ConfigDataWaypointInfo waypointInfo, RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearRandomEventList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEventList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideEventList(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEventListVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddPastScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearPastScenarioList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPastScenarioList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HidePastScenarioList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCooperateBattleButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowHeroRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDrillRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFetterRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNoticeCenterButtonRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNoticeCenterButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowGuildRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEventRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMissionRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowActivityRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFriendRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateNewChatCount(int newCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateUnreadMailCount(int newCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GameObject> GetUserGuideDialogHideGameObjects()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateMonthCardOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMonthCardRedMark(bool flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRecommendGiftBoxRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRecommendGiftBoxTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRecommendGiftBoxIconState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateSystemTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMonthCardButtonState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenMonthCardPanel(List<MonthCard> allCardList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int OnMonthCardSort(MonthCard x, MonthCard y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseMonthCardPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DeveloperModeClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompassButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCurrentScenarioButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockScenarioBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockScenarioGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMainButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBagButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectCardButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFetterButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStoreButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDrillButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEventButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnchartedButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArenaButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRiftButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestButton2Click(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCooperateBattleButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecommendGiftBoxButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMailButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActivityButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOpenServiceActivityButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackflowButtonActivityButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOppoButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnYingYongBaoButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeCenterButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInvestigationButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateInvestigationButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateOpenServiceActivityButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateBackFlowActivityButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldEventListItem_OnButtonClick(WorldEventListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEventListBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPastScenarioListBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PastScenarioListItem_OnStartButtonClick(PastScenarioListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMonthCardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMonthCardCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMonthCardItemItemClick(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMonthCardBuyClick(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnShowPlayerInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCompass
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCurrentScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnShowMainButtonBar
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataRiftLevelInfo> EventOnUnlockScenarioGotoRiftLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataWaypointInfo, ConfigDataEventInfo> EventOnGotoEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataScenarioInfo> EventOnStartPastScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClosePastScenarioList
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHero
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowBag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowSelectCard
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowMisision
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowFetter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowDrill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowFriend
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowGuild
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowUncharted
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowArena
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowRift
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowTest
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowTest2
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowCooperateBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRecommendGiftBox
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowMail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowRanking
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowAnnouncement
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnOpenWebInvestigation
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnNoticeCenterButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowOpenServiceActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowBackFlowActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnMonthCardButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnRefreshMonthCardPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnMonthCardItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnMonthCardItemBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnYYBButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnOppoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public WorldUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowPlayerInfo()
    {
      this.EventOnShowPlayerInfo = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCompass()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCompass()
    {
      this.EventOnCompass = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCurrentScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCurrentScenario()
    {
      this.EventOnCurrentScenario = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowMainButtonBar(bool obj)
    {
    }

    private void __clearDele_EventOnShowMainButtonBar(bool obj)
    {
      this.EventOnShowMainButtonBar = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnUnlockScenarioGotoRiftLevel(ConfigDataRiftLevelInfo obj)
    {
    }

    private void __clearDele_EventOnUnlockScenarioGotoRiftLevel(ConfigDataRiftLevelInfo obj)
    {
      this.EventOnUnlockScenarioGotoRiftLevel = (Action<ConfigDataRiftLevelInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGotoEvent(ConfigDataWaypointInfo arg1, ConfigDataEventInfo arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGotoEvent(ConfigDataWaypointInfo arg1, ConfigDataEventInfo arg2)
    {
      this.EventOnGotoEvent = (Action<ConfigDataWaypointInfo, ConfigDataEventInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartPastScenario(ConfigDataScenarioInfo obj)
    {
    }

    private void __clearDele_EventOnStartPastScenario(ConfigDataScenarioInfo obj)
    {
      this.EventOnStartPastScenario = (Action<ConfigDataScenarioInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClosePastScenarioList()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClosePastScenarioList()
    {
      this.EventOnClosePastScenarioList = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHero()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHero()
    {
      this.EventOnShowHero = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowBag()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowBag()
    {
      this.EventOnShowBag = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowSelectCard()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowSelectCard()
    {
      this.EventOnShowSelectCard = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowMisision()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowMisision()
    {
      this.EventOnShowMisision = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowFetter()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowFetter()
    {
      this.EventOnShowFetter = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowStore()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowStore()
    {
      this.EventOnShowStore = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowDrill()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowDrill()
    {
      this.EventOnShowDrill = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowFriend()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowFriend()
    {
      this.EventOnShowFriend = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowGuild()
    {
      this.EventOnShowGuild = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowEvent()
    {
      this.EventOnShowEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowUncharted()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowUncharted()
    {
      this.EventOnShowUncharted = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowArena()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowArena()
    {
      this.EventOnShowArena = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowRift()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowRift()
    {
      this.EventOnShowRift = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowTest()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowTest()
    {
      this.EventOnShowTest = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowTest2()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowTest2()
    {
      this.EventOnShowTest2 = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowCooperateBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowCooperateBattle()
    {
      this.EventOnShowCooperateBattle = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRecommendGiftBox()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRecommendGiftBox()
    {
      this.EventOnRecommendGiftBox = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowMail()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowMail()
    {
      this.EventOnShowMail = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowChat()
    {
      this.EventOnShowChat = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowRanking()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowRanking()
    {
      this.EventOnShowRanking = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowAnnouncement()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowAnnouncement()
    {
      this.EventOnShowAnnouncement = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnOpenWebInvestigation()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnOpenWebInvestigation()
    {
      this.EventOnOpenWebInvestigation = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnNoticeCenterButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnNoticeCenterButtonClick()
    {
      this.EventOnNoticeCenterButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowOpenServiceActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowOpenServiceActivity()
    {
      this.EventOnShowOpenServiceActivity = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowBackFlowActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowBackFlowActivity()
    {
      this.EventOnShowBackFlowActivity = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnMonthCardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnMonthCardButtonClick()
    {
      this.EventOnMonthCardButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRefreshMonthCardPanel(bool obj)
    {
    }

    private void __clearDele_EventOnRefreshMonthCardPanel(bool obj)
    {
      this.EventOnRefreshMonthCardPanel = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnMonthCardItemClick(int obj)
    {
    }

    private void __clearDele_EventOnMonthCardItemClick(int obj)
    {
      this.EventOnMonthCardItemClick = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnMonthCardItemBuyClick(int obj)
    {
    }

    private void __clearDele_EventOnMonthCardItemBuyClick(int obj)
    {
      this.EventOnMonthCardItemBuyClick = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnYYBButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnYYBButtonClick()
    {
      this.EventOnYYBButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnOppoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnOppoButtonClick()
    {
      this.EventOnOppoButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private WorldUIController m_owner;

      public LuaExportHelper(WorldUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnShowPlayerInfo()
      {
        this.m_owner.__callDele_EventOnShowPlayerInfo();
      }

      public void __clearDele_EventOnShowPlayerInfo()
      {
        this.m_owner.__clearDele_EventOnShowPlayerInfo();
      }

      public void __callDele_EventOnCompass()
      {
        this.m_owner.__callDele_EventOnCompass();
      }

      public void __clearDele_EventOnCompass()
      {
        this.m_owner.__clearDele_EventOnCompass();
      }

      public void __callDele_EventOnCurrentScenario()
      {
        this.m_owner.__callDele_EventOnCurrentScenario();
      }

      public void __clearDele_EventOnCurrentScenario()
      {
        this.m_owner.__clearDele_EventOnCurrentScenario();
      }

      public void __callDele_EventOnShowMainButtonBar(bool obj)
      {
        this.m_owner.__callDele_EventOnShowMainButtonBar(obj);
      }

      public void __clearDele_EventOnShowMainButtonBar(bool obj)
      {
        this.m_owner.__clearDele_EventOnShowMainButtonBar(obj);
      }

      public void __callDele_EventOnUnlockScenarioGotoRiftLevel(ConfigDataRiftLevelInfo obj)
      {
        this.m_owner.__callDele_EventOnUnlockScenarioGotoRiftLevel(obj);
      }

      public void __clearDele_EventOnUnlockScenarioGotoRiftLevel(ConfigDataRiftLevelInfo obj)
      {
        this.m_owner.__clearDele_EventOnUnlockScenarioGotoRiftLevel(obj);
      }

      public void __callDele_EventOnGotoEvent(ConfigDataWaypointInfo arg1, ConfigDataEventInfo arg2)
      {
        this.m_owner.__callDele_EventOnGotoEvent(arg1, arg2);
      }

      public void __clearDele_EventOnGotoEvent(
        ConfigDataWaypointInfo arg1,
        ConfigDataEventInfo arg2)
      {
        this.m_owner.__clearDele_EventOnGotoEvent(arg1, arg2);
      }

      public void __callDele_EventOnStartPastScenario(ConfigDataScenarioInfo obj)
      {
        this.m_owner.__callDele_EventOnStartPastScenario(obj);
      }

      public void __clearDele_EventOnStartPastScenario(ConfigDataScenarioInfo obj)
      {
        this.m_owner.__clearDele_EventOnStartPastScenario(obj);
      }

      public void __callDele_EventOnClosePastScenarioList()
      {
        this.m_owner.__callDele_EventOnClosePastScenarioList();
      }

      public void __clearDele_EventOnClosePastScenarioList()
      {
        this.m_owner.__clearDele_EventOnClosePastScenarioList();
      }

      public void __callDele_EventOnShowHero()
      {
        this.m_owner.__callDele_EventOnShowHero();
      }

      public void __clearDele_EventOnShowHero()
      {
        this.m_owner.__clearDele_EventOnShowHero();
      }

      public void __callDele_EventOnShowBag()
      {
        this.m_owner.__callDele_EventOnShowBag();
      }

      public void __clearDele_EventOnShowBag()
      {
        this.m_owner.__clearDele_EventOnShowBag();
      }

      public void __callDele_EventOnShowSelectCard()
      {
        this.m_owner.__callDele_EventOnShowSelectCard();
      }

      public void __clearDele_EventOnShowSelectCard()
      {
        this.m_owner.__clearDele_EventOnShowSelectCard();
      }

      public void __callDele_EventOnShowMisision()
      {
        this.m_owner.__callDele_EventOnShowMisision();
      }

      public void __clearDele_EventOnShowMisision()
      {
        this.m_owner.__clearDele_EventOnShowMisision();
      }

      public void __callDele_EventOnShowFetter()
      {
        this.m_owner.__callDele_EventOnShowFetter();
      }

      public void __clearDele_EventOnShowFetter()
      {
        this.m_owner.__clearDele_EventOnShowFetter();
      }

      public void __callDele_EventOnShowStore()
      {
        this.m_owner.__callDele_EventOnShowStore();
      }

      public void __clearDele_EventOnShowStore()
      {
        this.m_owner.__clearDele_EventOnShowStore();
      }

      public void __callDele_EventOnShowDrill()
      {
        this.m_owner.__callDele_EventOnShowDrill();
      }

      public void __clearDele_EventOnShowDrill()
      {
        this.m_owner.__clearDele_EventOnShowDrill();
      }

      public void __callDele_EventOnShowFriend()
      {
        this.m_owner.__callDele_EventOnShowFriend();
      }

      public void __clearDele_EventOnShowFriend()
      {
        this.m_owner.__clearDele_EventOnShowFriend();
      }

      public void __callDele_EventOnShowGuild()
      {
        this.m_owner.__callDele_EventOnShowGuild();
      }

      public void __clearDele_EventOnShowGuild()
      {
        this.m_owner.__clearDele_EventOnShowGuild();
      }

      public void __callDele_EventOnShowEvent()
      {
        this.m_owner.__callDele_EventOnShowEvent();
      }

      public void __clearDele_EventOnShowEvent()
      {
        this.m_owner.__clearDele_EventOnShowEvent();
      }

      public void __callDele_EventOnShowUncharted()
      {
        this.m_owner.__callDele_EventOnShowUncharted();
      }

      public void __clearDele_EventOnShowUncharted()
      {
        this.m_owner.__clearDele_EventOnShowUncharted();
      }

      public void __callDele_EventOnShowArena()
      {
        this.m_owner.__callDele_EventOnShowArena();
      }

      public void __clearDele_EventOnShowArena()
      {
        this.m_owner.__clearDele_EventOnShowArena();
      }

      public void __callDele_EventOnShowRift()
      {
        this.m_owner.__callDele_EventOnShowRift();
      }

      public void __clearDele_EventOnShowRift()
      {
        this.m_owner.__clearDele_EventOnShowRift();
      }

      public void __callDele_EventOnShowTest()
      {
        this.m_owner.__callDele_EventOnShowTest();
      }

      public void __clearDele_EventOnShowTest()
      {
        this.m_owner.__clearDele_EventOnShowTest();
      }

      public void __callDele_EventOnShowTest2()
      {
        this.m_owner.__callDele_EventOnShowTest2();
      }

      public void __clearDele_EventOnShowTest2()
      {
        this.m_owner.__clearDele_EventOnShowTest2();
      }

      public void __callDele_EventOnShowCooperateBattle()
      {
        this.m_owner.__callDele_EventOnShowCooperateBattle();
      }

      public void __clearDele_EventOnShowCooperateBattle()
      {
        this.m_owner.__clearDele_EventOnShowCooperateBattle();
      }

      public void __callDele_EventOnRecommendGiftBox()
      {
        this.m_owner.__callDele_EventOnRecommendGiftBox();
      }

      public void __clearDele_EventOnRecommendGiftBox()
      {
        this.m_owner.__clearDele_EventOnRecommendGiftBox();
      }

      public void __callDele_EventOnShowMail()
      {
        this.m_owner.__callDele_EventOnShowMail();
      }

      public void __clearDele_EventOnShowMail()
      {
        this.m_owner.__clearDele_EventOnShowMail();
      }

      public void __callDele_EventOnShowChat()
      {
        this.m_owner.__callDele_EventOnShowChat();
      }

      public void __clearDele_EventOnShowChat()
      {
        this.m_owner.__clearDele_EventOnShowChat();
      }

      public void __callDele_EventOnShowRanking()
      {
        this.m_owner.__callDele_EventOnShowRanking();
      }

      public void __clearDele_EventOnShowRanking()
      {
        this.m_owner.__clearDele_EventOnShowRanking();
      }

      public void __callDele_EventOnShowAnnouncement()
      {
        this.m_owner.__callDele_EventOnShowAnnouncement();
      }

      public void __clearDele_EventOnShowAnnouncement()
      {
        this.m_owner.__clearDele_EventOnShowAnnouncement();
      }

      public void __callDele_EventOnOpenWebInvestigation()
      {
        this.m_owner.__callDele_EventOnOpenWebInvestigation();
      }

      public void __clearDele_EventOnOpenWebInvestigation()
      {
        this.m_owner.__clearDele_EventOnOpenWebInvestigation();
      }

      public void __callDele_EventOnNoticeCenterButtonClick()
      {
        this.m_owner.__callDele_EventOnNoticeCenterButtonClick();
      }

      public void __clearDele_EventOnNoticeCenterButtonClick()
      {
        this.m_owner.__clearDele_EventOnNoticeCenterButtonClick();
      }

      public void __callDele_EventOnShowOpenServiceActivity()
      {
        this.m_owner.__callDele_EventOnShowOpenServiceActivity();
      }

      public void __clearDele_EventOnShowOpenServiceActivity()
      {
        this.m_owner.__clearDele_EventOnShowOpenServiceActivity();
      }

      public void __callDele_EventOnShowBackFlowActivity()
      {
        this.m_owner.__callDele_EventOnShowBackFlowActivity();
      }

      public void __clearDele_EventOnShowBackFlowActivity()
      {
        this.m_owner.__clearDele_EventOnShowBackFlowActivity();
      }

      public void __callDele_EventOnMonthCardButtonClick()
      {
        this.m_owner.__callDele_EventOnMonthCardButtonClick();
      }

      public void __clearDele_EventOnMonthCardButtonClick()
      {
        this.m_owner.__clearDele_EventOnMonthCardButtonClick();
      }

      public void __callDele_EventOnRefreshMonthCardPanel(bool obj)
      {
        this.m_owner.__callDele_EventOnRefreshMonthCardPanel(obj);
      }

      public void __clearDele_EventOnRefreshMonthCardPanel(bool obj)
      {
        this.m_owner.__clearDele_EventOnRefreshMonthCardPanel(obj);
      }

      public void __callDele_EventOnMonthCardItemClick(int obj)
      {
        this.m_owner.__callDele_EventOnMonthCardItemClick(obj);
      }

      public void __clearDele_EventOnMonthCardItemClick(int obj)
      {
        this.m_owner.__clearDele_EventOnMonthCardItemClick(obj);
      }

      public void __callDele_EventOnMonthCardItemBuyClick(int obj)
      {
        this.m_owner.__callDele_EventOnMonthCardItemBuyClick(obj);
      }

      public void __clearDele_EventOnMonthCardItemBuyClick(int obj)
      {
        this.m_owner.__clearDele_EventOnMonthCardItemBuyClick(obj);
      }

      public void __callDele_EventOnYYBButtonClick()
      {
        this.m_owner.__callDele_EventOnYYBButtonClick();
      }

      public void __clearDele_EventOnYYBButtonClick()
      {
        this.m_owner.__clearDele_EventOnYYBButtonClick();
      }

      public void __callDele_EventOnOppoButtonClick()
      {
        this.m_owner.__callDele_EventOnOppoButtonClick();
      }

      public void __clearDele_EventOnOppoButtonClick()
      {
        this.m_owner.__clearDele_EventOnOppoButtonClick();
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Image m_fogImage
      {
        get
        {
          return this.m_owner.m_fogImage;
        }
        set
        {
          this.m_owner.m_fogImage = value;
        }
      }

      public Button m_playerButton
      {
        get
        {
          return this.m_owner.m_playerButton;
        }
        set
        {
          this.m_owner.m_playerButton = value;
        }
      }

      public Text m_playerNameText
      {
        get
        {
          return this.m_owner.m_playerNameText;
        }
        set
        {
          this.m_owner.m_playerNameText = value;
        }
      }

      public Text m_playerLevelText
      {
        get
        {
          return this.m_owner.m_playerLevelText;
        }
        set
        {
          this.m_owner.m_playerLevelText = value;
        }
      }

      public Text m_playerVipText
      {
        get
        {
          return this.m_owner.m_playerVipText;
        }
        set
        {
          this.m_owner.m_playerVipText = value;
        }
      }

      public Text m_playerExpText
      {
        get
        {
          return this.m_owner.m_playerExpText;
        }
        set
        {
          this.m_owner.m_playerExpText = value;
        }
      }

      public Image m_playerExpImage
      {
        get
        {
          return this.m_owner.m_playerExpImage;
        }
        set
        {
          this.m_owner.m_playerExpImage = value;
        }
      }

      public Image m_playerIconImage
      {
        get
        {
          return this.m_owner.m_playerIconImage;
        }
        set
        {
          this.m_owner.m_playerIconImage = value;
        }
      }

      public Button m_compassButton
      {
        get
        {
          return this.m_owner.m_compassButton;
        }
        set
        {
          this.m_owner.m_compassButton = value;
        }
      }

      public Button m_currentScenarioButton
      {
        get
        {
          return this.m_owner.m_currentScenarioButton;
        }
        set
        {
          this.m_owner.m_currentScenarioButton = value;
        }
      }

      public Text m_currentScenarioNameText
      {
        get
        {
          return this.m_owner.m_currentScenarioNameText;
        }
        set
        {
          this.m_owner.m_currentScenarioNameText = value;
        }
      }

      public CommonUIStateController m_newScenarioUIStateController
      {
        get
        {
          return this.m_owner.m_newScenarioUIStateController;
        }
        set
        {
          this.m_owner.m_newScenarioUIStateController = value;
        }
      }

      public Text m_newScenarioNameText
      {
        get
        {
          return this.m_owner.m_newScenarioNameText;
        }
        set
        {
          this.m_owner.m_newScenarioNameText = value;
        }
      }

      public CommonUIStateController m_enterScenarioUIStateController
      {
        get
        {
          return this.m_owner.m_enterScenarioUIStateController;
        }
        set
        {
          this.m_owner.m_enterScenarioUIStateController = value;
        }
      }

      public Text m_enterScenarioChapterText
      {
        get
        {
          return this.m_owner.m_enterScenarioChapterText;
        }
        set
        {
          this.m_owner.m_enterScenarioChapterText = value;
        }
      }

      public Text m_enterScenarioNameText
      {
        get
        {
          return this.m_owner.m_enterScenarioNameText;
        }
        set
        {
          this.m_owner.m_enterScenarioNameText = value;
        }
      }

      public CommonUIStateController m_enterMonsterUIStateController
      {
        get
        {
          return this.m_owner.m_enterMonsterUIStateController;
        }
        set
        {
          this.m_owner.m_enterMonsterUIStateController = value;
        }
      }

      public RectTransform m_eventListTransform
      {
        get
        {
          return this.m_owner.m_eventListTransform;
        }
        set
        {
          this.m_owner.m_eventListTransform = value;
        }
      }

      public CommonUIStateController m_eventListUIStateController
      {
        get
        {
          return this.m_owner.m_eventListUIStateController;
        }
        set
        {
          this.m_owner.m_eventListUIStateController = value;
        }
      }

      public ScrollRect m_randomEventScrollRect
      {
        get
        {
          return this.m_owner.m_randomEventScrollRect;
        }
        set
        {
          this.m_owner.m_randomEventScrollRect = value;
        }
      }

      public Button m_eventListBackgroundButton
      {
        get
        {
          return this.m_owner.m_eventListBackgroundButton;
        }
        set
        {
          this.m_owner.m_eventListBackgroundButton = value;
        }
      }

      public CommonUIStateController m_pastScenarioListUIStateController
      {
        get
        {
          return this.m_owner.m_pastScenarioListUIStateController;
        }
        set
        {
          this.m_owner.m_pastScenarioListUIStateController = value;
        }
      }

      public Button m_pastScenarioListBackgroundButton
      {
        get
        {
          return this.m_owner.m_pastScenarioListBackgroundButton;
        }
        set
        {
          this.m_owner.m_pastScenarioListBackgroundButton = value;
        }
      }

      public ScrollRect m_pastScenarioListScrollRect
      {
        get
        {
          return this.m_owner.m_pastScenarioListScrollRect;
        }
        set
        {
          this.m_owner.m_pastScenarioListScrollRect = value;
        }
      }

      public CommonUIStateController m_unlockScenarioUIStateController
      {
        get
        {
          return this.m_owner.m_unlockScenarioUIStateController;
        }
        set
        {
          this.m_owner.m_unlockScenarioUIStateController = value;
        }
      }

      public Button m_unlockScenarioBackgroundButton
      {
        get
        {
          return this.m_owner.m_unlockScenarioBackgroundButton;
        }
        set
        {
          this.m_owner.m_unlockScenarioBackgroundButton = value;
        }
      }

      public Text m_unlockScenarioTitleText
      {
        get
        {
          return this.m_owner.m_unlockScenarioTitleText;
        }
        set
        {
          this.m_owner.m_unlockScenarioTitleText = value;
        }
      }

      public Text m_unlockScenarioText
      {
        get
        {
          return this.m_owner.m_unlockScenarioText;
        }
        set
        {
          this.m_owner.m_unlockScenarioText = value;
        }
      }

      public Button m_unlockScenarioGotoButton
      {
        get
        {
          return this.m_owner.m_unlockScenarioGotoButton;
        }
        set
        {
          this.m_owner.m_unlockScenarioGotoButton = value;
        }
      }

      public Button m_mainButton
      {
        get
        {
          return this.m_owner.m_mainButton;
        }
        set
        {
          this.m_owner.m_mainButton = value;
        }
      }

      public GameObject m_mainButtonOnGameObject
      {
        get
        {
          return this.m_owner.m_mainButtonOnGameObject;
        }
        set
        {
          this.m_owner.m_mainButtonOnGameObject = value;
        }
      }

      public GameObject m_mainButtonOffGameObject
      {
        get
        {
          return this.m_owner.m_mainButtonOffGameObject;
        }
        set
        {
          this.m_owner.m_mainButtonOffGameObject = value;
        }
      }

      public RectTransform m_margin1Transform
      {
        get
        {
          return this.m_owner.m_margin1Transform;
        }
        set
        {
          this.m_owner.m_margin1Transform = value;
        }
      }

      public CommonUIStateController m_mainButtonBarUIStateController
      {
        get
        {
          return this.m_owner.m_mainButtonBarUIStateController;
        }
        set
        {
          this.m_owner.m_mainButtonBarUIStateController = value;
        }
      }

      public Button m_heroButton
      {
        get
        {
          return this.m_owner.m_heroButton;
        }
        set
        {
          this.m_owner.m_heroButton = value;
        }
      }

      public GameObject m_heroButtonRedMarkGameObject
      {
        get
        {
          return this.m_owner.m_heroButtonRedMarkGameObject;
        }
        set
        {
          this.m_owner.m_heroButtonRedMarkGameObject = value;
        }
      }

      public Button m_bagButton
      {
        get
        {
          return this.m_owner.m_bagButton;
        }
        set
        {
          this.m_owner.m_bagButton = value;
        }
      }

      public Button m_missionButton
      {
        get
        {
          return this.m_owner.m_missionButton;
        }
        set
        {
          this.m_owner.m_missionButton = value;
        }
      }

      public GameObject m_missionButtonRedMarkGameObject
      {
        get
        {
          return this.m_owner.m_missionButtonRedMarkGameObject;
        }
        set
        {
          this.m_owner.m_missionButtonRedMarkGameObject = value;
        }
      }

      public Button m_fetterButton
      {
        get
        {
          return this.m_owner.m_fetterButton;
        }
        set
        {
          this.m_owner.m_fetterButton = value;
        }
      }

      public GameObject m_fetterButtonRedMarkGameObject
      {
        get
        {
          return this.m_owner.m_fetterButtonRedMarkGameObject;
        }
        set
        {
          this.m_owner.m_fetterButtonRedMarkGameObject = value;
        }
      }

      public Button m_storeButton
      {
        get
        {
          return this.m_owner.m_storeButton;
        }
        set
        {
          this.m_owner.m_storeButton = value;
        }
      }

      public Button m_selectCardButton
      {
        get
        {
          return this.m_owner.m_selectCardButton;
        }
        set
        {
          this.m_owner.m_selectCardButton = value;
        }
      }

      public Button m_drillButton
      {
        get
        {
          return this.m_owner.m_drillButton;
        }
        set
        {
          this.m_owner.m_drillButton = value;
        }
      }

      public GameObject m_drillButtonRedMark
      {
        get
        {
          return this.m_owner.m_drillButtonRedMark;
        }
        set
        {
          this.m_owner.m_drillButtonRedMark = value;
        }
      }

      public Button m_guildButton
      {
        get
        {
          return this.m_owner.m_guildButton;
        }
        set
        {
          this.m_owner.m_guildButton = value;
        }
      }

      public GameObject m_sociatyButtonRedMark
      {
        get
        {
          return this.m_owner.m_sociatyButtonRedMark;
        }
        set
        {
          this.m_owner.m_sociatyButtonRedMark = value;
        }
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public GameObject m_rightGameObject
      {
        get
        {
          return this.m_owner.m_rightGameObject;
        }
        set
        {
          this.m_owner.m_rightGameObject = value;
        }
      }

      public Button m_eventButton
      {
        get
        {
          return this.m_owner.m_eventButton;
        }
        set
        {
          this.m_owner.m_eventButton = value;
        }
      }

      public GameObject m_eventButtonRedMarkGameObject
      {
        get
        {
          return this.m_owner.m_eventButtonRedMarkGameObject;
        }
        set
        {
          this.m_owner.m_eventButtonRedMarkGameObject = value;
        }
      }

      public Button m_unchartedButton
      {
        get
        {
          return this.m_owner.m_unchartedButton;
        }
        set
        {
          this.m_owner.m_unchartedButton = value;
        }
      }

      public Button m_arenaButton
      {
        get
        {
          return this.m_owner.m_arenaButton;
        }
        set
        {
          this.m_owner.m_arenaButton = value;
        }
      }

      public Button m_riftButton
      {
        get
        {
          return this.m_owner.m_riftButton;
        }
        set
        {
          this.m_owner.m_riftButton = value;
        }
      }

      public Button m_testButton
      {
        get
        {
          return this.m_owner.m_testButton;
        }
        set
        {
          this.m_owner.m_testButton = value;
        }
      }

      public Button m_testButton2
      {
        get
        {
          return this.m_owner.m_testButton2;
        }
        set
        {
          this.m_owner.m_testButton2 = value;
        }
      }

      public Button m_cooperateBattleButton
      {
        get
        {
          return this.m_owner.m_cooperateBattleButton;
        }
        set
        {
          this.m_owner.m_cooperateBattleButton = value;
        }
      }

      public Button m_recommendGiftBoxButton
      {
        get
        {
          return this.m_owner.m_recommendGiftBoxButton;
        }
        set
        {
          this.m_owner.m_recommendGiftBoxButton = value;
        }
      }

      public CommonUIStateController m_recommendGiftBoxButtonAnimation
      {
        get
        {
          return this.m_owner.m_recommendGiftBoxButtonAnimation;
        }
        set
        {
          this.m_owner.m_recommendGiftBoxButtonAnimation = value;
        }
      }

      public GameObject m_recommendGiftBoxRedMark
      {
        get
        {
          return this.m_owner.m_recommendGiftBoxRedMark;
        }
        set
        {
          this.m_owner.m_recommendGiftBoxRedMark = value;
        }
      }

      public Text m_recommendGiftBoxTimeText
      {
        get
        {
          return this.m_owner.m_recommendGiftBoxTimeText;
        }
        set
        {
          this.m_owner.m_recommendGiftBoxTimeText = value;
        }
      }

      public GameObject m_leftGameObject
      {
        get
        {
          return this.m_owner.m_leftGameObject;
        }
        set
        {
          this.m_owner.m_leftGameObject = value;
        }
      }

      public Button m_friendButton
      {
        get
        {
          return this.m_owner.m_friendButton;
        }
        set
        {
          this.m_owner.m_friendButton = value;
        }
      }

      public GameObject m_friendButtonRedMark
      {
        get
        {
          return this.m_owner.m_friendButtonRedMark;
        }
        set
        {
          this.m_owner.m_friendButtonRedMark = value;
        }
      }

      public Button m_mailButton
      {
        get
        {
          return this.m_owner.m_mailButton;
        }
        set
        {
          this.m_owner.m_mailButton = value;
        }
      }

      public Text m_unreadMailCountText
      {
        get
        {
          return this.m_owner.m_unreadMailCountText;
        }
        set
        {
          this.m_owner.m_unreadMailCountText = value;
        }
      }

      public Button m_chatButton
      {
        get
        {
          return this.m_owner.m_chatButton;
        }
        set
        {
          this.m_owner.m_chatButton = value;
        }
      }

      public Text m_newChatCountText
      {
        get
        {
          return this.m_owner.m_newChatCountText;
        }
        set
        {
          this.m_owner.m_newChatCountText = value;
        }
      }

      public Button m_rankButton
      {
        get
        {
          return this.m_owner.m_rankButton;
        }
        set
        {
          this.m_owner.m_rankButton = value;
        }
      }

      public Button m_activityButton
      {
        get
        {
          return this.m_owner.m_activityButton;
        }
        set
        {
          this.m_owner.m_activityButton = value;
        }
      }

      public GameObject m_activityRedMarkGameObject
      {
        get
        {
          return this.m_owner.m_activityRedMarkGameObject;
        }
        set
        {
          this.m_owner.m_activityRedMarkGameObject = value;
        }
      }

      public Button m_investigationButton
      {
        get
        {
          return this.m_owner.m_investigationButton;
        }
        set
        {
          this.m_owner.m_investigationButton = value;
        }
      }

      public GameObject m_investigationButtonRedMark
      {
        get
        {
          return this.m_owner.m_investigationButtonRedMark;
        }
        set
        {
          this.m_owner.m_investigationButtonRedMark = value;
        }
      }

      public Button m_noticeCenterButton
      {
        get
        {
          return this.m_owner.m_noticeCenterButton;
        }
        set
        {
          this.m_owner.m_noticeCenterButton = value;
        }
      }

      public GameObject m_noticeCenterButtonRedMark
      {
        get
        {
          return this.m_owner.m_noticeCenterButtonRedMark;
        }
        set
        {
          this.m_owner.m_noticeCenterButtonRedMark = value;
        }
      }

      public Button m_YYBButton
      {
        get
        {
          return this.m_owner.m_YYBButton;
        }
        set
        {
          this.m_owner.m_YYBButton = value;
        }
      }

      public Button m_OppoButton
      {
        get
        {
          return this.m_owner.m_OppoButton;
        }
        set
        {
          this.m_owner.m_OppoButton = value;
        }
      }

      public Button m_openServiceActivityButton
      {
        get
        {
          return this.m_owner.m_openServiceActivityButton;
        }
        set
        {
          this.m_owner.m_openServiceActivityButton = value;
        }
      }

      public GameObject m_openServiceActivityButtonRedMark
      {
        get
        {
          return this.m_owner.m_openServiceActivityButtonRedMark;
        }
        set
        {
          this.m_owner.m_openServiceActivityButtonRedMark = value;
        }
      }

      public Button m_backflowButtonActivityButton
      {
        get
        {
          return this.m_owner.m_backflowButtonActivityButton;
        }
        set
        {
          this.m_owner.m_backflowButtonActivityButton = value;
        }
      }

      public GameObject m_backflowButtonActivityButtonRedMark
      {
        get
        {
          return this.m_owner.m_backflowButtonActivityButtonRedMark;
        }
        set
        {
          this.m_owner.m_backflowButtonActivityButtonRedMark = value;
        }
      }

      public Button m_monthCardButton
      {
        get
        {
          return this.m_owner.m_monthCardButton;
        }
        set
        {
          this.m_owner.m_monthCardButton = value;
        }
      }

      public CommonUIStateController m_monthCardButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_monthCardButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_monthCardButtonStateCtrl = value;
        }
      }

      public GameObject m_monthCardRedMarkObj
      {
        get
        {
          return this.m_owner.m_monthCardRedMarkObj;
        }
        set
        {
          this.m_owner.m_monthCardRedMarkObj = value;
        }
      }

      public CommonUIStateController m_monthCardPanelUIStateController
      {
        get
        {
          return this.m_owner.m_monthCardPanelUIStateController;
        }
        set
        {
          this.m_owner.m_monthCardPanelUIStateController = value;
        }
      }

      public GameObject m_monthCardItemGroupObj
      {
        get
        {
          return this.m_owner.m_monthCardItemGroupObj;
        }
        set
        {
          this.m_owner.m_monthCardItemGroupObj = value;
        }
      }

      public Button m_monthCardCloseButton
      {
        get
        {
          return this.m_owner.m_monthCardCloseButton;
        }
        set
        {
          this.m_owner.m_monthCardCloseButton = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_randomEventListItemPrefab
      {
        get
        {
          return this.m_owner.m_randomEventListItemPrefab;
        }
        set
        {
          this.m_owner.m_randomEventListItemPrefab = value;
        }
      }

      public GameObject m_pastScenarioListItemPrefab
      {
        get
        {
          return this.m_owner.m_pastScenarioListItemPrefab;
        }
        set
        {
          this.m_owner.m_pastScenarioListItemPrefab = value;
        }
      }

      public GameObject m_monthCardItemPrefab
      {
        get
        {
          return this.m_owner.m_monthCardItemPrefab;
        }
        set
        {
          this.m_owner.m_monthCardItemPrefab = value;
        }
      }

      public GameObject m_ServerTimeGO
      {
        get
        {
          return this.m_owner.m_ServerTimeGO;
        }
        set
        {
          this.m_owner.m_ServerTimeGO = value;
        }
      }

      public Text m_ServerTimeText
      {
        get
        {
          return this.m_owner.m_ServerTimeText;
        }
        set
        {
          this.m_owner.m_ServerTimeText = value;
        }
      }

      public ConfigDataRiftLevelInfo m_gotoRiftLevelInfo
      {
        get
        {
          return this.m_owner.m_gotoRiftLevelInfo;
        }
        set
        {
          this.m_owner.m_gotoRiftLevelInfo = value;
        }
      }

      public int m_developerClickCount
      {
        get
        {
          return this.m_owner.m_developerClickCount;
        }
        set
        {
          this.m_owner.m_developerClickCount = value;
        }
      }

      public Color m_fogColor
      {
        get
        {
          return this.m_owner.m_fogColor;
        }
        set
        {
          this.m_owner.m_fogColor = value;
        }
      }

      public List<WorldEventListItemUIController> m_randomEventListItems
      {
        get
        {
          return this.m_owner.m_randomEventListItems;
        }
        set
        {
          this.m_owner.m_randomEventListItems = value;
        }
      }

      public List<PastScenarioListItemUIController> m_pastScenarioListItems
      {
        get
        {
          return this.m_owner.m_pastScenarioListItems;
        }
        set
        {
          this.m_owner.m_pastScenarioListItems = value;
        }
      }

      public List<GameObject> m_userGuideDialogHideGameObjects
      {
        get
        {
          return this.m_owner.m_userGuideDialogHideGameObjects;
        }
        set
        {
          this.m_owner.m_userGuideDialogHideGameObjects = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public bool m_alwaysShowMonthCardButton
      {
        get
        {
          return this.m_owner.m_alwaysShowMonthCardButton;
        }
        set
        {
          this.m_owner.m_alwaysShowMonthCardButton = value;
        }
      }

      public List<ConfigDataMonthCardInfo> m_allMonthCardList
      {
        get
        {
          return this.m_owner.m_allMonthCardList;
        }
        set
        {
          this.m_owner.m_allMonthCardList = value;
        }
      }

      public ConfigDataGiftStoreItemInfo m_strongRecommendGiftStoreItem
      {
        get
        {
          return this.m_owner.m_strongRecommendGiftStoreItem;
        }
        set
        {
          this.m_owner.m_strongRecommendGiftStoreItem = value;
        }
      }

      public DateTime m_strongRecommendGiftStoreItemEndTime
      {
        get
        {
          return this.m_owner.m_strongRecommendGiftStoreItemEndTime;
        }
        set
        {
          this.m_owner.m_strongRecommendGiftStoreItemEndTime = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnEnable()
      {
        this.m_owner.OnEnable();
      }

      public void UpdateDeveloperMode()
      {
        this.m_owner.UpdateDeveloperMode();
      }

      public IEnumerator Co_EnterMonster(Action onEnd)
      {
        return this.m_owner.Co_EnterMonster(onEnd);
      }

      public void SetMonthCardRedMark(bool flag)
      {
        this.m_owner.SetMonthCardRedMark(flag);
      }

      public int OnMonthCardSort(MonthCard x, MonthCard y)
      {
        return this.m_owner.OnMonthCardSort(x, y);
      }

      public void DeveloperModeClick()
      {
        this.m_owner.DeveloperModeClick();
      }

      public void OnCompassButtonClick()
      {
        this.m_owner.OnCompassButtonClick();
      }

      public void OnCurrentScenarioButtonClick()
      {
        this.m_owner.OnCurrentScenarioButtonClick();
      }

      public void OnUnlockScenarioBackgroundButtonClick()
      {
        this.m_owner.OnUnlockScenarioBackgroundButtonClick();
      }

      public void OnUnlockScenarioGotoButtonClick()
      {
        this.m_owner.OnUnlockScenarioGotoButtonClick();
      }

      public void OnMainButtonClick()
      {
        this.m_owner.OnMainButtonClick();
      }

      public void OnPlayerButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnPlayerButtonClick(ctrl);
      }

      public void OnHeroButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnHeroButtonClick(ctrl);
      }

      public void OnBagButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnBagButtonClick(ctrl);
      }

      public void OnSelectCardButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnSelectCardButtonClick(ctrl);
      }

      public void OnMissionButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnMissionButtonClick(ctrl);
      }

      public void OnFetterButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnFetterButtonClick(ctrl);
      }

      public void OnStoreButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnStoreButtonClick(ctrl);
      }

      public void OnDrillButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnDrillButtonClick(ctrl);
      }

      public void OnFriendButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnFriendButtonClick(ctrl);
      }

      public void OnGuildButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnGuildButtonClick(ctrl);
      }

      public void OnEventButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnEventButtonClick(ctrl);
      }

      public void OnUnchartedButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnUnchartedButtonClick(ctrl);
      }

      public void OnArenaButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnArenaButtonClick(ctrl);
      }

      public void OnRiftButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnRiftButtonClick(ctrl);
      }

      public void OnTestButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnTestButtonClick(ctrl);
      }

      public void OnTestButton2Click(UIControllerBase ctrl)
      {
        this.m_owner.OnTestButton2Click(ctrl);
      }

      public void OnCooperateBattleButtonClick()
      {
        this.m_owner.OnCooperateBattleButtonClick();
      }

      public void OnRecommendGiftBoxButtonClick()
      {
        this.m_owner.OnRecommendGiftBoxButtonClick();
      }

      public void OnMailButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnMailButtonClick(ctrl);
      }

      public void OnChatButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnChatButtonClick(ctrl);
      }

      public void OnRankButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnRankButtonClick(ctrl);
      }

      public void OnActivityButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnActivityButtonClick(ctrl);
      }

      public void OnOpenServiceActivityButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnOpenServiceActivityButtonClick(ctrl);
      }

      public void OnBackflowButtonActivityButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnBackflowButtonActivityButtonClick(ctrl);
      }

      public void OnOppoButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnOppoButtonClick(ctrl);
      }

      public void OnYingYongBaoButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnYingYongBaoButtonClick(ctrl);
      }

      public void OnNoticeCenterButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnNoticeCenterButtonClick(ctrl);
      }

      public void OnInvestigationButtonClick(UIControllerBase ctrl)
      {
        this.m_owner.OnInvestigationButtonClick(ctrl);
      }

      public void SaveState()
      {
        this.m_owner.SaveState();
      }

      public void WorldEventListItem_OnButtonClick(WorldEventListItemUIController ctrl)
      {
        this.m_owner.WorldEventListItem_OnButtonClick(ctrl);
      }

      public void OnEventListBackgroundButtonClick()
      {
        this.m_owner.OnEventListBackgroundButtonClick();
      }

      public void OnPastScenarioListBackgroundButtonClick()
      {
        this.m_owner.OnPastScenarioListBackgroundButtonClick();
      }

      public void PastScenarioListItem_OnStartButtonClick(PastScenarioListItemUIController ctrl)
      {
        this.m_owner.PastScenarioListItem_OnStartButtonClick(ctrl);
      }

      public void OnMonthCardButtonClick()
      {
        this.m_owner.OnMonthCardButtonClick();
      }

      public void OnMonthCardCloseButtonClick()
      {
        this.m_owner.OnMonthCardCloseButtonClick();
      }

      public void OnMonthCardItemItemClick(int cardId)
      {
        this.m_owner.OnMonthCardItemItemClick(cardId);
      }

      public void OnMonthCardBuyClick(int cardId)
      {
        this.m_owner.OnMonthCardBuyClick(cardId);
      }
    }
  }
}
