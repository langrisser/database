﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerInfoInitNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class PlayerInfoInitNetTask : NetWorkTransactionTask
  {
    protected PlayerInfoInitNetTask.ResultState m_state;

    public PlayerInfoInitNetTask()
      : base(100f, (UITaskBase) null, false)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerInfoInitAck(object msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnPlayerInfoInitEnd()
    {
      this.m_state = PlayerInfoInitNetTask.ResultState.InitEnd;
      this.OnTransactionComplete();
    }

    public PlayerInfoInitNetTask.ResultState ReqResultState
    {
      get
      {
        return this.m_state;
      }
    }

    public enum ResultState
    {
      None,
      CreateCharacter,
      InitEnd,
    }
  }
}
