﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RaffleUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Scene;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RaffleUITask : UITask
  {
    public static string ParamKey_PoolId = "PoolId";
    protected GetRewardGoodsUITask m_getRewardGoodsUITask;
    protected SceneLayerBase m_rewardPanelLayer;
    protected RaffleUIController m_mainCtrl;
    protected Raffle3DUIController m_raffle3dUICtrl;
    protected RaffleRewardUIController m_rewardPanelCtrl;
    protected RafflePool m_rafflePool;
    protected RaffleUITask.DrawAnimationState m_drawAniamtionState;
    protected int m_drawnRaffleId;
    protected List<Goods> m_drawnGoods;
    public Action EventOnClose;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    protected override void OnStop()
    {
      base.OnStop();
    }

    protected override void OnPause()
    {
      base.OnPause();
    }

    protected override bool OnResume(UIIntent intent)
    {
      this.InitLayerStateOnResume();
      return base.OnResume(intent);
    }

    protected override bool IsNeedUpdateDataCache()
    {
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateDataCache()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitLayerStateOnResume()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateDrawAnimation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearContextOnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnShowHelpButtonClick()
    {
      CommonUIController.Instance.ShowExplanation(ExplanationId.ExplanationId_Raffle);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRewardPanelButtonClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnDoRaffleButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StartDrawAnimation(int raffleId, List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OpenSrBox(Goods goods, Action<List<Goods>> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsSrBox(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRechargeDialogResult(DialogBoxResult result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnNotEnoughItemDialogResult(DialogBoxResult result)
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnRewardPanelCloseButtonClick()
    {
      this.m_rewardPanelCtrl.ShowRaffleRewardPanel(false, (Action) null);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnLevelEffectBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRewardGoodsUITaskClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowErrorTipWnd(int errorCode)
    {
      // ISSUE: unable to decompile the method.
    }

    protected bool IsPipelineStateMaskNeedUpdate(RaffleUITask.PipeLineStateMaskType state)
    {
      return this.m_currPipeLineCtx.IsNeedUpdate((int) state);
    }

    protected void EnablePipelineStateMask(RaffleUITask.PipeLineStateMaskType state)
    {
      this.m_currPipeLineCtx.AddUpdateMask((int) state);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowNotEnoughCrystalMsgBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowNotEoughItemMsgBox()
    {
      // ISSUE: unable to decompile the method.
    }

    protected ProjectLPlayerContext PlayerCtx
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    public enum PipeLineStateMaskType
    {
      ShowRewardPanel,
      ShowDrawAnimation,
    }

    public enum DrawAnimationState
    {
      Idle,
      Drawing,
      RewardLevel,
      RewardDetail,
    }
  }
}
