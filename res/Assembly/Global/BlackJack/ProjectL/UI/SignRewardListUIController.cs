﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SignRewardListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SignRewardListUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_signUIStateCtrl;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./RewardList/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./RewardList/RewardListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewSignRewardItemContent;
    [AutoBind("./RewardList/SignDaysMonth/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_signDaysMonthText;
    [AutoBind("./ShowBoxRewardPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_showBoxRewardPanelUIStateController;
    [AutoBind("./ShowBoxRewardPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_showBoxRewardPanelBGButton;
    [AutoBind("./ShowBoxRewardPanel/LayoutRoot/ListPanel/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxRewardScrollViewObj;
    [AutoBind("./ShowBoxRewardPanel/LayoutRoot/BoxGoodsDesc/Item/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_boxIconImage;
    [AutoBind("./ShowBoxRewardPanel/LayoutRoot/BoxGoodsDesc/Item/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxCountText;
    [AutoBind("./ShowBoxRewardPanel/LayoutRoot/BoxGoodsDesc/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxNameText;
    [AutoBind("./ShowBoxRewardPanel/LayoutRoot/BoxGoodsDesc/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxHaveCountText;
    [AutoBind("./ShowBoxRewardPanel/LayoutRoot/BoxGoodsDesc/DescPanel/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxDescText;
    [AutoBind("./Prefabs/BoxItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_signBoxItemPrefab;
    private List<SignRewardItemUIController> m_signRewardItemCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private string m_sOriSignValueText;

    [MethodImpl((MethodImplOptions) 32768)]
    public SignRewardListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableCloseButton(bool bEnable)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSignDays()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSignRewardItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySignAnimation(Action onEnd)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AutoSign()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SignOpenTween(Action onEnd = null)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBoxRewards(List<Goods> items, SignRewardItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseBoxRewardShowPanel()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSignItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSignBoxClick(GoodsType type, int id, int count)
    {
    }

    public event Action EventOnSignTodayListClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<GoodsType, int, int> EventOnSignTodayBoxListClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnSignCloseButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
