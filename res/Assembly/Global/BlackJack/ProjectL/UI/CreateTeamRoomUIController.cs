﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CreateTeamRoomUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class CreateTeamRoomUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/CreateButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_createButton;
    [AutoBind("./Panel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelButton;
    [AutoBind("./Panel/AuthorityToggle/AllPeopleToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_authorityAllToggle;
    [AutoBind("./Panel/AuthorityToggle/FriendAndGuildToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_authorityFriendToggle;
    [AutoBind("./Panel/AuthorityToggle/NotpublicToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_authorityNonPublicToggle;
    [AutoBind("./Panel/Info/GameFunctionType/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_gameFunctionTypeScrollRect;
    [AutoBind("./Panel/Info/Location/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_locationScrollRect;
    [AutoBind("./Panel/Info/PlayerLevel/LeftScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_playerLevelMinScrollRect;
    [AutoBind("./Panel/Info/PlayerLevel/RightScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_playerLevelMaxScrollRect;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/RoomInfoListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_roomInfoListItemPrefab;
    private ScrollSnapCenter m_gameFunctionTypeScrollSnapCenter;
    private ScrollSnapCenter m_locationScrollSnapCenter;
    private ScrollSnapCenter m_playerLevelMinScrollSnapCenter;
    private ScrollSnapCenter m_playerLevelMaxScrollSnapCenter;
    private List<TeamRoomInfoListItemUIController> m_gameFunctionTypeListItems;
    private List<TeamRoomInfoListItemUIController> m_locationListItems;
    private List<TeamRoomInfoListItemUIController> m_playerLevelMinListItems;
    private List<TeamRoomInfoListItemUIController> m_playerLevelMaxListItems;
    [DoNotToLua]
    private CreateTeamRoomUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_OpenCollectionActivityInt32_hotfix;
    private LuaFunction m_CloseAction_hotfix;
    private LuaFunction m_AddGameFunctionTypeListItems_hotfix;
    private LuaFunction m_AddCollectionActivityListItemsInt32_hotfix;
    private LuaFunction m_ClearGameFunctionTypeListItems_hotfix;
    private LuaFunction m_AddGameFunctionTypeListItemStringGameFunctionTypeInt32Boolean_hotfix;
    private LuaFunction m_AddLocationListItemsGameFunctionTypeInt32_hotfix;
    private LuaFunction m_ClearLocationListItems_hotfix;
    private LuaFunction m_AddLocationListItemStringInt32Boolean_hotfix;
    private LuaFunction m_AddPlayerLevelMinListItemInt32_hotfix;
    private LuaFunction m_ClearPlayerLevelMinListItems_hotfix;
    private LuaFunction m_AddPlayerLevelMaxListItemInt32_hotfix;
    private LuaFunction m_ClearPlayerLevelMaxListItems_hotfix;
    private LuaFunction m_SetGameFunctionTypeGameFunctionTypeInt32_hotfix;
    private LuaFunction m_GetGameFunctionType_hotfix;
    private LuaFunction m_SetLocationInt32_hotfix;
    private LuaFunction m_GetLocationId_hotfix;
    private LuaFunction m_SetPlayerLevelMinInt32_hotfix;
    private LuaFunction m_GetPlayerLevelMin_hotfix;
    private LuaFunction m_SetPlayerLevelMaxInt32_hotfix;
    private LuaFunction m_GetPlayerLevelMax_hotfix;
    private LuaFunction m_SetAuthorityTeamRoomAuthority_hotfix;
    private LuaFunction m_GetAuthority_hotfix;
    private LuaFunction m_SaveTeamRoomSettingTeamRoomSetting_hotfix;
    private LuaFunction m_OnCreateButtonClick_hotfix;
    private LuaFunction m_OnCancelButtonClick_hotfix;
    private LuaFunction m_OnBackgroundButtonClick_hotfix;
    private LuaFunction m_GameFunctionTypeListItem_OnButtonClickTeamRoomInfoListItemUIController_hotfix;
    private LuaFunction m_LocationListItem_OnButtonClickTeamRoomInfoListItemUIController_hotfix;
    private LuaFunction m_PlayerLevelMinListItem_OnButtonClickTeamRoomInfoListItemUIController_hotfix;
    private LuaFunction m_PlayerLevelMaxListItem_OnButtonClickTeamRoomInfoListItemUIController_hotfix;
    private LuaFunction m_GameFunctionTypeScrollSnapCenter_OnCenterItemChangedInt32_hotfix;
    private LuaFunction m_GameFunctionTypeScrollRect_OnValueChagedVector2_hotfix;
    private LuaFunction m_LocationScrollRect_OnValueChagedVector2_hotfix;
    private LuaFunction m_PlayerLevelMinScrollRect_OnValueChagedVector2_hotfix;
    private LuaFunction m_PlayerLevelMaxScrollRect_OnValueChagedVector2_hotfix;
    private LuaFunction m_ScaleListItemScrollRectList`1_hotfix;
    private LuaFunction m_add_EventOnCreateTeamRoomAction`1_hotfix;
    private LuaFunction m_remove_EventOnCreateTeamRoomAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private CreateTeamRoomUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenCollectionActivity(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddGameFunctionTypeListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddCollectionActivityListItems(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearGameFunctionTypeListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddGameFunctionTypeListItem(
      string name,
      GameFunctionType gameFunctionType,
      int chapterId,
      bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLocationListItems(GameFunctionType gameFunctionType, int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearLocationListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLocationListItem(string name, int id, bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPlayerLevelMinListItem(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearPlayerLevelMinListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPlayerLevelMaxListItem(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearPlayerLevelMaxListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetRoomInfoListItemIndexByValue(
      List<TeamRoomInfoListItemUIController> list,
      int value,
      int value2 = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGameFunctionType(GameFunctionType gameFunctionType, int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameFunctionType GetGameFunctionType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocation(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetLocationId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPlayerLevelMin(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetPlayerLevelMin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPlayerLevelMax(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetPlayerLevelMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetAuthority(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamRoomAuthority GetAuthority()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveTeamRoomSetting(TeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCreateButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GameFunctionTypeListItem_OnButtonClick(TeamRoomInfoListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LocationListItem_OnButtonClick(TeamRoomInfoListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelMinListItem_OnButtonClick(TeamRoomInfoListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelMaxListItem_OnButtonClick(TeamRoomInfoListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GameFunctionTypeScrollSnapCenter_OnCenterItemChanged(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GameFunctionTypeScrollRect_OnValueChaged(Vector2 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LocationScrollRect_OnValueChaged(Vector2 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelMinScrollRect_OnValueChaged(Vector2 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelMaxScrollRect_OnValueChaged(Vector2 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ScaleListItem(
      ScrollRect scrollRect,
      List<TeamRoomInfoListItemUIController> listItems)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<TeamRoomSetting> EventOnCreateTeamRoom
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CreateTeamRoomUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCreateTeamRoom(TeamRoomSetting obj)
    {
    }

    private void __clearDele_EventOnCreateTeamRoom(TeamRoomSetting obj)
    {
      this.EventOnCreateTeamRoom = (Action<TeamRoomSetting>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CreateTeamRoomUIController m_owner;

      public LuaExportHelper(CreateTeamRoomUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnCreateTeamRoom(TeamRoomSetting obj)
      {
        this.m_owner.__callDele_EventOnCreateTeamRoom(obj);
      }

      public void __clearDele_EventOnCreateTeamRoom(TeamRoomSetting obj)
      {
        this.m_owner.__clearDele_EventOnCreateTeamRoom(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_createButton
      {
        get
        {
          return this.m_owner.m_createButton;
        }
        set
        {
          this.m_owner.m_createButton = value;
        }
      }

      public Button m_cancelButton
      {
        get
        {
          return this.m_owner.m_cancelButton;
        }
        set
        {
          this.m_owner.m_cancelButton = value;
        }
      }

      public Toggle m_authorityAllToggle
      {
        get
        {
          return this.m_owner.m_authorityAllToggle;
        }
        set
        {
          this.m_owner.m_authorityAllToggle = value;
        }
      }

      public Toggle m_authorityFriendToggle
      {
        get
        {
          return this.m_owner.m_authorityFriendToggle;
        }
        set
        {
          this.m_owner.m_authorityFriendToggle = value;
        }
      }

      public Toggle m_authorityNonPublicToggle
      {
        get
        {
          return this.m_owner.m_authorityNonPublicToggle;
        }
        set
        {
          this.m_owner.m_authorityNonPublicToggle = value;
        }
      }

      public ScrollRect m_gameFunctionTypeScrollRect
      {
        get
        {
          return this.m_owner.m_gameFunctionTypeScrollRect;
        }
        set
        {
          this.m_owner.m_gameFunctionTypeScrollRect = value;
        }
      }

      public ScrollRect m_locationScrollRect
      {
        get
        {
          return this.m_owner.m_locationScrollRect;
        }
        set
        {
          this.m_owner.m_locationScrollRect = value;
        }
      }

      public ScrollRect m_playerLevelMinScrollRect
      {
        get
        {
          return this.m_owner.m_playerLevelMinScrollRect;
        }
        set
        {
          this.m_owner.m_playerLevelMinScrollRect = value;
        }
      }

      public ScrollRect m_playerLevelMaxScrollRect
      {
        get
        {
          return this.m_owner.m_playerLevelMaxScrollRect;
        }
        set
        {
          this.m_owner.m_playerLevelMaxScrollRect = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_roomInfoListItemPrefab
      {
        get
        {
          return this.m_owner.m_roomInfoListItemPrefab;
        }
        set
        {
          this.m_owner.m_roomInfoListItemPrefab = value;
        }
      }

      public ScrollSnapCenter m_gameFunctionTypeScrollSnapCenter
      {
        get
        {
          return this.m_owner.m_gameFunctionTypeScrollSnapCenter;
        }
        set
        {
          this.m_owner.m_gameFunctionTypeScrollSnapCenter = value;
        }
      }

      public ScrollSnapCenter m_locationScrollSnapCenter
      {
        get
        {
          return this.m_owner.m_locationScrollSnapCenter;
        }
        set
        {
          this.m_owner.m_locationScrollSnapCenter = value;
        }
      }

      public ScrollSnapCenter m_playerLevelMinScrollSnapCenter
      {
        get
        {
          return this.m_owner.m_playerLevelMinScrollSnapCenter;
        }
        set
        {
          this.m_owner.m_playerLevelMinScrollSnapCenter = value;
        }
      }

      public ScrollSnapCenter m_playerLevelMaxScrollSnapCenter
      {
        get
        {
          return this.m_owner.m_playerLevelMaxScrollSnapCenter;
        }
        set
        {
          this.m_owner.m_playerLevelMaxScrollSnapCenter = value;
        }
      }

      public List<TeamRoomInfoListItemUIController> m_gameFunctionTypeListItems
      {
        get
        {
          return this.m_owner.m_gameFunctionTypeListItems;
        }
        set
        {
          this.m_owner.m_gameFunctionTypeListItems = value;
        }
      }

      public List<TeamRoomInfoListItemUIController> m_locationListItems
      {
        get
        {
          return this.m_owner.m_locationListItems;
        }
        set
        {
          this.m_owner.m_locationListItems = value;
        }
      }

      public List<TeamRoomInfoListItemUIController> m_playerLevelMinListItems
      {
        get
        {
          return this.m_owner.m_playerLevelMinListItems;
        }
        set
        {
          this.m_owner.m_playerLevelMinListItems = value;
        }
      }

      public List<TeamRoomInfoListItemUIController> m_playerLevelMaxListItems
      {
        get
        {
          return this.m_owner.m_playerLevelMaxListItems;
        }
        set
        {
          this.m_owner.m_playerLevelMaxListItems = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void AddGameFunctionTypeListItems()
      {
        this.m_owner.AddGameFunctionTypeListItems();
      }

      public void AddCollectionActivityListItems(int collectionActivityId)
      {
        this.m_owner.AddCollectionActivityListItems(collectionActivityId);
      }

      public void ClearGameFunctionTypeListItems()
      {
        this.m_owner.ClearGameFunctionTypeListItems();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void AddGameFunctionTypeListItem(
        string name,
        GameFunctionType gameFunctionType,
        int chapterId,
        bool isLocked)
      {
        // ISSUE: unable to decompile the method.
      }

      public void AddLocationListItems(GameFunctionType gameFunctionType, int chapterId)
      {
        this.m_owner.AddLocationListItems(gameFunctionType, chapterId);
      }

      public void ClearLocationListItems()
      {
        this.m_owner.ClearLocationListItems();
      }

      public void AddLocationListItem(string name, int id, bool isLocked)
      {
        this.m_owner.AddLocationListItem(name, id, isLocked);
      }

      public void AddPlayerLevelMinListItem(int level)
      {
        this.m_owner.AddPlayerLevelMinListItem(level);
      }

      public void ClearPlayerLevelMinListItems()
      {
        this.m_owner.ClearPlayerLevelMinListItems();
      }

      public void AddPlayerLevelMaxListItem(int level)
      {
        this.m_owner.AddPlayerLevelMaxListItem(level);
      }

      public void ClearPlayerLevelMaxListItems()
      {
        this.m_owner.ClearPlayerLevelMaxListItems();
      }

      public static int GetRoomInfoListItemIndexByValue(
        List<TeamRoomInfoListItemUIController> list,
        int value,
        int value2)
      {
        return CreateTeamRoomUIController.GetRoomInfoListItemIndexByValue(list, value, value2);
      }

      public GameFunctionType GetGameFunctionType()
      {
        return this.m_owner.GetGameFunctionType();
      }

      public int GetLocationId()
      {
        return this.m_owner.GetLocationId();
      }

      public void SetPlayerLevelMin(int level)
      {
        this.m_owner.SetPlayerLevelMin(level);
      }

      public int GetPlayerLevelMin()
      {
        return this.m_owner.GetPlayerLevelMin();
      }

      public void SetPlayerLevelMax(int level)
      {
        this.m_owner.SetPlayerLevelMax(level);
      }

      public int GetPlayerLevelMax()
      {
        return this.m_owner.GetPlayerLevelMax();
      }

      public void SetAuthority(TeamRoomAuthority authority)
      {
        this.m_owner.SetAuthority(authority);
      }

      public TeamRoomAuthority GetAuthority()
      {
        return this.m_owner.GetAuthority();
      }

      public void SaveTeamRoomSetting(TeamRoomSetting setting)
      {
        this.m_owner.SaveTeamRoomSetting(setting);
      }

      public void OnCreateButtonClick()
      {
        this.m_owner.OnCreateButtonClick();
      }

      public void OnCancelButtonClick()
      {
        this.m_owner.OnCancelButtonClick();
      }

      public void OnBackgroundButtonClick()
      {
        this.m_owner.OnBackgroundButtonClick();
      }

      public void GameFunctionTypeListItem_OnButtonClick(TeamRoomInfoListItemUIController ctrl)
      {
        this.m_owner.GameFunctionTypeListItem_OnButtonClick(ctrl);
      }

      public void LocationListItem_OnButtonClick(TeamRoomInfoListItemUIController ctrl)
      {
        this.m_owner.LocationListItem_OnButtonClick(ctrl);
      }

      public void PlayerLevelMinListItem_OnButtonClick(TeamRoomInfoListItemUIController ctrl)
      {
        this.m_owner.PlayerLevelMinListItem_OnButtonClick(ctrl);
      }

      public void PlayerLevelMaxListItem_OnButtonClick(TeamRoomInfoListItemUIController ctrl)
      {
        this.m_owner.PlayerLevelMaxListItem_OnButtonClick(ctrl);
      }

      public void GameFunctionTypeScrollSnapCenter_OnCenterItemChanged(int idx)
      {
        this.m_owner.GameFunctionTypeScrollSnapCenter_OnCenterItemChanged(idx);
      }

      public void GameFunctionTypeScrollRect_OnValueChaged(Vector2 value)
      {
        this.m_owner.GameFunctionTypeScrollRect_OnValueChaged(value);
      }

      public void LocationScrollRect_OnValueChaged(Vector2 value)
      {
        this.m_owner.LocationScrollRect_OnValueChaged(value);
      }

      public void PlayerLevelMinScrollRect_OnValueChaged(Vector2 value)
      {
        this.m_owner.PlayerLevelMinScrollRect_OnValueChaged(value);
      }

      public void PlayerLevelMaxScrollRect_OnValueChaged(Vector2 value)
      {
        this.m_owner.PlayerLevelMaxScrollRect_OnValueChaged(value);
      }

      public void ScaleListItem(
        ScrollRect scrollRect,
        List<TeamRoomInfoListItemUIController> listItems)
      {
        this.m_owner.ScaleListItem(scrollRect, listItems);
      }
    }
  }
}
