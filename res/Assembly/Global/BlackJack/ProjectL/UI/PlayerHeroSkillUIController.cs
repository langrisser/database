﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerHeroSkillUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class PlayerHeroSkillUIController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IEventSystemHandler
  {
    private GameObject m_popupGameObject;
    private ConfigDataSkillInfo m_skillInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkillInfoAndDescObj(ConfigDataSkillInfo skillInfo, GameObject descObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ConfigDataSkillInfo> EventOnSkillClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
