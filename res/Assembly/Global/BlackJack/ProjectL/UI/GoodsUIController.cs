﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GoodsUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class GoodsUIController : UIControllerBase
  {
    public GoodsType m_goodsType;
    public int m_goodsId;
    public int m_goodsCount;
    public Goods m_goods;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_frameImage;
    private GameObject m_haveGetGroupRoot;
    private Text m_nameText;
    private Text m_countText;
    private GameObject m_crystalEffectGameObject;
    private GameObject m_ssrEffectGameObject;
    private GameObject m_ssrPieceEffectGameObject;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetReward(Goods r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCount(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTag(string tagName, bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowHaveGot(bool isGot)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GoodsUIController CreateRewardGoods(
      Goods g,
      Transform parent,
      GameObject prefab,
      bool showCount = true,
      Action<GoodsUIController> onGoodsClick = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CreateRewardGoodsList(
      List<Goods> goods,
      Transform parent,
      GameObject prefab,
      List<GoodsUIController> ctrlList = null,
      bool showCount = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
    }

    public event Action<GoodsUIController> OnClickEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
