﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentDepotListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class EquipmentDepotListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_stateCtrl;
    [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_icon;
    [AutoBind("./BgFrame", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_bgFrame;
    [AutoBind("./SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_SSREffect;
    [AutoBind("./CheckImageMask", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_checkImageMask;
    [AutoBind("./StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_starGroup;
    [AutoBind("./Lv/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_lvValueText;
    [AutoBind("./EquipingTag/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_equipingTagHeadIcon;
    [AutoBind("./LockImage", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_lockImage;
    [AutoBind("./SelectFrame", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_selectFrame;
    [AutoBind("./Mask", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_mask;
    [AutoBind("./EnchantmentIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_enchantmentIcon;
    [AutoBind("./CountText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_countText;
    public bool IsEquiped;
    public ulong EquipmentInstanceId;
    public BagItemBase BagItem;
    public ScrollItemBaseUIController ScrollItemBaseUICtrl;
    [DoNotToLua]
    private EquipmentDepotListItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitEquipmentDepotListItemInfoBagItemBaseBooleanBoolean_hotfix;
    private LuaFunction m_ShowCheckImageBoolean_hotfix;
    private LuaFunction m_ShowSelectFrameBoolean_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_RegisterItemClickEventAction`1_hotfix;
    private LuaFunction m_RegisterItemNeedFillEventAction`1_hotfix;
    private LuaFunction m_RegisterItem3DTouchEventAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEquipmentDepotListItemInfo(BagItemBase bagItem, bool canWear, bool isInDepot = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCheckImage(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectFrame(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterItemClickEvent(Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterItem3DTouchEvent(Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public EquipmentDepotListItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private EquipmentDepotListItemUIController m_owner;

      public LuaExportHelper(EquipmentDepotListItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }
    }
  }
}
