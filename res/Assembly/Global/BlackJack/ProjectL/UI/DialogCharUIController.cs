﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogCharUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class DialogCharUIController : UIControllerBase
  {
    private CommonUIStateController m_uiStateController;
    private Image m_image;
    private ConfigDataCharImageInfo m_charImageInfo;
    private UISpineGraphic m_spineGraphic;
    private Color m_curColor;
    private Color m_tweenColor;
    private float m_tweenColorTime;
    private string m_idleAnimationName;
    private string m_idleFacialAnimationName;
    private float m_voiceTime;
    private float m_blinkTime;
    private bool m_isPlayingPreAnimation;
    private bool m_isPlayingEyeAnimation;
    private bool m_isPlayingMouthAnimation;
    private const int EYE_ANIMATION_TRACK = 1;
    private const int MOUTH_ANIMATION_TRACK = 2;
    private const string EYE_ANIMATION = "_eye";
    private const string EYE_STILL_ANIMATION = "_eye_still";
    private const string MOUTH_ANIMATION = "_mouth";
    private const string MOUTH_STILL_ANIMATION = "_mouth_still";
    [DoNotToLua]
    private DialogCharUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnDisable_hotfix;
    private LuaFunction m_CreateGraphicConfigDataCharImageInfo_hotfix;
    private LuaFunction m_DestroyGraphic_hotfix;
    private LuaFunction m_SetScaleOffetSingleSingle_hotfix;
    private LuaFunction m_SetDirectionInt32_hotfix;
    private LuaFunction m_SetColorColor_hotfix;
    private LuaFunction m_TweenColorColorSingle_hotfix;
    private LuaFunction m_GetCharImageInfo_hotfix;
    private LuaFunction m_StartFacialAnimationSingle_hotfix;
    private LuaFunction m_EnterInt32Action_hotfix;
    private LuaFunction m_LeaveInt32Action_hotfix;
    private LuaFunction m_Co_TweenColorColorColorSingleAction_hotfix;
    private LuaFunction m_SetAnimationStringStringStringString_hotfix;
    private LuaFunction m_StopMouthAnimation_hotfix;
    private LuaFunction m_SetNextBlinkTime_hotfix;
    private LuaFunction m_GetAnimationDurationString_hotfix;
    private LuaFunction m_PlayAnimationStringBooleanInt32_hotfix;
    private LuaFunction m_StopAnimationInt32_hotfix;
    private LuaFunction m_LateUpdate_hotfix;
    private LuaFunction m_PlayEyeAnimationString_hotfix;
    private LuaFunction m_PlayEyeStillAnimationString_hotfix;
    private LuaFunction m_PlayMouthAnimationString_hotfix;
    private LuaFunction m_PlayMouthStillAnimationString_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGraphic(ConfigDataCharImageInfo charImageInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScaleOffet(float scale, float yOffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetColor(Color c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TweenColor(Color c, float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageInfo GetCharImageInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartFacialAnimation(float voiceTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Enter(int enterType, Action onEndAction = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Leave(int leaveType, Action onEndAction = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_TweenColor(
      Color fromColor,
      Color toColor,
      float time,
      Action onEndAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnimation(
      string preAnimation,
      string preFacialAnimation,
      string idleAnimation,
      string idleFacialAnimation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopMouthAnimation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetNextBlinkTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAnimationDuration(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop, int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopAnimation(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayEyeAnimation(string facialAnimation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayEyeStillAnimation(string facialAnimation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayMouthAnimation(string facialAnimation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayMouthStillAnimation(string facialAnimation)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public DialogCharUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private DialogCharUIController m_owner;

      public LuaExportHelper(DialogCharUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Image m_image
      {
        get
        {
          return this.m_owner.m_image;
        }
        set
        {
          this.m_owner.m_image = value;
        }
      }

      public ConfigDataCharImageInfo m_charImageInfo
      {
        get
        {
          return this.m_owner.m_charImageInfo;
        }
        set
        {
          this.m_owner.m_charImageInfo = value;
        }
      }

      public UISpineGraphic m_spineGraphic
      {
        get
        {
          return this.m_owner.m_spineGraphic;
        }
        set
        {
          this.m_owner.m_spineGraphic = value;
        }
      }

      public Color m_curColor
      {
        get
        {
          return this.m_owner.m_curColor;
        }
        set
        {
          this.m_owner.m_curColor = value;
        }
      }

      public Color m_tweenColor
      {
        get
        {
          return this.m_owner.m_tweenColor;
        }
        set
        {
          this.m_owner.m_tweenColor = value;
        }
      }

      public float m_tweenColorTime
      {
        get
        {
          return this.m_owner.m_tweenColorTime;
        }
        set
        {
          this.m_owner.m_tweenColorTime = value;
        }
      }

      public string m_idleAnimationName
      {
        get
        {
          return this.m_owner.m_idleAnimationName;
        }
        set
        {
          this.m_owner.m_idleAnimationName = value;
        }
      }

      public string m_idleFacialAnimationName
      {
        get
        {
          return this.m_owner.m_idleFacialAnimationName;
        }
        set
        {
          this.m_owner.m_idleFacialAnimationName = value;
        }
      }

      public float m_voiceTime
      {
        get
        {
          return this.m_owner.m_voiceTime;
        }
        set
        {
          this.m_owner.m_voiceTime = value;
        }
      }

      public float m_blinkTime
      {
        get
        {
          return this.m_owner.m_blinkTime;
        }
        set
        {
          this.m_owner.m_blinkTime = value;
        }
      }

      public bool m_isPlayingPreAnimation
      {
        get
        {
          return this.m_owner.m_isPlayingPreAnimation;
        }
        set
        {
          this.m_owner.m_isPlayingPreAnimation = value;
        }
      }

      public bool m_isPlayingEyeAnimation
      {
        get
        {
          return this.m_owner.m_isPlayingEyeAnimation;
        }
        set
        {
          this.m_owner.m_isPlayingEyeAnimation = value;
        }
      }

      public bool m_isPlayingMouthAnimation
      {
        get
        {
          return this.m_owner.m_isPlayingMouthAnimation;
        }
        set
        {
          this.m_owner.m_isPlayingMouthAnimation = value;
        }
      }

      public static int EYE_ANIMATION_TRACK
      {
        get
        {
          return 1;
        }
      }

      public static int MOUTH_ANIMATION_TRACK
      {
        get
        {
          return 2;
        }
      }

      public static string EYE_ANIMATION
      {
        get
        {
          return "_eye";
        }
      }

      public static string EYE_STILL_ANIMATION
      {
        get
        {
          return "_eye_still";
        }
      }

      public static string MOUTH_ANIMATION
      {
        get
        {
          return "_mouth";
        }
      }

      public static string MOUTH_STILL_ANIMATION
      {
        get
        {
          return "_mouth_still";
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnDisable()
      {
        this.m_owner.OnDisable();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public IEnumerator Co_TweenColor(
        Color fromColor,
        Color toColor,
        float time,
        Action onEndAction)
      {
        // ISSUE: unable to decompile the method.
      }

      public void SetNextBlinkTime()
      {
        this.m_owner.SetNextBlinkTime();
      }

      public void PlayAnimation(string name, bool loop, int trackIndex)
      {
        this.m_owner.PlayAnimation(name, loop, trackIndex);
      }

      public void StopAnimation(int trackIndex)
      {
        this.m_owner.StopAnimation(trackIndex);
      }

      public void LateUpdate()
      {
        this.m_owner.LateUpdate();
      }

      public void PlayEyeAnimation(string facialAnimation)
      {
        this.m_owner.PlayEyeAnimation(facialAnimation);
      }

      public void PlayEyeStillAnimation(string facialAnimation)
      {
        this.m_owner.PlayEyeStillAnimation(facialAnimation);
      }

      public void PlayMouthAnimation(string facialAnimation)
      {
        this.m_owner.PlayMouthAnimation(facialAnimation);
      }

      public void PlayMouthStillAnimation(string facialAnimation)
      {
        this.m_owner.PlayMouthStillAnimation(facialAnimation);
      }
    }
  }
}
