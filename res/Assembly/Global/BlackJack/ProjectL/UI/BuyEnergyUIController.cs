﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BuyEnergyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BuyEnergyUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/MoneyBuy/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyCountText;
    [AutoBind("./Panel/MoneyBuy/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buyButton;
    [AutoBind("./Panel/MoneyBuy/BuyButton/Price/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_priceText;
    [AutoBind("./Panel/MoneyBuy/BuyButton/GrayImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_buyGrayImage;
    [AutoBind("./Panel/PropBuy/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyCountMedicineText;
    [AutoBind("./Panel/PropBuy/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_useMedicineButton;
    [AutoBind("./Panel/PropBuy/BuyButton/Price/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_useMedicineCountText;
    [AutoBind("./Panel/PropBuy/ResidueValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_redidueMedicineCountText;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./EnergyNotEnoughTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_energyNotEnoughTipStateCtrl;
    [AutoBind("./EnergyNotEnoughTips/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyNotEnoughTipText;

    private BuyEnergyUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEnergy(int energyCount, int price, bool IsBoughtNumsUsedOut)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMedicine(int energyCount, int useMedicineCount, int redidueMedicineCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnergyNotEnoughTip(string s)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseEnergyMedicineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnUseEnergyMedicine
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnBuyEnergy
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
