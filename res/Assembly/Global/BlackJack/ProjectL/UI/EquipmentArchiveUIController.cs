﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentArchiveUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class EquipmentArchiveUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prefabAnimation;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./EquipmentInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentInfoPanel;
    [AutoBind("./EquipmentInfoPanel/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getButton;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/All", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_aLLItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Weapon", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_weaponItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Armour", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_armorItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Helmet", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_helmetItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Ornament", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_ornamentItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Other", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_otherItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_filterText;
    [AutoBind("./EquipmentList/EquipmentFilter/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipmentFilterButton;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_filterPanel;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/BGImages/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterBGButton;
    [AutoBind("./EquipmentList/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private InfinityGridLayoutGroup m_itemContentInfinityGrid;
    [AutoBind("./EquipmentList/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemContent;
    [AutoBind("./Prefab/ItemPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemPrefab;
    [AutoBind("./CountLimit/CountGroup/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currentItemCountText;
    [AutoBind("./CountLimit/CountGroup/MaxText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_maxItemCountText;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_selectBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_allBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_armorBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_helmetBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_ornamentBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_weaponBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_otherBagItemList;
    private ArchiveUITask m_task;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private List<ArchiveItemUIController> m_itemUIControllerList;
    private EquipmentArchiveUIController.EquipmentInfoWrap m_selectEquipmentInfoWrap;
    private ArchiveItemDetailInfoController m_itemDetailInfoController;
    private int m_allOwnItemCount;
    private int m_armorOwnItemCount;
    private int m_helmetOwnItemCount;
    private int m_ornamentOwnItemCount;
    private int m_weaponOwnItemCount;
    private int m_otherOwnItemCount;
    [DoNotToLua]
    private EquipmentArchiveUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetTaskArchiveUITask_hotfix;
    private LuaFunction m_EnterController_hotfix;
    private LuaFunction m_ItemListCompareEquipmentInfoWrapEquipmentInfoWrap_hotfix;
    private LuaFunction m_RefreshItemArchiveItemUIController_hotfix;
    private LuaFunction m_RefreshItemCountInt32Int32_hotfix;
    private LuaFunction m_CreateItemListList`1_hotfix;
    private LuaFunction m_DestroyItemBagList_hotfix;
    private LuaFunction m_RefreshItemBagWithHeroDataEquipmentInfoWrap_hotfix;
    private LuaFunction m_OnReturnClick_hotfix;
    private LuaFunction m_OnItemClickArchiveItemUIController_hotfix;
    private LuaFunction m_OnEquipmentFilterSwitchClickGameObject_hotfix;
    private LuaFunction m_OnEquipmentFilterClick_hotfix;
    private LuaFunction m_OnFilterBGClick_hotfix;
    private LuaFunction m_OnGetItemPathClick_hotfix;
    private LuaFunction m_UpdateChildrenCallbackDelegateInt32Transform_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentArchiveUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTask(ArchiveUITask task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnterController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ItemListCompare(
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentWrap1,
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentWrap2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshItem(ArchiveItemUIController archiveItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshItemCount(int currentCount, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateItemList(
      List<EquipmentArchiveUIController.EquipmentInfoWrap> equipmentInfoWrapList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyItemBagList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshItemBagWithHeroData(
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentInfoWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemClick(ArchiveItemUIController archiveItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentFilterSwitchClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentFilterClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterBGClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetItemPathClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChildrenCallbackDelegate(int index, Transform trans)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public EquipmentArchiveUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [HotFix]
    public class EquipmentInfoWrap
    {
      public ConfigDataEquipmentInfo equipmentInfo;
      public bool isUnlocked;
      public bool isSelect;
    }

    public class LuaExportHelper
    {
      private EquipmentArchiveUIController m_owner;

      public LuaExportHelper(EquipmentArchiveUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_prefabAnimation
      {
        get
        {
          return this.m_owner.m_prefabAnimation;
        }
        set
        {
          this.m_owner.m_prefabAnimation = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public GameObject m_equipmentInfoPanel
      {
        get
        {
          return this.m_owner.m_equipmentInfoPanel;
        }
        set
        {
          this.m_owner.m_equipmentInfoPanel = value;
        }
      }

      public Button m_getButton
      {
        get
        {
          return this.m_owner.m_getButton;
        }
        set
        {
          this.m_owner.m_getButton = value;
        }
      }

      public Toggle m_aLLItemToggle
      {
        get
        {
          return this.m_owner.m_aLLItemToggle;
        }
        set
        {
          this.m_owner.m_aLLItemToggle = value;
        }
      }

      public Toggle m_weaponItemToggle
      {
        get
        {
          return this.m_owner.m_weaponItemToggle;
        }
        set
        {
          this.m_owner.m_weaponItemToggle = value;
        }
      }

      public Toggle m_armorItemToggle
      {
        get
        {
          return this.m_owner.m_armorItemToggle;
        }
        set
        {
          this.m_owner.m_armorItemToggle = value;
        }
      }

      public Toggle m_helmetItemToggle
      {
        get
        {
          return this.m_owner.m_helmetItemToggle;
        }
        set
        {
          this.m_owner.m_helmetItemToggle = value;
        }
      }

      public Toggle m_ornamentItemToggle
      {
        get
        {
          return this.m_owner.m_ornamentItemToggle;
        }
        set
        {
          this.m_owner.m_ornamentItemToggle = value;
        }
      }

      public Toggle m_otherItemToggle
      {
        get
        {
          return this.m_owner.m_otherItemToggle;
        }
        set
        {
          this.m_owner.m_otherItemToggle = value;
        }
      }

      public Text m_filterText
      {
        get
        {
          return this.m_owner.m_filterText;
        }
        set
        {
          this.m_owner.m_filterText = value;
        }
      }

      public Button m_equipmentFilterButton
      {
        get
        {
          return this.m_owner.m_equipmentFilterButton;
        }
        set
        {
          this.m_owner.m_equipmentFilterButton = value;
        }
      }

      public GameObject m_filterPanel
      {
        get
        {
          return this.m_owner.m_filterPanel;
        }
        set
        {
          this.m_owner.m_filterPanel = value;
        }
      }

      public Button m_filterBGButton
      {
        get
        {
          return this.m_owner.m_filterBGButton;
        }
        set
        {
          this.m_owner.m_filterBGButton = value;
        }
      }

      public InfinityGridLayoutGroup m_itemContentInfinityGrid
      {
        get
        {
          return this.m_owner.m_itemContentInfinityGrid;
        }
        set
        {
          this.m_owner.m_itemContentInfinityGrid = value;
        }
      }

      public GameObject m_itemContent
      {
        get
        {
          return this.m_owner.m_itemContent;
        }
        set
        {
          this.m_owner.m_itemContent = value;
        }
      }

      public GameObject m_itemPrefab
      {
        get
        {
          return this.m_owner.m_itemPrefab;
        }
        set
        {
          this.m_owner.m_itemPrefab = value;
        }
      }

      public Text m_currentItemCountText
      {
        get
        {
          return this.m_owner.m_currentItemCountText;
        }
        set
        {
          this.m_owner.m_currentItemCountText = value;
        }
      }

      public Text m_maxItemCountText
      {
        get
        {
          return this.m_owner.m_maxItemCountText;
        }
        set
        {
          this.m_owner.m_maxItemCountText = value;
        }
      }

      public List<EquipmentArchiveUIController.EquipmentInfoWrap> m_selectBagItemList
      {
        get
        {
          return this.m_owner.m_selectBagItemList;
        }
        set
        {
          this.m_owner.m_selectBagItemList = value;
        }
      }

      public List<EquipmentArchiveUIController.EquipmentInfoWrap> m_allBagItemList
      {
        get
        {
          return this.m_owner.m_allBagItemList;
        }
        set
        {
          this.m_owner.m_allBagItemList = value;
        }
      }

      public List<EquipmentArchiveUIController.EquipmentInfoWrap> m_armorBagItemList
      {
        get
        {
          return this.m_owner.m_armorBagItemList;
        }
        set
        {
          this.m_owner.m_armorBagItemList = value;
        }
      }

      public List<EquipmentArchiveUIController.EquipmentInfoWrap> m_helmetBagItemList
      {
        get
        {
          return this.m_owner.m_helmetBagItemList;
        }
        set
        {
          this.m_owner.m_helmetBagItemList = value;
        }
      }

      public List<EquipmentArchiveUIController.EquipmentInfoWrap> m_ornamentBagItemList
      {
        get
        {
          return this.m_owner.m_ornamentBagItemList;
        }
        set
        {
          this.m_owner.m_ornamentBagItemList = value;
        }
      }

      public List<EquipmentArchiveUIController.EquipmentInfoWrap> m_weaponBagItemList
      {
        get
        {
          return this.m_owner.m_weaponBagItemList;
        }
        set
        {
          this.m_owner.m_weaponBagItemList = value;
        }
      }

      public List<EquipmentArchiveUIController.EquipmentInfoWrap> m_otherBagItemList
      {
        get
        {
          return this.m_owner.m_otherBagItemList;
        }
        set
        {
          this.m_owner.m_otherBagItemList = value;
        }
      }

      public ArchiveUITask m_task
      {
        get
        {
          return this.m_owner.m_task;
        }
        set
        {
          this.m_owner.m_task = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public List<ArchiveItemUIController> m_itemUIControllerList
      {
        get
        {
          return this.m_owner.m_itemUIControllerList;
        }
        set
        {
          this.m_owner.m_itemUIControllerList = value;
        }
      }

      public EquipmentArchiveUIController.EquipmentInfoWrap m_selectEquipmentInfoWrap
      {
        get
        {
          return this.m_owner.m_selectEquipmentInfoWrap;
        }
        set
        {
          this.m_owner.m_selectEquipmentInfoWrap = value;
        }
      }

      public ArchiveItemDetailInfoController m_itemDetailInfoController
      {
        get
        {
          return this.m_owner.m_itemDetailInfoController;
        }
        set
        {
          this.m_owner.m_itemDetailInfoController = value;
        }
      }

      public int m_allOwnItemCount
      {
        get
        {
          return this.m_owner.m_allOwnItemCount;
        }
        set
        {
          this.m_owner.m_allOwnItemCount = value;
        }
      }

      public int m_armorOwnItemCount
      {
        get
        {
          return this.m_owner.m_armorOwnItemCount;
        }
        set
        {
          this.m_owner.m_armorOwnItemCount = value;
        }
      }

      public int m_helmetOwnItemCount
      {
        get
        {
          return this.m_owner.m_helmetOwnItemCount;
        }
        set
        {
          this.m_owner.m_helmetOwnItemCount = value;
        }
      }

      public int m_ornamentOwnItemCount
      {
        get
        {
          return this.m_owner.m_ornamentOwnItemCount;
        }
        set
        {
          this.m_owner.m_ornamentOwnItemCount = value;
        }
      }

      public int m_weaponOwnItemCount
      {
        get
        {
          return this.m_owner.m_weaponOwnItemCount;
        }
        set
        {
          this.m_owner.m_weaponOwnItemCount = value;
        }
      }

      public int m_otherOwnItemCount
      {
        get
        {
          return this.m_owner.m_otherOwnItemCount;
        }
        set
        {
          this.m_owner.m_otherOwnItemCount = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public int ItemListCompare(
        EquipmentArchiveUIController.EquipmentInfoWrap equipmentWrap1,
        EquipmentArchiveUIController.EquipmentInfoWrap equipmentWrap2)
      {
        return this.m_owner.ItemListCompare(equipmentWrap1, equipmentWrap2);
      }

      public void RefreshItem(ArchiveItemUIController archiveItemUIController)
      {
        this.m_owner.RefreshItem(archiveItemUIController);
      }

      public void RefreshItemCount(int currentCount, int maxCount)
      {
        this.m_owner.RefreshItemCount(currentCount, maxCount);
      }

      public void CreateItemList(
        List<EquipmentArchiveUIController.EquipmentInfoWrap> equipmentInfoWrapList)
      {
        this.m_owner.CreateItemList(equipmentInfoWrapList);
      }

      public void DestroyItemBagList()
      {
        this.m_owner.DestroyItemBagList();
      }

      public void RefreshItemBagWithHeroData(
        EquipmentArchiveUIController.EquipmentInfoWrap equipmentInfoWrap)
      {
        this.m_owner.RefreshItemBagWithHeroData(equipmentInfoWrap);
      }

      public void OnReturnClick()
      {
        this.m_owner.OnReturnClick();
      }

      public void OnItemClick(ArchiveItemUIController archiveItemUIController)
      {
        this.m_owner.OnItemClick(archiveItemUIController);
      }

      public void OnEquipmentFilterSwitchClick(GameObject obj)
      {
        this.m_owner.OnEquipmentFilterSwitchClick(obj);
      }

      public void OnEquipmentFilterClick()
      {
        this.m_owner.OnEquipmentFilterClick();
      }

      public void OnFilterBGClick()
      {
        this.m_owner.OnFilterBGClick();
      }

      public void OnGetItemPathClick()
      {
        this.m_owner.OnGetItemPathClick();
      }

      public void UpdateChildrenCallbackDelegate(int index, Transform trans)
      {
        this.m_owner.UpdateChildrenCallbackDelegate(index, trans);
      }
    }
  }
}
