﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildMassiveCombatUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class GuildMassiveCombatUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoButton;
    [AutoBind("./Margin/RankingButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankingButton;
    [AutoBind("./Margin/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardsButton;
    [AutoBind("./RightButtom/GiveUpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_giveUpButton;
    [AutoBind("./Margin/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./Margin/ChatButton/Redmark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chatButtonRedmark;
    [AutoBind("./WorldButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_worldpButton;
    [AutoBind("./DifficultLevel/ModoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_difficultLevelStateCtrl;
    [AutoBind("./GuildCoinPanel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildCoinText;
    [AutoBind("./IntegralPanel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildIntegralText;
    [AutoBind("./IntegralPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildIntegralButton;
    [AutoBind("./GuildCoinPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildCoinButton;
    [AutoBind("./IntegralPanelDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_integralPanelDescGo;
    [AutoBind("./IntegralPanelDesc/BG/BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_integralPanelBoundauryGo;
    [AutoBind("./IntegralPanelDesc/BG/ItemInfoGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_integralPanelDescNameText;
    [AutoBind("./IntegralPanelDesc/BG/BGImage/FrameImage/BottomImage2/Desc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_integralPanelDescText;
    [AutoBind("./QuestPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_questPanelGo;
    [AutoBind("./ContributionPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_contributionPanelStateCtrl;
    [AutoBind("./ContributionPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_contributionPanelBackBGButton;
    [AutoBind("./RewardInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardInfoPanelStateCtrl;
    [AutoBind("./RewardInfoPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardInfoBackBGButton;
    [AutoBind("./QuestInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_questInfoPanelStateCtrl;
    [AutoBind("./QuestInfoPanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_questInfoPanelBackBGButton;
    [AutoBind("./QuestInfoPanel/Detail/LeftPart/StageImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_questInfoPanelStageImage;
    [AutoBind("./QuestInfoPanel/Detail/LeftPart/RewardGroup/RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_questInfoPanelRewardGroup;
    [AutoBind("./QuestInfoPanel/Detail/LeftPart/Hard/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_questInfoPanelHardText;
    [AutoBind("./QuestInfoPanel/Detail/LeftPart/SuppressValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_questInfoPanelSuppressValueText;
    [AutoBind("./QuestInfoPanel/Detail/RightPart/HeroListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_questInfoPanelHeroListScrollRect;
    [AutoBind("./QuestInfoPanel/Detail/RightPart/HeroListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_questInfoPanelHeroListScrollContent;
    [AutoBind("./QuestInfoPanel/Detail/RightPart/TeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_questInfoPanelTeamButton;
    [AutoBind("./QuestInfoPanel/Detail/RightPart/PersonalButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_questInfoPanelPersonalButton;
    [AutoBind("./QuestInfoPanel/Detail/RightPart/BuffDesc/Text/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_questInfoPanelBuffIconImage;
    [AutoBind("./QuestInfoPanel/Detail/RightPart/BuffDesc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_questInfoPanelBuffText;
    [AutoBind("./Prefab/HeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_questInfoPanelHeroListItemPrefab;
    [AutoBind("./WinOrFailedEffectGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildMassiveCombatResultStateCtrl;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private GuildMassiveCombatInfo m_curCombatInfo;
    private GuildMassiveCombatStronghold m_curSelectStrongHold;
    private List<GuildMassiveCombatStrongHoldUIController> m_strongHoldsCtrlList;
    [DoNotToLua]
    private GuildMassiveCombatUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_GuildMassiveCombatUpdateViewGuildMassiveCombatInfo_hotfix;
    private LuaFunction m_UpdateStrongHolds_hotfix;
    private LuaFunction m_OnGuildMassiveCombatStrongHoldClickGuildMassiveCombatStrongHoldUIController_hotfix;
    private LuaFunction m_SetQuestInfoPanelGuildMassiveCombatStronghold_hotfix;
    private LuaFunction m_SetRewardGroupList`1_hotfix;
    private LuaFunction m_SetHeroListPanelGuildMassiveCombatStronghold_hotfix;
    private LuaFunction m_GetCombatHeroListGuildMassiveCombatStronghold_hotfix;
    private LuaFunction m_ComparerCombatHeroByPowerHeroHero_hotfix;
    private LuaFunction m_CloseQuestInfoPanel_hotfix;
    private LuaFunction m_OnQuestInfoPanelBackBgButtonClick_hotfix;
    private LuaFunction m_ShowCombatResultEffectBooleanActionSingle_hotfix;
    private LuaFunction m_Co_ShowCombatResultEffectBooleanActionSingle_hotfix;
    private LuaFunction m_OnQuestInfoPanelPersonalButtonClick_hotfix;
    private LuaFunction m_OnQuestInfoPanelTeamButtonClick_hotfix;
    private LuaFunction m_OnRankingButtonClick_hotfix;
    private LuaFunction m_OnRewardsButtonClick_hotfix;
    private LuaFunction m_OnGiveUpButtonClick_hotfix;
    private LuaFunction m_OnChatButtonClick_hotfix;
    private LuaFunction m_OnWorldButtonClick_hotfix;
    private LuaFunction m_OnGuildIntegralButtonClick_hotfix;
    private LuaFunction m_OnGuildCoinButtonClick_hotfix;
    private LuaFunction m_UpdateChatButtonRedmark_hotfix;
    private LuaFunction m_OnInfoButtonClick_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_CloseSubWindow_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnChatButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnChatButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnReturnToWorldAction_hotfix;
    private LuaFunction m_remove_EventOnReturnToWorldAction_hotfix;
    private LuaFunction m_add_EventOnGiveUpButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnGiveUpButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnSingleBattleAction`1_hotfix;
    private LuaFunction m_remove_EventOnSingleBattleAction`1_hotfix;
    private LuaFunction m_add_EventOnTeamBattleAction`1_hotfix;
    private LuaFunction m_remove_EventOnTeamBattleAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GuildMassiveCombatUpdateView(GuildMassiveCombatInfo combatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStrongHolds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildMassiveCombatStrongHoldClick(GuildMassiveCombatStrongHoldUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetQuestInfoPanel(GuildMassiveCombatStronghold strongHold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRewardGroup(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroListPanel(GuildMassiveCombatStronghold strongHoldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Hero> GetCombatHeroList(GuildMassiveCombatStronghold strongHoldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComparerCombatHeroByPower(Hero h1, Hero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseQuestInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnQuestInfoPanelBackBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCombatResultEffect(bool isWin, Action onFinish = null, float delay = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowCombatResultEffect(
      bool isWin,
      Action onFinish,
      float delay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnQuestInfoPanelPersonalButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnQuestInfoPanelTeamButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardsButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGiveUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWorldButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildIntegralButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildCoinButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateChatButtonRedmark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseSubWindow()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnChatButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturnToWorld
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGiveUpButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatStronghold> EventOnSingleBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatStronghold> EventOnTeamBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public GuildMassiveCombatUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChatButtonClick()
    {
      this.EventOnChatButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturnToWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturnToWorld()
    {
      this.EventOnReturnToWorld = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGiveUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGiveUpButtonClick()
    {
      this.EventOnGiveUpButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSingleBattle(GuildMassiveCombatStronghold obj)
    {
    }

    private void __clearDele_EventOnSingleBattle(GuildMassiveCombatStronghold obj)
    {
      this.EventOnSingleBattle = (Action<GuildMassiveCombatStronghold>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTeamBattle(GuildMassiveCombatStronghold obj)
    {
    }

    private void __clearDele_EventOnTeamBattle(GuildMassiveCombatStronghold obj)
    {
      this.EventOnTeamBattle = (Action<GuildMassiveCombatStronghold>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private GuildMassiveCombatUIController m_owner;

      public LuaExportHelper(GuildMassiveCombatUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnChatButtonClick()
      {
        this.m_owner.__callDele_EventOnChatButtonClick();
      }

      public void __clearDele_EventOnChatButtonClick()
      {
        this.m_owner.__clearDele_EventOnChatButtonClick();
      }

      public void __callDele_EventOnReturnToWorld()
      {
        this.m_owner.__callDele_EventOnReturnToWorld();
      }

      public void __clearDele_EventOnReturnToWorld()
      {
        this.m_owner.__clearDele_EventOnReturnToWorld();
      }

      public void __callDele_EventOnGiveUpButtonClick()
      {
        this.m_owner.__callDele_EventOnGiveUpButtonClick();
      }

      public void __clearDele_EventOnGiveUpButtonClick()
      {
        this.m_owner.__clearDele_EventOnGiveUpButtonClick();
      }

      public void __callDele_EventOnSingleBattle(GuildMassiveCombatStronghold obj)
      {
        this.m_owner.__callDele_EventOnSingleBattle(obj);
      }

      public void __clearDele_EventOnSingleBattle(GuildMassiveCombatStronghold obj)
      {
        this.m_owner.__clearDele_EventOnSingleBattle(obj);
      }

      public void __callDele_EventOnTeamBattle(GuildMassiveCombatStronghold obj)
      {
        this.m_owner.__callDele_EventOnTeamBattle(obj);
      }

      public void __clearDele_EventOnTeamBattle(GuildMassiveCombatStronghold obj)
      {
        this.m_owner.__clearDele_EventOnTeamBattle(obj);
      }

      public CommonUIStateController m_stateCtrl
      {
        get
        {
          return this.m_owner.m_stateCtrl;
        }
        set
        {
          this.m_owner.m_stateCtrl = value;
        }
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_infoButton
      {
        get
        {
          return this.m_owner.m_infoButton;
        }
        set
        {
          this.m_owner.m_infoButton = value;
        }
      }

      public Button m_rankingButton
      {
        get
        {
          return this.m_owner.m_rankingButton;
        }
        set
        {
          this.m_owner.m_rankingButton = value;
        }
      }

      public Button m_rewardsButton
      {
        get
        {
          return this.m_owner.m_rewardsButton;
        }
        set
        {
          this.m_owner.m_rewardsButton = value;
        }
      }

      public Button m_giveUpButton
      {
        get
        {
          return this.m_owner.m_giveUpButton;
        }
        set
        {
          this.m_owner.m_giveUpButton = value;
        }
      }

      public Button m_chatButton
      {
        get
        {
          return this.m_owner.m_chatButton;
        }
        set
        {
          this.m_owner.m_chatButton = value;
        }
      }

      public GameObject m_chatButtonRedmark
      {
        get
        {
          return this.m_owner.m_chatButtonRedmark;
        }
        set
        {
          this.m_owner.m_chatButtonRedmark = value;
        }
      }

      public Button m_worldpButton
      {
        get
        {
          return this.m_owner.m_worldpButton;
        }
        set
        {
          this.m_owner.m_worldpButton = value;
        }
      }

      public CommonUIStateController m_difficultLevelStateCtrl
      {
        get
        {
          return this.m_owner.m_difficultLevelStateCtrl;
        }
        set
        {
          this.m_owner.m_difficultLevelStateCtrl = value;
        }
      }

      public Text m_guildCoinText
      {
        get
        {
          return this.m_owner.m_guildCoinText;
        }
        set
        {
          this.m_owner.m_guildCoinText = value;
        }
      }

      public Text m_guildIntegralText
      {
        get
        {
          return this.m_owner.m_guildIntegralText;
        }
        set
        {
          this.m_owner.m_guildIntegralText = value;
        }
      }

      public Button m_guildIntegralButton
      {
        get
        {
          return this.m_owner.m_guildIntegralButton;
        }
        set
        {
          this.m_owner.m_guildIntegralButton = value;
        }
      }

      public Button m_guildCoinButton
      {
        get
        {
          return this.m_owner.m_guildCoinButton;
        }
        set
        {
          this.m_owner.m_guildCoinButton = value;
        }
      }

      public GameObject m_integralPanelDescGo
      {
        get
        {
          return this.m_owner.m_integralPanelDescGo;
        }
        set
        {
          this.m_owner.m_integralPanelDescGo = value;
        }
      }

      public GameObject m_integralPanelBoundauryGo
      {
        get
        {
          return this.m_owner.m_integralPanelBoundauryGo;
        }
        set
        {
          this.m_owner.m_integralPanelBoundauryGo = value;
        }
      }

      public Text m_integralPanelDescNameText
      {
        get
        {
          return this.m_owner.m_integralPanelDescNameText;
        }
        set
        {
          this.m_owner.m_integralPanelDescNameText = value;
        }
      }

      public Text m_integralPanelDescText
      {
        get
        {
          return this.m_owner.m_integralPanelDescText;
        }
        set
        {
          this.m_owner.m_integralPanelDescText = value;
        }
      }

      public GameObject m_questPanelGo
      {
        get
        {
          return this.m_owner.m_questPanelGo;
        }
        set
        {
          this.m_owner.m_questPanelGo = value;
        }
      }

      public CommonUIStateController m_contributionPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_contributionPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_contributionPanelStateCtrl = value;
        }
      }

      public Button m_contributionPanelBackBGButton
      {
        get
        {
          return this.m_owner.m_contributionPanelBackBGButton;
        }
        set
        {
          this.m_owner.m_contributionPanelBackBGButton = value;
        }
      }

      public CommonUIStateController m_rewardInfoPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_rewardInfoPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_rewardInfoPanelStateCtrl = value;
        }
      }

      public Button m_rewardInfoBackBGButton
      {
        get
        {
          return this.m_owner.m_rewardInfoBackBGButton;
        }
        set
        {
          this.m_owner.m_rewardInfoBackBGButton = value;
        }
      }

      public CommonUIStateController m_questInfoPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_questInfoPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_questInfoPanelStateCtrl = value;
        }
      }

      public Button m_questInfoPanelBackBGButton
      {
        get
        {
          return this.m_owner.m_questInfoPanelBackBGButton;
        }
        set
        {
          this.m_owner.m_questInfoPanelBackBGButton = value;
        }
      }

      public Image m_questInfoPanelStageImage
      {
        get
        {
          return this.m_owner.m_questInfoPanelStageImage;
        }
        set
        {
          this.m_owner.m_questInfoPanelStageImage = value;
        }
      }

      public GameObject m_questInfoPanelRewardGroup
      {
        get
        {
          return this.m_owner.m_questInfoPanelRewardGroup;
        }
        set
        {
          this.m_owner.m_questInfoPanelRewardGroup = value;
        }
      }

      public Text m_questInfoPanelHardText
      {
        get
        {
          return this.m_owner.m_questInfoPanelHardText;
        }
        set
        {
          this.m_owner.m_questInfoPanelHardText = value;
        }
      }

      public Text m_questInfoPanelSuppressValueText
      {
        get
        {
          return this.m_owner.m_questInfoPanelSuppressValueText;
        }
        set
        {
          this.m_owner.m_questInfoPanelSuppressValueText = value;
        }
      }

      public ScrollRect m_questInfoPanelHeroListScrollRect
      {
        get
        {
          return this.m_owner.m_questInfoPanelHeroListScrollRect;
        }
        set
        {
          this.m_owner.m_questInfoPanelHeroListScrollRect = value;
        }
      }

      public GameObject m_questInfoPanelHeroListScrollContent
      {
        get
        {
          return this.m_owner.m_questInfoPanelHeroListScrollContent;
        }
        set
        {
          this.m_owner.m_questInfoPanelHeroListScrollContent = value;
        }
      }

      public Button m_questInfoPanelTeamButton
      {
        get
        {
          return this.m_owner.m_questInfoPanelTeamButton;
        }
        set
        {
          this.m_owner.m_questInfoPanelTeamButton = value;
        }
      }

      public Button m_questInfoPanelPersonalButton
      {
        get
        {
          return this.m_owner.m_questInfoPanelPersonalButton;
        }
        set
        {
          this.m_owner.m_questInfoPanelPersonalButton = value;
        }
      }

      public Image m_questInfoPanelBuffIconImage
      {
        get
        {
          return this.m_owner.m_questInfoPanelBuffIconImage;
        }
        set
        {
          this.m_owner.m_questInfoPanelBuffIconImage = value;
        }
      }

      public Text m_questInfoPanelBuffText
      {
        get
        {
          return this.m_owner.m_questInfoPanelBuffText;
        }
        set
        {
          this.m_owner.m_questInfoPanelBuffText = value;
        }
      }

      public GameObject m_questInfoPanelHeroListItemPrefab
      {
        get
        {
          return this.m_owner.m_questInfoPanelHeroListItemPrefab;
        }
        set
        {
          this.m_owner.m_questInfoPanelHeroListItemPrefab = value;
        }
      }

      public CommonUIStateController m_guildMassiveCombatResultStateCtrl
      {
        get
        {
          return this.m_owner.m_guildMassiveCombatResultStateCtrl;
        }
        set
        {
          this.m_owner.m_guildMassiveCombatResultStateCtrl = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public GuildMassiveCombatInfo m_curCombatInfo
      {
        get
        {
          return this.m_owner.m_curCombatInfo;
        }
        set
        {
          this.m_owner.m_curCombatInfo = value;
        }
      }

      public GuildMassiveCombatStronghold m_curSelectStrongHold
      {
        get
        {
          return this.m_owner.m_curSelectStrongHold;
        }
        set
        {
          this.m_owner.m_curSelectStrongHold = value;
        }
      }

      public List<GuildMassiveCombatStrongHoldUIController> m_strongHoldsCtrlList
      {
        get
        {
          return this.m_owner.m_strongHoldsCtrlList;
        }
        set
        {
          this.m_owner.m_strongHoldsCtrlList = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void UpdateStrongHolds()
      {
        this.m_owner.UpdateStrongHolds();
      }

      public void OnGuildMassiveCombatStrongHoldClick(GuildMassiveCombatStrongHoldUIController ctrl)
      {
        this.m_owner.OnGuildMassiveCombatStrongHoldClick(ctrl);
      }

      public void SetQuestInfoPanel(GuildMassiveCombatStronghold strongHold)
      {
        this.m_owner.SetQuestInfoPanel(strongHold);
      }

      public void SetRewardGroup(List<Goods> rewards)
      {
        this.m_owner.SetRewardGroup(rewards);
      }

      public void SetHeroListPanel(GuildMassiveCombatStronghold strongHoldInfo)
      {
        this.m_owner.SetHeroListPanel(strongHoldInfo);
      }

      public List<Hero> GetCombatHeroList(GuildMassiveCombatStronghold strongHoldInfo)
      {
        return this.m_owner.GetCombatHeroList(strongHoldInfo);
      }

      public int ComparerCombatHeroByPower(Hero h1, Hero h2)
      {
        return this.m_owner.ComparerCombatHeroByPower(h1, h2);
      }

      public void OnQuestInfoPanelBackBgButtonClick()
      {
        this.m_owner.OnQuestInfoPanelBackBgButtonClick();
      }

      public IEnumerator Co_ShowCombatResultEffect(
        bool isWin,
        Action onFinish,
        float delay)
      {
        return this.m_owner.Co_ShowCombatResultEffect(isWin, onFinish, delay);
      }

      public void OnQuestInfoPanelPersonalButtonClick()
      {
        this.m_owner.OnQuestInfoPanelPersonalButtonClick();
      }

      public void OnQuestInfoPanelTeamButtonClick()
      {
        this.m_owner.OnQuestInfoPanelTeamButtonClick();
      }

      public void OnRankingButtonClick()
      {
        this.m_owner.OnRankingButtonClick();
      }

      public void OnRewardsButtonClick()
      {
        this.m_owner.OnRewardsButtonClick();
      }

      public void OnGiveUpButtonClick()
      {
        this.m_owner.OnGiveUpButtonClick();
      }

      public void OnChatButtonClick()
      {
        this.m_owner.OnChatButtonClick();
      }

      public void OnWorldButtonClick()
      {
        this.m_owner.OnWorldButtonClick();
      }

      public void OnGuildIntegralButtonClick()
      {
        this.m_owner.OnGuildIntegralButtonClick();
      }

      public void OnGuildCoinButtonClick()
      {
        this.m_owner.OnGuildCoinButtonClick();
      }

      public void OnInfoButtonClick()
      {
        this.m_owner.OnInfoButtonClick();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }
    }
  }
}
