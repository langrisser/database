﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleResultScoreUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleResultScoreUIController : UIControllerBase
  {
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/ScoreGroup/Get", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_getScoreUIStateController;
    [AutoBind("./Panel/ScoreGroup/Get/Name/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_getScoreNameText;
    [AutoBind("./Panel/ScoreGroup/Get/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_getScoreText;
    [AutoBind("./Panel/ScoreGroup/Get/Text2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_getScoreText2;
    [AutoBind("./Panel/ScoreGroup/Get/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_getScoreDescText;
    [AutoBind("./Panel/ScoreGroup/Have/Name/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_haveScoreNameText;
    [AutoBind("./Panel/ScoreGroup/Have/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_haveScoreText;
    [AutoBind("./Panel/RewardGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_rewardGroupScrollRect;
    [AutoBind("./Panel/NextRewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_nextRewardGroupGameObject;
    [AutoBind("./Panel/NextRewardGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nextRewardScoreText;
    [AutoBind("./Panel/NextRewardGroup/RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_nextRewardGroupTransform;
    private bool m_isClick;
    [DoNotToLua]
    private BattleResultScoreUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_ShowBattleResultScoreBattleReward_hotfix;
    private LuaFunction m_Co_ShowBattleResultScoreBattleReward_hotfix;
    private LuaFunction m_SetUnchartedScoreRewardBattleRewardConfigDataUnchartedScoreInfo_hotfix;
    private LuaFunction m_SetGuildMassiveCombatScoreRewardBattleRewardConfigDataGuildMassiveCombatLevelInfo_hotfix;
    private LuaFunction m_SetRewardBattleRewardStringInt32Int32List`1_hotfix;
    private LuaFunction m_Co_SetAndWaitUIStateCommonUIStateControllerString_hotfix;
    private LuaFunction m_Co_WaitClick_hotfix;
    private LuaFunction m_OnBackgroundButtonClick_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;

    private BattleResultScoreUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleResultScore(BattleReward battleReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowBattleResultScore(BattleReward battleReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUnchartedScoreReward(
      BattleReward battleReward,
      ConfigDataUnchartedScoreInfo unchartedScoreInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGuildMassiveCombatScoreReward(
      BattleReward battleReward,
      ConfigDataGuildMassiveCombatLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetReward(
      BattleReward battleReward,
      string scoreName,
      int curScore,
      int nextScore,
      List<Goods> nextRewardGoods)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BattleResultScoreUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleResultScoreUIController m_owner;

      public LuaExportHelper(BattleResultScoreUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public Button m_backgroundButton
      {
        get
        {
          return this.m_owner.m_backgroundButton;
        }
        set
        {
          this.m_owner.m_backgroundButton = value;
        }
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public CommonUIStateController m_getScoreUIStateController
      {
        get
        {
          return this.m_owner.m_getScoreUIStateController;
        }
        set
        {
          this.m_owner.m_getScoreUIStateController = value;
        }
      }

      public Text m_getScoreNameText
      {
        get
        {
          return this.m_owner.m_getScoreNameText;
        }
        set
        {
          this.m_owner.m_getScoreNameText = value;
        }
      }

      public Text m_getScoreText
      {
        get
        {
          return this.m_owner.m_getScoreText;
        }
        set
        {
          this.m_owner.m_getScoreText = value;
        }
      }

      public Text m_getScoreText2
      {
        get
        {
          return this.m_owner.m_getScoreText2;
        }
        set
        {
          this.m_owner.m_getScoreText2 = value;
        }
      }

      public Text m_getScoreDescText
      {
        get
        {
          return this.m_owner.m_getScoreDescText;
        }
        set
        {
          this.m_owner.m_getScoreDescText = value;
        }
      }

      public Text m_haveScoreNameText
      {
        get
        {
          return this.m_owner.m_haveScoreNameText;
        }
        set
        {
          this.m_owner.m_haveScoreNameText = value;
        }
      }

      public Text m_haveScoreText
      {
        get
        {
          return this.m_owner.m_haveScoreText;
        }
        set
        {
          this.m_owner.m_haveScoreText = value;
        }
      }

      public ScrollRect m_rewardGroupScrollRect
      {
        get
        {
          return this.m_owner.m_rewardGroupScrollRect;
        }
        set
        {
          this.m_owner.m_rewardGroupScrollRect = value;
        }
      }

      public GameObject m_nextRewardGroupGameObject
      {
        get
        {
          return this.m_owner.m_nextRewardGroupGameObject;
        }
        set
        {
          this.m_owner.m_nextRewardGroupGameObject = value;
        }
      }

      public Text m_nextRewardScoreText
      {
        get
        {
          return this.m_owner.m_nextRewardScoreText;
        }
        set
        {
          this.m_owner.m_nextRewardScoreText = value;
        }
      }

      public Transform m_nextRewardGroupTransform
      {
        get
        {
          return this.m_owner.m_nextRewardGroupTransform;
        }
        set
        {
          this.m_owner.m_nextRewardGroupTransform = value;
        }
      }

      public bool m_isClick
      {
        get
        {
          return this.m_owner.m_isClick;
        }
        set
        {
          this.m_owner.m_isClick = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public IEnumerator Co_ShowBattleResultScore(BattleReward battleReward)
      {
        return this.m_owner.Co_ShowBattleResultScore(battleReward);
      }

      public void SetUnchartedScoreReward(
        BattleReward battleReward,
        ConfigDataUnchartedScoreInfo unchartedScoreInfo)
      {
        this.m_owner.SetUnchartedScoreReward(battleReward, unchartedScoreInfo);
      }

      public void SetGuildMassiveCombatScoreReward(
        BattleReward battleReward,
        ConfigDataGuildMassiveCombatLevelInfo levelInfo)
      {
        this.m_owner.SetGuildMassiveCombatScoreReward(battleReward, levelInfo);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetReward(
        BattleReward battleReward,
        string scoreName,
        int curScore,
        int nextScore,
        List<Goods> nextRewardGoods)
      {
        // ISSUE: unable to decompile the method.
      }

      public IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
      {
        return this.m_owner.Co_SetAndWaitUIState(ctrl, state);
      }

      public IEnumerator Co_WaitClick()
      {
        return this.m_owner.Co_WaitClick();
      }

      public void OnBackgroundButtonClick()
      {
        this.m_owner.OnBackgroundButtonClick();
      }
    }
  }
}
