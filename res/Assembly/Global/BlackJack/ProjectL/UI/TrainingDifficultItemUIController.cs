﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TrainingDifficultItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TrainingDifficultItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateCtrl;
    [AutoBind("./UnLockText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unLockText;
    [AutoBind("./NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_newImage;
    [AutoBind("./SelectImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectImage;
    private bool m_isLock;
    private bool m_isNew;
    public int DifficultLevel;
    private int m_unlockPlayerLevel;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitDifficultItem(int unlockPlayerLevel, int difficult)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public void ShowSelectFrame(bool isShow)
    {
      this.m_selectImage.SetActive(isShow);
    }

    public event Action<TrainingDifficultItemUIController> EventOnItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
