﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AnikiGymListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class AnikiGymListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_toggle;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./NameTextGray", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameTextGray;
    [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./BGImageGray", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImageGray;
    [AutoBind("./OpenTime/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_openTimeText;
    [AutoBind("./Time", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject;
    [AutoBind("./Time/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./NotOpenButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_notOpenButton;
    private ConfigDataAnikiGymInfo m_anikiGymInfo;
    private bool m_isLocked;
    private DateTime m_expireTime;
    private float m_updateCountdown;
    [DoNotToLua]
    private AnikiGymListItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetAnikiGymInfoConfigDataAnikiGymInfo_hotfix;
    private LuaFunction m_GetAnikiGymInfo_hotfix;
    private LuaFunction m_SetLockedBoolean_hotfix;
    private LuaFunction m_IsLocked_hotfix;
    private LuaFunction m_SetSelectedBoolean_hotfix;
    private LuaFunction m_IsSelected_hotfix;
    private LuaFunction m_SetExpireTimeDateTime_hotfix;
    private LuaFunction m_UpdateRemainTime_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_OnToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnNotOpenButtonClick_hotfix;
    private LuaFunction m_add_EventOnToggleValueChangedAction`2_hotfix;
    private LuaFunction m_remove_EventOnToggleValueChangedAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public AnikiGymListItemUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnikiGymInfo(ConfigDataAnikiGymInfo anikiGymInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAnikiGymInfo GetAnikiGymInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocked(bool locked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelected(bool selected)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSelected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetExpireTime(DateTime expireTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRemainTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNotOpenButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool, AnikiGymListItemUIController> EventOnToggleValueChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public AnikiGymListItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnToggleValueChanged(bool arg1, AnikiGymListItemUIController arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnToggleValueChanged(bool arg1, AnikiGymListItemUIController arg2)
    {
      this.EventOnToggleValueChanged = (Action<bool, AnikiGymListItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private AnikiGymListItemUIController m_owner;

      public LuaExportHelper(AnikiGymListItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnToggleValueChanged(bool arg1, AnikiGymListItemUIController arg2)
      {
        this.m_owner.__callDele_EventOnToggleValueChanged(arg1, arg2);
      }

      public void __clearDele_EventOnToggleValueChanged(
        bool arg1,
        AnikiGymListItemUIController arg2)
      {
        this.m_owner.__clearDele_EventOnToggleValueChanged(arg1, arg2);
      }

      public Toggle m_toggle
      {
        get
        {
          return this.m_owner.m_toggle;
        }
        set
        {
          this.m_owner.m_toggle = value;
        }
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public Text m_nameTextGray
      {
        get
        {
          return this.m_owner.m_nameTextGray;
        }
        set
        {
          this.m_owner.m_nameTextGray = value;
        }
      }

      public Image m_iconImage
      {
        get
        {
          return this.m_owner.m_iconImage;
        }
        set
        {
          this.m_owner.m_iconImage = value;
        }
      }

      public Image m_iconImageGray
      {
        get
        {
          return this.m_owner.m_iconImageGray;
        }
        set
        {
          this.m_owner.m_iconImageGray = value;
        }
      }

      public Text m_openTimeText
      {
        get
        {
          return this.m_owner.m_openTimeText;
        }
        set
        {
          this.m_owner.m_openTimeText = value;
        }
      }

      public GameObject m_timeGameObject
      {
        get
        {
          return this.m_owner.m_timeGameObject;
        }
        set
        {
          this.m_owner.m_timeGameObject = value;
        }
      }

      public Text m_timeText
      {
        get
        {
          return this.m_owner.m_timeText;
        }
        set
        {
          this.m_owner.m_timeText = value;
        }
      }

      public Button m_notOpenButton
      {
        get
        {
          return this.m_owner.m_notOpenButton;
        }
        set
        {
          this.m_owner.m_notOpenButton = value;
        }
      }

      public ConfigDataAnikiGymInfo m_anikiGymInfo
      {
        get
        {
          return this.m_owner.m_anikiGymInfo;
        }
        set
        {
          this.m_owner.m_anikiGymInfo = value;
        }
      }

      public bool m_isLocked
      {
        get
        {
          return this.m_owner.m_isLocked;
        }
        set
        {
          this.m_owner.m_isLocked = value;
        }
      }

      public DateTime m_expireTime
      {
        get
        {
          return this.m_owner.m_expireTime;
        }
        set
        {
          this.m_owner.m_expireTime = value;
        }
      }

      public float m_updateCountdown
      {
        get
        {
          return this.m_owner.m_updateCountdown;
        }
        set
        {
          this.m_owner.m_updateCountdown = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void UpdateRemainTime()
      {
        this.m_owner.UpdateRemainTime();
      }

      public void Update()
      {
        this.m_owner.Update();
      }

      public void OnToggleValueChanged(bool on)
      {
        this.m_owner.OnToggleValueChanged(on);
      }

      public void OnNotOpenButtonClick()
      {
        this.m_owner.OnNotOpenButtonClick();
      }
    }
  }
}
