﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CommonUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class CommonUIController : UIControllerBase
  {
    public static bool m_hasFinishAndroidBackEvent;
    private static bool m_isRunningMessageBoxAndQuitApp;
    private static CommonUIController s_instance;
    public TestUI TestUI;
    [AutoBind("./ASRRoot", AutoBindAttribute.InitState.NotInit, false)]
    public XunfeiSDKWrapper m_xfWrapper;
    [AutoBind("./Loading", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_loadingGameObject;
    [AutoBind("./DisableInput", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_disableInputGameObject;
    [AutoBind("./DisableInput/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_disableInputHintImage;
    [AutoBind("./TransparentMask", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_transparentMaskGameObject;
    [AutoBind("./BlackFrame", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_blackFrameGameObject;
    [AutoBind("./Fade", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_fadeImage;
    [AutoBind("./TestUIBackground", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_testUIBackgroundGameObject;
    [AutoBind("./TestUIBackground/ToolBar", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_testUIBackgroundToolBarGameObject;
    [AutoBind("./TestUIBackground/ToolToggle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_testUIBackgroundToolToggleGameObject;
    [AutoBind("./iPhoneXTest", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_iPhoneXTestGameObject;
    [AutoBind("./Message", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_messageUIStateController;
    [AutoBind("./Message/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_messageText;
    [AutoBind("./Explanation", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_explanationUIStateController;
    [AutoBind("./Explanation/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_explanationBackgroundButton;
    [AutoBind("./Explanation/Panel/BGImage/FrameImage/Scrollrect/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_explanationText;
    [AutoBind("./Notice", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noticeGameObject;
    [AutoBind("./Notice/Mask/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_noticeText;
    [AutoBind("./Recorder", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_screenRecorderGameObject;
    [AutoBind("./Tip", AutoBindAttribute.InitState.Inactive, false)]
    private CommonUIStateController m_tipUIStateController;
    [AutoBind("./Tip/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tipText;
    private DialogBox m_dialogBox;
    private CommonUIStateController m_networkWatingStateController;
    private ScreenFade m_screenFade;
    private bool m_hideFadeOutLoadingFadeIn;
    private float m_defaultFadeOutTime;
    private float m_defaultFadeInTime;
    private const float NoticeTweenSpeed = 100f;
    private const float ShowNoticeInterval = 2f;
    private const float ShowNoticeTime = 10f;
    private bool m_isShowingNotice;
    private List<string> m_waitingNotices;
    private Vector3 m_noticeTextInitPos;
    private float m_noticeTextInitWidth;
    private Coroutine m_delayHideNoticeCoroutine;
    private TweenPos m_noticeShowTween;
    private TweenPos m_noticeMoveTween;
    private bool m_isGameDisableInput;
    private bool m_isFrameworkUITaskDisableInput;
    private bool m_isFrameworkNetTaskDisableInput;
    private Coroutine m_showMessageCoroutine;
    private Action m_showMessageEndAction;
    private DateTime m_pauseTime;
    private DateTime m_unfocusDateTime;
    private double m_unfocusTimer;
    private TouchFx m_touchFx;
    private Vector2 m_mysteriesScrollRectValue;
    [DoNotToLua]
    private CommonUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnDestroy_hotfix;
    private LuaFunction m_InitTouchFx_hotfix;
    private LuaFunction m_DisposeTouchFx_hotfix;
    private LuaFunction m_SetTouchFxStyleInt32_hotfix;
    private LuaFunction m_SetTouchFXParentActiveBoolean_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_ShowMessageStringSingleActionBoolean_hotfix;
    private LuaFunction m_Co_ShowMessageStringSingleActionBoolean_hotfix;
    private LuaFunction m_ShowMessageStringTableIdSingleActionBoolean_hotfix;
    private LuaFunction m_ShowErrorMessageInt32SingleActionBoolean_hotfix;
    private LuaFunction m_ShowDialogBoxStringAction`1StringString_hotfix;
    private LuaFunction m_CancelDialogBox_hotfix;
    private LuaFunction m_ShowDialogBoxStringTableIdAction`1StringTableIdStringTableId_hotfix;
    private LuaFunction m_ShowExplanationString_hotfix;
    private LuaFunction m_ShowExplanationExplanationId_hotfix;
    private LuaFunction m_ShowTipString_hotfix;
    private LuaFunction m_ShowNoticeString_hotfix;
    private LuaFunction m_ShowNextNotice_hotfix;
    private LuaFunction m_OnNoticeShowTweenFinished_hotfix;
    private LuaFunction m_OnNoticeMoveTweenFinished_hotfix;
    private LuaFunction m_DelayHideNoticeSingle_hotfix;
    private LuaFunction m_ShowLoadingFadeStyle_hotfix;
    private LuaFunction m_HideLoading_hotfix;
    private LuaFunction m_ShowWaitingNetBoolean_hotfix;
    private LuaFunction m_EnableInputBoolean_hotfix;
    private LuaFunction m_IsEnableInput_hotfix;
    private LuaFunction m_FrameworkUITaskEnableInputBoolean_hotfix;
    private LuaFunction m_IsFrameworkUITaskDisableInput_hotfix;
    private LuaFunction m_FrameworkNetTaskEnableInputBoolean_hotfix;
    private LuaFunction m_IsFrameworkNetTaskDisableInput_hotfix;
    private LuaFunction m_UpdateDisableInput_hotfix;
    private LuaFunction m_IsAnyDisableInput_hotfix;
    private LuaFunction m_IsDisableInputObjectGameObject_hotfix;
    private LuaFunction m_IsTestToolbarObjectGameObject_hotfix;
    private LuaFunction m_SetDisableInputHintColorColor_hotfix;
    private LuaFunction m_ShowBlackFrameBoolean_hotfix;
    private LuaFunction m_ShowTestUIBackgroundBooleanBoolean_hotfix;
    private LuaFunction m_ShowiPhoneXTestBoolean_hotfix;
    private LuaFunction m_StartFadeOutActionFadeStyleSingle_hotfix;
    private LuaFunction m_StartShowFadeOutLoadingFadeInActionFadeStyleSingleSingle_hotfix;
    private LuaFunction m_ShowFadeOutLoadingFadeInActionFadeStyleSingleSingle_hotfix;
    private LuaFunction m_HideFadeOutLoadingFadeIn_hotfix;
    private LuaFunction m_FadeInSingleColorAction_hotfix;
    private LuaFunction m_FadeOutSingleColorAction_hotfix;
    private LuaFunction m_IsFading_hotfix;
    private LuaFunction m_set_EnableScreenRecordFunctionBoolean_hotfix;
    private LuaFunction m_get_EnableScreenRecordFunction_hotfix;
    private LuaFunction m_OnApplicationFocusBoolean_hotfix;
    private LuaFunction m_OnApplicationPauseBoolean_hotfix;
    private LuaFunction m_CloseExplanationWindow_hotfix;
    private LuaFunction m_SaveMysteriesScrollRectValueVector2_hotfix;
    private LuaFunction m_SetMysteriesScrollRectScrollRect_hotfix;
    private LuaFunction m_Co_SetMysteriesScrollRectScrollRect_hotfix;
    private LuaFunction m_OnExplanationBackgroundButtonClick_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CommonUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StaticLateUpdate()
    {
      if (CommonUIController.m_hasFinishAndroidBackEvent)
      {
        CommonUIController.m_hasFinishAndroidBackEvent = false;
      }
      else
      {
        if (!Input.GetKeyDown(KeyCode.Escape))
          return;
        CommonUIController.OnAndroidBackKeyUp();
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnAndroidBackKeyUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator MessageBoxAndQuitApp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnDestroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTouchFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DisposeTouchFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTouchFxStyle(int style)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTouchFXParentActive(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(string txt, float time = 2f, Action onEnd = null, bool disableInput = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowMessage(
      string txt,
      float time,
      Action onEnd,
      bool disableInput)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(StringTableId id, float time = 2f, Action onEnd = null, bool disableInput = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowErrorMessage(int errorCode, float time = 2f, Action onEnd = null, bool disableInput = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDialogBox(
      string msgText,
      Action<DialogBoxResult> callback,
      string okText = "",
      string cancelText = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CancelDialogBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDialogBox(
      StringTableId msgId,
      Action<DialogBoxResult> callback,
      StringTableId okId = (StringTableId) 0,
      StringTableId cancelId = (StringTableId) 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowExplanation(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowExplanation(ExplanationId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTip(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNotice(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowNextNotice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeShowTweenFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeMoveTweenFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayHideNotice(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowLoading(FadeStyle style)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideLoading()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitingNet(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableInput(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnableInput()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FrameworkUITaskEnableInput(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFrameworkUITaskDisableInput()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FrameworkNetTaskEnableInput(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFrameworkNetTaskDisableInput()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateDisableInput()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyDisableInput()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDisableInputObject(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTestToolbarObject(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDisableInputHintColor(Color c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBlackFrame(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTestUIBackground(bool showToggle, bool showBar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowiPhoneXTest(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartFadeOut(Action fadeoutEnd, FadeStyle style = FadeStyle.Black, float fadeOutTime = -1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartShowFadeOutLoadingFadeIn(
      Action fadeOutEnd,
      FadeStyle style = FadeStyle.Black,
      float fadeOutTime = -1f,
      float fadeInTime = -1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ShowFadeOutLoadingFadeIn(
      Action fadeOutEnd,
      FadeStyle style,
      float fadeOutTime,
      float fadeInTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideFadeOutLoadingFadeIn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FadeIn(float time, Color color, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FadeOut(float time, Color color, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFading()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool EnableScreenRecordFunction
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationFocus(bool focus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationPause(bool isPause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseExplanationWindow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveMysteriesScrollRectValue(Vector2 v)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMysteriesScrollRect(ScrollRect sr)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetMysteriesScrollRect(ScrollRect sr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExplanationBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public static CommonUIController Instance
    {
      get
      {
        return CommonUIController.s_instance;
      }
    }

    [DoNotToLua]
    public CommonUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      base.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_m_showMessageEndAction()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_m_showMessageEndAction()
    {
      this.m_showMessageEndAction = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CommonUIController m_owner;

      public LuaExportHelper(CommonUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_m_showMessageEndAction()
      {
        this.m_owner.__callDele_m_showMessageEndAction();
      }

      public void __clearDele_m_showMessageEndAction()
      {
        this.m_owner.__clearDele_m_showMessageEndAction();
      }

      public static bool m_isRunningMessageBoxAndQuitApp
      {
        get
        {
          return CommonUIController.m_isRunningMessageBoxAndQuitApp;
        }
        set
        {
          CommonUIController.m_isRunningMessageBoxAndQuitApp = value;
        }
      }

      public static CommonUIController s_instance
      {
        get
        {
          return CommonUIController.s_instance;
        }
        set
        {
          CommonUIController.s_instance = value;
        }
      }

      public GameObject m_loadingGameObject
      {
        get
        {
          return this.m_owner.m_loadingGameObject;
        }
        set
        {
          this.m_owner.m_loadingGameObject = value;
        }
      }

      public GameObject m_disableInputGameObject
      {
        get
        {
          return this.m_owner.m_disableInputGameObject;
        }
        set
        {
          this.m_owner.m_disableInputGameObject = value;
        }
      }

      public Image m_disableInputHintImage
      {
        get
        {
          return this.m_owner.m_disableInputHintImage;
        }
        set
        {
          this.m_owner.m_disableInputHintImage = value;
        }
      }

      public GameObject m_transparentMaskGameObject
      {
        get
        {
          return this.m_owner.m_transparentMaskGameObject;
        }
        set
        {
          this.m_owner.m_transparentMaskGameObject = value;
        }
      }

      public GameObject m_blackFrameGameObject
      {
        get
        {
          return this.m_owner.m_blackFrameGameObject;
        }
        set
        {
          this.m_owner.m_blackFrameGameObject = value;
        }
      }

      public Image m_fadeImage
      {
        get
        {
          return this.m_owner.m_fadeImage;
        }
        set
        {
          this.m_owner.m_fadeImage = value;
        }
      }

      public GameObject m_testUIBackgroundGameObject
      {
        get
        {
          return this.m_owner.m_testUIBackgroundGameObject;
        }
        set
        {
          this.m_owner.m_testUIBackgroundGameObject = value;
        }
      }

      public GameObject m_testUIBackgroundToolBarGameObject
      {
        get
        {
          return this.m_owner.m_testUIBackgroundToolBarGameObject;
        }
        set
        {
          this.m_owner.m_testUIBackgroundToolBarGameObject = value;
        }
      }

      public GameObject m_testUIBackgroundToolToggleGameObject
      {
        get
        {
          return this.m_owner.m_testUIBackgroundToolToggleGameObject;
        }
        set
        {
          this.m_owner.m_testUIBackgroundToolToggleGameObject = value;
        }
      }

      public GameObject m_iPhoneXTestGameObject
      {
        get
        {
          return this.m_owner.m_iPhoneXTestGameObject;
        }
        set
        {
          this.m_owner.m_iPhoneXTestGameObject = value;
        }
      }

      public CommonUIStateController m_messageUIStateController
      {
        get
        {
          return this.m_owner.m_messageUIStateController;
        }
        set
        {
          this.m_owner.m_messageUIStateController = value;
        }
      }

      public Text m_messageText
      {
        get
        {
          return this.m_owner.m_messageText;
        }
        set
        {
          this.m_owner.m_messageText = value;
        }
      }

      public CommonUIStateController m_explanationUIStateController
      {
        get
        {
          return this.m_owner.m_explanationUIStateController;
        }
        set
        {
          this.m_owner.m_explanationUIStateController = value;
        }
      }

      public Button m_explanationBackgroundButton
      {
        get
        {
          return this.m_owner.m_explanationBackgroundButton;
        }
        set
        {
          this.m_owner.m_explanationBackgroundButton = value;
        }
      }

      public Text m_explanationText
      {
        get
        {
          return this.m_owner.m_explanationText;
        }
        set
        {
          this.m_owner.m_explanationText = value;
        }
      }

      public GameObject m_noticeGameObject
      {
        get
        {
          return this.m_owner.m_noticeGameObject;
        }
        set
        {
          this.m_owner.m_noticeGameObject = value;
        }
      }

      public Text m_noticeText
      {
        get
        {
          return this.m_owner.m_noticeText;
        }
        set
        {
          this.m_owner.m_noticeText = value;
        }
      }

      public GameObject m_screenRecorderGameObject
      {
        get
        {
          return this.m_owner.m_screenRecorderGameObject;
        }
        set
        {
          this.m_owner.m_screenRecorderGameObject = value;
        }
      }

      public CommonUIStateController m_tipUIStateController
      {
        get
        {
          return this.m_owner.m_tipUIStateController;
        }
        set
        {
          this.m_owner.m_tipUIStateController = value;
        }
      }

      public Text m_tipText
      {
        get
        {
          return this.m_owner.m_tipText;
        }
        set
        {
          this.m_owner.m_tipText = value;
        }
      }

      public DialogBox m_dialogBox
      {
        get
        {
          return this.m_owner.m_dialogBox;
        }
        set
        {
          this.m_owner.m_dialogBox = value;
        }
      }

      public CommonUIStateController m_networkWatingStateController
      {
        get
        {
          return this.m_owner.m_networkWatingStateController;
        }
        set
        {
          this.m_owner.m_networkWatingStateController = value;
        }
      }

      public ScreenFade m_screenFade
      {
        get
        {
          return this.m_owner.m_screenFade;
        }
        set
        {
          this.m_owner.m_screenFade = value;
        }
      }

      public bool m_hideFadeOutLoadingFadeIn
      {
        get
        {
          return this.m_owner.m_hideFadeOutLoadingFadeIn;
        }
        set
        {
          this.m_owner.m_hideFadeOutLoadingFadeIn = value;
        }
      }

      public float m_defaultFadeOutTime
      {
        get
        {
          return this.m_owner.m_defaultFadeOutTime;
        }
        set
        {
          this.m_owner.m_defaultFadeOutTime = value;
        }
      }

      public float m_defaultFadeInTime
      {
        get
        {
          return this.m_owner.m_defaultFadeInTime;
        }
        set
        {
          this.m_owner.m_defaultFadeInTime = value;
        }
      }

      public static float NoticeTweenSpeed
      {
        get
        {
          return 100f;
        }
      }

      public static float ShowNoticeInterval
      {
        get
        {
          return 2f;
        }
      }

      public static float ShowNoticeTime
      {
        get
        {
          return 10f;
        }
      }

      public bool m_isShowingNotice
      {
        get
        {
          return this.m_owner.m_isShowingNotice;
        }
        set
        {
          this.m_owner.m_isShowingNotice = value;
        }
      }

      public List<string> m_waitingNotices
      {
        get
        {
          return this.m_owner.m_waitingNotices;
        }
        set
        {
          this.m_owner.m_waitingNotices = value;
        }
      }

      public Vector3 m_noticeTextInitPos
      {
        get
        {
          return this.m_owner.m_noticeTextInitPos;
        }
        set
        {
          this.m_owner.m_noticeTextInitPos = value;
        }
      }

      public float m_noticeTextInitWidth
      {
        get
        {
          return this.m_owner.m_noticeTextInitWidth;
        }
        set
        {
          this.m_owner.m_noticeTextInitWidth = value;
        }
      }

      public Coroutine m_delayHideNoticeCoroutine
      {
        get
        {
          return this.m_owner.m_delayHideNoticeCoroutine;
        }
        set
        {
          this.m_owner.m_delayHideNoticeCoroutine = value;
        }
      }

      public TweenPos m_noticeShowTween
      {
        get
        {
          return this.m_owner.m_noticeShowTween;
        }
        set
        {
          this.m_owner.m_noticeShowTween = value;
        }
      }

      public TweenPos m_noticeMoveTween
      {
        get
        {
          return this.m_owner.m_noticeMoveTween;
        }
        set
        {
          this.m_owner.m_noticeMoveTween = value;
        }
      }

      public bool m_isGameDisableInput
      {
        get
        {
          return this.m_owner.m_isGameDisableInput;
        }
        set
        {
          this.m_owner.m_isGameDisableInput = value;
        }
      }

      public bool m_isFrameworkUITaskDisableInput
      {
        get
        {
          return this.m_owner.m_isFrameworkUITaskDisableInput;
        }
        set
        {
          this.m_owner.m_isFrameworkUITaskDisableInput = value;
        }
      }

      public bool m_isFrameworkNetTaskDisableInput
      {
        get
        {
          return this.m_owner.m_isFrameworkNetTaskDisableInput;
        }
        set
        {
          this.m_owner.m_isFrameworkNetTaskDisableInput = value;
        }
      }

      public Coroutine m_showMessageCoroutine
      {
        get
        {
          return this.m_owner.m_showMessageCoroutine;
        }
        set
        {
          this.m_owner.m_showMessageCoroutine = value;
        }
      }

      public Action m_showMessageEndAction
      {
        get
        {
          return this.m_owner.m_showMessageEndAction;
        }
        set
        {
          this.m_owner.m_showMessageEndAction = value;
        }
      }

      public DateTime m_pauseTime
      {
        get
        {
          return this.m_owner.m_pauseTime;
        }
        set
        {
          this.m_owner.m_pauseTime = value;
        }
      }

      public DateTime m_unfocusDateTime
      {
        get
        {
          return this.m_owner.m_unfocusDateTime;
        }
        set
        {
          this.m_owner.m_unfocusDateTime = value;
        }
      }

      public double m_unfocusTimer
      {
        get
        {
          return this.m_owner.m_unfocusTimer;
        }
        set
        {
          this.m_owner.m_unfocusTimer = value;
        }
      }

      public TouchFx m_touchFx
      {
        get
        {
          return this.m_owner.m_touchFx;
        }
        set
        {
          this.m_owner.m_touchFx = value;
        }
      }

      public Vector2 m_mysteriesScrollRectValue
      {
        get
        {
          return this.m_owner.m_mysteriesScrollRectValue;
        }
        set
        {
          this.m_owner.m_mysteriesScrollRectValue = value;
        }
      }

      public static IEnumerator MessageBoxAndQuitApp()
      {
        return CommonUIController.MessageBoxAndQuitApp();
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnDestroy()
      {
        this.m_owner.OnDestroy();
      }

      public void Update()
      {
        this.m_owner.Update();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public IEnumerator Co_ShowMessage(
        string txt,
        float time,
        Action onEnd,
        bool disableInput)
      {
        // ISSUE: unable to decompile the method.
      }

      public void ShowNextNotice()
      {
        this.m_owner.ShowNextNotice();
      }

      public void OnNoticeShowTweenFinished()
      {
        this.m_owner.OnNoticeShowTweenFinished();
      }

      public void OnNoticeMoveTweenFinished()
      {
        this.m_owner.OnNoticeMoveTweenFinished();
      }

      public IEnumerator DelayHideNotice(float time)
      {
        return this.m_owner.DelayHideNotice(time);
      }

      public void UpdateDisableInput()
      {
        this.m_owner.UpdateDisableInput();
      }

      public void SetDisableInputHintColor(Color c)
      {
        this.m_owner.SetDisableInputHintColor(c);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public IEnumerator ShowFadeOutLoadingFadeIn(
        Action fadeOutEnd,
        FadeStyle style,
        float fadeOutTime,
        float fadeInTime)
      {
        // ISSUE: unable to decompile the method.
      }

      public void OnApplicationFocus(bool focus)
      {
        this.m_owner.OnApplicationFocus(focus);
      }

      public void OnApplicationPause(bool isPause)
      {
        this.m_owner.OnApplicationPause(isPause);
      }

      public IEnumerator Co_SetMysteriesScrollRect(ScrollRect sr)
      {
        return this.m_owner.Co_SetMysteriesScrollRect(sr);
      }

      public void OnExplanationBackgroundButtonClick()
      {
        this.m_owner.OnExplanationBackgroundButtonClick();
      }
    }
  }
}
