﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GoddessDialogBoxUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Art;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GoddessDialogBoxUIController : UIControllerBase
  {
    private const int WordsSpeed = 45;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private DialogText m_text;
    [AutoBind("./Arrow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_waitGameObject;
    private float m_time;
    private float m_voicePlayTime;
    private int m_wordsDisplayLength;
    private int m_wordsDisplayLengthMax;
    private bool m_isOpened;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsOpened()
    {
      return this.m_isOpened;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWords(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetVoicePlayTime(float voicePlayTime)
    {
      this.m_voicePlayTime = voicePlayTime;
    }

    public float GetWordsDisplayTime()
    {
      return (float) this.m_wordsDisplayLengthMax / 45f;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DisplayAllWords()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsAllWordsDisplayed()
    {
      return this.m_wordsDisplayLength == this.m_wordsDisplayLengthMax;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
