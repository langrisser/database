﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MailDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class MailDetailUIController : UIControllerBase
  {
    private Mail m_currentMailInfo;
    private ProjectLPlayerContext m_playerContext;
    private List<RewardGoodsUIController> m_attachmentUICtrlList;
    private GameObject ItemUICtrlPrefab;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController BgUIState;
    [AutoBind("./MailDetail_Title/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text TitleText;
    [AutoBind("./TimeOutTextPanel/TimeOutText", AutoBindAttribute.InitState.NotInit, false)]
    public Text TimeOutText;
    [AutoBind("./MailDetail/ListScrollView/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text ContextText;
    [AutoBind("./MailDetail/ListScrollViewSmall/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text SmallContextText;
    [AutoBind("./MailDetail/RewardItem/Content", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject RewardRoot;
    [AutoBind("./MailDetail/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx GetButton;
    [AutoBind("./MailDetail/GotoButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button GotoButton;
    [AutoBind("./NoMailPanel/ChooseText", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ChooseTextGo;
    [AutoBind("./NoMailPanel/NoneText", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject NoneTextGo;
    [DoNotToLua]
    private MailDetailUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_UpdateMailDetailMailBoolean_hotfix;
    private LuaFunction m_ShowAttachmentsUIMail_hotfix;
    private LuaFunction m_OnGotoButtonClick_hotfix;
    private LuaFunction m_OnGetAttachmentButtonClick_hotfix;
    private LuaFunction m_add_EventOnGetAttachmentButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetAttachmentButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnGotoButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGotoButtonClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public MailDetailUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateMailDetail(Mail mailInfo, bool haveMail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowAttachmentsUI(Mail mailInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetAttachmentButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<Mail> EventOnGetAttachmentButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Mail> EventOnGotoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public MailDetailUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetAttachmentButtonClick(Mail obj)
    {
    }

    private void __clearDele_EventOnGetAttachmentButtonClick(Mail obj)
    {
      this.EventOnGetAttachmentButtonClick = (Action<Mail>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGotoButtonClick(Mail obj)
    {
    }

    private void __clearDele_EventOnGotoButtonClick(Mail obj)
    {
      this.EventOnGotoButtonClick = (Action<Mail>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private MailDetailUIController m_owner;

      public LuaExportHelper(MailDetailUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnGetAttachmentButtonClick(Mail obj)
      {
        this.m_owner.__callDele_EventOnGetAttachmentButtonClick(obj);
      }

      public void __clearDele_EventOnGetAttachmentButtonClick(Mail obj)
      {
        this.m_owner.__clearDele_EventOnGetAttachmentButtonClick(obj);
      }

      public void __callDele_EventOnGotoButtonClick(Mail obj)
      {
        this.m_owner.__callDele_EventOnGotoButtonClick(obj);
      }

      public void __clearDele_EventOnGotoButtonClick(Mail obj)
      {
        this.m_owner.__clearDele_EventOnGotoButtonClick(obj);
      }

      public Mail m_currentMailInfo
      {
        get
        {
          return this.m_owner.m_currentMailInfo;
        }
        set
        {
          this.m_owner.m_currentMailInfo = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public List<RewardGoodsUIController> m_attachmentUICtrlList
      {
        get
        {
          return this.m_owner.m_attachmentUICtrlList;
        }
        set
        {
          this.m_owner.m_attachmentUICtrlList = value;
        }
      }

      public GameObject ItemUICtrlPrefab
      {
        get
        {
          return this.m_owner.ItemUICtrlPrefab;
        }
        set
        {
          this.m_owner.ItemUICtrlPrefab = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ShowAttachmentsUI(Mail mailInfo)
      {
        this.m_owner.ShowAttachmentsUI(mailInfo);
      }

      public void OnGotoButtonClick()
      {
        this.m_owner.OnGotoButtonClick();
      }

      public void OnGetAttachmentButtonClick()
      {
        this.m_owner.OnGetAttachmentButtonClick();
      }
    }
  }
}
