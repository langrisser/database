﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class CollectionActivityUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./OrganizeTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_teamButton;
    [AutoBind("./OrganizeTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teamUIStateController;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/ExchangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_exchangeButton;
    [AutoBind("./Margin/ExchangeButton/Redmark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_exchangeRedPointGameObject;
    [AutoBind("./Margin/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./Margin/ChatButton/Redmark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chatRedPointGameObject;
    [AutoBind("./NameGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityNameText;
    [AutoBind("./LevelListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_levelListUIStateController;
    [AutoBind("./LevelListPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_levelListBackgroundButton;
    [AutoBind("./LevelListPanel/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelListScrollRect;
    [AutoBind("./ItemList/ItemListGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currencyItemListGroup;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/LevelItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelItemPrefab;
    [AutoBind("./Prefabs/ResourceItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currencyItemPrefab;
    [DoNotToLua]
    private CollectionActivityUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_SetCollectionActivityConfigDataCollectionActivityInfo_hotfix;
    private LuaFunction m_SetCurrencyItemListConfigDataCollectionActivityInfo_hotfix;
    private LuaFunction m_SetCanTeamBattleBoolean_hotfix;
    private LuaFunction m_ShowLevelList_hotfix;
    private LuaFunction m_HideLevelList_hotfix;
    private LuaFunction m_ClearLevelList_hotfix;
    private LuaFunction m_AddLevelListItemConfigDataCollectionActivityScenarioLevelInfoBooleanInt32_hotfix;
    private LuaFunction m_AddLevelListItemConfigDataCollectionActivityChallengeLevelInfoBooleanInt32Int32_hotfix;
    private LuaFunction m_AddLevelListItemConfigDataCollectionActivityLootLevelInfoBooleanInt32BooleanInt32_hotfix;
    private LuaFunction m_SetChatUnreadCountInt32_hotfix;
    private LuaFunction m_SetExchangeRedMarkActiveBoolean_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnTeamButtonClick_hotfix;
    private LuaFunction m_OnExchangeButtonClick_hotfix;
    private LuaFunction m_OnChatButtonClick_hotfix;
    private LuaFunction m_OnLevelLickBackgroundButtonClick_hotfix;
    private LuaFunction m_CollectionActivityLevelListItem_OnStartButtonClickCollectionActivityLevelListItemUIController_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnShowTeamAction_hotfix;
    private LuaFunction m_remove_EventOnShowTeamAction_hotfix;
    private LuaFunction m_add_EventOnShowExchangeAction_hotfix;
    private LuaFunction m_remove_EventOnShowExchangeAction_hotfix;
    private LuaFunction m_add_EventOnShowChatAction_hotfix;
    private LuaFunction m_remove_EventOnShowChatAction_hotfix;
    private LuaFunction m_add_EventOnStartScenarioLevelAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartScenarioLevelAction`1_hotfix;
    private LuaFunction m_add_EventOnStartChallengeLevelAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartChallengeLevelAction`1_hotfix;
    private LuaFunction m_add_EventOnStartLootLevelAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartLootLevelAction`1_hotfix;

    private CollectionActivityUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCollectionActivity(
      ConfigDataCollectionActivityInfo collectionActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurrencyItemList(
      ConfigDataCollectionActivityInfo collectionActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanTeamBattle(bool canTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowLevelList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideLevelList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearLevelList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLevelListItem(
      ConfigDataCollectionActivityScenarioLevelInfo levelInfo,
      bool isFinished,
      int unlockDay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLevelListItem(
      ConfigDataCollectionActivityChallengeLevelInfo levelInfo,
      bool isFinished,
      int unlockDay,
      int playerLevelRequired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLevelListItem(
      ConfigDataCollectionActivityLootLevelInfo levelInfo,
      bool isFinished,
      int unlockDay,
      bool isPreLevelLocked,
      int playerLevelRequired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChatUnreadCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetExchangeRedMarkActive(bool flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTeamButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExchangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelLickBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityLevelListItem_OnStartButtonClick(
      CollectionActivityLevelListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowExchange
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataCollectionActivityScenarioLevelInfo> EventOnStartScenarioLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataCollectionActivityChallengeLevelInfo> EventOnStartChallengeLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataCollectionActivityLootLevelInfo> EventOnStartLootLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CollectionActivityUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowTeam()
    {
      this.EventOnShowTeam = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowExchange()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowExchange()
    {
      this.EventOnShowExchange = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowChat()
    {
      this.EventOnShowChat = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartScenarioLevel(
      ConfigDataCollectionActivityScenarioLevelInfo obj)
    {
    }

    private void __clearDele_EventOnStartScenarioLevel(
      ConfigDataCollectionActivityScenarioLevelInfo obj)
    {
      this.EventOnStartScenarioLevel = (Action<ConfigDataCollectionActivityScenarioLevelInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartChallengeLevel(
      ConfigDataCollectionActivityChallengeLevelInfo obj)
    {
    }

    private void __clearDele_EventOnStartChallengeLevel(
      ConfigDataCollectionActivityChallengeLevelInfo obj)
    {
      this.EventOnStartChallengeLevel = (Action<ConfigDataCollectionActivityChallengeLevelInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartLootLevel(ConfigDataCollectionActivityLootLevelInfo obj)
    {
    }

    private void __clearDele_EventOnStartLootLevel(ConfigDataCollectionActivityLootLevelInfo obj)
    {
      this.EventOnStartLootLevel = (Action<ConfigDataCollectionActivityLootLevelInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityUIController m_owner;

      public LuaExportHelper(CollectionActivityUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnShowTeam()
      {
        this.m_owner.__callDele_EventOnShowTeam();
      }

      public void __clearDele_EventOnShowTeam()
      {
        this.m_owner.__clearDele_EventOnShowTeam();
      }

      public void __callDele_EventOnShowExchange()
      {
        this.m_owner.__callDele_EventOnShowExchange();
      }

      public void __clearDele_EventOnShowExchange()
      {
        this.m_owner.__clearDele_EventOnShowExchange();
      }

      public void __callDele_EventOnShowChat()
      {
        this.m_owner.__callDele_EventOnShowChat();
      }

      public void __clearDele_EventOnShowChat()
      {
        this.m_owner.__clearDele_EventOnShowChat();
      }

      public void __callDele_EventOnStartScenarioLevel(
        ConfigDataCollectionActivityScenarioLevelInfo obj)
      {
        this.m_owner.__callDele_EventOnStartScenarioLevel(obj);
      }

      public void __clearDele_EventOnStartScenarioLevel(
        ConfigDataCollectionActivityScenarioLevelInfo obj)
      {
        this.m_owner.__clearDele_EventOnStartScenarioLevel(obj);
      }

      public void __callDele_EventOnStartChallengeLevel(
        ConfigDataCollectionActivityChallengeLevelInfo obj)
      {
        this.m_owner.__callDele_EventOnStartChallengeLevel(obj);
      }

      public void __clearDele_EventOnStartChallengeLevel(
        ConfigDataCollectionActivityChallengeLevelInfo obj)
      {
        this.m_owner.__clearDele_EventOnStartChallengeLevel(obj);
      }

      public void __callDele_EventOnStartLootLevel(ConfigDataCollectionActivityLootLevelInfo obj)
      {
        this.m_owner.__callDele_EventOnStartLootLevel(obj);
      }

      public void __clearDele_EventOnStartLootLevel(ConfigDataCollectionActivityLootLevelInfo obj)
      {
        this.m_owner.__clearDele_EventOnStartLootLevel(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Button m_teamButton
      {
        get
        {
          return this.m_owner.m_teamButton;
        }
        set
        {
          this.m_owner.m_teamButton = value;
        }
      }

      public CommonUIStateController m_teamUIStateController
      {
        get
        {
          return this.m_owner.m_teamUIStateController;
        }
        set
        {
          this.m_owner.m_teamUIStateController = value;
        }
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Button m_exchangeButton
      {
        get
        {
          return this.m_owner.m_exchangeButton;
        }
        set
        {
          this.m_owner.m_exchangeButton = value;
        }
      }

      public GameObject m_exchangeRedPointGameObject
      {
        get
        {
          return this.m_owner.m_exchangeRedPointGameObject;
        }
        set
        {
          this.m_owner.m_exchangeRedPointGameObject = value;
        }
      }

      public Button m_chatButton
      {
        get
        {
          return this.m_owner.m_chatButton;
        }
        set
        {
          this.m_owner.m_chatButton = value;
        }
      }

      public GameObject m_chatRedPointGameObject
      {
        get
        {
          return this.m_owner.m_chatRedPointGameObject;
        }
        set
        {
          this.m_owner.m_chatRedPointGameObject = value;
        }
      }

      public Text m_activityNameText
      {
        get
        {
          return this.m_owner.m_activityNameText;
        }
        set
        {
          this.m_owner.m_activityNameText = value;
        }
      }

      public CommonUIStateController m_levelListUIStateController
      {
        get
        {
          return this.m_owner.m_levelListUIStateController;
        }
        set
        {
          this.m_owner.m_levelListUIStateController = value;
        }
      }

      public Button m_levelListBackgroundButton
      {
        get
        {
          return this.m_owner.m_levelListBackgroundButton;
        }
        set
        {
          this.m_owner.m_levelListBackgroundButton = value;
        }
      }

      public ScrollRect m_levelListScrollRect
      {
        get
        {
          return this.m_owner.m_levelListScrollRect;
        }
        set
        {
          this.m_owner.m_levelListScrollRect = value;
        }
      }

      public GameObject m_currencyItemListGroup
      {
        get
        {
          return this.m_owner.m_currencyItemListGroup;
        }
        set
        {
          this.m_owner.m_currencyItemListGroup = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_levelItemPrefab
      {
        get
        {
          return this.m_owner.m_levelItemPrefab;
        }
        set
        {
          this.m_owner.m_levelItemPrefab = value;
        }
      }

      public GameObject m_currencyItemPrefab
      {
        get
        {
          return this.m_owner.m_currencyItemPrefab;
        }
        set
        {
          this.m_owner.m_currencyItemPrefab = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetCurrencyItemList(
        ConfigDataCollectionActivityInfo collectionActivityInfo)
      {
        this.m_owner.SetCurrencyItemList(collectionActivityInfo);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnTeamButtonClick()
      {
        this.m_owner.OnTeamButtonClick();
      }

      public void OnExchangeButtonClick()
      {
        this.m_owner.OnExchangeButtonClick();
      }

      public void OnChatButtonClick()
      {
        this.m_owner.OnChatButtonClick();
      }

      public void OnLevelLickBackgroundButtonClick()
      {
        this.m_owner.OnLevelLickBackgroundButtonClick();
      }

      public void CollectionActivityLevelListItem_OnStartButtonClick(
        CollectionActivityLevelListItemUIController ctrl)
      {
        this.m_owner.CollectionActivityLevelListItem_OnStartButtonClick(ctrl);
      }
    }
  }
}
