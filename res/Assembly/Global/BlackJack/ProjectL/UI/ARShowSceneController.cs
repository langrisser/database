﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ARShowSceneController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.AR;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using Spine.Unity;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ARShowSceneController : UIControllerBase
  {
    [AutoBind("./ARCamera", AutoBindAttribute.InitState.NotInit, false)]
    private Camera m_camera;
    [AutoBind("./FocusSquare/FocusSquare", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_focusSquare;
    [AutoBind("./FocusSquare/FocusSquare/Quad", AutoBindAttribute.InitState.NotInit, false)]
    private Renderer m_focusSquareRenderer;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charNode;
    [AutoBind("./CharDraw", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charDrawNode;
    [AutoBind("./CharGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGroupNode;
    private ARPlaneTrace arPlaneTrace;
    private SkeletonAnimation m_roleSkeleton;
    private List<SkeletonAnimation> m_roleSkeletonList;
    private ARUITask m_task;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private string[] m_roleAnimationList;
    private bool isPlaneTrace;

    [MethodImpl((MethodImplOptions) 32768)]
    public ARShowSceneController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetUITask(ARUITask task)
    {
      this.m_task = task;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SummonHeroBattleShow(int selectHeroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SummonHeroDrawShow(int selectHeroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SummonHeroTeamShow()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCharClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    public void PlaySingleCharAnimation(string animationName, bool isLoop)
    {
      this.PlayAnimation(this.m_roleSkeleton, animationName, isLoop);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(
      SkeletonAnimation skeletonAnimation,
      string animationName,
      bool isLoop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(
      SkeletonAnimation skeletonAnimation,
      HeroActionType actionType,
      bool isLoop)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetCharDirection(bool isLookRight)
    {
      this.SetCharDirection(this.m_charNode, isLookRight);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCharDirection(GameObject charObj, bool isLookRight)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCharScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamDistance(float distance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyChar()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFindPlane()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaneTraceEnable(bool isActive)
    {
    }
  }
}
