﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ClimbTowerRaidUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ClimbTowerRaidUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Panel/EngryAndMission/EngryValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_costEnergyText;
    [AutoBind("./Panel/EngryAndMission/MissionValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_floorNameText;
    [AutoBind("./Panel/TeamEXP/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./Panel/TeamEXP/ExpValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerExpText;
    [AutoBind("./Panel/TeamEXP/ExpProgress/ExpProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerExpImage;
    [AutoBind("./Panel/RewardScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_rewardScollRect;
    private bool m_isClick;
    private PlayerLevelUpUITask m_playerLevelUpUITask;

    private ClimbTowerRaidUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void ShowRaidResult(BattleReward reward)
    {
      this.StartCoroutine(this.Co_ShowRaidResult(reward));
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowRaidResult(BattleReward reward)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowPlayerLevelUp(int oldLevel, int newLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnBackgroundButtonClick()
    {
      this.m_isClick = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelUpUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
