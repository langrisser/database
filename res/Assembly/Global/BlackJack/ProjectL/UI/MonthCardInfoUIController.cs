﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MonthCardInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class MonthCardInfoUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController PanelStateController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button BackgroundButton;
    [AutoBind("./LayoutRoot/ItemDesc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text NameText;
    [AutoBind("./LayoutRoot/ItemDesc/Item/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image IconImage;
    [AutoBind("./LayoutRoot/ItemDesc/LeftDay", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController LeftDayStateCtrl;
    [AutoBind("./LayoutRoot/ItemDesc/LeftDay/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text LeftDays;
    [AutoBind("./LayoutRoot/ListPanel/TitleImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController MonthCardOrSubscribeStateCtrl;
    [AutoBind("./LayoutRoot/ListPanel/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Text DescText;
    private int m_monthCardId;
    private float m_leftTime;
    private const float m_updateInterval = 1f;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowReward(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
