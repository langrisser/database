﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomInviteeInfoGetNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TeamRoomInviteeInfoGetNetTask : UINetTask
  {
    private List<string> m_inviteeUserIds;
    private TeamRoomInviteeInfoType m_infoType;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomInviteeInfoGetNetTask(
      List<string> inviteeUserIds,
      TeamRoomInviteeInfoType infoType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnTeamRoomInviteeLevelInfoGetAck(int result)
    {
      this.Result = result;
      this.OnTransactionComplete();
    }
  }
}
