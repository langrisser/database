﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerResourceUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PlayerResourceUIController : UIControllerBase
  {
    private Text m_goldText;
    private Button m_addGoldButton;
    private Text m_crystalText;
    private Button m_addCrystalButton;
    private Text m_energyText;
    private Button m_addEnergyButton;
    private Button m_energyStatusButton;
    private BuyEnergyUITask m_buyEnergyUITask;
    [DoNotToLua]
    private PlayerResourceUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnEnable_hotfix;
    private LuaFunction m_OnDisable_hotfix;
    private LuaFunction m_OnDestroy_hotfix;
    private LuaFunction m_UpdatePlayerResource_hotfix;
    private LuaFunction m_SetGoldCountInt32_hotfix;
    private LuaFunction m_SetCrystalCountInt32_hotfix;
    private LuaFunction m_SetEnergyInt32Int32_hotfix;
    private LuaFunction m_OnAddGoldButtonClick_hotfix;
    private LuaFunction m_OnAddCrystalButtonClick_hotfix;
    private LuaFunction m_OnAddEnergyButtonClick_hotfix;
    private LuaFunction m_OnEnergyStatusButtonClick_hotfix;
    private LuaFunction m_PlayerContext_OnPlayerInfoChangeNtf_hotfix;
    private LuaFunction m_add_EventOnAddGoldAction_hotfix;
    private LuaFunction m_remove_EventOnAddGoldAction_hotfix;
    private LuaFunction m_add_EventOnAddCrystalAction_hotfix;
    private LuaFunction m_remove_EventOnAddCrystalAction_hotfix;

    private PlayerResourceUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnDestroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePlayerResource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGoldCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCrystalCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEnergy(int count, int max)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddGoldButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddCrystalButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddEnergyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnergyStatusButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoChangeNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnAddGold
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddCrystal
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PlayerResourceUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      base.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddGold()
    {
      this.EventOnAddGold = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddCrystal()
    {
      this.EventOnAddCrystal = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PlayerResourceUIController m_owner;

      public LuaExportHelper(PlayerResourceUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnAddGold()
      {
        this.m_owner.__callDele_EventOnAddGold();
      }

      public void __clearDele_EventOnAddGold()
      {
        this.m_owner.__clearDele_EventOnAddGold();
      }

      public void __callDele_EventOnAddCrystal()
      {
        this.m_owner.__callDele_EventOnAddCrystal();
      }

      public void __clearDele_EventOnAddCrystal()
      {
        this.m_owner.__clearDele_EventOnAddCrystal();
      }

      public Text m_goldText
      {
        get
        {
          return this.m_owner.m_goldText;
        }
        set
        {
          this.m_owner.m_goldText = value;
        }
      }

      public Button m_addGoldButton
      {
        get
        {
          return this.m_owner.m_addGoldButton;
        }
        set
        {
          this.m_owner.m_addGoldButton = value;
        }
      }

      public Text m_crystalText
      {
        get
        {
          return this.m_owner.m_crystalText;
        }
        set
        {
          this.m_owner.m_crystalText = value;
        }
      }

      public Button m_addCrystalButton
      {
        get
        {
          return this.m_owner.m_addCrystalButton;
        }
        set
        {
          this.m_owner.m_addCrystalButton = value;
        }
      }

      public Text m_energyText
      {
        get
        {
          return this.m_owner.m_energyText;
        }
        set
        {
          this.m_owner.m_energyText = value;
        }
      }

      public Button m_addEnergyButton
      {
        get
        {
          return this.m_owner.m_addEnergyButton;
        }
        set
        {
          this.m_owner.m_addEnergyButton = value;
        }
      }

      public Button m_energyStatusButton
      {
        get
        {
          return this.m_owner.m_energyStatusButton;
        }
        set
        {
          this.m_owner.m_energyStatusButton = value;
        }
      }

      public BuyEnergyUITask m_buyEnergyUITask
      {
        get
        {
          return this.m_owner.m_buyEnergyUITask;
        }
        set
        {
          this.m_owner.m_buyEnergyUITask = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnEnable()
      {
        this.m_owner.OnEnable();
      }

      public void OnDisable()
      {
        this.m_owner.OnDisable();
      }

      public void OnDestroy()
      {
        this.m_owner.OnDestroy();
      }

      public void SetGoldCount(int count)
      {
        this.m_owner.SetGoldCount(count);
      }

      public void SetCrystalCount(int count)
      {
        this.m_owner.SetCrystalCount(count);
      }

      public void SetEnergy(int count, int max)
      {
        this.m_owner.SetEnergy(count, max);
      }

      public void OnAddGoldButtonClick()
      {
        this.m_owner.OnAddGoldButtonClick();
      }

      public void OnAddCrystalButtonClick()
      {
        this.m_owner.OnAddCrystalButtonClick();
      }

      public void OnAddEnergyButtonClick()
      {
        this.m_owner.OnAddEnergyButtonClick();
      }

      public void OnEnergyStatusButtonClick()
      {
        this.m_owner.OnEnergyStatusButtonClick();
      }

      public void PlayerContext_OnPlayerInfoChangeNtf()
      {
        this.m_owner.PlayerContext_OnPlayerInfoChangeNtf();
      }
    }
  }
}
