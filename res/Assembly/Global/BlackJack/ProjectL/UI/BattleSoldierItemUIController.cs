﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleSoldierItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleSoldierItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./SoldierIconImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierIconImg;
    [AutoBind("./Faction/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierRangeText;
    [AutoBind("./Faction/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMoveText;
    [AutoBind("./Faction/TypeBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_typeBgImage;
    [AutoBind("./Faction/TypeBgImage2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_typeBgImage2;
    [AutoBind("./TitleBg/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./SelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectButton;
    [AutoBind("./SelectTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectPanel;
    [AutoBind("./Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGrapgic;
    private UISpineGraphic m_graphic;
    private ConfigDataModelSkinResourceInfo m_solderSkinResInfo;
    [DoNotToLua]
    private BattleSoldierItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitSoldierItemConfigDataSoldierInfoConfigDataModelSkinResourceInfo_hotfix;
    private LuaFunction m_OnSelectButtonClick_hotfix;
    private LuaFunction m_SetSelectPanelBoolean_hotfix;
    private LuaFunction m_OnClick_hotfix;
    private LuaFunction m_CreateSpineGraphic_hotfix;
    private LuaFunction m_DestroySpineGraphic_hotfix;
    private LuaFunction m_OnDestroy_hotfix;
    private LuaFunction m_add_EventOnSoldierItemClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnSoldierItemClickAction`1_hotfix;
    private LuaFunction m_add_EventOnSelectButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnSelectButtonClickAction`1_hotfix;
    private LuaFunction m_set_SoldierInfoConfigDataSoldierInfo_hotfix;
    private LuaFunction m_get_SoldierInfo_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierItem(
      ConfigDataSoldierInfo soldierInfo,
      ConfigDataModelSkinResourceInfo soldierSkinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelectPanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnDestroy()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleSoldierItemUIController> EventOnSoldierItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleSoldierItemUIController> EventOnSelectButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSoldierInfo SoldierInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BattleSoldierItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      base.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSoldierItemClick(BattleSoldierItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSoldierItemClick(BattleSoldierItemUIController obj)
    {
      this.EventOnSoldierItemClick = (Action<BattleSoldierItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSelectButtonClick(BattleSoldierItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSelectButtonClick(BattleSoldierItemUIController obj)
    {
      this.EventOnSelectButtonClick = (Action<BattleSoldierItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleSoldierItemUIController m_owner;

      public LuaExportHelper(BattleSoldierItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnSoldierItemClick(BattleSoldierItemUIController obj)
      {
        this.m_owner.__callDele_EventOnSoldierItemClick(obj);
      }

      public void __clearDele_EventOnSoldierItemClick(BattleSoldierItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnSoldierItemClick(obj);
      }

      public void __callDele_EventOnSelectButtonClick(BattleSoldierItemUIController obj)
      {
        this.m_owner.__callDele_EventOnSelectButtonClick(obj);
      }

      public void __clearDele_EventOnSelectButtonClick(BattleSoldierItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnSelectButtonClick(obj);
      }

      public Button m_button
      {
        get
        {
          return this.m_owner.m_button;
        }
        set
        {
          this.m_owner.m_button = value;
        }
      }

      public Image m_soldierIconImg
      {
        get
        {
          return this.m_owner.m_soldierIconImg;
        }
        set
        {
          this.m_owner.m_soldierIconImg = value;
        }
      }

      public Text m_soldierRangeText
      {
        get
        {
          return this.m_owner.m_soldierRangeText;
        }
        set
        {
          this.m_owner.m_soldierRangeText = value;
        }
      }

      public Text m_soldierMoveText
      {
        get
        {
          return this.m_owner.m_soldierMoveText;
        }
        set
        {
          this.m_owner.m_soldierMoveText = value;
        }
      }

      public Image m_typeBgImage
      {
        get
        {
          return this.m_owner.m_typeBgImage;
        }
        set
        {
          this.m_owner.m_typeBgImage = value;
        }
      }

      public Image m_typeBgImage2
      {
        get
        {
          return this.m_owner.m_typeBgImage2;
        }
        set
        {
          this.m_owner.m_typeBgImage2 = value;
        }
      }

      public Text m_soldierNameText
      {
        get
        {
          return this.m_owner.m_soldierNameText;
        }
        set
        {
          this.m_owner.m_soldierNameText = value;
        }
      }

      public Button m_selectButton
      {
        get
        {
          return this.m_owner.m_selectButton;
        }
        set
        {
          this.m_owner.m_selectButton = value;
        }
      }

      public GameObject m_selectPanel
      {
        get
        {
          return this.m_owner.m_selectPanel;
        }
        set
        {
          this.m_owner.m_selectPanel = value;
        }
      }

      public GameObject m_soldierGrapgic
      {
        get
        {
          return this.m_owner.m_soldierGrapgic;
        }
        set
        {
          this.m_owner.m_soldierGrapgic = value;
        }
      }

      public UISpineGraphic m_graphic
      {
        get
        {
          return this.m_owner.m_graphic;
        }
        set
        {
          this.m_owner.m_graphic = value;
        }
      }

      public ConfigDataModelSkinResourceInfo m_solderSkinResInfo
      {
        get
        {
          return this.m_owner.m_solderSkinResInfo;
        }
        set
        {
          this.m_owner.m_solderSkinResInfo = value;
        }
      }

      public ConfigDataSoldierInfo SoldierInfo
      {
        set
        {
          this.m_owner.SoldierInfo = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnSelectButtonClick()
      {
        this.m_owner.OnSelectButtonClick();
      }

      public void OnClick()
      {
        this.m_owner.OnClick();
      }

      public void CreateSpineGraphic()
      {
        this.m_owner.CreateSpineGraphic();
      }

      public void DestroySpineGraphic()
      {
        this.m_owner.DestroySpineGraphic();
      }

      public void OnDestroy()
      {
        this.m_owner.OnDestroy();
      }
    }
  }
}
