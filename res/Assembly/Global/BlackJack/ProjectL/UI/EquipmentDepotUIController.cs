﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentDepotUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using MarchingBytes;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class EquipmentDepotUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./PlayerResource/Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldText;
    [AutoBind("./PlayerResource/Gold/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goldAddButton;
    [AutoBind("./EquipmentList/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_loopScrollView;
    [AutoBind("./EquipmentList/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_listItemPool;
    [AutoBind("./EquipmentList/ListScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listScrollViewItemRoot;
    [AutoBind("./EquipmentList/Filter/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterSortButton;
    [AutoBind("./EquipmentList/Filter/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_filterSortTypeText;
    [AutoBind("./EquipmentList/Filter/SortOrderButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterSortOrderButton;
    [AutoBind("./EquipmentList/Filter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_filterSortTypesGo;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_filterSortTypesGridLayout;
    [AutoBind("./EquipmentList/NoItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listNotItemGo;
    [AutoBind("./Desc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descTitleText;
    [AutoBind("./Desc/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIcon;
    [AutoBind("./Desc/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIconBg;
    [AutoBind("./Desc/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSSREffect;
    [AutoBind("./Desc/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descStarGroup;
    [AutoBind("./Desc/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descLvText;
    [AutoBind("./Desc/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descExpText;
    [AutoBind("./Desc/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descProgressImage;
    [AutoBind("./Desc/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descEquipLimitContent;
    [AutoBind("./Desc/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./Desc/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSkillContent;
    [AutoBind("./Desc/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descSkillContentStateCtrl;
    [AutoBind("./Desc/SkillContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillNameText;
    [AutoBind("./Desc/SkillContent/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillLvText;
    [AutoBind("./Desc/SkillContent/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descUnlockCoditionText;
    [AutoBind("./Desc/SkillContent/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillOwnerText;
    [AutoBind("./Desc/SkillContent/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSkillOwnerBGImage;
    [AutoBind("./Desc/SkillContent/DescScrollView/Mask/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillDescText;
    [AutoBind("./Desc/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descNotEquipSkillTip;
    [AutoBind("./Desc/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descLockButton;
    [AutoBind("./Desc/EquipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descEquipButton;
    [AutoBind("./Desc/UnloadButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descUnloadButton;
    [AutoBind("./Desc/PropGroup/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropContent;
    [AutoBind("./Desc/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropATGo;
    [AutoBind("./Desc/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropATValueText;
    [AutoBind("./Desc/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropDFGo;
    [AutoBind("./Desc/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropDFValueText;
    [AutoBind("./Desc/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropHPGo;
    [AutoBind("./Desc/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropHPValueText;
    [AutoBind("./Desc/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropMagiccGo;
    [AutoBind("./Desc/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropMagicValueText;
    [AutoBind("./Desc/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropMagicDFGo;
    [AutoBind("./Desc/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropMagicDFValueText;
    [AutoBind("./Desc/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropDexGo;
    [AutoBind("./Desc/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropDexValueText;
    [AutoBind("./Desc/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropGroupStateCtrl;
    [AutoBind("./Desc/PropGroup/EnchantmentGroup/PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropEnchantmentGroup;
    [AutoBind("./Desc/PropGroup/EnchantmentGroup/Resonance/RuneIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descPropEnchantmentGroupRuneIconImage;
    [AutoBind("./Desc/PropGroup/EnchantmentGroup/Resonance/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropEnchantmentGroupRuneNameText;
    [AutoBind("./Desc/PropGroup/EnchantmentGroup/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl;
    [AutoBind("./Desc/PropertyComparison", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_PropertyComparisonGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescPropGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Hp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescHpGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Hp/FirstNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescHpOldText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Hp/LastValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescHpNewText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescATGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/AT/FirstNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescATOldText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/AT/LastValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescATNewText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescDFGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/DF/FirstNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescDFOldText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/DF/LastValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescDFNewText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescMagicDFGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/MagicDF/FirstNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescMagicDFOldText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/MagicDF/LastValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescMagicDFNewText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescMagicGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Magic/FirstNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescMagicOldText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Magic/LastValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescMagicNewText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescDexGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Dex/FirstNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescDexOldText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Dex/LastValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescDexNewText;
    [AutoBind("./BattlePower", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battlePowerGo;
    [AutoBind("./BattlePower/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battlePowerValueText;
    [AutoBind("./ConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confirmPanelStateCtrl;
    [AutoBind("./ConfirmPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmPanelCancelButton;
    [AutoBind("./ConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmPanelConfirmButton;
    private int m_isAscend;
    private EquipmentDepotUIController.EquipmentSortTypeState m_curEquipmentSortType;
    private Hero m_hero;
    private int m_slot;
    private int m_canUseEquipmentCount;
    private ulong m_curEquipmentInstanceId;
    private bool m_isFirstIn;
    private List<EquipmentBagItem> m_equipItemCache;
    private List<EquipmentDepotListItemUIController> m_equipmentDepotCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private EquipmentDepotUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitSortTypePanel_hotfix;
    private LuaFunction m_InitLoopScrollRect_hotfix;
    private LuaFunction m_OnPoolObjectCreatedStringGameObject_hotfix;
    private LuaFunction m_UpdateEquipmentDepotViewHeroInt32UInt64_hotfix;
    private LuaFunction m_CreateEquipmentList_hotfix;
    private LuaFunction m_SortEquipmentListByTypeList`1_hotfix;
    private LuaFunction m_DefaultEquipmentItemComparerEquipmentBagItemEquipmentBagItem_hotfix;
    private LuaFunction m_CollectEquipmentPropValueAndSortList`1PropertyModifyType_hotfix;
    private LuaFunction m_OnEquipmentListItemClickUIControllerBase_hotfix;
    private LuaFunction m_OnEquipmentListItemNeedFillUIControllerBase_hotfix;
    private LuaFunction m_OnFilterSortButtonClick_hotfix;
    private LuaFunction m_OnCloseFilterSortTypeGo_hotfix;
    private LuaFunction m_OnFilterSortOrderButtonClick_hotfix;
    private LuaFunction m_OnFilterTypeButtonClickEquipmentSortItemUIControllerBoolean_hotfix;
    private LuaFunction m_SetEquipmentItemDescEquipmentBagItem_hotfix;
    private LuaFunction m_SetEquipmentComparisonEquipmentBagItem_hotfix;
    private LuaFunction m_ResetEquipmentComparisonPropValue_hotfix;
    private LuaFunction m_SetEquipmentComparisonPropPropertyModifyTypeInt32Boolean_hotfix;
    private LuaFunction m_SetPropItemColorTextText_hotfix;
    private LuaFunction m_SetPropItemsPropertyModifyTypeInt32Int32Int32_hotfix;
    private LuaFunction m_OnDescEquipButtonClick_hotfix;
    private LuaFunction m_OnConfirmPanelConfirmButtonClick_hotfix;
    private LuaFunction m_OnConfirmPanelCancelButtonClick_hotfix;
    private LuaFunction m_OnDescUnloadButtonClick_hotfix;
    private LuaFunction m_OnDescLockButtonClick_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnGoldAddButtonClick_hotfix;
    private LuaFunction m_ResetScrollViewToTop_hotfix;
    private LuaFunction m_GetFirstEquipmentGoInListForUserGuide_hotfix;
    private LuaFunction m_ClickEquipmentListItemForUserGuideUIControllerBase_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnGoldAddButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnGoldAddButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnTakeOffEquipmentAction`3_hotfix;
    private LuaFunction m_remove_EventOnTakeOffEquipmentAction`3_hotfix;
    private LuaFunction m_add_EventOnLockButtonClickAction`2_hotfix;
    private LuaFunction m_remove_EventOnLockButtonClickAction`2_hotfix;
    private LuaFunction m_add_EventOnEquipButtonClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnEquipButtonClickAction`3_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentDepotUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitSortTypePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoopScrollRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateEquipmentDepotView(Hero hero, int slot, ulong equipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateEquipmentList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortEquipmentListByType(List<EquipmentBagItem> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int DefaultEquipmentItemComparer(EquipmentBagItem e1, EquipmentBagItem e2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEquipmentPropValueAndSort(
      List<EquipmentBagItem> list,
      PropertyModifyType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentListItemClick(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentListItemNeedFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterSortButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseFilterSortTypeGo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterSortOrderButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterTypeButtonClick(EquipmentSortItemUIController ctrl, bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentItemDesc(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentComparison(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetEquipmentComparisonPropValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentComparisonProp(PropertyModifyType type, int value, bool isDressed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItemColor(Text oldText, Text newText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescEquipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmPanelCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescUnloadButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoldAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject GetFirstEquipmentGoInListForUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClickEquipmentListItemForUserGuide(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGoldAddButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action> EventOnTakeOffEquipment
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, int> EventOnLockButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ulong, int> EventOnEquipButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public EquipmentDepotUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGoldAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGoldAddButtonClick()
    {
      this.EventOnGoldAddButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTakeOffEquipment(int arg1, int arg2, Action arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnTakeOffEquipment(int arg1, int arg2, Action arg3)
    {
      this.EventOnTakeOffEquipment = (Action<int, int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnLockButtonClick(ulong arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnLockButtonClick(ulong arg1, int arg2)
    {
      this.EventOnLockButtonClick = (Action<ulong, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEquipButtonClick(int arg1, ulong arg2, int arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEquipButtonClick(int arg1, ulong arg2, int arg3)
    {
      this.EventOnEquipButtonClick = (Action<int, ulong, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum EquipmentSortTypeState
    {
      Lv,
      StarLv,
      Rank,
      Hp,
      AT,
      DF,
      MagicAT,
      MagicDF,
      Dex,
    }

    public class LuaExportHelper
    {
      private EquipmentDepotUIController m_owner;

      public LuaExportHelper(EquipmentDepotUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnGoldAddButtonClick()
      {
        this.m_owner.__callDele_EventOnGoldAddButtonClick();
      }

      public void __clearDele_EventOnGoldAddButtonClick()
      {
        this.m_owner.__clearDele_EventOnGoldAddButtonClick();
      }

      public void __callDele_EventOnTakeOffEquipment(int arg1, int arg2, Action arg3)
      {
        this.m_owner.__callDele_EventOnTakeOffEquipment(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnTakeOffEquipment(int arg1, int arg2, Action arg3)
      {
        this.m_owner.__clearDele_EventOnTakeOffEquipment(arg1, arg2, arg3);
      }

      public void __callDele_EventOnLockButtonClick(ulong arg1, int arg2)
      {
        this.m_owner.__callDele_EventOnLockButtonClick(arg1, arg2);
      }

      public void __clearDele_EventOnLockButtonClick(ulong arg1, int arg2)
      {
        this.m_owner.__clearDele_EventOnLockButtonClick(arg1, arg2);
      }

      public void __callDele_EventOnEquipButtonClick(int arg1, ulong arg2, int arg3)
      {
        this.m_owner.__callDele_EventOnEquipButtonClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnEquipButtonClick(int arg1, ulong arg2, int arg3)
      {
        this.m_owner.__clearDele_EventOnEquipButtonClick(arg1, arg2, arg3);
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Text m_goldText
      {
        get
        {
          return this.m_owner.m_goldText;
        }
        set
        {
          this.m_owner.m_goldText = value;
        }
      }

      public Button m_goldAddButton
      {
        get
        {
          return this.m_owner.m_goldAddButton;
        }
        set
        {
          this.m_owner.m_goldAddButton = value;
        }
      }

      public LoopVerticalScrollRect m_loopScrollView
      {
        get
        {
          return this.m_owner.m_loopScrollView;
        }
        set
        {
          this.m_owner.m_loopScrollView = value;
        }
      }

      public EasyObjectPool m_listItemPool
      {
        get
        {
          return this.m_owner.m_listItemPool;
        }
        set
        {
          this.m_owner.m_listItemPool = value;
        }
      }

      public GameObject m_listScrollViewItemRoot
      {
        get
        {
          return this.m_owner.m_listScrollViewItemRoot;
        }
        set
        {
          this.m_owner.m_listScrollViewItemRoot = value;
        }
      }

      public Button m_filterSortButton
      {
        get
        {
          return this.m_owner.m_filterSortButton;
        }
        set
        {
          this.m_owner.m_filterSortButton = value;
        }
      }

      public Text m_filterSortTypeText
      {
        get
        {
          return this.m_owner.m_filterSortTypeText;
        }
        set
        {
          this.m_owner.m_filterSortTypeText = value;
        }
      }

      public Button m_filterSortOrderButton
      {
        get
        {
          return this.m_owner.m_filterSortOrderButton;
        }
        set
        {
          this.m_owner.m_filterSortOrderButton = value;
        }
      }

      public GameObject m_filterSortTypesGo
      {
        get
        {
          return this.m_owner.m_filterSortTypesGo;
        }
        set
        {
          this.m_owner.m_filterSortTypesGo = value;
        }
      }

      public GameObject m_filterSortTypesGridLayout
      {
        get
        {
          return this.m_owner.m_filterSortTypesGridLayout;
        }
        set
        {
          this.m_owner.m_filterSortTypesGridLayout = value;
        }
      }

      public GameObject m_listNotItemGo
      {
        get
        {
          return this.m_owner.m_listNotItemGo;
        }
        set
        {
          this.m_owner.m_listNotItemGo = value;
        }
      }

      public Text m_descTitleText
      {
        get
        {
          return this.m_owner.m_descTitleText;
        }
        set
        {
          this.m_owner.m_descTitleText = value;
        }
      }

      public Image m_descIcon
      {
        get
        {
          return this.m_owner.m_descIcon;
        }
        set
        {
          this.m_owner.m_descIcon = value;
        }
      }

      public Image m_descIconBg
      {
        get
        {
          return this.m_owner.m_descIconBg;
        }
        set
        {
          this.m_owner.m_descIconBg = value;
        }
      }

      public GameObject m_descSSREffect
      {
        get
        {
          return this.m_owner.m_descSSREffect;
        }
        set
        {
          this.m_owner.m_descSSREffect = value;
        }
      }

      public GameObject m_descStarGroup
      {
        get
        {
          return this.m_owner.m_descStarGroup;
        }
        set
        {
          this.m_owner.m_descStarGroup = value;
        }
      }

      public Text m_descLvText
      {
        get
        {
          return this.m_owner.m_descLvText;
        }
        set
        {
          this.m_owner.m_descLvText = value;
        }
      }

      public Text m_descExpText
      {
        get
        {
          return this.m_owner.m_descExpText;
        }
        set
        {
          this.m_owner.m_descExpText = value;
        }
      }

      public Image m_descProgressImage
      {
        get
        {
          return this.m_owner.m_descProgressImage;
        }
        set
        {
          this.m_owner.m_descProgressImage = value;
        }
      }

      public GameObject m_descEquipLimitContent
      {
        get
        {
          return this.m_owner.m_descEquipLimitContent;
        }
        set
        {
          this.m_owner.m_descEquipLimitContent = value;
        }
      }

      public Text m_descEquipUnlimitText
      {
        get
        {
          return this.m_owner.m_descEquipUnlimitText;
        }
        set
        {
          this.m_owner.m_descEquipUnlimitText = value;
        }
      }

      public GameObject m_descSkillContent
      {
        get
        {
          return this.m_owner.m_descSkillContent;
        }
        set
        {
          this.m_owner.m_descSkillContent = value;
        }
      }

      public CommonUIStateController m_descSkillContentStateCtrl
      {
        get
        {
          return this.m_owner.m_descSkillContentStateCtrl;
        }
        set
        {
          this.m_owner.m_descSkillContentStateCtrl = value;
        }
      }

      public Text m_descSkillNameText
      {
        get
        {
          return this.m_owner.m_descSkillNameText;
        }
        set
        {
          this.m_owner.m_descSkillNameText = value;
        }
      }

      public Text m_descSkillLvText
      {
        get
        {
          return this.m_owner.m_descSkillLvText;
        }
        set
        {
          this.m_owner.m_descSkillLvText = value;
        }
      }

      public Text m_descUnlockCoditionText
      {
        get
        {
          return this.m_owner.m_descUnlockCoditionText;
        }
        set
        {
          this.m_owner.m_descUnlockCoditionText = value;
        }
      }

      public Text m_descSkillOwnerText
      {
        get
        {
          return this.m_owner.m_descSkillOwnerText;
        }
        set
        {
          this.m_owner.m_descSkillOwnerText = value;
        }
      }

      public GameObject m_descSkillOwnerBGImage
      {
        get
        {
          return this.m_owner.m_descSkillOwnerBGImage;
        }
        set
        {
          this.m_owner.m_descSkillOwnerBGImage = value;
        }
      }

      public Text m_descSkillDescText
      {
        get
        {
          return this.m_owner.m_descSkillDescText;
        }
        set
        {
          this.m_owner.m_descSkillDescText = value;
        }
      }

      public GameObject m_descNotEquipSkillTip
      {
        get
        {
          return this.m_owner.m_descNotEquipSkillTip;
        }
        set
        {
          this.m_owner.m_descNotEquipSkillTip = value;
        }
      }

      public Button m_descLockButton
      {
        get
        {
          return this.m_owner.m_descLockButton;
        }
        set
        {
          this.m_owner.m_descLockButton = value;
        }
      }

      public Button m_descEquipButton
      {
        get
        {
          return this.m_owner.m_descEquipButton;
        }
        set
        {
          this.m_owner.m_descEquipButton = value;
        }
      }

      public Button m_descUnloadButton
      {
        get
        {
          return this.m_owner.m_descUnloadButton;
        }
        set
        {
          this.m_owner.m_descUnloadButton = value;
        }
      }

      public GameObject m_descPropContent
      {
        get
        {
          return this.m_owner.m_descPropContent;
        }
        set
        {
          this.m_owner.m_descPropContent = value;
        }
      }

      public GameObject m_descPropATGo
      {
        get
        {
          return this.m_owner.m_descPropATGo;
        }
        set
        {
          this.m_owner.m_descPropATGo = value;
        }
      }

      public Text m_descPropATValueText
      {
        get
        {
          return this.m_owner.m_descPropATValueText;
        }
        set
        {
          this.m_owner.m_descPropATValueText = value;
        }
      }

      public GameObject m_descPropDFGo
      {
        get
        {
          return this.m_owner.m_descPropDFGo;
        }
        set
        {
          this.m_owner.m_descPropDFGo = value;
        }
      }

      public Text m_descPropDFValueText
      {
        get
        {
          return this.m_owner.m_descPropDFValueText;
        }
        set
        {
          this.m_owner.m_descPropDFValueText = value;
        }
      }

      public GameObject m_descPropHPGo
      {
        get
        {
          return this.m_owner.m_descPropHPGo;
        }
        set
        {
          this.m_owner.m_descPropHPGo = value;
        }
      }

      public Text m_descPropHPValueText
      {
        get
        {
          return this.m_owner.m_descPropHPValueText;
        }
        set
        {
          this.m_owner.m_descPropHPValueText = value;
        }
      }

      public GameObject m_descPropMagiccGo
      {
        get
        {
          return this.m_owner.m_descPropMagiccGo;
        }
        set
        {
          this.m_owner.m_descPropMagiccGo = value;
        }
      }

      public Text m_descPropMagicValueText
      {
        get
        {
          return this.m_owner.m_descPropMagicValueText;
        }
        set
        {
          this.m_owner.m_descPropMagicValueText = value;
        }
      }

      public GameObject m_descPropMagicDFGo
      {
        get
        {
          return this.m_owner.m_descPropMagicDFGo;
        }
        set
        {
          this.m_owner.m_descPropMagicDFGo = value;
        }
      }

      public Text m_descPropMagicDFValueText
      {
        get
        {
          return this.m_owner.m_descPropMagicDFValueText;
        }
        set
        {
          this.m_owner.m_descPropMagicDFValueText = value;
        }
      }

      public GameObject m_descPropDexGo
      {
        get
        {
          return this.m_owner.m_descPropDexGo;
        }
        set
        {
          this.m_owner.m_descPropDexGo = value;
        }
      }

      public Text m_descPropDexValueText
      {
        get
        {
          return this.m_owner.m_descPropDexValueText;
        }
        set
        {
          this.m_owner.m_descPropDexValueText = value;
        }
      }

      public CommonUIStateController m_descPropGroupStateCtrl
      {
        get
        {
          return this.m_owner.m_descPropGroupStateCtrl;
        }
        set
        {
          this.m_owner.m_descPropGroupStateCtrl = value;
        }
      }

      public GameObject m_descPropEnchantmentGroup
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroup;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroup = value;
        }
      }

      public Image m_descPropEnchantmentGroupRuneIconImage
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroupRuneIconImage;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroupRuneIconImage = value;
        }
      }

      public Text m_descPropEnchantmentGroupRuneNameText
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroupRuneNameText;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroupRuneNameText = value;
        }
      }

      public CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroupRuneStateCtrl;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroupRuneStateCtrl = value;
        }
      }

      public GameObject m_PropertyComparisonGo
      {
        get
        {
          return this.m_owner.m_PropertyComparisonGo;
        }
        set
        {
          this.m_owner.m_PropertyComparisonGo = value;
        }
      }

      public GameObject m_allDescPropGo
      {
        get
        {
          return this.m_owner.m_allDescPropGo;
        }
        set
        {
          this.m_owner.m_allDescPropGo = value;
        }
      }

      public GameObject m_allDescHpGo
      {
        get
        {
          return this.m_owner.m_allDescHpGo;
        }
        set
        {
          this.m_owner.m_allDescHpGo = value;
        }
      }

      public Text m_allDescHpOldText
      {
        get
        {
          return this.m_owner.m_allDescHpOldText;
        }
        set
        {
          this.m_owner.m_allDescHpOldText = value;
        }
      }

      public Text m_allDescHpNewText
      {
        get
        {
          return this.m_owner.m_allDescHpNewText;
        }
        set
        {
          this.m_owner.m_allDescHpNewText = value;
        }
      }

      public GameObject m_allDescATGo
      {
        get
        {
          return this.m_owner.m_allDescATGo;
        }
        set
        {
          this.m_owner.m_allDescATGo = value;
        }
      }

      public Text m_allDescATOldText
      {
        get
        {
          return this.m_owner.m_allDescATOldText;
        }
        set
        {
          this.m_owner.m_allDescATOldText = value;
        }
      }

      public Text m_allDescATNewText
      {
        get
        {
          return this.m_owner.m_allDescATNewText;
        }
        set
        {
          this.m_owner.m_allDescATNewText = value;
        }
      }

      public GameObject m_allDescDFGo
      {
        get
        {
          return this.m_owner.m_allDescDFGo;
        }
        set
        {
          this.m_owner.m_allDescDFGo = value;
        }
      }

      public Text m_allDescDFOldText
      {
        get
        {
          return this.m_owner.m_allDescDFOldText;
        }
        set
        {
          this.m_owner.m_allDescDFOldText = value;
        }
      }

      public Text m_allDescDFNewText
      {
        get
        {
          return this.m_owner.m_allDescDFNewText;
        }
        set
        {
          this.m_owner.m_allDescDFNewText = value;
        }
      }

      public GameObject m_allDescMagicDFGo
      {
        get
        {
          return this.m_owner.m_allDescMagicDFGo;
        }
        set
        {
          this.m_owner.m_allDescMagicDFGo = value;
        }
      }

      public Text m_allDescMagicDFOldText
      {
        get
        {
          return this.m_owner.m_allDescMagicDFOldText;
        }
        set
        {
          this.m_owner.m_allDescMagicDFOldText = value;
        }
      }

      public Text m_allDescMagicDFNewText
      {
        get
        {
          return this.m_owner.m_allDescMagicDFNewText;
        }
        set
        {
          this.m_owner.m_allDescMagicDFNewText = value;
        }
      }

      public GameObject m_allDescMagicGo
      {
        get
        {
          return this.m_owner.m_allDescMagicGo;
        }
        set
        {
          this.m_owner.m_allDescMagicGo = value;
        }
      }

      public Text m_allDescMagicOldText
      {
        get
        {
          return this.m_owner.m_allDescMagicOldText;
        }
        set
        {
          this.m_owner.m_allDescMagicOldText = value;
        }
      }

      public Text m_allDescMagicNewText
      {
        get
        {
          return this.m_owner.m_allDescMagicNewText;
        }
        set
        {
          this.m_owner.m_allDescMagicNewText = value;
        }
      }

      public GameObject m_allDescDexGo
      {
        get
        {
          return this.m_owner.m_allDescDexGo;
        }
        set
        {
          this.m_owner.m_allDescDexGo = value;
        }
      }

      public Text m_allDescDexOldText
      {
        get
        {
          return this.m_owner.m_allDescDexOldText;
        }
        set
        {
          this.m_owner.m_allDescDexOldText = value;
        }
      }

      public Text m_allDescDexNewText
      {
        get
        {
          return this.m_owner.m_allDescDexNewText;
        }
        set
        {
          this.m_owner.m_allDescDexNewText = value;
        }
      }

      public GameObject m_battlePowerGo
      {
        get
        {
          return this.m_owner.m_battlePowerGo;
        }
        set
        {
          this.m_owner.m_battlePowerGo = value;
        }
      }

      public Text m_battlePowerValueText
      {
        get
        {
          return this.m_owner.m_battlePowerValueText;
        }
        set
        {
          this.m_owner.m_battlePowerValueText = value;
        }
      }

      public CommonUIStateController m_confirmPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_confirmPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_confirmPanelStateCtrl = value;
        }
      }

      public Button m_confirmPanelCancelButton
      {
        get
        {
          return this.m_owner.m_confirmPanelCancelButton;
        }
        set
        {
          this.m_owner.m_confirmPanelCancelButton = value;
        }
      }

      public Button m_confirmPanelConfirmButton
      {
        get
        {
          return this.m_owner.m_confirmPanelConfirmButton;
        }
        set
        {
          this.m_owner.m_confirmPanelConfirmButton = value;
        }
      }

      public int m_isAscend
      {
        get
        {
          return this.m_owner.m_isAscend;
        }
        set
        {
          this.m_owner.m_isAscend = value;
        }
      }

      public EquipmentDepotUIController.EquipmentSortTypeState m_curEquipmentSortType
      {
        get
        {
          return this.m_owner.m_curEquipmentSortType;
        }
        set
        {
          this.m_owner.m_curEquipmentSortType = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public int m_slot
      {
        get
        {
          return this.m_owner.m_slot;
        }
        set
        {
          this.m_owner.m_slot = value;
        }
      }

      public int m_canUseEquipmentCount
      {
        get
        {
          return this.m_owner.m_canUseEquipmentCount;
        }
        set
        {
          this.m_owner.m_canUseEquipmentCount = value;
        }
      }

      public ulong m_curEquipmentInstanceId
      {
        get
        {
          return this.m_owner.m_curEquipmentInstanceId;
        }
        set
        {
          this.m_owner.m_curEquipmentInstanceId = value;
        }
      }

      public bool m_isFirstIn
      {
        get
        {
          return this.m_owner.m_isFirstIn;
        }
        set
        {
          this.m_owner.m_isFirstIn = value;
        }
      }

      public List<EquipmentBagItem> m_equipItemCache
      {
        get
        {
          return this.m_owner.m_equipItemCache;
        }
        set
        {
          this.m_owner.m_equipItemCache = value;
        }
      }

      public List<EquipmentDepotListItemUIController> m_equipmentDepotCtrlList
      {
        get
        {
          return this.m_owner.m_equipmentDepotCtrlList;
        }
        set
        {
          this.m_owner.m_equipmentDepotCtrlList = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void InitSortTypePanel()
      {
        this.m_owner.InitSortTypePanel();
      }

      public void InitLoopScrollRect()
      {
        this.m_owner.InitLoopScrollRect();
      }

      public void OnPoolObjectCreated(string poolName, GameObject go)
      {
        this.m_owner.OnPoolObjectCreated(poolName, go);
      }

      public void CreateEquipmentList()
      {
        this.m_owner.CreateEquipmentList();
      }

      public void SortEquipmentListByType(List<EquipmentBagItem> list)
      {
        this.m_owner.SortEquipmentListByType(list);
      }

      public int DefaultEquipmentItemComparer(EquipmentBagItem e1, EquipmentBagItem e2)
      {
        return this.m_owner.DefaultEquipmentItemComparer(e1, e2);
      }

      public void CollectEquipmentPropValueAndSort(
        List<EquipmentBagItem> list,
        PropertyModifyType type)
      {
        this.m_owner.CollectEquipmentPropValueAndSort(list, type);
      }

      public void OnEquipmentListItemClick(UIControllerBase itemCtrl)
      {
        this.m_owner.OnEquipmentListItemClick(itemCtrl);
      }

      public void OnEquipmentListItemNeedFill(UIControllerBase itemCtrl)
      {
        this.m_owner.OnEquipmentListItemNeedFill(itemCtrl);
      }

      public void OnFilterSortButtonClick()
      {
        this.m_owner.OnFilterSortButtonClick();
      }

      public void OnCloseFilterSortTypeGo()
      {
        this.m_owner.OnCloseFilterSortTypeGo();
      }

      public void OnFilterSortOrderButtonClick()
      {
        this.m_owner.OnFilterSortOrderButtonClick();
      }

      public void OnFilterTypeButtonClick(EquipmentSortItemUIController ctrl, bool isOn)
      {
        this.m_owner.OnFilterTypeButtonClick(ctrl, isOn);
      }

      public void SetEquipmentItemDesc(EquipmentBagItem equipment)
      {
        this.m_owner.SetEquipmentItemDesc(equipment);
      }

      public void SetEquipmentComparison(EquipmentBagItem equipment)
      {
        this.m_owner.SetEquipmentComparison(equipment);
      }

      public void ResetEquipmentComparisonPropValue()
      {
        this.m_owner.ResetEquipmentComparisonPropValue();
      }

      public void SetEquipmentComparisonProp(PropertyModifyType type, int value, bool isDressed)
      {
        this.m_owner.SetEquipmentComparisonProp(type, value, isDressed);
      }

      public void SetPropItemColor(Text oldText, Text newText)
      {
        this.m_owner.SetPropItemColor(oldText, newText);
      }

      public void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
      {
        this.m_owner.SetPropItems(type, value, addValue, level);
      }

      public void OnDescEquipButtonClick()
      {
        this.m_owner.OnDescEquipButtonClick();
      }

      public void OnConfirmPanelConfirmButtonClick()
      {
        this.m_owner.OnConfirmPanelConfirmButtonClick();
      }

      public void OnConfirmPanelCancelButtonClick()
      {
        this.m_owner.OnConfirmPanelCancelButtonClick();
      }

      public void OnDescUnloadButtonClick()
      {
        this.m_owner.OnDescUnloadButtonClick();
      }

      public void OnDescLockButtonClick()
      {
        this.m_owner.OnDescLockButtonClick();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnGoldAddButtonClick()
      {
        this.m_owner.OnGoldAddButtonClick();
      }

      public void ResetScrollViewToTop()
      {
        this.m_owner.ResetScrollViewToTop();
      }
    }
  }
}
