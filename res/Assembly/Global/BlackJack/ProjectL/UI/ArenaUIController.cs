﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ArenaUIController : UIControllerBase
  {
    private static bool s_isShowAutoBattleTips = true;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./PlayerResource/ArenaCoin/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineArenaCoinText;
    [AutoBind("./PlayerResource/ArenaOnlineCoin/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineArenaCoinText;
    [AutoBind("./PlayerResource/Ticket", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_arenaTicketButton;
    [AutoBind("./PlayerResource/Ticket/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaTicketText;
    [AutoBind("./PlayerResource/Ticket/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addArenaTicketButton;
    [AutoBind("./Switch", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_switchUIStateController;
    [AutoBind("./Switch/SwitchButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_switchButton;
    [AutoBind("./Switch/SwitchButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_switchButtonUIStateController;
    [AutoBind("./OfflineAndOnlingChange", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_offlineOnlineChangeUIStateController;
    [AutoBind("./AutoBattleTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_autoBattleTipsUIStateController;
    [AutoBind("./AutoBattleTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoBattleTipsBackgroundButton;
    [AutoBind("./AutoBattleTips/Panel/ShowToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_autoBattleTipsShowToggle;
    [AutoBind("./AutoBattleTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoBattleTipsConfirmButton;
    [AutoBind("./AutoBattleTips/Panel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoBattleTipsCancelButton;
    [AutoBind("./RewardPreview", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardPreviewUIStateController;
    [AutoBind("./RewardPreview/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardPreviewBackgroundButton;
    [AutoBind("./RewardPreview/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_rewardPreviewScrollRect;
    [AutoBind("./ArenaTicketDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaTicketDescGameObject;
    [AutoBind("./ArenaTicketDesc", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_arenaTicketDescUIStateCtrl;
    [AutoBind("./ArenaTicketDesc/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_arenaTicketDescBackgroundButton;
    [AutoBind("./BuyArenaTicket", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buyArenaTicketUIStateController;
    [AutoBind("./BuyArenaTicket/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buyArenaTicketBackgroundButton;
    [AutoBind("./BuyArenaTicket/Panel/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_buyArenaTicketCountText;
    [AutoBind("./BuyArenaTicket/Panel/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buyArenaTicketBuyButton;
    [AutoBind("./BuyArenaTicket/Panel/BuyButton/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_buyArenaTicketPriceText;
    [AutoBind("./ArenaPointReward", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_offineArenaPointRewardUIStateCtrl;
    [AutoBind("./ArenaPointReward/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_offlineArenaPointRewardBGButton;
    [AutoBind("./ArenaPointReward/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_offlineArenaPointRewardCloseButton;
    [AutoBind("./ArenaPointReward/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_offlineArenaPointRewardScrollRect;
    [AutoBind("./ArenaRankingReward", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_onlineArenaRankingRewardUIStateCtrl;
    [AutoBind("./ArenaRankingReward/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_onlineArenaRankingRewardBGButton;
    [AutoBind("./ArenaRankingReward/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_onlineArenaRankingRewardCloseButton;
    [AutoBind("./ArenaRankingReward/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_onlineArenaRankingRewardscrollRect;
    [AutoBind("./Attack", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_attackUIStateController;
    [AutoBind("./Attack/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackBackgroundButton;
    [AutoBind("./Attack/Panel/Detail/Title/PowerValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_attackBattlePowerText;
    [AutoBind("./Attack/Panel/Detail/AttackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackConfirmButton;
    [AutoBind("./Attack/Panel/Detail/AutoBattleButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackAutoConfirmButton;
    [AutoBind("./Attack/Panel/Detail/Graphics", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_attackGraphicsGameObject;
    [AutoBind("./Attack/Panel/Detail/Title/PlayerImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_attackPlayerIconImage;
    [AutoBind("./Attack/Panel/Detail/Title/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_attackPlayerNameText;
    [AutoBind("./Attack/Panel/Detail/Title/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_attackPlayerLVText;
    [AutoBind("./Attack/Panel/Detail/Title/IntegralValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_attackPlayerArenaPointText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/ArenaLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaLevelListItemPrefab;
    [AutoBind("./Prefabs/ArenaRankingListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaRankingListItemPrefab;
    [AutoBind("./Prefabs/ArenaBattleReportListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineArenaBattleReportListItemPrefab;
    [AutoBind("./Prefabs/ArenaOpponentListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaOpponentListItemPrefab;
    [AutoBind("./Prefabs/ArenaPointRewardListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaPointRewardListItemPrefab;
    [AutoBind("./Prefabs/ArenaOnlineBattleReportListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineArenaBattleReportListItemPrefab;
    private bool m_isIgnoreToggleEvent;
    private bool m_isRevengeOpponent;
    private GainRewardButton[] m_offlineVictoryPointsRewardButtons;
    private GainRewardButton[] m_onlineWeekWinRewardButtons;
    private GameObjectPool<ArenaLevelListItemUIController> m_onlineArenaLevelPool;
    private List<UISpineGraphic> m_opponentGraphics;
    [AutoBind("./Offline", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineGameObject;
    [AutoBind("./Offline/Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_offlineMarginTransform;
    [AutoBind("./Offline/Margin/Tabs/ClashToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_offlineClashToggle;
    [AutoBind("./Offline/Margin/Tabs/BattleReportToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_offlineBattleReportToggle;
    [AutoBind("./Offline/Margin/Tabs/RankingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_offlineRankingToggle;
    [AutoBind("./Offline/Panels/Clash", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineClashPanelGameObject;
    [AutoBind("./Offline/Panels/Clash/Opponents", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineArenaOpponentsGameObject;
    [AutoBind("./Offline/Panels/Clash/InSettle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineInSettleGameObject;
    [AutoBind("./Offline/Panels/Clash/RefreshTime/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineArenaOpponentsRefreshTimeText;
    [AutoBind("./Offline/Panels/BattleReport", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineBattleReportPanelGameObject;
    [AutoBind("./Offline/Panels/BattleReport/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_offlineBattleReportScrollRect;
    [AutoBind("./Offline/Panels/BattleReport/EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineNoBattleReportGameObject;
    [AutoBind("./Offline/Panels/Ranking", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineRankingPanelGameObject;
    [AutoBind("./Offline/Panels/Ranking/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_offlineRankingScrollRect;
    [AutoBind("./Offline/Panels/Ranking/Player/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineRankingPlayerNameText;
    [AutoBind("./Offline/Panels/Ranking/Player/Ranking", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_offlineRankingPlayerRankingUIStateController;
    [AutoBind("./Offline/Panels/Ranking/Player/Ranking/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineRankingPlayerRankingText;
    [AutoBind("./Offline/Panels/Ranking/Player/Ranking/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_offlineRankingPlayerRankingImage;
    [AutoBind("./Offline/Panels/Ranking/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineRankingArenaPointsText;
    [AutoBind("./Offline/Panels/Ranking/Player/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_offlineRankingPlayerIconImage;
    [AutoBind("./Offline/Panels/Ranking/Player/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_offlineRankingPlayerHeadFrameTransform;
    [AutoBind("./Offline/Panels/Ranking/Player/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineRankingPlayerLevelText;
    [AutoBind("./Offline/Margin/Player/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_offlinePlayerIconImage;
    [AutoBind("./Offline/Margin/Player/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlinePlayerNameText;
    [AutoBind("./Offline/Margin/Player/LvGroup/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlinePlayerLevelText;
    [AutoBind("./Offline/Margin/Player/BattlePower/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlinePlayerBattlePowerText;
    [AutoBind("./Offline/Margin/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineArenaPointsText;
    [AutoBind("./Offline/Margin/Player/ArenaPointsUp/Text1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineArenaPointsUpText1;
    [AutoBind("./Offline/Margin/Player/ArenaPointsUp/Text2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineArenaPointsUpText2;
    [AutoBind("./Offline/Margin/Player/ArenaPointsUp/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_offlineArenaPointsUpBarImage;
    [AutoBind("./Offline/Margin/Player/DefendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_offlineDefendButton;
    [AutoBind("./Offline/Margin/Player/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_offlineArenaPointRewardButton;
    [AutoBind("./Offline/DownGroup/VictoryPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineVictoryPointsText;
    [AutoBind("./Offline/DownGroup/VictoryPointsReward/Button1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineVictoryPointsRewardButton1GameObject;
    [AutoBind("./Offline/DownGroup/VictoryPointsReward/Button2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineVictoryPointsRewardButton2GameObject;
    [AutoBind("./Offline/DownGroup/VictoryPointsReward/Button3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineVictoryPointsRewardButton3GameObject;
    [AutoBind("./Offline/DownGroup/VictoryPointsReward/BarFrame/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_offlineVictoryPointsRewardImage;
    [AutoBind("./Offline/DownGroup/WeekWin/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineWeekWinText;
    private GameObjectPool<ArenaOpponentListItemUIController> m_arenaOpponentPool;
    private GameObjectPool<ArenaRankingListItemUIController> m_arenaRankingPool;
    private GameObjectPool<OfflineArenaBattleReportListItemUIController> m_offlineArenaBattleReportPool;
    private GameObjectPool<ArenaPointRewardListItemUIController> m_offlineArenaPointRewardPool;
    [AutoBind("./Online", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineGameObject;
    [AutoBind("./Online/Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_onlineMarginTransform;
    [AutoBind("./Online/Margin/Tabs/ClashToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_onlineClashToggle;
    [AutoBind("./Online/Margin/Tabs/BattleReportToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_onlineBattleReportToggle;
    [AutoBind("./Online/Margin/Tabs/ArenaLevelToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_onlineDanToggle;
    [AutoBind("./Online/Margin/Tabs/LocalRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_onlineLocalRankingToggle;
    [AutoBind("./Online/Margin/Tabs/GlobalRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_onlineGlobalRankingToggle;
    [AutoBind("./Online/Panels/Clash", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineClashPanelGameObject;
    [AutoBind("./Online/Panels/Clash/LadderMode", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_onlineClashLadderModeUIStateController;
    [AutoBind("./Online/Panels/Clash/LadderMode/WinRate/WinRateValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineClashWinRateText;
    [AutoBind("./Online/Panels/Clash/LadderMode/WinRate/WinTimes/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineClashWinCountText;
    [AutoBind("./Online/Panels/Clash/LadderMode/WinRate/FailedTimes/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineClashLoseCountText;
    [AutoBind("./Online/Panels/Clash/LadderMode/WinRate/Times/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineClashTotalCountText;
    [AutoBind("./Online/Panels/Clash/LadderMode/WinRate/ArenaLevelIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineClashDanIconImage;
    [AutoBind("./Online/Panels/Clash/LadderMode/PromoteCompetitionInfo/NowArenaLevelIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineClashPromoteDanIconImage;
    [AutoBind("./Online/Panels/Clash/LadderMode/PromoteCompetitionInfo/AfterArenaLevelIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineClashPromoteNextDanIconImage;
    [AutoBind("./Online/Panels/Clash/LadderMode/PromoteCompetitionInfo/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineClashPromoteCountImage;
    [AutoBind("./Online/Panels/Clash/LadderMode/PromoteCompetitionInfo/CompetitionRoundGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineClashPromoteRoundGroupGameObject;
    [AutoBind("./Online/Panels/Clash/LadderMode/OpenTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineClashLadderOpenTimeGameObject;
    [AutoBind("./Online/Panels/Clash/LadderMode/OpenTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineClashLadderOpenTimeText;
    [AutoBind("./Online/Panels/Clash/LadderMode/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_onlineClashLadderChallengeButton;
    [AutoBind("./Online/Panels/Clash/LadderMode/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_onlineClashLadderChallengeButtonUIStateController;
    [AutoBind("./Online/Panels/Clash/CasualMode/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_onlineClashCasualChallengeButton;
    [AutoBind("./Online/Panels/BattleReport", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineBattleReportPanelGameObject;
    [AutoBind("./Online/Panels/BattleReport/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_onlineBattleReportScrollRect;
    [AutoBind("./Online/Panels/BattleReport/EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineNoBattleReportGameObject;
    [AutoBind("./Online/Panels/ArenaLevel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineDanPanelGameObject;
    [AutoBind("./Online/Panels/ArenaLevel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_onlineArenaLevelScrollRect;
    [AutoBind("./Online/Panels/Ranking", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineRankingPanelGameObject;
    [AutoBind("./Online/Panels/Ranking/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_onlineRankingScrollRect;
    [AutoBind("./Online/Panels/Ranking/Player/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineRankingPlayerNameText;
    [AutoBind("./Online/Panels/Ranking/Player/Ranking", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_onlineRankingPlayerRankingUIStateController;
    [AutoBind("./Online/Panels/Ranking/Player/Ranking/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineRankingPlayerRankingText;
    [AutoBind("./Online/Panels/Ranking/Player/Ranking/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineRankingPlayerRankingImage;
    [AutoBind("./Online/Panels/Ranking/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineRankingArenaPointsText;
    [AutoBind("./Online/Panels/Ranking/Player/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineRankingPlayerIconImage;
    [AutoBind("./Online/Panels/Ranking/Player/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_onlineRankingPlayerHeadFrameTransform;
    [AutoBind("./Online/Panels/Ranking/Player/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineRankingPlayerLevelText;
    [AutoBind("./Online/Margin/Player/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlinePlayerIconImage;
    [AutoBind("./Online/Margin/Player/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlinePlayerNameText;
    [AutoBind("./Online/Margin/Player/ArenaLevel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineDanText;
    [AutoBind("./Online/Margin/Player/ArenaLevel/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineDanIconImage;
    [AutoBind("./Online/Margin/Player/LvGroup/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlinePlayerLevelText;
    [AutoBind("./Online/Margin/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineArenaPointsText;
    [AutoBind("./Online/Margin/Player/ArenaPointsUp/Text1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineArenaPointsUpText1;
    [AutoBind("./Online/Margin/Player/ArenaPointsUp/Text2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineArenaPointsUpText2;
    [AutoBind("./Online/Margin/Player/ArenaPointsUp/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineArenaPointsUpBarImage;
    [AutoBind("./Online/Margin/Player/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_onlineArenaRankingRewardButton;
    [AutoBind("./Online/DownGroup/WeekWin/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineWeekWinText;
    [AutoBind("./Online/DownGroup/WeekWinReward/Button1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineWeekWinRewardButton1GameObject;
    [AutoBind("./Online/DownGroup/WeekWinReward/Button2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineWeekWinRewardButton2GameObject;
    [AutoBind("./Online/DownGroup/WeekWinReward/Button3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineWeekWinRewardButton3GameObject;
    [AutoBind("./Online/DownGroup/WeekWinReward/BarFrame/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineWeekWinRewardImage;
    [AutoBind("./PromoteBattleTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_promoteBattleUIStateController;
    [AutoBind("./PromoteBattleTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteBattleBGButton;
    [AutoBind("./PromoteBattleTips/Panel/TipsText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_promoteBattleText;
    [AutoBind("./PromoteBattleTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteBattleConfirmButton;
    [AutoBind("./PromoteSucceedTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_promoteSucceedUIStateController;
    [AutoBind("./PromoteSucceedTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteSucceedBGButton;
    [AutoBind("./PromoteSucceedTips/Panel/ArenaLevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_promoteSucceedDanImage;
    [AutoBind("./PromoteSucceedTips/Panel/Info/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_promoteSucceedDanText;
    [AutoBind("./PromoteSucceedTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteSucceedConfirmButton;
    [AutoBind("./MatchingFailedTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_matchingFailedUIStateController;
    [AutoBind("./MatchingFailedTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_matchingFailedBGButton;
    [AutoBind("./MatchingFailedTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_matchingFailedConfirmButton;
    [AutoBind("./MatchingNow", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_matchingNowUIStateController;
    [AutoBind("./MatchingNow/Panel/Detail/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingNowTimeText;
    [AutoBind("./MatchingNow/Panel/Detail/PredictTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_matchingNowPredictTimeGameObject;
    [AutoBind("./MatchingNow/Panel/Detail/PredictTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingNowPredictTimeText;
    [AutoBind("./MatchingNow/Panel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_matchingNowCancelButton;
    private GameObjectPool<ArenaRankingListItemUIController> m_onlineArenaRankingPool;
    private GameObjectPool<OnlineArenaBattleReportListItemUIController> m_onlineArenaBattleReportPool;
    private GameObjectPool<ArenaRankingRewardListItemUIController> m_onlineArenaRankingRewardPool;
    private bool m_isMatchingNow;
    [DoNotToLua]
    private ArenaUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OpenArenaUITypeBoolean_hotfix;
    private LuaFunction m_SwitchOfflineOnlineArenaUIType_hotfix;
    private LuaFunction m_SetPlayerHeadIconInt32_hotfix;
    private LuaFunction m_SetPlayerNameString_hotfix;
    private LuaFunction m_SetPlayerLevelInt32_hotfix;
    private LuaFunction m_SetArenaHonorInt32_hotfix;
    private LuaFunction m_SetArenaTicketInt32Int32_hotfix;
    private LuaFunction m_SetBattlePowerInt32_hotfix;
    private LuaFunction m_ShowArenaTicketDesc_hotfix;
    private LuaFunction m_CloseArenaTicketDesc_hotfix;
    private LuaFunction m_CreateSpineGraphicConfigDataJobConnectionInfoConfigDataModelSkinResourceInfo_hotfix;
    private LuaFunction m_AddRewardPreviewGoodsList`1_hotfix;
    private LuaFunction m_ShowBuyArenaTicketInt32Int32_hotfix;
    private LuaFunction m_HideBuyArenaTicket_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnRewardPreviewBackgroundButtonClick_hotfix;
    private LuaFunction m_OnAddArenaTicketButtonClick_hotfix;
    private LuaFunction m_OnBuyArenaTicketBackgroundButtonClick_hotfix;
    private LuaFunction m_OnBuyArenaTicketBuyButtonClick_hotfix;
    private LuaFunction m_OnSwitchButtonClick_hotfix;
    private LuaFunction m_add_EventOnDefendAction_hotfix;
    private LuaFunction m_remove_EventOnDefendAction_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnShowBuyArenaTicketAction_hotfix;
    private LuaFunction m_remove_EventOnShowBuyArenaTicketAction_hotfix;
    private LuaFunction m_add_EventOnBuyArenaTicketAction_hotfix;
    private LuaFunction m_remove_EventOnBuyArenaTicketAction_hotfix;
    private LuaFunction m_add_EventOnSwitchOnlineOfflineAction_hotfix;
    private LuaFunction m_remove_EventOnSwitchOnlineOfflineAction_hotfix;
    private LuaFunction m_OfflineOnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetOfflineArenaPointsAndLevelInt32Int32_hotfix;
    private LuaFunction m_SetOfflineWeekWinInt32Int32_hotfix;
    private LuaFunction m_SetOfflineInSettleTimeBoolean_hotfix;
    private LuaFunction m_SetOfflineVictoryPointsRewardStatusInt32Int32GainRewardStatus_hotfix;
    private LuaFunction m_SetOfflineVictoryPointsRewardProgressInt32Int32_hotfix;
    private LuaFunction m_SetOfflineArenaOpponentsList`1Boolean_hotfix;
    private LuaFunction m_SetOfflineArenaOpponentsRefreshTimeTimeSpan_hotfix;
    private LuaFunction m_SetOfflineArenaBattleReportsList`1_hotfix;
    private LuaFunction m_SetOfflineArenaRankingsInt32List`1_hotfix;
    private LuaFunction m_ShowOfflinePanelOfflineArenaPanelType_hotfix;
    private LuaFunction m_ShowAutoBattleTips_hotfix;
    private LuaFunction m_HideAutoBattleTips_hotfix;
    private LuaFunction m_ShowArenaOpponentArenaOpponentList`1Int32Boolean_hotfix;
    private LuaFunction m_HideArenaOpponent_hotfix;
    private LuaFunction m_ClearOpponentGraphcs_hotfix;
    private LuaFunction m_OnOfflineClashToggleBoolean_hotfix;
    private LuaFunction m_OnOfflineBattleReportToggleBoolean_hotfix;
    private LuaFunction m_OnOfflineRankingToggleBoolean_hotfix;
    private LuaFunction m_OnOfflineArenaPointRewardButtonClick_hotfix;
    private LuaFunction m_OnOfflineArenaPointRewardCloseButtonClick_hotfix;
    private LuaFunction m_OnOfflineVictoryPointsRewardButtonClickGainRewardButton_hotfix;
    private LuaFunction m_OnOfflineArenaOpponentAttackButtonClickArenaOpponentListItemUIController_hotfix;
    private LuaFunction m_OnOfflineBattleReportRevengeButtonClickOfflineArenaBattleReportListItemUIController_hotfix;
    private LuaFunction m_OnOfflineBattleReportReplayButtonClickOfflineArenaBattleReportListItemUIController_hotfix;
    private LuaFunction m_OnDefendButtonClick_hotfix;
    private LuaFunction m_OnAttackConfirmButtonClick_hotfix;
    private LuaFunction m_OnAutoBattleButtonClick_hotfix;
    private LuaFunction m_OnAttackBackgroundButtonClick_hotfix;
    private LuaFunction m_OnAutoBattleTipsConfirmButtonClick_hotfix;
    private LuaFunction m_OnAutoBattleTipsCancelButtonClick_hotfix;
    private LuaFunction m_add_EventOnShowOfflinePanelAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowOfflinePanelAction`1_hotfix;
    private LuaFunction m_add_EventOnGainOfflineVictoryPointsRewardAction`1_hotfix;
    private LuaFunction m_remove_EventOnGainOfflineVictoryPointsRewardAction`1_hotfix;
    private LuaFunction m_add_EventOnShowOfflineArenaOpponentAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowOfflineArenaOpponentAction`1_hotfix;
    private LuaFunction m_add_EventOnAttackOfflineArenaOpponentAction`1_hotfix;
    private LuaFunction m_remove_EventOnAttackOfflineArenaOpponentAction`1_hotfix;
    private LuaFunction m_add_EventOnShowRevengeOfflineArenaOpponentAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowRevengeOfflineArenaOpponentAction`1_hotfix;
    private LuaFunction m_add_EventOnRevengeOfflineArenaOpponentAction`1_hotfix;
    private LuaFunction m_remove_EventOnRevengeOfflineArenaOpponentAction`1_hotfix;
    private LuaFunction m_add_EventOnOfflineBattleReportReplayAction`1_hotfix;
    private LuaFunction m_remove_EventOnOfflineBattleReportReplayAction`1_hotfix;
    private LuaFunction m_OnlineOnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetOnlineScoreAndDanInt32Int32_hotfix;
    private LuaFunction m_SetLadderModeBooleanBoolean_hotfix;
    private LuaFunction m_SetLadderOpenTimeString_hotfix;
    private LuaFunction m_SetLadderWeekWinInt32Int32_hotfix;
    private LuaFunction m_SetOnlineWeekWinRewardStatusInt32Int32GainRewardStatus_hotfix;
    private LuaFunction m_SetOnlineWeekWinRewardProgressInt32Int32_hotfix;
    private LuaFunction m_ShowPromoteBattleInt32_hotfix;
    private LuaFunction m_ShowPromoteSucceedInt32_hotfix;
    private LuaFunction m_SetPromoteBattleStatusInt32List`1_hotfix;
    private LuaFunction m_SetOnlineDansList`1Int32_hotfix;
    private LuaFunction m_SetOnlineArenaBattleReportsList`1_hotfix;
    private LuaFunction m_SetOnlineArenaRankingsInt32List`1List`1Boolean_hotfix;
    private LuaFunction m_ShowGlobalRankingToggleBoolean_hotfix;
    private LuaFunction m_ShowOnlinePanelOnlineArenaPanelType_hotfix;
    private LuaFunction m_ShowMatchingNow_hotfix;
    private LuaFunction m_HideMatchingNow_hotfix;
    private LuaFunction m_SetMatchingNowTimeTimeSpan_hotfix;
    private LuaFunction m_SetMatchingPredictTimeTimeSpan_hotfix;
    private LuaFunction m_IsMatchingNowPredictTimeActive_hotfix;
    private LuaFunction m_ShowMatchingFailed_hotfix;
    private LuaFunction m_OnOnlineClashToggleBoolean_hotfix;
    private LuaFunction m_OnOnlineBattleReportToggleBoolean_hotfix;
    private LuaFunction m_OnOnlineDanToggleBoolean_hotfix;
    private LuaFunction m_OnOnlineLocalRankingToggleBoolean_hotfix;
    private LuaFunction m_OnOnlineGlobalRankingToggleBoolean_hotfix;
    private LuaFunction m_OnOnlineWeekWinRewardButtonClickGainRewardButton_hotfix;
    private LuaFunction m_OnOnlineBattleReportReplayButtonClickOnlineArenaBattleReportListItemUIController_hotfix;
    private LuaFunction m_OnOnlineClashLadderChallengeButtonClick_hotfix;
    private LuaFunction m_OnOnlineClashCasualChallengeButtonClick_hotfix;
    private LuaFunction m_OnMatchingFailedConfirmButtonClick_hotfix;
    private LuaFunction m_OnMatchingNowCancelButtonClick_hotfix;
    private LuaFunction m_OnOnlineArenaRankingRewardButtonClick_hotfix;
    private LuaFunction m_OnOnlineArenaRankingRewardCloseButtonClick_hotfix;
    private LuaFunction m_OnPromoteBattleConfirmButtonClick_hotfix;
    private LuaFunction m_OnPromoteSucceedConfirmButtonClick_hotfix;
    private LuaFunction m_add_EventOnShowOnlinePanelAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowOnlinePanelAction`1_hotfix;
    private LuaFunction m_add_EventOnGainOnlineWeekWinRewardAction`1_hotfix;
    private LuaFunction m_remove_EventOnGainOnlineWeekWinRewardAction`1_hotfix;
    private LuaFunction m_add_EventOnOnlineBattleReportReplayAction`1_hotfix;
    private LuaFunction m_remove_EventOnOnlineBattleReportReplayAction`1_hotfix;
    private LuaFunction m_add_EventOnLadderChallengeAction_hotfix;
    private LuaFunction m_remove_EventOnLadderChallengeAction_hotfix;
    private LuaFunction m_add_EventOnCasualChallengeAction_hotfix;
    private LuaFunction m_remove_EventOnCasualChallengeAction_hotfix;
    private LuaFunction m_add_EventOnMatchingCancelAction_hotfix;
    private LuaFunction m_remove_EventOnMatchingCancelAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private ArenaUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(ArenaUIType uiType, bool canSwitchOnline)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SwitchOfflineOnline(ArenaUIType uiType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerHeadIcon(int playerHeadIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaHonor(int areanaCoin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaTicket(int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattlePower(int battlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowArenaTicketDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseArenaTicketDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UISpineGraphic CreateSpineGraphic(
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ConfigDataModelSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddRewardPreviewGoods(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBuyArenaTicket(int ticketCount, int price)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBuyArenaTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardPreviewBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddArenaTicketButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyArenaTicketBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyArenaTicketBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSwitchButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnDefend
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowBuyArenaTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBuyArenaTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSwitchOnlineOffline
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OfflineOnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineArenaPointsAndLevel(int arenaPoints, int arenaLevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineWeekWin(int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineInSettleTime(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineVictoryPointsRewardStatus(int idx, int num, GainRewardStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineVictoryPointsRewardProgress(int victoryPoints, int victoryPointsMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineArenaOpponents(List<ArenaOpponent> opponents, bool isGuardBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineArenaOpponentsRefreshTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineArenaBattleReports(List<ArenaBattleReport> battleReports)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineArenaRankings(int mineRank, List<ProArenaTopRankPlayer> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOfflinePanel(OfflineArenaPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowAutoBattleTips()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideAutoBattleTips()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowArenaOpponent(
      ArenaOpponent opponent,
      List<BattleHero> heros,
      int battlePower,
      bool isRevenge)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideArenaOpponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearOpponentGraphcs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineClashToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineBattleReportToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineRankingToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineArenaPointRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineArenaPointRewardCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineVictoryPointsRewardButtonClick(GainRewardButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineArenaOpponentAttackButtonClick(ArenaOpponentListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineBattleReportRevengeButtonClick(
      OfflineArenaBattleReportListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineBattleReportReplayButtonClick(
      OfflineArenaBattleReportListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDefendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAttackConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoBattleButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAttackBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoBattleTipsConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoBattleTipsCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<OfflineArenaPanelType> EventOnShowOfflinePanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGainOfflineVictoryPointsReward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnShowOfflineArenaOpponent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnAttackOfflineArenaOpponent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ArenaBattleReport> EventOnShowRevengeOfflineArenaOpponent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnRevengeOfflineArenaOpponent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ArenaBattleReport> EventOnOfflineBattleReportReplay
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnlineOnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOnlineScoreAndDan(int score, int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLadderMode(bool isOpened, bool isPromote)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLadderOpenTime(string openTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLadderWeekWin(int wins, int matches)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOnlineWeekWinRewardStatus(int idx, int num, GainRewardStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOnlineWeekWinRewardProgress(int wins, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPromoteBattle(int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPromoteSucceed(int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPromoteBattleStatus(
      int danId,
      List<RealTimePVPBattleReport> promoteBattleReports)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOnlineDans(List<ConfigDataRealTimePVPDanInfo> danInfos, int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOnlineArenaBattleReports(List<RealTimePVPBattleReport> battleReports)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOnlineArenaRankings(
      int mineRank,
      List<ProRealTimePVPLeaderboardPlayerInfo> playerInfos,
      List<ProUserSummary> userSummarys,
      bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowGlobalRankingToggle(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOnlinePanel(OnlineArenaPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMatchingNow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideMatchingNow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMatchingNowTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMatchingPredictTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMatchingNowPredictTimeActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMatchingFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineClashToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineBattleReportToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineDanToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineLocalRankingToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineGlobalRankingToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineWeekWinRewardButtonClick(GainRewardButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineBattleReportReplayButtonClick(
      OnlineArenaBattleReportListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineClashLadderChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineClashCasualChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMatchingFailedConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMatchingNowCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineArenaRankingRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineArenaRankingRewardCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPromoteBattleConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPromoteSucceedConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<OnlineArenaPanelType> EventOnShowOnlinePanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGainOnlineWeekWinReward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<RealTimePVPBattleReport> EventOnOnlineBattleReportReplay
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnLadderChallenge
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCasualChallenge
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnMatchingCancel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ArenaUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnDefend()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnDefend()
    {
      this.EventOnDefend = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowBuyArenaTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowBuyArenaTicket()
    {
      this.EventOnShowBuyArenaTicket = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBuyArenaTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBuyArenaTicket()
    {
      this.EventOnBuyArenaTicket = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSwitchOnlineOffline()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSwitchOnlineOffline()
    {
      this.EventOnSwitchOnlineOffline = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowOfflinePanel(OfflineArenaPanelType obj)
    {
    }

    private void __clearDele_EventOnShowOfflinePanel(OfflineArenaPanelType obj)
    {
      this.EventOnShowOfflinePanel = (Action<OfflineArenaPanelType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGainOfflineVictoryPointsReward(int obj)
    {
    }

    private void __clearDele_EventOnGainOfflineVictoryPointsReward(int obj)
    {
      this.EventOnGainOfflineVictoryPointsReward = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowOfflineArenaOpponent(int obj)
    {
    }

    private void __clearDele_EventOnShowOfflineArenaOpponent(int obj)
    {
      this.EventOnShowOfflineArenaOpponent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAttackOfflineArenaOpponent(bool obj)
    {
    }

    private void __clearDele_EventOnAttackOfflineArenaOpponent(bool obj)
    {
      this.EventOnAttackOfflineArenaOpponent = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowRevengeOfflineArenaOpponent(ArenaBattleReport obj)
    {
    }

    private void __clearDele_EventOnShowRevengeOfflineArenaOpponent(ArenaBattleReport obj)
    {
      this.EventOnShowRevengeOfflineArenaOpponent = (Action<ArenaBattleReport>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRevengeOfflineArenaOpponent(bool obj)
    {
    }

    private void __clearDele_EventOnRevengeOfflineArenaOpponent(bool obj)
    {
      this.EventOnRevengeOfflineArenaOpponent = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnOfflineBattleReportReplay(ArenaBattleReport obj)
    {
    }

    private void __clearDele_EventOnOfflineBattleReportReplay(ArenaBattleReport obj)
    {
      this.EventOnOfflineBattleReportReplay = (Action<ArenaBattleReport>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowOnlinePanel(OnlineArenaPanelType obj)
    {
    }

    private void __clearDele_EventOnShowOnlinePanel(OnlineArenaPanelType obj)
    {
      this.EventOnShowOnlinePanel = (Action<OnlineArenaPanelType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGainOnlineWeekWinReward(int obj)
    {
    }

    private void __clearDele_EventOnGainOnlineWeekWinReward(int obj)
    {
      this.EventOnGainOnlineWeekWinReward = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnOnlineBattleReportReplay(RealTimePVPBattleReport obj)
    {
    }

    private void __clearDele_EventOnOnlineBattleReportReplay(RealTimePVPBattleReport obj)
    {
      this.EventOnOnlineBattleReportReplay = (Action<RealTimePVPBattleReport>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnLadderChallenge()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnLadderChallenge()
    {
      this.EventOnLadderChallenge = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCasualChallenge()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCasualChallenge()
    {
      this.EventOnCasualChallenge = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnMatchingCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnMatchingCancel()
    {
      this.EventOnMatchingCancel = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ArenaUIController m_owner;

      public LuaExportHelper(ArenaUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnDefend()
      {
        this.m_owner.__callDele_EventOnDefend();
      }

      public void __clearDele_EventOnDefend()
      {
        this.m_owner.__clearDele_EventOnDefend();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnShowBuyArenaTicket()
      {
        this.m_owner.__callDele_EventOnShowBuyArenaTicket();
      }

      public void __clearDele_EventOnShowBuyArenaTicket()
      {
        this.m_owner.__clearDele_EventOnShowBuyArenaTicket();
      }

      public void __callDele_EventOnBuyArenaTicket()
      {
        this.m_owner.__callDele_EventOnBuyArenaTicket();
      }

      public void __clearDele_EventOnBuyArenaTicket()
      {
        this.m_owner.__clearDele_EventOnBuyArenaTicket();
      }

      public void __callDele_EventOnSwitchOnlineOffline()
      {
        this.m_owner.__callDele_EventOnSwitchOnlineOffline();
      }

      public void __clearDele_EventOnSwitchOnlineOffline()
      {
        this.m_owner.__clearDele_EventOnSwitchOnlineOffline();
      }

      public void __callDele_EventOnShowOfflinePanel(OfflineArenaPanelType obj)
      {
        this.m_owner.__callDele_EventOnShowOfflinePanel(obj);
      }

      public void __clearDele_EventOnShowOfflinePanel(OfflineArenaPanelType obj)
      {
        this.m_owner.__clearDele_EventOnShowOfflinePanel(obj);
      }

      public void __callDele_EventOnGainOfflineVictoryPointsReward(int obj)
      {
        this.m_owner.__callDele_EventOnGainOfflineVictoryPointsReward(obj);
      }

      public void __clearDele_EventOnGainOfflineVictoryPointsReward(int obj)
      {
        this.m_owner.__clearDele_EventOnGainOfflineVictoryPointsReward(obj);
      }

      public void __callDele_EventOnShowOfflineArenaOpponent(int obj)
      {
        this.m_owner.__callDele_EventOnShowOfflineArenaOpponent(obj);
      }

      public void __clearDele_EventOnShowOfflineArenaOpponent(int obj)
      {
        this.m_owner.__clearDele_EventOnShowOfflineArenaOpponent(obj);
      }

      public void __callDele_EventOnAttackOfflineArenaOpponent(bool obj)
      {
        this.m_owner.__callDele_EventOnAttackOfflineArenaOpponent(obj);
      }

      public void __clearDele_EventOnAttackOfflineArenaOpponent(bool obj)
      {
        this.m_owner.__clearDele_EventOnAttackOfflineArenaOpponent(obj);
      }

      public void __callDele_EventOnShowRevengeOfflineArenaOpponent(ArenaBattleReport obj)
      {
        this.m_owner.__callDele_EventOnShowRevengeOfflineArenaOpponent(obj);
      }

      public void __clearDele_EventOnShowRevengeOfflineArenaOpponent(ArenaBattleReport obj)
      {
        this.m_owner.__clearDele_EventOnShowRevengeOfflineArenaOpponent(obj);
      }

      public void __callDele_EventOnRevengeOfflineArenaOpponent(bool obj)
      {
        this.m_owner.__callDele_EventOnRevengeOfflineArenaOpponent(obj);
      }

      public void __clearDele_EventOnRevengeOfflineArenaOpponent(bool obj)
      {
        this.m_owner.__clearDele_EventOnRevengeOfflineArenaOpponent(obj);
      }

      public void __callDele_EventOnOfflineBattleReportReplay(ArenaBattleReport obj)
      {
        this.m_owner.__callDele_EventOnOfflineBattleReportReplay(obj);
      }

      public void __clearDele_EventOnOfflineBattleReportReplay(ArenaBattleReport obj)
      {
        this.m_owner.__clearDele_EventOnOfflineBattleReportReplay(obj);
      }

      public void __callDele_EventOnShowOnlinePanel(OnlineArenaPanelType obj)
      {
        this.m_owner.__callDele_EventOnShowOnlinePanel(obj);
      }

      public void __clearDele_EventOnShowOnlinePanel(OnlineArenaPanelType obj)
      {
        this.m_owner.__clearDele_EventOnShowOnlinePanel(obj);
      }

      public void __callDele_EventOnGainOnlineWeekWinReward(int obj)
      {
        this.m_owner.__callDele_EventOnGainOnlineWeekWinReward(obj);
      }

      public void __clearDele_EventOnGainOnlineWeekWinReward(int obj)
      {
        this.m_owner.__clearDele_EventOnGainOnlineWeekWinReward(obj);
      }

      public void __callDele_EventOnOnlineBattleReportReplay(RealTimePVPBattleReport obj)
      {
        this.m_owner.__callDele_EventOnOnlineBattleReportReplay(obj);
      }

      public void __clearDele_EventOnOnlineBattleReportReplay(RealTimePVPBattleReport obj)
      {
        this.m_owner.__clearDele_EventOnOnlineBattleReportReplay(obj);
      }

      public void __callDele_EventOnLadderChallenge()
      {
        this.m_owner.__callDele_EventOnLadderChallenge();
      }

      public void __clearDele_EventOnLadderChallenge()
      {
        this.m_owner.__clearDele_EventOnLadderChallenge();
      }

      public void __callDele_EventOnCasualChallenge()
      {
        this.m_owner.__callDele_EventOnCasualChallenge();
      }

      public void __clearDele_EventOnCasualChallenge()
      {
        this.m_owner.__clearDele_EventOnCasualChallenge();
      }

      public void __callDele_EventOnMatchingCancel()
      {
        this.m_owner.__callDele_EventOnMatchingCancel();
      }

      public void __clearDele_EventOnMatchingCancel()
      {
        this.m_owner.__clearDele_EventOnMatchingCancel();
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Text m_offlineArenaCoinText
      {
        get
        {
          return this.m_owner.m_offlineArenaCoinText;
        }
        set
        {
          this.m_owner.m_offlineArenaCoinText = value;
        }
      }

      public Text m_onlineArenaCoinText
      {
        get
        {
          return this.m_owner.m_onlineArenaCoinText;
        }
        set
        {
          this.m_owner.m_onlineArenaCoinText = value;
        }
      }

      public Button m_arenaTicketButton
      {
        get
        {
          return this.m_owner.m_arenaTicketButton;
        }
        set
        {
          this.m_owner.m_arenaTicketButton = value;
        }
      }

      public Text m_arenaTicketText
      {
        get
        {
          return this.m_owner.m_arenaTicketText;
        }
        set
        {
          this.m_owner.m_arenaTicketText = value;
        }
      }

      public Button m_addArenaTicketButton
      {
        get
        {
          return this.m_owner.m_addArenaTicketButton;
        }
        set
        {
          this.m_owner.m_addArenaTicketButton = value;
        }
      }

      public CommonUIStateController m_switchUIStateController
      {
        get
        {
          return this.m_owner.m_switchUIStateController;
        }
        set
        {
          this.m_owner.m_switchUIStateController = value;
        }
      }

      public Button m_switchButton
      {
        get
        {
          return this.m_owner.m_switchButton;
        }
        set
        {
          this.m_owner.m_switchButton = value;
        }
      }

      public CommonUIStateController m_switchButtonUIStateController
      {
        get
        {
          return this.m_owner.m_switchButtonUIStateController;
        }
        set
        {
          this.m_owner.m_switchButtonUIStateController = value;
        }
      }

      public CommonUIStateController m_offlineOnlineChangeUIStateController
      {
        get
        {
          return this.m_owner.m_offlineOnlineChangeUIStateController;
        }
        set
        {
          this.m_owner.m_offlineOnlineChangeUIStateController = value;
        }
      }

      public CommonUIStateController m_autoBattleTipsUIStateController
      {
        get
        {
          return this.m_owner.m_autoBattleTipsUIStateController;
        }
        set
        {
          this.m_owner.m_autoBattleTipsUIStateController = value;
        }
      }

      public Button m_autoBattleTipsBackgroundButton
      {
        get
        {
          return this.m_owner.m_autoBattleTipsBackgroundButton;
        }
        set
        {
          this.m_owner.m_autoBattleTipsBackgroundButton = value;
        }
      }

      public Toggle m_autoBattleTipsShowToggle
      {
        get
        {
          return this.m_owner.m_autoBattleTipsShowToggle;
        }
        set
        {
          this.m_owner.m_autoBattleTipsShowToggle = value;
        }
      }

      public Button m_autoBattleTipsConfirmButton
      {
        get
        {
          return this.m_owner.m_autoBattleTipsConfirmButton;
        }
        set
        {
          this.m_owner.m_autoBattleTipsConfirmButton = value;
        }
      }

      public Button m_autoBattleTipsCancelButton
      {
        get
        {
          return this.m_owner.m_autoBattleTipsCancelButton;
        }
        set
        {
          this.m_owner.m_autoBattleTipsCancelButton = value;
        }
      }

      public CommonUIStateController m_rewardPreviewUIStateController
      {
        get
        {
          return this.m_owner.m_rewardPreviewUIStateController;
        }
        set
        {
          this.m_owner.m_rewardPreviewUIStateController = value;
        }
      }

      public Button m_rewardPreviewBackgroundButton
      {
        get
        {
          return this.m_owner.m_rewardPreviewBackgroundButton;
        }
        set
        {
          this.m_owner.m_rewardPreviewBackgroundButton = value;
        }
      }

      public ScrollRect m_rewardPreviewScrollRect
      {
        get
        {
          return this.m_owner.m_rewardPreviewScrollRect;
        }
        set
        {
          this.m_owner.m_rewardPreviewScrollRect = value;
        }
      }

      public GameObject m_arenaTicketDescGameObject
      {
        get
        {
          return this.m_owner.m_arenaTicketDescGameObject;
        }
        set
        {
          this.m_owner.m_arenaTicketDescGameObject = value;
        }
      }

      public CommonUIStateController m_arenaTicketDescUIStateCtrl
      {
        get
        {
          return this.m_owner.m_arenaTicketDescUIStateCtrl;
        }
        set
        {
          this.m_owner.m_arenaTicketDescUIStateCtrl = value;
        }
      }

      public Button m_arenaTicketDescBackgroundButton
      {
        get
        {
          return this.m_owner.m_arenaTicketDescBackgroundButton;
        }
        set
        {
          this.m_owner.m_arenaTicketDescBackgroundButton = value;
        }
      }

      public CommonUIStateController m_buyArenaTicketUIStateController
      {
        get
        {
          return this.m_owner.m_buyArenaTicketUIStateController;
        }
        set
        {
          this.m_owner.m_buyArenaTicketUIStateController = value;
        }
      }

      public Button m_buyArenaTicketBackgroundButton
      {
        get
        {
          return this.m_owner.m_buyArenaTicketBackgroundButton;
        }
        set
        {
          this.m_owner.m_buyArenaTicketBackgroundButton = value;
        }
      }

      public Text m_buyArenaTicketCountText
      {
        get
        {
          return this.m_owner.m_buyArenaTicketCountText;
        }
        set
        {
          this.m_owner.m_buyArenaTicketCountText = value;
        }
      }

      public Button m_buyArenaTicketBuyButton
      {
        get
        {
          return this.m_owner.m_buyArenaTicketBuyButton;
        }
        set
        {
          this.m_owner.m_buyArenaTicketBuyButton = value;
        }
      }

      public Text m_buyArenaTicketPriceText
      {
        get
        {
          return this.m_owner.m_buyArenaTicketPriceText;
        }
        set
        {
          this.m_owner.m_buyArenaTicketPriceText = value;
        }
      }

      public CommonUIStateController m_offineArenaPointRewardUIStateCtrl
      {
        get
        {
          return this.m_owner.m_offineArenaPointRewardUIStateCtrl;
        }
        set
        {
          this.m_owner.m_offineArenaPointRewardUIStateCtrl = value;
        }
      }

      public Button m_offlineArenaPointRewardBGButton
      {
        get
        {
          return this.m_owner.m_offlineArenaPointRewardBGButton;
        }
        set
        {
          this.m_owner.m_offlineArenaPointRewardBGButton = value;
        }
      }

      public Button m_offlineArenaPointRewardCloseButton
      {
        get
        {
          return this.m_owner.m_offlineArenaPointRewardCloseButton;
        }
        set
        {
          this.m_owner.m_offlineArenaPointRewardCloseButton = value;
        }
      }

      public ScrollRect m_offlineArenaPointRewardScrollRect
      {
        get
        {
          return this.m_owner.m_offlineArenaPointRewardScrollRect;
        }
        set
        {
          this.m_owner.m_offlineArenaPointRewardScrollRect = value;
        }
      }

      public CommonUIStateController m_onlineArenaRankingRewardUIStateCtrl
      {
        get
        {
          return this.m_owner.m_onlineArenaRankingRewardUIStateCtrl;
        }
        set
        {
          this.m_owner.m_onlineArenaRankingRewardUIStateCtrl = value;
        }
      }

      public Button m_onlineArenaRankingRewardBGButton
      {
        get
        {
          return this.m_owner.m_onlineArenaRankingRewardBGButton;
        }
        set
        {
          this.m_owner.m_onlineArenaRankingRewardBGButton = value;
        }
      }

      public Button m_onlineArenaRankingRewardCloseButton
      {
        get
        {
          return this.m_owner.m_onlineArenaRankingRewardCloseButton;
        }
        set
        {
          this.m_owner.m_onlineArenaRankingRewardCloseButton = value;
        }
      }

      public ScrollRect m_onlineArenaRankingRewardscrollRect
      {
        get
        {
          return this.m_owner.m_onlineArenaRankingRewardscrollRect;
        }
        set
        {
          this.m_owner.m_onlineArenaRankingRewardscrollRect = value;
        }
      }

      public CommonUIStateController m_attackUIStateController
      {
        get
        {
          return this.m_owner.m_attackUIStateController;
        }
        set
        {
          this.m_owner.m_attackUIStateController = value;
        }
      }

      public Button m_attackBackgroundButton
      {
        get
        {
          return this.m_owner.m_attackBackgroundButton;
        }
        set
        {
          this.m_owner.m_attackBackgroundButton = value;
        }
      }

      public Text m_attackBattlePowerText
      {
        get
        {
          return this.m_owner.m_attackBattlePowerText;
        }
        set
        {
          this.m_owner.m_attackBattlePowerText = value;
        }
      }

      public Button m_attackConfirmButton
      {
        get
        {
          return this.m_owner.m_attackConfirmButton;
        }
        set
        {
          this.m_owner.m_attackConfirmButton = value;
        }
      }

      public Button m_attackAutoConfirmButton
      {
        get
        {
          return this.m_owner.m_attackAutoConfirmButton;
        }
        set
        {
          this.m_owner.m_attackAutoConfirmButton = value;
        }
      }

      public GameObject m_attackGraphicsGameObject
      {
        get
        {
          return this.m_owner.m_attackGraphicsGameObject;
        }
        set
        {
          this.m_owner.m_attackGraphicsGameObject = value;
        }
      }

      public Image m_attackPlayerIconImage
      {
        get
        {
          return this.m_owner.m_attackPlayerIconImage;
        }
        set
        {
          this.m_owner.m_attackPlayerIconImage = value;
        }
      }

      public Text m_attackPlayerNameText
      {
        get
        {
          return this.m_owner.m_attackPlayerNameText;
        }
        set
        {
          this.m_owner.m_attackPlayerNameText = value;
        }
      }

      public Text m_attackPlayerLVText
      {
        get
        {
          return this.m_owner.m_attackPlayerLVText;
        }
        set
        {
          this.m_owner.m_attackPlayerLVText = value;
        }
      }

      public Text m_attackPlayerArenaPointText
      {
        get
        {
          return this.m_owner.m_attackPlayerArenaPointText;
        }
        set
        {
          this.m_owner.m_attackPlayerArenaPointText = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_arenaLevelListItemPrefab
      {
        get
        {
          return this.m_owner.m_arenaLevelListItemPrefab;
        }
        set
        {
          this.m_owner.m_arenaLevelListItemPrefab = value;
        }
      }

      public GameObject m_arenaRankingListItemPrefab
      {
        get
        {
          return this.m_owner.m_arenaRankingListItemPrefab;
        }
        set
        {
          this.m_owner.m_arenaRankingListItemPrefab = value;
        }
      }

      public GameObject m_offlineArenaBattleReportListItemPrefab
      {
        get
        {
          return this.m_owner.m_offlineArenaBattleReportListItemPrefab;
        }
        set
        {
          this.m_owner.m_offlineArenaBattleReportListItemPrefab = value;
        }
      }

      public GameObject m_arenaOpponentListItemPrefab
      {
        get
        {
          return this.m_owner.m_arenaOpponentListItemPrefab;
        }
        set
        {
          this.m_owner.m_arenaOpponentListItemPrefab = value;
        }
      }

      public GameObject m_arenaPointRewardListItemPrefab
      {
        get
        {
          return this.m_owner.m_arenaPointRewardListItemPrefab;
        }
        set
        {
          this.m_owner.m_arenaPointRewardListItemPrefab = value;
        }
      }

      public GameObject m_onlineArenaBattleReportListItemPrefab
      {
        get
        {
          return this.m_owner.m_onlineArenaBattleReportListItemPrefab;
        }
        set
        {
          this.m_owner.m_onlineArenaBattleReportListItemPrefab = value;
        }
      }

      public bool m_isIgnoreToggleEvent
      {
        get
        {
          return this.m_owner.m_isIgnoreToggleEvent;
        }
        set
        {
          this.m_owner.m_isIgnoreToggleEvent = value;
        }
      }

      public bool m_isRevengeOpponent
      {
        get
        {
          return this.m_owner.m_isRevengeOpponent;
        }
        set
        {
          this.m_owner.m_isRevengeOpponent = value;
        }
      }

      public static bool s_isShowAutoBattleTips
      {
        get
        {
          return ArenaUIController.s_isShowAutoBattleTips;
        }
        set
        {
          ArenaUIController.s_isShowAutoBattleTips = value;
        }
      }

      public GainRewardButton[] m_offlineVictoryPointsRewardButtons
      {
        get
        {
          return this.m_owner.m_offlineVictoryPointsRewardButtons;
        }
        set
        {
          this.m_owner.m_offlineVictoryPointsRewardButtons = value;
        }
      }

      public GainRewardButton[] m_onlineWeekWinRewardButtons
      {
        get
        {
          return this.m_owner.m_onlineWeekWinRewardButtons;
        }
        set
        {
          this.m_owner.m_onlineWeekWinRewardButtons = value;
        }
      }

      public GameObjectPool<ArenaLevelListItemUIController> m_onlineArenaLevelPool
      {
        get
        {
          return this.m_owner.m_onlineArenaLevelPool;
        }
        set
        {
          this.m_owner.m_onlineArenaLevelPool = value;
        }
      }

      public List<UISpineGraphic> m_opponentGraphics
      {
        get
        {
          return this.m_owner.m_opponentGraphics;
        }
        set
        {
          this.m_owner.m_opponentGraphics = value;
        }
      }

      public GameObject m_offlineGameObject
      {
        get
        {
          return this.m_owner.m_offlineGameObject;
        }
        set
        {
          this.m_owner.m_offlineGameObject = value;
        }
      }

      public RectTransform m_offlineMarginTransform
      {
        get
        {
          return this.m_owner.m_offlineMarginTransform;
        }
        set
        {
          this.m_owner.m_offlineMarginTransform = value;
        }
      }

      public Toggle m_offlineClashToggle
      {
        get
        {
          return this.m_owner.m_offlineClashToggle;
        }
        set
        {
          this.m_owner.m_offlineClashToggle = value;
        }
      }

      public Toggle m_offlineBattleReportToggle
      {
        get
        {
          return this.m_owner.m_offlineBattleReportToggle;
        }
        set
        {
          this.m_owner.m_offlineBattleReportToggle = value;
        }
      }

      public Toggle m_offlineRankingToggle
      {
        get
        {
          return this.m_owner.m_offlineRankingToggle;
        }
        set
        {
          this.m_owner.m_offlineRankingToggle = value;
        }
      }

      public GameObject m_offlineClashPanelGameObject
      {
        get
        {
          return this.m_owner.m_offlineClashPanelGameObject;
        }
        set
        {
          this.m_owner.m_offlineClashPanelGameObject = value;
        }
      }

      public GameObject m_offlineArenaOpponentsGameObject
      {
        get
        {
          return this.m_owner.m_offlineArenaOpponentsGameObject;
        }
        set
        {
          this.m_owner.m_offlineArenaOpponentsGameObject = value;
        }
      }

      public GameObject m_offlineInSettleGameObject
      {
        get
        {
          return this.m_owner.m_offlineInSettleGameObject;
        }
        set
        {
          this.m_owner.m_offlineInSettleGameObject = value;
        }
      }

      public Text m_offlineArenaOpponentsRefreshTimeText
      {
        get
        {
          return this.m_owner.m_offlineArenaOpponentsRefreshTimeText;
        }
        set
        {
          this.m_owner.m_offlineArenaOpponentsRefreshTimeText = value;
        }
      }

      public GameObject m_offlineBattleReportPanelGameObject
      {
        get
        {
          return this.m_owner.m_offlineBattleReportPanelGameObject;
        }
        set
        {
          this.m_owner.m_offlineBattleReportPanelGameObject = value;
        }
      }

      public ScrollRect m_offlineBattleReportScrollRect
      {
        get
        {
          return this.m_owner.m_offlineBattleReportScrollRect;
        }
        set
        {
          this.m_owner.m_offlineBattleReportScrollRect = value;
        }
      }

      public GameObject m_offlineNoBattleReportGameObject
      {
        get
        {
          return this.m_owner.m_offlineNoBattleReportGameObject;
        }
        set
        {
          this.m_owner.m_offlineNoBattleReportGameObject = value;
        }
      }

      public GameObject m_offlineRankingPanelGameObject
      {
        get
        {
          return this.m_owner.m_offlineRankingPanelGameObject;
        }
        set
        {
          this.m_owner.m_offlineRankingPanelGameObject = value;
        }
      }

      public ScrollRect m_offlineRankingScrollRect
      {
        get
        {
          return this.m_owner.m_offlineRankingScrollRect;
        }
        set
        {
          this.m_owner.m_offlineRankingScrollRect = value;
        }
      }

      public Text m_offlineRankingPlayerNameText
      {
        get
        {
          return this.m_owner.m_offlineRankingPlayerNameText;
        }
        set
        {
          this.m_owner.m_offlineRankingPlayerNameText = value;
        }
      }

      public CommonUIStateController m_offlineRankingPlayerRankingUIStateController
      {
        get
        {
          return this.m_owner.m_offlineRankingPlayerRankingUIStateController;
        }
        set
        {
          this.m_owner.m_offlineRankingPlayerRankingUIStateController = value;
        }
      }

      public Text m_offlineRankingPlayerRankingText
      {
        get
        {
          return this.m_owner.m_offlineRankingPlayerRankingText;
        }
        set
        {
          this.m_owner.m_offlineRankingPlayerRankingText = value;
        }
      }

      public Image m_offlineRankingPlayerRankingImage
      {
        get
        {
          return this.m_owner.m_offlineRankingPlayerRankingImage;
        }
        set
        {
          this.m_owner.m_offlineRankingPlayerRankingImage = value;
        }
      }

      public Text m_offlineRankingArenaPointsText
      {
        get
        {
          return this.m_owner.m_offlineRankingArenaPointsText;
        }
        set
        {
          this.m_owner.m_offlineRankingArenaPointsText = value;
        }
      }

      public Image m_offlineRankingPlayerIconImage
      {
        get
        {
          return this.m_owner.m_offlineRankingPlayerIconImage;
        }
        set
        {
          this.m_owner.m_offlineRankingPlayerIconImage = value;
        }
      }

      public Transform m_offlineRankingPlayerHeadFrameTransform
      {
        get
        {
          return this.m_owner.m_offlineRankingPlayerHeadFrameTransform;
        }
        set
        {
          this.m_owner.m_offlineRankingPlayerHeadFrameTransform = value;
        }
      }

      public Text m_offlineRankingPlayerLevelText
      {
        get
        {
          return this.m_owner.m_offlineRankingPlayerLevelText;
        }
        set
        {
          this.m_owner.m_offlineRankingPlayerLevelText = value;
        }
      }

      public Image m_offlinePlayerIconImage
      {
        get
        {
          return this.m_owner.m_offlinePlayerIconImage;
        }
        set
        {
          this.m_owner.m_offlinePlayerIconImage = value;
        }
      }

      public Text m_offlinePlayerNameText
      {
        get
        {
          return this.m_owner.m_offlinePlayerNameText;
        }
        set
        {
          this.m_owner.m_offlinePlayerNameText = value;
        }
      }

      public Text m_offlinePlayerLevelText
      {
        get
        {
          return this.m_owner.m_offlinePlayerLevelText;
        }
        set
        {
          this.m_owner.m_offlinePlayerLevelText = value;
        }
      }

      public Text m_offlinePlayerBattlePowerText
      {
        get
        {
          return this.m_owner.m_offlinePlayerBattlePowerText;
        }
        set
        {
          this.m_owner.m_offlinePlayerBattlePowerText = value;
        }
      }

      public Text m_offlineArenaPointsText
      {
        get
        {
          return this.m_owner.m_offlineArenaPointsText;
        }
        set
        {
          this.m_owner.m_offlineArenaPointsText = value;
        }
      }

      public Text m_offlineArenaPointsUpText1
      {
        get
        {
          return this.m_owner.m_offlineArenaPointsUpText1;
        }
        set
        {
          this.m_owner.m_offlineArenaPointsUpText1 = value;
        }
      }

      public Text m_offlineArenaPointsUpText2
      {
        get
        {
          return this.m_owner.m_offlineArenaPointsUpText2;
        }
        set
        {
          this.m_owner.m_offlineArenaPointsUpText2 = value;
        }
      }

      public Image m_offlineArenaPointsUpBarImage
      {
        get
        {
          return this.m_owner.m_offlineArenaPointsUpBarImage;
        }
        set
        {
          this.m_owner.m_offlineArenaPointsUpBarImage = value;
        }
      }

      public Button m_offlineDefendButton
      {
        get
        {
          return this.m_owner.m_offlineDefendButton;
        }
        set
        {
          this.m_owner.m_offlineDefendButton = value;
        }
      }

      public Button m_offlineArenaPointRewardButton
      {
        get
        {
          return this.m_owner.m_offlineArenaPointRewardButton;
        }
        set
        {
          this.m_owner.m_offlineArenaPointRewardButton = value;
        }
      }

      public Text m_offlineVictoryPointsText
      {
        get
        {
          return this.m_owner.m_offlineVictoryPointsText;
        }
        set
        {
          this.m_owner.m_offlineVictoryPointsText = value;
        }
      }

      public GameObject m_offlineVictoryPointsRewardButton1GameObject
      {
        get
        {
          return this.m_owner.m_offlineVictoryPointsRewardButton1GameObject;
        }
        set
        {
          this.m_owner.m_offlineVictoryPointsRewardButton1GameObject = value;
        }
      }

      public GameObject m_offlineVictoryPointsRewardButton2GameObject
      {
        get
        {
          return this.m_owner.m_offlineVictoryPointsRewardButton2GameObject;
        }
        set
        {
          this.m_owner.m_offlineVictoryPointsRewardButton2GameObject = value;
        }
      }

      public GameObject m_offlineVictoryPointsRewardButton3GameObject
      {
        get
        {
          return this.m_owner.m_offlineVictoryPointsRewardButton3GameObject;
        }
        set
        {
          this.m_owner.m_offlineVictoryPointsRewardButton3GameObject = value;
        }
      }

      public Image m_offlineVictoryPointsRewardImage
      {
        get
        {
          return this.m_owner.m_offlineVictoryPointsRewardImage;
        }
        set
        {
          this.m_owner.m_offlineVictoryPointsRewardImage = value;
        }
      }

      public Text m_offlineWeekWinText
      {
        get
        {
          return this.m_owner.m_offlineWeekWinText;
        }
        set
        {
          this.m_owner.m_offlineWeekWinText = value;
        }
      }

      public GameObjectPool<ArenaOpponentListItemUIController> m_arenaOpponentPool
      {
        get
        {
          return this.m_owner.m_arenaOpponentPool;
        }
        set
        {
          this.m_owner.m_arenaOpponentPool = value;
        }
      }

      public GameObjectPool<ArenaRankingListItemUIController> m_arenaRankingPool
      {
        get
        {
          return this.m_owner.m_arenaRankingPool;
        }
        set
        {
          this.m_owner.m_arenaRankingPool = value;
        }
      }

      public GameObjectPool<OfflineArenaBattleReportListItemUIController> m_offlineArenaBattleReportPool
      {
        get
        {
          return this.m_owner.m_offlineArenaBattleReportPool;
        }
        set
        {
          this.m_owner.m_offlineArenaBattleReportPool = value;
        }
      }

      public GameObjectPool<ArenaPointRewardListItemUIController> m_offlineArenaPointRewardPool
      {
        get
        {
          return this.m_owner.m_offlineArenaPointRewardPool;
        }
        set
        {
          this.m_owner.m_offlineArenaPointRewardPool = value;
        }
      }

      public GameObject m_onlineGameObject
      {
        get
        {
          return this.m_owner.m_onlineGameObject;
        }
        set
        {
          this.m_owner.m_onlineGameObject = value;
        }
      }

      public RectTransform m_onlineMarginTransform
      {
        get
        {
          return this.m_owner.m_onlineMarginTransform;
        }
        set
        {
          this.m_owner.m_onlineMarginTransform = value;
        }
      }

      public Toggle m_onlineClashToggle
      {
        get
        {
          return this.m_owner.m_onlineClashToggle;
        }
        set
        {
          this.m_owner.m_onlineClashToggle = value;
        }
      }

      public Toggle m_onlineBattleReportToggle
      {
        get
        {
          return this.m_owner.m_onlineBattleReportToggle;
        }
        set
        {
          this.m_owner.m_onlineBattleReportToggle = value;
        }
      }

      public Toggle m_onlineDanToggle
      {
        get
        {
          return this.m_owner.m_onlineDanToggle;
        }
        set
        {
          this.m_owner.m_onlineDanToggle = value;
        }
      }

      public Toggle m_onlineLocalRankingToggle
      {
        get
        {
          return this.m_owner.m_onlineLocalRankingToggle;
        }
        set
        {
          this.m_owner.m_onlineLocalRankingToggle = value;
        }
      }

      public Toggle m_onlineGlobalRankingToggle
      {
        get
        {
          return this.m_owner.m_onlineGlobalRankingToggle;
        }
        set
        {
          this.m_owner.m_onlineGlobalRankingToggle = value;
        }
      }

      public GameObject m_onlineClashPanelGameObject
      {
        get
        {
          return this.m_owner.m_onlineClashPanelGameObject;
        }
        set
        {
          this.m_owner.m_onlineClashPanelGameObject = value;
        }
      }

      public CommonUIStateController m_onlineClashLadderModeUIStateController
      {
        get
        {
          return this.m_owner.m_onlineClashLadderModeUIStateController;
        }
        set
        {
          this.m_owner.m_onlineClashLadderModeUIStateController = value;
        }
      }

      public Text m_onlineClashWinRateText
      {
        get
        {
          return this.m_owner.m_onlineClashWinRateText;
        }
        set
        {
          this.m_owner.m_onlineClashWinRateText = value;
        }
      }

      public Text m_onlineClashWinCountText
      {
        get
        {
          return this.m_owner.m_onlineClashWinCountText;
        }
        set
        {
          this.m_owner.m_onlineClashWinCountText = value;
        }
      }

      public Text m_onlineClashLoseCountText
      {
        get
        {
          return this.m_owner.m_onlineClashLoseCountText;
        }
        set
        {
          this.m_owner.m_onlineClashLoseCountText = value;
        }
      }

      public Text m_onlineClashTotalCountText
      {
        get
        {
          return this.m_owner.m_onlineClashTotalCountText;
        }
        set
        {
          this.m_owner.m_onlineClashTotalCountText = value;
        }
      }

      public Image m_onlineClashDanIconImage
      {
        get
        {
          return this.m_owner.m_onlineClashDanIconImage;
        }
        set
        {
          this.m_owner.m_onlineClashDanIconImage = value;
        }
      }

      public Image m_onlineClashPromoteDanIconImage
      {
        get
        {
          return this.m_owner.m_onlineClashPromoteDanIconImage;
        }
        set
        {
          this.m_owner.m_onlineClashPromoteDanIconImage = value;
        }
      }

      public Image m_onlineClashPromoteNextDanIconImage
      {
        get
        {
          return this.m_owner.m_onlineClashPromoteNextDanIconImage;
        }
        set
        {
          this.m_owner.m_onlineClashPromoteNextDanIconImage = value;
        }
      }

      public Image m_onlineClashPromoteCountImage
      {
        get
        {
          return this.m_owner.m_onlineClashPromoteCountImage;
        }
        set
        {
          this.m_owner.m_onlineClashPromoteCountImage = value;
        }
      }

      public GameObject m_onlineClashPromoteRoundGroupGameObject
      {
        get
        {
          return this.m_owner.m_onlineClashPromoteRoundGroupGameObject;
        }
        set
        {
          this.m_owner.m_onlineClashPromoteRoundGroupGameObject = value;
        }
      }

      public GameObject m_onlineClashLadderOpenTimeGameObject
      {
        get
        {
          return this.m_owner.m_onlineClashLadderOpenTimeGameObject;
        }
        set
        {
          this.m_owner.m_onlineClashLadderOpenTimeGameObject = value;
        }
      }

      public Text m_onlineClashLadderOpenTimeText
      {
        get
        {
          return this.m_owner.m_onlineClashLadderOpenTimeText;
        }
        set
        {
          this.m_owner.m_onlineClashLadderOpenTimeText = value;
        }
      }

      public Button m_onlineClashLadderChallengeButton
      {
        get
        {
          return this.m_owner.m_onlineClashLadderChallengeButton;
        }
        set
        {
          this.m_owner.m_onlineClashLadderChallengeButton = value;
        }
      }

      public CommonUIStateController m_onlineClashLadderChallengeButtonUIStateController
      {
        get
        {
          return this.m_owner.m_onlineClashLadderChallengeButtonUIStateController;
        }
        set
        {
          this.m_owner.m_onlineClashLadderChallengeButtonUIStateController = value;
        }
      }

      public Button m_onlineClashCasualChallengeButton
      {
        get
        {
          return this.m_owner.m_onlineClashCasualChallengeButton;
        }
        set
        {
          this.m_owner.m_onlineClashCasualChallengeButton = value;
        }
      }

      public GameObject m_onlineBattleReportPanelGameObject
      {
        get
        {
          return this.m_owner.m_onlineBattleReportPanelGameObject;
        }
        set
        {
          this.m_owner.m_onlineBattleReportPanelGameObject = value;
        }
      }

      public ScrollRect m_onlineBattleReportScrollRect
      {
        get
        {
          return this.m_owner.m_onlineBattleReportScrollRect;
        }
        set
        {
          this.m_owner.m_onlineBattleReportScrollRect = value;
        }
      }

      public GameObject m_onlineNoBattleReportGameObject
      {
        get
        {
          return this.m_owner.m_onlineNoBattleReportGameObject;
        }
        set
        {
          this.m_owner.m_onlineNoBattleReportGameObject = value;
        }
      }

      public GameObject m_onlineDanPanelGameObject
      {
        get
        {
          return this.m_owner.m_onlineDanPanelGameObject;
        }
        set
        {
          this.m_owner.m_onlineDanPanelGameObject = value;
        }
      }

      public ScrollRect m_onlineArenaLevelScrollRect
      {
        get
        {
          return this.m_owner.m_onlineArenaLevelScrollRect;
        }
        set
        {
          this.m_owner.m_onlineArenaLevelScrollRect = value;
        }
      }

      public GameObject m_onlineRankingPanelGameObject
      {
        get
        {
          return this.m_owner.m_onlineRankingPanelGameObject;
        }
        set
        {
          this.m_owner.m_onlineRankingPanelGameObject = value;
        }
      }

      public ScrollRect m_onlineRankingScrollRect
      {
        get
        {
          return this.m_owner.m_onlineRankingScrollRect;
        }
        set
        {
          this.m_owner.m_onlineRankingScrollRect = value;
        }
      }

      public Text m_onlineRankingPlayerNameText
      {
        get
        {
          return this.m_owner.m_onlineRankingPlayerNameText;
        }
        set
        {
          this.m_owner.m_onlineRankingPlayerNameText = value;
        }
      }

      public CommonUIStateController m_onlineRankingPlayerRankingUIStateController
      {
        get
        {
          return this.m_owner.m_onlineRankingPlayerRankingUIStateController;
        }
        set
        {
          this.m_owner.m_onlineRankingPlayerRankingUIStateController = value;
        }
      }

      public Text m_onlineRankingPlayerRankingText
      {
        get
        {
          return this.m_owner.m_onlineRankingPlayerRankingText;
        }
        set
        {
          this.m_owner.m_onlineRankingPlayerRankingText = value;
        }
      }

      public Image m_onlineRankingPlayerRankingImage
      {
        get
        {
          return this.m_owner.m_onlineRankingPlayerRankingImage;
        }
        set
        {
          this.m_owner.m_onlineRankingPlayerRankingImage = value;
        }
      }

      public Text m_onlineRankingArenaPointsText
      {
        get
        {
          return this.m_owner.m_onlineRankingArenaPointsText;
        }
        set
        {
          this.m_owner.m_onlineRankingArenaPointsText = value;
        }
      }

      public Image m_onlineRankingPlayerIconImage
      {
        get
        {
          return this.m_owner.m_onlineRankingPlayerIconImage;
        }
        set
        {
          this.m_owner.m_onlineRankingPlayerIconImage = value;
        }
      }

      public Transform m_onlineRankingPlayerHeadFrameTransform
      {
        get
        {
          return this.m_owner.m_onlineRankingPlayerHeadFrameTransform;
        }
        set
        {
          this.m_owner.m_onlineRankingPlayerHeadFrameTransform = value;
        }
      }

      public Text m_onlineRankingPlayerLevelText
      {
        get
        {
          return this.m_owner.m_onlineRankingPlayerLevelText;
        }
        set
        {
          this.m_owner.m_onlineRankingPlayerLevelText = value;
        }
      }

      public Image m_onlinePlayerIconImage
      {
        get
        {
          return this.m_owner.m_onlinePlayerIconImage;
        }
        set
        {
          this.m_owner.m_onlinePlayerIconImage = value;
        }
      }

      public Text m_onlinePlayerNameText
      {
        get
        {
          return this.m_owner.m_onlinePlayerNameText;
        }
        set
        {
          this.m_owner.m_onlinePlayerNameText = value;
        }
      }

      public Text m_onlineDanText
      {
        get
        {
          return this.m_owner.m_onlineDanText;
        }
        set
        {
          this.m_owner.m_onlineDanText = value;
        }
      }

      public Image m_onlineDanIconImage
      {
        get
        {
          return this.m_owner.m_onlineDanIconImage;
        }
        set
        {
          this.m_owner.m_onlineDanIconImage = value;
        }
      }

      public Text m_onlinePlayerLevelText
      {
        get
        {
          return this.m_owner.m_onlinePlayerLevelText;
        }
        set
        {
          this.m_owner.m_onlinePlayerLevelText = value;
        }
      }

      public Text m_onlineArenaPointsText
      {
        get
        {
          return this.m_owner.m_onlineArenaPointsText;
        }
        set
        {
          this.m_owner.m_onlineArenaPointsText = value;
        }
      }

      public Text m_onlineArenaPointsUpText1
      {
        get
        {
          return this.m_owner.m_onlineArenaPointsUpText1;
        }
        set
        {
          this.m_owner.m_onlineArenaPointsUpText1 = value;
        }
      }

      public Text m_onlineArenaPointsUpText2
      {
        get
        {
          return this.m_owner.m_onlineArenaPointsUpText2;
        }
        set
        {
          this.m_owner.m_onlineArenaPointsUpText2 = value;
        }
      }

      public Image m_onlineArenaPointsUpBarImage
      {
        get
        {
          return this.m_owner.m_onlineArenaPointsUpBarImage;
        }
        set
        {
          this.m_owner.m_onlineArenaPointsUpBarImage = value;
        }
      }

      public Button m_onlineArenaRankingRewardButton
      {
        get
        {
          return this.m_owner.m_onlineArenaRankingRewardButton;
        }
        set
        {
          this.m_owner.m_onlineArenaRankingRewardButton = value;
        }
      }

      public Text m_onlineWeekWinText
      {
        get
        {
          return this.m_owner.m_onlineWeekWinText;
        }
        set
        {
          this.m_owner.m_onlineWeekWinText = value;
        }
      }

      public GameObject m_onlineWeekWinRewardButton1GameObject
      {
        get
        {
          return this.m_owner.m_onlineWeekWinRewardButton1GameObject;
        }
        set
        {
          this.m_owner.m_onlineWeekWinRewardButton1GameObject = value;
        }
      }

      public GameObject m_onlineWeekWinRewardButton2GameObject
      {
        get
        {
          return this.m_owner.m_onlineWeekWinRewardButton2GameObject;
        }
        set
        {
          this.m_owner.m_onlineWeekWinRewardButton2GameObject = value;
        }
      }

      public GameObject m_onlineWeekWinRewardButton3GameObject
      {
        get
        {
          return this.m_owner.m_onlineWeekWinRewardButton3GameObject;
        }
        set
        {
          this.m_owner.m_onlineWeekWinRewardButton3GameObject = value;
        }
      }

      public Image m_onlineWeekWinRewardImage
      {
        get
        {
          return this.m_owner.m_onlineWeekWinRewardImage;
        }
        set
        {
          this.m_owner.m_onlineWeekWinRewardImage = value;
        }
      }

      public CommonUIStateController m_promoteBattleUIStateController
      {
        get
        {
          return this.m_owner.m_promoteBattleUIStateController;
        }
        set
        {
          this.m_owner.m_promoteBattleUIStateController = value;
        }
      }

      public Button m_promoteBattleBGButton
      {
        get
        {
          return this.m_owner.m_promoteBattleBGButton;
        }
        set
        {
          this.m_owner.m_promoteBattleBGButton = value;
        }
      }

      public Text m_promoteBattleText
      {
        get
        {
          return this.m_owner.m_promoteBattleText;
        }
        set
        {
          this.m_owner.m_promoteBattleText = value;
        }
      }

      public Button m_promoteBattleConfirmButton
      {
        get
        {
          return this.m_owner.m_promoteBattleConfirmButton;
        }
        set
        {
          this.m_owner.m_promoteBattleConfirmButton = value;
        }
      }

      public CommonUIStateController m_promoteSucceedUIStateController
      {
        get
        {
          return this.m_owner.m_promoteSucceedUIStateController;
        }
        set
        {
          this.m_owner.m_promoteSucceedUIStateController = value;
        }
      }

      public Button m_promoteSucceedBGButton
      {
        get
        {
          return this.m_owner.m_promoteSucceedBGButton;
        }
        set
        {
          this.m_owner.m_promoteSucceedBGButton = value;
        }
      }

      public Image m_promoteSucceedDanImage
      {
        get
        {
          return this.m_owner.m_promoteSucceedDanImage;
        }
        set
        {
          this.m_owner.m_promoteSucceedDanImage = value;
        }
      }

      public Text m_promoteSucceedDanText
      {
        get
        {
          return this.m_owner.m_promoteSucceedDanText;
        }
        set
        {
          this.m_owner.m_promoteSucceedDanText = value;
        }
      }

      public Button m_promoteSucceedConfirmButton
      {
        get
        {
          return this.m_owner.m_promoteSucceedConfirmButton;
        }
        set
        {
          this.m_owner.m_promoteSucceedConfirmButton = value;
        }
      }

      public CommonUIStateController m_matchingFailedUIStateController
      {
        get
        {
          return this.m_owner.m_matchingFailedUIStateController;
        }
        set
        {
          this.m_owner.m_matchingFailedUIStateController = value;
        }
      }

      public Button m_matchingFailedBGButton
      {
        get
        {
          return this.m_owner.m_matchingFailedBGButton;
        }
        set
        {
          this.m_owner.m_matchingFailedBGButton = value;
        }
      }

      public Button m_matchingFailedConfirmButton
      {
        get
        {
          return this.m_owner.m_matchingFailedConfirmButton;
        }
        set
        {
          this.m_owner.m_matchingFailedConfirmButton = value;
        }
      }

      public CommonUIStateController m_matchingNowUIStateController
      {
        get
        {
          return this.m_owner.m_matchingNowUIStateController;
        }
        set
        {
          this.m_owner.m_matchingNowUIStateController = value;
        }
      }

      public Text m_matchingNowTimeText
      {
        get
        {
          return this.m_owner.m_matchingNowTimeText;
        }
        set
        {
          this.m_owner.m_matchingNowTimeText = value;
        }
      }

      public GameObject m_matchingNowPredictTimeGameObject
      {
        get
        {
          return this.m_owner.m_matchingNowPredictTimeGameObject;
        }
        set
        {
          this.m_owner.m_matchingNowPredictTimeGameObject = value;
        }
      }

      public Text m_matchingNowPredictTimeText
      {
        get
        {
          return this.m_owner.m_matchingNowPredictTimeText;
        }
        set
        {
          this.m_owner.m_matchingNowPredictTimeText = value;
        }
      }

      public Button m_matchingNowCancelButton
      {
        get
        {
          return this.m_owner.m_matchingNowCancelButton;
        }
        set
        {
          this.m_owner.m_matchingNowCancelButton = value;
        }
      }

      public GameObjectPool<ArenaRankingListItemUIController> m_onlineArenaRankingPool
      {
        get
        {
          return this.m_owner.m_onlineArenaRankingPool;
        }
        set
        {
          this.m_owner.m_onlineArenaRankingPool = value;
        }
      }

      public GameObjectPool<OnlineArenaBattleReportListItemUIController> m_onlineArenaBattleReportPool
      {
        get
        {
          return this.m_owner.m_onlineArenaBattleReportPool;
        }
        set
        {
          this.m_owner.m_onlineArenaBattleReportPool = value;
        }
      }

      public GameObjectPool<ArenaRankingRewardListItemUIController> m_onlineArenaRankingRewardPool
      {
        get
        {
          return this.m_owner.m_onlineArenaRankingRewardPool;
        }
        set
        {
          this.m_owner.m_onlineArenaRankingRewardPool = value;
        }
      }

      public bool m_isMatchingNow
      {
        get
        {
          return this.m_owner.m_isMatchingNow;
        }
        set
        {
          this.m_owner.m_isMatchingNow = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ShowArenaTicketDesc()
      {
        this.m_owner.ShowArenaTicketDesc();
      }

      public void CloseArenaTicketDesc()
      {
        this.m_owner.CloseArenaTicketDesc();
      }

      public UISpineGraphic CreateSpineGraphic(
        ConfigDataJobConnectionInfo jobConnectionInfo,
        ConfigDataModelSkinResourceInfo skinResInfo)
      {
        return this.m_owner.CreateSpineGraphic(jobConnectionInfo, skinResInfo);
      }

      public void AddRewardPreviewGoods(List<Goods> goods)
      {
        this.m_owner.AddRewardPreviewGoods(goods);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnRewardPreviewBackgroundButtonClick()
      {
        this.m_owner.OnRewardPreviewBackgroundButtonClick();
      }

      public void OnAddArenaTicketButtonClick()
      {
        this.m_owner.OnAddArenaTicketButtonClick();
      }

      public void OnBuyArenaTicketBackgroundButtonClick()
      {
        this.m_owner.OnBuyArenaTicketBackgroundButtonClick();
      }

      public void OnBuyArenaTicketBuyButtonClick()
      {
        this.m_owner.OnBuyArenaTicketBuyButtonClick();
      }

      public void OnSwitchButtonClick()
      {
        this.m_owner.OnSwitchButtonClick();
      }

      public void OfflineOnBindFiledsCompleted()
      {
        this.m_owner.OfflineOnBindFiledsCompleted();
      }

      public void ShowAutoBattleTips()
      {
        this.m_owner.ShowAutoBattleTips();
      }

      public void HideAutoBattleTips()
      {
        this.m_owner.HideAutoBattleTips();
      }

      public void HideArenaOpponent()
      {
        this.m_owner.HideArenaOpponent();
      }

      public void ClearOpponentGraphcs()
      {
        this.m_owner.ClearOpponentGraphcs();
      }

      public void OnOfflineClashToggle(bool on)
      {
        this.m_owner.OnOfflineClashToggle(on);
      }

      public void OnOfflineBattleReportToggle(bool on)
      {
        this.m_owner.OnOfflineBattleReportToggle(on);
      }

      public void OnOfflineRankingToggle(bool on)
      {
        this.m_owner.OnOfflineRankingToggle(on);
      }

      public void OnOfflineArenaPointRewardButtonClick()
      {
        this.m_owner.OnOfflineArenaPointRewardButtonClick();
      }

      public void OnOfflineArenaPointRewardCloseButtonClick()
      {
        this.m_owner.OnOfflineArenaPointRewardCloseButtonClick();
      }

      public void OnOfflineVictoryPointsRewardButtonClick(GainRewardButton b)
      {
        this.m_owner.OnOfflineVictoryPointsRewardButtonClick(b);
      }

      public void OnOfflineArenaOpponentAttackButtonClick(ArenaOpponentListItemUIController ctrl)
      {
        this.m_owner.OnOfflineArenaOpponentAttackButtonClick(ctrl);
      }

      public void OnOfflineBattleReportRevengeButtonClick(
        OfflineArenaBattleReportListItemUIController ctrl)
      {
        this.m_owner.OnOfflineBattleReportRevengeButtonClick(ctrl);
      }

      public void OnOfflineBattleReportReplayButtonClick(
        OfflineArenaBattleReportListItemUIController ctrl)
      {
        this.m_owner.OnOfflineBattleReportReplayButtonClick(ctrl);
      }

      public void OnDefendButtonClick()
      {
        this.m_owner.OnDefendButtonClick();
      }

      public void OnAttackConfirmButtonClick()
      {
        this.m_owner.OnAttackConfirmButtonClick();
      }

      public void OnAutoBattleButtonClick()
      {
        this.m_owner.OnAutoBattleButtonClick();
      }

      public void OnAttackBackgroundButtonClick()
      {
        this.m_owner.OnAttackBackgroundButtonClick();
      }

      public void OnAutoBattleTipsConfirmButtonClick()
      {
        this.m_owner.OnAutoBattleTipsConfirmButtonClick();
      }

      public void OnAutoBattleTipsCancelButtonClick()
      {
        this.m_owner.OnAutoBattleTipsCancelButtonClick();
      }

      public void OnlineOnBindFiledsCompleted()
      {
        this.m_owner.OnlineOnBindFiledsCompleted();
      }

      public void OnOnlineClashToggle(bool on)
      {
        this.m_owner.OnOnlineClashToggle(on);
      }

      public void OnOnlineBattleReportToggle(bool on)
      {
        this.m_owner.OnOnlineBattleReportToggle(on);
      }

      public void OnOnlineDanToggle(bool on)
      {
        this.m_owner.OnOnlineDanToggle(on);
      }

      public void OnOnlineLocalRankingToggle(bool on)
      {
        this.m_owner.OnOnlineLocalRankingToggle(on);
      }

      public void OnOnlineGlobalRankingToggle(bool on)
      {
        this.m_owner.OnOnlineGlobalRankingToggle(on);
      }

      public void OnOnlineWeekWinRewardButtonClick(GainRewardButton b)
      {
        this.m_owner.OnOnlineWeekWinRewardButtonClick(b);
      }

      public void OnOnlineBattleReportReplayButtonClick(
        OnlineArenaBattleReportListItemUIController ctrl)
      {
        this.m_owner.OnOnlineBattleReportReplayButtonClick(ctrl);
      }

      public void OnOnlineClashLadderChallengeButtonClick()
      {
        this.m_owner.OnOnlineClashLadderChallengeButtonClick();
      }

      public void OnOnlineClashCasualChallengeButtonClick()
      {
        this.m_owner.OnOnlineClashCasualChallengeButtonClick();
      }

      public void OnMatchingFailedConfirmButtonClick()
      {
        this.m_owner.OnMatchingFailedConfirmButtonClick();
      }

      public void OnMatchingNowCancelButtonClick()
      {
        this.m_owner.OnMatchingNowCancelButtonClick();
      }

      public void OnOnlineArenaRankingRewardButtonClick()
      {
        this.m_owner.OnOnlineArenaRankingRewardButtonClick();
      }

      public void OnOnlineArenaRankingRewardCloseButtonClick()
      {
        this.m_owner.OnOnlineArenaRankingRewardCloseButtonClick();
      }

      public void OnPromoteBattleConfirmButtonClick()
      {
        this.m_owner.OnPromoteBattleConfirmButtonClick();
      }

      public void OnPromoteSucceedConfirmButtonClick()
      {
        this.m_owner.OnPromoteSucceedConfirmButtonClick();
      }
    }
  }
}
