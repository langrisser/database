﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.BattleRoomRealTimePVPBattleFinishNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "BattleRoomRealTimePVPBattleFinishNtf")]
  [Serializable]
  public class BattleRoomRealTimePVPBattleFinishNtf : IExtensible
  {
    private ulong _WinSessionId;
    private int _Mode;
    private int _DanDiff;
    private int _LocalRankDiff;
    private int _GlobalRankDiff;
    private bool _IsPromotion;
    private ProRealTimePVPBattleReport _Report;
    private ulong _BattleInstanceId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomRealTimePVPBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WinSessionId")]
    public ulong WinSessionId
    {
      get
      {
        return this._WinSessionId;
      }
      set
      {
        this._WinSessionId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Mode")]
    public int Mode
    {
      get
      {
        return this._Mode;
      }
      set
      {
        this._Mode = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanDiff")]
    public int DanDiff
    {
      get
      {
        return this._DanDiff;
      }
      set
      {
        this._DanDiff = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LocalRankDiff")]
    public int LocalRankDiff
    {
      get
      {
        return this._LocalRankDiff;
      }
      set
      {
        this._LocalRankDiff = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GlobalRankDiff")]
    public int GlobalRankDiff
    {
      get
      {
        return this._GlobalRankDiff;
      }
      set
      {
        this._GlobalRankDiff = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsPromotion")]
    public bool IsPromotion
    {
      get
      {
        return this._IsPromotion;
      }
      set
      {
        this._IsPromotion = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Report")]
    public ProRealTimePVPBattleReport Report
    {
      get
      {
        return this._Report;
      }
      set
      {
        this._Report = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleInstanceId")]
    public ulong BattleInstanceId
    {
      get
      {
        return this._BattleInstanceId;
      }
      set
      {
        this._BattleInstanceId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
