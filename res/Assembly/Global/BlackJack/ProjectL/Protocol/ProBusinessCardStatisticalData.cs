﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBusinessCardStatisticalData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProBusinessCardStatisticalData")]
  [Serializable]
  public class ProBusinessCardStatisticalData : IExtensible
  {
    private int _MostSkilledHeroId;
    private int _HeroTotalPower;
    private int _AchievementMissionNums;
    private int _MasterJobNums;
    private int _RiftAchievementNums;
    private int _ChooseLevelAchievementNums;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBusinessCardStatisticalData()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MostSkilledHeroId")]
    public int MostSkilledHeroId
    {
      get
      {
        return this._MostSkilledHeroId;
      }
      set
      {
        this._MostSkilledHeroId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroTotalPower")]
    public int HeroTotalPower
    {
      get
      {
        return this._HeroTotalPower;
      }
      set
      {
        this._HeroTotalPower = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AchievementMissionNums")]
    public int AchievementMissionNums
    {
      get
      {
        return this._AchievementMissionNums;
      }
      set
      {
        this._AchievementMissionNums = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MasterJobNums")]
    public int MasterJobNums
    {
      get
      {
        return this._MasterJobNums;
      }
      set
      {
        this._MasterJobNums = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RiftAchievementNums")]
    public int RiftAchievementNums
    {
      get
      {
        return this._RiftAchievementNums;
      }
      set
      {
        this._RiftAchievementNums = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChooseLevelAchievementNums")]
    public int ChooseLevelAchievementNums
    {
      get
      {
        return this._ChooseLevelAchievementNums;
      }
      set
      {
        this._ChooseLevelAchievementNums = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
