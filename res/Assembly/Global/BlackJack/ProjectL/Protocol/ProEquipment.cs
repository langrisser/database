﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProEquipment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProEquipment")]
  [Serializable]
  public class ProEquipment : IExtensible
  {
    private int _Level;
    private int _Exp;
    private int _StarLevel;
    private bool _Locked;
    private readonly List<ProCommonBattleProperty> _EnchantProperties;
    private int _ResonanceId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProEquipment()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Exp")]
    public int Exp
    {
      get
      {
        return this._Exp;
      }
      set
      {
        this._Exp = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StarLevel")]
    public int StarLevel
    {
      get
      {
        return this._StarLevel;
      }
      set
      {
        this._StarLevel = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Locked")]
    public bool Locked
    {
      get
      {
        return this._Locked;
      }
      set
      {
        this._Locked = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "EnchantProperties")]
    public List<ProCommonBattleProperty> EnchantProperties
    {
      get
      {
        return this._EnchantProperties;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ResonanceId")]
    public int ResonanceId
    {
      get
      {
        return this._ResonanceId;
      }
      set
      {
        this._ResonanceId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
