﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.BattleRoomDataChangeNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "BattleRoomDataChangeNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class BattleRoomDataChangeNtf : IExtensible
  {
    private ulong _RoomId;
    private int _BattleId;
    private int _BPTurn;
    private int _BPStatus;
    private int _BPRule;
    private long _LastestTurnChangeTime;
    private int _Mode;
    private ulong _RealtimePVPBattleInstanceId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomDataChangeNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomId")]
    public ulong RoomId
    {
      get
      {
        return this._RoomId;
      }
      set
      {
        this._RoomId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleId")]
    public int BattleId
    {
      get
      {
        return this._BattleId;
      }
      set
      {
        this._BattleId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BPTurn")]
    public int BPTurn
    {
      get
      {
        return this._BPTurn;
      }
      set
      {
        this._BPTurn = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BPStatus")]
    public int BPStatus
    {
      get
      {
        return this._BPStatus;
      }
      set
      {
        this._BPStatus = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BPRule")]
    public int BPRule
    {
      get
      {
        return this._BPRule;
      }
      set
      {
        this._BPRule = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LastestTurnChangeTime")]
    public long LastestTurnChangeTime
    {
      get
      {
        return this._LastestTurnChangeTime;
      }
      set
      {
        this._LastestTurnChangeTime = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Mode")]
    public int Mode
    {
      get
      {
        return this._Mode;
      }
      set
      {
        this._Mode = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "RealtimePVPBattleInstanceId")]
    [DefaultValue(0.0f)]
    public ulong RealtimePVPBattleInstanceId
    {
      get
      {
        return this._RealtimePVPBattleInstanceId;
      }
      set
      {
        this._RealtimePVPBattleInstanceId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
