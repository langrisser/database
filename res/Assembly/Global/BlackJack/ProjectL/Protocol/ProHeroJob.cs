﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProHeroJob
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProHeroJob")]
  [Serializable]
  public class ProHeroJob : IExtensible
  {
    private int _JobRelatedId;
    private int _JobLevel;
    private int _Mast;
    private readonly List<int> _Achievements;
    private int _ModelSkinId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHeroJob()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "JobRelatedId")]
    public int JobRelatedId
    {
      get
      {
        return this._JobRelatedId;
      }
      set
      {
        this._JobRelatedId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "JobLevel")]
    public int JobLevel
    {
      get
      {
        return this._JobLevel;
      }
      set
      {
        this._JobLevel = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Mast")]
    public int Mast
    {
      get
      {
        return this._Mast;
      }
      set
      {
        this._Mast = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "Achievements")]
    public List<int> Achievements
    {
      get
      {
        return this._Achievements;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ModelSkinId")]
    public int ModelSkinId
    {
      get
      {
        return this._ModelSkinId;
      }
      set
      {
        this._ModelSkinId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
