﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProSpecificDaysLoginOperationalActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProSpecificDaysLoginOperationalActivity")]
  [Serializable]
  public class ProSpecificDaysLoginOperationalActivity : IExtensible
  {
    private ProOperationalActivityBasicInfo _BasicInfo;
    private readonly List<long> _SpecificLoginTimes;
    private readonly List<int> _GainRewardIndexs;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProSpecificDaysLoginOperationalActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "BasicInfo")]
    public ProOperationalActivityBasicInfo BasicInfo
    {
      get
      {
        return this._BasicInfo;
      }
      set
      {
        this._BasicInfo = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "SpecificLoginTimes")]
    public List<long> SpecificLoginTimes
    {
      get
      {
        return this._SpecificLoginTimes;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "GainRewardIndexs")]
    public List<int> GainRewardIndexs
    {
      get
      {
        return this._GainRewardIndexs;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
