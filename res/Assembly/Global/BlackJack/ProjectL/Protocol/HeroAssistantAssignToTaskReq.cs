﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.HeroAssistantAssignToTaskReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "HeroAssistantAssignToTaskReq")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class HeroAssistantAssignToTaskReq : IExtensible
  {
    private int _TaskId;
    private int _Slot;
    private readonly List<int> _HeroIds;
    private int _WorkSeconds;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAssistantAssignToTaskReq()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TaskId")]
    public int TaskId
    {
      get
      {
        return this._TaskId;
      }
      set
      {
        this._TaskId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Slot")]
    public int Slot
    {
      get
      {
        return this._Slot;
      }
      set
      {
        this._Slot = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "HeroIds")]
    public List<int> HeroIds
    {
      get
      {
        return this._HeroIds;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WorkSeconds")]
    public int WorkSeconds
    {
      get
      {
        return this._WorkSeconds;
      }
      set
      {
        this._WorkSeconds = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
