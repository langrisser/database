﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSBattleNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "DSBattleNtf")]
  [Serializable]
  public class DSBattleNtf : IExtensible
  {
    private uint _Version;
    private readonly List<ProTeam> _Teams;
    private ProBattleProcessing _ProcessingBattleInfo;
    private readonly List<int> _GotBattleTreasureIds;
    private int _ArenaBattleStatus;
    private int _ArenaBattleId;
    private int _ArenaBattleRandomSeed;
    private ulong _BattleRoomId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSBattleNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Teams")]
    public List<ProTeam> Teams
    {
      get
      {
        return this._Teams;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "ProcessingBattleInfo")]
    public ProBattleProcessing ProcessingBattleInfo
    {
      get
      {
        return this._ProcessingBattleInfo;
      }
      set
      {
        this._ProcessingBattleInfo = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "GotBattleTreasureIds")]
    public List<int> GotBattleTreasureIds
    {
      get
      {
        return this._GotBattleTreasureIds;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ArenaBattleStatus")]
    public int ArenaBattleStatus
    {
      get
      {
        return this._ArenaBattleStatus;
      }
      set
      {
        this._ArenaBattleStatus = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ArenaBattleId")]
    [DefaultValue(0)]
    public int ArenaBattleId
    {
      get
      {
        return this._ArenaBattleId;
      }
      set
      {
        this._ArenaBattleId = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ArenaBattleRandomSeed")]
    [DefaultValue(0)]
    public int ArenaBattleRandomSeed
    {
      get
      {
        return this._ArenaBattleRandomSeed;
      }
      set
      {
        this._ArenaBattleRandomSeed = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleRoomId")]
    public ulong BattleRoomId
    {
      get
      {
        return this._BattleRoomId;
      }
      set
      {
        this._BattleRoomId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
