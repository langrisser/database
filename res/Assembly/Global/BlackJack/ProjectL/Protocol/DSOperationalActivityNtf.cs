﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSOperationalActivityNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "DSOperationalActivityNtf")]
  [Serializable]
  public class DSOperationalActivityNtf : IExtensible
  {
    private uint _Version;
    private readonly List<ProPlayerLevelUpOperationalActivity> _PlayerLevelUpAwardOperationalActivities;
    private readonly List<ProSpecificDaysLoginOperationalActivity> _SpecificDaysLoginAwardOperationalActivities;
    private readonly List<ProAccumulateLoginOperationalActivity> _AccumulateLoginAwardOperationalActivities;
    private readonly List<ProLimitedTimeExchangeOperationActivity> _LimitedTimeExchangeOperationActivities;
    private readonly List<ProEffectOperationActivity> _EffectOperationActivities;
    private readonly List<ProAccumulateRechargeOperationalActivity> _AccumulateRechargeOperationalActivities;
    private readonly List<ProAccumulateConsumeCrystalOperationalActivity> _AccumulateConsumeCrystalOperationalActivitites;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSOperationalActivityNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "PlayerLevelUpAwardOperationalActivities")]
    public List<ProPlayerLevelUpOperationalActivity> PlayerLevelUpAwardOperationalActivities
    {
      get
      {
        return this._PlayerLevelUpAwardOperationalActivities;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "SpecificDaysLoginAwardOperationalActivities")]
    public List<ProSpecificDaysLoginOperationalActivity> SpecificDaysLoginAwardOperationalActivities
    {
      get
      {
        return this._SpecificDaysLoginAwardOperationalActivities;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "AccumulateLoginAwardOperationalActivities")]
    public List<ProAccumulateLoginOperationalActivity> AccumulateLoginAwardOperationalActivities
    {
      get
      {
        return this._AccumulateLoginAwardOperationalActivities;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "LimitedTimeExchangeOperationActivities")]
    public List<ProLimitedTimeExchangeOperationActivity> LimitedTimeExchangeOperationActivities
    {
      get
      {
        return this._LimitedTimeExchangeOperationActivities;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "EffectOperationActivities")]
    public List<ProEffectOperationActivity> EffectOperationActivities
    {
      get
      {
        return this._EffectOperationActivities;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "AccumulateRechargeOperationalActivities")]
    public List<ProAccumulateRechargeOperationalActivity> AccumulateRechargeOperationalActivities
    {
      get
      {
        return this._AccumulateRechargeOperationalActivities;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "AccumulateConsumeCrystalOperationalActivitites")]
    public List<ProAccumulateConsumeCrystalOperationalActivity> AccumulateConsumeCrystalOperationalActivitites
    {
      get
      {
        return this._AccumulateConsumeCrystalOperationalActivitites;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
