﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.FriendSocialRelationUpdateNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "FriendSocialRelationUpdateNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class FriendSocialRelationUpdateNtf : IExtensible
  {
    private int _Flag;
    private readonly List<ProUserSummary> _Friends;
    private readonly List<ProUserSummary> _Blacklist;
    private readonly List<ProUserSummary> _Invite;
    private readonly List<ProUserSummary> _Invited;
    private readonly List<ProUserSummary> _RecentContactsChat;
    private readonly List<ProUserSummary> _RecentContactsTeamBattle;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendSocialRelationUpdateNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Flag")]
    public int Flag
    {
      get
      {
        return this._Flag;
      }
      set
      {
        this._Flag = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Friends")]
    public List<ProUserSummary> Friends
    {
      get
      {
        return this._Friends;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Blacklist")]
    public List<ProUserSummary> Blacklist
    {
      get
      {
        return this._Blacklist;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "Invite")]
    public List<ProUserSummary> Invite
    {
      get
      {
        return this._Invite;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "Invited")]
    public List<ProUserSummary> Invited
    {
      get
      {
        return this._Invited;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "RecentContactsChat")]
    public List<ProUserSummary> RecentContactsChat
    {
      get
      {
        return this._RecentContactsChat;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "RecentContactsTeamBattle")]
    public List<ProUserSummary> RecentContactsTeamBattle
    {
      get
      {
        return this._RecentContactsTeamBattle;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
