﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSLevelNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "DSLevelNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class DSLevelNtf : IExtensible
  {
    private uint _Version;
    private int _CurrentWayPointId;
    private int _ScenarioId;
    private readonly List<int> _ArrivedWayPointIds;
    private readonly List<ProRandomEvent> _RandomEvents;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSLevelNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrentWayPointId")]
    public int CurrentWayPointId
    {
      get
      {
        return this._CurrentWayPointId;
      }
      set
      {
        this._CurrentWayPointId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScenarioId")]
    public int ScenarioId
    {
      get
      {
        return this._ScenarioId;
      }
      set
      {
        this._ScenarioId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "ArrivedWayPointIds")]
    public List<int> ArrivedWayPointIds
    {
      get
      {
        return this._ArrivedWayPointIds;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "RandomEvents")]
    public List<ProRandomEvent> RandomEvents
    {
      get
      {
        return this._RandomEvents;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
