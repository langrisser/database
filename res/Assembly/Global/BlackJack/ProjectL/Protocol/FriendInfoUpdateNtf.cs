﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.FriendInfoUpdateNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "FriendInfoUpdateNtf")]
  [Serializable]
  public class FriendInfoUpdateNtf : IExtensible
  {
    private int _Likes;
    private string _FriendshipPointsReceived;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendInfoUpdateNtf()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Likes")]
    public int Likes
    {
      get
      {
        return this._Likes;
      }
      set
      {
        this._Likes = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "FriendshipPointsReceived")]
    public string FriendshipPointsReceived
    {
      get
      {
        return this._FriendshipPointsReceived;
      }
      set
      {
        this._FriendshipPointsReceived = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
