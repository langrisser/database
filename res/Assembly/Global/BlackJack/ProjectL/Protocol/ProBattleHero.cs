﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBattleHero
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProBattleHero")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProBattleHero : IExtensible
  {
    private int _HeroId;
    private int _ActionPositionIndex;
    private int _ActionValue;
    private int _Level;
    private int _StarLevel;
    private int _ActiveHeroJobRelatedId;
    private readonly List<ProBattleHeroJob> _Jobs;
    private readonly List<int> _SelectedSkillList;
    private int _SelectedSoldierId;
    private readonly List<ProBattleHeroEquipment> _Equipments;
    private readonly List<ProHeroFetter> _Fetters;
    private int _Power;
    private int _ModelSkinId;
    private int _SelectedSoldierSkinId;
    private int _CharSkinId;
    private int _HeartFetterLevel;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroId")]
    public int HeroId
    {
      get
      {
        return this._HeroId;
      }
      set
      {
        this._HeroId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActionPositionIndex")]
    public int ActionPositionIndex
    {
      get
      {
        return this._ActionPositionIndex;
      }
      set
      {
        this._ActionPositionIndex = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActionValue")]
    public int ActionValue
    {
      get
      {
        return this._ActionValue;
      }
      set
      {
        this._ActionValue = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StarLevel")]
    public int StarLevel
    {
      get
      {
        return this._StarLevel;
      }
      set
      {
        this._StarLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActiveHeroJobRelatedId")]
    public int ActiveHeroJobRelatedId
    {
      get
      {
        return this._ActiveHeroJobRelatedId;
      }
      set
      {
        this._ActiveHeroJobRelatedId = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "Jobs")]
    public List<ProBattleHeroJob> Jobs
    {
      get
      {
        return this._Jobs;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "SelectedSkillList")]
    public List<int> SelectedSkillList
    {
      get
      {
        return this._SelectedSkillList;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SelectedSoldierId")]
    public int SelectedSoldierId
    {
      get
      {
        return this._SelectedSoldierId;
      }
      set
      {
        this._SelectedSoldierId = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "Equipments")]
    public List<ProBattleHeroEquipment> Equipments
    {
      get
      {
        return this._Equipments;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, Name = "Fetters")]
    public List<ProHeroFetter> Fetters
    {
      get
      {
        return this._Fetters;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Power")]
    public int Power
    {
      get
      {
        return this._Power;
      }
      set
      {
        this._Power = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ModelSkinId")]
    public int ModelSkinId
    {
      get
      {
        return this._ModelSkinId;
      }
      set
      {
        this._ModelSkinId = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SelectedSoldierSkinId")]
    public int SelectedSoldierSkinId
    {
      get
      {
        return this._SelectedSoldierSkinId;
      }
      set
      {
        this._SelectedSoldierSkinId = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CharSkinId")]
    public int CharSkinId
    {
      get
      {
        return this._CharSkinId;
      }
      set
      {
        this._CharSkinId = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeartFetterLevel")]
    public int HeartFetterLevel
    {
      get
      {
        return this._HeartFetterLevel;
      }
      set
      {
        this._HeartFetterLevel = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
