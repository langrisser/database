﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ChatMessageNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ChatMessageNtf")]
  [Serializable]
  public class ChatMessageNtf : IExtensible
  {
    private int _ChannelId;
    private ProChatInfo _ChatInfo;
    private ProChatContentText _Content;
    private ProChatContentVoice _VoiceInfo;
    private ProChatVoiceSimpleInfo _VoiceSimpleInfo;
    private ProChatEnterRoomInfo _EnterRoomInfo;
    private string _DestGameUserId;
    private long _SendTime;
    private string _ChatGroupId;
    private bool _JustForSelf;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessageNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChannelId")]
    public int ChannelId
    {
      get
      {
        return this._ChannelId;
      }
      set
      {
        this._ChannelId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "ChatInfo")]
    public ProChatInfo ChatInfo
    {
      get
      {
        return this._ChatInfo;
      }
      set
      {
        this._ChatInfo = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "Content")]
    public ProChatContentText Content
    {
      get
      {
        return this._Content;
      }
      set
      {
        this._Content = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "VoiceInfo")]
    [DefaultValue(null)]
    public ProChatContentVoice VoiceInfo
    {
      get
      {
        return this._VoiceInfo;
      }
      set
      {
        this._VoiceInfo = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "VoiceSimpleInfo")]
    [DefaultValue(null)]
    public ProChatVoiceSimpleInfo VoiceSimpleInfo
    {
      get
      {
        return this._VoiceSimpleInfo;
      }
      set
      {
        this._VoiceSimpleInfo = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "EnterRoomInfo")]
    public ProChatEnterRoomInfo EnterRoomInfo
    {
      get
      {
        return this._EnterRoomInfo;
      }
      set
      {
        this._EnterRoomInfo = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "DestGameUserId")]
    public string DestGameUserId
    {
      get
      {
        return this._DestGameUserId;
      }
      set
      {
        this._DestGameUserId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "SendTime")]
    public long SendTime
    {
      get
      {
        return this._SendTime;
      }
      set
      {
        this._SendTime = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = false, Name = "ChatGroupId")]
    public string ChatGroupId
    {
      get
      {
        return this._ChatGroupId;
      }
      set
      {
        this._ChatGroupId = value;
      }
    }

    [DefaultValue(false)]
    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = false, Name = "JustForSelf")]
    public bool JustForSelf
    {
      get
      {
        return this._JustForSelf;
      }
      set
      {
        this._JustForSelf = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
