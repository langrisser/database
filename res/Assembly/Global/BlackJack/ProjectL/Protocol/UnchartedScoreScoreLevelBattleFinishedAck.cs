﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.UnchartedScoreScoreLevelBattleFinishedAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "UnchartedScoreScoreLevelBattleFinishedAck")]
  [Serializable]
  public class UnchartedScoreScoreLevelBattleFinishedAck : IExtensible
  {
    private int _Result;
    private int _UnchartedScoreId;
    private int _LevelId;
    private bool _Win;
    private ProChangedGoodsNtf _Treasure;
    private readonly List<ProGoods> _NormalRewards;
    private readonly List<ProGoods> _DailyRewards;
    private readonly List<ProGoods> _ScoreRewards;
    private readonly List<int> _BattleTreasures;
    private ProChangedGoodsNtf _Ntf;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedScoreScoreLevelBattleFinishedAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UnchartedScoreId")]
    public int UnchartedScoreId
    {
      get
      {
        return this._UnchartedScoreId;
      }
      set
      {
        this._UnchartedScoreId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LevelId")]
    public int LevelId
    {
      get
      {
        return this._LevelId;
      }
      set
      {
        this._LevelId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Win")]
    public bool Win
    {
      get
      {
        return this._Win;
      }
      set
      {
        this._Win = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "Treasure")]
    [DefaultValue(null)]
    public ProChangedGoodsNtf Treasure
    {
      get
      {
        return this._Treasure;
      }
      set
      {
        this._Treasure = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "NormalRewards")]
    public List<ProGoods> NormalRewards
    {
      get
      {
        return this._NormalRewards;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "DailyRewards")]
    public List<ProGoods> DailyRewards
    {
      get
      {
        return this._DailyRewards;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "ScoreRewards")]
    public List<ProGoods> ScoreRewards
    {
      get
      {
        return this._ScoreRewards;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "BattleTreasures")]
    public List<int> BattleTreasures
    {
      get
      {
        return this._BattleTreasures;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = false, Name = "Ntf")]
    public ProChangedGoodsNtf Ntf
    {
      get
      {
        return this._Ntf;
      }
      set
      {
        this._Ntf = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
