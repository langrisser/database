﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProArenaPlayerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProArenaPlayerInfo")]
  [Serializable]
  public class ProArenaPlayerInfo : IExtensible
  {
    private ProArenaPlayerDefensiveTeam _DefensiveTeam;
    private readonly List<ProArenaOpponent> _Opponents;
    private int _ArenaLevelId;
    private int _ArenaPoints;
    private bool _AttackedOpponent;
    private int _VictoryPoints;
    private long _WeekLastFlushTime;
    private readonly List<int> _ReceivedVictoryPointsRewardIndexs;
    private readonly List<int> _ThisWeekBattleIds;
    private int _ConsecutiveVictoryNums;
    private int _ThisWeekTotalBattleNums;
    private int _ThisWeekVictoryNums;
    private bool _IsAutoBattle;
    private long _NextFlushOpponentTime;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "DefensiveTeam")]
    public ProArenaPlayerDefensiveTeam DefensiveTeam
    {
      get
      {
        return this._DefensiveTeam;
      }
      set
      {
        this._DefensiveTeam = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Opponents")]
    public List<ProArenaOpponent> Opponents
    {
      get
      {
        return this._Opponents;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaLevelId")]
    public int ArenaLevelId
    {
      get
      {
        return this._ArenaLevelId;
      }
      set
      {
        this._ArenaLevelId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaPoints")]
    public int ArenaPoints
    {
      get
      {
        return this._ArenaPoints;
      }
      set
      {
        this._ArenaPoints = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "AttackedOpponent")]
    public bool AttackedOpponent
    {
      get
      {
        return this._AttackedOpponent;
      }
      set
      {
        this._AttackedOpponent = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "VictoryPoints")]
    public int VictoryPoints
    {
      get
      {
        return this._VictoryPoints;
      }
      set
      {
        this._VictoryPoints = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WeekLastFlushTime")]
    public long WeekLastFlushTime
    {
      get
      {
        return this._WeekLastFlushTime;
      }
      set
      {
        this._WeekLastFlushTime = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "ReceivedVictoryPointsRewardIndexs")]
    public List<int> ReceivedVictoryPointsRewardIndexs
    {
      get
      {
        return this._ReceivedVictoryPointsRewardIndexs;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "ThisWeekBattleIds")]
    public List<int> ThisWeekBattleIds
    {
      get
      {
        return this._ThisWeekBattleIds;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConsecutiveVictoryNums")]
    public int ConsecutiveVictoryNums
    {
      get
      {
        return this._ConsecutiveVictoryNums;
      }
      set
      {
        this._ConsecutiveVictoryNums = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ThisWeekTotalBattleNums")]
    public int ThisWeekTotalBattleNums
    {
      get
      {
        return this._ThisWeekTotalBattleNums;
      }
      set
      {
        this._ThisWeekTotalBattleNums = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ThisWeekVictoryNums")]
    public int ThisWeekVictoryNums
    {
      get
      {
        return this._ThisWeekVictoryNums;
      }
      set
      {
        this._ThisWeekVictoryNums = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsAutoBattle")]
    public bool IsAutoBattle
    {
      get
      {
        return this._IsAutoBattle;
      }
      set
      {
        this._IsAutoBattle = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextFlushOpponentTime")]
    public long NextFlushOpponentTime
    {
      get
      {
        return this._NextFlushOpponentTime;
      }
      set
      {
        this._NextFlushOpponentTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
