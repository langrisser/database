﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.PlayerInfoInitReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "PlayerInfoInitReq")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class PlayerInfoInitReq : IExtensible
  {
    private uint _DSBasicInfoVersion;
    private uint _DSBagVersion;
    private uint _DSHeroVersion;
    private uint _DSLevelVersion;
    private uint _DSRiftVersion;
    private uint _DSBattleVersion;
    private uint _DSMailVersion;
    private uint _DSFixedStoreVersion;
    private uint _DSChatVersion;
    private uint _DSSelectCardVersion;
    private uint _DSMissionVersion;
    private uint _DSThearchyTrialVersion;
    private uint _DSAnikiGymVersion;
    private uint _DSTrainingGroundVersion;
    private uint _DSOperationalActivityVersion;
    private uint _ClientAnnouncementVersion;
    private uint _DSRandomStoreVersion;
    private uint _DSArenaVersion;
    private uint _DSSurveyVersion;
    private uint _DSTreasureMapVersion;
    private uint _DSMemoryCorridorVersion;
    private uint _DSHeroDungeonVersion;
    private uint _DSHerotrainningVersion;
    private uint _DSHeroAssistantsVersion;
    private uint _DSFriendVersion;
    private uint _DSHeroPhantomVersion;
    private uint _DSCooperateBattleVersion;
    private uint _DSNoviceVersion;
    private uint _DSRechargeStoreVersion;
    private uint _DSResourceVersion;
    private uint _DSRealTimePVPVersion;
    private uint _DSRaffleVersion;
    private uint _DSRiftStoreVersion;
    private uint _DSUnchartedScoreVersion;
    private uint _DSClimbTowerVersion;
    private uint _DSEternalShrineVersion;
    private uint _DSRefluxVersion;
    private uint _DSCollectionActivityVersion;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerInfoInitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSBasicInfoVersion")]
    public uint DSBasicInfoVersion
    {
      get
      {
        return this._DSBasicInfoVersion;
      }
      set
      {
        this._DSBasicInfoVersion = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSBagVersion")]
    public uint DSBagVersion
    {
      get
      {
        return this._DSBagVersion;
      }
      set
      {
        this._DSBagVersion = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSHeroVersion")]
    public uint DSHeroVersion
    {
      get
      {
        return this._DSHeroVersion;
      }
      set
      {
        this._DSHeroVersion = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSLevelVersion")]
    public uint DSLevelVersion
    {
      get
      {
        return this._DSLevelVersion;
      }
      set
      {
        this._DSLevelVersion = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRiftVersion")]
    public uint DSRiftVersion
    {
      get
      {
        return this._DSRiftVersion;
      }
      set
      {
        this._DSRiftVersion = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSBattleVersion")]
    public uint DSBattleVersion
    {
      get
      {
        return this._DSBattleVersion;
      }
      set
      {
        this._DSBattleVersion = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSMailVersion")]
    public uint DSMailVersion
    {
      get
      {
        return this._DSMailVersion;
      }
      set
      {
        this._DSMailVersion = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSFixedStoreVersion")]
    public uint DSFixedStoreVersion
    {
      get
      {
        return this._DSFixedStoreVersion;
      }
      set
      {
        this._DSFixedStoreVersion = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSChatVersion")]
    public uint DSChatVersion
    {
      get
      {
        return this._DSChatVersion;
      }
      set
      {
        this._DSChatVersion = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSSelectCardVersion")]
    public uint DSSelectCardVersion
    {
      get
      {
        return this._DSSelectCardVersion;
      }
      set
      {
        this._DSSelectCardVersion = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSMissionVersion")]
    public uint DSMissionVersion
    {
      get
      {
        return this._DSMissionVersion;
      }
      set
      {
        this._DSMissionVersion = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSThearchyTrialVersion")]
    public uint DSThearchyTrialVersion
    {
      get
      {
        return this._DSThearchyTrialVersion;
      }
      set
      {
        this._DSThearchyTrialVersion = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSAnikiGymVersion")]
    public uint DSAnikiGymVersion
    {
      get
      {
        return this._DSAnikiGymVersion;
      }
      set
      {
        this._DSAnikiGymVersion = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSTrainingGroundVersion")]
    public uint DSTrainingGroundVersion
    {
      get
      {
        return this._DSTrainingGroundVersion;
      }
      set
      {
        this._DSTrainingGroundVersion = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSOperationalActivityVersion")]
    public uint DSOperationalActivityVersion
    {
      get
      {
        return this._DSOperationalActivityVersion;
      }
      set
      {
        this._DSOperationalActivityVersion = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ClientAnnouncementVersion")]
    public uint ClientAnnouncementVersion
    {
      get
      {
        return this._ClientAnnouncementVersion;
      }
      set
      {
        this._ClientAnnouncementVersion = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRandomStoreVersion")]
    public uint DSRandomStoreVersion
    {
      get
      {
        return this._DSRandomStoreVersion;
      }
      set
      {
        this._DSRandomStoreVersion = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSArenaVersion")]
    public uint DSArenaVersion
    {
      get
      {
        return this._DSArenaVersion;
      }
      set
      {
        this._DSArenaVersion = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSSurveyVersion")]
    public uint DSSurveyVersion
    {
      get
      {
        return this._DSSurveyVersion;
      }
      set
      {
        this._DSSurveyVersion = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSTreasureMapVersion")]
    public uint DSTreasureMapVersion
    {
      get
      {
        return this._DSTreasureMapVersion;
      }
      set
      {
        this._DSTreasureMapVersion = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSMemoryCorridorVersion")]
    public uint DSMemoryCorridorVersion
    {
      get
      {
        return this._DSMemoryCorridorVersion;
      }
      set
      {
        this._DSMemoryCorridorVersion = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSHeroDungeonVersion")]
    public uint DSHeroDungeonVersion
    {
      get
      {
        return this._DSHeroDungeonVersion;
      }
      set
      {
        this._DSHeroDungeonVersion = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSHerotrainningVersion")]
    public uint DSHerotrainningVersion
    {
      get
      {
        return this._DSHerotrainningVersion;
      }
      set
      {
        this._DSHerotrainningVersion = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSHeroAssistantsVersion")]
    public uint DSHeroAssistantsVersion
    {
      get
      {
        return this._DSHeroAssistantsVersion;
      }
      set
      {
        this._DSHeroAssistantsVersion = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSFriendVersion")]
    public uint DSFriendVersion
    {
      get
      {
        return this._DSFriendVersion;
      }
      set
      {
        this._DSFriendVersion = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSHeroPhantomVersion")]
    public uint DSHeroPhantomVersion
    {
      get
      {
        return this._DSHeroPhantomVersion;
      }
      set
      {
        this._DSHeroPhantomVersion = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSCooperateBattleVersion")]
    public uint DSCooperateBattleVersion
    {
      get
      {
        return this._DSCooperateBattleVersion;
      }
      set
      {
        this._DSCooperateBattleVersion = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSNoviceVersion")]
    public uint DSNoviceVersion
    {
      get
      {
        return this._DSNoviceVersion;
      }
      set
      {
        this._DSNoviceVersion = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRechargeStoreVersion")]
    public uint DSRechargeStoreVersion
    {
      get
      {
        return this._DSRechargeStoreVersion;
      }
      set
      {
        this._DSRechargeStoreVersion = value;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSResourceVersion")]
    public uint DSResourceVersion
    {
      get
      {
        return this._DSResourceVersion;
      }
      set
      {
        this._DSResourceVersion = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRealTimePVPVersion")]
    public uint DSRealTimePVPVersion
    {
      get
      {
        return this._DSRealTimePVPVersion;
      }
      set
      {
        this._DSRealTimePVPVersion = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRaffleVersion")]
    public uint DSRaffleVersion
    {
      get
      {
        return this._DSRaffleVersion;
      }
      set
      {
        this._DSRaffleVersion = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRiftStoreVersion")]
    public uint DSRiftStoreVersion
    {
      get
      {
        return this._DSRiftStoreVersion;
      }
      set
      {
        this._DSRiftStoreVersion = value;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSUnchartedScoreVersion")]
    public uint DSUnchartedScoreVersion
    {
      get
      {
        return this._DSUnchartedScoreVersion;
      }
      set
      {
        this._DSUnchartedScoreVersion = value;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSClimbTowerVersion")]
    public uint DSClimbTowerVersion
    {
      get
      {
        return this._DSClimbTowerVersion;
      }
      set
      {
        this._DSClimbTowerVersion = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSEternalShrineVersion")]
    public uint DSEternalShrineVersion
    {
      get
      {
        return this._DSEternalShrineVersion;
      }
      set
      {
        this._DSEternalShrineVersion = value;
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRefluxVersion")]
    public uint DSRefluxVersion
    {
      get
      {
        return this._DSRefluxVersion;
      }
      set
      {
        this._DSRefluxVersion = value;
      }
    }

    [ProtoMember(38, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSCollectionActivityVersion")]
    public uint DSCollectionActivityVersion
    {
      get
      {
        return this._DSCollectionActivityVersion;
      }
      set
      {
        this._DSCollectionActivityVersion = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
