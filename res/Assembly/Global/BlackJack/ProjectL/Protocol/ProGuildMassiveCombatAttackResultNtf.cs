﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProGuildMassiveCombatAttackResultNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProGuildMassiveCombatAttackResultNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProGuildMassiveCombatAttackResultNtf : IExtensible
  {
    private ProChangedGoodsNtf _Ntf;
    private int _Points;
    private readonly List<int> _UsedHeroIds;
    private int _TotalPoints;
    private readonly List<ProGoods> _PointslRewards;
    private int _PerfectWinPoints;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildMassiveCombatAttackResultNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "Ntf")]
    public ProChangedGoodsNtf Ntf
    {
      get
      {
        return this._Ntf;
      }
      set
      {
        this._Ntf = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Points")]
    public int Points
    {
      get
      {
        return this._Points;
      }
      set
      {
        this._Points = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "UsedHeroIds")]
    public List<int> UsedHeroIds
    {
      get
      {
        return this._UsedHeroIds;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TotalPoints")]
    public int TotalPoints
    {
      get
      {
        return this._TotalPoints;
      }
      set
      {
        this._TotalPoints = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "PointslRewards")]
    public List<ProGoods> PointslRewards
    {
      get
      {
        return this._PointslRewards;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PerfectWinPoints")]
    public int PerfectWinPoints
    {
      get
      {
        return this._PerfectWinPoints;
      }
      set
      {
        this._PerfectWinPoints = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
