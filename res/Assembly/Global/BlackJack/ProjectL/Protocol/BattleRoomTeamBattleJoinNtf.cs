﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.BattleRoomTeamBattleJoinNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "BattleRoomTeamBattleJoinNtf")]
  [Serializable]
  public class BattleRoomTeamBattleJoinNtf : IExtensible
  {
    private ulong _RoomId;
    private int _BattleId;
    private readonly List<ProBattleRoomPlayer> _Players;
    private int _RandomSeed;
    private long _ReadyTimeOut;
    private int _ArmyRandomSeed;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomTeamBattleJoinNtf()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomId")]
    public ulong RoomId
    {
      get
      {
        return this._RoomId;
      }
      set
      {
        this._RoomId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleId")]
    public int BattleId
    {
      get
      {
        return this._BattleId;
      }
      set
      {
        this._BattleId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Players")]
    public List<ProBattleRoomPlayer> Players
    {
      get
      {
        return this._Players;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RandomSeed")]
    public int RandomSeed
    {
      get
      {
        return this._RandomSeed;
      }
      set
      {
        this._RandomSeed = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ReadyTimeOut")]
    public long ReadyTimeOut
    {
      get
      {
        return this._ReadyTimeOut;
      }
      set
      {
        this._ReadyTimeOut = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArmyRandomSeed")]
    public int ArmyRandomSeed
    {
      get
      {
        return this._ArmyRandomSeed;
      }
      set
      {
        this._ArmyRandomSeed = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
