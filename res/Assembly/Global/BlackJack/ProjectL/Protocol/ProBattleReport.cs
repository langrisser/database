﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProBattleReport")]
  [Serializable]
  public class ProBattleReport : IExtensible
  {
    private readonly List<ProBattleCommand> _MineCommands;
    private readonly List<ProBattleCommand> _OpponentCommands;
    private byte[] _BattleTeamsSetupLog;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "MineCommands")]
    public List<ProBattleCommand> MineCommands
    {
      get
      {
        return this._MineCommands;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "OpponentCommands")]
    public List<ProBattleCommand> OpponentCommands
    {
      get
      {
        return this._OpponentCommands;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "BattleTeamsSetupLog")]
    [DefaultValue(null)]
    public byte[] BattleTeamsSetupLog
    {
      get
      {
        return this._BattleTeamsSetupLog;
      }
      set
      {
        this._BattleTeamsSetupLog = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
