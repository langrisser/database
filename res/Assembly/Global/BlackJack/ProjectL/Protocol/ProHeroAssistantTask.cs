﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProHeroAssistantTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProHeroAssistantTask")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProHeroAssistantTask : IExtensible
  {
    private int _ConfigId;
    private readonly List<int> _AssignedHeroIds;
    private long _StartTime;
    private long _EndTime;
    private int _Slot;
    private int _CompleteRate;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHeroAssistantTask()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConfigId")]
    public int ConfigId
    {
      get
      {
        return this._ConfigId;
      }
      set
      {
        this._ConfigId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "AssignedHeroIds")]
    public List<int> AssignedHeroIds
    {
      get
      {
        return this._AssignedHeroIds;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StartTime")]
    public long StartTime
    {
      get
      {
        return this._StartTime;
      }
      set
      {
        this._StartTime = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EndTime")]
    public long EndTime
    {
      get
      {
        return this._EndTime;
      }
      set
      {
        this._EndTime = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Slot")]
    public int Slot
    {
      get
      {
        return this._Slot;
      }
      set
      {
        this._Slot = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CompleteRate")]
    public int CompleteRate
    {
      get
      {
        return this._CompleteRate;
      }
      set
      {
        this._CompleteRate = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
