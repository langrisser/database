﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSGiftStoreNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "DSGiftStoreNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class DSGiftStoreNtf : IExtensible
  {
    private uint _Version;
    private readonly List<ProGiftStoreItem> _BoughtItems;
    private readonly List<ProGiftStoreFirstBoughtRecord> _Records;
    private readonly List<ProOrderReward> _OrderRewards;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSGiftStoreNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "BoughtItems")]
    public List<ProGiftStoreItem> BoughtItems
    {
      get
      {
        return this._BoughtItems;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Records")]
    public List<ProGiftStoreFirstBoughtRecord> Records
    {
      get
      {
        return this._Records;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "OrderRewards")]
    public List<ProOrderReward> OrderRewards
    {
      get
      {
        return this._OrderRewards;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
