﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProTeamRoom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProTeamRoom")]
  [Serializable]
  public class ProTeamRoom : IExtensible
  {
    private int _RoomId;
    private int _LeaderPosition;
    private readonly List<ProTeamRoomPlayer> _Players;
    private long _LeaderKickOutTime;
    private ProTeamRoomSetting _Setting;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProTeamRoom()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomId")]
    public int RoomId
    {
      get
      {
        return this._RoomId;
      }
      set
      {
        this._RoomId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LeaderPosition")]
    public int LeaderPosition
    {
      get
      {
        return this._LeaderPosition;
      }
      set
      {
        this._LeaderPosition = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Players")]
    public List<ProTeamRoomPlayer> Players
    {
      get
      {
        return this._Players;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LeaderKickOutTime")]
    public long LeaderKickOutTime
    {
      get
      {
        return this._LeaderKickOutTime;
      }
      set
      {
        this._LeaderKickOutTime = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Setting")]
    public ProTeamRoomSetting Setting
    {
      get
      {
        return this._Setting;
      }
      set
      {
        this._Setting = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
