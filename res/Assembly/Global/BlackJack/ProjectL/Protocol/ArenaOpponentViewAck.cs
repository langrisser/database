﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ArenaOpponentViewAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ArenaOpponentViewAck")]
  [Serializable]
  public class ArenaOpponentViewAck : IExtensible
  {
    private int _Result;
    private int _OpponentIndex;
    private readonly List<ProBattleHero> _Heroes;
    private int _BattlePower;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentViewAck()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OpponentIndex")]
    public int OpponentIndex
    {
      get
      {
        return this._OpponentIndex;
      }
      set
      {
        this._OpponentIndex = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Heroes")]
    public List<ProBattleHero> Heroes
    {
      get
      {
        return this._Heroes;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattlePower")]
    public int BattlePower
    {
      get
      {
        return this._BattlePower;
      }
      set
      {
        this._BattlePower = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
