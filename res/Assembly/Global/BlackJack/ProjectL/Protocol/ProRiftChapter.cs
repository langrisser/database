﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProRiftChapter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProRiftChapter")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProRiftChapter : IExtensible
  {
    private readonly List<ProRiftLevel> _ChapterLevels;
    private readonly List<int> _StarRewardIndexes;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRiftChapter()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, Name = "ChapterLevels")]
    public List<ProRiftLevel> ChapterLevels
    {
      get
      {
        return this._ChapterLevels;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "StarRewardIndexes")]
    public List<int> StarRewardIndexes
    {
      get
      {
        return this._StarRewardIndexes;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
