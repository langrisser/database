﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LocalProcessingBattleData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class LocalProcessingBattleData
  {
    public int Version;
    public int Type;
    public int TypeId;
    public int RandomSeed;
    public int ArmyRandomSeed;
    public int Step;
    public int RegretCount;
    public LocalProcessingBattleData.LocalBattleCommand[] BattleCommands;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [CustomLuaClass]
    public class LocalBattleCommand
    {
      public int Type;
      public int Step;
      public int Actor;
      public int Skill;
      public int PX;
      public int PY;
      public int P2X;
      public int P2Y;

      [MethodImpl((MethodImplOptions) 32768)]
      public BattleCommand ToBattleCommand()
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void FromBattleCommand(BattleCommand cmd)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
