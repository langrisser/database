﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.IMonoBehaviourEventListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

namespace BlackJack.ProjectL.Misc
{
  public interface IMonoBehaviourEventListener
  {
    void OnStart(MonoBehaviourEvent e);

    void OnDestroy(MonoBehaviourEvent e);

    void OnUpdate(MonoBehaviourEvent e);

    void OnLateUpdate(MonoBehaviourEvent e);

    void OnRenderObject(MonoBehaviourEvent e);
  }
}
