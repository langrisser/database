﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LocalProcessingBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class LocalProcessingBattle
  {
    private static LocalProcessingBattle s_instance;
    private static LocalProcessingBattle s_arenaInstance;
    private string m_fileName;
    private LocalProcessingBattleData m_data;

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalProcessingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetFileName(string name)
    {
      this.m_fileName = name;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Save()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Load()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Delete()
    {
      // ISSUE: unable to decompile the method.
    }

    public LocalProcessingBattleData Data
    {
      get
      {
        return this.m_data;
      }
    }

    public static LocalProcessingBattle Instance
    {
      set
      {
        LocalProcessingBattle.s_instance = value;
      }
      get
      {
        return LocalProcessingBattle.s_instance;
      }
    }

    public static LocalProcessingBattle ArenaInstance
    {
      set
      {
        LocalProcessingBattle.s_arenaInstance = value;
      }
      get
      {
        return LocalProcessingBattle.s_arenaInstance;
      }
    }
  }
}
