﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LruAssetCache
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class LruAssetCache
  {
    private int m_maxCount;
    private int m_cacheType;
    private LinkedList<Object> m_assets;

    [MethodImpl((MethodImplOptions) 32768)]
    public LruAssetCache(int cacheType, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetCacheType()
    {
      return this.m_cacheType;
    }

    public int GetMaxCount()
    {
      return this.m_maxCount;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Add(Object a)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Clear()
    {
      this.m_assets.Clear();
    }
  }
}
