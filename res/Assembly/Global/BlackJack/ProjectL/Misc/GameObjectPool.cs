﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.GameObjectPool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class GameObjectPool
  {
    public List<GameObject> m_list;
    private GameObject m_prefab;
    private GameObject m_parent;

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObjectPool()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Setup(GameObject prefab, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject Allocate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deactive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<GameObject> GetList()
    {
      return this.m_list;
    }
  }
}
