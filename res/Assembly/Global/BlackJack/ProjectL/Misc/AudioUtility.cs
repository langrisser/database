﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.AudioUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class AudioUtility
  {
    public const string MusicCategory = "Music_VolumeControl";
    public const string SoundCategory = "SFX_VolumeControl";
    public const string VoiceCategory = "Voice_VolumeControl";
    public const string StopAllMusic = "Action_StopMusic";
    public const string StopAllVoice = "Action_StopVoice";
    public const string AllSoundsCategory = "All_Game_Sounds";

    public void OnPlaySound(OnClickSound e, string name)
    {
      AudioUtility.PlaySound(name);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static float[] NormalizeVolume(float[] voiceData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AudioClip ClipAudioByLength(AudioClip ac, float realRecordLength)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static IAudioPlayback PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<string> GetLanguages()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetLanguage(string language)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetLanguage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StopSound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SearchAndPlaySpineAnimEventSound(
      string spineDataName,
      string animationName,
      string eventName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string SoundIDToName(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PlaySound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PlaySound(AudioClip a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetVolume(string category, float volume, bool isSmooth = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static float GetVolume(string category)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
