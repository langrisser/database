﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.CommonReflection.IComponentBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

namespace BlackJack.ProjectL.CommonReflection
{
  public interface IComponentBase
  {
    string GetName();

    void Init();

    void PostInit();

    void DeInit();

    bool Serialize<T>(T dest);

    void DeSerialize<T>(T source);

    void PostDeSerialize();

    IComponentOwner Owner { get; set; }
  }
}
