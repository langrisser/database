﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.LibClient.StateMachine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.LibClient
{
  [CustomLuaClass]
  public class StateMachine
  {
    public virtual int SetStateCheck(int commingEvent, int newState = -1, bool testOnly = false)
    {
      this.State = newState;
      return this.State;
    }

    public virtual void SetStateWithoutCheck(int newState)
    {
      this.State = newState;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool EventCheck(int commingEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    public int State { get; protected set; }
  }
}
