﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.LibClient.IClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

namespace BlackJack.ProjectL.LibClient
{
  public interface IClient
  {
    bool LoginByAuthToken(
      string serverAddress,
      int serverPort,
      string authToken,
      string clientVersion,
      string clientDeviceId,
      string localization,
      int loginChannelId,
      int bornChannelId,
      string serverDomain);

    bool LoginBySessionToken(
      string sessionToken,
      string clientVersion,
      string localization,
      int loginChannelId,
      int bornChannelId);

    bool Disconnect();

    bool SendMessage(object msg);

    void Tick();

    void Close();

    void BlockProcessMsg(bool isBlock);
  }
}
