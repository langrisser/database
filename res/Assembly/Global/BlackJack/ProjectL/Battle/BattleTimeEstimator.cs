﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleTimeEstimator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattleTimeEstimator
  {
    private int m_time;
    private BattleBase m_battle;
    private int m_moveGridTime;
    private int m_cameraSpeed;
    private GridPosition m_cameraPositionMin;
    private GridPosition m_cameraPositionMax;
    private int m_buffHitStep;
    private int m_dieStep;
    private GridPosition m_cameraPosition;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTimeEstimator()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(BattleBase battle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitCameraPositionBound(ConfigDataBattlefieldInfo battlefieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetTime()
    {
      return this.m_time;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorActive(BattleActor a, bool newStep)
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnBattleActorMove(BattleActor a, GridPosition p, int dir)
    {
      this.AddTime(this.m_moveGridTime);
    }

    public void OnBattleActorPerformMove(
      BattleActor a,
      GridPosition p,
      int dir,
      bool cameraFollow)
    {
      this.AddTime(this.m_moveGridTime);
    }

    public void OnBattleActorPunchMove(BattleActor a, string fxName, bool isDragExchange)
    {
      this.AddTime(this.m_moveGridTime);
    }

    public void OnBattleActorExchangeMove(
      BattleActor a,
      BattleActor b,
      int moveType,
      string fxName)
    {
      this.AddTime(this.m_moveGridTime);
    }

    public void OnBattleActorPlayAnimation(BattleActor a, string animationName, int animationTime)
    {
      this.AddTime(animationTime);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSkill(BattleActor a, ConfigDataSkillInfo skillInfo, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnBattleActorImmune(BattleActor a)
    {
      this.AddTime(500);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPassiveSkill(
      BattleActor a,
      BattleActor target,
      BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorBuffHit(
      BattleActor a,
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTerrainHit(
      BattleActor a,
      ConfigDataTerrainInfo terrainInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTeleport(BattleActor a, ConfigDataSkillInfo skillInfo, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSummon(BattleActor a, ConfigDataSkillInfo skillInfo)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorDie(BattleActor actor, bool isAfterCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnBattleActorAppear(BattleActor a, int effectType, string fxName)
    {
      this.AddTime(500);
    }

    public void OnBattleActorDisappear(BattleActor actor, int effectType, string fxName)
    {
      this.AddTime(500);
    }

    public void OnBattleActorChangeTeam(BattleActor a)
    {
      this.AddTime(500);
    }

    public void OnBattleActorReplace(BattleActor a0, BattleActor a1, string fxName)
    {
      this.AddTime(500);
    }

    public void OnBattleActorCameraFocus(BattleActor a)
    {
      this.AddCameraMoveTime(a.Position);
    }

    public void OnBattleActorGainBattleTreasure(BattleActor a, ConfigDataBattleTreasureInfo t)
    {
      this.AddTime(2000);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartGuard(BattleActor a, BattleActor target)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopGuard(BattleActor a, BattleActor target)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartCombat(BattleActor a, BattleActor b, ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopCombat(
      int teamAHeroTotalDamage,
      int teamASoldierTotalDamage,
      bool teamACriticalAttack,
      int teamBHeroTotalDamage,
      int teamBSoldierTotalDamage,
      bool teamBCriticalAttack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextTeam(int team, bool isNpc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextPlayer(int prevPlayerIndex, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnCameraFocus(GridPosition p)
    {
      this.AddCameraMoveTime(p);
    }

    public void OnWaitTime(int timeInMs)
    {
      this.AddTime(timeInMs);
    }

    private void AddTime(int timeInMs)
    {
      this.m_time += timeInMs;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddCameraMoveTime(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition ClampCameraPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeSkillCastTime(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
