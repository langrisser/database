﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.ActorAnim
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class ActorAnim
  {
    public const string Idle = "idle";
    public const string Walk = "walk";
    public const string Run = "run";
    public const string Attack = "attack1";
    public const string SuperAttack = "superattack";
    public const string Sing = "sing";
    public const string Cast = "cast";
    public const string Hurt = "hurt";
    public const string Stun = "stun";
    public const string Die = "death";
    public const string Die1 = "death1";
    public const string Die2 = "death2";
  }
}
