﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.ServerBattleListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class ServerBattleListener : IBattleListener
  {
    private BattleTimeEstimator m_battleTimeEstimator;

    [MethodImpl((MethodImplOptions) 32768)]
    public ServerBattleListener()
    {
    }

    public void Init(BattleBase battle)
    {
      this.m_battleTimeEstimator.Init(battle);
    }

    public void OnBattleActorCreate(BattleActor a, bool visible)
    {
    }

    public void OnBattleActorCreateEnd(BattleActor a)
    {
    }

    public void OnBattleActorActive(BattleActor a, bool newStep)
    {
      this.m_battleTimeEstimator.OnBattleActorActive(a, newStep);
    }

    public void OnBattleActorActionBegin(BattleActor a)
    {
    }

    public void OnBattleActorActionEnd(BattleActor a)
    {
    }

    public void OnBattleActorMove(BattleActor a, GridPosition p, int dir)
    {
      this.m_battleTimeEstimator.OnBattleActorMove(a, p, dir);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPerformMove(
      BattleActor a,
      GridPosition p,
      int dir,
      bool cameraFollow)
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnBattleActorPunchMove(BattleActor a, string fxName, bool isDragExchange)
    {
      this.m_battleTimeEstimator.OnBattleActorPunchMove(a, fxName, isDragExchange);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorExchangeMove(
      BattleActor a,
      BattleActor b,
      int moveType,
      string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnBattleActorSetDir(BattleActor a, int dir)
    {
    }

    public void OnBattleActorPlayFx(BattleActor a, string fxName, int attachMode)
    {
    }

    public void OnBattleActorPlayAnimation(BattleActor a, string animationName, int animationTime)
    {
      this.m_battleTimeEstimator.OnBattleActorPlayAnimation(a, animationName, animationTime);
    }

    public void OnBattleActorChangeIdleAnimation(BattleActor a, string idleAnimationName)
    {
    }

    public void OnBattleActorSkill(BattleActor a, ConfigDataSkillInfo skillInfo, GridPosition p)
    {
      this.m_battleTimeEstimator.OnBattleActorSkill(a, skillInfo, p);
    }

    public void OnBattleActorSkillHitBegin(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      bool isRebound)
    {
    }

    public void OnBattleActorSkillHit(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType,
      bool isRebound)
    {
    }

    public void OnBattleActorSkillHitEnd(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      bool isRebound)
    {
    }

    public void OnBattleActorAttachBuff(BattleActor a, BuffState buffState)
    {
    }

    public void OnBattleActorDetachBuff(BattleActor a, BuffState buffState)
    {
    }

    public void OnBattleActorImmune(BattleActor a)
    {
      this.m_battleTimeEstimator.OnBattleActorImmune(a);
    }

    public void OnBattleActorPassiveSkill(
      BattleActor a,
      BattleActor target,
      BuffState sourceBuffState)
    {
      this.m_battleTimeEstimator.OnBattleActorPassiveSkill(a, target, sourceBuffState);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorBuffHit(
      BattleActor a,
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTerrainHit(
      BattleActor a,
      ConfigDataTerrainInfo terrainInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
    }

    public void OnBattleActorTeleport(BattleActor a, ConfigDataSkillInfo skillInfo, GridPosition p)
    {
      this.m_battleTimeEstimator.OnBattleActorTeleport(a, skillInfo, p);
    }

    public void OnBattleActorSummon(BattleActor a, ConfigDataSkillInfo skillInfo)
    {
      this.m_battleTimeEstimator.OnBattleActorSummon(a, skillInfo);
    }

    public void OnBattleActorDie(BattleActor a, bool isAfterCombat)
    {
      this.m_battleTimeEstimator.OnBattleActorDie(a, isAfterCombat);
    }

    public void OnBattleActorAppear(BattleActor a, int effectType, string fxName)
    {
      this.m_battleTimeEstimator.OnBattleActorAppear(a, effectType, fxName);
    }

    public void OnBattleActorDisappear(BattleActor a, int effectType, string fxName)
    {
      this.m_battleTimeEstimator.OnBattleActorDisappear(a, effectType, fxName);
    }

    public void OnBattleActorChangeTeam(BattleActor a)
    {
      this.m_battleTimeEstimator.OnBattleActorChangeTeam(a);
    }

    public void OnBattleActorChangeArmy(BattleActor a)
    {
    }

    public void OnBattleActorReplace(BattleActor a0, BattleActor a1, string fxName)
    {
      this.m_battleTimeEstimator.OnBattleActorReplace(a0, a1, fxName);
    }

    public void OnBattleActorCameraFocus(BattleActor a)
    {
      this.m_battleTimeEstimator.OnBattleActorCameraFocus(a);
    }

    public void OnBattleActorGainBattleTreasure(
      BattleActor a,
      ConfigDataBattleTreasureInfo treasureInfo)
    {
      this.m_battleTimeEstimator.OnBattleActorGainBattleTreasure(a, treasureInfo);
    }

    public void OnStartGuard(BattleActor a, BattleActor target)
    {
      this.m_battleTimeEstimator.OnStartGuard(a, target);
    }

    public void OnStopGuard(BattleActor a, BattleActor target)
    {
      this.m_battleTimeEstimator.OnStopGuard(a, target);
    }

    public void OnBeforeStartCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
    }

    public void OnCancelCombat()
    {
    }

    public void OnStartCombat(BattleActor a, BattleActor b, ConfigDataSkillInfo attackerSkillInfo)
    {
      this.m_battleTimeEstimator.OnStartCombat(a, b, attackerSkillInfo);
    }

    public void OnPreStopCombat()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopCombat(
      int teamAHeroTotalDamage,
      int teamASoldierTotalDamage,
      bool teamACriticalAttack,
      int teamBHeroTotalDamage,
      int teamBSoldierTotalDamage,
      bool teamBCriticalAttack)
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnBattleNextTurn(int turn)
    {
      this.m_battleTimeEstimator.OnBattleNextTurn(turn);
    }

    public void OnBattleNextTeam(int team, bool isNpc)
    {
      this.m_battleTimeEstimator.OnBattleNextTeam(team, isNpc);
    }

    public void OnBattleNextPlayer(int prevPlayerIndex, int playerIndex)
    {
      this.m_battleTimeEstimator.OnBattleNextPlayer(prevPlayerIndex, playerIndex);
    }

    public void OnBattleNextActor(BattleActor actor)
    {
    }

    public void OnCombatActorSkill(CombatActor a, ConfigDataSkillInfo skillInfo)
    {
    }

    public void OnCombatActorHit(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      int totalDamage,
      DamageNumberType damageNumberType)
    {
    }

    public void OnCombatActorDie(CombatActor a)
    {
    }

    public void OnStartSkillCutscene(
      ConfigDataSkillInfo skillInfo,
      ConfigDataCutsceneInfo cutsceneInfo,
      int team)
    {
    }

    public void OnStartPassiveSkillCutscene(BuffState sourceBuffState, int team)
    {
    }

    public void OnStopSkillCutscene()
    {
    }

    public void OnStartBattleDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
    }

    public void OnStartBattlePerform(ConfigDataBattlePerformInfo performInfo)
    {
    }

    public void OnStopBattlePerform()
    {
    }

    public void OnChangeMapTerrain(GridPosition p, ConfigDataTerrainInfo terrainInfo)
    {
    }

    public void OnCameraFocus(GridPosition p)
    {
      this.m_battleTimeEstimator.OnCameraFocus(p);
    }

    public void OnPlayMusic(string musicName)
    {
    }

    public void OnPlaySound(string soundName)
    {
    }

    public void OnPlayFx(string fxName, GridPosition p)
    {
    }

    public void OnWaitTime(int timeInMs)
    {
      this.m_battleTimeEstimator.OnWaitTime(timeInMs);
    }

    public void OnBattleTreasureCreate(ConfigDataBattleTreasureInfo treasureInfo, bool isOpened)
    {
    }

    public IBattleGraphic CreateCombatGraphic(string assetName, float scale)
    {
      return (IBattleGraphic) null;
    }

    public void DestroyCombatGraphic(IBattleGraphic model)
    {
    }

    public IBattleGraphic PlayFx(string assetName, float scale)
    {
      return (IBattleGraphic) null;
    }

    public void PlaySound(string name)
    {
    }

    public void DrawLine(Vector2i p0, Vector2i p1, Colori color)
    {
    }

    public void DrawLine(Vector2i p0, Fix64 z0, Vector2i p1, Fix64 z1, Colori color)
    {
    }

    public void LogBattleStart()
    {
    }

    public void LogBattleStop(bool isWin)
    {
    }

    public void LogBattleTeam(BattleTeam team0, BattleTeam team1)
    {
    }

    public void LogActorMove(BattleActor actor, GridPosition fromPos, GridPosition toPos)
    {
    }

    public void LogActorStandby(BattleActor actor)
    {
    }

    public void LogActorAttack(BattleActor actor, BattleActor targetActor)
    {
    }

    public void LogActorSkill(
      BattleActor actor,
      ConfigDataSkillInfo skillInfo,
      BattleActor targetActor,
      GridPosition targetPos)
    {
    }

    public void LogActorDie(BattleActor actor, BattleActor killerActor)
    {
    }

    public int GetTime()
    {
      return this.m_battleTimeEstimator.GetTime();
    }

    public void ClearTime()
    {
      this.m_battleTimeEstimator.ClearTime();
    }
  }
}
