﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.UtilityTools;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class BattleActor : Entity
  {
    private BattleTeam m_team;
    private GridPosition m_position;
    private int m_direction;
    private GridPosition m_initPosition;
    private GridPosition m_beforeGuardPosition;
    private BattleProperty m_heroBattleProperty;
    private BattleProperty m_soldierBattleProperty;
    private int m_heroHealthPoint;
    private int m_soldierTotalHealthPoint;
    private int m_soldierSingleHealthPointMax;
    private int m_initSoldierCount;
    private int m_heroLevel;
    private int m_heroStar;
    private int m_jobLevel;
    private ConfigDataJobInfo[] m_masterJobInfos;
    private BattleActorEquipment[] m_equipments;
    private ConfigDataSkillInfo[] m_resonanceSkillInfos;
    private ConfigDataSkillInfo[] m_fetterSkillInfos;
    private int m_actionValue;
    private MoveType m_moveType;
    private bool m_isActionFinished;
    private int m_buffNewTurnCount;
    private int m_hasExtraActionMovePoint;
    private int m_hasExtraMovingMovePoint;
    private ExtraActionType m_curExtraActionType;
    private int m_curExtraActionMovePoint;
    private bool m_isVisible;
    private bool m_isCheckedDie;
    private bool m_isPostCheckedDie;
    private bool m_isRetreat;
    private List<BattleSkillState> m_skillStates;
    private List<BuffState> m_buffStates;
    private int m_buffIdCounter;
    private ulong m_buffTypes;
    private uint m_fightTags;
    private BattleActor m_summoner;
    private BattleActor m_killerActor;
    private bool m_isNpc;
    private bool m_isPlayerNpc;
    private BattleActorSourceType m_sourceType;
    private ConfigDataHeroInfo m_heroInfo;
    private ConfigDataJobConnectionInfo m_jobConnectionInfo;
    private ConfigDataJobInfo m_jobInfo;
    private ConfigDataArmyInfo m_heroArmyInfo;
    private ConfigDataSoldierInfo m_soldierInfo;
    private ConfigDataArmyInfo m_soldierArmyInfo;
    private ConfigDataCharImageSkinResourceInfo m_heroCharImageSkinResourceInfo;
    private ConfigDataModelSkinResourceInfo m_heroModelSkinResourceInfo;
    private ConfigDataModelSkinResourceInfo m_soldierModelSkinResourceInfo;
    private ConfigDataCharImageInfo m_heroVoiceCharImageInfo;
    private ConfigDataSkillInfo[] m_extraPassiveSkillInfos;
    private ConfigDataSkillInfo m_extraTalentSkillInfo;
    private bool m_isActionCriticalAttack;
    private bool m_isActionKillActor;
    private bool m_isActionDamageActor;
    private bool m_isBeCriticalAttack;
    private int m_actionMoveGrids;
    private int m_actionRemainMovePoint;
    private ConfigDataSkillInfo m_lastDamageBySkillInfo;
    private int m_firstDamageTurn;
    private bool m_isTurnDamage;
    private ulong m_satisfyConditions;
    private short m_actionCount;
    private short m_combatAttackCount;
    private short m_beCombatAttackCount;
    private short m_useSkillCount;
    private short m_killActorCount;
    private int m_dieTurn;
    private int m_deathAnimType;
    private uint m_executedCommandTypes;
    private ConfigDataSkillInfo m_executedSkillInfo;
    private int m_playerIndex;
    private ConfigDataBehavior m_curBehaviorCfg;
    private BattleActor.BehaviorState m_curBehaviorState;
    private BehaviorTarget m_moveTarget;
    private BehaviorTarget m_attackTarget;
    private int m_attackSkillIndex;
    private int[] m_beAttackedCountOfTurns;
    private int m_groupId;
    private int m_aiCreateBattleCommandCount;
    private int m_aiBeginTurn;
    private const float m_healSkillTargetHPThresh = 0.7f;
    [DoNotToLua]
    private BattleActor.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Dispose_hotfix;
    private LuaFunction m_InitializeBattleTeamConfigDataHeroInfoConfigDataJobConnectionInfoConfigDataSoldierInfoConfigDataSkillInfobeConfigDataJobInfobeBattleActorEquipmentbeConfigDataSkillInfobeConfigDataSkillInfobeInt32Int32Int32Int32GridPositionInt32BooleanInt32Int32Int32BattleActorSourceTypeInt32_hotfix;
    private LuaFunction m_InitializeSkinConfigDataCharImageSkinResourceInfoConfigDataModelSkinResourceInfoConfigDataModelSkinResourceInfo_hotfix;
    private LuaFunction m_InitializeExtraPassiveSkillAndTalentList`1ConfigDataSkillInfo_hotfix;
    private LuaFunction m_InitializeEndBooleanInt32Int32_hotfix;
    private LuaFunction m_UpdateBattleProperties_hotfix;
    private LuaFunction m_UpdateBattlePropertiesInCombatBattleActorBooleanInt32ConfigDataSkillInfo_hotfix;
    private LuaFunction m_ComputeBattlePropertiesBattlePropertyModifier_hotfix;
    private LuaFunction m_CollectJobMasterPropertyModifiersBattlePropertyModifier_hotfix;
    private LuaFunction m_CollectEquipmentPropertyModifiersBattlePropertyModifier_hotfix;
    private LuaFunction m_SetPositionGridPosition_hotfix;
    private LuaFunction m_SetDirectionInt32_hotfix;
    private LuaFunction m_FaceToGridPosition_hotfix;
    private LuaFunction m_GuardMoveGridPosition_hotfix;
    private LuaFunction m_UnguardMove_hotfix;
    private LuaFunction m_MoveToGridPosition_hotfix;
    private LuaFunction m_ClearMapActor_hotfix;
    private LuaFunction m_GetTerrain_hotfix;
    private LuaFunction m_GetBuffEffectedTerrain_hotfix;
    private LuaFunction m_ChangeTeamInt32Boolean_hotfix;
    private LuaFunction m_CreateBattleCommandBattleCommandType_hotfix;
    private LuaFunction m_StartBattle_hotfix;
    private LuaFunction m_StopBattleBoolean_hotfix;
    private LuaFunction m_NextTurn_hotfix;
    private LuaFunction m_ActionBegin_hotfix;
    private LuaFunction m_ActionEnd_hotfix;
    private LuaFunction m_IsActionFinished_hotfix;
    private LuaFunction m_CanAction_hotfix;
    private LuaFunction m_PostActionTerrainDamage_hotfix;
    private LuaFunction m_FindPathGridPositionGridPositionInt32Int32List`1_hotfix;
    private LuaFunction m_FindPathGridPositionGridPositionInt32FindPathIgnoreTypeInt32List`1_hotfix;
    private LuaFunction m_FindMoveRegionGridPositionList`1_hotfix;
    private LuaFunction m_ShouldLog_hotfix;
    private LuaFunction m_AddExecutedCommandTypeBattleCommandType_hotfix;
    private LuaFunction m_HasExecutedCommandTypeBattleCommandType_hotfix;
    private LuaFunction m_IsExecutedCommandTypeBattleCommandType_hotfix;
    private LuaFunction m_ExecuteMoveCommandGridPosition_hotfix;
    private LuaFunction m_ExecutePerformMoveCommandGridPositionBoolean_hotfix;
    private LuaFunction m_ExecuteCombatCommandBattleActorConfigDataSkillInfoBoolean_hotfix;
    private LuaFunction m_ExecuteSkillCommandInt32GridPositionGridPosition_hotfix;
    private LuaFunction m_ExecutePerformSkillCommandConfigDataSkillInfoGridPositionGridPosition_hotfix;
    private LuaFunction m_ExecuteDoneCommand_hotfix;
    private LuaFunction m_IsDead_hotfix;
    private LuaFunction m_IsRetreat_hotfix;
    private LuaFunction m_IsDeadOrRetreat_hotfix;
    private LuaFunction m_SetVisibleBoolean_hotfix;
    private LuaFunction m_IsVisible_hotfix;
    private LuaFunction m_IsInvincible_hotfix;
    private LuaFunction m_IsSummoned_hotfix;
    private LuaFunction m_IsNpc_hotfix;
    private LuaFunction m_IsAINpc_hotfix;
    private LuaFunction m_IsPlayerNpc_hotfix;
    private LuaFunction m_IsPlayerActor_hotfix;
    private LuaFunction m_IsAIActor_hotfix;
    private LuaFunction m_IsEventOrPerformActor_hotfix;
    private LuaFunction m_CanBeTarget_hotfix;
    private LuaFunction m_SetHeroHealthPointInt32_hotfix;
    private LuaFunction m_SetSoldierTotalHealthPointInt32_hotfix;
    private LuaFunction m_CheckDieBattleActor_hotfix;
    private LuaFunction m_PostCheckDie_hotfix;
    private LuaFunction m_RetreatInt32StringBoolean_hotfix;
    private LuaFunction m_GetSoldierAttackDistance_hotfix;
    private LuaFunction m_GetMaxAttackDistance_hotfix;
    private LuaFunction m_GetTalentSkillInfo_hotfix;
    private LuaFunction m_GetSkillDistanceConfigDataSkillInfo_hotfix;
    private LuaFunction m_GetMovePoint_hotfix;
    private LuaFunction m_GetMoveType_hotfix;
    private LuaFunction m_GetTotalHealthPoint_hotfix;
    private LuaFunction m_GetTotalHealthPointMax_hotfix;
    private LuaFunction m_GetTotalHealthPointPercent_hotfix;
    private LuaFunction m_GetSoldierCount_hotfix;
    private LuaFunction m_SetLastDamageConfigDataSkillInfo_hotfix;
    private LuaFunction m_GetLastDamageBySkill_hotfix;
    private LuaFunction m_GetFirstDamageTurn_hotfix;
    private LuaFunction m_IsTurnDamage_hotfix;
    private LuaFunction m_SetSetisfyConditionInt32_hotfix;
    private LuaFunction m_IsSatisfyConditionInt32_hotfix;
    private LuaFunction m_GetDeathAnimType_hotfix;
    private LuaFunction m_SetBeCriticalAttackBoolean_hotfix;
    private LuaFunction m_SetActionCriticalAttack_hotfix;
    private LuaFunction m_SetActionKillActor_hotfix;
    private LuaFunction m_SetActionDamageActor_hotfix;
    private LuaFunction m_IsExtraAction_hotfix;
    private LuaFunction m_IsExtraMoving_hotfix;
    private LuaFunction m_GetActionCount_hotfix;
    private LuaFunction m_IncreaseCombatAttackCount_hotfix;
    private LuaFunction m_GetCombatAttackCount_hotfix;
    private LuaFunction m_IncreaseBeCombatAttackCount_hotfix;
    private LuaFunction m_GetBeCombatAttackCount_hotfix;
    private LuaFunction m_IncreaseUseSkillCount_hotfix;
    private LuaFunction m_GetUseSkillCount_hotfix;
    private LuaFunction m_IncreaseKillActorCount_hotfix;
    private LuaFunction m_GetKillActorCount_hotfix;
    private LuaFunction m_GetKillerActor_hotfix;
    private LuaFunction m_GetDieTurn_hotfix;
    private LuaFunction m_GetSourceType_hotfix;
    private LuaFunction m_GetTrainingTechBattlePlayer_hotfix;
    private LuaFunction m_get_Position_hotfix;
    private LuaFunction m_get_InitPosition_hotfix;
    private LuaFunction m_get_Direction_hotfix;
    private LuaFunction m_get_Battle_hotfix;
    private LuaFunction m_get_Team_hotfix;
    private LuaFunction m_get_TeamNumber_hotfix;
    private LuaFunction m_get_HeroBattleProperty_hotfix;
    private LuaFunction m_get_SoldierBattleProperty_hotfix;
    private LuaFunction m_get_HeroHealthPoint_hotfix;
    private LuaFunction m_get_SoldierTotalHealthPoint_hotfix;
    private LuaFunction m_get_SoldierSingleHealthPointMax_hotfix;
    private LuaFunction m_get_FightTags_hotfix;
    private LuaFunction m_get_HeroLevel_hotfix;
    private LuaFunction m_get_HeroStar_hotfix;
    private LuaFunction m_get_JobLevel_hotfix;
    private LuaFunction m_get_ActionValue_hotfix;
    private LuaFunction m_set_ActionValueInt32_hotfix;
    private LuaFunction m_get_HeroInfo_hotfix;
    private LuaFunction m_get_HeroId_hotfix;
    private LuaFunction m_get_HeroArmyInfo_hotfix;
    private LuaFunction m_get_HeroArmyId_hotfix;
    private LuaFunction m_get_JobConnectionInfo_hotfix;
    private LuaFunction m_get_JobInfo_hotfix;
    private LuaFunction m_get_SoldierInfo_hotfix;
    private LuaFunction m_get_SoldierArmyInfo_hotfix;
    private LuaFunction m_get_HeroCharImageSkinResourceInfo_hotfix;
    private LuaFunction m_get_HeroModelSkinResourceInfo_hotfix;
    private LuaFunction m_get_SoldierModelSkinResourceInfo_hotfix;
    private LuaFunction m_get_HeroVoiceCharImageInfo_hotfix;
    private LuaFunction m_get_PlayerIndex_hotfix;
    private LuaFunction m_FindMoveAndAttackRegionInt32Int32_hotfix;
    private LuaFunction m_FindRandomEmptyPositionInt32Int32GridPosition__hotfix;
    private LuaFunction m_FindAttackPositionInt32Int32GridPositionBooleanBoolean_hotfix;
    private LuaFunction m_ComputeActorScoreBuffBattleActorInt32_hotfix;
    private LuaFunction m_SelectNearestTargetBattleTeam_hotfix;
    private LuaFunction m_GetAIRandomNumber_hotfix;
    private LuaFunction m_GetSkillTargetTeamConfigDataSkillInfo_hotfix;
    private LuaFunction m_CanAttackOrUseSkill_hotfix;
    private LuaFunction m_get_Group_hotfix;
    private LuaFunction m_set_GroupBehaviorGroup_hotfix;
    private LuaFunction m_get_GroupId_hotfix;
    private LuaFunction m_IncreaseBeAttackedCount_hotfix;
    private LuaFunction m_get_IsAttackedByEnemy_hotfix;
    private LuaFunction m_get_IsAttackedByEnemyInLastTrun_hotfix;
    private LuaFunction m_get_InstanceID_hotfix;
    private LuaFunction m_GetBehaviorId_hotfix;
    private LuaFunction m_SetBehaviorInt32_hotfix;
    private LuaFunction m_SetBehaviorStateBehaviorState_hotfix;
    private LuaFunction m_CheckBehaviorConditionBehaviorConditionParamData_hotfix;
    private LuaFunction m_DoBehaviorChangeRules_hotfix;
    private LuaFunction m_get_NextBehaviorByChangeRules_hotfix;
    private LuaFunction m_FindEmptyGridInCanAttackAndTouchRangeGridPositionInt32Int32_hotfix;
    private LuaFunction m_FindActorsInCanAttackAndTouchRangeList`1GridPositionInt32Int32Boolean_hotfix;
    private LuaFunction m_FindFarthestPositionList`1GridPosition_hotfix;
    private LuaFunction m_FindNearestPositionList`1GridPosition_hotfix;
    private LuaFunction m_FindNearestActorList`1GridPosition_hotfix;
    private LuaFunction m_DoSelectTargetSelectTargetParamData_hotfix;
    private LuaFunction m_FindActorsByIDFilterList`1Int32beInt32be_hotfix;
    private LuaFunction m_SelectExtraMoveTarget_hotfix;
    private LuaFunction m_SelectMoveTarget_hotfix;
    private LuaFunction m_GenerateCommandOfMoveGridPosition_hotfix;
    private LuaFunction m_GenerateCommandOfMoveGridPositionGridPosition_hotfix;
    private LuaFunction m_FindPositionToMoveToTargetGridPosition_hotfix;
    private LuaFunction m_FindPositionToMoveToTargetGridPositionBattleActor__hotfix;
    private LuaFunction m_ComputeRestrictScoreBattleActorBattleActor_hotfix;
    private LuaFunction m_GetArmyRistrictScoreArmyTagArmyTag_hotfix;
    private LuaFunction m_get_TotalHPPercent_hotfix;
    private LuaFunction m_DefaultSelectDamageSkillTargetList`1_hotfix;
    private LuaFunction m_IsSelectRangeSkill_hotfix;
    private LuaFunction m_get_BehaviorSelectSkillInfo_hotfix;
    private LuaFunction m_FindActorsInGridsList`1List`1_hotfix;
    private LuaFunction m_FindMaxAoeSkillCoverPositionConfigDataSkillInfoList`1List`1ClassValue`1_hotfix;
    private LuaFunction m_FindMaxAoeSkillCoverActorConfigDataSkillInfoList`1List`1ClassValue`1_hotfix;
    private LuaFunction m_DefaultSelectAttackTarget_hotfix;
    private LuaFunction m_SelectAttackTargetWithSummonSkill_hotfix;
    private LuaFunction m_FindGridsLessEqualDistanceGridPositionInt32_hotfix;
    private LuaFunction m_FindActorsLessEqualDistanceList`1GridPositionInt32_hotfix;
    private LuaFunction m_DefaultSelectHealSkillTargetList`1_hotfix;
    private LuaFunction m_DefaultSelectBuffSkillTargetList`1ConfigDataSkillInfo_hotfix;
    private LuaFunction m_SelectAttackTargetInSkillRange_hotfix;
    private LuaFunction m_FindActorsWithBuffNList`1Int32_hotfix;
    private LuaFunction m_FindActorsWithSkillAITypeNList`1Int32_hotfix;
    private LuaFunction m_SelectAttackTarget_hotfix;
    private LuaFunction m_SelectSkill_hotfix;
    private LuaFunction m_SelectSkillDirectReachTarget_hotfix;
    private LuaFunction m_DefaultSelectSkillInt32beInt32be_hotfix;
    private LuaFunction m_FindActorsInCanNormalAttackAndTouchRangeList`1List`1_hotfix;
    private LuaFunction m_IsSkillAGoodAISelectionConfigDataSkillInfo_hotfix;
    private LuaFunction m_FindCastSkillPositionConfigDataSkillInfoBehaviorTarget_hotfix;
    private LuaFunction m_FindAttackPositionsInt32Int32GridPositionList`1_hotfix;
    private LuaFunction m_GenerateCommandOfAttack_hotfix;
    private LuaFunction m_DoBehaviorMove_hotfix;
    private LuaFunction m_IsAttackTargetStillValid_hotfix;
    private LuaFunction m_DoBehaviorAttack_hotfix;
    private LuaFunction m_GenerateAIBattleCommand_hotfix;
    private LuaFunction m_AICreateBattleCommandBattleCommandType_hotfix;
    private LuaFunction m_SelectAttackRegionTargetBattleTeamComputeActorScoreFuncInt32_hotfix;
    private LuaFunction m_GetBuffStates_hotfix;
    private LuaFunction m_InitializeBuffs_hotfix;
    private LuaFunction m_AttachPassiveSkillBuffsConfigDataSkillInfoBuffSourceType_hotfix;
    private LuaFunction m_AttachBuffConfigDataBuffInfoBattleActorBuffSourceTypeConfigDataSkillInfoBuffState_hotfix;
    private LuaFunction m_RemoveBuffBuffState_hotfix;
    private LuaFunction m_RemoveBuffListList`1_hotfix;
    private LuaFunction m_RemoveBuffListList`1BuffState_hotfix;
    private LuaFunction m_FindBuffInt32BuffState_hotfix;
    private LuaFunction m_RemoveAllBuffs_hotfix;
    private LuaFunction m_SkillDispelBuffConfigDataSkillInfo_hotfix;
    private LuaFunction m_HasBuffInt32_hotfix;
    private LuaFunction m_CollectBuffPropertyModifiersAndFightTagsBattlePropertyModifierUInt32__hotfix;
    private LuaFunction m_CollectBuffPropertyModifiersAndFightTagsBattlePropertyModifierUInt32_BooleanBoolean_hotfix;
    private LuaFunction m_CollectPropertyModifierBattlePropertyModifierPropertyModifyTypeInt32BooleanBoolean_hotfix;
    private LuaFunction m_CollectOtherActorBuffPropertyModifiersAndFightTagsBattlePropertyModifierUInt32__hotfix;
    private LuaFunction m_CollectBuffPropertyModifiersAndFightTagsInCombatBattlePropertyModifierUInt32_BattleActorBooleanInt32ConfigDataSkillInfo_hotfix;
    private LuaFunction m_CollectBuffPropertyExchangeBattlePropertyBattlePropertyModifier_hotfix;
    private LuaFunction m_CollectBuffPropertyReplace_hotfix;
    private LuaFunction m_BuffMoveTypeChange_hotfix;
    private LuaFunction m_UpdateAllAuras_hotfix;
    private LuaFunction m_RemoveAuraAppliedBuffsBuffState_hotfix;
    private LuaFunction m_RemovePackChildBuffsBuffState_hotfix;
    private LuaFunction m_OnBuffHitBuffStateInt32Int32DamageNumberType_hotfix;
    private LuaFunction m_HasFightTagFightTag_hotfix;
    private LuaFunction m_UpdateBuffTypes_hotfix;
    private LuaFunction m_SetBuffTypeBuffTypeBoolean_hotfix;
    private LuaFunction m_HasBuffTypeBuffType_hotfix;
    private LuaFunction m_IsBuffEffectiveBuffState_hotfix;
    private LuaFunction m_IsBuffHpConditionSatisfiedConfigDataBuffInfo_hotfix;
    private LuaFunction m_IsBuffHpConditionSatisfiedInt32Int32Int32_hotfix;
    private LuaFunction m_IsBuffEffectiveConditionSatisfiedConfigDataBuffInfo_hotfix;
    private LuaFunction m_IsBuffCombatConditionSatisfiedBattleActorBooleanInt32Int32Int32Int32ConfigDataSkillInfo_hotfix;
    private LuaFunction m_IsBuffCombatConditionSatisfiedBattleActorBooleanBooleanInt32BooleanList`1ConfigDataSkillInfo_hotfix;
    private LuaFunction m_ComputeNeighborAliveActorCountInt32Int32_hotfix;
    private LuaFunction m_IsBuffCooldownBuffState_hotfix;
    private LuaFunction m_StartBuffCooldownBuffState_hotfix;
    private LuaFunction m_ComputeBuffCountInt32_hotfix;
    private LuaFunction m_ComputeEnhanceOrDebuffCountInt32_hotfix;
    private LuaFunction m_IsImmuneBuffSubTypeInt32_hotfix;
    private LuaFunction m_BuffReboundDamageConfigDataSkillInfoInt32Int32__hotfix;
    private LuaFunction m_BuffHealChangeDamageInt32Int32Int32_Int32_BuffState__hotfix;
    private LuaFunction m_BuffBeHealChangeDamageInt32Int32Int32_Int32_BuffState__hotfix;
    private LuaFunction m_OnBuffHealOrChangeDamageHitBuffStateInt32Int32_hotfix;
    private LuaFunction m_GetBuffOverrideMovePointCost_hotfix;
    private LuaFunction m_GetBuffOverrideTerrain_hotfix;
    private LuaFunction m_AddBuffArmyRelationAttackInt32BooleanArmyRelationData__hotfix;
    private LuaFunction m_AddBuffArmyRelationDefendInt32BooleanArmyRelationData__hotfix;
    private LuaFunction m_AddBuffArmyRelationModifyAttackInt32BooleanArmyRelationData__hotfix;
    private LuaFunction m_AddBuffArmyRelationModifyDefendInt32BooleanArmyRelationData__hotfix;
    private LuaFunction m_IsBuffIgnoreArmyRelationInt32Boolean_hotfix;
    private LuaFunction m_IsBuffForceMagicDamageBoolean_hotfix;
    private LuaFunction m_IsBuffForcePhysicalDamageBoolean_hotfix;
    private LuaFunction m_ActionEndBuffEffect_hotfix;
    private LuaFunction m_UpdateBuffTime_hotfix;
    private LuaFunction m_ActionEndBuffDoubleMoveBooleanBoolean_hotfix;
    private LuaFunction m_ActionEndBuffHealOverTimeBoolean_hotfix;
    private LuaFunction m_ActionEndBuffDamageOverTime_hotfix;
    private LuaFunction m_ActionEndAddBuff_hotfix;
    private LuaFunction m_ActionEndAddBuffSuperBooleanBoolean_hotfix;
    private LuaFunction m_ActionEndRemoveDebuffBoolean_hotfix;
    private LuaFunction m_ActionEndRemoveEnhanceBuffBooleanBooleanBoolean_hotfix;
    private LuaFunction m_ActionEndBuffBattlefieldSkillBooleanBooleanBoolean_hotfix;
    private LuaFunction m_ActionEndBuffNewTurnBooleanBoolean_hotfix;
    private LuaFunction m_CombatBuffHealBattleActorBooleanBooleanInt32BooleanInt32Int32Int32_hotfix;
    private LuaFunction m_CombatBuffHealOtherBattleActorBooleanBooleanInt32BooleanInt32Int32Int32_hotfix;
    private LuaFunction m_CombatBuffDamageBattleActorBooleanBooleanInt32BooleanConfigDataSkillInfo_hotfix;
    private LuaFunction m_CombatApplyBuffBattleActorBooleanBooleanInt32BooleanConfigDataSkillInfo_hotfix;
    private LuaFunction m_AttackApplyBuffList`1_hotfix;
    private LuaFunction m_KillApplyBuffBooleanBoolean_hotfix;
    private LuaFunction m_AttackBuffDamageList`1BooleanBooleanInt32ConfigDataSkillInfo_hotfix;
    private LuaFunction m_AttackRemoveBuffList`1BooleanBooleanInt32_hotfix;
    private LuaFunction m_AttackRemoveSkillCooldownConfigDataSkillInfoBooleanBoolean_hotfix;
    private LuaFunction m_BattleFieldSkillApplyBuffList`1_hotfix;
    private LuaFunction m_AttackBuffPunchBattleActor_hotfix;
    private LuaFunction m_AttackBuffDragBattleActor_hotfix;
    private LuaFunction m_AttackBuffExchangePositionBattleActorBoolean_hotfix;
    private LuaFunction m_CombatBuffArmyChangeBattleActorConfigDataArmyInfoBooleanBooleanInt32Boolean_hotfix;
    private LuaFunction m_DamageBuffHealOther_hotfix;
    private LuaFunction m_BuffDoubleAttack_hotfix;
    private LuaFunction m_BuffDoubleSkill_hotfix;
    private LuaFunction m_BuffUndead_hotfix;
    private LuaFunction m_CanBuffGuardBattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_GetGuardActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_AfterCombatDetachBuff_hotfix;
    private LuaFunction m_GetSkillStateInt32_hotfix;
    private LuaFunction m_GetSkillStateByIdInt32_hotfix;
    private LuaFunction m_GetSkillStates_hotfix;
    private LuaFunction m_IsSkillUseableInt32_hotfix;
    private LuaFunction m_CanUseSkillOnTargetConfigDataSkillInfoBattleActor_hotfix;
    private LuaFunction m_IsBattlefiledSkillApplyTargetConfigDataSkillInfoBattleActor_hotfix;
    private LuaFunction m_FindBattlefieldSkillApplyTargetsConfigDataSkillInfoGridPositionList`1_hotfix;
    private LuaFunction m_ExecuteBattlefieldSkillConfigDataSkillInfoGridPositionGridPosition_hotfix;
    private LuaFunction m_SkillAttackConfigDataSkillInfoBattleActor_hotfix;
    private LuaFunction m_SkillSummonConfigDataSkillInfoGridPosition_hotfix;
    private LuaFunction m_SkillTeleportConfigDataSkillInfoBattleActorGridPosition_hotfix;
    private LuaFunction m_AttackByBattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_OnSkillHitConfigDataSkillInfoInt32Int32DamageNumberTypeBoolean_hotfix;
    private LuaFunction m_SkillAttackEndConfigDataSkillInfoList`1_hotfix;
    private LuaFunction m_CombatByBattleActor_hotfix;
    private LuaFunction m_TeleportByBattleActorConfigDataSkillInfoGridPosition_hotfix;
    private LuaFunction m_SummonByBattleActorConfigDataSkillInfoGridPosition_hotfix;
    private LuaFunction m_GetHeroAttackSkillInfoMoveTypeInt32_hotfix;
    private LuaFunction m_GetSoldierAttackSkillInfoMoveTypeInt32_hotfix;
    private LuaFunction m_GetHeroAttackSkillInfoBoolean_hotfix;
    private LuaFunction m_GetSoldierAttackSkillInfoBoolean_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(
      BattleTeam team,
      ConfigDataHeroInfo heroInfo,
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ConfigDataSoldierInfo soldierInfo,
      ConfigDataSkillInfo[] skillInfos,
      ConfigDataJobInfo[] masterJobInfos,
      BattleActorEquipment[] equipments,
      ConfigDataSkillInfo[] resonanceSkillInfos,
      ConfigDataSkillInfo[] fetterSkillInfos,
      int heroLevel,
      int heroStar,
      int jobLevel,
      int soldierCount,
      GridPosition pos,
      int dir,
      bool isNpc,
      int actionValue,
      int behaviorId,
      int groupId,
      BattleActorSourceType sourceType,
      int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeSkin(
      ConfigDataCharImageSkinResourceInfo heroCharImageSkinInfo,
      ConfigDataModelSkinResourceInfo heroModelSkinInfo,
      ConfigDataModelSkinResourceInfo soldierModelSkinInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeExtraPassiveSkillAndTalent(
      List<ConfigDataSkillInfo> skillInfos,
      ConfigDataSkillInfo talentSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeEnd(bool visible = true, int heroHp = -1, int soldierHp = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateBattleProperties()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateBattlePropertiesInCombat(
      BattleActor other,
      bool isAttacker,
      int distance,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeBattleProperties(BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CollectJobMasterPropertyModifiers(BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CollectEquipmentPropertyModifiers(BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FaceTo(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GuardMove(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnguardMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveTo(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo GetTerrain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo GetBuffEffectedTerrain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeTeam(int team, bool isNpc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleCommand CreateBattleCommand(BattleCommandType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattle(bool win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActionBegin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActionEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsActionFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PostActionTerrainDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(
      GridPosition start,
      GridPosition goal,
      int movePoint,
      int inRegion,
      List<GridPosition> path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(
      GridPosition start,
      GridPosition goal,
      int movePoint,
      FindPathIgnoreType ignoreType,
      int inRegion,
      List<GridPosition> path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindMoveRegion(GridPosition start, List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ShouldLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddExecutedCommandType(BattleCommandType cmdType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasExecutedCommandType(BattleCommandType cmdType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsExecutedCommandType(BattleCommandType cmdType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecuteMoveCommand(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecutePerformMoveCommand(GridPosition p, bool cameraFollow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecuteCombatCommand(
      BattleActor target,
      ConfigDataSkillInfo skillInfo,
      bool isPerform)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecuteSkillCommand(int skillIndex, GridPosition p, GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecutePerformSkillCommand(
      ConfigDataSkillInfo skillInfo,
      GridPosition p,
      GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecuteDoneCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRetreat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDeadOrRetreat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool v)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInvincible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSummoned()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNpc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAINpc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerNpc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAIActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEventOrPerformActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanBeTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroHealthPoint(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSoldierTotalHealthPoint(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckDie(BattleActor attacker)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostCheckDie()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Retreat(int effectType, string fxName, bool notifyListener = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierAttackDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxAttackDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetTalentSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSkillDistance(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMovePoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MoveType GetMoveType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MoveType ComputeMoveType(
      ConfigDataJobInfo heroJobInfo,
      ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MoveType ComputeDefaultMoveType(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MoveType ComputeMoveTypeMin(MoveType m1, MoveType m2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MoveType ComputeMoveTypeMax(MoveType m1, MoveType m2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTotalHealthPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTotalHealthPointMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTotalHealthPointPercent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastDamage(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetLastDamageBySkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFirstDamageTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTurnDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSetisfyCondition(int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSatisfyCondition(int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDeathAnimType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBeCriticalAttack(bool a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActionCriticalAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActionKillActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActionDamageActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExtraAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExtraMoving()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public short GetActionCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IncreaseCombatAttackCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public short GetCombatAttackCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IncreaseBeCombatAttackCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public short GetBeCombatAttackCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IncreaseUseSkillCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public short GetUseSkillCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IncreaseKillActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetKillActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetKillerActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDieTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorSourceType GetSourceType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePlayer GetTrainingTechBattlePlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    public GridPosition Position
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GridPosition InitPosition
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Direction
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleBase Battle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleTeam Team
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TeamNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleProperty HeroBattleProperty
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleProperty SoldierBattleProperty
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroHealthPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SoldierTotalHealthPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SoldierSingleHealthPointMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public uint FightTags
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroStar
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int JobLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ActionValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroInfo HeroInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataArmyInfo HeroArmyInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroArmyId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobInfo JobInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSoldierInfo SoldierInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataArmyInfo SoldierArmyInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataCharImageSkinResourceInfo HeroCharImageSkinResourceInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataModelSkinResourceInfo HeroModelSkinResourceInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataModelSkinResourceInfo SoldierModelSkinResourceInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataCharImageInfo HeroVoiceCharImageInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int PlayerIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FindMoveAndAttackRegion(int distance, int shape)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool FindRandomEmptyPosition(int attackDistance, int shape, ref GridPosition position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindAttackPosition(
      int attackDistance,
      int shape,
      GridPosition targetPos,
      bool checkMoveRegion,
      bool farAway)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComputeActorScoreDamage(BattleActor a, int param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComputeActorScoreHeal(BattleActor a, int param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeActorScoreBuff(BattleActor a, int param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor SelectNearestTarget(BattleTeam team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private RandomNumber GetAIRandomNumber()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleTeam GetSkillTargetTeam(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanAttackOrUseSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    public BehaviorGroup Group
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int GroupId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IncreaseBeAttackedCount()
    {
      // ISSUE: unable to decompile the method.
    }

    private bool IsAttackedByEnemy
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private bool IsAttackedByEnemyInLastTrun
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string InstanceID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBehaviorId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBehavior(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBehaviorState(BattleActor.BehaviorState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckBehaviorCondition(
      BehaviorCondition condition,
      ConfigDataBehavior.ParamData param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoBehaviorChangeRules()
    {
      // ISSUE: unable to decompile the method.
    }

    private int NextBehaviorByChangeRules
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition FindEmptyGridInCanAttackAndTouchRange(
      GridPosition startPoint,
      int attackDistance,
      int shape)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> FindActorsInCanAttackAndTouchRange(
      List<BattleActor> destActors,
      GridPosition startPoint,
      int attackDistance,
      int shape,
      bool excludeSelf = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindFarthestPosition(
      List<GridPosition> positions,
      GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindNearestPosition(
      List<GridPosition> positions,
      GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor FindNearestActor(List<BattleActor> actors, GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BehaviorTarget DoSelectTarget(
      SelectTarget st,
      ConfigDataBehavior.ParamData param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsByIDFilter(
      List<BattleActor> actors,
      int[] priorIDs,
      int[] excludeIDs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool Contains(int[] target, int nr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectExtraMoveTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectMoveTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GenerateCommandOfMove(GridPosition target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GenerateCommandOfMove(GridPosition target, GridPosition target2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindPositionToMoveToTarget(GridPosition target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindPositionToMoveToTarget(
      GridPosition target,
      out BattleActor blockingEnemy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeRestrictScore(BattleActor srcActor, BattleActor destActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetArmyRistrictScore(ArmyTag a, ArmyTag b)
    {
      // ISSUE: unable to decompile the method.
    }

    private float TotalHPPercent
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleActor SelectMinHPPercentActor(List<BattleActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<BattleActor> FindActorsAlive(List<BattleActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor DefaultSelectDamageSkillTarget(List<BattleActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsSelectRangeSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    private ConfigDataSkillInfo BehaviorSelectSkillInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsInGrids(
      List<BattleActor> actors,
      List<GridPosition> grids)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindMaxAoeSkillCoverPosition(
      ConfigDataSkillInfo si,
      List<GridPosition> asCenterPositions,
      List<BattleActor> beCoveredActors,
      ClassValue<int> maxCoverActorsCount = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor FindMaxAoeSkillCoverActor(
      ConfigDataSkillInfo si,
      List<BattleActor> asCenterActors,
      List<BattleActor> beCoveredActors,
      ClassValue<int> maxCoverActorsCount = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DefaultSelectAttackTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectAttackTargetWithSummonSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<GridPosition> FindGridsLessEqualDistance(
      GridPosition startPos,
      int dist)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsLessEqualDistance(
      List<BattleActor> actors,
      GridPosition startPos,
      int dist)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor DefaultSelectHealSkillTarget(List<BattleActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor DefaultSelectBuffSkillTarget(
      List<BattleActor> actors,
      ConfigDataSkillInfo skill)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectAttackTargetInSkillRange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsWithBuffN(
      List<BattleActor> actors,
      int buffID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsWithSkillAITypeN(
      List<BattleActor> actors,
      int paramN)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectAttackTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectSkillDirectReachTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool HasInt(int value, int[] arr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DefaultSelectSkill(int[] includeSkillIDs, int[] excludeSkillIDs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsInCanNormalAttackAndTouchRange(
      List<BattleActor> destActors,
      List<BattleActor> srcActors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsSkillAGoodAISelection(ConfigDataSkillInfo si)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindCastSkillPosition(ConfigDataSkillInfo si, BehaviorTarget t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FindAttackPositions(
      int attackDistance,
      int shape,
      GridPosition targetPos,
      List<GridPosition> positions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GenerateCommandOfAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoBehaviorMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsAttackTargetStillValid()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoBehaviorAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GenerateAIBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleCommand AICreateBattleCommand(BattleCommandType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor SelectAttackRegionTarget(
      BattleTeam team,
      BattleActor.ComputeActorScoreFunc computeScore,
      int computeScoreParam = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BuffState> GetBuffStates()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeBuffs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AttachPassiveSkillBuffs(ConfigDataSkillInfo skillInfo, BuffSourceType sourceType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AttachBuff(
      ConfigDataBuffInfo buffInfo,
      BattleActor applyer,
      BuffSourceType sourceType,
      ConfigDataSkillInfo sourceSkillInfo,
      BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareBuffOrder(BuffState b0, BuffState b1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RemoveBuff(BuffState bs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveBuffList(List<int> buffIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int RemoveBuffList(List<int> buffIds, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BuffState FindBuff(int buffId, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveAllBuffs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SkillDispelBuff(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasBuff(int buffId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBuffPropertyModifiersAndFightTags(
      BattlePropertyModifier pm,
      ref uint fightTags)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CollectBuffPropertyModifiersAndFightTags(
      BattlePropertyModifier pm,
      ref uint fightTags,
      bool collectStatic,
      bool collectDynamic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPropertyModifier(
      BattlePropertyModifier pm,
      PropertyModifyType modifyType,
      int value,
      bool collectStatic,
      bool collectDynamic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectOtherActorBuffPropertyModifiersAndFightTags(
      BattlePropertyModifier pm,
      ref uint fightTags)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBuffPropertyModifiersAndFightTagsInCombat(
      BattlePropertyModifier pm,
      ref uint fightTags,
      BattleActor target,
      bool isAttacker,
      int distance,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CollectBuffPropertyExchange(
      BattleProperty battleProperty,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CollectBuffPropertyReplace()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MoveType BuffMoveTypeChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateAllAuras()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveAuraAppliedBuffs(BuffState auraBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemovePackChildBuffs(BuffState packBuff)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuffHit(
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasFightTag(FightTag ft)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBuffTypes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBuffType(BuffType bt, bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBuffType(BuffType bt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBuffEffective(BuffState bs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffHpConditionSatisfied(ConfigDataBuffInfo buffInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffHpConditionSatisfied(int operatorType, int value, int targetType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffEffectiveConditionSatisfied(ConfigDataBuffInfo buffInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffCombatConditionSatisfied(
      BattleActor target,
      bool isAttacker,
      int distance,
      int param1,
      int param2,
      int param3,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffCombatConditionSatisfied(
      BattleActor target,
      bool beforeCombat,
      bool isAttacker,
      int distance,
      bool isCritical,
      List<int> paramList,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeNeighborAliveActorCount(int teamType, int distance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffCooldown(BuffState bs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBuffCooldown(BuffState bs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeBuffCount(int buffId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeEnhanceOrDebuffCount(int enhanceDebuffType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsImmuneBuffSubType(int subType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int BuffReboundDamage(ConfigDataSkillInfo skillInfo, int damage, out int reboundPercent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool BuffHealChangeDamage(
      int heroHeal,
      int soldierHeal,
      out int heroDamage,
      out int soldierDamage,
      out BuffState damageBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool BuffBeHealChangeDamage(
      int heroHeal,
      int soldierHeal,
      out int heroDamage,
      out int soldierDamage,
      out BuffState damageBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuffHealOrChangeDamageHit(BuffState bs, int heroHeal, int soldierHeal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBuffOverrideMovePointCost()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo GetBuffOverrideTerrain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBuffArmyRelationAttack(
      int targetArmyId,
      bool targetIsHero,
      ref ArmyRelationData armyRelation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBuffArmyRelationDefend(
      int targetArmyId,
      bool targetIsHero,
      ref ArmyRelationData armyRelation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBuffArmyRelationModifyAttack(
      int restrainValue,
      bool targetIsHero,
      ref ArmyRelationData armyRelation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBuffArmyRelationModifyDefend(
      int restrainValue,
      bool targetIsHero,
      ref ArmyRelationData armyRelation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBuffIgnoreArmyRelation(int armyId, bool isHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBuffForceMagicDamage(bool attackerIsHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBuffForcePhysicalDamage(bool attackerIsHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndBuffEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBuffTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActionEndBuffDoubleMove(bool isKill, bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndBuffHealOverTime(bool isDamage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndBuffDamageOverTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndAddBuff()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndAddBuffSuper(bool isKill, bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndRemoveDebuff(bool isDamage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndRemoveEnhanceBuff(bool isKill, bool isCritical, bool isDamage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndBuffBattlefieldSkill(bool isKill, bool isCritical, bool isDamage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndBuffNewTurn(bool isKill, bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareHealActor(BattleActor a0, BattleActor a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBuffHeal(
      BattleActor target,
      bool beforeCombat,
      bool isAttacker,
      int distance,
      bool isCritical,
      int heroDamage,
      int soldierDamage,
      int reboundPercent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBuffHealOther(
      BattleActor target,
      bool beforeCombat,
      bool isAttacker,
      int distance,
      bool isCritical,
      int heroDamage,
      int soldierDamage,
      int reboundPercent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBuffDamage(
      BattleActor target,
      bool beforeCombat,
      bool isAttacker,
      int distance,
      bool isCritical,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatApplyBuff(
      BattleActor target,
      bool beforeCombat,
      bool isAttacker,
      int distance,
      bool isCritical,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackApplyBuff(List<BattleActor> targets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void KillApplyBuff(bool isCritical, bool isUseSkill)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackBuffDamage(
      List<BattleActor> targets,
      bool beforeCombat,
      bool isAttacker,
      int combatDistance,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackRemoveBuff(
      List<BattleActor> targets,
      bool beforeCombat,
      bool isAttacker,
      int combatDistance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackRemoveSkillCooldown(
      ConfigDataSkillInfo skillInfo,
      bool isCritical,
      bool isKill)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BattleFieldSkillApplyBuff(List<BattleActor> targets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackBuffPunch(BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackBuffDrag(BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackBuffExchangePosition(BattleActor target, bool isTargetGurad)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBuffArmyChange(
      BattleActor target,
      ConfigDataArmyInfo targetHeroArmyInfo,
      bool beforeCombat,
      bool isAttacker,
      int distance,
      bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DamageBuffHealOther()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffState BuffDoubleAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffState BuffDoubleSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuffUndead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanBuffGuard(BattleActor target, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetGuardActor(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AfterCombatDetachBuff()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleSkillState GetSkillState(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleSkillState GetSkillStateById(int skillID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleSkillState> GetSkillStates()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsSkillUseable(int skillIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanUseSkillOnTarget(ConfigDataSkillInfo skillInfo, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBattlefiledSkillApplyTarget(ConfigDataSkillInfo skillInfo, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FindBattlefieldSkillApplyTargets(
      ConfigDataSkillInfo skillInfo,
      GridPosition targetPos,
      List<BattleActor> targetActors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ExecuteBattlefieldSkill(
      ConfigDataSkillInfo skillInfo,
      GridPosition p,
      GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool SkillAttack(ConfigDataSkillInfo skillInfo, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool SkillSummon(ConfigDataSkillInfo skillInfo, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool SkillTeleport(
      ConfigDataSkillInfo skillInfo,
      BattleActor target,
      GridPosition teleportPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AttackBy(BattleActor attacker, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillHit(
      ConfigDataSkillInfo skillInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType,
      bool isRebound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SkillAttackEnd(ConfigDataSkillInfo skillInfo, List<BattleActor> targets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBy(BattleActor attacker)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeleportBy(BattleActor attacker, ConfigDataSkillInfo skillInfo, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SummonBy(BattleActor attacker, ConfigDataSkillInfo skillInfo, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetHeroAttackSkillInfo(
      MoveType targetMoveType,
      int distance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetSoldierAttackSkillInfo(
      MoveType targetMoveType,
      int distance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataSkillInfo GetHeroAttackSkillInfo(bool isMelee)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataSkillInfo GetSoldierAttackSkillInfo(bool isMelee)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleActor.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Dispose()
    {
      base.Dispose();
    }

    private void __callBase_Tick()
    {
      this.Tick();
    }

    private void __callBase_TickGraphic(float dt)
    {
      this.TickGraphic(dt);
    }

    private void __callBase_Draw()
    {
      this.Draw();
    }

    private void __callBase_Pause(bool pause)
    {
      this.Pause(pause);
    }

    private void __callBase_DoPause(bool pause)
    {
      this.DoPause(pause);
    }

    private void __callBase_DeleteMe()
    {
      this.DeleteMe();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public delegate int ComputeActorScoreFunc(BattleActor a, int param);

    public enum BehaviorState
    {
      Move,
      Attack,
    }

    public class LuaExportHelper
    {
      private BattleActor m_owner;

      public LuaExportHelper(BattleActor owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Dispose()
      {
        this.m_owner.__callBase_Dispose();
      }

      public void __callBase_Tick()
      {
        this.m_owner.__callBase_Tick();
      }

      public void __callBase_TickGraphic(float dt)
      {
        this.m_owner.__callBase_TickGraphic(dt);
      }

      public void __callBase_Draw()
      {
        this.m_owner.__callBase_Draw();
      }

      public void __callBase_Pause(bool pause)
      {
        this.m_owner.__callBase_Pause(pause);
      }

      public void __callBase_DoPause(bool pause)
      {
        this.m_owner.__callBase_DoPause(pause);
      }

      public void __callBase_DeleteMe()
      {
        this.m_owner.__callBase_DeleteMe();
      }

      public BattleTeam m_team
      {
        get
        {
          return this.m_owner.m_team;
        }
        set
        {
          this.m_owner.m_team = value;
        }
      }

      public GridPosition m_position
      {
        get
        {
          return this.m_owner.m_position;
        }
        set
        {
          this.m_owner.m_position = value;
        }
      }

      public int m_direction
      {
        get
        {
          return this.m_owner.m_direction;
        }
        set
        {
          this.m_owner.m_direction = value;
        }
      }

      public GridPosition m_initPosition
      {
        get
        {
          return this.m_owner.m_initPosition;
        }
        set
        {
          this.m_owner.m_initPosition = value;
        }
      }

      public GridPosition m_beforeGuardPosition
      {
        get
        {
          return this.m_owner.m_beforeGuardPosition;
        }
        set
        {
          this.m_owner.m_beforeGuardPosition = value;
        }
      }

      public BattleProperty m_heroBattleProperty
      {
        get
        {
          return this.m_owner.m_heroBattleProperty;
        }
        set
        {
          this.m_owner.m_heroBattleProperty = value;
        }
      }

      public BattleProperty m_soldierBattleProperty
      {
        get
        {
          return this.m_owner.m_soldierBattleProperty;
        }
        set
        {
          this.m_owner.m_soldierBattleProperty = value;
        }
      }

      public int m_heroHealthPoint
      {
        get
        {
          return this.m_owner.m_heroHealthPoint;
        }
        set
        {
          this.m_owner.m_heroHealthPoint = value;
        }
      }

      public int m_soldierTotalHealthPoint
      {
        get
        {
          return this.m_owner.m_soldierTotalHealthPoint;
        }
        set
        {
          this.m_owner.m_soldierTotalHealthPoint = value;
        }
      }

      public int m_soldierSingleHealthPointMax
      {
        get
        {
          return this.m_owner.m_soldierSingleHealthPointMax;
        }
        set
        {
          this.m_owner.m_soldierSingleHealthPointMax = value;
        }
      }

      public int m_initSoldierCount
      {
        get
        {
          return this.m_owner.m_initSoldierCount;
        }
        set
        {
          this.m_owner.m_initSoldierCount = value;
        }
      }

      public int m_heroLevel
      {
        get
        {
          return this.m_owner.m_heroLevel;
        }
        set
        {
          this.m_owner.m_heroLevel = value;
        }
      }

      public int m_heroStar
      {
        get
        {
          return this.m_owner.m_heroStar;
        }
        set
        {
          this.m_owner.m_heroStar = value;
        }
      }

      public int m_jobLevel
      {
        get
        {
          return this.m_owner.m_jobLevel;
        }
        set
        {
          this.m_owner.m_jobLevel = value;
        }
      }

      public ConfigDataJobInfo[] m_masterJobInfos
      {
        get
        {
          return this.m_owner.m_masterJobInfos;
        }
        set
        {
          this.m_owner.m_masterJobInfos = value;
        }
      }

      public BattleActorEquipment[] m_equipments
      {
        get
        {
          return this.m_owner.m_equipments;
        }
        set
        {
          this.m_owner.m_equipments = value;
        }
      }

      public ConfigDataSkillInfo[] m_resonanceSkillInfos
      {
        get
        {
          return this.m_owner.m_resonanceSkillInfos;
        }
        set
        {
          this.m_owner.m_resonanceSkillInfos = value;
        }
      }

      public ConfigDataSkillInfo[] m_fetterSkillInfos
      {
        get
        {
          return this.m_owner.m_fetterSkillInfos;
        }
        set
        {
          this.m_owner.m_fetterSkillInfos = value;
        }
      }

      public int m_actionValue
      {
        get
        {
          return this.m_owner.m_actionValue;
        }
        set
        {
          this.m_owner.m_actionValue = value;
        }
      }

      public MoveType m_moveType
      {
        get
        {
          return this.m_owner.m_moveType;
        }
        set
        {
          this.m_owner.m_moveType = value;
        }
      }

      public bool m_isActionFinished
      {
        get
        {
          return this.m_owner.m_isActionFinished;
        }
        set
        {
          this.m_owner.m_isActionFinished = value;
        }
      }

      public int m_buffNewTurnCount
      {
        get
        {
          return this.m_owner.m_buffNewTurnCount;
        }
        set
        {
          this.m_owner.m_buffNewTurnCount = value;
        }
      }

      public int m_hasExtraActionMovePoint
      {
        get
        {
          return this.m_owner.m_hasExtraActionMovePoint;
        }
        set
        {
          this.m_owner.m_hasExtraActionMovePoint = value;
        }
      }

      public int m_hasExtraMovingMovePoint
      {
        get
        {
          return this.m_owner.m_hasExtraMovingMovePoint;
        }
        set
        {
          this.m_owner.m_hasExtraMovingMovePoint = value;
        }
      }

      public ExtraActionType m_curExtraActionType
      {
        get
        {
          return this.m_owner.m_curExtraActionType;
        }
        set
        {
          this.m_owner.m_curExtraActionType = value;
        }
      }

      public int m_curExtraActionMovePoint
      {
        get
        {
          return this.m_owner.m_curExtraActionMovePoint;
        }
        set
        {
          this.m_owner.m_curExtraActionMovePoint = value;
        }
      }

      public bool m_isVisible
      {
        get
        {
          return this.m_owner.m_isVisible;
        }
        set
        {
          this.m_owner.m_isVisible = value;
        }
      }

      public bool m_isCheckedDie
      {
        get
        {
          return this.m_owner.m_isCheckedDie;
        }
        set
        {
          this.m_owner.m_isCheckedDie = value;
        }
      }

      public bool m_isPostCheckedDie
      {
        get
        {
          return this.m_owner.m_isPostCheckedDie;
        }
        set
        {
          this.m_owner.m_isPostCheckedDie = value;
        }
      }

      public bool m_isRetreat
      {
        get
        {
          return this.m_owner.m_isRetreat;
        }
        set
        {
          this.m_owner.m_isRetreat = value;
        }
      }

      public List<BattleSkillState> m_skillStates
      {
        get
        {
          return this.m_owner.m_skillStates;
        }
        set
        {
          this.m_owner.m_skillStates = value;
        }
      }

      public List<BuffState> m_buffStates
      {
        get
        {
          return this.m_owner.m_buffStates;
        }
        set
        {
          this.m_owner.m_buffStates = value;
        }
      }

      public int m_buffIdCounter
      {
        get
        {
          return this.m_owner.m_buffIdCounter;
        }
        set
        {
          this.m_owner.m_buffIdCounter = value;
        }
      }

      public ulong m_buffTypes
      {
        get
        {
          return this.m_owner.m_buffTypes;
        }
        set
        {
          this.m_owner.m_buffTypes = value;
        }
      }

      public uint m_fightTags
      {
        get
        {
          return this.m_owner.m_fightTags;
        }
        set
        {
          this.m_owner.m_fightTags = value;
        }
      }

      public BattleActor m_summoner
      {
        get
        {
          return this.m_owner.m_summoner;
        }
        set
        {
          this.m_owner.m_summoner = value;
        }
      }

      public BattleActor m_killerActor
      {
        get
        {
          return this.m_owner.m_killerActor;
        }
        set
        {
          this.m_owner.m_killerActor = value;
        }
      }

      public bool m_isNpc
      {
        get
        {
          return this.m_owner.m_isNpc;
        }
        set
        {
          this.m_owner.m_isNpc = value;
        }
      }

      public bool m_isPlayerNpc
      {
        get
        {
          return this.m_owner.m_isPlayerNpc;
        }
        set
        {
          this.m_owner.m_isPlayerNpc = value;
        }
      }

      public BattleActorSourceType m_sourceType
      {
        get
        {
          return this.m_owner.m_sourceType;
        }
        set
        {
          this.m_owner.m_sourceType = value;
        }
      }

      public ConfigDataHeroInfo m_heroInfo
      {
        get
        {
          return this.m_owner.m_heroInfo;
        }
        set
        {
          this.m_owner.m_heroInfo = value;
        }
      }

      public ConfigDataJobConnectionInfo m_jobConnectionInfo
      {
        get
        {
          return this.m_owner.m_jobConnectionInfo;
        }
        set
        {
          this.m_owner.m_jobConnectionInfo = value;
        }
      }

      public ConfigDataJobInfo m_jobInfo
      {
        get
        {
          return this.m_owner.m_jobInfo;
        }
        set
        {
          this.m_owner.m_jobInfo = value;
        }
      }

      public ConfigDataArmyInfo m_heroArmyInfo
      {
        get
        {
          return this.m_owner.m_heroArmyInfo;
        }
        set
        {
          this.m_owner.m_heroArmyInfo = value;
        }
      }

      public ConfigDataSoldierInfo m_soldierInfo
      {
        get
        {
          return this.m_owner.m_soldierInfo;
        }
        set
        {
          this.m_owner.m_soldierInfo = value;
        }
      }

      public ConfigDataArmyInfo m_soldierArmyInfo
      {
        get
        {
          return this.m_owner.m_soldierArmyInfo;
        }
        set
        {
          this.m_owner.m_soldierArmyInfo = value;
        }
      }

      public ConfigDataCharImageSkinResourceInfo m_heroCharImageSkinResourceInfo
      {
        get
        {
          return this.m_owner.m_heroCharImageSkinResourceInfo;
        }
        set
        {
          this.m_owner.m_heroCharImageSkinResourceInfo = value;
        }
      }

      public ConfigDataModelSkinResourceInfo m_heroModelSkinResourceInfo
      {
        get
        {
          return this.m_owner.m_heroModelSkinResourceInfo;
        }
        set
        {
          this.m_owner.m_heroModelSkinResourceInfo = value;
        }
      }

      public ConfigDataModelSkinResourceInfo m_soldierModelSkinResourceInfo
      {
        get
        {
          return this.m_owner.m_soldierModelSkinResourceInfo;
        }
        set
        {
          this.m_owner.m_soldierModelSkinResourceInfo = value;
        }
      }

      public ConfigDataCharImageInfo m_heroVoiceCharImageInfo
      {
        get
        {
          return this.m_owner.m_heroVoiceCharImageInfo;
        }
        set
        {
          this.m_owner.m_heroVoiceCharImageInfo = value;
        }
      }

      public ConfigDataSkillInfo[] m_extraPassiveSkillInfos
      {
        get
        {
          return this.m_owner.m_extraPassiveSkillInfos;
        }
        set
        {
          this.m_owner.m_extraPassiveSkillInfos = value;
        }
      }

      public ConfigDataSkillInfo m_extraTalentSkillInfo
      {
        get
        {
          return this.m_owner.m_extraTalentSkillInfo;
        }
        set
        {
          this.m_owner.m_extraTalentSkillInfo = value;
        }
      }

      public bool m_isActionCriticalAttack
      {
        get
        {
          return this.m_owner.m_isActionCriticalAttack;
        }
        set
        {
          this.m_owner.m_isActionCriticalAttack = value;
        }
      }

      public bool m_isActionKillActor
      {
        get
        {
          return this.m_owner.m_isActionKillActor;
        }
        set
        {
          this.m_owner.m_isActionKillActor = value;
        }
      }

      public bool m_isActionDamageActor
      {
        get
        {
          return this.m_owner.m_isActionDamageActor;
        }
        set
        {
          this.m_owner.m_isActionDamageActor = value;
        }
      }

      public bool m_isBeCriticalAttack
      {
        get
        {
          return this.m_owner.m_isBeCriticalAttack;
        }
        set
        {
          this.m_owner.m_isBeCriticalAttack = value;
        }
      }

      public int m_actionMoveGrids
      {
        get
        {
          return this.m_owner.m_actionMoveGrids;
        }
        set
        {
          this.m_owner.m_actionMoveGrids = value;
        }
      }

      public int m_actionRemainMovePoint
      {
        get
        {
          return this.m_owner.m_actionRemainMovePoint;
        }
        set
        {
          this.m_owner.m_actionRemainMovePoint = value;
        }
      }

      public ConfigDataSkillInfo m_lastDamageBySkillInfo
      {
        get
        {
          return this.m_owner.m_lastDamageBySkillInfo;
        }
        set
        {
          this.m_owner.m_lastDamageBySkillInfo = value;
        }
      }

      public int m_firstDamageTurn
      {
        get
        {
          return this.m_owner.m_firstDamageTurn;
        }
        set
        {
          this.m_owner.m_firstDamageTurn = value;
        }
      }

      public bool m_isTurnDamage
      {
        get
        {
          return this.m_owner.m_isTurnDamage;
        }
        set
        {
          this.m_owner.m_isTurnDamage = value;
        }
      }

      public ulong m_satisfyConditions
      {
        get
        {
          return this.m_owner.m_satisfyConditions;
        }
        set
        {
          this.m_owner.m_satisfyConditions = value;
        }
      }

      public short m_actionCount
      {
        get
        {
          return this.m_owner.m_actionCount;
        }
        set
        {
          this.m_owner.m_actionCount = value;
        }
      }

      public short m_combatAttackCount
      {
        get
        {
          return this.m_owner.m_combatAttackCount;
        }
        set
        {
          this.m_owner.m_combatAttackCount = value;
        }
      }

      public short m_beCombatAttackCount
      {
        get
        {
          return this.m_owner.m_beCombatAttackCount;
        }
        set
        {
          this.m_owner.m_beCombatAttackCount = value;
        }
      }

      public short m_useSkillCount
      {
        get
        {
          return this.m_owner.m_useSkillCount;
        }
        set
        {
          this.m_owner.m_useSkillCount = value;
        }
      }

      public short m_killActorCount
      {
        get
        {
          return this.m_owner.m_killActorCount;
        }
        set
        {
          this.m_owner.m_killActorCount = value;
        }
      }

      public int m_dieTurn
      {
        get
        {
          return this.m_owner.m_dieTurn;
        }
        set
        {
          this.m_owner.m_dieTurn = value;
        }
      }

      public int m_deathAnimType
      {
        get
        {
          return this.m_owner.m_deathAnimType;
        }
        set
        {
          this.m_owner.m_deathAnimType = value;
        }
      }

      public uint m_executedCommandTypes
      {
        get
        {
          return this.m_owner.m_executedCommandTypes;
        }
        set
        {
          this.m_owner.m_executedCommandTypes = value;
        }
      }

      public ConfigDataSkillInfo m_executedSkillInfo
      {
        get
        {
          return this.m_owner.m_executedSkillInfo;
        }
        set
        {
          this.m_owner.m_executedSkillInfo = value;
        }
      }

      public int m_playerIndex
      {
        get
        {
          return this.m_owner.m_playerIndex;
        }
        set
        {
          this.m_owner.m_playerIndex = value;
        }
      }

      public ConfigDataBehavior m_curBehaviorCfg
      {
        get
        {
          return this.m_owner.m_curBehaviorCfg;
        }
        set
        {
          this.m_owner.m_curBehaviorCfg = value;
        }
      }

      public BattleActor.BehaviorState m_curBehaviorState
      {
        get
        {
          return this.m_owner.m_curBehaviorState;
        }
        set
        {
          this.m_owner.m_curBehaviorState = value;
        }
      }

      public BehaviorTarget m_moveTarget
      {
        get
        {
          return this.m_owner.m_moveTarget;
        }
        set
        {
          this.m_owner.m_moveTarget = value;
        }
      }

      public BehaviorTarget m_attackTarget
      {
        get
        {
          return this.m_owner.m_attackTarget;
        }
        set
        {
          this.m_owner.m_attackTarget = value;
        }
      }

      public int m_attackSkillIndex
      {
        get
        {
          return this.m_owner.m_attackSkillIndex;
        }
        set
        {
          this.m_owner.m_attackSkillIndex = value;
        }
      }

      public int[] m_beAttackedCountOfTurns
      {
        get
        {
          return this.m_owner.m_beAttackedCountOfTurns;
        }
        set
        {
          this.m_owner.m_beAttackedCountOfTurns = value;
        }
      }

      public int m_groupId
      {
        get
        {
          return this.m_owner.m_groupId;
        }
        set
        {
          this.m_owner.m_groupId = value;
        }
      }

      public int m_aiCreateBattleCommandCount
      {
        get
        {
          return this.m_owner.m_aiCreateBattleCommandCount;
        }
        set
        {
          this.m_owner.m_aiCreateBattleCommandCount = value;
        }
      }

      public int m_aiBeginTurn
      {
        get
        {
          return this.m_owner.m_aiBeginTurn;
        }
        set
        {
          this.m_owner.m_aiBeginTurn = value;
        }
      }

      public static float m_healSkillTargetHPThresh
      {
        get
        {
          return 0.7f;
        }
      }

      public bool IsAttackedByEnemy
      {
        get
        {
          return this.m_owner.IsAttackedByEnemy;
        }
      }

      public bool IsAttackedByEnemyInLastTrun
      {
        get
        {
          return this.m_owner.IsAttackedByEnemyInLastTrun;
        }
      }

      public int NextBehaviorByChangeRules
      {
        get
        {
          return this.m_owner.NextBehaviorByChangeRules;
        }
      }

      public float TotalHPPercent
      {
        get
        {
          return this.m_owner.TotalHPPercent;
        }
      }

      public ConfigDataSkillInfo BehaviorSelectSkillInfo
      {
        get
        {
          return this.m_owner.BehaviorSelectSkillInfo;
        }
      }

      public void ComputeBattleProperties(BattlePropertyModifier pm)
      {
        this.m_owner.ComputeBattleProperties(pm);
      }

      public void SetPosition(GridPosition p)
      {
        this.m_owner.SetPosition(p);
      }

      public void MoveTo(GridPosition p)
      {
        this.m_owner.MoveTo(p);
      }

      public void ClearMapActor()
      {
        this.m_owner.ClearMapActor();
      }

      public void PostActionTerrainDamage()
      {
        this.m_owner.PostActionTerrainDamage();
      }

      public bool ShouldLog()
      {
        return this.m_owner.ShouldLog();
      }

      public void AddExecutedCommandType(BattleCommandType cmdType)
      {
        this.m_owner.AddExecutedCommandType(cmdType);
      }

      public bool IsExecutedCommandType(BattleCommandType cmdType)
      {
        return this.m_owner.IsExecutedCommandType(cmdType);
      }

      public bool IsEventOrPerformActor()
      {
        return this.m_owner.IsEventOrPerformActor();
      }

      public void FindMoveAndAttackRegion(int distance, int shape)
      {
        this.m_owner.FindMoveAndAttackRegion(distance, shape);
      }

      public bool FindRandomEmptyPosition(int attackDistance, int shape, ref GridPosition position)
      {
        return this.m_owner.FindRandomEmptyPosition(attackDistance, shape, ref position);
      }

      public GridPosition FindAttackPosition(
        int attackDistance,
        int shape,
        GridPosition targetPos,
        bool checkMoveRegion,
        bool farAway)
      {
        return this.m_owner.FindAttackPosition(attackDistance, shape, targetPos, checkMoveRegion, farAway);
      }

      public static int ComputeActorScoreDamage(BattleActor a, int param)
      {
        return BattleActor.ComputeActorScoreDamage(a, param);
      }

      public static int ComputeActorScoreHeal(BattleActor a, int param)
      {
        return BattleActor.ComputeActorScoreHeal(a, param);
      }

      public int ComputeActorScoreBuff(BattleActor a, int param)
      {
        return this.m_owner.ComputeActorScoreBuff(a, param);
      }

      public BattleActor SelectNearestTarget(BattleTeam team)
      {
        return this.m_owner.SelectNearestTarget(team);
      }

      public RandomNumber GetAIRandomNumber()
      {
        return this.m_owner.GetAIRandomNumber();
      }

      public BattleTeam GetSkillTargetTeam(ConfigDataSkillInfo skillInfo)
      {
        return this.m_owner.GetSkillTargetTeam(skillInfo);
      }

      public bool CanAttackOrUseSkill()
      {
        return this.m_owner.CanAttackOrUseSkill();
      }

      public void SetBehaviorState(BattleActor.BehaviorState state)
      {
        this.m_owner.SetBehaviorState(state);
      }

      public void DoBehaviorChangeRules()
      {
        this.m_owner.DoBehaviorChangeRules();
      }

      public GridPosition FindFarthestPosition(
        List<GridPosition> positions,
        GridPosition startPos)
      {
        return this.m_owner.FindFarthestPosition(positions, startPos);
      }

      public GridPosition FindNearestPosition(
        List<GridPosition> positions,
        GridPosition startPos)
      {
        return this.m_owner.FindNearestPosition(positions, startPos);
      }

      public BattleActor FindNearestActor(
        List<BattleActor> actors,
        GridPosition startPos)
      {
        return this.m_owner.FindNearestActor(actors, startPos);
      }

      public BehaviorTarget DoSelectTarget(
        SelectTarget st,
        ConfigDataBehavior.ParamData param)
      {
        return this.m_owner.DoSelectTarget(st, param);
      }

      public List<BattleActor> FindActorsByIDFilter(
        List<BattleActor> actors,
        int[] priorIDs,
        int[] excludeIDs)
      {
        return this.m_owner.FindActorsByIDFilter(actors, priorIDs, excludeIDs);
      }

      public static bool Contains(int[] target, int nr)
      {
        return BattleActor.Contains(target, nr);
      }

      public void SelectExtraMoveTarget()
      {
        this.m_owner.SelectExtraMoveTarget();
      }

      public void SelectMoveTarget()
      {
        this.m_owner.SelectMoveTarget();
      }

      public void GenerateCommandOfMove(GridPosition target)
      {
        this.m_owner.GenerateCommandOfMove(target);
      }

      public void GenerateCommandOfMove(GridPosition target, GridPosition target2)
      {
        this.m_owner.GenerateCommandOfMove(target, target2);
      }

      public GridPosition FindPositionToMoveToTarget(GridPosition target)
      {
        return this.m_owner.FindPositionToMoveToTarget(target);
      }

      public GridPosition FindPositionToMoveToTarget(
        GridPosition target,
        out BattleActor blockingEnemy)
      {
        return this.m_owner.FindPositionToMoveToTarget(target, out blockingEnemy);
      }

      public int ComputeRestrictScore(BattleActor srcActor, BattleActor destActor)
      {
        return this.m_owner.ComputeRestrictScore(srcActor, destActor);
      }

      public int GetArmyRistrictScore(ArmyTag a, ArmyTag b)
      {
        return this.m_owner.GetArmyRistrictScore(a, b);
      }

      public BattleActor DefaultSelectDamageSkillTarget(List<BattleActor> actors)
      {
        return this.m_owner.DefaultSelectDamageSkillTarget(actors);
      }

      public bool IsSelectRangeSkill()
      {
        return this.m_owner.IsSelectRangeSkill();
      }

      public List<BattleActor> FindActorsInGrids(
        List<BattleActor> actors,
        List<GridPosition> grids)
      {
        return this.m_owner.FindActorsInGrids(actors, grids);
      }

      public GridPosition FindMaxAoeSkillCoverPosition(
        ConfigDataSkillInfo si,
        List<GridPosition> asCenterPositions,
        List<BattleActor> beCoveredActors,
        ClassValue<int> maxCoverActorsCount)
      {
        return this.m_owner.FindMaxAoeSkillCoverPosition(si, asCenterPositions, beCoveredActors, maxCoverActorsCount);
      }

      public BattleActor FindMaxAoeSkillCoverActor(
        ConfigDataSkillInfo si,
        List<BattleActor> asCenterActors,
        List<BattleActor> beCoveredActors,
        ClassValue<int> maxCoverActorsCount)
      {
        return this.m_owner.FindMaxAoeSkillCoverActor(si, asCenterActors, beCoveredActors, maxCoverActorsCount);
      }

      public void DefaultSelectAttackTarget()
      {
        this.m_owner.DefaultSelectAttackTarget();
      }

      public void SelectAttackTargetWithSummonSkill()
      {
        this.m_owner.SelectAttackTargetWithSummonSkill();
      }

      public List<GridPosition> FindGridsLessEqualDistance(
        GridPosition startPos,
        int dist)
      {
        return this.m_owner.FindGridsLessEqualDistance(startPos, dist);
      }

      public List<BattleActor> FindActorsLessEqualDistance(
        List<BattleActor> actors,
        GridPosition startPos,
        int dist)
      {
        return this.m_owner.FindActorsLessEqualDistance(actors, startPos, dist);
      }

      public BattleActor DefaultSelectHealSkillTarget(List<BattleActor> actors)
      {
        return this.m_owner.DefaultSelectHealSkillTarget(actors);
      }

      public BattleActor DefaultSelectBuffSkillTarget(
        List<BattleActor> actors,
        ConfigDataSkillInfo skill)
      {
        return this.m_owner.DefaultSelectBuffSkillTarget(actors, skill);
      }

      public void SelectAttackTargetInSkillRange()
      {
        this.m_owner.SelectAttackTargetInSkillRange();
      }

      public List<BattleActor> FindActorsWithBuffN(
        List<BattleActor> actors,
        int buffID)
      {
        return this.m_owner.FindActorsWithBuffN(actors, buffID);
      }

      public List<BattleActor> FindActorsWithSkillAITypeN(
        List<BattleActor> actors,
        int paramN)
      {
        return this.m_owner.FindActorsWithSkillAITypeN(actors, paramN);
      }

      public void SelectAttackTarget()
      {
        this.m_owner.SelectAttackTarget();
      }

      public void SelectSkill()
      {
        this.m_owner.SelectSkill();
      }

      public void SelectSkillDirectReachTarget()
      {
        this.m_owner.SelectSkillDirectReachTarget();
      }

      public static bool HasInt(int value, int[] arr)
      {
        return BattleActor.HasInt(value, arr);
      }

      public void DefaultSelectSkill(int[] includeSkillIDs, int[] excludeSkillIDs)
      {
        this.m_owner.DefaultSelectSkill(includeSkillIDs, excludeSkillIDs);
      }

      public List<BattleActor> FindActorsInCanNormalAttackAndTouchRange(
        List<BattleActor> destActors,
        List<BattleActor> srcActors)
      {
        return this.m_owner.FindActorsInCanNormalAttackAndTouchRange(destActors, srcActors);
      }

      public bool IsSkillAGoodAISelection(ConfigDataSkillInfo si)
      {
        return this.m_owner.IsSkillAGoodAISelection(si);
      }

      public GridPosition FindCastSkillPosition(
        ConfigDataSkillInfo si,
        BehaviorTarget t)
      {
        return this.m_owner.FindCastSkillPosition(si, t);
      }

      public void FindAttackPositions(
        int attackDistance,
        int shape,
        GridPosition targetPos,
        List<GridPosition> positions)
      {
        this.m_owner.FindAttackPositions(attackDistance, shape, targetPos, positions);
      }

      public void GenerateCommandOfAttack()
      {
        this.m_owner.GenerateCommandOfAttack();
      }

      public void DoBehaviorMove()
      {
        this.m_owner.DoBehaviorMove();
      }

      public bool IsAttackTargetStillValid()
      {
        return this.m_owner.IsAttackTargetStillValid();
      }

      public void DoBehaviorAttack()
      {
        this.m_owner.DoBehaviorAttack();
      }

      public BattleCommand AICreateBattleCommand(BattleCommandType type)
      {
        return this.m_owner.AICreateBattleCommand(type);
      }

      public BattleActor SelectAttackRegionTarget(
        BattleTeam team,
        BattleActor.ComputeActorScoreFunc computeScore,
        int computeScoreParam)
      {
        return this.m_owner.SelectAttackRegionTarget(team, computeScore, computeScoreParam);
      }

      public void InitializeBuffs()
      {
        this.m_owner.InitializeBuffs();
      }

      public void AttachPassiveSkillBuffs(ConfigDataSkillInfo skillInfo, BuffSourceType sourceType)
      {
        this.m_owner.AttachPassiveSkillBuffs(skillInfo, sourceType);
      }

      public static int CompareBuffOrder(BuffState b0, BuffState b1)
      {
        return BattleActor.CompareBuffOrder(b0, b1);
      }

      public bool RemoveBuff(BuffState bs)
      {
        return this.m_owner.RemoveBuff(bs);
      }

      public int RemoveBuffList(List<int> buffIds, BuffState sourceBuffState)
      {
        return this.m_owner.RemoveBuffList(buffIds, sourceBuffState);
      }

      public BuffState FindBuff(int buffId, BuffState sourceBuffState)
      {
        return this.m_owner.FindBuff(buffId, sourceBuffState);
      }

      public void RemoveAllBuffs()
      {
        this.m_owner.RemoveAllBuffs();
      }

      public bool HasBuff(int buffId)
      {
        return this.m_owner.HasBuff(buffId);
      }

      public void CollectBuffPropertyModifiersAndFightTags(
        BattlePropertyModifier pm,
        ref uint fightTags)
      {
        this.m_owner.CollectBuffPropertyModifiersAndFightTags(pm, ref fightTags);
      }

      public void CollectPropertyModifier(
        BattlePropertyModifier pm,
        PropertyModifyType modifyType,
        int value,
        bool collectStatic,
        bool collectDynamic)
      {
        this.m_owner.CollectPropertyModifier(pm, modifyType, value, collectStatic, collectDynamic);
      }

      public void CollectOtherActorBuffPropertyModifiersAndFightTags(
        BattlePropertyModifier pm,
        ref uint fightTags)
      {
        this.m_owner.CollectOtherActorBuffPropertyModifiersAndFightTags(pm, ref fightTags);
      }

      public void CollectBuffPropertyModifiersAndFightTagsInCombat(
        BattlePropertyModifier pm,
        ref uint fightTags,
        BattleActor target,
        bool isAttacker,
        int distance,
        ConfigDataSkillInfo attackerSkillInfo)
      {
        this.m_owner.CollectBuffPropertyModifiersAndFightTagsInCombat(pm, ref fightTags, target, isAttacker, distance, attackerSkillInfo);
      }

      public void RemoveAuraAppliedBuffs(BuffState auraBuffState)
      {
        this.m_owner.RemoveAuraAppliedBuffs(auraBuffState);
      }

      public void RemovePackChildBuffs(BuffState packBuff)
      {
        this.m_owner.RemovePackChildBuffs(packBuff);
      }

      public void OnBuffHit(
        BuffState buffState,
        int heroHpModify,
        int soldierHpModify,
        DamageNumberType damageNumberType)
      {
        this.m_owner.OnBuffHit(buffState, heroHpModify, soldierHpModify, damageNumberType);
      }

      public void UpdateBuffTypes()
      {
        this.m_owner.UpdateBuffTypes();
      }

      public void SetBuffType(BuffType bt, bool on)
      {
        this.m_owner.SetBuffType(bt, on);
      }

      public bool IsBuffHpConditionSatisfied(ConfigDataBuffInfo buffInfo)
      {
        return this.m_owner.IsBuffHpConditionSatisfied(buffInfo);
      }

      public bool IsBuffHpConditionSatisfied(int operatorType, int value, int targetType)
      {
        return this.m_owner.IsBuffHpConditionSatisfied(operatorType, value, targetType);
      }

      public bool IsBuffEffectiveConditionSatisfied(ConfigDataBuffInfo buffInfo)
      {
        return this.m_owner.IsBuffEffectiveConditionSatisfied(buffInfo);
      }

      public bool IsBuffCombatConditionSatisfied(
        BattleActor target,
        bool isAttacker,
        int distance,
        int param1,
        int param2,
        int param3,
        ConfigDataSkillInfo attackerSkillInfo)
      {
        return this.m_owner.IsBuffCombatConditionSatisfied(target, isAttacker, distance, param1, param2, param3, attackerSkillInfo);
      }

      public bool IsBuffCombatConditionSatisfied(
        BattleActor target,
        bool beforeCombat,
        bool isAttacker,
        int distance,
        bool isCritical,
        List<int> paramList,
        ConfigDataSkillInfo attackerSkillInfo)
      {
        return this.m_owner.IsBuffCombatConditionSatisfied(target, beforeCombat, isAttacker, distance, isCritical, paramList, attackerSkillInfo);
      }

      public int ComputeNeighborAliveActorCount(int teamType, int distance)
      {
        return this.m_owner.ComputeNeighborAliveActorCount(teamType, distance);
      }

      public bool IsBuffCooldown(BuffState bs)
      {
        return this.m_owner.IsBuffCooldown(bs);
      }

      public void StartBuffCooldown(BuffState bs)
      {
        this.m_owner.StartBuffCooldown(bs);
      }

      public int ComputeBuffCount(int buffId)
      {
        return this.m_owner.ComputeBuffCount(buffId);
      }

      public int ComputeEnhanceOrDebuffCount(int enhanceDebuffType)
      {
        return this.m_owner.ComputeEnhanceOrDebuffCount(enhanceDebuffType);
      }

      public bool IsImmuneBuffSubType(int subType)
      {
        return this.m_owner.IsImmuneBuffSubType(subType);
      }

      public bool BuffHealChangeDamage(
        int heroHeal,
        int soldierHeal,
        out int heroDamage,
        out int soldierDamage,
        out BuffState damageBuffState)
      {
        return this.m_owner.BuffHealChangeDamage(heroHeal, soldierHeal, out heroDamage, out soldierDamage, out damageBuffState);
      }

      public bool BuffBeHealChangeDamage(
        int heroHeal,
        int soldierHeal,
        out int heroDamage,
        out int soldierDamage,
        out BuffState damageBuffState)
      {
        return this.m_owner.BuffBeHealChangeDamage(heroHeal, soldierHeal, out heroDamage, out soldierDamage, out damageBuffState);
      }

      public void OnBuffHealOrChangeDamageHit(BuffState bs, int heroHeal, int soldierHeal)
      {
        this.m_owner.OnBuffHealOrChangeDamageHit(bs, heroHeal, soldierHeal);
      }

      public void ActionEndBuffEffect()
      {
        this.m_owner.ActionEndBuffEffect();
      }

      public void UpdateBuffTime()
      {
        this.m_owner.UpdateBuffTime();
      }

      public void ActionEndBuffHealOverTime(bool isDamage)
      {
        this.m_owner.ActionEndBuffHealOverTime(isDamage);
      }

      public void ActionEndBuffDamageOverTime()
      {
        this.m_owner.ActionEndBuffDamageOverTime();
      }

      public void ActionEndAddBuff()
      {
        this.m_owner.ActionEndAddBuff();
      }

      public void ActionEndAddBuffSuper(bool isKill, bool isCritical)
      {
        this.m_owner.ActionEndAddBuffSuper(isKill, isCritical);
      }

      public void ActionEndRemoveDebuff(bool isDamage)
      {
        this.m_owner.ActionEndRemoveDebuff(isDamage);
      }

      public void ActionEndRemoveEnhanceBuff(bool isKill, bool isCritical, bool isDamage)
      {
        this.m_owner.ActionEndRemoveEnhanceBuff(isKill, isCritical, isDamage);
      }

      public void ActionEndBuffBattlefieldSkill(bool isKill, bool isCritical, bool isDamage)
      {
        this.m_owner.ActionEndBuffBattlefieldSkill(isKill, isCritical, isDamage);
      }

      public void ActionEndBuffNewTurn(bool isKill, bool isCritical)
      {
        this.m_owner.ActionEndBuffNewTurn(isKill, isCritical);
      }

      public static int CompareHealActor(BattleActor a0, BattleActor a1)
      {
        return BattleActor.CompareHealActor(a0, a1);
      }

      public bool CanBuffGuard(BattleActor target, ConfigDataSkillInfo skillInfo)
      {
        return this.m_owner.CanBuffGuard(target, skillInfo);
      }

      public bool IsSkillUseable(int skillIndex)
      {
        return this.m_owner.IsSkillUseable(skillIndex);
      }

      public void FindBattlefieldSkillApplyTargets(
        ConfigDataSkillInfo skillInfo,
        GridPosition targetPos,
        List<BattleActor> targetActors)
      {
        this.m_owner.FindBattlefieldSkillApplyTargets(skillInfo, targetPos, targetActors);
      }

      public bool ExecuteBattlefieldSkill(
        ConfigDataSkillInfo skillInfo,
        GridPosition p,
        GridPosition p2)
      {
        return this.m_owner.ExecuteBattlefieldSkill(skillInfo, p, p2);
      }

      public bool SkillAttack(ConfigDataSkillInfo skillInfo, BattleActor target)
      {
        return this.m_owner.SkillAttack(skillInfo, target);
      }

      public bool SkillSummon(ConfigDataSkillInfo skillInfo, GridPosition p)
      {
        return this.m_owner.SkillSummon(skillInfo, p);
      }

      public bool SkillTeleport(
        ConfigDataSkillInfo skillInfo,
        BattleActor target,
        GridPosition teleportPos)
      {
        return this.m_owner.SkillTeleport(skillInfo, target, teleportPos);
      }

      public void AttackBy(BattleActor attacker, ConfigDataSkillInfo skillInfo)
      {
        this.m_owner.AttackBy(attacker, skillInfo);
      }

      public void OnSkillHit(
        ConfigDataSkillInfo skillInfo,
        int heroHpModify,
        int soldierHpModify,
        DamageNumberType damageNumberType,
        bool isRebound)
      {
        this.m_owner.OnSkillHit(skillInfo, heroHpModify, soldierHpModify, damageNumberType, isRebound);
      }

      public void SkillAttackEnd(ConfigDataSkillInfo skillInfo, List<BattleActor> targets)
      {
        this.m_owner.SkillAttackEnd(skillInfo, targets);
      }

      public void TeleportBy(BattleActor attacker, ConfigDataSkillInfo skillInfo, GridPosition p)
      {
        this.m_owner.TeleportBy(attacker, skillInfo, p);
      }

      public void SummonBy(BattleActor attacker, ConfigDataSkillInfo skillInfo, GridPosition p)
      {
        this.m_owner.SummonBy(attacker, skillInfo, p);
      }

      public ConfigDataSkillInfo GetHeroAttackSkillInfo(bool isMelee)
      {
        return this.m_owner.GetHeroAttackSkillInfo(isMelee);
      }

      public ConfigDataSkillInfo GetSoldierAttackSkillInfo(bool isMelee)
      {
        return this.m_owner.GetSoldierAttackSkillInfo(isMelee);
      }
    }
  }
}
