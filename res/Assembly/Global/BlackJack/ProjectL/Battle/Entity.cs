﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.Entity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class Entity
  {
    private bool m_isDeleteMe;
    private bool m_isPaused;
    protected int m_id;

    public virtual void Dispose()
    {
    }

    public virtual void Tick()
    {
    }

    public virtual void TickGraphic(float dt)
    {
    }

    public virtual void Draw()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DoPause(bool pause)
    {
    }

    public int Id
    {
      get
      {
        return this.m_id;
      }
    }

    public void DeleteMe()
    {
      this.m_isDeleteMe = true;
    }

    public bool IsDeleteMe
    {
      get
      {
        return this.m_isDeleteMe;
      }
    }

    public bool IsPaused
    {
      get
      {
        return this.m_isPaused;
      }
    }
  }
}
