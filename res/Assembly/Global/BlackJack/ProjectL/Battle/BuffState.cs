﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BuffState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class BuffState
  {
    public int m_id;
    public int m_displayOrder;
    public ConfigDataBuffInfo m_buffInfo;
    public BuffSourceType m_sourceType;
    public ConfigDataSkillInfo m_sourceSkillInfo;
    public BuffState m_sourceBuffState;
    public int m_time;
    public int m_effectTimes;
    public bool m_hasExtraTime;
    public BattleActor m_applyer;
    public BuffState m_parent;
    [DoNotToLua]
    private BuffState.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorInt32ConfigDataBuffInfoBattleActorBuffSourceTypeConfigDataSkillInfoBuffState_hotfix;
    private LuaFunction m_IsChild_hotfix;
    private LuaFunction m_CanNotDispel_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffState(
      int id,
      ConfigDataBuffInfo buffInfo,
      BattleActor applyer,
      BuffSourceType sourceType,
      ConfigDataSkillInfo sourceSkillInfo,
      BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanNotDispel()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BuffState.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BuffState m_owner;

      public LuaExportHelper(BuffState owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
