﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleFormula
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class BattleFormula
  {
    private static Fix64 d10;
    private static Fix64 d100;
    private static Fix64 d10000;
    [DoNotToLua]
    private BattleFormula.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleFormula()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Fix64 ComputePhysicalDamageValue(
      BattleProperty attackerProperty,
      BattleProperty targetProperty,
      ConfigDataSkillInfo skillInfo,
      bool isCritical,
      int targetTerrainBonus,
      int armyAttack,
      int armyDefend,
      int meleePunish,
      bool useMagicAsAttack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Fix64 ComputeMagicDamageValue(
      BattleProperty attackerProperty,
      BattleProperty targetProperty,
      ConfigDataSkillInfo skillInfo,
      bool isCritical,
      int targetTerrainBonus,
      int armyMagic,
      int armyMagicDefend,
      int meleePunish,
      bool useAttackAsMagic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 ComputeHealFinalValue(
      Fix64 baseHeal,
      int attackerBuff_HealMul,
      int targetBuff_HealReceiveMul)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 ComputeHealValue(
      BattleProperty attackerProperty,
      BattleProperty targetProperty,
      int skillPower,
      int attackerMagic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 ComputePercentHealValue(
      BattleProperty attackerProperty,
      BattleProperty targetProperty,
      int skillPower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Fix64 ComputeHealValue(
      BattleProperty attackerProperty,
      BattleProperty targetProperty,
      ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeSkillHpModifyValue(
      BattleProperty attackerProperty,
      BattleProperty targetProperty,
      ArmyRelationData armyRelation,
      ConfigDataSkillInfo skillInfo,
      bool isCritical,
      bool isBuffForceMagicDamage,
      bool isBuffForcePhysicalDamage,
      bool isBanMeleePunish,
      ConfigDataTerrainInfo targetTerrain,
      int gridDistance,
      bool isSameTeam,
      RandomNumber randomNumber,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeBuffHealValue(
      int value,
      int percent,
      int applyerBuff_HealMul,
      int targetBuff_HealReceiveMul)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeBuffDamageValue(
      int value,
      int percent,
      int applyerBuff_TrueDamageMul,
      int targetBuff_RecvTrueDamageMul)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeBuffDamageValue(int value, int percent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeBuffExchangeValue(int value, int percent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeHeroBattleProperty(
      int jobBase,
      int jobUp,
      int heroLevel,
      int heroStarMul,
      int growMul,
      int growAdd,
      int buffMul,
      int buffAdd,
      int selfMul,
      int selfAdd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeSoldierBattleProperty(
      int soldierBase,
      int soldierUp,
      int heroLevel,
      int heroCmdMul,
      int growMul,
      int growAdd,
      int buffMul,
      int buffAdd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeEquipmentPropertyAdd(int baseValue, int levelUpValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeHeroBattlePower(
      ConfigDataJobInfo jobInfo,
      int hp,
      int at,
      int df,
      int magic,
      int magicDf,
      int dex,
      int skillBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeSoldierBattlePower(
      ConfigDataSoldierInfo soldierInfo,
      int hp,
      int at,
      int df,
      int magicDf,
      int skillBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DamageNumberType ComputeDamangeNumberType(
      ConfigDataSkillInfo skillInfo,
      bool isCritical,
      ArmyRelationData armyRelation,
      bool isSameTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleFormula.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static BattleFormula()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleFormula m_owner;

      public LuaExportHelper(BattleFormula owner)
      {
        this.m_owner = owner;
      }

      public static Fix64 d10
      {
        get
        {
          return BattleFormula.d10;
        }
        set
        {
          BattleFormula.d10 = value;
        }
      }

      public static Fix64 d100
      {
        get
        {
          return BattleFormula.d100;
        }
        set
        {
          BattleFormula.d100 = value;
        }
      }

      public static Fix64 d10000
      {
        get
        {
          return BattleFormula.d10000;
        }
        set
        {
          BattleFormula.d10000 = value;
        }
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public static Fix64 ComputePhysicalDamageValue(
        BattleProperty attackerProperty,
        BattleProperty targetProperty,
        ConfigDataSkillInfo skillInfo,
        bool isCritical,
        int targetTerrainBonus,
        int armyAttack,
        int armyDefend,
        int meleePunish,
        bool useMagicAsAttack)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public static Fix64 ComputeMagicDamageValue(
        BattleProperty attackerProperty,
        BattleProperty targetProperty,
        ConfigDataSkillInfo skillInfo,
        bool isCritical,
        int targetTerrainBonus,
        int armyMagic,
        int armyMagicDefend,
        int meleePunish,
        bool useAttackAsMagic)
      {
        // ISSUE: unable to decompile the method.
      }

      public static Fix64 ComputeHealValue(
        BattleProperty attackerProperty,
        BattleProperty targetProperty,
        ConfigDataSkillInfo skillInfo)
      {
        return BattleFormula.ComputeHealValue(attackerProperty, targetProperty, skillInfo);
      }
    }
  }
}
