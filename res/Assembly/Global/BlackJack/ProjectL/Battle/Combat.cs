﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.Combat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class Combat
  {
    private BattleBase m_battle;
    private RandomNumber m_randomNumber;
    private CombatState m_state;
    private bool m_isPaused;
    private int m_entityIdCounter;
    private ushort m_hitIdCounter;
    private int m_combatGridDistance;
    private int m_frameCount;
    private int m_startCountdown;
    private int m_endCountdown;
    private int m_cutscenePauseCountdown;
    private CombatTeam[] m_teams;
    [DoNotToLua]
    private Combat.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorBattleBase_hotfix;
    private LuaFunction m_Dispose_hotfix;
    private LuaFunction m_Tick_hotfix;
    private LuaFunction m_TickGraphicSingle_hotfix;
    private LuaFunction m_Draw_hotfix;
    private LuaFunction m_PauseBoolean_hotfix;
    private LuaFunction m_StartBattleActorBattleActorConfigDataSkillInfoConfigDataSkillInfoInt32_hotfix;
    private LuaFunction m_Stop_hotfix;
    private LuaFunction m_SetupTeamInt32BattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_Clear_hotfix;
    private LuaFunction m_GetTeamInt32_hotfix;
    private LuaFunction m_GetNextEntityId_hotfix;
    private LuaFunction m_GetNextHitId_hotfix;
    private LuaFunction m_GetCombatGridDistance_hotfix;
    private LuaFunction m_OnActorCastSkillCombatActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_OnActorCastPassiveSkillCombatActorBuffState_hotfix;
    private LuaFunction m_OnActorDieCombatActor_hotfix;
    private LuaFunction m_IsPlay_hotfix;
    private LuaFunction m_IsPaused_hotfix;
    private LuaFunction m_IsProbabilitySatisfiedInt32_hotfix;
    private LuaFunction m_GetFrameCount_hotfix;
    private LuaFunction m_get_Battle_hotfix;
    private LuaFunction m_get_RandomNumber_hotfix;
    private LuaFunction m_get_Listener_hotfix;
    private LuaFunction m_get_ConfigDataLoader_hotfix;
    private LuaFunction m_get_State_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public Combat(BattleBase battle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(
      BattleActor actor0,
      BattleActor actor1,
      ConfigDataSkillInfo heroSkillInfo0,
      ConfigDataSkillInfo heroSkillInfo1,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Stop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupTeam(
      int teamNumber,
      BattleActor battleActor,
      ConfigDataSkillInfo heroSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatTeam GetTeam(int teamNumber)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextEntityId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetNextHitId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCombatGridDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorCastSkill(CombatActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorCastPassiveSkill(CombatActor a, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorDie(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPaused()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProbabilitySatisfied(int rate)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFrameCount()
    {
      // ISSUE: unable to decompile the method.
    }

    public static int FrameToMillisecond(int frame)
    {
      return frame * 1000 / 30;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame1(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleBase Battle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomNumber RandomNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IBattleListener Listener
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CombatState State
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public Combat.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private Combat m_owner;

      public LuaExportHelper(Combat owner)
      {
        this.m_owner = owner;
      }

      public BattleBase m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public RandomNumber m_randomNumber
      {
        get
        {
          return this.m_owner.m_randomNumber;
        }
        set
        {
          this.m_owner.m_randomNumber = value;
        }
      }

      public CombatState m_state
      {
        get
        {
          return this.m_owner.m_state;
        }
        set
        {
          this.m_owner.m_state = value;
        }
      }

      public bool m_isPaused
      {
        get
        {
          return this.m_owner.m_isPaused;
        }
        set
        {
          this.m_owner.m_isPaused = value;
        }
      }

      public int m_entityIdCounter
      {
        get
        {
          return this.m_owner.m_entityIdCounter;
        }
        set
        {
          this.m_owner.m_entityIdCounter = value;
        }
      }

      public ushort m_hitIdCounter
      {
        get
        {
          return this.m_owner.m_hitIdCounter;
        }
        set
        {
          this.m_owner.m_hitIdCounter = value;
        }
      }

      public int m_combatGridDistance
      {
        get
        {
          return this.m_owner.m_combatGridDistance;
        }
        set
        {
          this.m_owner.m_combatGridDistance = value;
        }
      }

      public int m_frameCount
      {
        get
        {
          return this.m_owner.m_frameCount;
        }
        set
        {
          this.m_owner.m_frameCount = value;
        }
      }

      public int m_startCountdown
      {
        get
        {
          return this.m_owner.m_startCountdown;
        }
        set
        {
          this.m_owner.m_startCountdown = value;
        }
      }

      public int m_endCountdown
      {
        get
        {
          return this.m_owner.m_endCountdown;
        }
        set
        {
          this.m_owner.m_endCountdown = value;
        }
      }

      public int m_cutscenePauseCountdown
      {
        get
        {
          return this.m_owner.m_cutscenePauseCountdown;
        }
        set
        {
          this.m_owner.m_cutscenePauseCountdown = value;
        }
      }

      public CombatTeam[] m_teams
      {
        get
        {
          return this.m_owner.m_teams;
        }
        set
        {
          this.m_owner.m_teams = value;
        }
      }

      public void Pause(bool pause)
      {
        this.m_owner.Pause(pause);
      }

      public void SetupTeam(
        int teamNumber,
        BattleActor battleActor,
        ConfigDataSkillInfo heroSkillInfo)
      {
        this.m_owner.SetupTeam(teamNumber, battleActor, heroSkillInfo);
      }
    }
  }
}
