﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatActorList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class CombatActorList
  {
    public static void RemoveAll(List<CombatActor> list)
    {
      EntityList.RemoveAll<CombatActor>(list);
    }

    public static void RemoveDeleted(List<CombatActor> list)
    {
      EntityList.RemoveDeleted<CombatActor>(list);
    }

    public static void Tick(List<CombatActor> list)
    {
      EntityList.Tick<CombatActor>(list);
    }

    public static void TickGraphic(List<CombatActor> list, float dt)
    {
      EntityList.TickGraphic<CombatActor>(list, dt);
    }

    public static void Draw(List<CombatActor> list)
    {
      EntityList.Draw<CombatActor>(list);
    }

    public static void Pause(List<CombatActor> list, bool pause)
    {
      EntityList.Pause<CombatActor>(list, pause);
    }
  }
}
