﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BuffSourceType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

namespace BlackJack.ProjectL.Battle
{
  public enum BuffSourceType
  {
    None,
    SkillApply,
    SkillSelf,
    PassiveSkill,
    TalentPassiveSkill,
    JobPassiveSkill,
    FetterPassiveSkill,
    EquipmentPassiveSkill,
    SoldierPassiveSkill,
    TrainingTechPassiveSkill,
    SkinPassiveSkill,
    BuffApply,
    AuraApply,
    EventApply,
    RuleApply,
    CooldownApply,
  }
}
