﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatFlyObjectList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class CombatFlyObjectList
  {
    public static void RemoveAll(List<CombatFlyObject> list)
    {
      EntityList.RemoveAll<CombatFlyObject>(list);
    }

    public static void RemoveDeleted(List<CombatFlyObject> list)
    {
      EntityList.RemoveDeleted<CombatFlyObject>(list);
    }

    public static void Tick(List<CombatFlyObject> list)
    {
      EntityList.Tick<CombatFlyObject>(list);
    }

    public static void TickGraphic(List<CombatFlyObject> list, float dt)
    {
      EntityList.TickGraphic<CombatFlyObject>(list, dt);
    }

    public static void Draw(List<CombatFlyObject> list)
    {
      EntityList.Draw<CombatFlyObject>(list);
    }

    public static void Pause(List<CombatFlyObject> list, bool pause)
    {
      EntityList.Pause<CombatFlyObject>(list, pause);
    }
  }
}
