﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.AssetCacheType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class AssetCacheType
  {
    public const int Hero0 = 1;
    public const int Hero1 = 2;
    public const int Soldier0 = 3;
    public const int Soldier1 = 4;
    public const int Fx = 5;
    public const int TerrainBackground = 6;
  }
}
