﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.UISpineGraphic
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using Spine.Unity;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class UISpineGraphic
  {
    private SkeletonGraphic m_spine;
    private Material m_material;
    private GameObject m_gameObject;
    private bool m_isAnimationLoop;
    private List<ReplaceAnim> m_replaceAnims;
    private string m_assetName;

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Create(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleSpineAnimationEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleSpineAnimationStart(Spine.AnimationState state, int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleSpineAnimationEnd(Spine.AnimationState state, int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int dir)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetParent(GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScale(float scale)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRectTransformSize(Vector2 size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetColor(Color color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(string name, bool loop, int trackIndex = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopAnimation(int trackIndex)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLoop(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayAnimation(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAnimationDuration(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAnimationDuration(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAnimationTime(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnimationTime(int trackIndex, float time)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetReplaceAnimations(List<ReplaceAnim> replaceAnims)
    {
      this.m_replaceAnims = replaceAnims;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasAnimation(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnimationSpeed(float s)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ForceUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSlot(string name, bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableCanvasGroupAlpha(bool enable)
    {
    }

    public GameObject GameObject
    {
      get
      {
        return this.m_gameObject;
      }
    }

    public string AssetName
    {
      get
      {
        return this.m_assetName;
      }
    }
  }
}
