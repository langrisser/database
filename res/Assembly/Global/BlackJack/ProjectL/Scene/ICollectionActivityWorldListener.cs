﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ICollectionActivityWorldListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;

namespace BlackJack.ProjectL.Scene
{
  public interface ICollectionActivityWorldListener
  {
    void OnWaypointClick(
      ConfigDataCollectionActivityWaypointInfo waypointInfo);
  }
}
