﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientBattleTreasure
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientBattleTreasure : Entity
  {
    private ClientBattle m_clientBattle;
    private ConfigDataBattleTreasureInfo m_battleTreasureInfo;
    private GenericGraphic m_graphic;
    private GridPosition m_position;
    private bool m_isOpened;
    private bool m_isGraphicSkillFade;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientBattle battle, ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
    }

    public override void Tick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateGraphicPosition(float zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 ComputeGraphicPosition(Vector2 p, float zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void Draw()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOpened(bool isOpened)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsOpened()
    {
      return this.m_isOpened;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicSkillFade(bool fade)
    {
      // ISSUE: unable to decompile the method.
    }

    public GridPosition Position
    {
      get
      {
        return this.m_position;
      }
    }

    public ConfigDataBattleTreasureInfo BattleTreasureInfo
    {
      get
      {
        return this.m_battleTreasureInfo;
      }
    }
  }
}
