﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientBattleActorList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientBattleActorList
  {
    public static void RemoveAll(List<ClientBattleActor> list)
    {
      EntityList.RemoveAll<ClientBattleActor>(list);
    }

    public static void RemoveDeleted(List<ClientBattleActor> list)
    {
      EntityList.RemoveDeleted<ClientBattleActor>(list);
    }

    public static void Tick(List<ClientBattleActor> list)
    {
      EntityList.Tick<ClientBattleActor>(list);
    }

    public static void TickGraphic(List<ClientBattleActor> list, float dt)
    {
      EntityList.TickGraphic<ClientBattleActor>(list, dt);
    }

    public static void Draw(List<ClientBattleActor> list)
    {
      EntityList.Draw<ClientBattleActor>(list);
    }

    public static void Pause(List<ClientBattleActor> list, bool pause)
    {
      EntityList.Pause<ClientBattleActor>(list, pause);
    }
  }
}
