﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Art;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.UI;
using FixMath.NET;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [HotFix]
  public class ClientBattle : IBattleListener, IFxEventListener, IGenericGraphicContainer
  {
    private ClientBattleState m_state;
    private int m_entityIdCounter;
    private bool m_isStopBattleWin;
    private bool m_isLoadingCombatCharImages;
    private int m_frameCount;
    private int m_endCountdown;
    private int m_cutscenePauseCountdown;
    private int m_restoreNonSkillTargetsCountdown;
    private string m_tickSoundName;
    private float m_battleTickTime;
    private float m_combatTickTime;
    private float m_timeScale;
    private float m_globalTimeScale;
    private SlowMotionState m_slowMotionState;
    private float m_slowMotionCountdown;
    private bool m_isPaused;
    private bool m_isAutoBattle;
    private bool m_isFastBattle;
    private bool m_isEnableDebugDraw;
    private bool m_isEnableSdkLogBattle;
    private RandomNumber m_randomNumber;
    private BattleBase m_battle;
    private ClientBattleActor m_nullActor;
    private List<ClientBattleActor> m_actors;
    private ClientBattleActor m_activeActor;
    private ClientBattleActor m_cameraFollowActor;
    private List<ClientActorAct> m_actorActs;
    private List<ClientBattleTreasure> m_treasures;
    private List<int> m_enforceActionOrderHeroIds;
    private BattleActor m_combatingBattleActorA;
    private BattleActor m_combatingBattleActorB;
    private ConfigDataSkillInfo m_combatSkillInfoA;
    private int m_ignoreMoveStep;
    private int m_ignoreSkillStep;
    private int m_ignoreTeleportDisappearStep;
    private int m_ignoreActiveTeam;
    private int m_ignoreActiveTurn;
    private ClientActorActSkillHit m_actorActSkillHit;
    private ClientActorActSkillHit m_actorActSkillRebound;
    private ConfigDataBattleDialogInfo m_curBattleDialogInfo;
    private int m_battleDialogResult;
    private bool m_isWaitBattleTreasureDialog;
    private bool m_isWaitBattleTreasureReward;
    private bool m_isWaitFastCombat;
    private bool m_isBattlePerforming;
    private int m_myPlayerIndex;
    private int m_myPlayerTeam;
    private int m_rebuildBattleStepMax;
    private SkipCombatMode m_skipCombatMode;
    private SkipCombatMode m_curSkipCombatMode;
    private IConfigDataLoader m_configDataLoader;
    private IClientBattleListener m_clientBattleListener;
    private ConfigDataBattleInfo m_battleInfo;
    private ConfigDataArenaBattleInfo m_arenaBattleInfo;
    private ConfigDataPVPBattleInfo m_pvpBattleInfo;
    private ConfigDataRealTimePVPBattleInfo m_realtimePvpBattleInfo;
    private BattleType m_battleType;
    private BattleCamera m_battleCamera;
    private CombatCamera m_combatCamera;
    private CutsceneCamera m_cutsceneCamera;
    private GameObject m_battleRoot;
    private GameObject m_combatRoot;
    private GameObject m_cutsceneRoot;
    private GameObject m_battleGraphicRoot;
    private GameObject m_combatGraphicRoot;
    private GameObject m_cutsceneGraphicRoot;
    private GameObject m_mapRoot;
    private GameObject m_mapBackground;
    private GameObject m_mapTerrainFxRoot;
    private GameObject m_mapTreasureRoot;
    private Dictionary<GridPosition, GameObject> m_mapTerrainFxs;
    private GameObject m_battleUIRoot;
    private GameObject m_battleActorUIRoot;
    private GraphicPool m_graphicPool;
    private GraphicPool m_fxPool;
    private FxPlayer m_battleFxPlayer;
    private FxPlayer m_combatFxPlayer;
    private FxPlayer m_cutscenePlayer;
    private GameObjectPool2<BattleActorUIController> m_battleActorUIControllerPool;
    [DoNotToLua]
    private ClientBattle.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Dispose_hotfix;
    private LuaFunction m_InitializeIClientBattleListenerGameObject_hotfix;
    private LuaFunction m_TickSingle_hotfix;
    private LuaFunction m_TestPathFind_hotfix;
    private LuaFunction m_TickSlowMotionSingle_hotfix;
    private LuaFunction m_TickCombat_hotfix;
    private LuaFunction m_TickClientBattle_hotfix;
    private LuaFunction m_TickClientBattle_Play_hotfix;
    private LuaFunction m_TickClientBattle_PreStartCombat_hotfix;
    private LuaFunction m_TickClientBattle_Stop_hotfix;
    private LuaFunction m_StartCombat_hotfix;
    private LuaFunction m_StopCombat_hotfix;
    private LuaFunction m_SetTimeScaleSingle_hotfix;
    private LuaFunction m_SetGlobalTimeScaleSingle_hotfix;
    private LuaFunction m_UpdateFinalTimeScale_hotfix;
    private LuaFunction m_TickGraphicSingle_hotfix;
    private LuaFunction m_Draw_hotfix;
    private LuaFunction m_Clear_hotfix;
    private LuaFunction m_CreateMapConfigDataBattleInfoBattleTypeInt32_hotfix;
    private LuaFunction m_CreateArenaMapConfigDataArenaBattleInfoInt32_hotfix;
    private LuaFunction m_CreatePVPMapConfigDataPVPBattleInfoInt32_hotfix;
    private LuaFunction m_CreateRealTimePVPMapConfigDataRealTimePVPBattleInfoInt32_hotfix;
    private LuaFunction m__CreateMapConfigDataBattlefieldInfoInt32Int32_hotfix;
    private LuaFunction m_ResetMap_hotfix;
    private LuaFunction m_StartBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32Int32Int32Int32Int32List`1List`1_hotfix;
    private LuaFunction m_StartArenaBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32ConfigDataArenaDefendRuleInfo_hotfix;
    private LuaFunction m_StartPVPBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32_hotfix;
    private LuaFunction m_StartRealTimePVPBattleTeamSetupBattleTeamSetupBattlePlayerbeInt32_hotfix;
    private LuaFunction m_IsTeamBattle_hotfix;
    private LuaFunction m_SetEnforceActionOrderHerosList`1_hotfix;
    private LuaFunction m_FirstStep_hotfix;
    private LuaFunction m_StopBooleanBoolean_hotfix;
    private LuaFunction m_GetWinConditionTargetPositionConfigDataBattleWinConditionInfo_hotfix;
    private LuaFunction m_GetLoseConditionTargetPositionConfigDataBattleLoseConditionInfo_hotfix;
    private LuaFunction m_PauseBoolean_hotfix;
    private LuaFunction m_SetAutoBattleBoolean_hotfix;
    private LuaFunction m_SetFastBattleBoolean_hotfix;
    private LuaFunction m_StartBattleDialogConfigDataBattleDialogInfo_hotfix;
    private LuaFunction m_StopBattleDialogInt32_hotfix;
    private LuaFunction m_GetBattleDialogResult_hotfix;
    private LuaFunction m_IsWaitBattleDialog_hotfix;
    private LuaFunction m_StartBattleTreasureDialogConfigDataBattleTreasureInfo_hotfix;
    private LuaFunction m_StopBattleTreasureDialog_hotfix;
    private LuaFunction m_IsWaitBattleTreasureDialog_hotfix;
    private LuaFunction m_StartBattleTreasureRewardConfigDataBattleTreasureInfo_hotfix;
    private LuaFunction m_StopBattleTreasureReward_hotfix;
    private LuaFunction m_IsWaitBattleTreasureReward_hotfix;
    private LuaFunction m_StartFastCombatFastCombatActorInfoFastCombatActorInfo_hotfix;
    private LuaFunction m_StopFastCombat_hotfix;
    private LuaFunction m_IsWaitFastCombat_hotfix;
    private LuaFunction m_StartBattlePerform_hotfix;
    private LuaFunction m_StopBattlePerform_hotfix;
    private LuaFunction m_IsBattlePerforming_hotfix;
    private LuaFunction m_FadeNonSkillTargetsClientBattleActorConfigDataSkillInfoGridPosition_hotfix;
    private LuaFunction m_RestoreNonSkillTargets_hotfix;
    private LuaFunction m_GetActors_hotfix;
    private LuaFunction m_GetActorBattleActor_hotfix;
    private LuaFunction m_IsNeedTargetIconBattleActor_hotfix;
    private LuaFunction m_CameraFocusActorClientBattleActor_hotfix;
    private LuaFunction m_CameraFocusPositionGridPosition_hotfix;
    private LuaFunction m_CameraFollowActorClientBattleActor_hotfix;
    private LuaFunction m_IsCameraFocusing_hotfix;
    private LuaFunction m_ComputeTotalHealthPointInt32_hotfix;
    private LuaFunction m_ComputeTotalHealthPointMaxInt32_hotfix;
    private LuaFunction m_EndAllActionInt32_hotfix;
    private LuaFunction m_IgnoreMoveStepInt32_hotfix;
    private LuaFunction m_IgnoreSkillStepInt32_hotfix;
    private LuaFunction m_IgnoreTeleportDisappearStepInt32_hotfix;
    private LuaFunction m_GridPositionToWorldPositionGridPosition_hotfix;
    private LuaFunction m_WorldPositionToGridPositionVector2_hotfix;
    private LuaFunction m_ScreenPositionToWorldPositionVector2_hotfix;
    private LuaFunction m_ScreenPositionToGridPositionVector2_hotfix;
    private LuaFunction m_GridPositionToScreenPositionGridPosition_hotfix;
    private LuaFunction m_DrawLineVector3Vector3Color_hotfix;
    private LuaFunction m_DrawMap_hotfix;
    private LuaFunction m_DrawCellGridPositionBattleActorConfigDataTerrainInfo_hotfix;
    private LuaFunction m_DrawGridGridPositionSingleColorBoolean_hotfix;
    private LuaFunction m_CreateBattleGraphicStringSingle_hotfix;
    private LuaFunction m_DestroyBattleGraphicGenericGraphic_hotfix;
    private LuaFunction m_GetCurrentCamera_hotfix;
    private LuaFunction m_BattleActorTryMoveBattleActorGridPositionInt32_hotfix;
    private LuaFunction m_CreateActorBattleActor_hotfix;
    private LuaFunction m_CreateTreasureConfigDataBattleTreasureInfoBoolean_hotfix;
    private LuaFunction m_GetTreasureConfigDataBattleTreasureInfo_hotfix;
    private LuaFunction m_GetNextEntityId_hotfix;
    private LuaFunction m_PlayScreenEffectString_hotfix;
    private LuaFunction m_CreateMapBackgroundConfigDataBattlefieldInfoGameObject_hotfix;
    private LuaFunction m_ClearMapBackground_hotfix;
    private LuaFunction m_CreateMapTerrainFxGameObject_hotfix;
    private LuaFunction m_ClearMapTerrainFx_hotfix;
    private LuaFunction m_AddMapTerrainFxGridPositionConfigDataTerrainInfoGameObject_hotfix;
    private LuaFunction m_ChangeMapTerrainFxGridPositionConfigDataTerrainInfo_hotfix;
    private LuaFunction m_CreateBattleActorUIController_hotfix;
    private LuaFunction m_DestroyBattleActorUIControllerBattleActorUIController_hotfix;
    private LuaFunction m_RebuildBattleLocalProcessingBattleData_hotfix;
    private LuaFunction m_RebuildBattleList`1Int32Int32_hotfix;
    private LuaFunction m_IsRebuildingBattle_hotfix;
    private LuaFunction m_StopRebuildingBattle_hotfix;
    private LuaFunction m_SetSkipCombatModeSkipCombatMode_hotfix;
    private LuaFunction m_IsSkipCombatMode_hotfix;
    private LuaFunction m_IsSkippingCombatBoolean_hotfix;
    private LuaFunction m_ComputeCombatArmyRelationValueBattleActorBattleActorBoolean_hotfix;
    private LuaFunction m__ComputeCombatArmyRelationValueBattleActorBattleActorBoolean_hotfix;
    private LuaFunction m_EnableSdkLogBattleBoolean_hotfix;
    private LuaFunction m_GetMyPlayerIndex_hotfix;
    private LuaFunction m_GetMyPlayerTeamNumber_hotfix;
    private LuaFunction m_get_Battle_hotfix;
    private LuaFunction m_get_BattleCamera_hotfix;
    private LuaFunction m_get_CombatCamera_hotfix;
    private LuaFunction m_get_State_hotfix;
    private LuaFunction m_set_EnableDebugDrawBoolean_hotfix;
    private LuaFunction m_get_EnableDebugDraw_hotfix;
    private LuaFunction m_get_IsAutoBattle_hotfix;
    private LuaFunction m_get_IsFastBattle_hotfix;
    private LuaFunction m_get_SkipCombatMode_hotfix;
    private LuaFunction m_get_IsPaused_hotfix;
    private LuaFunction m_get_BattleGraphicRoot_hotfix;
    private LuaFunction m_get_CombatGraphicRoot_hotfix;
    private LuaFunction m_get_MapTreasureRoot_hotfix;
    private LuaFunction m_get_BattleActorUIRoot_hotfix;
    private LuaFunction m_get_BattleFxPlayer_hotfix;
    private LuaFunction m_get_CombatFxPlayer_hotfix;
    private LuaFunction m_get_ConfigDataLoader_hotfix;
    private LuaFunction m_get_ClientBattleListener_hotfix;
    private LuaFunction m_get_RandomNumber_hotfix;
    private LuaFunction m_AppendActToActor_hotfix;
    private LuaFunction m_AppendActsToActorInt32Type_hotfix;
    private LuaFunction m_AppendActsToActorInt32TypeClientBattleActor_hotfix;
    private LuaFunction m_IsAnyActorHasAct_hotfix;
    private LuaFunction m_HasWaitingActBattleActorInt32_hotfix;
    private LuaFunction m_OnActorActiveClientBattleActorBooleanInt32Int32_hotfix;
    private LuaFunction m_OnActorPreStartCombatClientBattleActor_hotfix;
    private LuaFunction m_OnActorStopCombatEndClientBattleActor_hotfix;
    private LuaFunction m_OnActorCastSkillClientBattleActorConfigDataSkillInfoGridPosition_hotfix;
    private LuaFunction m_OnActorCastSkillEndClientBattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_OnBattleActorCreateBattleActorBoolean_hotfix;
    private LuaFunction m_OnBattleActorCreateEndBattleActor_hotfix;
    private LuaFunction m_OnBattleActorActiveBattleActorBoolean_hotfix;
    private LuaFunction m_OnBattleActorActionBeginBattleActor_hotfix;
    private LuaFunction m_OnBattleActorActionEndBattleActor_hotfix;
    private LuaFunction m_OnBattleActorMoveBattleActorGridPositionInt32_hotfix;
    private LuaFunction m_OnBattleActorPerformMoveBattleActorGridPositionInt32Boolean_hotfix;
    private LuaFunction m_OnBattleActorPunchMoveBattleActorStringBoolean_hotfix;
    private LuaFunction m_OnBattleActorExchangeMoveBattleActorBattleActorInt32String_hotfix;
    private LuaFunction m_OnBattleActorSetDirBattleActorInt32_hotfix;
    private LuaFunction m_OnBattleActorPlayFxBattleActorStringInt32_hotfix;
    private LuaFunction m_OnBattleActorPlayAnimationBattleActorStringInt32_hotfix;
    private LuaFunction m_OnBattleActorChangeIdleAnimationBattleActorString_hotfix;
    private LuaFunction m_OnBattleActorSkillBattleActorConfigDataSkillInfoGridPosition_hotfix;
    private LuaFunction m_OnBattleActorSkillHitBeginBattleActorConfigDataSkillInfoBoolean_hotfix;
    private LuaFunction m_OnBattleActorSkillHitBattleActorConfigDataSkillInfoInt32Int32DamageNumberTypeBoolean_hotfix;
    private LuaFunction m_OnBattleActorSkillHitEndBattleActorConfigDataSkillInfoBoolean_hotfix;
    private LuaFunction m_OnBattleActorAttachBuffBattleActorBuffState_hotfix;
    private LuaFunction m_OnBattleActorDetachBuffBattleActorBuffState_hotfix;
    private LuaFunction m_OnBattleActorImmuneBattleActor_hotfix;
    private LuaFunction m_OnBattleActorPassiveSkillBattleActorBattleActorBuffState_hotfix;
    private LuaFunction m_OnBattleActorBuffHitBattleActorBuffStateInt32Int32DamageNumberType_hotfix;
    private LuaFunction m_OnBattleActorTerrainHitBattleActorConfigDataTerrainInfoInt32Int32DamageNumberType_hotfix;
    private LuaFunction m_OnBattleActorTeleportBattleActorConfigDataSkillInfoGridPosition_hotfix;
    private LuaFunction m_OnBattleActorTeleportDisappearBattleActorConfigDataSkillInfoGridPosition_hotfix;
    private LuaFunction m_OnBattleActorTeleportAppearBattleActorConfigDataSkillInfoGridPosition_hotfix;
    private LuaFunction m_OnBattleActorSummonBattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_OnBattleActorDieBattleActorBoolean_hotfix;
    private LuaFunction m_OnBattleActorAppearBattleActorInt32String_hotfix;
    private LuaFunction m_OnBattleActorDisappearBattleActorInt32String_hotfix;
    private LuaFunction m_OnBattleActorChangeTeamBattleActor_hotfix;
    private LuaFunction m_OnBattleActorChangeArmyBattleActor_hotfix;
    private LuaFunction m_OnBattleActorReplaceBattleActorBattleActorString_hotfix;
    private LuaFunction m_OnBattleActorCameraFocusBattleActor_hotfix;
    private LuaFunction m_OnBattleActorGainBattleTreasureBattleActorConfigDataBattleTreasureInfo_hotfix;
    private LuaFunction m_OnStartGuardBattleActorBattleActor_hotfix;
    private LuaFunction m_OnStopGuardBattleActorBattleActor_hotfix;
    private LuaFunction m_OnBeforeStartCombatBattleActorBattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_OnCancelCombat_hotfix;
    private LuaFunction m_OnStartCombatBattleActorBattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_OnPreStopCombat_hotfix;
    private LuaFunction m_OnStopCombatInt32Int32BooleanInt32Int32Boolean_hotfix;
    private LuaFunction m_OnBattleNextTurnInt32_hotfix;
    private LuaFunction m_OnBattleNextTeamInt32Boolean_hotfix;
    private LuaFunction m_OnBattleNextPlayerInt32Int32_hotfix;
    private LuaFunction m_OnBattleNextActorBattleActor_hotfix;
    private LuaFunction m_OnCombatActorHitCombatActorCombatActorConfigDataSkillInfoInt32Int32DamageNumberType_hotfix;
    private LuaFunction m_OnCombatActorDieCombatActor_hotfix;
    private LuaFunction m_OnStartSkillCutsceneConfigDataSkillInfoConfigDataCutsceneInfoInt32_hotfix;
    private LuaFunction m_OnStartPassiveSkillCutsceneBuffStateInt32_hotfix;
    private LuaFunction m_OnStopSkillCutscene_hotfix;
    private LuaFunction m_OnStartBattleDialogConfigDataBattleDialogInfo_hotfix;
    private LuaFunction m_OnStartBattlePerformConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_OnStopBattlePerform_hotfix;
    private LuaFunction m_OnChangeMapTerrainGridPositionConfigDataTerrainInfo_hotfix;
    private LuaFunction m_OnCameraFocusGridPosition_hotfix;
    private LuaFunction m_OnPlayMusicString_hotfix;
    private LuaFunction m_OnPlaySoundString_hotfix;
    private LuaFunction m_OnPlayFxStringGridPosition_hotfix;
    private LuaFunction m_OnWaitTimeInt32_hotfix;
    private LuaFunction m_OnBattleTreasureCreateConfigDataBattleTreasureInfoBoolean_hotfix;
    private LuaFunction m_CreateCombatGraphicStringSingle_hotfix;
    private LuaFunction m_DestroyCombatGraphicIBattleGraphic_hotfix;
    private LuaFunction m_PlayFxStringSingle_hotfix;
    private LuaFunction m_PlaySoundString_hotfix;
    private LuaFunction m_PlaySoundSoundTableId_hotfix;
    private LuaFunction m_DrawLineVector2iVector2iColori_hotfix;
    private LuaFunction m_DrawLineVector2iFix64Vector2iFix64Colori_hotfix;
    private LuaFunction m_IsCombatGraphicMirrorX_hotfix;
    private LuaFunction m_OnAudioFxEventAudioClip_hotfix;
    private LuaFunction m_OnSoundFxEventString_hotfix;
    private LuaFunction m_OnCameraEffectFxEventString_hotfix;
    private LuaFunction m_OnScreenEffectFxEventString_hotfix;
    private LuaFunction m_OnGeneralFxEventString_hotfix;
    private LuaFunction m_GetCameraPosition_hotfix;
    private LuaFunction m_CombatPositionToWorldPositionVector2iFix64Boolean_hotfix;
    private LuaFunction m_IsCulledVector2Vector2Boolean_hotfix;
    private LuaFunction m_LogBattleStart_hotfix;
    private LuaFunction m_LogBattleStopBoolean_hotfix;
    private LuaFunction m_LogBattleTeamBattleTeamBattleTeam_hotfix;
    private LuaFunction m_LogActorMoveBattleActorGridPositionGridPosition_hotfix;
    private LuaFunction m_LogActorStandbyBattleActor_hotfix;
    private LuaFunction m_LogActorAttackBattleActorBattleActor_hotfix;
    private LuaFunction m_LogActorSkillBattleActorConfigDataSkillInfoBattleActorGridPosition_hotfix;
    private LuaFunction m_LogActorDieBattleActorBattleActor_hotfix;
    private LuaFunction m_SdkLogBattleStringObject_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(IClientBattleListener clientBattleListener, GameObject root)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestPathFind()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickSlowMotion(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientBattle_Play()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientBattle_PreStartCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientBattle_Stop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTimeScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGlobalTimeScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFinalTimeScale()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateMap(
      ConfigDataBattleInfo battleInfo,
      BattleType battleType,
      int myPlayerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateArenaMap(ConfigDataArenaBattleInfo battleInfo, int myPlayerTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreatePVPMap(ConfigDataPVPBattleInfo battleInfo, int myPlayerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateRealTimePVPMap(ConfigDataRealTimePVPBattleInfo battleInfo, int myPlayerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _CreateMap(ConfigDataBattlefieldInfo battlefieldInfo, int cameraX, int cameraY)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      int armyRandomSeed,
      int monsterLevel,
      int starTurnMax,
      int starDeadMax,
      List<ConfigDataBattleAchievementRelatedInfo> achievements,
      List<int> gainBattleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartArena(
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      ConfigDataArenaDefendRuleInfo arenaDefendRuleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartPVP(
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartRealTimePVP(
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTeamBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEnforceActionOrderHeros(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FirstStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Stop(bool win, bool skipPerform)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition GetWinConditionTargetPosition(
      ConfigDataBattleWinConditionInfo winConditionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition GetLoseConditionTargetPosition(
      ConfigDataBattleLoseConditionInfo loseConditionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAutoBattle(bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFastBattle(bool fastBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattleDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattleDialog(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattleDialogResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWaitBattleDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattleTreasureDialog(ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattleTreasureDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWaitBattleTreasureDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattleTreasureReward(ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattleTreasureReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWaitBattleTreasureReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartFastCombat(FastCombatActorInfo a, FastCombatActorInfo b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopFastCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWaitFastCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattlePerform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattlePerform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBattlePerforming()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FadeNonSkillTargets(
      ClientBattleActor ca,
      ConfigDataSkillInfo skillInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RestoreNonSkillTargets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ClientBattleActor> GetActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleActor GetActor(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsNeedTargetIcon(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CameraFocusActor(ClientBattleActor ca)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CameraFocusPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CameraFollowActor(ClientBattleActor ca)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCameraFocusing()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeTotalHealthPoint(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeTotalHealthPointMax(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EndAllAction(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IgnoreMoveStep(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IgnoreSkillStep(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IgnoreTeleportDisappearStep(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 GridPositionToWorldPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition WorldPositionToGridPosition(Vector2 sp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 ScreenPositionToWorldPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition ScreenPositionToGridPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 GridPositionToScreenPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DrawLine(Vector3 p0, Vector3 p1, Color color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrawMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrawCell(GridPosition p, BattleActor actor, ConfigDataTerrainInfo terrain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DrawGrid(GridPosition p, float scale, Color color, bool cross = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic CreateBattleGraphic(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyBattleGraphic(GenericGraphic graphic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CameraBase GetCurrentCamera()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BattleActorTryMove(BattleActor a, GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleActor CreateActor(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientBattleTreasure CreateTreasure(
      ConfigDataBattleTreasureInfo treasureInfo,
      bool isOpened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleTreasure GetTreasure(
      ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextEntityId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayScreenEffect(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateMapBackground(ConfigDataBattlefieldInfo battlefieldInfo, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapBackground()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateMapTerrainFx(GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapTerrainFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddMapTerrainFx(
      GridPosition p,
      ConfigDataTerrainInfo terrainInfo,
      GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeMapTerrainFx(GridPosition p, ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorUIController CreateBattleActorUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyBattleActorUIController(BattleActorUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RebuildBattle(LocalProcessingBattleData data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RebuildBattle(List<BattleCommand> commands, int fromStep = 0, int toStep = 2147483647)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRebuildingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopRebuildingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkipCombatMode(SkipCombatMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSkipCombatMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSkippingCombat(bool checkState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeCombatArmyRelationValue(BattleActor a, BattleActor b, bool isMagic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int _ComputeCombatArmyRelationValue(BattleActor a, BattleActor b, bool isMagic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void _ComputeArmyRelationValue(ArmyRelationData r, bool isMagic, ref int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsCombatMagicAttack(
      BattleActor attacker,
      BattleActor target,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableSdkLogBattle(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMyPlayerIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMyPlayerTeamNumber()
    {
      // ISSUE: unable to decompile the method.
    }

    public static int FrameToMillisecond(int frame)
    {
      return frame * 1000 / 30;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame1(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleBase Battle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleCamera BattleCamera
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CombatCamera CombatCamera
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ClientBattleState State
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool EnableDebugDraw
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAutoBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsFastBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public SkipCombatMode SkipCombatMode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPaused
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject BattleGraphicRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject CombatGraphicRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject MapTreasureRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject BattleActorUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public FxPlayer BattleFxPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public FxPlayer CombatFxPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IClientBattleListener ClientBattleListener
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomNumber RandomNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendActToActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendActsToActor(int step, System.Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendActsToActor(int step, System.Type type, ClientBattleActor ca)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyActorHasAct()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasWaitingAct(BattleActor a, int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorActive(ClientBattleActor a, bool newStep, int step, int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorPreStartCombat(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorStopCombatEnd(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorCastSkill(
      ClientBattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorCastSkillEnd(ClientBattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorCreate(BattleActor a, bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorCreateEnd(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorActive(BattleActor a, bool newStep)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorActionBegin(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorActionEnd(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorMove(BattleActor a, GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPerformMove(
      BattleActor a,
      GridPosition p,
      int dir,
      bool cameraFollow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPunchMove(BattleActor a, string fxName, bool isDragExchange)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorExchangeMove(
      BattleActor a,
      BattleActor b,
      int moveType,
      string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSetDir(BattleActor a, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPlayFx(BattleActor a, string fxName, int attachMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPlayAnimation(BattleActor a, string animationName, int animationTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorChangeIdleAnimation(BattleActor a, string idleAnimationName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSkill(BattleActor a, ConfigDataSkillInfo skill, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSkillHitBegin(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      bool isRebound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSkillHit(
      BattleActor a,
      ConfigDataSkillInfo skill,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType,
      bool isRebound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSkillHitEnd(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      bool isRebound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorAttachBuff(BattleActor a, BuffState buffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorDetachBuff(BattleActor a, BuffState buffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorImmune(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPassiveSkill(
      BattleActor a,
      BattleActor target,
      BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorBuffHit(
      BattleActor a,
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTerrainHit(
      BattleActor a,
      ConfigDataTerrainInfo terrainInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTeleport(BattleActor a, ConfigDataSkillInfo skill, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTeleportDisappear(
      BattleActor a,
      ConfigDataSkillInfo skill,
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTeleportAppear(
      BattleActor a,
      ConfigDataSkillInfo skill,
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSummon(BattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorDie(BattleActor a, bool isAfterCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorAppear(BattleActor a, int effectType, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorDisappear(BattleActor a, int effectType, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorChangeTeam(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorChangeArmy(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorReplace(BattleActor a0, BattleActor a1, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorCameraFocus(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorGainBattleTreasure(
      BattleActor a,
      ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartGuard(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopGuard(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeforeStartCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCancelCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartCombat(BattleActor a, BattleActor b, ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPreStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopCombat(
      int teamAHeroTotalDamage,
      int teamASoldierTotalDamage,
      bool teamACriticalAttack,
      int teamBHeroTotalDamage,
      int teamBSoldierTotalDamage,
      bool teamBCriticalAttack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextTeam(int team, bool isNpc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextPlayer(int prevPlayerIndex, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextActor(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCombatActorHit(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      int totalDamage,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCombatActorDie(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartSkillCutscene(
      ConfigDataSkillInfo skillInfo,
      ConfigDataCutsceneInfo cutsceneInfo,
      int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartPassiveSkillCutscene(BuffState sourceBuffState, int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopSkillCutscene()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartBattleDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartBattlePerform(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopBattlePerform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnChangeMapTerrain(GridPosition p, ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCameraFocus(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlayMusic(string musicName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlaySound(string soundName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlayFx(string fxName, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaitTime(int timeInMs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleTreasureCreate(ConfigDataBattleTreasureInfo treasureInfo, bool isOpened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IBattleGraphic CreateCombatGraphic(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyCombatGraphic(IBattleGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IBattleGraphic PlayFx(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DrawLine(Vector2i p0, Vector2i p1, Colori color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DrawLine(Vector2i p0, Fix64 z0, Vector2i p1, Fix64 z1, Colori color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCombatGraphicMirrorX()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnAudio(FxEvent e, AudioClip a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSound(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCameraEffect(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScreenEffect(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnGeneral(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetCameraPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 CombatPositionToWorldPosition(Vector2i p, Fix64 z, bool computeZOffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCulled(Vector2 bmin, Vector2 bmax, bool isCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogBattleStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogBattleStop(bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogBattleTeam(BattleTeam team0, BattleTeam team1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogActorMove(BattleActor actor, GridPosition fromPos, GridPosition toPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogActorStandby(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogActorAttack(BattleActor actor, BattleActor targetActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogActorSkill(
      BattleActor actor,
      ConfigDataSkillInfo skillInfo,
      BattleActor targetActor,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogActorDie(BattleActor actor, BattleActor killerActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SdkLogBattle(string eventId, object logData)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public ClientBattle.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [CustomLuaClass]
    public class LogDataBattleStart
    {
      public string Type;
      public int BattleId;

      [MethodImpl((MethodImplOptions) 32768)]
      public LogDataBattleStart()
      {
      }
    }

    [CustomLuaClass]
    public class LogDataArenaBattleStart
    {
      public string Type = "ArenaBattleStart";
      public int ArenaBattleId;
    }

    [CustomLuaClass]
    public class LogDataPVPBattleStart
    {
      public string Type = "PVPBattleStart";
      public int PVPBattleId;
    }

    [CustomLuaClass]
    public class LogDataRealTimePVPBattleStart
    {
      public string Type = "RealTimePVPBattleStart";
      public int RealTimePVPBattleId;
    }

    [CustomLuaClass]
    public class LogDataBattleStop
    {
      public string Type = "BattleStop";
      public bool IsWin;
    }

    [CustomLuaClass]
    public class LogDataBattleTeam
    {
      public string Type = "Team";
      public List<int> HeroId = new List<int>();
      public List<GridPosition> Pos = new List<GridPosition>();
    }

    [CustomLuaClass]
    public class LogDataActorMove
    {
      public string Type = "Move";
      public int Turn;
      public int HeroId;
      public GridPosition FromPos;
      public GridPosition ToPos;
    }

    [CustomLuaClass]
    public class LogDataActorStandby
    {
      public string Type = "Standby";
      public int Turn;
      public int HeroId;
      public GridPosition Pos;
    }

    [CustomLuaClass]
    public class LogDataActorAttack
    {
      public string Type = "Attack";
      public int Turn;
      public int HeroId;
      public int TargetHeroId;
      public GridPosition TargetPos;
    }

    [CustomLuaClass]
    public class LogDataActorSkill
    {
      public string Type = "Skill";
      public int Turn;
      public int HeroId;
      public int TargetHeroId;
      public GridPosition TargetPos;
      public int SkillId;
    }

    [CustomLuaClass]
    public class LogDataActorDie
    {
      public string Type = "Die";
      public int Turn;
      public int HeroId;
      public int KillerHeroId;
    }

    public class LuaExportHelper
    {
      private ClientBattle m_owner;

      public LuaExportHelper(ClientBattle owner)
      {
        this.m_owner = owner;
      }

      public ClientBattleState m_state
      {
        get
        {
          return this.m_owner.m_state;
        }
        set
        {
          this.m_owner.m_state = value;
        }
      }

      public int m_entityIdCounter
      {
        get
        {
          return this.m_owner.m_entityIdCounter;
        }
        set
        {
          this.m_owner.m_entityIdCounter = value;
        }
      }

      public bool m_isStopBattleWin
      {
        get
        {
          return this.m_owner.m_isStopBattleWin;
        }
        set
        {
          this.m_owner.m_isStopBattleWin = value;
        }
      }

      public bool m_isLoadingCombatCharImages
      {
        get
        {
          return this.m_owner.m_isLoadingCombatCharImages;
        }
        set
        {
          this.m_owner.m_isLoadingCombatCharImages = value;
        }
      }

      public int m_frameCount
      {
        get
        {
          return this.m_owner.m_frameCount;
        }
        set
        {
          this.m_owner.m_frameCount = value;
        }
      }

      public int m_endCountdown
      {
        get
        {
          return this.m_owner.m_endCountdown;
        }
        set
        {
          this.m_owner.m_endCountdown = value;
        }
      }

      public int m_cutscenePauseCountdown
      {
        get
        {
          return this.m_owner.m_cutscenePauseCountdown;
        }
        set
        {
          this.m_owner.m_cutscenePauseCountdown = value;
        }
      }

      public int m_restoreNonSkillTargetsCountdown
      {
        get
        {
          return this.m_owner.m_restoreNonSkillTargetsCountdown;
        }
        set
        {
          this.m_owner.m_restoreNonSkillTargetsCountdown = value;
        }
      }

      public string m_tickSoundName
      {
        get
        {
          return this.m_owner.m_tickSoundName;
        }
        set
        {
          this.m_owner.m_tickSoundName = value;
        }
      }

      public float m_battleTickTime
      {
        get
        {
          return this.m_owner.m_battleTickTime;
        }
        set
        {
          this.m_owner.m_battleTickTime = value;
        }
      }

      public float m_combatTickTime
      {
        get
        {
          return this.m_owner.m_combatTickTime;
        }
        set
        {
          this.m_owner.m_combatTickTime = value;
        }
      }

      public float m_timeScale
      {
        get
        {
          return this.m_owner.m_timeScale;
        }
        set
        {
          this.m_owner.m_timeScale = value;
        }
      }

      public float m_globalTimeScale
      {
        get
        {
          return this.m_owner.m_globalTimeScale;
        }
        set
        {
          this.m_owner.m_globalTimeScale = value;
        }
      }

      public SlowMotionState m_slowMotionState
      {
        get
        {
          return this.m_owner.m_slowMotionState;
        }
        set
        {
          this.m_owner.m_slowMotionState = value;
        }
      }

      public float m_slowMotionCountdown
      {
        get
        {
          return this.m_owner.m_slowMotionCountdown;
        }
        set
        {
          this.m_owner.m_slowMotionCountdown = value;
        }
      }

      public bool m_isPaused
      {
        get
        {
          return this.m_owner.m_isPaused;
        }
        set
        {
          this.m_owner.m_isPaused = value;
        }
      }

      public bool m_isAutoBattle
      {
        get
        {
          return this.m_owner.m_isAutoBattle;
        }
        set
        {
          this.m_owner.m_isAutoBattle = value;
        }
      }

      public bool m_isFastBattle
      {
        get
        {
          return this.m_owner.m_isFastBattle;
        }
        set
        {
          this.m_owner.m_isFastBattle = value;
        }
      }

      public bool m_isEnableDebugDraw
      {
        get
        {
          return this.m_owner.m_isEnableDebugDraw;
        }
        set
        {
          this.m_owner.m_isEnableDebugDraw = value;
        }
      }

      public bool m_isEnableSdkLogBattle
      {
        get
        {
          return this.m_owner.m_isEnableSdkLogBattle;
        }
        set
        {
          this.m_owner.m_isEnableSdkLogBattle = value;
        }
      }

      public RandomNumber m_randomNumber
      {
        get
        {
          return this.m_owner.m_randomNumber;
        }
        set
        {
          this.m_owner.m_randomNumber = value;
        }
      }

      public BattleBase m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public ClientBattleActor m_nullActor
      {
        get
        {
          return this.m_owner.m_nullActor;
        }
        set
        {
          this.m_owner.m_nullActor = value;
        }
      }

      public List<ClientBattleActor> m_actors
      {
        get
        {
          return this.m_owner.m_actors;
        }
        set
        {
          this.m_owner.m_actors = value;
        }
      }

      public ClientBattleActor m_activeActor
      {
        get
        {
          return this.m_owner.m_activeActor;
        }
        set
        {
          this.m_owner.m_activeActor = value;
        }
      }

      public ClientBattleActor m_cameraFollowActor
      {
        get
        {
          return this.m_owner.m_cameraFollowActor;
        }
        set
        {
          this.m_owner.m_cameraFollowActor = value;
        }
      }

      public List<ClientActorAct> m_actorActs
      {
        get
        {
          return this.m_owner.m_actorActs;
        }
        set
        {
          this.m_owner.m_actorActs = value;
        }
      }

      public List<ClientBattleTreasure> m_treasures
      {
        get
        {
          return this.m_owner.m_treasures;
        }
        set
        {
          this.m_owner.m_treasures = value;
        }
      }

      public List<int> m_enforceActionOrderHeroIds
      {
        get
        {
          return this.m_owner.m_enforceActionOrderHeroIds;
        }
        set
        {
          this.m_owner.m_enforceActionOrderHeroIds = value;
        }
      }

      public BattleActor m_combatingBattleActorA
      {
        get
        {
          return this.m_owner.m_combatingBattleActorA;
        }
        set
        {
          this.m_owner.m_combatingBattleActorA = value;
        }
      }

      public BattleActor m_combatingBattleActorB
      {
        get
        {
          return this.m_owner.m_combatingBattleActorB;
        }
        set
        {
          this.m_owner.m_combatingBattleActorB = value;
        }
      }

      public ConfigDataSkillInfo m_combatSkillInfoA
      {
        get
        {
          return this.m_owner.m_combatSkillInfoA;
        }
        set
        {
          this.m_owner.m_combatSkillInfoA = value;
        }
      }

      public int m_ignoreMoveStep
      {
        get
        {
          return this.m_owner.m_ignoreMoveStep;
        }
        set
        {
          this.m_owner.m_ignoreMoveStep = value;
        }
      }

      public int m_ignoreSkillStep
      {
        get
        {
          return this.m_owner.m_ignoreSkillStep;
        }
        set
        {
          this.m_owner.m_ignoreSkillStep = value;
        }
      }

      public int m_ignoreTeleportDisappearStep
      {
        get
        {
          return this.m_owner.m_ignoreTeleportDisappearStep;
        }
        set
        {
          this.m_owner.m_ignoreTeleportDisappearStep = value;
        }
      }

      public int m_ignoreActiveTeam
      {
        get
        {
          return this.m_owner.m_ignoreActiveTeam;
        }
        set
        {
          this.m_owner.m_ignoreActiveTeam = value;
        }
      }

      public int m_ignoreActiveTurn
      {
        get
        {
          return this.m_owner.m_ignoreActiveTurn;
        }
        set
        {
          this.m_owner.m_ignoreActiveTurn = value;
        }
      }

      public ClientActorActSkillHit m_actorActSkillHit
      {
        get
        {
          return this.m_owner.m_actorActSkillHit;
        }
        set
        {
          this.m_owner.m_actorActSkillHit = value;
        }
      }

      public ClientActorActSkillHit m_actorActSkillRebound
      {
        get
        {
          return this.m_owner.m_actorActSkillRebound;
        }
        set
        {
          this.m_owner.m_actorActSkillRebound = value;
        }
      }

      public ConfigDataBattleDialogInfo m_curBattleDialogInfo
      {
        get
        {
          return this.m_owner.m_curBattleDialogInfo;
        }
        set
        {
          this.m_owner.m_curBattleDialogInfo = value;
        }
      }

      public int m_battleDialogResult
      {
        get
        {
          return this.m_owner.m_battleDialogResult;
        }
        set
        {
          this.m_owner.m_battleDialogResult = value;
        }
      }

      public bool m_isWaitBattleTreasureDialog
      {
        get
        {
          return this.m_owner.m_isWaitBattleTreasureDialog;
        }
        set
        {
          this.m_owner.m_isWaitBattleTreasureDialog = value;
        }
      }

      public bool m_isWaitBattleTreasureReward
      {
        get
        {
          return this.m_owner.m_isWaitBattleTreasureReward;
        }
        set
        {
          this.m_owner.m_isWaitBattleTreasureReward = value;
        }
      }

      public bool m_isWaitFastCombat
      {
        get
        {
          return this.m_owner.m_isWaitFastCombat;
        }
        set
        {
          this.m_owner.m_isWaitFastCombat = value;
        }
      }

      public bool m_isBattlePerforming
      {
        get
        {
          return this.m_owner.m_isBattlePerforming;
        }
        set
        {
          this.m_owner.m_isBattlePerforming = value;
        }
      }

      public int m_myPlayerIndex
      {
        get
        {
          return this.m_owner.m_myPlayerIndex;
        }
        set
        {
          this.m_owner.m_myPlayerIndex = value;
        }
      }

      public int m_myPlayerTeam
      {
        get
        {
          return this.m_owner.m_myPlayerTeam;
        }
        set
        {
          this.m_owner.m_myPlayerTeam = value;
        }
      }

      public int m_rebuildBattleStepMax
      {
        get
        {
          return this.m_owner.m_rebuildBattleStepMax;
        }
        set
        {
          this.m_owner.m_rebuildBattleStepMax = value;
        }
      }

      public SkipCombatMode m_skipCombatMode
      {
        get
        {
          return this.m_owner.m_skipCombatMode;
        }
        set
        {
          this.m_owner.m_skipCombatMode = value;
        }
      }

      public SkipCombatMode m_curSkipCombatMode
      {
        get
        {
          return this.m_owner.m_curSkipCombatMode;
        }
        set
        {
          this.m_owner.m_curSkipCombatMode = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public IClientBattleListener m_clientBattleListener
      {
        get
        {
          return this.m_owner.m_clientBattleListener;
        }
        set
        {
          this.m_owner.m_clientBattleListener = value;
        }
      }

      public ConfigDataBattleInfo m_battleInfo
      {
        get
        {
          return this.m_owner.m_battleInfo;
        }
        set
        {
          this.m_owner.m_battleInfo = value;
        }
      }

      public ConfigDataArenaBattleInfo m_arenaBattleInfo
      {
        get
        {
          return this.m_owner.m_arenaBattleInfo;
        }
        set
        {
          this.m_owner.m_arenaBattleInfo = value;
        }
      }

      public ConfigDataPVPBattleInfo m_pvpBattleInfo
      {
        get
        {
          return this.m_owner.m_pvpBattleInfo;
        }
        set
        {
          this.m_owner.m_pvpBattleInfo = value;
        }
      }

      public ConfigDataRealTimePVPBattleInfo m_realtimePvpBattleInfo
      {
        get
        {
          return this.m_owner.m_realtimePvpBattleInfo;
        }
        set
        {
          this.m_owner.m_realtimePvpBattleInfo = value;
        }
      }

      public BattleType m_battleType
      {
        get
        {
          return this.m_owner.m_battleType;
        }
        set
        {
          this.m_owner.m_battleType = value;
        }
      }

      public BattleCamera m_battleCamera
      {
        get
        {
          return this.m_owner.m_battleCamera;
        }
        set
        {
          this.m_owner.m_battleCamera = value;
        }
      }

      public CombatCamera m_combatCamera
      {
        get
        {
          return this.m_owner.m_combatCamera;
        }
        set
        {
          this.m_owner.m_combatCamera = value;
        }
      }

      public CutsceneCamera m_cutsceneCamera
      {
        get
        {
          return this.m_owner.m_cutsceneCamera;
        }
        set
        {
          this.m_owner.m_cutsceneCamera = value;
        }
      }

      public GameObject m_battleRoot
      {
        get
        {
          return this.m_owner.m_battleRoot;
        }
        set
        {
          this.m_owner.m_battleRoot = value;
        }
      }

      public GameObject m_combatRoot
      {
        get
        {
          return this.m_owner.m_combatRoot;
        }
        set
        {
          this.m_owner.m_combatRoot = value;
        }
      }

      public GameObject m_cutsceneRoot
      {
        get
        {
          return this.m_owner.m_cutsceneRoot;
        }
        set
        {
          this.m_owner.m_cutsceneRoot = value;
        }
      }

      public GameObject m_battleGraphicRoot
      {
        get
        {
          return this.m_owner.m_battleGraphicRoot;
        }
        set
        {
          this.m_owner.m_battleGraphicRoot = value;
        }
      }

      public GameObject m_combatGraphicRoot
      {
        get
        {
          return this.m_owner.m_combatGraphicRoot;
        }
        set
        {
          this.m_owner.m_combatGraphicRoot = value;
        }
      }

      public GameObject m_cutsceneGraphicRoot
      {
        get
        {
          return this.m_owner.m_cutsceneGraphicRoot;
        }
        set
        {
          this.m_owner.m_cutsceneGraphicRoot = value;
        }
      }

      public GameObject m_mapRoot
      {
        get
        {
          return this.m_owner.m_mapRoot;
        }
        set
        {
          this.m_owner.m_mapRoot = value;
        }
      }

      public GameObject m_mapBackground
      {
        get
        {
          return this.m_owner.m_mapBackground;
        }
        set
        {
          this.m_owner.m_mapBackground = value;
        }
      }

      public GameObject m_mapTerrainFxRoot
      {
        get
        {
          return this.m_owner.m_mapTerrainFxRoot;
        }
        set
        {
          this.m_owner.m_mapTerrainFxRoot = value;
        }
      }

      public GameObject m_mapTreasureRoot
      {
        get
        {
          return this.m_owner.m_mapTreasureRoot;
        }
        set
        {
          this.m_owner.m_mapTreasureRoot = value;
        }
      }

      public Dictionary<GridPosition, GameObject> m_mapTerrainFxs
      {
        get
        {
          return this.m_owner.m_mapTerrainFxs;
        }
        set
        {
          this.m_owner.m_mapTerrainFxs = value;
        }
      }

      public GameObject m_battleUIRoot
      {
        get
        {
          return this.m_owner.m_battleUIRoot;
        }
        set
        {
          this.m_owner.m_battleUIRoot = value;
        }
      }

      public GameObject m_battleActorUIRoot
      {
        get
        {
          return this.m_owner.m_battleActorUIRoot;
        }
        set
        {
          this.m_owner.m_battleActorUIRoot = value;
        }
      }

      public GraphicPool m_graphicPool
      {
        get
        {
          return this.m_owner.m_graphicPool;
        }
        set
        {
          this.m_owner.m_graphicPool = value;
        }
      }

      public GraphicPool m_fxPool
      {
        get
        {
          return this.m_owner.m_fxPool;
        }
        set
        {
          this.m_owner.m_fxPool = value;
        }
      }

      public FxPlayer m_battleFxPlayer
      {
        get
        {
          return this.m_owner.m_battleFxPlayer;
        }
        set
        {
          this.m_owner.m_battleFxPlayer = value;
        }
      }

      public FxPlayer m_combatFxPlayer
      {
        get
        {
          return this.m_owner.m_combatFxPlayer;
        }
        set
        {
          this.m_owner.m_combatFxPlayer = value;
        }
      }

      public FxPlayer m_cutscenePlayer
      {
        get
        {
          return this.m_owner.m_cutscenePlayer;
        }
        set
        {
          this.m_owner.m_cutscenePlayer = value;
        }
      }

      public GameObjectPool2<BattleActorUIController> m_battleActorUIControllerPool
      {
        get
        {
          return this.m_owner.m_battleActorUIControllerPool;
        }
        set
        {
          this.m_owner.m_battleActorUIControllerPool = value;
        }
      }

      public void TestPathFind()
      {
        this.m_owner.TestPathFind();
      }

      public void TickSlowMotion(float dt)
      {
        this.m_owner.TickSlowMotion(dt);
      }

      public void TickCombat()
      {
        this.m_owner.TickCombat();
      }

      public void TickClientBattle()
      {
        this.m_owner.TickClientBattle();
      }

      public void TickClientBattle_Play()
      {
        this.m_owner.TickClientBattle_Play();
      }

      public void TickClientBattle_PreStartCombat()
      {
        this.m_owner.TickClientBattle_PreStartCombat();
      }

      public void TickClientBattle_Stop()
      {
        this.m_owner.TickClientBattle_Stop();
      }

      public void StartCombat()
      {
        this.m_owner.StartCombat();
      }

      public void StopCombat()
      {
        this.m_owner.StopCombat();
      }

      public void SetTimeScale(float scale)
      {
        this.m_owner.SetTimeScale(scale);
      }

      public void UpdateFinalTimeScale()
      {
        this.m_owner.UpdateFinalTimeScale();
      }

      public void TickGraphic(float dt)
      {
        this.m_owner.TickGraphic(dt);
      }

      public void Draw()
      {
        this.m_owner.Draw();
      }

      public void _CreateMap(ConfigDataBattlefieldInfo battlefieldInfo, int cameraX, int cameraY)
      {
        this.m_owner._CreateMap(battlefieldInfo, cameraX, cameraY);
      }

      public GridPosition GetWinConditionTargetPosition(
        ConfigDataBattleWinConditionInfo winConditionInfo)
      {
        return this.m_owner.GetWinConditionTargetPosition(winConditionInfo);
      }

      public GridPosition GetLoseConditionTargetPosition(
        ConfigDataBattleLoseConditionInfo loseConditionInfo)
      {
        return this.m_owner.GetLoseConditionTargetPosition(loseConditionInfo);
      }

      public void FadeNonSkillTargets(
        ClientBattleActor ca,
        ConfigDataSkillInfo skillInfo,
        GridPosition targetPos)
      {
        this.m_owner.FadeNonSkillTargets(ca, skillInfo, targetPos);
      }

      public void RestoreNonSkillTargets()
      {
        this.m_owner.RestoreNonSkillTargets();
      }

      public bool IsNeedTargetIcon(BattleActor a)
      {
        return this.m_owner.IsNeedTargetIcon(a);
      }

      public void DrawMap()
      {
        this.m_owner.DrawMap();
      }

      public void DrawCell(GridPosition p, BattleActor actor, ConfigDataTerrainInfo terrain)
      {
        this.m_owner.DrawCell(p, actor, terrain);
      }

      public CameraBase GetCurrentCamera()
      {
        return this.m_owner.GetCurrentCamera();
      }

      public ClientBattleTreasure CreateTreasure(
        ConfigDataBattleTreasureInfo treasureInfo,
        bool isOpened)
      {
        return this.m_owner.CreateTreasure(treasureInfo, isOpened);
      }

      public void CreateMapBackground(ConfigDataBattlefieldInfo battlefieldInfo, GameObject parent)
      {
        this.m_owner.CreateMapBackground(battlefieldInfo, parent);
      }

      public void ClearMapBackground()
      {
        this.m_owner.ClearMapBackground();
      }

      public void CreateMapTerrainFx(GameObject parent)
      {
        this.m_owner.CreateMapTerrainFx(parent);
      }

      public void ClearMapTerrainFx()
      {
        this.m_owner.ClearMapTerrainFx();
      }

      public void AddMapTerrainFx(
        GridPosition p,
        ConfigDataTerrainInfo terrainInfo,
        GameObject parent)
      {
        this.m_owner.AddMapTerrainFx(p, terrainInfo, parent);
      }

      public int _ComputeCombatArmyRelationValue(BattleActor a, BattleActor b, bool isMagic)
      {
        return this.m_owner._ComputeCombatArmyRelationValue(a, b, isMagic);
      }

      public static void _ComputeArmyRelationValue(ArmyRelationData r, bool isMagic, ref int value)
      {
        ClientBattle._ComputeArmyRelationValue(r, isMagic, ref value);
      }

      public void AppendActToActor()
      {
        this.m_owner.AppendActToActor();
      }

      public void AppendActsToActor(int step, System.Type type)
      {
        this.m_owner.AppendActsToActor(step, type);
      }

      public void AppendActsToActor(int step, System.Type type, ClientBattleActor ca)
      {
        this.m_owner.AppendActsToActor(step, type, ca);
      }

      public void SdkLogBattle(string eventId, object logData)
      {
        this.m_owner.SdkLogBattle(eventId, logData);
      }
    }
  }
}
