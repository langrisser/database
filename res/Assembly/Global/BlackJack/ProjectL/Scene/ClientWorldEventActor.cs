﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldEventActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.UI;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldEventActor : Entity
  {
    private ClientWorld m_clientWorld;
    private ConfigDataEventInfo m_eventInfo;
    private RandomEvent m_randomEvent;
    private ConfigDataWaypointInfo m_locateWaypointInfo;
    private GenericGraphic m_graphic;
    private Vector2 m_position;
    private int m_direction;
    private bool m_isVisible;
    private WorldEventActorUIController m_uiController;
    private Vector3 m_uiInitScale;
    private float m_graphicInitScale;
    private bool m_isPointerDown;
    private float m_scale;
    private float m_appearCountdown;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldEventActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(
      ClientWorld world,
      ConfigDataEventInfo eventInfo,
      RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void Tick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void Draw()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(Vector2 p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(ConfigDataWaypointInfo waypointInfo, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanClick(bool canClick)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAppearFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFx(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFx(string name, Vector3 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicEffect(GraphicEffect e, float param1 = 0.0f, float param2 = 0.0f)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGraphicEffect(GraphicEffect e)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsVisible()
    {
      return this.m_isVisible;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUISiblingIndex(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnUIPointerDown()
    {
      this.m_isPointerDown = true;
    }

    private void OnUIPointerUp()
    {
      this.m_isPointerDown = false;
    }

    private void OnUIClick()
    {
      this.m_clientWorld.OnEventActorClick(this);
    }

    public Vector2 Position
    {
      get
      {
        return this.m_position;
      }
    }

    public int Direction
    {
      get
      {
        return this.m_direction;
      }
    }

    public ConfigDataWaypointInfo LocateWaypointInfo
    {
      get
      {
        return this.m_locateWaypointInfo;
      }
    }

    public ConfigDataEventInfo EventInfo
    {
      get
      {
        return this.m_eventInfo;
      }
    }

    public RandomEvent RandomEvent
    {
      get
      {
        return this.m_randomEvent;
      }
    }
  }
}
