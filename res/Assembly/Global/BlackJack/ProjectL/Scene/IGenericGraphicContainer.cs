﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.IGenericGraphicContainer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using FixMath.NET;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public interface IGenericGraphicContainer
  {
    Vector3 GetCameraPosition();

    Vector3 CombatPositionToWorldPosition(Vector2i p, Fix64 z, bool computeZOffset);

    bool IsCulled(Vector2 bmin, Vector2 bmax, bool isCombat);

    bool IsCombatGraphicMirrorX();
  }
}
