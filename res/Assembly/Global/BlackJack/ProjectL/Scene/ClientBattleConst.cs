﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientBattleConst
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientBattleConst
  {
    public const int TickRate = 30;
    public const float TickTime = 0.03333334f;
    public const float GridWidth = 2f;
    public const float GridHeight = 2f;
    public const float FootHeightScale = 0.25f;
    public const float SkillFadeZOffset = 30f;
    public const float GuardZOffset = -1f;
    public const float ActorFxZOffset = -20f;
    public const float TreasureIdleZOffset = 1f;
    public const float TreasureOpenZOffset = -1f;
    public const int HpBarBuffCountMax = 3;
    public const int BattleResultHeroCountMax = 5;
  }
}
