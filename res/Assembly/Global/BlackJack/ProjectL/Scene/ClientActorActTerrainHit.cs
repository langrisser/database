﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientActorActTerrainHit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using SLua;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientActorActTerrainHit : ClientActorAct
  {
    public ConfigDataTerrainInfo m_terrainInfo;
    public int m_heroHp;
    public int m_soldierHp;
    public int m_heroHpModify;
    public int m_soldierHpModify;
    public DamageNumberType m_damageNumberType;
  }
}
