﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorld
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Art;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.UI;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [HotFix]
  public class ClientWorld : IFxEventListener
  {
    private ClientWorldState m_state;
    private int m_entityIdCounter;
    private int m_frameCount;
    private float m_worldTickTime;
    private float m_timeScale;
    private float m_globalTimeScale;
    private bool m_isPaused;
    private bool m_enableDebugDraw;
    private RandomNumber m_randomNumber;
    private ProjectLPlayerContext m_playerContext;
    private List<ClientWorldPlayerActor> m_playerActors;
    private List<ClientWorldEventActor> m_eventActors;
    private List<ClientWorldWaypoint> m_waypoints;
    private List<ClientWorldRegion> m_regions;
    private IConfigDataLoader m_configDataLoader;
    private IClientWorldListener m_clientWorldListener;
    private WorldCamera m_worldCamera;
    private WorldPathfinder m_pathfinder;
    private GameObject m_worldRoot;
    private GameObject m_graphicRoot;
    private GameObject m_mapRoot;
    private GameObject m_background;
    private GameObject m_backgroundWaypointsRoot;
    private GameObject m_backgroundRegionsRoot;
    private GameObject m_worldUIRoot;
    private GameObject m_waypointUIRoot;
    private GameObject m_eventUIRoot;
    private GameObject m_playerUIRoot;
    private GameObject m_waypointUIPrefab;
    private GameObject m_waypoint2UIPrefab;
    private GameObject m_eventUIPrefab;
    private GameObject m_playerUIPrefab;
    private WorldScenarioUIController m_worldScenarioUIController;
    private GraphicPool m_graphicPool;
    private GraphicPool m_fxPool;
    private FxPlayer m_fxPlayer;
    [DoNotToLua]
    private ClientWorld.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Dispose_hotfix;
    private LuaFunction m_InitializeIClientWorldListenerGameObject_hotfix;
    private LuaFunction m_OnWaypointUpdate_hotfix;
    private LuaFunction m_OnPlayerIconUpdate_hotfix;
    private LuaFunction m_TickSingle_hotfix;
    private LuaFunction m_TickClientWorld_hotfix;
    private LuaFunction m_SetTimeScaleSingle_hotfix;
    private LuaFunction m_SetGlobalTimeScaleSingle_hotfix;
    private LuaFunction m_UpdateFinalTimeScale_hotfix;
    private LuaFunction m_TickGraphicSingle_hotfix;
    private LuaFunction m_Draw_hotfix;
    private LuaFunction m_Clear_hotfix;
    private LuaFunction m_StartConfigDataWorldMapInfo_hotfix;
    private LuaFunction m_Stop_hotfix;
    private LuaFunction m_PauseBoolean_hotfix;
    private LuaFunction m_ShowWorldBoolean_hotfix;
    private LuaFunction m_CreateRegionConfigDataRegionInfo_hotfix;
    private LuaFunction m_CreateWaypointConfigDataWaypointInfo_hotfix;
    private LuaFunction m_CreatePlayerActorConfigDataWaypointInfoInt32_hotfix;
    private LuaFunction m_CreateEventActorConfigDataEventInfoConfigDataWaypointInfoInt32RandomEvent_hotfix;
    private LuaFunction m_UpdateRegionState_hotfix;
    private LuaFunction m_UpdateWaypointState_hotfix;
    private LuaFunction m_UpdatePlayerLocation_hotfix;
    private LuaFunction m_UpdateEventActorState_hotfix;
    private LuaFunction m_UpdateActiveScenario_hotfix;
    private LuaFunction m_GetRegionInt32_hotfix;
    private LuaFunction m_GetWaypointInt32_hotfix;
    private LuaFunction m_GetEventActorAtInt32_hotfix;
    private LuaFunction m_GetEventActoryByEventIdInt32_hotfix;
    private LuaFunction m_GetEventActors_hotfix;
    private LuaFunction m_GetPlayerActor_hotfix;
    private LuaFunction m_OnWaypointClickClientWorldWaypoint_hotfix;
    private LuaFunction m_OnEventActorClickClientWorldEventActor_hotfix;
    private LuaFunction m_CreateGraphicStringSingle_hotfix;
    private LuaFunction m_DestroyGraphicGenericGraphic_hotfix;
    private LuaFunction m_GetCamera_hotfix;
    private LuaFunction m_GetCameraPosition_hotfix;
    private LuaFunction m_IsCulledVector2Boolean_hotfix;
    private LuaFunction m_IsCulledVector2Vector2Boolean_hotfix;
    private LuaFunction m_GetNextEntityId_hotfix;
    private LuaFunction m_PlayScreenEffectString_hotfix;
    private LuaFunction m_CreateBackgroundConfigDataWorldMapInfoGameObject_hotfix;
    private LuaFunction m_ClearBackground_hotfix;
    private LuaFunction m_ShowBackgroundChildStringBoolean_hotfix;
    private LuaFunction m_PlaySoundString_hotfix;
    private LuaFunction m_PlaySoundSoundTableId_hotfix;
    private LuaFunction m_FindPathInt32Int32List`1Boolean_hotfix;
    private LuaFunction m_get_WorldCamera_hotfix;
    private LuaFunction m_get_State_hotfix;
    private LuaFunction m_set_EnableDebugDrawBoolean_hotfix;
    private LuaFunction m_get_EnableDebugDraw_hotfix;
    private LuaFunction m_get_GraphicRoot_hotfix;
    private LuaFunction m_get_WorldUIRoot_hotfix;
    private LuaFunction m_get_WaypointUIRoot_hotfix;
    private LuaFunction m_get_EventUIRoot_hotfix;
    private LuaFunction m_get_PlayerUIRoot_hotfix;
    private LuaFunction m_get_WaypointUIPrefab_hotfix;
    private LuaFunction m_get_Waypoint2UIPrefab_hotfix;
    private LuaFunction m_get_EventUIPrefab_hotfix;
    private LuaFunction m_get_PlayerUIPrefab_hotfix;
    private LuaFunction m_get_FxPlayer_hotfix;
    private LuaFunction m_get_ConfigDataLoader_hotfix;
    private LuaFunction m_get_ClientWorldListener_hotfix;
    private LuaFunction m_OnAudioFxEventAudioClip_hotfix;
    private LuaFunction m_OnSoundFxEventString_hotfix;
    private LuaFunction m_OnCameraEffectFxEventString_hotfix;
    private LuaFunction m_OnScreenEffectFxEventString_hotfix;
    private LuaFunction m_OnGeneralFxEventString_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(IClientWorldListener clientWorldListener, GameObject worldRoot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaypointUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlayerIconUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTimeScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGlobalTimeScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFinalTimeScale()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(ConfigDataWorldMapInfo mapInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Stop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWorld(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientWorldRegion CreateRegion(ConfigDataRegionInfo regionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientWorldWaypoint CreateWaypoint(
      ConfigDataWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientWorldPlayerActor CreatePlayerActor(
      ConfigDataWaypointInfo waypointInfo,
      int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientWorldEventActor CreateEventActor(
      ConfigDataEventInfo eventInfo,
      ConfigDataWaypointInfo waypointInfo,
      int dir,
      RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRegionState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateWaypointState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePlayerLocation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateEventActorState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareEventActorPositionY(
      ClientWorldEventActor a0,
      ClientWorldEventActor a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareWaypointPositionY(ClientWorldWaypoint a0, ClientWorldWaypoint a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateActiveScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldRegion GetRegion(int regionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldWaypoint GetWaypoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldEventActor GetEventActorAt(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldEventActor GetEventActoryByEventId(int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ClientWorldEventActor> GetEventActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldPlayerActor GetPlayerActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaypointClick(ClientWorldWaypoint wp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEventActorClick(ClientWorldEventActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic CreateGraphic(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyGraphic(GenericGraphic graphic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CameraBase GetCamera()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetCameraPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector3 ComputeActorPosition(Vector2 pos, float zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCulled(Vector2 p, bool isCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCulled(Vector2 bmin, Vector2 bmax, bool isCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextEntityId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayScreenEffect(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateBackground(ConfigDataWorldMapInfo worldMapInfo, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearBackground()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBackgroundChild(string childName, bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(int start, int goal, List<int> path, bool checkWaypointStatus)
    {
      // ISSUE: unable to decompile the method.
    }

    public static int FrameToMillisecond(int frame)
    {
      return frame * 1000 / 30;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame1(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    public WorldCamera WorldCamera
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ClientWorldState State
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool EnableDebugDraw
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject GraphicRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject WorldUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject WaypointUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject EventUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject PlayerUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject WaypointUIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject Waypoint2UIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject EventUIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject PlayerUIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public FxPlayer FxPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IClientWorldListener ClientWorldListener
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnAudio(FxEvent e, AudioClip a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSound(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCameraEffect(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScreenEffect(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnGeneral(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public ClientWorld.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ClientWorld m_owner;

      public LuaExportHelper(ClientWorld owner)
      {
        this.m_owner = owner;
      }

      public ClientWorldState m_state
      {
        get
        {
          return this.m_owner.m_state;
        }
        set
        {
          this.m_owner.m_state = value;
        }
      }

      public int m_entityIdCounter
      {
        get
        {
          return this.m_owner.m_entityIdCounter;
        }
        set
        {
          this.m_owner.m_entityIdCounter = value;
        }
      }

      public int m_frameCount
      {
        get
        {
          return this.m_owner.m_frameCount;
        }
        set
        {
          this.m_owner.m_frameCount = value;
        }
      }

      public float m_worldTickTime
      {
        get
        {
          return this.m_owner.m_worldTickTime;
        }
        set
        {
          this.m_owner.m_worldTickTime = value;
        }
      }

      public float m_timeScale
      {
        get
        {
          return this.m_owner.m_timeScale;
        }
        set
        {
          this.m_owner.m_timeScale = value;
        }
      }

      public float m_globalTimeScale
      {
        get
        {
          return this.m_owner.m_globalTimeScale;
        }
        set
        {
          this.m_owner.m_globalTimeScale = value;
        }
      }

      public bool m_isPaused
      {
        get
        {
          return this.m_owner.m_isPaused;
        }
        set
        {
          this.m_owner.m_isPaused = value;
        }
      }

      public bool m_enableDebugDraw
      {
        get
        {
          return this.m_owner.m_enableDebugDraw;
        }
        set
        {
          this.m_owner.m_enableDebugDraw = value;
        }
      }

      public RandomNumber m_randomNumber
      {
        get
        {
          return this.m_owner.m_randomNumber;
        }
        set
        {
          this.m_owner.m_randomNumber = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public List<ClientWorldPlayerActor> m_playerActors
      {
        get
        {
          return this.m_owner.m_playerActors;
        }
        set
        {
          this.m_owner.m_playerActors = value;
        }
      }

      public List<ClientWorldEventActor> m_eventActors
      {
        get
        {
          return this.m_owner.m_eventActors;
        }
        set
        {
          this.m_owner.m_eventActors = value;
        }
      }

      public List<ClientWorldWaypoint> m_waypoints
      {
        get
        {
          return this.m_owner.m_waypoints;
        }
        set
        {
          this.m_owner.m_waypoints = value;
        }
      }

      public List<ClientWorldRegion> m_regions
      {
        get
        {
          return this.m_owner.m_regions;
        }
        set
        {
          this.m_owner.m_regions = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public IClientWorldListener m_clientWorldListener
      {
        get
        {
          return this.m_owner.m_clientWorldListener;
        }
        set
        {
          this.m_owner.m_clientWorldListener = value;
        }
      }

      public WorldCamera m_worldCamera
      {
        get
        {
          return this.m_owner.m_worldCamera;
        }
        set
        {
          this.m_owner.m_worldCamera = value;
        }
      }

      public WorldPathfinder m_pathfinder
      {
        get
        {
          return this.m_owner.m_pathfinder;
        }
        set
        {
          this.m_owner.m_pathfinder = value;
        }
      }

      public GameObject m_worldRoot
      {
        get
        {
          return this.m_owner.m_worldRoot;
        }
        set
        {
          this.m_owner.m_worldRoot = value;
        }
      }

      public GameObject m_graphicRoot
      {
        get
        {
          return this.m_owner.m_graphicRoot;
        }
        set
        {
          this.m_owner.m_graphicRoot = value;
        }
      }

      public GameObject m_mapRoot
      {
        get
        {
          return this.m_owner.m_mapRoot;
        }
        set
        {
          this.m_owner.m_mapRoot = value;
        }
      }

      public GameObject m_background
      {
        get
        {
          return this.m_owner.m_background;
        }
        set
        {
          this.m_owner.m_background = value;
        }
      }

      public GameObject m_backgroundWaypointsRoot
      {
        get
        {
          return this.m_owner.m_backgroundWaypointsRoot;
        }
        set
        {
          this.m_owner.m_backgroundWaypointsRoot = value;
        }
      }

      public GameObject m_backgroundRegionsRoot
      {
        get
        {
          return this.m_owner.m_backgroundRegionsRoot;
        }
        set
        {
          this.m_owner.m_backgroundRegionsRoot = value;
        }
      }

      public GameObject m_worldUIRoot
      {
        get
        {
          return this.m_owner.m_worldUIRoot;
        }
        set
        {
          this.m_owner.m_worldUIRoot = value;
        }
      }

      public GameObject m_waypointUIRoot
      {
        get
        {
          return this.m_owner.m_waypointUIRoot;
        }
        set
        {
          this.m_owner.m_waypointUIRoot = value;
        }
      }

      public GameObject m_eventUIRoot
      {
        get
        {
          return this.m_owner.m_eventUIRoot;
        }
        set
        {
          this.m_owner.m_eventUIRoot = value;
        }
      }

      public GameObject m_playerUIRoot
      {
        get
        {
          return this.m_owner.m_playerUIRoot;
        }
        set
        {
          this.m_owner.m_playerUIRoot = value;
        }
      }

      public GameObject m_waypointUIPrefab
      {
        get
        {
          return this.m_owner.m_waypointUIPrefab;
        }
        set
        {
          this.m_owner.m_waypointUIPrefab = value;
        }
      }

      public GameObject m_waypoint2UIPrefab
      {
        get
        {
          return this.m_owner.m_waypoint2UIPrefab;
        }
        set
        {
          this.m_owner.m_waypoint2UIPrefab = value;
        }
      }

      public GameObject m_eventUIPrefab
      {
        get
        {
          return this.m_owner.m_eventUIPrefab;
        }
        set
        {
          this.m_owner.m_eventUIPrefab = value;
        }
      }

      public GameObject m_playerUIPrefab
      {
        get
        {
          return this.m_owner.m_playerUIPrefab;
        }
        set
        {
          this.m_owner.m_playerUIPrefab = value;
        }
      }

      public WorldScenarioUIController m_worldScenarioUIController
      {
        get
        {
          return this.m_owner.m_worldScenarioUIController;
        }
        set
        {
          this.m_owner.m_worldScenarioUIController = value;
        }
      }

      public GraphicPool m_graphicPool
      {
        get
        {
          return this.m_owner.m_graphicPool;
        }
        set
        {
          this.m_owner.m_graphicPool = value;
        }
      }

      public GraphicPool m_fxPool
      {
        get
        {
          return this.m_owner.m_fxPool;
        }
        set
        {
          this.m_owner.m_fxPool = value;
        }
      }

      public FxPlayer m_fxPlayer
      {
        get
        {
          return this.m_owner.m_fxPlayer;
        }
        set
        {
          this.m_owner.m_fxPlayer = value;
        }
      }

      public void TickClientWorld()
      {
        this.m_owner.TickClientWorld();
      }

      public void SetTimeScale(float scale)
      {
        this.m_owner.SetTimeScale(scale);
      }

      public void UpdateFinalTimeScale()
      {
        this.m_owner.UpdateFinalTimeScale();
      }

      public void TickGraphic(float dt)
      {
        this.m_owner.TickGraphic(dt);
      }

      public void Draw()
      {
        this.m_owner.Draw();
      }

      public void Clear()
      {
        this.m_owner.Clear();
      }

      public ClientWorldRegion CreateRegion(ConfigDataRegionInfo regionInfo)
      {
        return this.m_owner.CreateRegion(regionInfo);
      }

      public ClientWorldWaypoint CreateWaypoint(
        ConfigDataWaypointInfo waypointInfo)
      {
        return this.m_owner.CreateWaypoint(waypointInfo);
      }

      public ClientWorldPlayerActor CreatePlayerActor(
        ConfigDataWaypointInfo waypointInfo,
        int dir)
      {
        return this.m_owner.CreatePlayerActor(waypointInfo, dir);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public ClientWorldEventActor CreateEventActor(
        ConfigDataEventInfo eventInfo,
        ConfigDataWaypointInfo waypointInfo,
        int dir,
        RandomEvent randomEvent)
      {
        // ISSUE: unable to decompile the method.
      }

      public void UpdateRegionState()
      {
        this.m_owner.UpdateRegionState();
      }

      public void UpdateWaypointState()
      {
        this.m_owner.UpdateWaypointState();
      }

      public void UpdatePlayerLocation()
      {
        this.m_owner.UpdatePlayerLocation();
      }

      public static int CompareEventActorPositionY(
        ClientWorldEventActor a0,
        ClientWorldEventActor a1)
      {
        return ClientWorld.CompareEventActorPositionY(a0, a1);
      }

      public static int CompareWaypointPositionY(ClientWorldWaypoint a0, ClientWorldWaypoint a1)
      {
        return ClientWorld.CompareWaypointPositionY(a0, a1);
      }

      public CameraBase GetCamera()
      {
        return this.m_owner.GetCamera();
      }

      public void CreateBackground(ConfigDataWorldMapInfo worldMapInfo, GameObject parent)
      {
        this.m_owner.CreateBackground(worldMapInfo, parent);
      }

      public void ClearBackground()
      {
        this.m_owner.ClearBackground();
      }
    }
  }
}
