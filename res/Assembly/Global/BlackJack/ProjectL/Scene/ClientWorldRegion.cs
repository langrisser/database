﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldRegion
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldRegion : Entity
  {
    private ConfigDataRegionInfo m_regionInfo;
    private GameObject m_gameObject;
    private Animator m_animator;
    private bool m_isOpened;
    private bool m_isVisible;

    public ClientWorldRegion()
    {
      this.m_isVisible = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientWorld world, ConfigDataRegionInfo regionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGameObject(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void Dispose()
    {
      base.Dispose();
    }

    public override void Tick()
    {
    }

    public override void TickGraphic(float dt)
    {
    }

    public override void Draw()
    {
    }

    public void SetOpen(bool open)
    {
      this.m_isOpened = open;
    }

    public bool IsOpened()
    {
      return this.m_isOpened;
    }

    public override void DoPause(bool pause)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsVisible()
    {
      return this.m_isVisible;
    }

    public Vector2 Position
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataRegionInfo RegionInfo
    {
      get
      {
        return this.m_regionInfo;
      }
    }
  }
}
