﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldPlayerActorList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldPlayerActorList
  {
    public static void RemoveAll(List<ClientWorldPlayerActor> list)
    {
      EntityList.RemoveAll<ClientWorldPlayerActor>(list);
    }

    public static void RemoveDeleted(List<ClientWorldPlayerActor> list)
    {
      EntityList.RemoveDeleted<ClientWorldPlayerActor>(list);
    }

    public static void Tick(List<ClientWorldPlayerActor> list)
    {
      EntityList.Tick<ClientWorldPlayerActor>(list);
    }

    public static void TickGraphic(List<ClientWorldPlayerActor> list, float dt)
    {
      EntityList.TickGraphic<ClientWorldPlayerActor>(list, dt);
    }

    public static void Draw(List<ClientWorldPlayerActor> list)
    {
      EntityList.Draw<ClientWorldPlayerActor>(list);
    }

    public static void Pause(List<ClientWorldPlayerActor> list, bool pause)
    {
      EntityList.Pause<ClientWorldPlayerActor>(list, pause);
    }
  }
}
