﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldConst
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldConst
  {
    public const int TickRate = 30;
    public const float TickTime = 0.03333334f;
    public const float CameraAngle = 20f;
    public static Quaternion FaceCameraRotation;
    public const int WorldActorSortingOrder = 2;
    public const int WorldScenarioSortingOrder = 4;
    public const float WaypointZOffset = 0.0f;
    public const float EventActorZOffset = -0.04f;
    public const float PlayerActorZOffset = -0.08f;
    public const float WorldActorUIZOffset = -0.02f;

    [MethodImpl((MethodImplOptions) 32768)]
    static ClientWorldConst()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
