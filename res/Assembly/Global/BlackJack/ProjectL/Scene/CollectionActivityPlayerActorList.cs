﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.CollectionActivityPlayerActorList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class CollectionActivityPlayerActorList
  {
    public static void RemoveAll(List<CollectionActivityPlayerActor> list)
    {
      EntityList.RemoveAll<CollectionActivityPlayerActor>(list);
    }

    public static void RemoveDeleted(List<CollectionActivityPlayerActor> list)
    {
      EntityList.RemoveDeleted<CollectionActivityPlayerActor>(list);
    }

    public static void Tick(List<CollectionActivityPlayerActor> list)
    {
      EntityList.Tick<CollectionActivityPlayerActor>(list);
    }

    public static void TickGraphic(List<CollectionActivityPlayerActor> list, float dt)
    {
      EntityList.TickGraphic<CollectionActivityPlayerActor>(list, dt);
    }

    public static void Draw(List<CollectionActivityPlayerActor> list)
    {
      EntityList.Draw<CollectionActivityPlayerActor>(list);
    }

    public static void Pause(List<CollectionActivityPlayerActor> list, bool pause)
    {
      EntityList.Pause<CollectionActivityPlayerActor>(list, pause);
    }
  }
}
