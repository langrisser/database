﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.CollectionActivityPlayerActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.UI;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [HotFix]
  public class CollectionActivityPlayerActor : Entity
  {
    private CollectionActivityWorld m_collectionActivityWorld;
    private ConfigDataCollectionActivityWaypointInfo m_locateWaypointInfo;
    private List<CollectionActivityWaypoint> m_movePath;
    private Action m_onMoveEnd;
    private float m_teleportAppearCountdown;
    private float m_teleportEndActionCountdown;
    private CollectionActivityWaypoint m_teleportWaypoint;
    private Action m_onTeleportEnd;
    private GenericGraphic m_graphic;
    private Vector2 m_position;
    private int m_direction;
    private bool m_isVisible;
    private WorldPlayerActorUIController m_uiController;
    [DoNotToLua]
    private CollectionActivityPlayerActor.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitializeCollectionActivityWorld_hotfix;
    private LuaFunction m_CreateGraphicConfigDataJobConnectionInfoConfigDataModelSkinResourceInfo_hotfix;
    private LuaFunction m_CreateGraphicStringList`1_hotfix;
    private LuaFunction m_DestroyGraphic_hotfix;
    private LuaFunction m_Dispose_hotfix;
    private LuaFunction m_Tick_hotfix;
    private LuaFunction m_TickGraphicSingle_hotfix;
    private LuaFunction m_Draw_hotfix;
    private LuaFunction m_DoPauseBoolean_hotfix;
    private LuaFunction m_LocateVector2Int32_hotfix;
    private LuaFunction m_LocateConfigDataCollectionActivityWaypointInfoInt32_hotfix;
    private LuaFunction m_MovePathList`1Action_hotfix;
    private LuaFunction m_TeleportInt32Action_hotfix;
    private LuaFunction m_IsMoving_hotfix;
    private LuaFunction m_PlayAnimationStringBoolean_hotfix;
    private LuaFunction m_PlayFxString_hotfix;
    private LuaFunction m_PlayFxStringVector3_hotfix;
    private LuaFunction m_SetGraphicEffectGraphicEffectSingleSingle_hotfix;
    private LuaFunction m_ClearGraphicEffectGraphicEffect_hotfix;
    private LuaFunction m_PlaySoundString_hotfix;
    private LuaFunction m_PlaySoundSoundTableId_hotfix;
    private LuaFunction m_SetVisibleBoolean_hotfix;
    private LuaFunction m_IsVisible_hotfix;
    private LuaFunction m_get_Position_hotfix;
    private LuaFunction m_get_Direction_hotfix;
    private LuaFunction m_get_LocateWaypointInfo_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityPlayerActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(CollectionActivityWorld world)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGraphic(
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ConfigDataModelSkinResourceInfo heroSkinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGraphic(string assetName, List<ReplaceAnim> replaceAnims = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(Vector2 p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(
      ConfigDataCollectionActivityWaypointInfo waypointInfo,
      int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void MovePath(List<int> path, Action onMoveEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Teleport(int waypointId, Action onTeleportEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMoving()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFx(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFx(string name, Vector3 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicEffect(GraphicEffect e, float param1 = 0.0f, float param2 = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGraphicEffect(GraphicEffect e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    public Vector2 Position
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Direction
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataCollectionActivityWaypointInfo LocateWaypointInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CollectionActivityPlayerActor.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Dispose()
    {
      base.Dispose();
    }

    private void __callBase_Tick()
    {
      base.Tick();
    }

    private void __callBase_TickGraphic(float dt)
    {
      base.TickGraphic(dt);
    }

    private void __callBase_Draw()
    {
      base.Draw();
    }

    private void __callBase_Pause(bool pause)
    {
      this.Pause(pause);
    }

    private void __callBase_DoPause(bool pause)
    {
      base.DoPause(pause);
    }

    private void __callBase_DeleteMe()
    {
      this.DeleteMe();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_m_onMoveEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_m_onMoveEnd()
    {
      this.m_onMoveEnd = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_m_onTeleportEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_m_onTeleportEnd()
    {
      this.m_onTeleportEnd = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityPlayerActor m_owner;

      public LuaExportHelper(CollectionActivityPlayerActor owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Dispose()
      {
        this.m_owner.__callBase_Dispose();
      }

      public void __callBase_Tick()
      {
        this.m_owner.__callBase_Tick();
      }

      public void __callBase_TickGraphic(float dt)
      {
        this.m_owner.__callBase_TickGraphic(dt);
      }

      public void __callBase_Draw()
      {
        this.m_owner.__callBase_Draw();
      }

      public void __callBase_Pause(bool pause)
      {
        this.m_owner.__callBase_Pause(pause);
      }

      public void __callBase_DoPause(bool pause)
      {
        this.m_owner.__callBase_DoPause(pause);
      }

      public void __callBase_DeleteMe()
      {
        this.m_owner.__callBase_DeleteMe();
      }

      public void __callDele_m_onMoveEnd()
      {
        this.m_owner.__callDele_m_onMoveEnd();
      }

      public void __clearDele_m_onMoveEnd()
      {
        this.m_owner.__clearDele_m_onMoveEnd();
      }

      public void __callDele_m_onTeleportEnd()
      {
        this.m_owner.__callDele_m_onTeleportEnd();
      }

      public void __clearDele_m_onTeleportEnd()
      {
        this.m_owner.__clearDele_m_onTeleportEnd();
      }

      public CollectionActivityWorld m_collectionActivityWorld
      {
        get
        {
          return this.m_owner.m_collectionActivityWorld;
        }
        set
        {
          this.m_owner.m_collectionActivityWorld = value;
        }
      }

      public ConfigDataCollectionActivityWaypointInfo m_locateWaypointInfo
      {
        get
        {
          return this.m_owner.m_locateWaypointInfo;
        }
        set
        {
          this.m_owner.m_locateWaypointInfo = value;
        }
      }

      public List<CollectionActivityWaypoint> m_movePath
      {
        get
        {
          return this.m_owner.m_movePath;
        }
        set
        {
          this.m_owner.m_movePath = value;
        }
      }

      public Action m_onMoveEnd
      {
        get
        {
          return this.m_owner.m_onMoveEnd;
        }
        set
        {
          this.m_owner.m_onMoveEnd = value;
        }
      }

      public float m_teleportAppearCountdown
      {
        get
        {
          return this.m_owner.m_teleportAppearCountdown;
        }
        set
        {
          this.m_owner.m_teleportAppearCountdown = value;
        }
      }

      public float m_teleportEndActionCountdown
      {
        get
        {
          return this.m_owner.m_teleportEndActionCountdown;
        }
        set
        {
          this.m_owner.m_teleportEndActionCountdown = value;
        }
      }

      public CollectionActivityWaypoint m_teleportWaypoint
      {
        get
        {
          return this.m_owner.m_teleportWaypoint;
        }
        set
        {
          this.m_owner.m_teleportWaypoint = value;
        }
      }

      public Action m_onTeleportEnd
      {
        get
        {
          return this.m_owner.m_onTeleportEnd;
        }
        set
        {
          this.m_owner.m_onTeleportEnd = value;
        }
      }

      public GenericGraphic m_graphic
      {
        get
        {
          return this.m_owner.m_graphic;
        }
        set
        {
          this.m_owner.m_graphic = value;
        }
      }

      public Vector2 m_position
      {
        get
        {
          return this.m_owner.m_position;
        }
        set
        {
          this.m_owner.m_position = value;
        }
      }

      public int m_direction
      {
        get
        {
          return this.m_owner.m_direction;
        }
        set
        {
          this.m_owner.m_direction = value;
        }
      }

      public bool m_isVisible
      {
        get
        {
          return this.m_owner.m_isVisible;
        }
        set
        {
          this.m_owner.m_isVisible = value;
        }
      }

      public WorldPlayerActorUIController m_uiController
      {
        get
        {
          return this.m_owner.m_uiController;
        }
        set
        {
          this.m_owner.m_uiController = value;
        }
      }

      public void DestroyGraphic()
      {
        this.m_owner.DestroyGraphic();
      }

      public void PlayAnimation(string name, bool loop)
      {
        this.m_owner.PlayAnimation(name, loop);
      }

      public void PlayFx(string name)
      {
        this.m_owner.PlayFx(name);
      }

      public void PlayFx(string name, Vector3 p)
      {
        this.m_owner.PlayFx(name, p);
      }

      public void PlaySound(string name)
      {
        this.m_owner.PlaySound(name);
      }

      public void PlaySound(SoundTableId id)
      {
        this.m_owner.PlaySound(id);
      }
    }
  }
}
