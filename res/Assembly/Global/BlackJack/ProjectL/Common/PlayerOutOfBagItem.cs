﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PlayerOutOfBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class PlayerOutOfBagItem
  {
    public List<int> HeadFrames;
    public List<int> HeroSkinIds;
    public List<int> SoldierSkinIds;
    public List<int> EquipmentIds;
    public List<MonthCard> MonthCards;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerOutOfBagItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProResource ToPB(PlayerOutOfBagItem resource)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PlayerOutOfBagItem FromPB(ProResource pbResource)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
