﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RoleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RoleInfo
  {
    public string UserId { get; set; }

    public int ServerId { get; set; }

    public int PlayerLevel { get; set; }

    public int HeadIcon { get; set; }

    public string Name { get; set; }

    public int LastLoginHours { get; set; }
  }
}
