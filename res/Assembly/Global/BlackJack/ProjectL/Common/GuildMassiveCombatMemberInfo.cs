﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildMassiveCombatMemberInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  [HotFix]
  public class GuildMassiveCombatMemberInfo
  {
    public List<int> PointsRewardsClaimed;
    public UserSummary Summary;
    [DoNotToLua]
    private GuildMassiveCombatMemberInfo.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_get_UserId_hotfix;
    private LuaFunction m_set_UserIdString_hotfix;
    private LuaFunction m_get_Points_hotfix;
    private LuaFunction m_set_PointsInt32_hotfix;
    private LuaFunction m_get_PointsToday_hotfix;
    private LuaFunction m_set_PointsTodayInt32_hotfix;
    private LuaFunction m_get_Title_hotfix;
    private LuaFunction m_set_TitleGuildTitle_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatMemberInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public string UserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Points
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int PointsToday
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GuildTitle Title
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public GuildMassiveCombatMemberInfo.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private GuildMassiveCombatMemberInfo m_owner;

      public LuaExportHelper(GuildMassiveCombatMemberInfo owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
