﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class GuildMember
  {
    public string UserId { get; set; }

    public string Name { get; set; }

    public int Level { get; set; }

    public GuildTitle Title { get; set; }

    public int TotalActivities { get; set; }

    public int ThisWeekActivities { get; set; }

    public bool Online { get; set; }

    public DateTime LogoutTime { get; set; }

    public int TopHeroBattlePower { get; set; }

    public int HeadIcon { get; set; }

    public void WeeklyFlush()
    {
      this.ThisWeekActivities = 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAdmin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGuildMember ToPb(GuildMember m)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GuildMember FromPb(ProGuildMember pb)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
