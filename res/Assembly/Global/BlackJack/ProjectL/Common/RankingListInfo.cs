﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RankingListInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RankingListInfo
  {
    public RankingListType Type { get; set; }

    public int Score { get; set; }

    public int CurrRank { get; set; }

    public int LastRank { get; set; }

    public int ChampionHeroId { get; set; }

    public List<RankingTargetPlayerInfo> PlayerList { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRankingListInfo RankingListToPBRankingList(
      RankingListInfo rankingList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RankingListInfo PBRankingListToRankingList(
      ProRankingListInfo proRankingList)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
