﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaPlayerDefensiveTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ArenaPlayerDefensiveTeam
  {
    public List<ArenaPlayerDefensiveHero> Heroes;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerDefensiveTeam()
    {
    }

    public byte BattleId { get; set; }

    public byte ArenaDefenderRuleId { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaPlayerDefensiveTeam PBArenaDefensiveTeamToArenaDefensiveTeam(
      ProArenaPlayerDefensiveTeam pbDefensiveTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaPlayerDefensiveTeam ArenaDefensiveTeamToPBArenaDefensiveTeam(
      ArenaPlayerDefensiveTeam defensiveTeam)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
