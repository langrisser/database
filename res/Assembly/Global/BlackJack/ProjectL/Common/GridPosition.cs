﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GridPosition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public struct GridPosition
  {
    private static GridPosition _null = new GridPosition(-1, -1);
    public int x;
    public int y;

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    public GridPosition(int x, int y)
    {
      this.x = x;
      this.y = y;
    }

    public void Set(int x, int y)
    {
      this.x = x;
      this.y = y;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GridPosition Add(GridPosition a, GridPosition b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GridPosition Subtract(GridPosition a, GridPosition b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GridPosition Multiply(GridPosition a, int b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Distance(GridPosition a, GridPosition b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static float EuclideanDistance(GridPosition a, GridPosition b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator ==(GridPosition a, GridPosition b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator !=(GridPosition a, GridPosition b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GridPosition Round(float fx, float fy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GridPosition Lerp(GridPosition a, GridPosition b, float t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int GetHashCode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool Equals(object other)
    {
      // ISSUE: unable to decompile the method.
    }

    public static GridPosition Null
    {
      get
      {
        return GridPosition._null;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool TryParse(string str, out GridPosition ret)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
