﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RealTimePVPMatchStats
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RealTimePVPMatchStats
  {
    public int Matches;
    public int Wins;
    public int ConsecutiveWins;
    public int ConsecutiveLosses;

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddWins()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLosses()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPMatchStats DeepDuplicate()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
