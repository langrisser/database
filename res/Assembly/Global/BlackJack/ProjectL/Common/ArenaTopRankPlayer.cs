﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaTopRankPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ArenaTopRankPlayer
  {
    public string Name { get; set; }

    public ushort ArenaPoints { get; set; }

    public byte LevelId { get; set; }

    public int HeadIcon { get; set; }

    public int Level { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaTopRankPlayer ArenaTopRankPlayerToPBArenaTopRankPlayer(
      ArenaTopRankPlayer topRankPlayer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaTopRankPlayer PBArenaTopRankPlayerToArenaTopRankPlayer(
      ProArenaTopRankPlayer pbTopRankPlayer)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
