﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RechargeStoreComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class RechargeStoreComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionRechargeStore m_rechargeStoreDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    [DoNotToLua]
    private RechargeStoreComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_UpdateRechargeStoreStatusInt32Boolean_hotfix;
    private LuaFunction m_OnResetRechargeStoreStatusInt32_hotfix;
    private LuaFunction m_Sync2ClientUpdateRechargeStoreStatusInt32_hotfix;
    private LuaFunction m_IsGoodsBoughtInt32_hotfix;
    private LuaFunction m_CaculateGotCrystalNumsInt32_hotfix;
    private LuaFunction m_BuyGoodsInt32_hotfix;
    private LuaFunction m_add_BuyRechargeStoreGoodsEventAction`1_hotfix;
    private LuaFunction m_remove_BuyRechargeStoreGoodsEventAction`1_hotfix;
    private LuaFunction m_add_BuyStoreItemEventAction`2_hotfix;
    private LuaFunction m_remove_BuyStoreItemEventAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public RechargeStoreComponentCommon()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRechargeStoreStatus(int currentId, bool sync2Client = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnResetRechargeStoreStatus(int currentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void Sync2ClientUpdateRechargeStoreStatus(int currentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGoodsBought(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CaculateGotCrystalNums(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void BuyGoods(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> BuyRechargeStoreGoodsEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> BuyStoreItemEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public RechargeStoreComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_BuyRechargeStoreGoodsEvent(int obj)
    {
    }

    private void __clearDele_BuyRechargeStoreGoodsEvent(int obj)
    {
      this.BuyRechargeStoreGoodsEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_BuyStoreItemEvent(int arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_BuyStoreItemEvent(int arg1, int arg2)
    {
      this.BuyStoreItemEvent = (Action<int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private RechargeStoreComponentCommon m_owner;

      public LuaExportHelper(RechargeStoreComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_BuyRechargeStoreGoodsEvent(int obj)
      {
        this.m_owner.__callDele_BuyRechargeStoreGoodsEvent(obj);
      }

      public void __clearDele_BuyRechargeStoreGoodsEvent(int obj)
      {
        this.m_owner.__clearDele_BuyRechargeStoreGoodsEvent(obj);
      }

      public void __callDele_BuyStoreItemEvent(int arg1, int arg2)
      {
        this.m_owner.__callDele_BuyStoreItemEvent(arg1, arg2);
      }

      public void __clearDele_BuyStoreItemEvent(int arg1, int arg2)
      {
        this.m_owner.__clearDele_BuyStoreItemEvent(arg1, arg2);
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public DataSectionRechargeStore m_rechargeStoreDS
      {
        get
        {
          return this.m_owner.m_rechargeStoreDS;
        }
        set
        {
          this.m_owner.m_rechargeStoreDS = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public void OnResetRechargeStoreStatus(int currentId)
      {
        this.m_owner.OnResetRechargeStoreStatus(currentId);
      }

      public void Sync2ClientUpdateRechargeStoreStatus(int currentId)
      {
        this.m_owner.Sync2ClientUpdateRechargeStoreStatus(currentId);
      }

      public void BuyGoods(int goodsId)
      {
        this.m_owner.BuyGoods(goodsId);
      }
    }
  }
}
