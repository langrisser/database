﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ClimbTower
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ClimbTower
  {
    public int Floor { get; set; }

    public int HistoryFloorMax { get; set; }

    public DateTime NextFlushTime { get; set; }

    public DateTime TowerFloorUpdateTime { get; set; }

    public int LastTowerFloorRank { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProClimbTower ToPb(GlobalClimbTowerInfo globalClimbTowerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProClimbTowerFloor ToPb(
      GlobalClimbTowerFloor globalClimbTowerFloor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GlobalClimbTowerInfo FromPb(ProClimbTower pbClimbTower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GlobalClimbTowerFloor FromPb(
      ProClimbTowerFloor pbClimbTowerFloor)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
