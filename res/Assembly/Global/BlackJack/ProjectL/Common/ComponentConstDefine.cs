﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ComponentConstDefine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class ComponentConstDefine
  {
    public const string PlayerBasicInfoComponentName = "PlayerBasicInfo";
    public const string BagComponentName = "Bag";
    public const string HeroComponentName = "Hero";
    public const string BattleComponentName = "Battle";
    public const string LevelComponentName = "Level";
    public const string RiftComponentName = "Rift";
    public const string MailComponentName = "Mail";
    public const string RandomStoreComponentName = "RandomStore";
    public const string FixedStoreComponentName = "FixedStore";
    public const string ChatComponentName = "Chat";
    public const string ChatGroupComponentName = "ChatGroup";
    public const string SelectCardComponentName = "SelectCard";
    public const string RaffleComponentName = "Raffle";
    public const string SurveyComponentName = "Survey";
    public const string MissionComponentName = "Mission";
    public const string CommentComponentName = "Comment";
    public const string StatisticalDataComponentName = "StatisticalData";
    public const string ThearchyTrailComponentName = "ThearchyTrail";
    public const string EternalShrineComponentName = "EternalShrine";
    public const string AnikiGymComponentName = "AnikiGym";
    public const string HeroDungeonComponentName = "HeroDungeon";
    public const string HeroTrainningComponentName = "HeroTrainning";
    public const string TreasureMapComponentName = "TreasureMap";
    public const string TrainingGroundComponentName = "TrainingGround";
    public const string HeroAssistantsComponentName = "HeroAssistants";
    public const string MemoryCorridorComponentName = "MemoryCorridor";
    public const string ArenaComponentName = "Arena";
    public const string OperationalActivityComponentName = "OperationalActivity";
    public const string DanmakuComponentName = "Danmaku";
    public const string TeamComponentName = "Team";
    public const string GlobalRankingListComponentName = "GlobalRankingListComponent";
    public const string FriendComponentName = "Friend";
    public const string HeroPhantomComponentName = "HeroPhantom";
    public const string CooperateBattleComponentName = "CooperateBattle";
    public const string NoviceComponentName = "Novice";
    public const string RefluxComponentName = "Reflux";
    public const string RechargeStoreComponentName = "RechargeStore";
    public const string GiftStoreComponentName = "GiftStore";
    public const string ResourceComponentName = "Resource";
    public const string RealTimePVPComponentName = "RealTimePVP";
    public const string GuildComponentName = "Guild";
    public const string UnchartedScoreComponentName = "UnchartedScore";
    public const string ClimbTowerComponentName = "ClimbTower";
    public const string CollectionComponentName = "Collection";
    public const int PlayerStartLvl = 1;
    public const int CustomMailTemplateId = 0;
    public const int MailStatusUnread = 0;
    public const int MailStatusReaded = 1;
    public const int MailStatusGotAttachments = 2;
    public const int SelectOneTimes = 1;
    public const int SelectTenTimes = 10;
    public const int InsignificanceParam = 0;
    public const int AnyTypeParam = 0;
    public const int RiftLevelSimpleDifficulty = 1;
    public const int RiftLevelDiffcultDiffculty = 2;
    public const int ArenaOpponentNumsPerZone = 1;
    public const int ArenaOpponentNums = 3;
    public const int CoachStartLvl = 1;
    public const int InstrumentStartLvl = 1;
    public const int CoachFavorabilityStartLvl = 1;
    public const int HotCommentNumInAllComments = 3;
    public const int ArenaRevengeOpponentPointZoneId = 2;
    public const int ArenaThisWeekBattleIdNums = 3;
    public const int ArenaMinArenaPoints = 100;
    public const int MaxEnhanceMaterialNums = 5;
    public const uint ChatVoiceTimeLengthMax = 20;
    public const uint ChatVoiceTimeLengthMin = 1;
    public const int ChatRecordFrequency = 16000;
    public const int ChatEncodeAndDecodeSampleSize = 640;
    public const int ChatTextLengthLimit = 100;
    public const int ChatVoiceCacheCountMaxInWorldChannel = 10000;
    public const int ChatVoiceTimeoutInWorldChannel = 7200000;
    public const int ChatVoiceCacheCountMaxInGuildChannel = 10000;
    public const int ChatVoiceTimeoutInGuildChannel = 7200000;
    public const int ChatRoomCountMaxInWorldChannel = 999;
    public const int RankingListForTopHeroNum = 5;
    public const int RankingListQueryPeriod = 3000;
    public const int ZilongCheckAdvencePeriodSeconds = 0;
    public const int GuildSearchPeriod = 3000;
    [DoNotToLua]
    private ComponentConstDefine.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ComponentConstDefine()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public ComponentConstDefine.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ComponentConstDefine m_owner;

      public LuaExportHelper(ComponentConstDefine owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
