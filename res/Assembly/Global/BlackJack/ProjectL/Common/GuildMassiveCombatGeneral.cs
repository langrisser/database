﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildMassiveCombatGeneral
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class GuildMassiveCombatGeneral
  {
    public List<GuildMassiveCombatInfo> FinishedCombats;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatGeneral()
    {
    }

    public GuildMassiveCombatInfo RecentCombat { get; set; }

    public DateTime NextDailyRefreshTime { get; set; }
  }
}
