﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RealTimePVPBattleReportPlayerData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RealTimePVPBattleReportPlayerData
  {
    public List<BattleHero> Heroes;
    public List<TrainingTech> Techs;
    public int ScoreDiff;

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPBattleReportPlayerData()
    {
    }

    public string UserId { get; set; }

    public string Name { get; set; }

    public int HeadIcon { get; set; }

    public int Level { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetHeroes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPBattleReportPlayerData DeepCopy()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
