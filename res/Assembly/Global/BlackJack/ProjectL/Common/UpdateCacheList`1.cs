﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UpdateCacheList`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class UpdateCacheList<T> where T : class
  {
    private LinkedList<int> m_freeIndices;

    [MethodImpl((MethodImplOptions) 32768)]
    public UpdateCacheList()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Init(T data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Add(T data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Add(UpdateCache<T> cache, bool dirty = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Remove(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Set(int index, T data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Remove(UpdateCache<T> cache, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UpdateCache<T> Find(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<T> GetAllVaildDatas()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<T> IterateAllBagItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UpdateCache<T>> GetAllVaildCaches()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UpdateCache<T>> GetAllDirtyCaches()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Count
    {
      get
      {
        return this.Caches.Count;
      }
    }

    public int ValidCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsIndexValid(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<UpdateCache<T>> Caches { get; set; }
  }
}
