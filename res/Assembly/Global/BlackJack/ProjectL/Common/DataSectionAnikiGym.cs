﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionAnikiGym
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionAnikiGym : DataSection
  {
    private int m_challengedNums;
    private HashSet<int> m_finishedLevels;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionAnikiGym()
    {
    }

    public override void ClearInitedData()
    {
      this.m_finishedLevels.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitFinishedLevels(List<int> levels)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsLevelFinished(int levelId)
    {
      return this.m_finishedLevels.Contains(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFinishedLevel(int levelId)
    {
    }

    public HashSet<int> GetAllFinishedLevels()
    {
      return this.m_finishedLevels;
    }

    public void InitChallengedNums(int nums)
    {
      this.m_challengedNums = nums;
    }

    public void SetChallengedNums(int nums)
    {
      this.m_challengedNums = nums;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddChallengedNums(int nums)
    {
    }

    public int ChallengedNums
    {
      get
      {
        return this.m_challengedNums;
      }
    }
  }
}
