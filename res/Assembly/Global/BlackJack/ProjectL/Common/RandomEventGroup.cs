﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomEventGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RandomEventGroup
  {
    public int Index { get; set; }

    public int Nums { get; set; }

    public int MaxNums { get; set; }

    public bool CanGenerateRandomEvent()
    {
      return this.MaxNums > this.Nums;
    }
  }
}
