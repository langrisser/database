﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomEventLevelZone
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RandomEventLevelZone
  {
    public Dictionary<int, RandomEventGroup> Groups;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomEventLevelZone()
    {
    }

    public int LevelZoneId { get; set; }
  }
}
