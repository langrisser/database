﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.SurveyComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class SurveyComponentCommon : IComponentBase
  {
    protected BagComponentCommon m_bag;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected DataSectionSurvey m_survey;
    protected IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public SurveyComponentCommon()
    {
    }

    public string GetName()
    {
      return "Survey";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
      this.FlushSurvey();
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void FlushSurvey()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected SurveyStatus GetSurveyStatus(Survey survey)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetSurveyStatus(SurveyStatus status)
    {
      this.m_survey.SetSurveyStatus(status);
    }

    public void SetCurrentSurvey(Survey survey)
    {
      this.m_survey.SetCurrentSurvey(survey);
    }

    public SurveyStatus GetCurrentSurveyStatus()
    {
      return this.m_survey.GetCurrentSurveyStatus();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainSurveyReward()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
