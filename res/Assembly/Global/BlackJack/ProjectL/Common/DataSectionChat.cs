﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionChat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionChat : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitBannedTime(long bannedTime)
    {
      this.m_bannedTime = new DateTime(bannedTime);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBannedTimeExpired(DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Ban(DateTime bannedTime)
    {
      this.m_bannedTime = bannedTime;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Unban()
    {
    }

    public void InitSilentBannedTime(long bannedTime)
    {
      this.m_silentlyBannedTime = new DateTime(bannedTime);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSilentlyBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSilentBannedTimeExpired(DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SilentBan(DateTime bannedTime)
    {
      this.m_silentlyBannedTime = bannedTime;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SilentUnban()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanSendWorldChatByTime(int intervalTime, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime m_bannedTime { get; private set; }

    public DateTime m_silentlyBannedTime { get; private set; }

    public DateTime LastWorldChannelChatSendTime { get; set; }
  }
}
