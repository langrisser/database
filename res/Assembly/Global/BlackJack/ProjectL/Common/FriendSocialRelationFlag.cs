﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.FriendSocialRelationFlag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

namespace BlackJack.ProjectL.Common
{
  public enum FriendSocialRelationFlag
  {
    All = -1,
    None = 0,
    Friend = 1,
    Blacklist = 2,
    Invite = 4,
    Invited = 8,
    RecentContactsChat = 16, // 0x00000010
    RecentContactsTeamBattle = 32, // 0x00000020
  }
}
