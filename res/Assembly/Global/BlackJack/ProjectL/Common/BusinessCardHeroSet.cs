﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BusinessCardHeroSet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BusinessCardHeroSet
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCardHeroSet()
    {
      // ISSUE: unable to decompile the method.
    }

    public int HeroId { get; set; }

    public HeroDirectionType Direction { get; set; }

    public HeroActionType Action { get; set; }

    public int PositionIndex { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBusinessCardHeroSet ToProtocol(
      BusinessCardHeroSet heroSet)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProBusinessCardHeroSet> ToProtocols(
      List<BusinessCardHeroSet> heroSets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BusinessCardHeroSet FromProtocol(
      ProBusinessCardHeroSet pbHeroSet)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<BusinessCardHeroSet> FromProtocols(
      List<ProBusinessCardHeroSet> pbHeroSets)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
