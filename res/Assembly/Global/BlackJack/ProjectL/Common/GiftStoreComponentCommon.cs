﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GiftStoreComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class GiftStoreComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionGiftStore m_giftStoreDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected OperationalActivityCompomentCommon m_operationActivity;
    protected BagComponentCommon m_bag;
    protected ResourceComponentCommon m_resource;
    [DoNotToLua]
    private GiftStoreComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_OnFlushBoughtNums_hotfix;
    private LuaFunction m_GetOfferedStoreItemsByConfigBoolean_hotfix;
    private LuaFunction m_GetOfferedStoreItemsBoolean_hotfix;
    private LuaFunction m_IsOnSaleTimeDateTimeDateTimeDateTime_hotfix;
    private LuaFunction m_HasBoughtInt32_hotfix;
    private LuaFunction m_CanBuyGoodsInt32_hotfix;
    private LuaFunction m_CanAppleSubscribeGoodsInt32_hotfix;
    private LuaFunction m_AddFirstBuyGoodsRecordInt32String_hotfix;
    private LuaFunction m_GetAllOrderRewards_hotfix;
    private LuaFunction m_RemoveOrderRewardString_hotfix;
    private LuaFunction m_AddOrderRewardStringOrderReward_hotfix;
    private LuaFunction m_FindOrderRewardString_hotfix;
    private LuaFunction m_OnBuyGiftStoreGoodsConfigDataGiftStoreItemInfo_hotfix;
    private LuaFunction m_add_BuyGiftStoreGoodsEventAction`1_hotfix;
    private LuaFunction m_remove_BuyGiftStoreGoodsEventAction`1_hotfix;
    private LuaFunction m_add_BuyStoreItemEventAction`2_hotfix;
    private LuaFunction m_remove_BuyStoreItemEventAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreComponentCommon()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushBoughtNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<GiftStoreItem> GetOfferedStoreItemsByConfig(bool careShowInStore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GiftStoreItem> GetOfferedStoreItems(bool careShowInStore = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsOnSaleTime(DateTime saleStartTime, DateTime saleEndTime, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBought(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanBuyGoods(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAppleSubscribeGoods(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddFirstBuyGoodsRecord(int goodsId, string goodsRegisterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OrderReward> GetAllOrderRewards()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveOrderReward(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddOrderReward(string orderId, OrderReward reward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OrderReward FindOrderReward(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBuyGiftStoreGoods(ConfigDataGiftStoreItemInfo goodsInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> BuyGiftStoreGoodsEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> BuyStoreItemEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public GiftStoreComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_BuyGiftStoreGoodsEvent(int obj)
    {
    }

    private void __clearDele_BuyGiftStoreGoodsEvent(int obj)
    {
      this.BuyGiftStoreGoodsEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_BuyStoreItemEvent(int arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_BuyStoreItemEvent(int arg1, int arg2)
    {
      this.BuyStoreItemEvent = (Action<int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private GiftStoreComponentCommon m_owner;

      public LuaExportHelper(GiftStoreComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_BuyGiftStoreGoodsEvent(int obj)
      {
        this.m_owner.__callDele_BuyGiftStoreGoodsEvent(obj);
      }

      public void __clearDele_BuyGiftStoreGoodsEvent(int obj)
      {
        this.m_owner.__clearDele_BuyGiftStoreGoodsEvent(obj);
      }

      public void __callDele_BuyStoreItemEvent(int arg1, int arg2)
      {
        this.m_owner.__callDele_BuyStoreItemEvent(arg1, arg2);
      }

      public void __clearDele_BuyStoreItemEvent(int arg1, int arg2)
      {
        this.m_owner.__clearDele_BuyStoreItemEvent(arg1, arg2);
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public DataSectionGiftStore m_giftStoreDS
      {
        get
        {
          return this.m_owner.m_giftStoreDS;
        }
        set
        {
          this.m_owner.m_giftStoreDS = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public OperationalActivityCompomentCommon m_operationActivity
      {
        get
        {
          return this.m_owner.m_operationActivity;
        }
        set
        {
          this.m_owner.m_operationActivity = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public ResourceComponentCommon m_resource
      {
        get
        {
          return this.m_owner.m_resource;
        }
        set
        {
          this.m_owner.m_resource = value;
        }
      }

      public void OnFlushBoughtNums()
      {
        this.m_owner.OnFlushBoughtNums();
      }

      public List<GiftStoreItem> GetOfferedStoreItemsByConfig(bool careShowInStore)
      {
        return this.m_owner.GetOfferedStoreItemsByConfig(careShowInStore);
      }

      public bool IsOnSaleTime(DateTime saleStartTime, DateTime saleEndTime, DateTime currentTime)
      {
        return this.m_owner.IsOnSaleTime(saleStartTime, saleEndTime, currentTime);
      }

      public void OnBuyGiftStoreGoods(ConfigDataGiftStoreItemInfo goodsInfo)
      {
        this.m_owner.OnBuyGiftStoreGoods(goodsInfo);
      }
    }
  }
}
