﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.SelfSelectedBoxBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class SelfSelectedBoxBagItem : UseableBagItem
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public SelfSelectedBoxBagItem(
      GoodsType goodsTypeId,
      int contentId,
      int nums,
      ulong instanceId)
    {
    }

    public override int HaveEffect(IComponentOwner owner, params object[] param)
    {
      return this.Result;
    }

    private int Result { get; set; }
  }
}
