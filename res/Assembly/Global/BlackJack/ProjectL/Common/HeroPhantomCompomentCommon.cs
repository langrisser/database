﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroPhantomCompomentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class HeroPhantomCompomentCommon : IComponentBase
  {
    private IConfigDataLoader _configDataLoader;
    protected DataSectionHeroPhantom m_heroPhantomDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected HeroComponentCommon m_hero;
    protected RiftComponentCommon m_rift;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomCompomentCommon()
    {
    }

    public string GetName()
    {
      return "HeroPhantom";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckHeroPhantomAvailable(int HeroPhantomId, ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckHeroPhantomLevelAvailable(int LevelId, ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckPlayerOutOfBattle(ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckEnergy(int LevelId, ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckBag(int LevelId, ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CheckChallengeHeroPhantomLevel(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ChallengeHeroPhantomLevel(int LevelId, bool NoCheck = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonSuccessHeroPhantomLevel(
      HeroPhantomLevel Level,
      List<int> Heroes,
      List<int> BattleTreasures,
      List<int> newAchievementIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void FinishedHeroPhantomLevel(HeroPhantomLevel Level, List<int> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    public HeroPhantom GetHeroPhantom(int HeroPhantomId)
    {
      return this.m_heroPhantomDS.GetHeroPhantom(HeroPhantomId);
    }

    public HeroPhantomLevel GetHeroPhantomLevel(int LevelId)
    {
      return this.m_heroPhantomDS.GetHeroPhantomLevel(LevelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCompleteHeroPhantomLevelAchievement(int achievementRelatedInfoID)
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader m_configDataLoader
    {
      get
      {
        return this._configDataLoader;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllFinishedLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleType, int, List<int>> CompleteHeroPhantomMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<int> GetBattleAchievementMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
