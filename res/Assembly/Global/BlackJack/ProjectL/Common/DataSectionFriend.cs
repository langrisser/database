﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionFriend
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionFriend : DataSection
  {
    public List<string> LikedUsers;
    public List<string> FriendshipPointsSent;
    public List<string> FriendshipPointsReceived;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionFriend()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetBusinessCardInfo(BusinessCardInfoSet setInfo)
    {
      this.BusinessCardSetInfo = setInfo;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnFlushOfTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearLikedUsers()
    {
    }

    public void SetLikes(int likes)
    {
      this.Likes = likes;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddLikes(int addValue = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLikedUser(string userId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFriendshipPointsReceivedUser(string userId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RemoveFriendshipPointsReceivedUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RemoveFriendshipPointsSentUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFriendshipPointsSentUser(string userId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBusinessCardDesc(string desc)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBusinessCardHeroSet(List<BusinessCardHeroSet> heroSets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomHeroAction(bool actionRandom)
    {
    }

    public void SetFriendshipPointsFromFightWithFriendsToday(int points)
    {
      this.FriendshipPointsFromFightWithFriendsToday = points;
      this.SetDirty(true);
    }

    public void SetFriendshipPointsClaimedToday(int times)
    {
      this.FriendshipPointsClaimedToday = times;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Ban(DateTime banTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime BannedTime { get; set; }

    public BusinessCardInfoSet BusinessCardSetInfo { get; set; }

    public int Likes { get; set; }

    public int FriendshipPointsFromFightWithFriendsToday { get; set; }

    public int FriendshipPointsClaimedToday { get; set; }
  }
}
