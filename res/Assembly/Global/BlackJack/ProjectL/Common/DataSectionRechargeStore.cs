﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionRechargeStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionRechargeStore : DataSection
  {
    private HashSet<int> m_boughtGoodsList;
    public Dictionary<int, DateTime> m_banBuyingGoodsList;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionRechargeStore()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBoughtGoodsList(List<int> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearBoughtGoodsList()
    {
    }

    public bool IsGoodsBought(int goodsId)
    {
      return this.m_boughtGoodsList.Contains(goodsId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuyGoods(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    public HashSet<int> GetBoughtGoodsList()
    {
      return this.m_boughtGoodsList;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGoodsOnBanBuyingPeriod(int goodsId, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBanBuyingGoodsTime(int goodsId, DateTime expiredTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitmBanBuyingGoodsList(Dictionary<int, DateTime> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, DateTime> BanBuyingGoodsList
    {
      get
      {
        return this.m_banBuyingGoodsList;
      }
    }

    public int CurrentId { get; set; }
  }
}
