﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TrainingTech
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class TrainingTech
  {
    public int ConfigId { get; set; }

    public List<TrainingTechInfo> Infos
    {
      get
      {
        return this.Config.m_Infos;
      }
    }

    public int Level { get; set; }

    public ConfigDataTrainingTechInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateInstantiatedData(List<TrainingTech> AvailableTechs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TrainingTech FromDataSection(ProTrainingTech Tech)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProTrainingTech ToPro()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
