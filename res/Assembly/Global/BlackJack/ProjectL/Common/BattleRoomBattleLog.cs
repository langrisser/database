﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleRoomBattleLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BattleRoomBattleLog
  {
    public ulong RoomId { get; set; }

    public int BattleRoomType { get; set; }

    public int BattleId { get; set; }

    public int RandomNumberSeed { get; set; }

    public int ArmyRandomNumberSeed { get; set; }

    public List<BattleActorSetup> Team0 { get; set; }

    public List<BattleActorSetup> Team1 { get; set; }

    public List<BattlePlayer> Players { get; set; }
  }
}
