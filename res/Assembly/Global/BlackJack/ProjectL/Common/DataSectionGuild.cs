﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionGuild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionGuild : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionGuild()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.InvitedGuildIds.Clear();
    }

    public bool IsInInvitedGuild(string guildId)
    {
      return this.InvitedGuildIds.Contains(guildId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddJoinGuildInvitation(string guildId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveJoinGuildInvitation(string guildId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearJoinGuildInvitation()
    {
    }

    public string GuildId { get; set; }

    public DateTime NextJoinTime { get; set; }

    public List<string> InvitedGuildIds { get; set; }

    public GuildPlayerMassiveCombatInfo MassiveCombat { get; set; }
  }
}
