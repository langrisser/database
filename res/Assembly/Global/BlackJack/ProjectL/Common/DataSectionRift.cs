﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionRift
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionRift : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionRift()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitLevel(int chapterId, int levelId, int nums, int stars)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftChapter FindChapter(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftChapter AddChapter(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevel FindLevel(int chapterId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevel FindLevel(RiftChapter chapter, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLevel(RiftChapter chapter, int levelId, int nums, int stars)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLevel(RiftChapter chapter, int levelId, int nums, int stars)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLevelChallengeNums(RiftLevel riftLevel, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetLevelChallengeNums(RiftLevel riftLevel)
    {
      return riftLevel.Nums;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetLevelChallengeNums()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasGotAchievementRelationId(int achievementId)
    {
      return this.AchievementRelationIds.Contains(achievementId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAchievementRelationId(int achievementId, DateTime currTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAchievementRelationIds(
      List<int> achievementRelationIds,
      DateTime updateTime,
      int lastRiftAchievment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAllChapterStar(
      DateTime updateTime,
      int lastRiftStars,
      DateTime lastRiftRankUpdateTime)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChapterStarReward(int chapterId, int index)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGainChapterStarReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddChapterStarReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChapterTotalStars(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAllRiftLevelStars()
    {
      // ISSUE: unable to decompile the method.
    }

    public HashSet<int> FinishedRiftLevelIds { get; set; }

    public HashSet<int> AchievementRelationIds { get; set; }

    public DateTime AchievementUpdateTime { get; set; }

    public int LastRiftAchievementRank { get; set; }

    public Dictionary<int, RiftChapter> Chapters { get; set; }

    public int RiftStars
    {
      get
      {
        return this.GetAllRiftLevelStars();
      }
    }

    public DateTime RiftChaptersStarUpdateTime { get; set; }

    public int LastRiftStarsRank { get; set; }

    public DateTime LastRiftRankUpdateTime { get; set; }
  }
}
