﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TrainingGround
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class TrainingGround
  {
    public AnikiGymLevelClearCheck IsAnikiGymLevelCleared;
    public List<TrainingRoom> Rooms;
    private IConfigDataLoader _ConfigDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingGround()
    {
    }

    public IConfigDataLoader ConfigDataLoader
    {
      get
      {
        return this._ConfigDataLoader;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReloadConfigData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateInstantiatedData(List<TrainingTech> AvailableTechs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingRoom GetRoom(int RoomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTech GetLearntTech(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTech GetUnlearntTech(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TrainingTech> GetAvailableTechs()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<TrainingTech> IterateAvailableTechs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddNewLearntTech(TrainingTech Tech)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
