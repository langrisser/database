﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaOpponentDefensiveBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ArenaOpponentDefensiveBattleInfo
  {
    public ArenaPlayerDefensiveTeamSnapshot BattleTeamSnapshot;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentDefensiveBattleInfo()
    {
    }

    public ArenaBattleReportStatus Status { get; set; }

    public string OpponentUserId { get; set; }

    public int BattleRandomSeed { get; set; }

    public long BattleExpiredTime { get; set; }

    public int ArenaOpponentPointZoneId { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaOpponentDefensiveBattleInfo PBDefensiveBattleInfoToDefensiveBattleInfo(
      ProArenaDefensiveBattleInfo pbDefensiveBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaDefensiveBattleInfo DefensiveBattleInfoToPBDefensiveBattleInfo(
      ArenaOpponentDefensiveBattleInfo defensiveBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
