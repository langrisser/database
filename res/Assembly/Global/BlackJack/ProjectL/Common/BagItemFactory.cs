﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BagItemFactory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BagItemFactory
  {
    public IConfigDataLoader ConfigDataLoader { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ulong CreateBagItemInstanceId(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase CreateBagItem(GoodsType goodsTypeId, int contentId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase CreateInstanceBagItemByServer(
      GoodsType goodsTypeId,
      int contentId,
      int nums,
      ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase CreateInstanceBagItemByClient(ProGoods pbGoods)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
