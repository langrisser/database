﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionComment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionComment : DataSection
  {
    public DateTime m_bannedTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionComment()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitPlayerHeroCommentEntry(PlayerHeroCommentEntry playerCommentEntry)
    {
      this.m_playerHeroCommentEntries.Add(playerCommentEntry);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerHeroCommentEntry FindPlayerHeroCommentEntry(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PraiseHeroComment(int heroId, ulong commentInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CommentHero(int heroId, HeroCommentEntry commentEntry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Ban(DateTime bannedTime)
    {
      this.m_bannedTime = bannedTime;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Unban()
    {
    }

    public List<PlayerHeroCommentEntry> m_playerHeroCommentEntries { get; private set; }
  }
}
