﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionResource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionResource : DataSection
  {
    public PlayerOutOfBagItem m_resource;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionResource()
    {
    }

    public override void ClearInitedData()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<MonthCard> GetAllMonthCards()
    {
      return this.m_resource.MonthCards;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MonthCard FindMonthCardById(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddMonthCard(int monthCardId, DateTime expiredTime, string goodsId = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RemoveMonthCard(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveMonthCard(MonthCard monthCard)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasHeadFrameId(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddHeadFrame(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOwn(GoodsType goodtypeId, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroSkin(int heroSkinId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSoldierSkin(int soldierSkinId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddEquipmentId(int equipmentId)
    {
    }

    public void InitResource(PlayerOutOfBagItem resource)
    {
      this.m_resource = resource;
    }

    public PlayerOutOfBagItem Resource
    {
      get
      {
        return this.m_resource;
      }
    }
  }
}
