﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionArenaBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionArenaBattleReport : DataSection
  {
    private Dictionary<ulong, int> m_instanceId2CacheIndex;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionArenaBattleReport()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitArenaBattleReport(ArenaBattleReport report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaBattleReport(int index, ArenaBattleReport report)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddArenaBattleReport(ArenaBattleReport report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DirtyArenaBattleReport(ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<ArenaBattleReport> GetAllArenaBattleReports()
    {
      return this.ArenaBattleReportInfo.GetAllVaildDatas();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport FindArenaBattleReportByInstanceId(
      ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetNextBattleReportIndex(byte index)
    {
      this.NextBattleReportIndex = index;
      this.SetDirty(true);
    }

    public byte NextBattleReportIndex { get; set; }

    public int BattleReportNums
    {
      get
      {
        return this.m_instanceId2CacheIndex.Count;
      }
    }

    public ArenaBattleReportUpdateCache ArenaBattleReportInfo { get; set; }
  }
}
