﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ProcessingBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ProcessingBattle
  {
    public List<int> Params;
    public List<ulong> ULongParams;
    public List<string> StrParams;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProcessingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleType Type { get; set; }

    public int TypeId { get; set; }

    public int RandomSeed { get; set; }

    public int ArmyRandomSeed { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleProcessing BattleProcessingToPbBattleProcessing(
      ProcessingBattle battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProcessingBattle PbBattleProcessingToBattleProcessing(
      ProBattleProcessing pbBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
