﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleHeroJob
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BattleHeroJob
  {
    private int m_jobRelatedId;

    public int JobRelatedId
    {
      set
      {
        this.m_jobRelatedId = value;
        this.UpdateJobConnectionInfo();
      }
      get
      {
        return this.m_jobRelatedId;
      }
    }

    public int JobLevel { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroJob BattleHeroJobToHeroJob(BattleHeroJob battleHeroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHeroJob HeroJobToBattleHeroJob(HeroJob heroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHeroJob PBBattleHeroJobToBattleHeroJob(
      ProBattleHeroJob pbBattleHeroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleHeroJob BattleHeroJobToPBBattleHeroJob(
      BattleHeroJob battleHeroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateJobConnectionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo { private set; get; }

    public bool IsLevelMax()
    {
      return this.IsLevelMax(this.JobLevel);
    }

    public bool IsLevelMax(int jobLevel)
    {
      return this.JobConnectionInfo.IsJobLevelMax(jobLevel);
    }
  }
}
