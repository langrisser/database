﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionLevel : DataSection
  {
    private int m_currentWayPointId;
    private int m_lastFinishedScenarioId;
    public List<RandomEvent> RandomEvents;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionLevel()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetScenario(int id)
    {
      this.LastFinishedScenarioId = id;
      this.SetDirty(true);
    }

    public void InitScenario(int id)
    {
      this.LastFinishedScenarioId = id;
    }

    public void SetCurrentWayPoint(int wayPointId)
    {
      this.CurrentWayPointId = wayPointId;
      this.SetDirty(true);
    }

    public void InitCurrentWayPoint(int wayPointId)
    {
      this.CurrentWayPointId = wayPointId;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArrivedWayPointId(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomEvent(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void CleanWayPointEvent(int wayPointId)
    {
      this.CanMoveWayPointIds[wayPointId] = WayPointStatus.Arrived;
    }

    public void SetWayPointStatus(int wayPointId, WayPointStatus status)
    {
      this.CanMoveWayPointIds[wayPointId] = status;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSetRandomEvent(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistRandomEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCanMoveWayPointId(int wayPointId, WayPointStatus eventStatus)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddArrivedWayPoint(int wayPointId)
    {
      this.CanMoveWayPointIds.Add(wayPointId, WayPointStatus.Arrived);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWayPointArrived(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetNotEventWayPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetArrivedWayPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanExpandWayPoint(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool CanMoveWayPoint(int wayPointId)
    {
      return this.CanMoveWayPointIds.ContainsKey(wayPointId);
    }

    public int CurrentWayPointId
    {
      set
      {
        this.m_currentWayPointId = value;
        this.UpdateCurrentWaypointInfo();
      }
      get
      {
        return this.m_currentWayPointId;
      }
    }

    public int LastFinishedScenarioId
    {
      set
      {
        this.m_lastFinishedScenarioId = value;
        this.UpdateScenarioInfo();
      }
      get
      {
        return this.m_lastFinishedScenarioId;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitRandomEvents(List<RandomEvent> randomEvents)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomEvents(List<RandomEvent> randomEvents)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRandomEvent(RandomEvent randomEvent)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDeadLives(RandomEvent randomEvent, int deadLives)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveRandomEvent(RandomEvent randomEvent)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortByExpiredTimeAscend()
    {
      // ISSUE: unable to decompile the method.
    }

    public void ClearRandomEvents()
    {
      this.RandomEvents.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomEvent GetRandomEvent(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, WayPointStatus> CanMoveWayPointIds { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCurrentWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataScenarioInfo LastFinishedScenarioInfo { private set; get; }

    public ConfigDataWaypointInfo CurrentWaypointInfo { private set; get; }
  }
}
