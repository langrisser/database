﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class GuildComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    protected DataSectionGuild m_guildDS;
    private DateTime m_latGuildSearchTime;
    private DateTime m_latGuildRandomListTime;
    private DateTime m_latGuildInvitePlayerListTime;
    protected MissionComponentCommon m_mission;
    [DoNotToLua]
    private GuildComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_OnFlushPlayerGuild_hotfix;
    private LuaFunction m_HasOwnGuild_hotfix;
    private LuaFunction m_QuitGuildDateTime_hotfix;
    private LuaFunction m_GetGuildId_hotfix;
    private LuaFunction m_SetGuildIdString_hotfix;
    private LuaFunction m_CanCreateGuildStringStringInt32_hotfix;
    private LuaFunction m_CanJoinGuild_hotfix;
    private LuaFunction m_CanQuitGuild_hotfix;
    private LuaFunction m_CanKickOutGuild_hotfix;
    private LuaFunction m_CanApplyToJoinGuild_hotfix;
    private LuaFunction m_CanConfirmJoinGuildInvitationString_hotfix;
    private LuaFunction m_RefuseJoinGuildInvitationString_hotfix;
    private LuaFunction m_RefuseAllJoinGuildInvitation_hotfix;
    private LuaFunction m_CheckGuildNameString_hotfix;
    private LuaFunction m_CheckGuildSearchString_hotfix;
    private LuaFunction m_CheckGuildRandomList_hotfix;
    private LuaFunction m_CheckGuildInvitePlayerList_hotfix;
    private LuaFunction m_CanSetGuildHiringDeclarationString_hotfix;
    private LuaFunction m_CheckGuildHiringDeclarationString_hotfix;
    private LuaFunction m_CanSetGuildAnnouncementString_hotfix;
    private LuaFunction m_CheckGuildAnnouncementString_hotfix;
    private LuaFunction m_CanStartMassiveCombatInt32_hotfix;
    private LuaFunction m_CanTheseHeroesAttackStrongholdList`1_hotfix;
    private LuaFunction m_CanAttackStrongholdInt32_hotfix;
    private LuaFunction m_GetMassiveCombatUnusedHeroes_hotfix;
    private LuaFunction m_GetEliminateRateGuildMassiveCombatInfo_hotfix;
    private LuaFunction m_GetStrongholdEliminateRateGuildMassiveCombatStronghold_hotfix;
    private LuaFunction m_GetStartedCombatThisWeekGuildMassiveCombatGeneral_hotfix;
    private LuaFunction m_get_GuildId_hotfix;
    private LuaFunction m_get_NextJoinTime_hotfix;
    private LuaFunction m_get_GuildDS_hotfix;
    private LuaFunction m_get_GetGuildLastUpdateTime_hotfix;
    private LuaFunction m_set_GetGuildLastUpdateTimeInt64_hotfix;
    private LuaFunction m_add_JoinGuildEventAction_hotfix;
    private LuaFunction m_remove_JoinGuildEventAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildComponentCommon()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushPlayerGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOwnGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void QuitGuild(DateTime nextJoinTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetGuildId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGuildId(string id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCreateGuild(string guildName, string hiringDeclaration, int joinLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanJoinGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanQuitGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanKickOutGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanApplyToJoinGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanConfirmJoinGuildInvitation(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefuseJoinGuildInvitation(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefuseAllJoinGuildInvitation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CheckGuildName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildSearch(string searchText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildRandomList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildInvitePlayerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetGuildHiringDeclaration(string hiringDeclaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildHiringDeclaration(string hiringDeclaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetGuildAnnouncement(string announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildAnnouncement(string announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanStartMassiveCombat(int difficulty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTheseHeroesAttackStronghold(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackStronghold(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> GetMassiveCombatUnusedHeroes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEliminateRate(GuildMassiveCombatInfo combat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStrongholdEliminateRate(GuildMassiveCombatStronghold stronghold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStartedCombatThisWeek(GuildMassiveCombatGeneral generalInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public string GuildId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime NextJoinTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DataSectionGuild GuildDS
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long GetGuildLastUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action JoinGuildEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public GuildComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_JoinGuildEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_JoinGuildEvent()
    {
      this.JoinGuildEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private GuildComponentCommon m_owner;

      public LuaExportHelper(GuildComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_JoinGuildEvent()
      {
        this.m_owner.__callDele_JoinGuildEvent();
      }

      public void __clearDele_JoinGuildEvent()
      {
        this.m_owner.__clearDele_JoinGuildEvent();
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public DataSectionGuild m_guildDS
      {
        get
        {
          return this.m_owner.m_guildDS;
        }
        set
        {
          this.m_owner.m_guildDS = value;
        }
      }

      public DateTime m_latGuildSearchTime
      {
        get
        {
          return this.m_owner.m_latGuildSearchTime;
        }
        set
        {
          this.m_owner.m_latGuildSearchTime = value;
        }
      }

      public DateTime m_latGuildRandomListTime
      {
        get
        {
          return this.m_owner.m_latGuildRandomListTime;
        }
        set
        {
          this.m_owner.m_latGuildRandomListTime = value;
        }
      }

      public DateTime m_latGuildInvitePlayerListTime
      {
        get
        {
          return this.m_owner.m_latGuildInvitePlayerListTime;
        }
        set
        {
          this.m_owner.m_latGuildInvitePlayerListTime = value;
        }
      }

      public MissionComponentCommon m_mission
      {
        get
        {
          return this.m_owner.m_mission;
        }
        set
        {
          this.m_owner.m_mission = value;
        }
      }

      public void OnFlushPlayerGuild()
      {
        this.m_owner.OnFlushPlayerGuild();
      }
    }
  }
}
