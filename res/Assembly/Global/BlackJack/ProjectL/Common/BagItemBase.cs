﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BagItemBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BagItemBase
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase(GoodsType goodsTypeId, int contentId, int nums, ulong instanceId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual BagItemBase Clone()
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsInstanceBagItem(GoodsType goodsTypeId)
    {
      return goodsTypeId == GoodsType.GoodsType_Equipment;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    public string ListItemName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GoodsType GoodsTypeId { set; get; }

    public int ContentId { set; get; }

    public int Nums { set; get; }

    public ulong InstanceId { set; get; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsBagItem(GoodsType goodsType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsMoney(GoodsType goodsType)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsEnough(int consumeNums)
    {
      return consumeNums <= this.Nums;
    }

    public bool IsThisGoodsType(GoodsType goodsType)
    {
      return this.GoodsTypeId == goodsType;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual ProGoods ToPBGoods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Goods ToGoods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BagItemBase PBGoodsToBagItem(
      BagItemFactory bagItemFactory,
      ProGoods pbGoods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<BagItemBase> PBGoodsToBagItems(
      BagItemFactory bagItemFactory,
      List<ProGoods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProGoods> BagItemsToPBGoods(List<BagItemBase> bagItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProGoods> GoodsListToPBGoodsList(List<Goods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<Goods> PBGoodsListToGoodsList(List<ProGoods> pbGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGoods GoodsToPBGoods(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Goods PBGoodsToGoods(ProGoods pbGoods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateConfigData()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataItemInfo ItemInfo { private set; get; }

    public ConfigDataJobMaterialInfo JobMaterialInfo { private set; get; }

    public ConfigDataEquipmentInfo EquipmentInfo { private set; get; }

    public ConfigDataEnchantStoneInfo EnchantStoneInfo { private set; get; }
  }
}
