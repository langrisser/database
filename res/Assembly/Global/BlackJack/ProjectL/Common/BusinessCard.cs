﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BusinessCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BusinessCard
  {
    public List<BattleHero> Heroes;
    public List<TrainingTech> Techs;
    public List<BattleHero> MostSkilledHeroes;

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCard()
    {
      // ISSUE: unable to decompile the method.
    }

    public string UserId { get; set; }

    public string Name { get; set; }

    public int Level { get; set; }

    public int HeadIcon { get; set; }

    public int ArenaPoints { get; set; }

    public int Likes { get; set; }

    public bool IsOnLine { get; set; }

    public BusinessCardInfoSet SetInfo { get; set; }

    public BusinessCardStatisticalData StatisticalData { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBusinessCard ToProtocol(BusinessCard businessCard)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BusinessCard FromProtocol(ProBusinessCard pbBusinessCard)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
