﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UserSummary
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class UserSummary
  {
    public int Level = 1;
    public string UserId;
    public string Name;
    public int HeadIcon;
    public bool Online;
    public DateTime LogoutTime;
    public int TopHeroBattlePower;
  }
}
