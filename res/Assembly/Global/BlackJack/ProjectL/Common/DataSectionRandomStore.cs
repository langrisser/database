﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionRandomStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionRandomStore : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionRandomStore()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.Stores.Clear();
    }

    public void InitStore(RandomStore store)
    {
      this.Stores.Add(store);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddStore(RandomStore store)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStore FindStore(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStore(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    public void BuyStoreItem(RandomStoreItem storeItem)
    {
      storeItem.Bought = true;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetManualFlushNums(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetStoreNextFlushTime(RandomStore store, DateTime flushTime)
    {
      store.NextFlushTime = flushTime;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddStoreItems(RandomStore store, List<RandomStoreItem> storeItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearStoreItems(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    public RandomStoreItem GetStoreItem(RandomStore store, int index)
    {
      return store.Items[index];
    }

    public void SetManualFlushNums(RandomStore store, int nums)
    {
      store.ManualFlushNums = nums;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddManualFlushNums(RandomStore store, int addNums)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<RandomStore> Stores { get; set; }
  }
}
