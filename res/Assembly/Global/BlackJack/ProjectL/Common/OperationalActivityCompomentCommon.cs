﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.OperationalActivityCompomentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class OperationalActivityCompomentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    protected AnikiGymComponentCommon m_anikiGym;
    protected ThearchyTrialCompomentCommon m_thearchyTrial;
    protected MemoryCorridorCompomentCommon m_memoryCorridor;
    protected HeroTrainningComponentCommon m_heroTrainning;
    protected CooperateBattleCompomentCommon m_cooperateBattle;
    protected RiftComponentCommon m_rift;
    protected SelectCardComponentCommon m_selectCard;
    protected HeroDungeonComponentCommon m_heroDungeon;
    protected EternalShrineCompomentCommon m_eternalShrine;
    protected RaffleComponentCommon m_raffle;
    protected UnchartedScoreComponentCommon m_unchartedScore;
    protected FixedStoreComponentCommon m_fixedStore;
    protected RandomStoreComponentCommon m_randomStore;
    protected GiftStoreComponentCommon m_giftStore;
    protected RechargeStoreComponentCommon m_rechargeStore;
    protected CollectionComponentCommon m_collectionActivity;
    protected DataSectionOperationalActivity m_operationalActivityDS;
    protected DataSectionAnnouncement m_announcementDS;
    [DoNotToLua]
    private OperationalActivityCompomentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_OnFlushLoginDaysEvent_hotfix;
    private LuaFunction m_OnFlushSuccessCallBackOperationalActivityBase_hotfix;
    private LuaFunction m_OnPlayerLevelUpEventInt32_hotfix;
    private LuaFunction m_AddNewOperationalActivityOperationalActivityBase_hotfix;
    private LuaFunction m_PreInitNewNewOperationalActivityInfoOperationalActivityBase_hotfix;
    private LuaFunction m_InitNewNewOperationalActivityInfoOperationalActivityBase_hotfix;
    private LuaFunction m_OnAddNewActivityCallBackOperationalActivityBase_hotfix;
    private LuaFunction m_OnActivityInitCallBackOperationalActivityBase_hotfix;
    private LuaFunction m_RemoveAllExpiredOperationalActivities_hotfix;
    private LuaFunction m_CanExchangeItemGroupLimitedTimeExchangeOperationActivityInt32_hotfix;
    private LuaFunction m_CanGainOperactionalActivityRewardOperationalActivityBaseInt32_hotfix;
    private LuaFunction m_GetAllOperationalActivities_hotfix;
    private LuaFunction m_GetAllValidOperationalActivities_hotfix;
    private LuaFunction m_GetAwardOperationActivityRewardItemGroupIdByIndexOperationalActivityBaseInt32_hotfix;
    private LuaFunction m_EffectOperationActivityGenerateEffectOperationalActivityBaseBoolean_hotfix;
    private LuaFunction m_FindOperationalActivityByIdUInt64_hotfix;
    private LuaFunction m_FindOperationalActivitiesByTypeOperationalActivityType_hotfix;
    private LuaFunction m_FindOperationalActivityByActivityCardPoolIdInt32_hotfix;
    private LuaFunction m_FindOperationalActivityByRafflePoolIdInt32_hotfix;
    private LuaFunction m_FindOperationalActivityByUnchartedScoreIdInt32_hotfix;
    private LuaFunction m_FindShowActivityByUnchartedScoreIdInt32_hotfix;
    private LuaFunction m_FindOperationalActivityByCollectionActivityIdInt32_hotfix;
    private LuaFunction m_FindExsitOperationalActivityByCollectionActivityIdInt32_hotfix;
    private LuaFunction m_FindShowActivityByCollectionActivityIdInt32_hotfix;
    private LuaFunction m_FindGainRewardActivityByCollectionActivityIdInt32_hotfix;
    private LuaFunction m_OnAddRechargeRMBEventInt32DateTime_hotfix;
    private LuaFunction m_OnConsumeCrystalEventInt32_hotfix;
    private LuaFunction m_GetAllAdvertisementFlowLayouts_hotfix;
    private LuaFunction m_OnBuyStoreItemCallBackInt32Int32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityCompomentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnFlushLoginDaysEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnFlushSuccessCallBack(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlayerLevelUpEvent(int palyerLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddNewOperationalActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PreInitNewNewOperationalActivityInfo(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitNewNewOperationalActivityInfo(OperationalActivityBase operationalActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnAddNewActivityCallBack(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnActivityInitCallBack(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsEffectOperationalActivity(OperationalActivityBase operationalActivity)
    {
      return operationalActivity is EffectOperationalActivity;
    }

    public static bool IsAwardOperationActivity(OperationalActivityBase operationalActivity)
    {
      return operationalActivity is AwardOperationalActivityBase;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllExpiredOperationalActivities()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanExchangeItemGroup(
      LimitedTimeExchangeOperationActivity limitedTimeExchangeOperationalActivity,
      int itemGroupIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainOperactionalActivityReward(
      OperationalActivityBase operationalActivityBase,
      int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> GetAllOperationalActivities()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> GetAllValidOperationalActivities()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int GetAwardOperationActivityRewardItemGroupIdByIndex(
      OperationalActivityBase operationalActivity,
      int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void EffectOperationActivityGenerateEffect(
      OperationalActivityBase operationalActivity,
      bool isPositive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityById(
      ulong operationalActivityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> FindOperationalActivitiesByType(
      OperationalActivityType activityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityByActivityCardPoolId(
      int activityCardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityByRafflePoolId(
      int rafflePoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityByUnchartedScoreId(
      int UnchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindShowActivityByUnchartedScoreId(
      int UnchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityByCollectionActivityId(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindExsitOperationalActivityByCollectionActivityId(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindShowActivityByCollectionActivityId(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindGainRewardActivityByCollectionActivityId(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddRechargeRMBEvent(int nums, DateTime rechargeTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConsumeCrystalEvent(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<AdvertisementFlowLayout> GetAllAdvertisementFlowLayouts()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBuyStoreItemCallBack(int storeId, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public OperationalActivityCompomentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private OperationalActivityCompomentCommon m_owner;

      public LuaExportHelper(OperationalActivityCompomentCommon owner)
      {
        this.m_owner = owner;
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public AnikiGymComponentCommon m_anikiGym
      {
        get
        {
          return this.m_owner.m_anikiGym;
        }
        set
        {
          this.m_owner.m_anikiGym = value;
        }
      }

      public ThearchyTrialCompomentCommon m_thearchyTrial
      {
        get
        {
          return this.m_owner.m_thearchyTrial;
        }
        set
        {
          this.m_owner.m_thearchyTrial = value;
        }
      }

      public MemoryCorridorCompomentCommon m_memoryCorridor
      {
        get
        {
          return this.m_owner.m_memoryCorridor;
        }
        set
        {
          this.m_owner.m_memoryCorridor = value;
        }
      }

      public HeroTrainningComponentCommon m_heroTrainning
      {
        get
        {
          return this.m_owner.m_heroTrainning;
        }
        set
        {
          this.m_owner.m_heroTrainning = value;
        }
      }

      public CooperateBattleCompomentCommon m_cooperateBattle
      {
        get
        {
          return this.m_owner.m_cooperateBattle;
        }
        set
        {
          this.m_owner.m_cooperateBattle = value;
        }
      }

      public RiftComponentCommon m_rift
      {
        get
        {
          return this.m_owner.m_rift;
        }
        set
        {
          this.m_owner.m_rift = value;
        }
      }

      public SelectCardComponentCommon m_selectCard
      {
        get
        {
          return this.m_owner.m_selectCard;
        }
        set
        {
          this.m_owner.m_selectCard = value;
        }
      }

      public HeroDungeonComponentCommon m_heroDungeon
      {
        get
        {
          return this.m_owner.m_heroDungeon;
        }
        set
        {
          this.m_owner.m_heroDungeon = value;
        }
      }

      public EternalShrineCompomentCommon m_eternalShrine
      {
        get
        {
          return this.m_owner.m_eternalShrine;
        }
        set
        {
          this.m_owner.m_eternalShrine = value;
        }
      }

      public RaffleComponentCommon m_raffle
      {
        get
        {
          return this.m_owner.m_raffle;
        }
        set
        {
          this.m_owner.m_raffle = value;
        }
      }

      public UnchartedScoreComponentCommon m_unchartedScore
      {
        get
        {
          return this.m_owner.m_unchartedScore;
        }
        set
        {
          this.m_owner.m_unchartedScore = value;
        }
      }

      public FixedStoreComponentCommon m_fixedStore
      {
        get
        {
          return this.m_owner.m_fixedStore;
        }
        set
        {
          this.m_owner.m_fixedStore = value;
        }
      }

      public RandomStoreComponentCommon m_randomStore
      {
        get
        {
          return this.m_owner.m_randomStore;
        }
        set
        {
          this.m_owner.m_randomStore = value;
        }
      }

      public GiftStoreComponentCommon m_giftStore
      {
        get
        {
          return this.m_owner.m_giftStore;
        }
        set
        {
          this.m_owner.m_giftStore = value;
        }
      }

      public RechargeStoreComponentCommon m_rechargeStore
      {
        get
        {
          return this.m_owner.m_rechargeStore;
        }
        set
        {
          this.m_owner.m_rechargeStore = value;
        }
      }

      public CollectionComponentCommon m_collectionActivity
      {
        get
        {
          return this.m_owner.m_collectionActivity;
        }
        set
        {
          this.m_owner.m_collectionActivity = value;
        }
      }

      public DataSectionOperationalActivity m_operationalActivityDS
      {
        get
        {
          return this.m_owner.m_operationalActivityDS;
        }
        set
        {
          this.m_owner.m_operationalActivityDS = value;
        }
      }

      public DataSectionAnnouncement m_announcementDS
      {
        get
        {
          return this.m_owner.m_announcementDS;
        }
        set
        {
          this.m_owner.m_announcementDS = value;
        }
      }

      public void OnFlushSuccessCallBack(OperationalActivityBase activity)
      {
        this.m_owner.OnFlushSuccessCallBack(activity);
      }

      public void PreInitNewNewOperationalActivityInfo(OperationalActivityBase operationalActivity)
      {
        this.m_owner.PreInitNewNewOperationalActivityInfo(operationalActivity);
      }

      public void InitNewNewOperationalActivityInfo(OperationalActivityBase operationalActivityInfo)
      {
        this.m_owner.InitNewNewOperationalActivityInfo(operationalActivityInfo);
      }

      public void OnAddNewActivityCallBack(OperationalActivityBase activity)
      {
        this.m_owner.OnAddNewActivityCallBack(activity);
      }

      public void OnActivityInitCallBack(OperationalActivityBase activity)
      {
        this.m_owner.OnActivityInitCallBack(activity);
      }

      public int GetAwardOperationActivityRewardItemGroupIdByIndex(
        OperationalActivityBase operationalActivity,
        int rewardIndex)
      {
        return this.m_owner.GetAwardOperationActivityRewardItemGroupIdByIndex(operationalActivity, rewardIndex);
      }

      public void EffectOperationActivityGenerateEffect(
        OperationalActivityBase operationalActivity,
        bool isPositive)
      {
        this.m_owner.EffectOperationActivityGenerateEffect(operationalActivity, isPositive);
      }

      public void OnAddRechargeRMBEvent(int nums, DateTime rechargeTime)
      {
        this.m_owner.OnAddRechargeRMBEvent(nums, rechargeTime);
      }

      public void OnConsumeCrystalEvent(int nums)
      {
        this.m_owner.OnConsumeCrystalEvent(nums);
      }
    }
  }
}
