﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleRoomPlayerHeroSetup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BattleRoomPlayerHeroSetup
  {
    private List<BattleHeroSetupInfo> m_battleHeroSetupInfos;
    private int m_pvpHeroPositionCount0;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayerHeroSetup()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitTeamBattle(int playerCount, int heroPosCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitPVPBattle(int heroPosCount0, int heroPosCount1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPlayerIndex(int playerIdx, int heroPos, int heroPosCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetCount()
    {
      return this.m_battleHeroSetupInfos.Count;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SetHero(int playerIdx, int heroPos, BattleHero hero, bool checkHeroDup = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SwapHeros(int playerIdx, int heroPos1, int heroPos2, bool checkPos2HeroExist)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleHeroActionPositionIndex(int heroPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool MarkSetupHeroFlag(int playerIdx, int heroPos, SetupBattleHeroFlag Flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroSetupInfo GetBattleHeroSetupInfo(int heroPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindHeroPosition(int playerIdx, int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroCountByPlayerIndex(int playerIdx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetHeroIdsByPlayerIndex(int playerIdx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroPositionCountByPlayerIndex(int playerIdx)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<BattleHeroSetupInfo> GetBattleHeroSetupInfos()
    {
      return this.m_battleHeroSetupInfos;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int PVPRoomPositionToTeamPosition(int playerIndex, int heroPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int PVPTeamPositionToRoomPosition(int playerIndex, int heroPos)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
