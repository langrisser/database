﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RandomEvent
  {
    public int EventId { get; set; }

    public int WayPointId { get; set; }

    public int Lives { get; set; }

    public int DeadLives { get; set; }

    public long ExpiredTime { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRandomEvent RandomEventToPBRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProRandomEvent> RandomEventsToPBRandomEvents(
      List<RandomEvent> randomEvents)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RandomEvent PBRandomEventToRandomEvent(ProRandomEvent pbRadomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<RandomEvent> PBRandomEventsToRandomEvents(
      List<ProRandomEvent> pbRadomEvents)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
