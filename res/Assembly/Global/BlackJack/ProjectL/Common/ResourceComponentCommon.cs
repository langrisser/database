﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ResourceComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ResourceComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionResource m_resourceDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;

    [MethodImpl((MethodImplOptions) 32768)]
    public ResourceComponentCommon()
    {
    }

    public string GetName()
    {
      return "Resource";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void EffectValidMonthCard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<MonthCard> GetAllValidMonthCards()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddMonthCard(int monthCardId, DateTime expiredTime, string goodsId = null)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RemoveMonthCard(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    public MonthCard FindMonthCardById(int cardId)
    {
      return this.m_resourceDS.FindMonthCardById(cardId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMonthCardVaild(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMonthCardValid(int monthCardId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMonthCardInvalid(int monthCardId)
    {
    }

    public bool HasHeadFrameId(int headFrameId)
    {
      return this.m_resourceDS.HasHeadFrameId(headFrameId);
    }

    public virtual void AddHeadFrame(int headFrameId, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      this.m_resourceDS.AddHeadFrame(headFrameId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddHeroSkin(int heroSkinId, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddSoldierSkin(
      int soldierSkinId,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOwn(GoodsType goodtypeId, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCreateBagItemEventCallBack(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddEquipmentId(int equipmentId)
    {
    }

    public event Action<int> MonthCardValidEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<int> MonthCardInvalidEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
