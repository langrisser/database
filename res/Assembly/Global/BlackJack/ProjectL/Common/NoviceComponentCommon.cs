﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.NoviceComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class NoviceComponentCommon : IComponentBase
  {
    private IConfigDataLoader _configDataLoader;
    protected List<ConfigDataNoviceRewardInfo> m_novicePointsRewards;
    public TimeSpan m_noviceMissionDuration;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    protected MissionComponentCommon m_mission;
    protected DataSectionNovice m_noviceDS;
    [DoNotToLua]
    private NoviceComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_GetMissionPoints_hotfix;
    private LuaFunction m_AddMissionPointsInt32_hotfix;
    private LuaFunction m_GetRewardClaimedSlots_hotfix;
    private LuaFunction m_IsRewardClaimedInt32_hotfix;
    private LuaFunction m_CanClaimRewardInt32_hotfix;
    private LuaFunction m_ClaimRewardInt32Boolean_hotfix;
    private LuaFunction m_GetNovicePointsRewardsConfig_hotfix;
    private LuaFunction m_GetDaysAfterCreation_hotfix;
    private LuaFunction m_GetProcessingMissions_hotfix;
    private LuaFunction m_GetFinishedMissions_hotfix;
    private LuaFunction m_GetMissionDayConfigDataMissionInfo_hotfix;
    private LuaFunction m_GetMissions_hotfix;
    private LuaFunction m_GetMissionsEndTime_hotfix;
    private LuaFunction m_get_m_configDataLoader_hotfix;
    private LuaFunction m_set_m_configDataLoaderIConfigDataLoader_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public NoviceComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMissionPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddMissionPoints(int Delta)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetRewardClaimedSlots()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRewardClaimed(int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanClaimReward(int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int ClaimReward(int Slot, bool NoCheck = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataNoviceRewardInfo> GetNovicePointsRewardsConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDaysAfterCreation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetProcessingMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetFinishedMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMissionDay(ConfigDataMissionInfo Mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, List<int>> GetMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan GetMissionsEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader m_configDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public NoviceComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private NoviceComponentCommon m_owner;

      public LuaExportHelper(NoviceComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public IConfigDataLoader _configDataLoader
      {
        get
        {
          return this.m_owner._configDataLoader;
        }
        set
        {
          this.m_owner._configDataLoader = value;
        }
      }

      public List<ConfigDataNoviceRewardInfo> m_novicePointsRewards
      {
        get
        {
          return this.m_owner.m_novicePointsRewards;
        }
        set
        {
          this.m_owner.m_novicePointsRewards = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public MissionComponentCommon m_mission
      {
        get
        {
          return this.m_owner.m_mission;
        }
        set
        {
          this.m_owner.m_mission = value;
        }
      }

      public DataSectionNovice m_noviceDS
      {
        get
        {
          return this.m_owner.m_noviceDS;
        }
        set
        {
          this.m_owner.m_noviceDS = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }
    }
  }
}
