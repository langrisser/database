﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionAnnouncement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionAnnouncement : DataSection
  {
    private List<Announcement> m_announcements;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionAnnouncement()
    {
    }

    public override void ClearInitedData()
    {
      this.m_announcements.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitAnnouncements(List<Announcement> announcements)
    {
      this.m_announcements.AddRange((IEnumerable<Announcement>) announcements);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnnouncements(List<Announcement> announcements)
    {
    }

    public void SetClientAnnouncementCurrentVersion(uint currentVersion)
    {
      this.ClientCurrentVersion = currentVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetServerAnnouncementCurrentVersion(int serverCurrentVersion)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAnnouncement(Announcement announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Announcement> FindAnnouncementsByCondition(
      Predicate<Announcement> isConditionMatched)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Announcement FindAnnouncementsByInstanceId(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAnnouncement(Announcement announcement)
    {
    }

    public uint ClientCurrentVersion { get; set; }
  }
}
