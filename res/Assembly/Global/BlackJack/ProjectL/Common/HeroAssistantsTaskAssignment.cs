﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroAssistantsTaskAssignment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class HeroAssistantsTaskAssignment
  {
    public int TaskId;
    public HeroAssistantsTask Task;
    public List<int> AssignedHeroIds;
    public DateTime StartTime;
    public DateTime EndTime;
    public int Slot;
    public int? _CompleteRate;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAssistantsTaskAssignment()
    {
    }

    public int AssignPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CompleteRate
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
