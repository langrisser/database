﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionMail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionMail : DataSection
  {
    private Dictionary<ulong, Mail> m_mails;
    private int m_globalMailId;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionMail()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.m_mails.Clear();
    }

    public bool IsMailReaded(Mail mail)
    {
      return mail.Status >= 1;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail AddMail(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitMail(Mail mail)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveMail(Mail mail)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearMailBox()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail FindMail(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadMail(Mail mail, DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGotAttachments(Mail mail, DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitGlobalMailId(int value)
    {
      this.m_globalMailId = value;
    }

    public void SetGlobalMailId(int value)
    {
      this.m_globalMailId = value;
      this.SetDirty(true);
    }

    public int GlobalMailId
    {
      get
      {
        return this.m_globalMailId;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail GetFirstMail()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetMailBoxSize()
    {
      return this.m_mails.Count;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mail> GetMails()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
