﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionUnchartedScore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionUnchartedScore : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionUnchartedScore()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.UnchartedScores.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveUnchartedScore(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, UnchartedScore> UnchartedScores { get; set; }
  }
}
