﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RealTimePVPInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RealTimePVPInfo
  {
    public string UserId { get; set; }

    public int Score { get; set; }

    public int LocalRank { get; set; }

    public int GlobalRank { get; set; }

    public int Dan { get; set; }

    public bool IsBot { get; set; }
  }
}
