﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AccumulateLoginOperationalActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class AccumulateLoginOperationalActivity : AwardOperationalActivityBase
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public AccumulateLoginOperationalActivity()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AccumulateLoginOperationalActivity(
      ulong instanceId,
      int operationalActivityId,
      OperationalActivityType operationalActivityType)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeserializeFromPB(
      ProAccumulateLoginOperationalActivity pbOperationalActivity,
      ConfigDataOperationalActivityInfo config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ToPBNtf(DSOperationalActivityNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAccumulateLoginOperationalActivity SerializeToPB()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int CanGainRewardByIndex(int rewardIndex, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddLoginTime(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<long> SpecificLoginTimes { get; set; }
  }
}
