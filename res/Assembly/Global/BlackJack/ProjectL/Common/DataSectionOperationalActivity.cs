﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionOperationalActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionOperationalActivity : DataSection
  {
    private List<OperationalActivityBase> m_operationalActivities;
    private List<AdvertisementFlowLayout> m_layouts;
    private WebInfoControl m_webInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionOperationalActivity()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityById(
      ulong operationalActivityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> FindOperationalActivitiesByType(
      OperationalActivityType activityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitOperationalActivities(
      List<OperationalActivityBase> operationalActivities)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOperationalActivities(
      List<OperationalActivityBase> operationalActivities)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddAccumulateLoginTime(
      AccumulateLoginOperationalActivity accumulateLoginOperationalActivity,
      DateTime loginTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddRechargeRMB(AccumulateRechargeOperationalActivity activity, int nums)
    {
      activity.AddRechargeRMB(nums);
      this.SetDirty(true);
    }

    public void AddConsumeCrystal(
      AccumulateConsumeCrystalOperationalActivity activity,
      int nums)
    {
      activity.AddConsumeCrystal(nums);
      this.SetDirty(true);
    }

    public void SetPlayerLevel(
      PlayerLevelUpOperationalActivity playerLevelUpOperationalActivity,
      int playerLevel)
    {
      playerLevelUpOperationalActivity.PlayerLevel = playerLevel;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSpecificLoginTimes(
      SpecificDaysLoginOperationalActivity specificDaysLoginOperationalActivity,
      long time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ExchangeItemGroup(
      LimitedTimeExchangeOperationActivity limitedTimeExchangeOperationActivity,
      int itemGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void GainReward(AwardOperationalActivityBase operationalActivity, int rewardIndex)
    {
      operationalActivity.GainReward(rewardIndex);
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearOperationalActivities()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddNewOperationalActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateOperationalActivity(OperationalActivityBase operationalActivity)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveExpiredOperationalActivity(OperationalActivityBase operatinoalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ClearAdvertisementLayout()
    {
      this.m_layouts.Clear();
    }

    public void InitAdvertisementLayout(AdvertisementFlowLayout layout)
    {
      this.m_layouts.Add(layout);
    }

    public List<AdvertisementFlowLayout> GetAllAdvertisementFlowLayouts()
    {
      return this.m_layouts;
    }

    public void InitWebInfo(WebInfoControl info)
    {
      this.m_webInfo = info;
    }

    public WebInfoControl GetWebInfo()
    {
      return this.m_webInfo;
    }

    public List<OperationalActivityBase> AllOperationalActivities
    {
      get
      {
        return this.m_operationalActivities;
      }
    }
  }
}
