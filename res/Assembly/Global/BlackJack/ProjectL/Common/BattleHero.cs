﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleHero
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BattleHero
  {
    private int m_heroId;
    public List<BattleHeroJob> Jobs;
    public List<int> SelectedSkillList;
    private int m_selectedSoldierId;
    public List<BattleHeroEquipment> Equipments;
    public Dictionary<int, int> Fetters;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero()
    {
      // ISSUE: unable to decompile the method.
    }

    public int HeroId
    {
      set
      {
        this.m_heroId = value;
        this.UpdateHeroInfo();
      }
      get
      {
        return this.m_heroId;
      }
    }

    public int ActionPositionIndex { get; set; }

    public int ActionValue { get; set; }

    public int Level { get; set; }

    public int StarLevel { get; set; }

    public int ActiveHeroJobRelatedId { get; set; }

    public int ModelSkinId { get; set; }

    public int CharSkinId { get; set; }

    public int SelectedSoldierSkinId { get; set; }

    public int SelectedSoldierId
    {
      set
      {
        this.m_selectedSoldierId = value;
        this.UpdateSoldierInfo();
      }
      get
      {
        return this.m_selectedSoldierId;
      }
    }

    public int HeartFetterLevel { get; set; }

    public int Power { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroJob GetJob(int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleHeroJob GetActiveJob()
    {
      return this.GetJob(this.ActiveHeroJobRelatedId);
    }

    public ConfigDataCharImageSkinResourceInfo GetHeroCharImageSkinResourceInfo(
      IConfigDataLoader configDataLoader)
    {
      return BattleUtility.GetHeroCharImageSkinResourceInfo(configDataLoader, this.CharSkinId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo GetHeroActiveJobModelSkinResourceInfo(
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo GetSelectedSoldierModelSkinResourceInfo(
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHero HeroToBattleHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHero PBBattleHeroToBattleHero(ProBattleHero pbBattleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleHero BattleHeroToPBBattleHero(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleHero ViewBattleHeroToPBBattleHero(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHero PBBattleHeroToViewBattleHero(ProBattleHero pbBattleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSoldierInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataHeroInfo HeroInfo { private set; get; }

    public ConfigDataSoldierInfo SelectedSoldierInfo { private set; get; }
  }
}
