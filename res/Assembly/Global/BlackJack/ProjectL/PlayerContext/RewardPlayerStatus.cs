﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.RewardPlayerStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class RewardPlayerStatus
  {
    public int Level;
    public int Exp;
    public int NextLevelExp;
    public int Energy;
    public int ArenaVictoryPoints;
    public int ArenaLevelID;
    public int RealTimePVPScore;
    public int RealTimePVPDan;
    public bool RealTimePVPIsPromotion;
    public BattleMode RealtTimePVPBattleMode;
    public int ClimbTowerFinishedFloorId;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
