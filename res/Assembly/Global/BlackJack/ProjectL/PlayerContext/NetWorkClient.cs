﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.NetWorkClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.PlayerContext;
using BlackJack.ProjectL.LibClient;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class NetWorkClient : IPlayerContextNetworkClient
  {
    private IClient m_client;

    public NetWorkClient(IClient realClient)
    {
      this.m_client = realClient;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool LoginByAuthToken(
      string serverAddress,
      int serverPort,
      string authToken,
      string clientVersion,
      string clientDeviceId,
      string localization,
      int loginChannelId,
      int bornChannelId,
      string serverDomain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool LoginBySessionToken(
      string sessionToken,
      string clientVersion,
      string localization,
      int loginChannelId,
      int bornChannelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool Disconnect()
    {
      return this.m_client.Disconnect();
    }

    public bool SendMessage(object msg)
    {
      return this.m_client.SendMessage(msg);
    }

    public void Tick()
    {
      this.m_client.Tick();
    }

    public void Close()
    {
      this.m_client.Close();
    }

    public void BlockProcessMsg(bool isBlock)
    {
      this.m_client.BlockProcessMsg(isBlock);
    }
  }
}
