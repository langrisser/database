﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ThearchyTrialComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class ThearchyTrialComponent : ThearchyTrialCompomentCommon
  {
    [DoNotToLua]
    private ThearchyTrialComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSThearchyTrialNtf_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_GetCurrentTicketNums_hotfix;
    private LuaFunction m_FinishedThearchyTrialLevelInt32BooleanList`1_hotfix;
    private LuaFunction m_SetSuccessThearchyTrialLevelConfigDataThearchyTrialLevelInfoList`1List`1Boolean_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ThearchyTrialComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSThearchyTrialNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentTicketNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedThearchyTrialLevel(int levelId, bool isWin, List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessThearchyTrialLevel(
      ConfigDataThearchyTrialLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      bool isTeamBattle = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public ThearchyTrialComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private int __callBase_AttackThearchyTrialLevel(int levelId)
    {
      return this.AttackThearchyTrialLevel(levelId);
    }

    private bool __callBase_IsLevelFinished(int levelId)
    {
      return this.IsLevelFinished(levelId);
    }

    private int __callBase_CanAttackThearchyTrialLevel(int levelId, bool isTeamBattle)
    {
      return this.CanAttackThearchyTrialLevel(levelId, isTeamBattle);
    }

    private int __callBase_CheckThearchyTrialOpened(int thearchyTrialId)
    {
      return this.CheckThearchyTrialOpened(thearchyTrialId);
    }

    private int __callBase_CanOpenLevel(int levelId)
    {
      return this.CanOpenLevel(levelId);
    }

    private int __callBase_CanAttackLevelByEnergyAndSoOn(
      ConfigDataThearchyTrialLevelInfo levelInfo,
      bool isTeamBattle)
    {
      return this.CanAttackLevelByEnergyAndSoOn(levelInfo, isTeamBattle);
    }

    private void __callBase_SetCommonSuccessThearchyTrialLevel(
      ConfigDataThearchyTrialLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      int energyCost,
      bool isBattleTeam)
    {
      this.SetCommonSuccessThearchyTrialLevel(levelInfo, battleTreasures, heroes, energyCost, isBattleTeam);
    }

    private HashSet<int> __callBase_GetAllFinishedLevels()
    {
      return this.GetAllFinishedLevels();
    }

    private bool __callBase_IsBlessing(int levelId)
    {
      return this.IsBlessing(levelId);
    }

    private bool __callBase_IsDailyChallenge()
    {
      return this.IsDailyChallenge();
    }

    private int __callBase_GetDailyChallengNums()
    {
      return this.GetDailyChallengNums();
    }

    private void __callBase_AddChallengedNums(int nums)
    {
      this.AddChallengedNums(nums);
    }

    private List<int> __callBase_GetAllUnlockedLevels()
    {
      return this.GetAllUnlockedLevels();
    }

    private bool __callBase_IsLevelUnlocked(int levelId)
    {
      return this.IsLevelUnlocked(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ThearchyTrialComponent m_owner;

      public LuaExportHelper(ThearchyTrialComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public int __callBase_AttackThearchyTrialLevel(int levelId)
      {
        return this.m_owner.__callBase_AttackThearchyTrialLevel(levelId);
      }

      public bool __callBase_IsLevelFinished(int levelId)
      {
        return this.m_owner.__callBase_IsLevelFinished(levelId);
      }

      public int __callBase_CanAttackThearchyTrialLevel(int levelId, bool isTeamBattle)
      {
        return this.m_owner.__callBase_CanAttackThearchyTrialLevel(levelId, isTeamBattle);
      }

      public int __callBase_CheckThearchyTrialOpened(int thearchyTrialId)
      {
        return this.m_owner.__callBase_CheckThearchyTrialOpened(thearchyTrialId);
      }

      public int __callBase_CanOpenLevel(int levelId)
      {
        return this.m_owner.__callBase_CanOpenLevel(levelId);
      }

      public int __callBase_CanAttackLevelByEnergyAndSoOn(
        ConfigDataThearchyTrialLevelInfo levelInfo,
        bool isTeamBattle)
      {
        return this.m_owner.__callBase_CanAttackLevelByEnergyAndSoOn(levelInfo, isTeamBattle);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_SetCommonSuccessThearchyTrialLevel(
        ConfigDataThearchyTrialLevelInfo levelInfo,
        List<int> battleTreasures,
        List<int> heroes,
        int energyCost,
        bool isBattleTeam)
      {
      }

      public HashSet<int> __callBase_GetAllFinishedLevels()
      {
        return this.m_owner.__callBase_GetAllFinishedLevels();
      }

      public bool __callBase_IsBlessing(int levelId)
      {
        return this.m_owner.__callBase_IsBlessing(levelId);
      }

      public bool __callBase_IsDailyChallenge()
      {
        return this.m_owner.__callBase_IsDailyChallenge();
      }

      public int __callBase_GetDailyChallengNums()
      {
        return this.m_owner.__callBase_GetDailyChallengNums();
      }

      public void __callBase_AddChallengedNums(int nums)
      {
        this.m_owner.__callBase_AddChallengedNums(nums);
      }

      public List<int> __callBase_GetAllUnlockedLevels()
      {
        return this.m_owner.__callBase_GetAllUnlockedLevels();
      }

      public bool __callBase_IsLevelUnlocked(int levelId)
      {
        return this.m_owner.__callBase_IsLevelUnlocked(levelId);
      }
    }
  }
}
