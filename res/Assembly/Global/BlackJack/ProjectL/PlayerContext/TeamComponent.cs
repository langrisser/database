﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.TeamComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class TeamComponent : TeamComponentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSTeamNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateTeam(TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AutoMatchTeamRoom(
      GameFunctionType gameFunctionTypeId,
      int locationId,
      TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeTeamRoomAuthority(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinTeamRoom(TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void QuitTeamRoom()
    {
      this.Room = (TeamRoom) null;
      base.QuitTeamRoom();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerJoinTeamRoom(TeamRoomPlayer player)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerQuitTeamRoom(
      TeamRoomPlayer quitPlayer,
      ulong leaderSessionId,
      long leaderKickOutTime)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LeaveWaitingListAndJoinRoom(TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeductTeamPveBattleEnergyByClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public TeamRoom Room { get; set; }

    public bool IsTeamRoomInviteAgain { get; set; }
  }
}
