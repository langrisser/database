﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ChatGroupComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class ChatGroupComponent : ChatGroupComponentCommon
  {
    public DateTime m_dataLastUpdateTime;
    private Dictionary<string, ChatGroupCompactInfo> m_currChatGroupDict;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected ChatComponent m_chatComponent;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatGroupComponent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatGroupCompactInfo GetChatGroupSimpleInfo(string groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NotifyChatGroupInfo(
      List<ProChatGroupCompactInfo> chatGroupInfoList,
      DateTime nowServerTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChatGroupStateChangedNtf(ProChatGroupInfo groupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime DataLastUpdateTime
    {
      get
      {
        return this.m_dataLastUpdateTime;
      }
    }
  }
}
