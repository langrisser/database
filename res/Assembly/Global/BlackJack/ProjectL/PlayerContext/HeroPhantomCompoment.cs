﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.HeroPhantomCompoment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class HeroPhantomCompoment : HeroPhantomCompomentCommon
  {
    [DoNotToLua]
    private HeroPhantomCompoment.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSHeroPhantomNtf_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_SetSuccessHeroPhantomLevelHeroPhantomLevelList`1List`1List`1_hotfix;
    private LuaFunction m_FinishedHeroPhantomLevelHeroPhantomLevelBooleanList`1List`1List`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomCompoment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSHeroPhantomNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessHeroPhantomLevel(
      HeroPhantomLevel Level,
      List<int> Heroes,
      List<int> BattleTreasures,
      List<int> newAchievementIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedHeroPhantomLevel(
      HeroPhantomLevel Level,
      bool IsWin,
      List<int> Heroes,
      List<int> BattleTreasures,
      List<int> achievementIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public HeroPhantomCompoment.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private bool __callBase_CheckHeroPhantomAvailable(int HeroPhantomId, ref int Err)
    {
      return this.CheckHeroPhantomAvailable(HeroPhantomId, ref Err);
    }

    private bool __callBase_CheckHeroPhantomLevelAvailable(int LevelId, ref int Err)
    {
      return this.CheckHeroPhantomLevelAvailable(LevelId, ref Err);
    }

    private bool __callBase_CheckPlayerOutOfBattle(ref int Err)
    {
      return this.CheckPlayerOutOfBattle(ref Err);
    }

    private bool __callBase_CheckEnergy(int LevelId, ref int Err)
    {
      return this.CheckEnergy(LevelId, ref Err);
    }

    private bool __callBase_CheckBag(int LevelId, ref int Err)
    {
      return this.CheckBag(LevelId, ref Err);
    }

    private int __callBase_CheckChallengeHeroPhantomLevel(int LevelId)
    {
      return this.CheckChallengeHeroPhantomLevel(LevelId);
    }

    private int __callBase_ChallengeHeroPhantomLevel(int LevelId, bool NoCheck)
    {
      return this.ChallengeHeroPhantomLevel(LevelId, NoCheck);
    }

    private void __callBase_SetCommonSuccessHeroPhantomLevel(
      HeroPhantomLevel Level,
      List<int> Heroes,
      List<int> BattleTreasures,
      List<int> newAchievementIds)
    {
      this.SetCommonSuccessHeroPhantomLevel(Level, Heroes, BattleTreasures, newAchievementIds);
    }

    private void __callBase_FinishedHeroPhantomLevel(HeroPhantomLevel Level, List<int> heroes)
    {
      this.FinishedHeroPhantomLevel(Level, heroes);
    }

    private HeroPhantom __callBase_GetHeroPhantom(int HeroPhantomId)
    {
      return this.GetHeroPhantom(HeroPhantomId);
    }

    private HeroPhantomLevel __callBase_GetHeroPhantomLevel(int LevelId)
    {
      return this.GetHeroPhantomLevel(LevelId);
    }

    private bool __callBase_IsCompleteHeroPhantomLevelAchievement(int achievementRelatedInfoID)
    {
      return this.IsCompleteHeroPhantomLevelAchievement(achievementRelatedInfoID);
    }

    private List<int> __callBase_GetAllFinishedLevels()
    {
      return this.GetAllFinishedLevels();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroPhantomCompoment m_owner;

      public LuaExportHelper(HeroPhantomCompoment owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public bool __callBase_CheckHeroPhantomAvailable(int HeroPhantomId, ref int Err)
      {
        return this.m_owner.__callBase_CheckHeroPhantomAvailable(HeroPhantomId, ref Err);
      }

      public bool __callBase_CheckHeroPhantomLevelAvailable(int LevelId, ref int Err)
      {
        return this.m_owner.__callBase_CheckHeroPhantomLevelAvailable(LevelId, ref Err);
      }

      public bool __callBase_CheckPlayerOutOfBattle(ref int Err)
      {
        return this.m_owner.__callBase_CheckPlayerOutOfBattle(ref Err);
      }

      public bool __callBase_CheckEnergy(int LevelId, ref int Err)
      {
        return this.m_owner.__callBase_CheckEnergy(LevelId, ref Err);
      }

      public bool __callBase_CheckBag(int LevelId, ref int Err)
      {
        return this.m_owner.__callBase_CheckBag(LevelId, ref Err);
      }

      public int __callBase_CheckChallengeHeroPhantomLevel(int LevelId)
      {
        return this.m_owner.__callBase_CheckChallengeHeroPhantomLevel(LevelId);
      }

      public int __callBase_ChallengeHeroPhantomLevel(int LevelId, bool NoCheck)
      {
        return this.m_owner.__callBase_ChallengeHeroPhantomLevel(LevelId, NoCheck);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_SetCommonSuccessHeroPhantomLevel(
        HeroPhantomLevel Level,
        List<int> Heroes,
        List<int> BattleTreasures,
        List<int> newAchievementIds)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_FinishedHeroPhantomLevel(HeroPhantomLevel Level, List<int> heroes)
      {
        this.m_owner.__callBase_FinishedHeroPhantomLevel(Level, heroes);
      }

      public HeroPhantom __callBase_GetHeroPhantom(int HeroPhantomId)
      {
        return this.m_owner.__callBase_GetHeroPhantom(HeroPhantomId);
      }

      public HeroPhantomLevel __callBase_GetHeroPhantomLevel(int LevelId)
      {
        return this.m_owner.__callBase_GetHeroPhantomLevel(LevelId);
      }

      public bool __callBase_IsCompleteHeroPhantomLevelAchievement(int achievementRelatedInfoID)
      {
        return this.m_owner.__callBase_IsCompleteHeroPhantomLevelAchievement(achievementRelatedInfoID);
      }

      public List<int> __callBase_GetAllFinishedLevels()
      {
        return this.m_owner.__callBase_GetAllFinishedLevels();
      }
    }
  }
}
