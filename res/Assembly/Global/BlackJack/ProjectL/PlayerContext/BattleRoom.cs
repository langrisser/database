﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.BattleRoom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class BattleRoom
  {
    public ulong RoomId;
    public BattleRoomType BattleRoomType;
    public int BattleId;
    public GameFunctionType GameFunctionType;
    public int LocationId;
    public BattleRoomStatus BattleRoomStatus;
    public DateTime ReadyTimeout;
    public DateTime LastPlayerBeginActionTime;
    public List<BattleRoomPlayer> Players;
    public BattleRoomPlayerHeroSetup BattleRoomPlayerHeroSetup;
    public int MyPlayerIndex;
    public int LeaderPlayerIndex;
    public List<BattleCommand> BattleCommands;
    public int BattleStars;
    public BattleReward BattleReward;
    public ulong PVPWinSessionId;
    public BattleMode BattleMode;
    public BattleRoomBPRule BPRule;
    public int BPTurn;
    public BattleRoomBPStatus BPStatus;
    public DateTime LatestTurnChangeDateTime;
    public ulong RealtimePVPBattleInstanceId;
    public List<int> PreferredHeroTagIds;
    public ulong GuildMassiveCombatInstanceId;
    [DoNotToLua]
    private BattleRoom.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Clear_hotfix;
    private LuaFunction m_InitPlayersList`1_hotfix;
    private LuaFunction m_FindPlayerBySessionIdUInt64_hotfix;
    private LuaFunction m_FindPlayerByUserIdString_hotfix;
    private LuaFunction m_SetMyPlayerString_hotfix;
    private LuaFunction m_SetLeaderPlayerString_hotfix;
    private LuaFunction m_FindPlayerIndexString_hotfix;
    private LuaFunction m_FindPlayerIndexBySessiongIdUInt64_hotfix;
    private LuaFunction m_GetMyPlayer_hotfix;
    private LuaFunction m_IsTeamOrGuildMassiveCombatRoomType_hotfix;
    private LuaFunction m_IsAnyPVPBattleRoomType_hotfix;
    private LuaFunction m_IsRealTimePVPBattleRoomType_hotfix;
    private LuaFunction m_GetPlayerIndexInCurrentPickTurn_hotfix;
    private LuaFunction m_GetHeroSetupCountMaxInPickTurnInt32_hotfix;
    private LuaFunction m_GetHeroSetupCountInCurrentPickTurn_hotfix;
    private LuaFunction m_GetRemainHeroSetupCountInCurrentPickTurn_hotfix;
    private LuaFunction m_GetHeroSetupFlagCountInt32SetupBattleHeroFlag_hotfix;
    private LuaFunction m_IsHeroSetupHasFlagInt32Int32SetupBattleHeroFlag_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPlayers(List<ProBattleRoomPlayer> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer FindPlayerBySessionId(ulong sessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer FindPlayerByUserId(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMyPlayer(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLeaderPlayer(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindPlayerIndex(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindPlayerIndexBySessiongId(ulong sessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer GetMyPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTeamOrGuildMassiveCombatRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyPVPBattleRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRealTimePVPBattleRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerIndexInCurrentPickTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroSetupCountMaxInPickTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroSetupCountInCurrentPickTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRemainHeroSetupCountInCurrentPickTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroSetupFlagCount(int playerIndex, SetupBattleHeroFlag flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroSetupHasFlag(int playerIndex, int heroId, SetupBattleHeroFlag flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleRoom.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleRoom m_owner;

      public LuaExportHelper(BattleRoom owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
