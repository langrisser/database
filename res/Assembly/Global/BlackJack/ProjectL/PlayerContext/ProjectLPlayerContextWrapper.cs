﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ProjectLPlayerContextWrapper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.PlayerContext;
using BlackJack.ProjectL.LibClient;
using SLua;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class ProjectLPlayerContextWrapper : IClientEventHandler
  {
    private IPlayerContextNetworkEventHandler m_playerContext;

    public ProjectLPlayerContextWrapper(IPlayerContextNetworkEventHandler playerContext)
    {
      this.m_playerContext = playerContext;
    }

    public void OnConnected()
    {
      this.m_playerContext.OnGameServerConnected();
    }

    public void OnDisconnected()
    {
      this.m_playerContext.OnGameServerDisconnected();
    }

    public void OnError(int err, string excepionInfo = null)
    {
      this.m_playerContext.OnGameServerError(err, excepionInfo);
    }

    public void OnLoginByAuthTokenAck(int result, bool needRedirect, string sessionToken)
    {
      this.m_playerContext.OnLoginByAuthTokenAck(result, needRedirect, sessionToken);
    }

    public void OnLoginBySessionTokenAck(int result)
    {
      this.m_playerContext.OnLoginBySessionTokenAck(result);
    }

    public void OnMessage(object msg, int msgId)
    {
      this.m_playerContext.OnGameServerMessage(msg, msgId);
    }
  }
}
