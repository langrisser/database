﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ArenaComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class ArenaComponent : ArenaComponentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    public override void Tick(uint deltaMillisecond)
    {
      base.Tick(deltaMillisecond);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetArenaTicketNextGivenTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSArenaNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    public ushort GetDSVersion()
    {
      return this.m_arenaDS.ClientVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddArenaBattleReportPlayBackData(ProArenaBattleReport pbArenaBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartArenaBattle(ProArenaDefensiveBattleInfo pbBattleInfo, bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReconnectArenaBattle(ProArenaDefensiveBattleInfo pbBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RevengeOpponent(
      ulong battleReportInstanceId,
      bool autoBattle,
      ProArenaDefensiveBattleInfo pbBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartArenaBattle(ArenaOpponentDefensiveBattleInfo battleInfo)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefensiveTeam(ProArenaPlayerDefensiveTeam pbDefensiveTeamInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinsihArenaBattle(bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FlushOpponents(List<ProArenaOpponent> pbOpponents, long nextFlushTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitArenaPlayInfo(ProArenaPlayerInfo pbArenaPlayerInfo)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBattleReportBasicInfo(
      List<ProArenaBattleReport> pbArenaBattleReports,
      int nextBattleReportIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    public void GainVictoryPointsReward(int victoryPointsRewardIndex)
    {
      this.AddReceivedVictoryPointsRewardIndex(victoryPointsRewardIndex);
    }

    public ArenaPlayerInfo GetArenaPlayerInfo()
    {
      return this.m_arenaDS.ArenaPlayerInfo;
    }

    public ArenaBattleReport GetArenaBattleReport(ulong instanceId)
    {
      return this.m_arenaBattleReportDS.FindArenaBattleReportByInstanceId(instanceId);
    }
  }
}
