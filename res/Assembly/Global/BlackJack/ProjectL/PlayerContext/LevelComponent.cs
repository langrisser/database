﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.LevelComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class LevelComponent : LevelComponentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    public override void Tick(uint deltaMillisecond)
    {
      base.Tick(deltaMillisecond);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSLevelNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public ushort GetDSVersion()
    {
      return this.m_levelDS.ClientVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override int HandleDialogEvent(
      ConfigDataWaypointInfo wayPointInfo,
      List<Goods> itemRewards,
      int playerExpReward,
      int goldReward,
      int energyCost,
      int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int CanAttackScenario(
      ConfigDataScenarioInfo secenarioInfo,
      bool scenarioFinished,
      bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackScenario(ConfigDataScenarioInfo secenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackEventWayPoint(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override int HandleTresureEvent(int wayPointId, ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void SetBattleWayPointSuccessful(
      ConfigDataWaypointInfo wayPointInfo,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRegionOpen(int regionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WayPointStatus GetWaypointStatus(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataWaypointInfo GetPlayerCurrentWaypointInfo()
    {
      return this.m_levelDS.CurrentWaypointInfo;
    }

    public ConfigDataScenarioInfo GetLastFinishedScenarioInfo()
    {
      return this.m_levelDS.LastFinishedScenarioInfo;
    }

    public override void SetFinishedScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      base.SetFinishedScenario(scenarioInfo);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishBattleWayPoint(
      int wayPointId,
      bool isWin,
      int result,
      List<int> battleTreasureIds)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
