﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PVPInviteInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using SLua;
using System;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class PVPInviteInfo
  {
    public UserSummary Inviter;
    public DateTime Timeout;
  }
}
