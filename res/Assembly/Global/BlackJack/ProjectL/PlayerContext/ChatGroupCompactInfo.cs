﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ChatGroupCompactInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class ChatGroupCompactInfo
  {
    public string ChatGroupId;
    public string ChatGroupName;
    public ChatUserCompactInfo Owner;
    public int UserCount;
    public int OnlineUserCount;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatGroupCompactInfo(ProChatGroupCompactInfo pbGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
