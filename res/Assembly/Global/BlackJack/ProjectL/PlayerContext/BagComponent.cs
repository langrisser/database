﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.BagComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class BagComponent : BagComponentCommon
  {
    [DoNotToLua]
    private BagComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSBagNtf_hotfix;
    private LuaFunction m_DeSerializeDSBagExtraNtf_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_ChangeBagItemProGoods_hotfix;
    private LuaFunction m_IsExistBagItemIgnoreInstanceDifferenceGoodsTypeInt32_hotfix;
    private LuaFunction m_GetBagItemNumsGoodsTypeInt32_hotfix;
    private LuaFunction m_GetInstanceBagItemInstanceIdsByContentIdInt32_hotfix;
    private LuaFunction m_AddInstanceBagItemDirectlyProGoods_hotfix;
    private LuaFunction m_AddTypeBagItemDirectlyGoodsTypeInt32Int32_hotfix;
    private LuaFunction m_DecomposeBagItemsList`1_hotfix;
    private LuaFunction m_EnchantEquipmentUInt64UInt64_hotfix;
    private LuaFunction m_GetEquipmentResonanceNumsUInt64_hotfix;
    private LuaFunction m_GetEquipmentResonanceNumsByHeroIdUInt64Int32_hotfix;
    private LuaFunction m_CanUseEnergyMedicineInt32Int32_hotfix;
    private LuaFunction m_HasOwnGoodsTypeInt32_hotfix;
    private LuaFunction m_GetEquipmentResonanceNumsByHeroUInt64Hero_hotfix;
    private LuaFunction m_HandleGetRewardListList`1Action`2_hotfix;
    private LuaFunction m_HandleBoxOpenNetTaskGoodsTypeInt32Int32Action`1Action`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BagComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSBagNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSBagExtraNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeBagItem(ProGoods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistBagItemIgnoreInstanceDifference(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBagItemNums(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ulong> GetInstanceBagItemInstanceIdsByContentId(int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase AddInstanceBagItemDirectly(ProGoods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase AddTypeBagItemDirectly(
      GoodsType goodsTypeId,
      int contentId,
      int currentNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int DecomposeBagItems(List<ProGoods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int EnchantEquipment(ulong equipmentInstanceId, ulong enchantStoneInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEquipmentResonanceNums(ulong equipmentInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEquipmentResonanceNumsByHeroId(ulong equipmentInstanceId, int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUseEnergyMedicine(int itemId, int useCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOwn(GoodsType goodsType, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetEquipmentResonanceNumsByHero(ulong equipmentInstanceId, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HandleGetRewardList(
      List<Goods> rewardList,
      Action<List<Goods>, List<int>> actionOnResult)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleBoxOpenNetTask(
      GoodsType type,
      int id,
      int count,
      Action<List<Goods>> successedCallback,
      Action<int> failedCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BagComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private int __callBase_IsGoodsEnough(
      List<Goods> conditions,
      out List<BagItemBase> BagItemsInBag)
    {
      return this.IsGoodsEnough(conditions, out BagItemsInBag);
    }

    private void __callBase_ConsumeGoods(
      List<Goods> needToConsumeGoods,
      List<BagItemBase> BagItemsInBag,
      List<BagItemBase> changedGoods,
      GameFunctionType causeId,
      string location)
    {
      this.ConsumeGoods(needToConsumeGoods, BagItemsInBag, changedGoods, causeId, location);
    }

    private bool __callBase_IsBagFullByRandomGoods(
      int addRandomRewardExpectSize,
      List<Goods> addGoods,
      List<Goods> deleteGoods)
    {
      return this.IsBagFullByRandomGoods(addRandomRewardExpectSize, addGoods, deleteGoods);
    }

    private bool __callBase_IsBagFullByGoods(List<Goods> addGoods, List<Goods> deleteGoods)
    {
      return this.IsBagFullByGoods(addGoods, deleteGoods);
    }

    private bool __callBase_IsBagFull(int expectSize)
    {
      return this.IsBagFull(expectSize);
    }

    private int __callBase_GetBagSize()
    {
      return this.GetBagSize();
    }

    private bool __callBase_IsBagFullByCurrentSize()
    {
      return this.IsBagFullByCurrentSize();
    }

    private List<Goods> __callBase_TransformHeroGoods(int heroId, int nums)
    {
      return this.TransformHeroGoods(heroId, nums);
    }

    private int __callBase_IsBagItemContentIdInConfig(GoodsType goodsTypeId, int contentId)
    {
      return this.IsBagItemContentIdInConfig(goodsTypeId, contentId);
    }

    private void __callBase_RemoveAllBagItems(
      List<BagItemBase> changedBagItems,
      int removeItemMaxNums)
    {
      this.RemoveAllBagItems(changedBagItems, removeItemMaxNums);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int __callBase_RemoveBagItem(
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums,
      ulong instanceId,
      GameFunctionType caseId,
      string location)
    {
      // ISSUE: unable to decompile the method.
    }

    private int __callBase_RemoveBagItemByType(
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums,
      GameFunctionType caseId,
      string location)
    {
      return this.RemoveBagItemByType(goodsTypeId, contentId, consumeNums, caseId, location);
    }

    private int __callBase_RemoveBagItemByInstanceId(
      ulong instanceId,
      GameFunctionType caseId,
      string location)
    {
      return this.RemoveBagItemByInstanceId(instanceId, caseId, location);
    }

    private BagItemBase __callBase_RemoveBagItemDirectly(
      BagItemBase bagItem,
      int consumeNums,
      GameFunctionType caseId,
      string location)
    {
      return this.RemoveBagItemDirectly(bagItem, consumeNums, caseId, location);
    }

    private BagItemBase __callBase_FindBagItem(
      GoodsType goodsTypeId,
      int contentId,
      ulong instanceId)
    {
      return this.FindBagItem(goodsTypeId, contentId, instanceId);
    }

    private BagItemBase __callBase_FindBagItemByInstanceId(ulong instanceId)
    {
      return this.FindBagItemByInstanceId(instanceId);
    }

    private BagItemBase __callBase_FindBagItemByType(
      GoodsType goodsTypeId,
      int contentId)
    {
      return this.FindBagItemByType(goodsTypeId, contentId);
    }

    private UseableBagItem __callBase_FindUseableBagItem(
      GoodsType goodsTypeId,
      int contentId)
    {
      return this.FindUseableBagItem(goodsTypeId, contentId);
    }

    private List<BagItemBase> __callBase_GetAllBagItems()
    {
      return this.GetAllBagItems();
    }

    private IEnumerable<BagItemBase> __callBase_IterateAllBagItems()
    {
      return this.IterateAllBagItems();
    }

    private List<BagItemBase> __callBase_FindEnoughBagItems(List<Goods> conditions)
    {
      return this.FindEnoughBagItems(conditions);
    }

    private int __callBase_UseBagItem(
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums,
      object[] param)
    {
      return this.UseBagItem(goodsTypeId, contentId, consumeNums, param);
    }

    private int __callBase_UseBagItem(
      UseableBagItem useableBagItem,
      int consumeNums,
      object[] param)
    {
      return this.UseBagItem(useableBagItem, consumeNums, param);
    }

    private int __callBase_UseBagItemDirectly(
      UseableBagItem useableBagItem,
      int consumeNums,
      object[] param)
    {
      return this.UseBagItemDirectly(useableBagItem, consumeNums, param);
    }

    private int __callBase_OpenSelfSelectedBox(
      int selectedIndex,
      GoodsType goodsTypeId,
      int contentId,
      int nums)
    {
      return this.OpenSelfSelectedBox(selectedIndex, goodsTypeId, contentId, nums);
    }

    private int __callBase_OpenHeroSkinSelfSelectedBox(
      List<int> selectedIndexes,
      GoodsType goodsTypeId,
      int contentId,
      int nums)
    {
      return this.OpenHeroSkinSelfSelectedBox(selectedIndexes, goodsTypeId, contentId, nums);
    }

    private int __callBase_HasEnoughBagItem(BagItemBase bagItem, int consumeNums)
    {
      return this.HasEnoughBagItem(bagItem, consumeNums);
    }

    private bool __callBase_IsBagItemEnough(BagItemBase bagItem, int consumeNums)
    {
      return this.IsBagItemEnough(bagItem, consumeNums);
    }

    private bool __callBase_IsBagItemEnough(GoodsType bagItemType, int bagItemId, int nums)
    {
      return this.IsBagItemEnough(bagItemType, bagItemId, nums);
    }

    private int __callBase_GetTicketId(GameFunctionType causeId)
    {
      return this.GetTicketId(causeId);
    }

    private bool __callBase_IsLevelTicketsMaxByUI(
      GameFunctionType causeId,
      GoodsType goodTypeId,
      int contentId,
      int nums)
    {
      return this.IsLevelTicketsMaxByUI(causeId, goodTypeId, contentId, nums);
    }

    private int __callBase_SellBagItem(ulong instanceId, int nums)
    {
      return this.SellBagItem(instanceId, nums);
    }

    private void __callBase_CombineSameGoodsAndReplaceExistHeroToFragment(List<Goods> goodsList)
    {
      this.CombineSameGoodsAndReplaceExistHeroToFragment(goodsList);
    }

    private int __callBase_CanDecomposeBagItems(List<ProGoods> goods)
    {
      return this.CanDecomposeBagItems(goods);
    }

    private int __callBase_CanDecomposeABagItem(
      GoodsType goodsTypeId,
      int contentId,
      int nums,
      ulong instanceId)
    {
      return this.CanDecomposeABagItem(goodsTypeId, contentId, nums, instanceId);
    }

    private void __callBase_SetBagItemDirty(BagItemBase bagItem)
    {
      this.SetBagItemDirty(bagItem);
    }

    private bool __callBase_IsPercentBaseBattleProperty(PropertyModifyType id)
    {
      return this.IsPercentBaseBattleProperty(id);
    }

    private int __callBase_CanLockAndUnLockEquipment(ulong instanceId)
    {
      return this.CanLockAndUnLockEquipment(instanceId);
    }

    private int __callBase_LockAndUnLockEquipment(ulong instanceId)
    {
      return this.LockAndUnLockEquipment(instanceId);
    }

    private int __callBase_CanEnhanceEquipment(ulong instanceId, List<ulong> materialIds)
    {
      return this.CanEnhanceEquipment(instanceId, materialIds);
    }

    private bool __callBase_CanWearEquipmentByEquipmentType(BagItemBase equipment)
    {
      return this.CanWearEquipmentByEquipmentType(equipment);
    }

    private int __callBase_CalculateEnhanceEquipmentExp(List<BagItemBase> materials)
    {
      return this.CalculateEnhanceEquipmentExp(materials);
    }

    private int __callBase_CalculateEnhanceEquipmentGold(int exp)
    {
      return this.CalculateEnhanceEquipmentGold(exp);
    }

    private int __callBase_EnhanceEquipment(ulong instanceId, List<ulong> materialIds)
    {
      return this.EnhanceEquipment(instanceId, materialIds);
    }

    private int __callBase_CanEnchantEquipment(
      ulong equipmentInstanceId,
      ulong enchantStoneInstanceId)
    {
      return this.CanEnchantEquipment(equipmentInstanceId, enchantStoneInstanceId);
    }

    private void __callBase_OutPutEqipmentEnhanceOperateLog(
      EquipmentBagItem equipment,
      int preLevel,
      int preExp,
      List<Goods> costItems)
    {
      this.OutPutEqipmentEnhanceOperateLog(equipment, preLevel, preExp, costItems);
    }

    private void __callBase_OutPutEquipmentUpgrageOperateLog(
      EquipmentBagItem equipment,
      EquipmentBagItem material,
      int preLvlLimit,
      int postLvlLimit,
      List<Goods> costItems)
    {
      this.OutPutEquipmentUpgrageOperateLog(equipment, material, preLvlLimit, postLvlLimit, costItems);
    }

    private void __callBase_LevelUpEquipment(EquipmentBagItem equipment)
    {
      this.LevelUpEquipment(equipment);
    }

    private int __callBase_CaculateEquipmentNextLevelExp(int equipmentContentId, int equipmentLevel)
    {
      return this.CaculateEquipmentNextLevelExp(equipmentContentId, equipmentLevel);
    }

    private int __callBase_CanLevelUpEquipmentStar(ulong instanceId, ulong materialId)
    {
      return this.CanLevelUpEquipmentStar(instanceId, materialId);
    }

    private int __callBase_CalculateLevelUpEquipmentStarGold(EquipmentBagItem equipment)
    {
      return this.CalculateLevelUpEquipmentStarGold(equipment);
    }

    private int __callBase_LevelUpEquipmentStar(ulong instanceId, ulong materialId)
    {
      return this.LevelUpEquipmentStar(instanceId, materialId);
    }

    private int __callBase_CalculateEquipmentTotalExp(EquipmentBagItem equipment)
    {
      return this.CalculateEquipmentTotalExp(equipment);
    }

    private int __callBase_CalculateDecomposeEquipmentBackGold(EquipmentBagItem equipment)
    {
      return this.CalculateDecomposeEquipmentBackGold(equipment);
    }

    private void __callBase_OutPutItemOperationLog(
      GoodsType itemTypeId,
      int itemId,
      int nums,
      GameFunctionType causeId,
      string location)
    {
      this.OutPutItemOperationLog(itemTypeId, itemId, nums, causeId, location);
    }

    private void __callBase_OnSaveEnchantSaveMissionEvent(EquipmentBagItem equipment)
    {
      this.OnSaveEnchantSaveMissionEvent(equipment);
    }

    private void __callBase_OnEnchantMissionEvent(EquipmentBagItem equipment)
    {
      this.OnEnchantMissionEvent(equipment);
    }

    private void __callBase_OnCreateBagItemEvent(BagItemBase bagItem)
    {
      this.OnCreateBagItemEvent(bagItem);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BagComponent m_owner;

      public LuaExportHelper(BagComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public int __callBase_IsGoodsEnough(
        List<Goods> conditions,
        out List<BagItemBase> BagItemsInBag)
      {
        return this.m_owner.__callBase_IsGoodsEnough(conditions, out BagItemsInBag);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_ConsumeGoods(
        List<Goods> needToConsumeGoods,
        List<BagItemBase> BagItemsInBag,
        List<BagItemBase> changedGoods,
        GameFunctionType causeId,
        string location)
      {
      }

      public bool __callBase_IsBagFullByRandomGoods(
        int addRandomRewardExpectSize,
        List<Goods> addGoods,
        List<Goods> deleteGoods)
      {
        return this.m_owner.__callBase_IsBagFullByRandomGoods(addRandomRewardExpectSize, addGoods, deleteGoods);
      }

      public bool __callBase_IsBagFullByGoods(List<Goods> addGoods, List<Goods> deleteGoods)
      {
        return this.m_owner.__callBase_IsBagFullByGoods(addGoods, deleteGoods);
      }

      public bool __callBase_IsBagFull(int expectSize)
      {
        return this.m_owner.__callBase_IsBagFull(expectSize);
      }

      public int __callBase_GetBagSize()
      {
        return this.m_owner.__callBase_GetBagSize();
      }

      public bool __callBase_IsBagFullByCurrentSize()
      {
        return this.m_owner.__callBase_IsBagFullByCurrentSize();
      }

      public List<Goods> __callBase_TransformHeroGoods(int heroId, int nums)
      {
        return this.m_owner.__callBase_TransformHeroGoods(heroId, nums);
      }

      public int __callBase_IsBagItemContentIdInConfig(GoodsType goodsTypeId, int contentId)
      {
        return this.m_owner.__callBase_IsBagItemContentIdInConfig(goodsTypeId, contentId);
      }

      public void __callBase_RemoveAllBagItems(
        List<BagItemBase> changedBagItems,
        int removeItemMaxNums)
      {
        this.m_owner.__callBase_RemoveAllBagItems(changedBagItems, removeItemMaxNums);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int __callBase_RemoveBagItem(
        GoodsType goodsTypeId,
        int contentId,
        int consumeNums,
        ulong instanceId,
        GameFunctionType caseId,
        string location)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int __callBase_RemoveBagItemByType(
        GoodsType goodsTypeId,
        int contentId,
        int consumeNums,
        GameFunctionType caseId,
        string location)
      {
        // ISSUE: unable to decompile the method.
      }

      public int __callBase_RemoveBagItemByInstanceId(
        ulong instanceId,
        GameFunctionType caseId,
        string location)
      {
        return this.m_owner.__callBase_RemoveBagItemByInstanceId(instanceId, caseId, location);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public BagItemBase __callBase_RemoveBagItemDirectly(
        BagItemBase bagItem,
        int consumeNums,
        GameFunctionType caseId,
        string location)
      {
        // ISSUE: unable to decompile the method.
      }

      public BagItemBase __callBase_FindBagItem(
        GoodsType goodsTypeId,
        int contentId,
        ulong instanceId)
      {
        return this.m_owner.__callBase_FindBagItem(goodsTypeId, contentId, instanceId);
      }

      public BagItemBase __callBase_FindBagItemByInstanceId(ulong instanceId)
      {
        return this.m_owner.__callBase_FindBagItemByInstanceId(instanceId);
      }

      public BagItemBase __callBase_FindBagItemByType(
        GoodsType goodsTypeId,
        int contentId)
      {
        return this.m_owner.__callBase_FindBagItemByType(goodsTypeId, contentId);
      }

      public UseableBagItem __callBase_FindUseableBagItem(
        GoodsType goodsTypeId,
        int contentId)
      {
        return this.m_owner.__callBase_FindUseableBagItem(goodsTypeId, contentId);
      }

      public List<BagItemBase> __callBase_GetAllBagItems()
      {
        return this.m_owner.__callBase_GetAllBagItems();
      }

      public IEnumerable<BagItemBase> __callBase_IterateAllBagItems()
      {
        return this.m_owner.__callBase_IterateAllBagItems();
      }

      public List<BagItemBase> __callBase_FindEnoughBagItems(List<Goods> conditions)
      {
        return this.m_owner.__callBase_FindEnoughBagItems(conditions);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int __callBase_UseBagItem(
        GoodsType goodsTypeId,
        int contentId,
        int consumeNums,
        object[] param)
      {
        // ISSUE: unable to decompile the method.
      }

      public int __callBase_UseBagItem(
        UseableBagItem useableBagItem,
        int consumeNums,
        object[] param)
      {
        return this.m_owner.__callBase_UseBagItem(useableBagItem, consumeNums, param);
      }

      public int __callBase_UseBagItemDirectly(
        UseableBagItem useableBagItem,
        int consumeNums,
        object[] param)
      {
        return this.m_owner.__callBase_UseBagItemDirectly(useableBagItem, consumeNums, param);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int __callBase_OpenSelfSelectedBox(
        int selectedIndex,
        GoodsType goodsTypeId,
        int contentId,
        int nums)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int __callBase_OpenHeroSkinSelfSelectedBox(
        List<int> selectedIndexes,
        GoodsType goodsTypeId,
        int contentId,
        int nums)
      {
        // ISSUE: unable to decompile the method.
      }

      public int __callBase_HasEnoughBagItem(BagItemBase bagItem, int consumeNums)
      {
        return this.m_owner.__callBase_HasEnoughBagItem(bagItem, consumeNums);
      }

      public bool __callBase_IsBagItemEnough(BagItemBase bagItem, int consumeNums)
      {
        return this.m_owner.__callBase_IsBagItemEnough(bagItem, consumeNums);
      }

      public bool __callBase_IsBagItemEnough(GoodsType bagItemType, int bagItemId, int nums)
      {
        return this.m_owner.__callBase_IsBagItemEnough(bagItemType, bagItemId, nums);
      }

      public int __callBase_GetTicketId(GameFunctionType causeId)
      {
        return this.m_owner.__callBase_GetTicketId(causeId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool __callBase_IsLevelTicketsMaxByUI(
        GameFunctionType causeId,
        GoodsType goodTypeId,
        int contentId,
        int nums)
      {
        // ISSUE: unable to decompile the method.
      }

      public int __callBase_SellBagItem(ulong instanceId, int nums)
      {
        return this.m_owner.__callBase_SellBagItem(instanceId, nums);
      }

      public void __callBase_CombineSameGoodsAndReplaceExistHeroToFragment(List<Goods> goodsList)
      {
        this.m_owner.__callBase_CombineSameGoodsAndReplaceExistHeroToFragment(goodsList);
      }

      public int __callBase_CanDecomposeBagItems(List<ProGoods> goods)
      {
        return this.m_owner.__callBase_CanDecomposeBagItems(goods);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int __callBase_CanDecomposeABagItem(
        GoodsType goodsTypeId,
        int contentId,
        int nums,
        ulong instanceId)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_SetBagItemDirty(BagItemBase bagItem)
      {
        this.m_owner.__callBase_SetBagItemDirty(bagItem);
      }

      public bool __callBase_IsPercentBaseBattleProperty(PropertyModifyType id)
      {
        return this.m_owner.__callBase_IsPercentBaseBattleProperty(id);
      }

      public int __callBase_CanLockAndUnLockEquipment(ulong instanceId)
      {
        return this.m_owner.__callBase_CanLockAndUnLockEquipment(instanceId);
      }

      public int __callBase_LockAndUnLockEquipment(ulong instanceId)
      {
        return this.m_owner.__callBase_LockAndUnLockEquipment(instanceId);
      }

      public int __callBase_CanEnhanceEquipment(ulong instanceId, List<ulong> materialIds)
      {
        return this.m_owner.__callBase_CanEnhanceEquipment(instanceId, materialIds);
      }

      public bool __callBase_CanWearEquipmentByEquipmentType(BagItemBase equipment)
      {
        return this.m_owner.__callBase_CanWearEquipmentByEquipmentType(equipment);
      }

      public int __callBase_CalculateEnhanceEquipmentExp(List<BagItemBase> materials)
      {
        return this.m_owner.__callBase_CalculateEnhanceEquipmentExp(materials);
      }

      public int __callBase_CalculateEnhanceEquipmentGold(int exp)
      {
        return this.m_owner.__callBase_CalculateEnhanceEquipmentGold(exp);
      }

      public int __callBase_EnhanceEquipment(ulong instanceId, List<ulong> materialIds)
      {
        return this.m_owner.__callBase_EnhanceEquipment(instanceId, materialIds);
      }

      public int __callBase_CanEnchantEquipment(
        ulong equipmentInstanceId,
        ulong enchantStoneInstanceId)
      {
        return this.m_owner.__callBase_CanEnchantEquipment(equipmentInstanceId, enchantStoneInstanceId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OutPutEqipmentEnhanceOperateLog(
        EquipmentBagItem equipment,
        int preLevel,
        int preExp,
        List<Goods> costItems)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OutPutEquipmentUpgrageOperateLog(
        EquipmentBagItem equipment,
        EquipmentBagItem material,
        int preLvlLimit,
        int postLvlLimit,
        List<Goods> costItems)
      {
      }

      public void __callBase_LevelUpEquipment(EquipmentBagItem equipment)
      {
        this.m_owner.__callBase_LevelUpEquipment(equipment);
      }

      public int __callBase_CaculateEquipmentNextLevelExp(
        int equipmentContentId,
        int equipmentLevel)
      {
        return this.m_owner.__callBase_CaculateEquipmentNextLevelExp(equipmentContentId, equipmentLevel);
      }

      public int __callBase_CanLevelUpEquipmentStar(ulong instanceId, ulong materialId)
      {
        return this.m_owner.__callBase_CanLevelUpEquipmentStar(instanceId, materialId);
      }

      public int __callBase_CalculateLevelUpEquipmentStarGold(EquipmentBagItem equipment)
      {
        return this.m_owner.__callBase_CalculateLevelUpEquipmentStarGold(equipment);
      }

      public int __callBase_LevelUpEquipmentStar(ulong instanceId, ulong materialId)
      {
        return this.m_owner.__callBase_LevelUpEquipmentStar(instanceId, materialId);
      }

      public int __callBase_CalculateEquipmentTotalExp(EquipmentBagItem equipment)
      {
        return this.m_owner.__callBase_CalculateEquipmentTotalExp(equipment);
      }

      public int __callBase_CalculateDecomposeEquipmentBackGold(EquipmentBagItem equipment)
      {
        return this.m_owner.__callBase_CalculateDecomposeEquipmentBackGold(equipment);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OutPutItemOperationLog(
        GoodsType itemTypeId,
        int itemId,
        int nums,
        GameFunctionType causeId,
        string location)
      {
      }

      public void __callBase_OnSaveEnchantSaveMissionEvent(EquipmentBagItem equipment)
      {
        this.m_owner.__callBase_OnSaveEnchantSaveMissionEvent(equipment);
      }

      public void __callBase_OnEnchantMissionEvent(EquipmentBagItem equipment)
      {
        this.m_owner.__callBase_OnEnchantMissionEvent(equipment);
      }

      public void __callBase_OnCreateBagItemEvent(BagItemBase bagItem)
      {
        this.m_owner.__callBase_OnCreateBagItemEvent(bagItem);
      }

      public int GetEquipmentResonanceNumsByHero(ulong equipmentInstanceId, Hero hero)
      {
        return this.m_owner.GetEquipmentResonanceNumsByHero(equipmentInstanceId, hero);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void HandleBoxOpenNetTask(
        GoodsType type,
        int id,
        int count,
        Action<List<Goods>> successedCallback,
        Action<int> failedCallback)
      {
      }
    }
  }
}
