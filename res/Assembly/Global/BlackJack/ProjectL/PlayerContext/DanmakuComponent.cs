﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.DanmakuComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class DanmakuComponent : DanmakuComponentCommon
  {
    private List<PostDanmakuEntry> m_postDanmakuEntries;
    private LevelDanmaku m_levelDanmaku;
    private List<int> m_newSendDanmakuTurnList;

    [MethodImpl((MethodImplOptions) 32768)]
    public DanmakuComponent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLevelDanmaku(LevelDanmaku levelDanmaku)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FilterSensitiveWords()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private LevelDanmaku BuildLevelDanmakuOrderByTime(LevelDanmaku source)
    {
      // ISSUE: unable to decompile the method.
    }

    public LevelDanmaku GetLevelDanmaku()
    {
      return this.m_levelDanmaku;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostLevelDanmaku(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPostDanmakuEntryToLocalLevelDanmaku(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddTurnDanmakuToLevelDanmaku(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private DanmakuEntry CreateDanmakuEntry(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearLevelDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<PostDanmakuEntry> GetPostedLevelDanmaku()
    {
      this.ClearNewSendDanmakuTurnList();
      return this.m_postDanmakuEntries;
    }

    public void ClearNewSendDanmakuTurnList()
    {
      this.m_newSendDanmakuTurnList.Clear();
    }
  }
}
