﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CollectionActivityComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class CollectionActivityComponent : CollectionComponentCommon
  {
    private List<ConfigDataCollectionActivityScenarioLevelInfo> m_tempScenarioLevelInfos;
    protected HeroComponentCommon m_hero;
    protected CollectionActivityExchangeModel m_exchangeModel;
    [DoNotToLua]
    private CollectionActivityComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSCollectionNtf_hotfix;
    private LuaFunction m_GetCollectionActivityList_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_IsCollectionActivityOpenedInt32_hotfix;
    private LuaFunction m_IsCollectionActivityDisplayInt32_hotfix;
    private LuaFunction m_IsCollectionActivityExchangeTimeInt32_hotfix;
    private LuaFunction m_GetCollectionActivityOpenTimeInt32DateTime_DateTime__hotfix;
    private LuaFunction m_GetCollectionActivityExchangeEndTimeInt32_hotfix;
    private LuaFunction m_FindActivityInstanceIdByCollectionActivityIdInt32_hotfix;
    private LuaFunction m_FindCollectionActivityByCollectionActivityIdInt32_hotfix;
    private LuaFunction m_GetNextScenarioLevelIdInt32_hotfix;
    private LuaFunction m_GetCurrentWaypointIdInt32_hotfix;
    private LuaFunction m_GetFinishedScenarioLevelInfosInt32List`1_hotfix;
    private LuaFunction m_GetWaypointStateInt32CollectionActivityWaypointStateType_String__hotfix;
    private LuaFunction m_GetPlayerGraphicResourceInt32_hotfix;
    private LuaFunction m_GetWaypointRedPointCountInt32_hotfix;
    private LuaFunction m_GetLevelUnlockDayInt32Int32_hotfix;
    private LuaFunction m_GetScenarioLevelUnlockDayInt32_hotfix;
    private LuaFunction m_GetChallengeLevelUnlockDayInt32_hotfix;
    private LuaFunction m_GetLootLevelUnlockDayInt32_hotfix;
    private LuaFunction m_IsScenarioLevelFinishedInt32_hotfix;
    private LuaFunction m_IsChallengeLevelFinishedInt32_hotfix;
    private LuaFunction m_IsLootLevelFinishedInt32_hotfix;
    private LuaFunction m_IsChallengePreLevelFinishedInt32_hotfix;
    private LuaFunction m_IsChallengeLevelPlayerLevelVaildInt32Int32__hotfix;
    private LuaFunction m_IsLootPreLevelFinishedInt32_hotfix;
    private LuaFunction m_IsLootLevelPlayerLevelVaildInt32Int32__hotfix;
    private LuaFunction m_FinishedLevelGameFunctionTypeInt32Boolean_hotfix;
    private LuaFunction m_SetSuccessLevelGameFunctionTypeInt32List`1Boolean_hotfix;
    private LuaFunction m_HandleScenarioInt32_hotfix;
    private LuaFunction m_GetLevelNameCollectionActivityLevelTypeInt32_hotfix;
    private LuaFunction m_GetPageLockMessageCollectionActivityPage_hotfix;
    private LuaFunction m_IsLevelFinishedLevelInfo_hotfix;
    private LuaFunction m_get_ExchangeModel_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSCollectionNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionActivity> GetCollectionActivityList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityOpened(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityDisplay(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityExchangeTime(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetCollectionActivityOpenTime(
      int collectionActivityId,
      out DateTime startTime,
      out DateTime endTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCollectionActivityExchangeEndTime(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong FindActivityInstanceIdByCollectionActivityId(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivity FindCollectionActivityByCollectionActivityId(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextScenarioLevelId(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentWaypointId(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetFinishedScenarioLevelInfos(
      int collectionActivityId,
      List<ConfigDataCollectionActivityScenarioLevelInfo> scenarioLevelInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetWaypointState(
      int waypointId,
      out CollectionActivityWaypointStateType waypointLockState,
      out string waypointGraphicState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPlayerGraphicResource(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetWaypointRedPointCount(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetLevelUnlockDay(int activityId, int daysBeforeActivate)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetScenarioLevelUnlockDay(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChallengeLevelUnlockDay(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLootLevelUnlockDay(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengePreLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelPlayerLevelVaild(int levelId, out int playerLevelRequired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootPreLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootLevelPlayerLevelVaild(int levelId, out int playerLevelRequired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedLevel(GameFunctionType levelType, int levelId, bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessLevel(
      GameFunctionType levelType,
      int levelId,
      List<int> heroes,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HandleScenario(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetLevelName(CollectionActivityLevelType type, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPageLockMessage(CollectionActivityPage page)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelFinished(LevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public CollectionActivityExchangeModel ExchangeModel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CollectionActivityComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private IEnumerable<ConfigDataCollectionActivityExchangeTableInfo> __callBase_IterateAvailableExchangeItems()
    {
      return this.IterateAvailableExchangeItems();
    }

    private int __callBase_CanExchangeItem(ulong activityInstanceId, int exchangeItemID)
    {
      return this.CanExchangeItem(activityInstanceId, exchangeItemID);
    }

    private int __callBase_ExchangeItem(ulong activityInstanceId, int exchangeItemID)
    {
      return this.ExchangeItem(activityInstanceId, exchangeItemID);
    }

    private ConfigDataCollectionActivityInfo __callBase_GetCollectionActivityInfo(
      ulong activityInstanceId)
    {
      return this.GetCollectionActivityInfo(activityInstanceId);
    }

    private ulong __callBase_GetActivityInstanceId(GameFunctionType typeId, int levelId)
    {
      return this.GetActivityInstanceId(typeId, levelId);
    }

    private ConfigDataCollectionActivityInfo __callBase_GetCollectionActivityInfo(
      GameFunctionType typeId,
      int levelId)
    {
      return this.GetCollectionActivityInfo(typeId, levelId);
    }

    private int __callBase_CanMoveToWayPoint(int waypointId)
    {
      return this.CanMoveToWayPoint(waypointId);
    }

    private int __callBase_MoveToWayPoint(int waypointId)
    {
      return this.MoveToWayPoint(waypointId);
    }

    private int __callBase_CanHandleScenario(int levelId)
    {
      return this.CanHandleScenario(levelId);
    }

    private int __callBase_IsNextScenarioId(ulong activityInstanceId, int levelId)
    {
      return this.IsNextScenarioId(activityInstanceId, levelId);
    }

    private int __callBase_GetNextScenarioId(ulong activityInstanceId)
    {
      return this.GetNextScenarioId(activityInstanceId);
    }

    private int __callBase_CanAttackLevel(GameFunctionType typeId, int levelId)
    {
      return this.CanAttackLevel(typeId, levelId);
    }

    private bool __callBase_IsPrevLevelUnlock(
      CollectionActivity activity,
      List<LevelInfo> PrevLevels)
    {
      return this.IsPrevLevelUnlock(activity, PrevLevels);
    }

    private bool __callBase_IsScenarioFinished(
      CollectionActivity collectionActivity,
      int scenarioId)
    {
      return this.IsScenarioFinished(collectionActivity, scenarioId);
    }

    private int __callBase_AttackLevel(GameFunctionType typeId, int levelId)
    {
      return this.AttackLevel(typeId, levelId);
    }

    private List<int> __callBase_GetAllUnlockedLootLevels()
    {
      return this.GetAllUnlockedLootLevels();
    }

    private bool __callBase_IsLootLevelUnlock(int levelId)
    {
      return this.IsLootLevelUnlock(levelId);
    }

    private void __callBase_AddFinishedLootLevel(CollectionActivity collectionActivity, int levelId)
    {
      this.AddFinishedLootLevel(collectionActivity, levelId);
    }

    private void __callBase_SetCommonSuccessLevel(
      CollectionActivity collectionActivity,
      GameFunctionType typeId,
      int levelId,
      List<int> heroes,
      bool isBattleTeam)
    {
      this.SetCommonSuccessLevel(collectionActivity, typeId, levelId, heroes, isBattleTeam);
    }

    private CollectionActivity __callBase_FindCollectionActivity(ulong instanceId)
    {
      return this.FindCollectionActivity(instanceId);
    }

    private void __callBase_RemoveCollectionActivity(OperationalActivityBase operationalActivity)
    {
      this.RemoveCollectionActivity(operationalActivity);
    }

    private void __callBase_AddCollectionActivity(OperationalActivityBase operationalActivity)
    {
      this.AddCollectionActivity(operationalActivity);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityComponent m_owner;

      public LuaExportHelper(CollectionActivityComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public IEnumerable<ConfigDataCollectionActivityExchangeTableInfo> __callBase_IterateAvailableExchangeItems()
      {
        return this.m_owner.__callBase_IterateAvailableExchangeItems();
      }

      public int __callBase_CanExchangeItem(ulong activityInstanceId, int exchangeItemID)
      {
        return this.m_owner.__callBase_CanExchangeItem(activityInstanceId, exchangeItemID);
      }

      public int __callBase_ExchangeItem(ulong activityInstanceId, int exchangeItemID)
      {
        return this.m_owner.__callBase_ExchangeItem(activityInstanceId, exchangeItemID);
      }

      public ConfigDataCollectionActivityInfo __callBase_GetCollectionActivityInfo(
        ulong activityInstanceId)
      {
        return this.m_owner.__callBase_GetCollectionActivityInfo(activityInstanceId);
      }

      public ulong __callBase_GetActivityInstanceId(GameFunctionType typeId, int levelId)
      {
        return this.m_owner.__callBase_GetActivityInstanceId(typeId, levelId);
      }

      public ConfigDataCollectionActivityInfo __callBase_GetCollectionActivityInfo(
        GameFunctionType typeId,
        int levelId)
      {
        return this.m_owner.__callBase_GetCollectionActivityInfo(typeId, levelId);
      }

      public int __callBase_CanMoveToWayPoint(int waypointId)
      {
        return this.m_owner.__callBase_CanMoveToWayPoint(waypointId);
      }

      public int __callBase_MoveToWayPoint(int waypointId)
      {
        return this.m_owner.__callBase_MoveToWayPoint(waypointId);
      }

      public int __callBase_CanHandleScenario(int levelId)
      {
        return this.m_owner.__callBase_CanHandleScenario(levelId);
      }

      public int __callBase_IsNextScenarioId(ulong activityInstanceId, int levelId)
      {
        return this.m_owner.__callBase_IsNextScenarioId(activityInstanceId, levelId);
      }

      public int __callBase_GetNextScenarioId(ulong activityInstanceId)
      {
        return this.m_owner.__callBase_GetNextScenarioId(activityInstanceId);
      }

      public int __callBase_CanAttackLevel(GameFunctionType typeId, int levelId)
      {
        return this.m_owner.__callBase_CanAttackLevel(typeId, levelId);
      }

      public bool __callBase_IsPrevLevelUnlock(
        CollectionActivity activity,
        List<LevelInfo> PrevLevels)
      {
        return this.m_owner.__callBase_IsPrevLevelUnlock(activity, PrevLevels);
      }

      public bool __callBase_IsScenarioFinished(
        CollectionActivity collectionActivity,
        int scenarioId)
      {
        return this.m_owner.__callBase_IsScenarioFinished(collectionActivity, scenarioId);
      }

      public int __callBase_AttackLevel(GameFunctionType typeId, int levelId)
      {
        return this.m_owner.__callBase_AttackLevel(typeId, levelId);
      }

      public List<int> __callBase_GetAllUnlockedLootLevels()
      {
        return this.m_owner.__callBase_GetAllUnlockedLootLevels();
      }

      public bool __callBase_IsLootLevelUnlock(int levelId)
      {
        return this.m_owner.__callBase_IsLootLevelUnlock(levelId);
      }

      public void __callBase_AddFinishedLootLevel(
        CollectionActivity collectionActivity,
        int levelId)
      {
        this.m_owner.__callBase_AddFinishedLootLevel(collectionActivity, levelId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_SetCommonSuccessLevel(
        CollectionActivity collectionActivity,
        GameFunctionType typeId,
        int levelId,
        List<int> heroes,
        bool isBattleTeam)
      {
      }

      public CollectionActivity __callBase_FindCollectionActivity(ulong instanceId)
      {
        return this.m_owner.__callBase_FindCollectionActivity(instanceId);
      }

      public void __callBase_RemoveCollectionActivity(OperationalActivityBase operationalActivity)
      {
        this.m_owner.__callBase_RemoveCollectionActivity(operationalActivity);
      }

      public void __callBase_AddCollectionActivity(OperationalActivityBase operationalActivity)
      {
        this.m_owner.__callBase_AddCollectionActivity(operationalActivity);
      }

      public List<ConfigDataCollectionActivityScenarioLevelInfo> m_tempScenarioLevelInfos
      {
        get
        {
          return this.m_owner.m_tempScenarioLevelInfos;
        }
        set
        {
          this.m_owner.m_tempScenarioLevelInfos = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public CollectionActivityExchangeModel m_exchangeModel
      {
        get
        {
          return this.m_owner.m_exchangeModel;
        }
        set
        {
          this.m_owner.m_exchangeModel = value;
        }
      }

      public CollectionActivity FindCollectionActivityByCollectionActivityId(
        int collectionActivityId)
      {
        return this.m_owner.FindCollectionActivityByCollectionActivityId(collectionActivityId);
      }

      public void GetFinishedScenarioLevelInfos(
        int collectionActivityId,
        List<ConfigDataCollectionActivityScenarioLevelInfo> scenarioLevelInfos)
      {
        this.m_owner.GetFinishedScenarioLevelInfos(collectionActivityId, scenarioLevelInfos);
      }

      public int GetLevelUnlockDay(int activityId, int daysBeforeActivate)
      {
        return this.m_owner.GetLevelUnlockDay(activityId, daysBeforeActivate);
      }
    }
  }
}
