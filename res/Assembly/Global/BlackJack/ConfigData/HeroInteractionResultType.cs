﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroInteractionResultType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroInteractionResultType")]
  public enum HeroInteractionResultType
  {
    [ProtoEnum(Name = "HeroInteractionResultType_None", Value = 0)] HeroInteractionResultType_None,
    [ProtoEnum(Name = "HeroInteractionResultType_Norml", Value = 1)] HeroInteractionResultType_Norml,
    [ProtoEnum(Name = "HeroInteractionResultType_SmallUp", Value = 2)] HeroInteractionResultType_SmallUp,
    [ProtoEnum(Name = "HeroInteractionResultType_BigUp", Value = 3)] HeroInteractionResultType_BigUp,
  }
}
