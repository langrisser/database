﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityWaypointStateType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityWaypointStateType")]
  public enum CollectionActivityWaypointStateType
  {
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_None", Value = 0)] CollectionActivityWaypointStateType_None,
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_Hidden", Value = 1)] CollectionActivityWaypointStateType_Hidden,
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_Locked", Value = 2)] CollectionActivityWaypointStateType_Locked,
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_Unlocked", Value = 3)] CollectionActivityWaypointStateType_Unlocked,
  }
}
