﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataInitInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataInitInfo")]
  [Serializable]
  public class ConfigDataInitInfo : IExtensible
  {
    private int _ID;
    private List<int> _Heros_ID;
    private int _Gold;
    private int _Crystal;
    private int _Waypoint_ID;
    private List<Goods> _BagItem;
    private int _ArenaTicket;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataInitInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "Heros_ID")]
    public List<int> Heros_ID
    {
      get
      {
        return this._Heros_ID;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Gold")]
    public int Gold
    {
      get
      {
        return this._Gold;
      }
      set
      {
        this._Gold = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Crystal")]
    public int Crystal
    {
      get
      {
        return this._Crystal;
      }
      set
      {
        this._Crystal = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Waypoint_ID")]
    public int Waypoint_ID
    {
      get
      {
        return this._Waypoint_ID;
      }
      set
      {
        this._Waypoint_ID = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "BagItem")]
    public List<Goods> BagItem
    {
      get
      {
        return this._BagItem;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaTicket")]
    public int ArenaTicket
    {
      get
      {
        return this._ArenaTicket;
      }
      set
      {
        this._ArenaTicket = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
