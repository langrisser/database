﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGroupBehavior
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [CustomLuaClass]
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataGroupBehavior")]
  [Serializable]
  public class ConfigDataGroupBehavior : IExtensible
  {
    private int _ID;
    private BehaviorCondition _SelectLeaderCondition;
    private string _SLCParam;
    private int _LeaderBehavior;
    private int _MemberBehavior;
    private IExtension extensionObject;
    public ConfigDataBehavior.ParamData SLCParamData;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGroupBehavior()
    {
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SelectLeaderCondition")]
    public BehaviorCondition SelectLeaderCondition
    {
      get
      {
        return this._SelectLeaderCondition;
      }
      set
      {
        this._SelectLeaderCondition = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "SLCParam")]
    public string SLCParam
    {
      get
      {
        return this._SLCParam;
      }
      set
      {
        this._SLCParam = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LeaderBehavior")]
    public int LeaderBehavior
    {
      get
      {
        return this._LeaderBehavior;
      }
      set
      {
        this._LeaderBehavior = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MemberBehavior")]
    public int MemberBehavior
    {
      get
      {
        return this._MemberBehavior;
      }
      set
      {
        this._MemberBehavior = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
