﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroHeartFetterInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroHeartFetterInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataHeroHeartFetterInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Icon;
    private int _MaxLevel;
    private List<HeroHeartFetterUnlockCondition> _UnlockConditions;
    private List<Goods> _UnlockReward;
    private List<int> _HeroHeartFetterSkills;
    private List<HeroFetterLevelUpCost> _LevelUpMaterials;
    private List<int> _LevelUpGold;
    private List<HeroHeartFetterUnlockSkill> _UnlockSkills_ID;
    private int _ShownSkillCount;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroHeartFetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MaxLevel")]
    public int MaxLevel
    {
      get
      {
        return this._MaxLevel;
      }
      set
      {
        this._MaxLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "UnlockConditions")]
    public List<HeroHeartFetterUnlockCondition> UnlockConditions
    {
      get
      {
        return this._UnlockConditions;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "UnlockReward")]
    public List<Goods> UnlockReward
    {
      get
      {
        return this._UnlockReward;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "HeroHeartFetterSkills")]
    public List<int> HeroHeartFetterSkills
    {
      get
      {
        return this._HeroHeartFetterSkills;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "LevelUpMaterials")]
    public List<HeroFetterLevelUpCost> LevelUpMaterials
    {
      get
      {
        return this._LevelUpMaterials;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, Name = "LevelUpGold")]
    public List<int> LevelUpGold
    {
      get
      {
        return this._LevelUpGold;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, Name = "UnlockSkills_ID")]
    public List<HeroHeartFetterUnlockSkill> UnlockSkills_ID
    {
      get
      {
        return this._UnlockSkills_ID;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ShownSkillCount")]
    public int ShownSkillCount
    {
      get
      {
        return this._ShownSkillCount;
      }
      set
      {
        this._ShownSkillCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
