﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroConfessionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroConfessionInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataHeroConfessionInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _BackGroundImage;
    private string _CharImage;
    private int _PreWordID;
    private int _CorrectID;
    private int _WrongID;
    private int _UnknownID;
    private int _ConfessionLetterID;
    private int _LastWordID;
    private int _UnlockHeroHeartFetterMinLevel;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroConfessionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "BackGroundImage")]
    public string BackGroundImage
    {
      get
      {
        return this._BackGroundImage;
      }
      set
      {
        this._BackGroundImage = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "CharImage")]
    public string CharImage
    {
      get
      {
        return this._CharImage;
      }
      set
      {
        this._CharImage = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PreWordID")]
    public int PreWordID
    {
      get
      {
        return this._PreWordID;
      }
      set
      {
        this._PreWordID = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CorrectID")]
    public int CorrectID
    {
      get
      {
        return this._CorrectID;
      }
      set
      {
        this._CorrectID = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WrongID")]
    public int WrongID
    {
      get
      {
        return this._WrongID;
      }
      set
      {
        this._WrongID = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UnknownID")]
    public int UnknownID
    {
      get
      {
        return this._UnknownID;
      }
      set
      {
        this._UnknownID = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConfessionLetterID")]
    public int ConfessionLetterID
    {
      get
      {
        return this._ConfessionLetterID;
      }
      set
      {
        this._ConfessionLetterID = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LastWordID")]
    public int LastWordID
    {
      get
      {
        return this._LastWordID;
      }
      set
      {
        this._LastWordID = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UnlockHeroHeartFetterMinLevel")]
    public int UnlockHeroHeartFetterMinLevel
    {
      get
      {
        return this._UnlockHeroHeartFetterMinLevel;
      }
      set
      {
        this._UnlockHeroHeartFetterMinLevel = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
