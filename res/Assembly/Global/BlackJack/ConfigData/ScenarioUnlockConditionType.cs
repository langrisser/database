﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ScenarioUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ScenarioUnlockConditionType")]
  public enum ScenarioUnlockConditionType
  {
    [ProtoEnum(Name = "ScenarioUnlockConditionType_None", Value = 0)] ScenarioUnlockConditionType_None,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_PlayerLevel", Value = 1)] ScenarioUnlockConditionType_PlayerLevel,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_RiftLevel", Value = 2)] ScenarioUnlockConditionType_RiftLevel,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_Count", Value = 3)] ScenarioUnlockConditionType_Count,
  }
}
