﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionCardPoolType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MissionCardPoolType")]
  public enum MissionCardPoolType
  {
    [ProtoEnum(Name = "MissionCardPoolType_None", Value = 0)] MissionCardPoolType_None,
    [ProtoEnum(Name = "MissionCardPoolType_Free", Value = 1)] MissionCardPoolType_Free,
    [ProtoEnum(Name = "MissionCardPoolType_Hero", Value = 2)] MissionCardPoolType_Hero,
    [ProtoEnum(Name = "MissionCardPoolType_Equipment", Value = 3)] MissionCardPoolType_Equipment,
  }
}
