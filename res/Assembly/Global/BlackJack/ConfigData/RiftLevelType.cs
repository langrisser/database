﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftLevelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftLevelType")]
  public enum RiftLevelType
  {
    [ProtoEnum(Name = "RiftLevelType_None", Value = 0)] RiftLevelType_None,
    [ProtoEnum(Name = "RiftLevelType_Scenario", Value = 1)] RiftLevelType_Scenario,
    [ProtoEnum(Name = "RiftLevelType_Event", Value = 2)] RiftLevelType_Event,
  }
}
