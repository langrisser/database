﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCollectionActivityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [CustomLuaClass]
  [ProtoContract(Name = "ConfigDataCollectionActivityInfo")]
  [Serializable]
  public class ConfigDataCollectionActivityInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _TabImage;
    private string _BackgroundImage;
    private string _StartPlayerResource;
    private int _MapId;
    private List<int> _ExchangeTable;
    private List<CurrencyItemInfo> _CurrencyItemTable;
    private IExtension extensionObject;
    public ConfigDataCollectionActivityMapInfo MapInfo;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "TabImage")]
    public string TabImage
    {
      get
      {
        return this._TabImage;
      }
      set
      {
        this._TabImage = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "BackgroundImage")]
    public string BackgroundImage
    {
      get
      {
        return this._BackgroundImage;
      }
      set
      {
        this._BackgroundImage = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "StartPlayerResource")]
    public string StartPlayerResource
    {
      get
      {
        return this._StartPlayerResource;
      }
      set
      {
        this._StartPlayerResource = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MapId")]
    public int MapId
    {
      get
      {
        return this._MapId;
      }
      set
      {
        this._MapId = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "ExchangeTable")]
    public List<int> ExchangeTable
    {
      get
      {
        return this._ExchangeTable;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "CurrencyItemTable")]
    public List<CurrencyItemInfo> CurrencyItemTable
    {
      get
      {
        return this._CurrencyItemTable;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
