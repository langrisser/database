﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TrainingTechResourceRequirements
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [CustomLuaClass]
  public class TrainingTechResourceRequirements
  {
    public int Gold;
    public int RoomLevel;
    public List<int> PreTechs;
    public List<int> PreTechLevels;
    public List<Goods> Materials;

    [MethodImpl((MethodImplOptions) 32768)]
    public static TrainingTechResourceRequirements operator +(
      TrainingTechResourceRequirements x,
      TrainingTechResourceRequirements y)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
