﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FetterCompleteConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FetterCompleteConditionType")]
  public enum FetterCompleteConditionType
  {
    [ProtoEnum(Name = "FetterCompleteConditionType_None", Value = 0)] FetterCompleteConditionType_None,
    [ProtoEnum(Name = "FetterCompleteConditionType_HeroFavorabilityLevel", Value = 1)] FetterCompleteConditionType_HeroFavorabilityLevel,
    [ProtoEnum(Name = "FetterCompleteConditionType_Mission", Value = 2)] FetterCompleteConditionType_Mission,
  }
}
