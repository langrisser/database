﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.SelectSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "SelectSkill")]
  public enum SelectSkill
  {
    [ProtoEnum(Name = "SelectSkill_DefaultSelection", Value = 1)] SelectSkill_DefaultSelection = 1,
    [ProtoEnum(Name = "SelectSkill_DirectReachTargetSkill", Value = 2)] SelectSkill_DirectReachTargetSkill = 2,
    [ProtoEnum(Name = "SelectSkill_ExcludeSkillID", Value = 3)] SelectSkill_ExcludeSkillID = 3,
    [ProtoEnum(Name = "SelectSkill_IncludeSkillID", Value = 4)] SelectSkill_IncludeSkillID = 4,
  }
}
