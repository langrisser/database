﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.GameFunctionOpenConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "GameFunctionOpenConditionType")]
  public enum GameFunctionOpenConditionType
  {
    [ProtoEnum(Name = "GameFunctionOpenConditionType_None", Value = 0)] GameFunctionOpenConditionType_None,
    [ProtoEnum(Name = "GameFunctionOpenConditionType_PlayerLevel", Value = 1)] GameFunctionOpenConditionType_PlayerLevel,
    [ProtoEnum(Name = "GameFunctionOpenConditionType_Scenario", Value = 2)] GameFunctionOpenConditionType_Scenario,
    [ProtoEnum(Name = "GameFunctionOpenConditionType_WaypointEvent", Value = 3)] GameFunctionOpenConditionType_WaypointEvent,
    [ProtoEnum(Name = "GameFunctionOpenConditionType_RiftLevel", Value = 4)] GameFunctionOpenConditionType_RiftLevel,
  }
}
