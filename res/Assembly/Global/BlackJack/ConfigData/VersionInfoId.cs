﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.VersionInfoId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "VersionInfoId")]
  public enum VersionInfoId
  {
    [ProtoEnum(Name = "VersionInfoId_ClientProgram", Value = 1)] VersionInfoId_ClientProgram = 1,
    [ProtoEnum(Name = "VersionInfoId_AssetBundle", Value = 2)] VersionInfoId_AssetBundle = 2,
    [ProtoEnum(Name = "VersionInfoId_BattleReport", Value = 3)] VersionInfoId_BattleReport = 3,
  }
}
