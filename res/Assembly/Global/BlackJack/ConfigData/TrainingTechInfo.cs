﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TrainingTechInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [CustomLuaClass]
  public class TrainingTechInfo
  {
    public int ID { get; set; }

    public int TechID { get; set; }

    public int RoomExp { get; set; }

    public int SoldierIDUnlocked { get; set; }

    public int SoldierSkillLevelup { get; set; }

    public int SoldierSkillID { get; set; }

    public int Gold { get; set; }

    public int RoomLevel { get; set; }

    public List<Goods> Materials { get; set; }

    public List<int> PreIds { get; set; }

    public List<int> PreTechLevels { get; set; }

    public ConfigDataTrainingTechLevelInfo Config { get; set; }

    public TrainingTechResourceRequirements LevelupRequirements
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
