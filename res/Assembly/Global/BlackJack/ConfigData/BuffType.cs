﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BuffType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BuffType")]
  public enum BuffType
  {
    [ProtoEnum(Name = "BuffType_None", Value = 0)] BuffType_None,
    [ProtoEnum(Name = "BuffType_PropertiesModify", Value = 1)] BuffType_PropertiesModify,
    [ProtoEnum(Name = "BuffType_Charge", Value = 2)] BuffType_Charge,
    [ProtoEnum(Name = "BuffType_FieldArmy", Value = 3)] BuffType_FieldArmy,
    [ProtoEnum(Name = "BuffType_Immune", Value = 4)] BuffType_Immune,
    [ProtoEnum(Name = "BuffType_DamageRebound", Value = 5)] BuffType_DamageRebound,
    [ProtoEnum(Name = "BuffType_CombatPropertiesModify", Value = 6)] BuffType_CombatPropertiesModify,
    [ProtoEnum(Name = "BuffType_CombatAttachBuff", Value = 7)] BuffType_CombatAttachBuff,
    [ProtoEnum(Name = "BuffType_DamageAttachBuff", Value = 8)] BuffType_DamageAttachBuff,
    [ProtoEnum(Name = "BuffType_DamageRemoveBuff", Value = 9)] BuffType_DamageRemoveBuff,
    [ProtoEnum(Name = "BuffType_CombatHeal", Value = 10)] BuffType_CombatHeal,
    [ProtoEnum(Name = "BuffType_Guard", Value = 11)] BuffType_Guard,
    [ProtoEnum(Name = "BuffType_DoubleAttack", Value = 12)] BuffType_DoubleAttack,
    [ProtoEnum(Name = "BuffType_DoubleMove", Value = 13)] BuffType_DoubleMove,
    [ProtoEnum(Name = "BuffType_Punch", Value = 14)] BuffType_Punch,
    [ProtoEnum(Name = "BuffType_DamageRemoveCD", Value = 15)] BuffType_DamageRemoveCD,
    [ProtoEnum(Name = "BuffType_HealOverTime", Value = 16)] BuffType_HealOverTime,
    [ProtoEnum(Name = "BuffType_DamageOverTime", Value = 17)] BuffType_DamageOverTime,
    [ProtoEnum(Name = "BuffType_NewTurn", Value = 18)] BuffType_NewTurn,
    [ProtoEnum(Name = "BuffType_RemoveDebuff", Value = 19)] BuffType_RemoveDebuff,
    [ProtoEnum(Name = "BuffType_TerrainAdv", Value = 20)] BuffType_TerrainAdv,
    [ProtoEnum(Name = "BuffType_Restrain", Value = 21)] BuffType_Restrain,
    [ProtoEnum(Name = "BuffType_CombatHealOther", Value = 22)] BuffType_CombatHealOther,
    [ProtoEnum(Name = "BuffType_AddBuff", Value = 23)] BuffType_AddBuff,
    [ProtoEnum(Name = "BuffType_CombatDamage", Value = 24)] BuffType_CombatDamage,
    [ProtoEnum(Name = "BuffType_MagicAttack", Value = 25)] BuffType_MagicAttack,
    [ProtoEnum(Name = "BuffType_TeamBuff", Value = 26)] BuffType_TeamBuff,
    [ProtoEnum(Name = "BuffType_BFSkillAttachBuff", Value = 27)] BuffType_BFSkillAttachBuff,
    [ProtoEnum(Name = "BuffType_BFSkill", Value = 28)] BuffType_BFSkill,
    [ProtoEnum(Name = "BuffType_NerverDie", Value = 29)] BuffType_NerverDie,
    [ProtoEnum(Name = "BuffType_BattlePropertiesSet", Value = 30)] BuffType_BattlePropertiesSet,
    [ProtoEnum(Name = "BuffType_Drag", Value = 31)] BuffType_Drag,
    [ProtoEnum(Name = "BuffType_PropertiesExchange", Value = 32)] BuffType_PropertiesExchange,
    [ProtoEnum(Name = "BuffType_DamageAfterDamage", Value = 33)] BuffType_DamageAfterDamage,
    [ProtoEnum(Name = "BuffType_HeroAura", Value = 34)] BuffType_HeroAura,
    [ProtoEnum(Name = "BuffType_TagAura", Value = 35)] BuffType_TagAura,
    [ProtoEnum(Name = "BuffType_RelationModify", Value = 36)] BuffType_RelationModify,
    [ProtoEnum(Name = "BuffType_BuffPropertiesModify", Value = 37)] BuffType_BuffPropertiesModify,
    [ProtoEnum(Name = "BuffType_AddBuffSuper", Value = 38)] BuffType_AddBuffSuper,
    [ProtoEnum(Name = "BuffType_Removebuff", Value = 39)] BuffType_Removebuff,
    [ProtoEnum(Name = "BuffType_BuffPack", Value = 40)] BuffType_BuffPack,
    [ProtoEnum(Name = "BuffType_Replace", Value = 41)] BuffType_Replace,
    [ProtoEnum(Name = "BuffType_ArmyChange", Value = 42)] BuffType_ArmyChange,
    [ProtoEnum(Name = "BuffType_MoveChange", Value = 43)] BuffType_MoveChange,
    [ProtoEnum(Name = "BuffType_DamageHealOther", Value = 44)] BuffType_DamageHealOther,
    [ProtoEnum(Name = "BuffType_PhysicalAttack", Value = 45)] BuffType_PhysicalAttack,
    [ProtoEnum(Name = "BuffType_HealChange", Value = 46)] BuffType_HealChange,
    [ProtoEnum(Name = "BuffType_IgnoreRestrain", Value = 47)] BuffType_IgnoreRestrain,
    [ProtoEnum(Name = "BuffType_HealDamage", Value = 48)] BuffType_HealDamage,
    [ProtoEnum(Name = "BuffType_KillAttachBuff", Value = 49)] BuffType_KillAttachBuff,
    [ProtoEnum(Name = "BuffType_DoubleSkill", Value = 50)] BuffType_DoubleSkill,
  }
}
