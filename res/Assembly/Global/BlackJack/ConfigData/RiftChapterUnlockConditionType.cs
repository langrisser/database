﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftChapterUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftChapterUnlockConditionType")]
  public enum RiftChapterUnlockConditionType
  {
    [ProtoEnum(Name = "RiftChapterUnlockConditionType_None", Value = 0)] RiftChapterUnlockConditionType_None,
    [ProtoEnum(Name = "RiftChapterUnlockConditionType_PlayerLevel", Value = 1)] RiftChapterUnlockConditionType_PlayerLevel,
    [ProtoEnum(Name = "RiftChapterUnlockConditionType_Scenario", Value = 2)] RiftChapterUnlockConditionType_Scenario,
    [ProtoEnum(Name = "RiftChapterUnlockConditionType_ChapterStar", Value = 3)] RiftChapterUnlockConditionType_ChapterStar,
  }
}
