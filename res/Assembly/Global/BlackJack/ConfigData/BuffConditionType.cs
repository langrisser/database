﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BuffConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BuffConditionType")]
  public enum BuffConditionType
  {
    [ProtoEnum(Name = "BuffConditionType_None", Value = 0)] BuffConditionType_None,
    [ProtoEnum(Name = "BuffConditionType_IsAlone", Value = 1)] BuffConditionType_IsAlone,
    [ProtoEnum(Name = "BuffConditionType_NotAlone", Value = 2)] BuffConditionType_NotAlone,
    [ProtoEnum(Name = "BuffConditionType_Terrain", Value = 3)] BuffConditionType_Terrain,
    [ProtoEnum(Name = "BuffConditionType_HeroArmy", Value = 4)] BuffConditionType_HeroArmy,
    [ProtoEnum(Name = "BuffConditionType_TerrainIsDF", Value = 5)] BuffConditionType_TerrainIsDF,
    [ProtoEnum(Name = "BuffConditionType_ArmyCombination", Value = 6)] BuffConditionType_ArmyCombination,
    [ProtoEnum(Name = "BuffConditionType_HeroJob", Value = 7)] BuffConditionType_HeroJob,
  }
}
