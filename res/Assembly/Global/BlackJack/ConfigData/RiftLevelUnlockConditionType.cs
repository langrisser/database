﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftLevelUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftLevelUnlockConditionType")]
  public enum RiftLevelUnlockConditionType
  {
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_None", Value = 0)] RiftLevelUnlockConditionType_None,
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_Scenario", Value = 1)] RiftLevelUnlockConditionType_Scenario,
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_Achievement", Value = 2)] RiftLevelUnlockConditionType_Achievement,
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_Hero", Value = 3)] RiftLevelUnlockConditionType_Hero,
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_RiftLevel", Value = 4)] RiftLevelUnlockConditionType_RiftLevel,
  }
}
