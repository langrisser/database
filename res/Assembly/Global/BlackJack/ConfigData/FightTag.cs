﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FightTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FightTag")]
  public enum FightTag
  {
    [ProtoEnum(Name = "FightTag_None", Value = 0)] FightTag_None,
    [ProtoEnum(Name = "FightTag_IngoreGuard", Value = 1)] FightTag_IngoreGuard,
    [ProtoEnum(Name = "FightTag_BanGuard", Value = 2)] FightTag_BanGuard,
    [ProtoEnum(Name = "FightTag_BanActiveSkill", Value = 3)] FightTag_BanActiveSkill,
    [ProtoEnum(Name = "FightTag_BanPassiveSkill", Value = 4)] FightTag_BanPassiveSkill,
    [ProtoEnum(Name = "FightTag_BanHeal", Value = 5)] FightTag_BanHeal,
    [ProtoEnum(Name = "FightTag_BanDamage", Value = 6)] FightTag_BanDamage,
    [ProtoEnum(Name = "FightTag_BanBuff", Value = 7)] FightTag_BanBuff,
    [ProtoEnum(Name = "FightTag_BanDeBuff", Value = 8)] FightTag_BanDeBuff,
    [ProtoEnum(Name = "FightTag_BanPunch", Value = 9)] FightTag_BanPunch,
    [ProtoEnum(Name = "FightTag_Stun", Value = 10)] FightTag_Stun,
    [ProtoEnum(Name = "FightTag_Hide", Value = 11)] FightTag_Hide,
    [ProtoEnum(Name = "FightTag_BanPercentDamage", Value = 12)] FightTag_BanPercentDamage,
    [ProtoEnum(Name = "FightTag_BanMeleePunish", Value = 13)] FightTag_BanMeleePunish,
    [ProtoEnum(Name = "FightTag_OnlyAttackHero", Value = 14)] FightTag_OnlyAttackHero,
    [ProtoEnum(Name = "FightTag_BanSoldierHeal", Value = 15)] FightTag_BanSoldierHeal,
  }
}
