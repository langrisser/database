﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.WaypointStyleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "WaypointStyleType")]
  public enum WaypointStyleType
  {
    [ProtoEnum(Name = "WaypointStyleType_None", Value = 0)] WaypointStyleType_None,
    [ProtoEnum(Name = "WaypointStyleType_Forest", Value = 1)] WaypointStyleType_Forest,
    [ProtoEnum(Name = "WaypointStyleType_Mountain", Value = 2)] WaypointStyleType_Mountain,
    [ProtoEnum(Name = "WaypointStyleType_Cave", Value = 3)] WaypointStyleType_Cave,
    [ProtoEnum(Name = "WaypointStyleType_Village", Value = 4)] WaypointStyleType_Village,
  }
}
