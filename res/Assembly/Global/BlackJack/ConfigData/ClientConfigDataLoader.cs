﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ClientConfigDataLoader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.ConfigData;
using BlackJack.BJFramework.Runtime.Resource;
using BlackJack.ProjectL.Common;
using BlackJack.UtilityTools;
using ProtoBuf;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix]
  public class ClientConfigDataLoader : ClientConfigDataLoaderBase, IConfigDataLoader
  {
    protected int m_allConfigsCount;
    protected int m_loadingConfigIndex;
    private Dictionary<int, ConfigDataActivityCardPoolGroupInfo> m_ConfigDataActivityCardPoolGroupInfoData;
    private Dictionary<int, ConfigDataAnikiGymInfo> m_ConfigDataAnikiGymInfoData;
    private Dictionary<int, ConfigDataAnikiLevelInfo> m_ConfigDataAnikiLevelInfoData;
    private Dictionary<int, ConfigDataArenaBattleInfo> m_ConfigDataArenaBattleInfoData;
    private Dictionary<int, ConfigDataArenaDefendRuleInfo> m_ConfigDataArenaDefendRuleInfoData;
    private Dictionary<int, ConfigDataArenaLevelInfo> m_ConfigDataArenaLevelInfoData;
    private Dictionary<int, ConfigDataArenaOpponentPointZoneInfo> m_ConfigDataArenaOpponentPointZoneInfoData;
    private Dictionary<int, ConfigDataArenaRankRewardInfo> m_ConfigDataArenaRankRewardInfoData;
    private Dictionary<int, ConfigDataArenaRobotInfo> m_ConfigDataArenaRobotInfoData;
    private Dictionary<int, ConfigDataArenaSettleTimeInfo> m_ConfigDataArenaSettleTimeInfoData;
    private Dictionary<int, ConfigDataArenaVictoryPointsRewardInfo> m_ConfigDataArenaVictoryPointsRewardInfoData;
    private Dictionary<int, ConfigDataArmyInfo> m_ConfigDataArmyInfoData;
    private Dictionary<int, ConfigDataArmyRelation> m_ConfigDataArmyRelationData;
    private Dictionary<int, ConfigDataAssetReplaceInfo> m_ConfigDataAssetReplaceInfoData;
    private Dictionary<int, ConfigDataBanditInfo> m_ConfigDataBanditInfoData;
    private Dictionary<int, ConfigDataBattleAchievementInfo> m_ConfigDataBattleAchievementInfoData;
    private Dictionary<int, ConfigDataBattleAchievementRelatedInfo> m_ConfigDataBattleAchievementRelatedInfoData;
    private Dictionary<int, ConfigDataBattleDialogInfo> m_ConfigDataBattleDialogInfoData;
    private Dictionary<int, ConfigDataBattleEventActionInfo> m_ConfigDataBattleEventActionInfoData;
    private Dictionary<int, ConfigDataBattleEventTriggerInfo> m_ConfigDataBattleEventTriggerInfoData;
    private Dictionary<int, ConfigDataBattlefieldInfo> m_ConfigDataBattlefieldInfoData;
    private Dictionary<int, ConfigDataBattleInfo> m_ConfigDataBattleInfoData;
    private Dictionary<int, ConfigDataBattleLoseConditionInfo> m_ConfigDataBattleLoseConditionInfoData;
    private Dictionary<int, ConfigDataBattlePerformInfo> m_ConfigDataBattlePerformInfoData;
    private Dictionary<int, ConfigDataBattleRandomArmyInfo> m_ConfigDataBattleRandomArmyInfoData;
    private Dictionary<int, ConfigDataBattleRandomTalentInfo> m_ConfigDataBattleRandomTalentInfoData;
    private Dictionary<int, ConfigDataBattleTreasureInfo> m_ConfigDataBattleTreasureInfoData;
    private Dictionary<int, ConfigDataBattleWinConditionInfo> m_ConfigDataBattleWinConditionInfoData;
    private Dictionary<int, ConfigDataBehavior> m_ConfigDataBehaviorData;
    private Dictionary<int, ConfigDataBehaviorChangeRule> m_ConfigDataBehaviorChangeRuleData;
    private Dictionary<int, ConfigDataBigExpressionInfo> m_ConfigDataBigExpressionInfoData;
    private Dictionary<int, ConfigDataBuffInfo> m_ConfigDataBuffInfoData;
    private Dictionary<int, ConfigDataBuyArenaTicketInfo> m_ConfigDataBuyArenaTicketInfoData;
    private Dictionary<int, ConfigDataBuyEnergyInfo> m_ConfigDataBuyEnergyInfoData;
    private Dictionary<int, ConfigDataCardPoolGroupInfo> m_ConfigDataCardPoolGroupInfoData;
    private Dictionary<int, ConfigDataCardPoolInfo> m_ConfigDataCardPoolInfoData;
    private Dictionary<int, ConfigDataChallengeLevelInfo> m_ConfigDataChallengeLevelInfoData;
    private Dictionary<int, ConfigDataCharImageInfo> m_ConfigDataCharImageInfoData;
    private Dictionary<int, ConfigDataCharImageSkinResourceInfo> m_ConfigDataCharImageSkinResourceInfoData;
    private Dictionary<int, ConfigDataCollectionActivityChallengeLevelInfo> m_ConfigDataCollectionActivityChallengeLevelInfoData;
    private Dictionary<int, ConfigDataCollectionActivityCurrencyItemExtInfo> m_ConfigDataCollectionActivityCurrencyItemExtInfoData;
    private Dictionary<int, ConfigDataCollectionActivityExchangeTableInfo> m_ConfigDataCollectionActivityExchangeTableInfoData;
    private Dictionary<int, ConfigDataCollectionActivityInfo> m_ConfigDataCollectionActivityInfoData;
    private Dictionary<int, ConfigDataCollectionActivityLootLevelInfo> m_ConfigDataCollectionActivityLootLevelInfoData;
    private Dictionary<int, ConfigDataCollectionActivityMapInfo> m_ConfigDataCollectionActivityMapInfoData;
    private Dictionary<int, ConfigDataCollectionActivityScenarioLevelInfo> m_ConfigDataCollectionActivityScenarioLevelInfoData;
    private Dictionary<int, ConfigDataCollectionActivityWaypointInfo> m_ConfigDataCollectionActivityWaypointInfoData;
    private Dictionary<int, ConfigDataConfessionDialogInfo> m_ConfigDataConfessionDialogInfoData;
    private Dictionary<int, ConfigDataConfessionLetterInfo> m_ConfigDataConfessionLetterInfoData;
    private Dictionary<int, ConfigDataConfigableConst> m_ConfigDataConfigableConstData;
    private Dictionary<int, ConfigDataConfigIDRangeInfo> m_ConfigDataConfigIDRangeInfoData;
    private Dictionary<int, ConfigDataCooperateBattleInfo> m_ConfigDataCooperateBattleInfoData;
    private Dictionary<int, ConfigDataCooperateBattleLevelInfo> m_ConfigDataCooperateBattleLevelInfoData;
    private Dictionary<int, ConfigDataCrystalCardPoolGroupInfo> m_ConfigDataCrystalCardPoolGroupInfoData;
    private Dictionary<int, ConfigDataCutsceneInfo> m_ConfigDataCutsceneInfoData;
    private Dictionary<int, ConfigDataDailyPushNotification> m_ConfigDataDailyPushNotificationData;
    private Dictionary<int, ConfigDataDeviceSetting> m_ConfigDataDeviceSettingData;
    private Dictionary<int, ConfigDataDialogInfo> m_ConfigDataDialogInfoData;
    private Dictionary<int, ConfigDataEnchantStoneInfo> m_ConfigDataEnchantStoneInfoData;
    private Dictionary<int, ConfigDataEnchantTemplateInfo> m_ConfigDataEnchantTemplateInfoData;
    private Dictionary<int, ConfigDataEquipmentInfo> m_ConfigDataEquipmentInfoData;
    private Dictionary<int, ConfigDataEquipmentLevelInfo> m_ConfigDataEquipmentLevelInfoData;
    private Dictionary<int, ConfigDataEquipmentLevelLimitInfo> m_ConfigDataEquipmentLevelLimitInfoData;
    private Dictionary<int, ConfigDataErrorCodeStringTable> m_ConfigDataErrorCodeStringTableData;
    private Dictionary<int, ConfigDataEternalShrineInfo> m_ConfigDataEternalShrineInfoData;
    private Dictionary<int, ConfigDataEternalShrineLevelInfo> m_ConfigDataEternalShrineLevelInfoData;
    private Dictionary<int, ConfigDataEventInfo> m_ConfigDataEventInfoData;
    private Dictionary<int, ConfigDataExplanationInfo> m_ConfigDataExplanationInfoData;
    private Dictionary<int, ConfigDataFixedStoreItemInfo> m_ConfigDataFixedStoreItemInfoData;
    private Dictionary<int, ConfigDataFlyObjectInfo> m_ConfigDataFlyObjectInfoData;
    private Dictionary<int, ConfigDataFreeCardPoolGroupInfo> m_ConfigDataFreeCardPoolGroupInfoData;
    private Dictionary<int, ConfigDataFxFlipInfo> m_ConfigDataFxFlipInfoData;
    private Dictionary<int, ConfigDataGameFunctionOpenInfo> m_ConfigDataGameFunctionOpenInfoData;
    private Dictionary<int, ConfigDataGamePlayTeamTypeInfo> m_ConfigDataGamePlayTeamTypeInfoData;
    private Dictionary<int, ConfigDataGiftCDKeyInfo> m_ConfigDataGiftCDKeyInfoData;
    private Dictionary<int, ConfigDataGiftStoreItemInfo> m_ConfigDataGiftStoreItemInfoData;
    private Dictionary<int, ConfigDataGoddessDialogInfo> m_ConfigDataGoddessDialogInfoData;
    private Dictionary<int, ConfigDataGroupBehavior> m_ConfigDataGroupBehaviorData;
    private Dictionary<int, ConfigDataGuildMassiveCombatDifficultyInfo> m_ConfigDataGuildMassiveCombatDifficultyInfoData;
    private Dictionary<int, ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo> m_ConfigDataGuildMassiveCombatIndividualPointsRewardsInfoData;
    private Dictionary<int, ConfigDataGuildMassiveCombatLevelInfo> m_ConfigDataGuildMassiveCombatLevelInfoData;
    private Dictionary<int, ConfigDataGuildMassiveCombatRewardsInfo> m_ConfigDataGuildMassiveCombatRewardsInfoData;
    private Dictionary<int, ConfigDataGuildMassiveCombatStrongholdInfo> m_ConfigDataGuildMassiveCombatStrongholdInfoData;
    private Dictionary<int, ConfigDataHeadFrameInfo> m_ConfigDataHeadFrameInfoData;
    private Dictionary<int, ConfigDataHeroAssistantTaskGeneralInfo> m_ConfigDataHeroAssistantTaskGeneralInfoData;
    private Dictionary<int, ConfigDataHeroAssistantTaskInfo> m_ConfigDataHeroAssistantTaskInfoData;
    private Dictionary<int, ConfigDataHeroAssistantTaskScheduleInfo> m_ConfigDataHeroAssistantTaskScheduleInfoData;
    private Dictionary<int, ConfigDataHeroBiographyInfo> m_ConfigDataHeroBiographyInfoData;
    private Dictionary<int, ConfigDataHeroConfessionInfo> m_ConfigDataHeroConfessionInfoData;
    private Dictionary<int, ConfigDataHeroDungeonLevelInfo> m_ConfigDataHeroDungeonLevelInfoData;
    private Dictionary<int, ConfigDataHeroFavorabilityLevelInfo> m_ConfigDataHeroFavorabilityLevelInfoData;
    private Dictionary<int, ConfigDataHeroFetterInfo> m_ConfigDataHeroFetterInfoData;
    private Dictionary<int, ConfigDataHeroHeartFetterInfo> m_ConfigDataHeroHeartFetterInfoData;
    private Dictionary<int, ConfigDataHeroInfo> m_ConfigDataHeroInfoData;
    private Dictionary<int, ConfigDataHeroInformationInfo> m_ConfigDataHeroInformationInfoData;
    private Dictionary<int, ConfigDataHeroInteractionInfo> m_ConfigDataHeroInteractionInfoData;
    private Dictionary<int, ConfigDataHeroLevelInfo> m_ConfigDataHeroLevelInfoData;
    private Dictionary<int, ConfigDataHeroPerformanceInfo> m_ConfigDataHeroPerformanceInfoData;
    private Dictionary<int, ConfigDataHeroPerformanceWordInfo> m_ConfigDataHeroPerformanceWordInfoData;
    private Dictionary<int, ConfigDataHeroPhantomInfo> m_ConfigDataHeroPhantomInfoData;
    private Dictionary<int, ConfigDataHeroPhantomLevelInfo> m_ConfigDataHeroPhantomLevelInfoData;
    private Dictionary<int, ConfigDataHeroSkinInfo> m_ConfigDataHeroSkinInfoData;
    private Dictionary<int, ConfigDataHeroSkinSelfSelectedBoxInfo> m_ConfigDataHeroSkinSelfSelectedBoxInfoData;
    private Dictionary<int, ConfigDataHeroStarInfo> m_ConfigDataHeroStarInfoData;
    private Dictionary<int, ConfigDataHeroTagInfo> m_ConfigDataHeroTagInfoData;
    private Dictionary<int, ConfigDataHeroTrainningInfo> m_ConfigDataHeroTrainningInfoData;
    private Dictionary<int, ConfigDataHeroTrainningLevelInfo> m_ConfigDataHeroTrainningLevelInfoData;
    private Dictionary<int, ConfigDataInitInfo> m_ConfigDataInitInfoData;
    private Dictionary<int, ConfigDataItemInfo> m_ConfigDataItemInfoData;
    private Dictionary<int, ConfigDataJobConnectionInfo> m_ConfigDataJobConnectionInfoData;
    private Dictionary<int, ConfigDataJobInfo> m_ConfigDataJobInfoData;
    private Dictionary<int, ConfigDataJobLevelInfo> m_ConfigDataJobLevelInfoData;
    private Dictionary<int, ConfigDataJobMaterialInfo> m_ConfigDataJobMaterialInfoData;
    private Dictionary<int, ConfigDataJobUnlockConditionInfo> m_ConfigDataJobUnlockConditionInfoData;
    private Dictionary<int, ConfigDataLanguageDataInfo> m_ConfigDataLanguageDataInfoData;
    private Dictionary<int, ConfigDataMailInfo> m_ConfigDataMailInfoData;
    private Dictionary<int, ConfigDataMemoryCorridorInfo> m_ConfigDataMemoryCorridorInfoData;
    private Dictionary<int, ConfigDataMemoryCorridorLevelInfo> m_ConfigDataMemoryCorridorLevelInfoData;
    private Dictionary<int, ConfigDataMissionExtNoviceInfo> m_ConfigDataMissionExtNoviceInfoData;
    private Dictionary<int, ConfigDataMissionExtRefluxInfo> m_ConfigDataMissionExtRefluxInfoData;
    private Dictionary<int, ConfigDataMissionInfo> m_ConfigDataMissionInfoData;
    private Dictionary<int, ConfigDataModelSkinResourceInfo> m_ConfigDataModelSkinResourceInfoData;
    private Dictionary<int, ConfigDataMonthCardInfo> m_ConfigDataMonthCardInfoData;
    private Dictionary<int, ConfigDataNoviceRewardInfo> m_ConfigDataNoviceRewardInfoData;
    private Dictionary<int, ConfigDataOperationalActivityInfo> m_ConfigDataOperationalActivityInfoData;
    private Dictionary<int, ConfigDataOperationalActivityItemGroupInfo> m_ConfigDataOperationalActivityItemGroupInfoData;
    private Dictionary<int, ConfigDataPerformanceInfo> m_ConfigDataPerformanceInfoData;
    private Dictionary<int, ConfigDataPlayerLevelInfo> m_ConfigDataPlayerLevelInfoData;
    private Dictionary<int, ConfigDataPrefabStateInfo> m_ConfigDataPrefabStateInfoData;
    private Dictionary<int, ConfigDataPropertyModifyInfo> m_ConfigDataPropertyModifyInfoData;
    private Dictionary<int, ConfigDataProtagonistInfo> m_ConfigDataProtagonistInfoData;
    private Dictionary<int, ConfigDataPVPBattleInfo> m_ConfigDataPVPBattleInfoData;
    private Dictionary<int, ConfigDataRafflePoolInfo> m_ConfigDataRafflePoolInfoData;
    private Dictionary<int, ConfigDataRandomBoxInfo> m_ConfigDataRandomBoxInfoData;
    private Dictionary<int, ConfigDataRandomDropRewardInfo> m_ConfigDataRandomDropRewardInfoData;
    private Dictionary<int, ConfigDataRandomNameHead> m_ConfigDataRandomNameHeadData;
    private Dictionary<int, ConfigDataRandomNameMiddle> m_ConfigDataRandomNameMiddleData;
    private Dictionary<int, ConfigDataRandomNameTail> m_ConfigDataRandomNameTailData;
    private Dictionary<int, ConfigDataRandomStoreInfo> m_ConfigDataRandomStoreInfoData;
    private Dictionary<int, ConfigDataRandomStoreItemInfo> m_ConfigDataRandomStoreItemInfoData;
    private Dictionary<int, ConfigDataRandomStoreLevelZoneInfo> m_ConfigDataRandomStoreLevelZoneInfoData;
    private Dictionary<int, ConfigDataRankInfo> m_ConfigDataRankInfoData;
    private Dictionary<int, ConfigDataRealTimePVPBattleInfo> m_ConfigDataRealTimePVPBattleInfoData;
    private Dictionary<int, ConfigDataRealTimePVPDanInfo> m_ConfigDataRealTimePVPDanInfoData;
    private Dictionary<int, ConfigDataRealTimePVPDanRewardInfo> m_ConfigDataRealTimePVPDanRewardInfoData;
    private Dictionary<int, ConfigDataRealTimePVPLocalRankingRewardInfo> m_ConfigDataRealTimePVPLocalRankingRewardInfoData;
    private Dictionary<int, ConfigDataRealTimePVPNoviceMatchmakingInfo> m_ConfigDataRealTimePVPNoviceMatchmakingInfoData;
    private Dictionary<int, ConfigDataRealTimePVPRankingRewardInfo> m_ConfigDataRealTimePVPRankingRewardInfoData;
    private Dictionary<int, ConfigDataRealTimePVPSettleTimeInfo> m_ConfigDataRealTimePVPSettleTimeInfoData;
    private Dictionary<int, ConfigDataRealTimePVPWinsBonusInfo> m_ConfigDataRealTimePVPWinsBonusInfoData;
    private Dictionary<int, ConfigDataRechargeStoreItemInfo> m_ConfigDataRechargeStoreItemInfoData;
    private Dictionary<int, ConfigDataRefluxRewardInfo> m_ConfigDataRefluxRewardInfoData;
    private Dictionary<int, ConfigDataRegionInfo> m_ConfigDataRegionInfoData;
    private Dictionary<int, ConfigDataResonanceInfo> m_ConfigDataResonanceInfoData;
    private Dictionary<int, ConfigDataRiftChapterInfo> m_ConfigDataRiftChapterInfoData;
    private Dictionary<int, ConfigDataRiftLevelInfo> m_ConfigDataRiftLevelInfoData;
    private Dictionary<int, ConfigDataScenarioInfo> m_ConfigDataScenarioInfoData;
    private Dictionary<int, ConfigDataScoreLevelInfo> m_ConfigDataScoreLevelInfoData;
    private Dictionary<int, ConfigDataSelectContentInfo> m_ConfigDataSelectContentInfoData;
    private Dictionary<int, ConfigDataSelectProbabilityInfo> m_ConfigDataSelectProbabilityInfoData;
    private Dictionary<int, ConfigDataSelfSelectedBoxInfo> m_ConfigDataSelfSelectedBoxInfoData;
    private Dictionary<int, ConfigDataSensitiveWords> m_ConfigDataSensitiveWordsData;
    private Dictionary<int, ConfigDataSignRewardInfo> m_ConfigDataSignRewardInfoData;
    private Dictionary<int, ConfigDataSkillInfo> m_ConfigDataSkillInfoData;
    private Dictionary<int, ConfigDataSmallExpressionPathInfo> m_ConfigDataSmallExpressionPathInfoData;
    private Dictionary<int, ConfigDataSoldierInfo> m_ConfigDataSoldierInfoData;
    private Dictionary<int, ConfigDataSoldierSkinInfo> m_ConfigDataSoldierSkinInfoData;
    private Dictionary<int, ConfigDataSoundTable> m_ConfigDataSoundTableData;
    private Dictionary<int, ConfigDataSpineAnimationSoundTable> m_ConfigDataSpineAnimationSoundTableData;
    private Dictionary<int, ConfigDataSST_0_CN> m_ConfigDataSST_0_CNData;
    private Dictionary<int, ConfigDataSST_0_EN> m_ConfigDataSST_0_ENData;
    private Dictionary<int, ConfigDataSST_1_CN> m_ConfigDataSST_1_CNData;
    private Dictionary<int, ConfigDataSST_1_EN> m_ConfigDataSST_1_ENData;
    private Dictionary<int, ConfigDataStaticBoxInfo> m_ConfigDataStaticBoxInfoData;
    private Dictionary<int, ConfigDataStoreInfo> m_ConfigDataStoreInfoData;
    private Dictionary<int, ConfigDataStringTable> m_ConfigDataStringTableData;
    private Dictionary<int, ConfigDataStringTableForListInfo> m_ConfigDataStringTableForListInfoData;
    private Dictionary<int, ConfigDataSurveyInfo> m_ConfigDataSurveyInfoData;
    private Dictionary<int, ConfigDataSystemBroadcastInfo> m_ConfigDataSystemBroadcastInfoData;
    private Dictionary<int, ConfigDataTarotInfo> m_ConfigDataTarotInfoData;
    private Dictionary<int, ConfigDataTerrainInfo> m_ConfigDataTerrainInfoData;
    private Dictionary<int, ConfigDataThearchyTrialInfo> m_ConfigDataThearchyTrialInfoData;
    private Dictionary<int, ConfigDataThearchyTrialLevelInfo> m_ConfigDataThearchyTrialLevelInfoData;
    private Dictionary<int, ConfigDataTicketLimitGameFunctionTypeInfo> m_ConfigDataTicketLimitGameFunctionTypeInfoData;
    private Dictionary<int, ConfigDataTicketLimitInfo> m_ConfigDataTicketLimitInfoData;
    private Dictionary<int, ConfigDataTowerBattleRuleInfo> m_ConfigDataTowerBattleRuleInfoData;
    private Dictionary<int, ConfigDataTowerBonusHeroGroupInfo> m_ConfigDataTowerBonusHeroGroupInfoData;
    private Dictionary<int, ConfigDataTowerFloorInfo> m_ConfigDataTowerFloorInfoData;
    private Dictionary<int, ConfigDataTowerLevelInfo> m_ConfigDataTowerLevelInfoData;
    private Dictionary<int, ConfigDataTrainingCourseInfo> m_ConfigDataTrainingCourseInfoData;
    private Dictionary<int, ConfigDataTrainingRoomInfo> m_ConfigDataTrainingRoomInfoData;
    private Dictionary<int, ConfigDataTrainingRoomLevelInfo> m_ConfigDataTrainingRoomLevelInfoData;
    private Dictionary<int, ConfigDataTrainingTechInfo> m_ConfigDataTrainingTechInfoData;
    private Dictionary<int, ConfigDataTrainingTechLevelInfo> m_ConfigDataTrainingTechLevelInfoData;
    private Dictionary<int, ConfigDataTranslate> m_ConfigDataTranslateData;
    private Dictionary<int, ConfigDataTreasureLevelInfo> m_ConfigDataTreasureLevelInfoData;
    private Dictionary<int, ConfigDataUnchartedScoreInfo> m_ConfigDataUnchartedScoreInfoData;
    private Dictionary<int, ConfigDataUnchartedScoreModelInfo> m_ConfigDataUnchartedScoreModelInfoData;
    private Dictionary<int, ConfigDataUnchartedScoreRewardGroupInfo> m_ConfigDataUnchartedScoreRewardGroupInfoData;
    private Dictionary<int, ConfigDataUserGuide> m_ConfigDataUserGuideData;
    private Dictionary<int, ConfigDataUserGuideDialogInfo> m_ConfigDataUserGuideDialogInfoData;
    private Dictionary<int, ConfigDataUserGuideStep> m_ConfigDataUserGuideStepData;
    private Dictionary<int, ConfigDataVersionInfo> m_ConfigDataVersionInfoData;
    private Dictionary<int, ConfigDataWaypointInfo> m_ConfigDataWaypointInfoData;
    private Dictionary<int, ConfigDataWorldMapInfo> m_ConfigDataWorldMapInfoData;
    private Dictionary<int, ConfigDataST_CN> m_ConfigDataST_CNData;
    private Dictionary<int, ConfigDataST_EN> m_ConfigDataST_ENData;
    private const int SignRewardSpecificErrorIdIncrementValue = -1;
    private const int ChapterSpecificErrorIdIncrementValue = -2;
    private const int EventSpecificErrorIdIncrementValue = -3;
    private const int RiftLevelSpecificErrorIdIncrementValue = -4;
    private const int InitInfoSpecificErrorIdIncrementValue = -5;
    private const int RandomDropSpecificErrorIdIncrementValue = -6;
    private const int ScenarioSpecificErrorIdIncrementValue = -7;
    private const int FreeCardPoolSpecificErrorIdIncrementValue = -8;
    private const int CrystalCardPoolSpecificErrorIdIncrementValue = -9;
    private const int ActivityCardPoolSpecificErrorIdIncrementValue = -10;
    private const int HeroJobUnlockConditionsSpecificErrorIdIncrementValue = -23;
    private const int ErrCodeFixedStoreItemFirstRewardSpecificErrorIdIncrementValue = -33;
    private const int ErrCodeFixedStoreSellItemSpecificErrorIdIncrementValue = -34;
    private const int ErrCodeCoachFavorabilityLevelUpCostItemSpecificErrorIdIncrementValue = -40;
    private const int ErrCodeHeroDungeonStarRewardSpecificErrorIdIncrementValue = -53;
    private const int ErrCodeHeroDungeonLevelAchievementRewardSpecificErrorIdIncrementValue = -54;
    private const int ErrCodeOperationalActivityItemGroupItemSpecificErrorIdIncreasementValue = -65;
    private const int ErrCodeStaticBoxItemSpecificErrorIdIncreasementValue = -68;
    private const int ErrCodeBattleTreasureRewardSpecificErrorIdIncreasementValue = -70;
    public const int ErrCodeGiftStoreFirstRewardSpecificErrorIdIncreasementValue = -91;
    [DoNotToLua]
    private ArmyRelationData[,] m_armyRelationTable;
    private Dictionary<string, string> m_fxFlipTable;
    [DoNotToLua]
    private ClientConfigDataLoader.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetAllInitLoadConfigDataAssetPath_hotfix;
    private LuaFunction m_GetAllLazyLoadConfigDataAssetPath_hotfix;
    private LuaFunction m_OnInitLoadFromAssetEndWorkerInt32Int32Int32Action`1List`1List`1_hotfix;
    private LuaFunction m_AddConfigDataItemForLuaDummyTypeStringDummyType_hotfix;
    private LuaFunction m_GetAllInitLoadConfigDataTypeNameForLuaDummy_hotfix;
    private LuaFunction m_OnLazyLoadFromAssetEndStringStringObjectAction`1_hotfix;
    private LuaFunction m_GetLazyLoadConfigAssetNameByKeyStringInt32_hotfix;
    private LuaFunction m_GetConfigDataActivityCardPoolGroupInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataActivityCardPoolGroupInfo_hotfix;
    private LuaFunction m_GetConfigDataAnikiGymInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataAnikiGymInfo_hotfix;
    private LuaFunction m_GetConfigDataAnikiLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataAnikiLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataArenaBattleInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataArenaBattleInfo_hotfix;
    private LuaFunction m_GetConfigDataArenaDefendRuleInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataArenaDefendRuleInfo_hotfix;
    private LuaFunction m_GetConfigDataArenaLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataArenaLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataArenaOpponentPointZoneInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataArenaOpponentPointZoneInfo_hotfix;
    private LuaFunction m_GetConfigDataArenaRankRewardInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataArenaRankRewardInfo_hotfix;
    private LuaFunction m_GetConfigDataArenaRobotInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataArenaRobotInfo_hotfix;
    private LuaFunction m_GetConfigDataArenaSettleTimeInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataArenaSettleTimeInfo_hotfix;
    private LuaFunction m_GetConfigDataArenaVictoryPointsRewardInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataArenaVictoryPointsRewardInfo_hotfix;
    private LuaFunction m_GetConfigDataArmyInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataArmyInfo_hotfix;
    private LuaFunction m_GetConfigDataArmyRelationInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataArmyRelation_hotfix;
    private LuaFunction m_GetConfigDataAssetReplaceInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataAssetReplaceInfo_hotfix;
    private LuaFunction m_GetConfigDataBanditInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBanditInfo_hotfix;
    private LuaFunction m_GetConfigDataBattleAchievementInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattleAchievementInfo_hotfix;
    private LuaFunction m_GetConfigDataBattleAchievementRelatedInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattleAchievementRelatedInfo_hotfix;
    private LuaFunction m_GetConfigDataBattleDialogInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattleDialogInfo_hotfix;
    private LuaFunction m_GetConfigDataBattleEventActionInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattleEventActionInfo_hotfix;
    private LuaFunction m_GetConfigDataBattleEventTriggerInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattleEventTriggerInfo_hotfix;
    private LuaFunction m_GetConfigDataBattlefieldInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattlefieldInfo_hotfix;
    private LuaFunction m_GetConfigDataBattleInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattleInfo_hotfix;
    private LuaFunction m_GetConfigDataBattleLoseConditionInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattleLoseConditionInfo_hotfix;
    private LuaFunction m_GetConfigDataBattlePerformInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattlePerformInfo_hotfix;
    private LuaFunction m_GetConfigDataBattleRandomArmyInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattleRandomArmyInfo_hotfix;
    private LuaFunction m_GetConfigDataBattleRandomTalentInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattleRandomTalentInfo_hotfix;
    private LuaFunction m_GetConfigDataBattleTreasureInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattleTreasureInfo_hotfix;
    private LuaFunction m_GetConfigDataBattleWinConditionInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBattleWinConditionInfo_hotfix;
    private LuaFunction m_GetConfigDataBehaviorInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBehavior_hotfix;
    private LuaFunction m_GetConfigDataBehaviorChangeRuleInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBehaviorChangeRule_hotfix;
    private LuaFunction m_GetConfigDataBigExpressionInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBigExpressionInfo_hotfix;
    private LuaFunction m_GetConfigDataBuffInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBuffInfo_hotfix;
    private LuaFunction m_GetConfigDataBuyArenaTicketInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBuyArenaTicketInfo_hotfix;
    private LuaFunction m_GetConfigDataBuyEnergyInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataBuyEnergyInfo_hotfix;
    private LuaFunction m_GetConfigDataCardPoolGroupInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCardPoolGroupInfo_hotfix;
    private LuaFunction m_GetConfigDataCardPoolInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCardPoolInfo_hotfix;
    private LuaFunction m_GetConfigDataChallengeLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataChallengeLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataCharImageInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCharImageInfo_hotfix;
    private LuaFunction m_GetConfigDataCharImageSkinResourceInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCharImageSkinResourceInfo_hotfix;
    private LuaFunction m_GetConfigDataCollectionActivityChallengeLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCollectionActivityChallengeLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataCollectionActivityCurrencyItemExtInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCollectionActivityCurrencyItemExtInfo_hotfix;
    private LuaFunction m_GetConfigDataCollectionActivityExchangeTableInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCollectionActivityExchangeTableInfo_hotfix;
    private LuaFunction m_GetConfigDataCollectionActivityInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCollectionActivityInfo_hotfix;
    private LuaFunction m_GetConfigDataCollectionActivityLootLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCollectionActivityLootLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataCollectionActivityMapInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCollectionActivityMapInfo_hotfix;
    private LuaFunction m_GetConfigDataCollectionActivityScenarioLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCollectionActivityScenarioLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataCollectionActivityWaypointInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCollectionActivityWaypointInfo_hotfix;
    private LuaFunction m_GetConfigDataConfessionDialogInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataConfessionDialogInfo_hotfix;
    private LuaFunction m_GetConfigDataConfessionLetterInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataConfessionLetterInfo_hotfix;
    private LuaFunction m_GetConfigDataConfigableConstInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataConfigableConst_hotfix;
    private LuaFunction m_GetConfigDataConfigIDRangeInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataConfigIDRangeInfo_hotfix;
    private LuaFunction m_GetConfigDataCooperateBattleInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCooperateBattleInfo_hotfix;
    private LuaFunction m_GetConfigDataCooperateBattleLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCooperateBattleLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataCrystalCardPoolGroupInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCrystalCardPoolGroupInfo_hotfix;
    private LuaFunction m_GetConfigDataCutsceneInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataCutsceneInfo_hotfix;
    private LuaFunction m_GetConfigDataDailyPushNotificationInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataDailyPushNotification_hotfix;
    private LuaFunction m_GetConfigDataDeviceSettingInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataDeviceSetting_hotfix;
    private LuaFunction m_GetConfigDataDialogInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataDialogInfo_hotfix;
    private LuaFunction m_GetConfigDataEnchantStoneInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataEnchantStoneInfo_hotfix;
    private LuaFunction m_GetConfigDataEnchantTemplateInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataEnchantTemplateInfo_hotfix;
    private LuaFunction m_GetConfigDataEquipmentInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataEquipmentInfo_hotfix;
    private LuaFunction m_GetConfigDataEquipmentLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataEquipmentLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataEquipmentLevelLimitInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataEquipmentLevelLimitInfo_hotfix;
    private LuaFunction m_GetConfigDataErrorCodeStringTableInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataErrorCodeStringTable_hotfix;
    private LuaFunction m_GetConfigDataEternalShrineInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataEternalShrineInfo_hotfix;
    private LuaFunction m_GetConfigDataEternalShrineLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataEternalShrineLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataEventInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataEventInfo_hotfix;
    private LuaFunction m_GetConfigDataExplanationInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataExplanationInfo_hotfix;
    private LuaFunction m_GetConfigDataFixedStoreItemInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataFixedStoreItemInfo_hotfix;
    private LuaFunction m_GetConfigDataFlyObjectInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataFlyObjectInfo_hotfix;
    private LuaFunction m_GetConfigDataFreeCardPoolGroupInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataFreeCardPoolGroupInfo_hotfix;
    private LuaFunction m_GetConfigDataFxFlipInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataFxFlipInfo_hotfix;
    private LuaFunction m_GetConfigDataGameFunctionOpenInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataGameFunctionOpenInfo_hotfix;
    private LuaFunction m_GetConfigDataGamePlayTeamTypeInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataGamePlayTeamTypeInfo_hotfix;
    private LuaFunction m_GetConfigDataGiftCDKeyInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataGiftCDKeyInfo_hotfix;
    private LuaFunction m_GetConfigDataGiftStoreItemInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataGiftStoreItemInfo_hotfix;
    private LuaFunction m_GetConfigDataGoddessDialogInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataGoddessDialogInfo_hotfix;
    private LuaFunction m_GetConfigDataGroupBehaviorInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataGroupBehavior_hotfix;
    private LuaFunction m_GetConfigDataGuildMassiveCombatDifficultyInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataGuildMassiveCombatDifficultyInfo_hotfix;
    private LuaFunction m_GetConfigDataGuildMassiveCombatIndividualPointsRewardsInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataGuildMassiveCombatIndividualPointsRewardsInfo_hotfix;
    private LuaFunction m_GetConfigDataGuildMassiveCombatLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataGuildMassiveCombatLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataGuildMassiveCombatRewardsInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataGuildMassiveCombatRewardsInfo_hotfix;
    private LuaFunction m_GetConfigDataGuildMassiveCombatStrongholdInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataGuildMassiveCombatStrongholdInfo_hotfix;
    private LuaFunction m_GetConfigDataHeadFrameInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeadFrameInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroAssistantTaskGeneralInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroAssistantTaskGeneralInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroAssistantTaskInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroAssistantTaskInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroAssistantTaskScheduleInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroAssistantTaskScheduleInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroBiographyInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroBiographyInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroConfessionInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroConfessionInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroDungeonLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroDungeonLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroFavorabilityLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroFavorabilityLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroFetterInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroFetterInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroHeartFetterInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroHeartFetterInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroInformationInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroInformationInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroInteractionInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroInteractionInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroPerformanceInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroPerformanceInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroPerformanceWordInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroPerformanceWordInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroPhantomInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroPhantomInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroPhantomLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroPhantomLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroSkinInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroSkinInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroSkinSelfSelectedBoxInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroSkinSelfSelectedBoxInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroStarInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroStarInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroTagInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroTagInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroTrainningInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroTrainningInfo_hotfix;
    private LuaFunction m_GetConfigDataHeroTrainningLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataHeroTrainningLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataInitInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataInitInfo_hotfix;
    private LuaFunction m_GetConfigDataItemInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataItemInfo_hotfix;
    private LuaFunction m_GetConfigDataJobConnectionInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataJobConnectionInfo_hotfix;
    private LuaFunction m_GetConfigDataJobInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataJobInfo_hotfix;
    private LuaFunction m_GetConfigDataJobLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataJobLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataJobMaterialInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataJobMaterialInfo_hotfix;
    private LuaFunction m_GetConfigDataJobUnlockConditionInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataJobUnlockConditionInfo_hotfix;
    private LuaFunction m_GetConfigDataLanguageDataInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataLanguageDataInfo_hotfix;
    private LuaFunction m_GetConfigDataMailInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataMailInfo_hotfix;
    private LuaFunction m_GetConfigDataMemoryCorridorInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataMemoryCorridorInfo_hotfix;
    private LuaFunction m_GetConfigDataMemoryCorridorLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataMemoryCorridorLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataMissionExtNoviceInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataMissionExtNoviceInfo_hotfix;
    private LuaFunction m_GetConfigDataMissionExtRefluxInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataMissionExtRefluxInfo_hotfix;
    private LuaFunction m_GetConfigDataMissionInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataMissionInfo_hotfix;
    private LuaFunction m_GetConfigDataModelSkinResourceInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataModelSkinResourceInfo_hotfix;
    private LuaFunction m_GetConfigDataMonthCardInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataMonthCardInfo_hotfix;
    private LuaFunction m_GetConfigDataNoviceRewardInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataNoviceRewardInfo_hotfix;
    private LuaFunction m_GetConfigDataOperationalActivityInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataOperationalActivityInfo_hotfix;
    private LuaFunction m_GetConfigDataOperationalActivityItemGroupInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataOperationalActivityItemGroupInfo_hotfix;
    private LuaFunction m_GetConfigDataPerformanceInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataPerformanceInfo_hotfix;
    private LuaFunction m_GetConfigDataPlayerLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataPlayerLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataPrefabStateInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataPrefabStateInfo_hotfix;
    private LuaFunction m_GetConfigDataPropertyModifyInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataPropertyModifyInfo_hotfix;
    private LuaFunction m_GetConfigDataProtagonistInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataProtagonistInfo_hotfix;
    private LuaFunction m_GetConfigDataPVPBattleInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataPVPBattleInfo_hotfix;
    private LuaFunction m_GetConfigDataRafflePoolInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRafflePoolInfo_hotfix;
    private LuaFunction m_GetConfigDataRandomBoxInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRandomBoxInfo_hotfix;
    private LuaFunction m_GetConfigDataRandomDropRewardInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRandomDropRewardInfo_hotfix;
    private LuaFunction m_GetConfigDataRandomNameHeadInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRandomNameHead_hotfix;
    private LuaFunction m_GetConfigDataRandomNameMiddleInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRandomNameMiddle_hotfix;
    private LuaFunction m_GetConfigDataRandomNameTailInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRandomNameTail_hotfix;
    private LuaFunction m_GetConfigDataRandomStoreInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRandomStoreInfo_hotfix;
    private LuaFunction m_GetConfigDataRandomStoreItemInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRandomStoreItemInfo_hotfix;
    private LuaFunction m_GetConfigDataRandomStoreLevelZoneInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRandomStoreLevelZoneInfo_hotfix;
    private LuaFunction m_GetConfigDataRankInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRankInfo_hotfix;
    private LuaFunction m_GetConfigDataRealTimePVPBattleInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRealTimePVPBattleInfo_hotfix;
    private LuaFunction m_GetConfigDataRealTimePVPDanInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRealTimePVPDanInfo_hotfix;
    private LuaFunction m_GetConfigDataRealTimePVPDanRewardInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRealTimePVPDanRewardInfo_hotfix;
    private LuaFunction m_GetConfigDataRealTimePVPLocalRankingRewardInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRealTimePVPLocalRankingRewardInfo_hotfix;
    private LuaFunction m_GetConfigDataRealTimePVPNoviceMatchmakingInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRealTimePVPNoviceMatchmakingInfo_hotfix;
    private LuaFunction m_GetConfigDataRealTimePVPRankingRewardInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRealTimePVPRankingRewardInfo_hotfix;
    private LuaFunction m_GetConfigDataRealTimePVPSettleTimeInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRealTimePVPSettleTimeInfo_hotfix;
    private LuaFunction m_GetConfigDataRealTimePVPWinsBonusInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRealTimePVPWinsBonusInfo_hotfix;
    private LuaFunction m_GetConfigDataRechargeStoreItemInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRechargeStoreItemInfo_hotfix;
    private LuaFunction m_GetConfigDataRefluxRewardInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRefluxRewardInfo_hotfix;
    private LuaFunction m_GetConfigDataRegionInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRegionInfo_hotfix;
    private LuaFunction m_GetConfigDataResonanceInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataResonanceInfo_hotfix;
    private LuaFunction m_GetConfigDataRiftChapterInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRiftChapterInfo_hotfix;
    private LuaFunction m_GetConfigDataRiftLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataRiftLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataScenarioInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataScenarioInfo_hotfix;
    private LuaFunction m_GetConfigDataScoreLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataScoreLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataSelectContentInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSelectContentInfo_hotfix;
    private LuaFunction m_GetConfigDataSelectProbabilityInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSelectProbabilityInfo_hotfix;
    private LuaFunction m_GetConfigDataSelfSelectedBoxInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSelfSelectedBoxInfo_hotfix;
    private LuaFunction m_GetConfigDataSensitiveWordsInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSensitiveWords_hotfix;
    private LuaFunction m_GetConfigDataSignRewardInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSignRewardInfo_hotfix;
    private LuaFunction m_GetConfigDataSkillInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSkillInfo_hotfix;
    private LuaFunction m_GetConfigDataSmallExpressionPathInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSmallExpressionPathInfo_hotfix;
    private LuaFunction m_GetConfigDataSoldierInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSoldierInfo_hotfix;
    private LuaFunction m_GetConfigDataSoldierSkinInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSoldierSkinInfo_hotfix;
    private LuaFunction m_GetConfigDataSoundTableInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSoundTable_hotfix;
    private LuaFunction m_GetConfigDataSpineAnimationSoundTableInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSpineAnimationSoundTable_hotfix;
    private LuaFunction m_GetConfigDataSST_0_CNInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSST_0_CN_hotfix;
    private LuaFunction m_GetConfigDataSST_0_ENInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSST_0_EN_hotfix;
    private LuaFunction m_GetConfigDataSST_1_CNInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSST_1_CN_hotfix;
    private LuaFunction m_GetConfigDataSST_1_ENInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSST_1_EN_hotfix;
    private LuaFunction m_GetConfigDataStaticBoxInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataStaticBoxInfo_hotfix;
    private LuaFunction m_GetConfigDataStoreInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataStoreInfo_hotfix;
    private LuaFunction m_GetConfigDataStringTableInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataStringTable_hotfix;
    private LuaFunction m_GetConfigDataStringTableForListInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataStringTableForListInfo_hotfix;
    private LuaFunction m_GetConfigDataSurveyInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSurveyInfo_hotfix;
    private LuaFunction m_GetConfigDataSystemBroadcastInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataSystemBroadcastInfo_hotfix;
    private LuaFunction m_GetConfigDataTarotInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTarotInfo_hotfix;
    private LuaFunction m_GetConfigDataTerrainInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTerrainInfo_hotfix;
    private LuaFunction m_GetConfigDataThearchyTrialInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataThearchyTrialInfo_hotfix;
    private LuaFunction m_GetConfigDataThearchyTrialLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataThearchyTrialLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataTicketLimitGameFunctionTypeInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTicketLimitGameFunctionTypeInfo_hotfix;
    private LuaFunction m_GetConfigDataTicketLimitInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTicketLimitInfo_hotfix;
    private LuaFunction m_GetConfigDataTowerBattleRuleInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTowerBattleRuleInfo_hotfix;
    private LuaFunction m_GetConfigDataTowerBonusHeroGroupInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTowerBonusHeroGroupInfo_hotfix;
    private LuaFunction m_GetConfigDataTowerFloorInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTowerFloorInfo_hotfix;
    private LuaFunction m_GetConfigDataTowerLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTowerLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataTrainingCourseInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTrainingCourseInfo_hotfix;
    private LuaFunction m_GetConfigDataTrainingRoomInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTrainingRoomInfo_hotfix;
    private LuaFunction m_GetConfigDataTrainingRoomLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTrainingRoomLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataTrainingTechInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTrainingTechInfo_hotfix;
    private LuaFunction m_GetConfigDataTrainingTechLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTrainingTechLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataTranslateInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTranslate_hotfix;
    private LuaFunction m_GetConfigDataTreasureLevelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataTreasureLevelInfo_hotfix;
    private LuaFunction m_GetConfigDataUnchartedScoreInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataUnchartedScoreInfo_hotfix;
    private LuaFunction m_GetConfigDataUnchartedScoreModelInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataUnchartedScoreModelInfo_hotfix;
    private LuaFunction m_GetConfigDataUnchartedScoreRewardGroupInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataUnchartedScoreRewardGroupInfo_hotfix;
    private LuaFunction m_GetConfigDataUserGuideInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataUserGuide_hotfix;
    private LuaFunction m_GetConfigDataUserGuideDialogInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataUserGuideDialogInfo_hotfix;
    private LuaFunction m_GetConfigDataUserGuideStepInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataUserGuideStep_hotfix;
    private LuaFunction m_GetConfigDataVersionInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataVersionInfo_hotfix;
    private LuaFunction m_GetConfigDataWaypointInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataWaypointInfo_hotfix;
    private LuaFunction m_GetConfigDataWorldMapInfoInt32_hotfix;
    private LuaFunction m_LuaGetAllConfigDataWorldMapInfo_hotfix;
    private LuaFunction m_GetConfigDataST_CNInt32_hotfix;
    private LuaFunction m_GetConfigDataST_CNInt32Action`1_hotfix;
    private LuaFunction m_LuaGetAllConfigDataST_CN_hotfix;
    private LuaFunction m_IsConfigDataST_CNReadyInt32_hotfix;
    private LuaFunction m_ClearConfigDataST_CN_hotfix;
    private LuaFunction m_GetConfigDataST_ENInt32_hotfix;
    private LuaFunction m_GetConfigDataST_ENInt32Action`1_hotfix;
    private LuaFunction m_LuaGetAllConfigDataST_EN_hotfix;
    private LuaFunction m_IsConfigDataST_ENReadyInt32_hotfix;
    private LuaFunction m_ClearConfigDataST_EN_hotfix;
    private LuaFunction m_get_LoadingProgress_hotfix;
    private LuaFunction m_UtilityInitialize_hotfix;
    private LuaFunction m_EditorInitialize_hotfix;
    private LuaFunction m_ReleaseInitialize_hotfix;
    private LuaFunction m_PreInitialize_hotfix;
    private LuaFunction m_ReplaceAssetPaths_hotfix;
    private LuaFunction m_UtilityGetArmyRelationDataArmyTagArmyTag_hotfix;
    private LuaFunction m_UtilityGetConfigableConstConfigableConstId_hotfix;
    private LuaFunction m_UtilityGetConfigableConstStringConfigableConstId_hotfix;
    private LuaFunction m_UtilityGetStringByStringTableStringTableId_hotfix;
    private LuaFunction m_UtilityGetFxFlipNameString_hotfix;
    private LuaFunction m_UtilityGetVersionVersionInfoId_hotfix;
    private LuaFunction m_UtilityGetVersionStringVersionInfoId_hotfix;
    private LuaFunction m_get_Version_hotfix;
    private LuaFunction m_UtilityGetSoundSoundTableId_hotfix;
    private LuaFunction m_CheckGoodsGoodsTypeInt32_hotfix;
    private LuaFunction m_CheckGoodsListList`1Int32String_hotfix;
    private LuaFunction m_WeightGoodsToGoodsList`1_hotfix;
    private LuaFunction m_PreCheckData_hotfix;
    private LuaFunction m_PostCheckData_hotfix;
    private LuaFunction m_CheckAssetsExist_hotfix;
    private LuaFunction m_CheckAssetExistStringStringStringInt32_hotfix;
    private LuaFunction m_UtilityGetErrorCodeStringInt32_hotfix;
    private LuaFunction m_CheckRandomBoxGoodsListList`1String_hotfix;
    private LuaFunction m_AddArmyRelationDataAttackArmyTagArmyTagInt32Int32_hotfix;
    private LuaFunction m_AddArmyRelationDataDefendArmyTagArmyTagInt32Int32_hotfix;
    private LuaFunction m_UtilityGetSensitiveWords_hotfix;
    private LuaFunction m_get_Const_BattleActorMoveSpeed_hotfix;
    private LuaFunction m_set_Const_BattleActorMoveSpeedInt32_hotfix;
    private LuaFunction m_get_Const_CombatHeroDistance_hotfix;
    private LuaFunction m_set_Const_CombatHeroDistanceInt32_hotfix;
    private LuaFunction m_get_Const_CombatSplitScreenDistance_hotfix;
    private LuaFunction m_set_Const_CombatSplitScreenDistanceInt32_hotfix;
    private LuaFunction m_get_Const_MeleeATKPunish_Mult_hotfix;
    private LuaFunction m_set_Const_MeleeATKPunish_MultInt32_hotfix;
    private LuaFunction m_get_Const_SoldierMoveDelay_hotfix;
    private LuaFunction m_set_Const_SoldierMoveDelayInt32_hotfix;
    private LuaFunction m_get_Const_SoldierReturnDelay_hotfix;
    private LuaFunction m_set_Const_SoldierReturnDelayInt32_hotfix;
    private LuaFunction m_get_Const_SkillPauseTime_hotfix;
    private LuaFunction m_set_Const_SkillPauseTimeInt32_hotfix;
    private LuaFunction m_get_Const_SkillPreCastDelay_hotfix;
    private LuaFunction m_set_Const_SkillPreCastDelayInt32_hotfix;
    private LuaFunction m_get_Const_DamagePostDelay_hotfix;
    private LuaFunction m_set_Const_DamagePostDelayInt32_hotfix;
    private LuaFunction m_get_Const_BuffHitPostDelay_hotfix;
    private LuaFunction m_set_Const_BuffHitPostDelayInt32_hotfix;
    private LuaFunction m_get_Const_SoldierCountMax_hotfix;
    private LuaFunction m_set_Const_SoldierCountMaxInt32_hotfix;
    private LuaFunction m_get_Const_HPBarFxAccumulateTime_hotfix;
    private LuaFunction m_set_Const_HPBarFxAccumulateTimeInt32_hotfix;
    private LuaFunction m_get_Const_HPBarFxAccumulateDamage_hotfix;
    private LuaFunction m_set_Const_HPBarFxAccumulateDamageInt32_hotfix;
    private LuaFunction m_get_Const_CriticalDamageBase_hotfix;
    private LuaFunction m_set_Const_CriticalDamageBaseInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_FlushTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_FlushTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_FlushPeriodDay_hotfix;
    private LuaFunction m_set_ConfigableConstId_FlushPeriodDayInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_TimeEventStartTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_TimeEventStartTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_TimeEventEndTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_TimeEventEndTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_TimeEventMinCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_TimeEventMinCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_TimeEventMaxCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_TimeEventMaxCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RiftLevelActionEventProbality_hotfix;
    private LuaFunction m_set_ConfigableConstId_RiftLevelActionEventProbalityInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ScenarioActionEventProbality_hotfix;
    private LuaFunction m_set_ConfigableConstId_ScenarioActionEventProbalityInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_EventActionEventProbality_hotfix;
    private LuaFunction m_set_ConfigableConstId_EventActionEventProbalityInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ProbalityMax_hotfix;
    private LuaFunction m_set_ConfigableConstId_ProbalityMaxInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ActionEventMaxCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_ActionEventMaxCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MapRandomEventMaxCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_MapRandomEventMaxCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_EnergyAddPeriod_hotfix;
    private LuaFunction m_set_ConfigableConstId_EnergyAddPeriodInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_EnergyMax_hotfix;
    private LuaFunction m_set_ConfigableConstId_EnergyMaxInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroJobMaterialMaxNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroJobMaterialMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_BagMaxCapacity_hotfix;
    private LuaFunction m_set_ConfigableConstId_BagMaxCapacityInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroJobRankMax_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroJobRankMaxInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroLevelUpCeiling_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroLevelUpCeilingInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_SelectSkillsMaxCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_SelectSkillsMaxCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_BagGridMaxCapacity_hotfix;
    private LuaFunction m_set_ConfigableConstId_BagGridMaxCapacityInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroStarLevelMax_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroStarLevelMaxInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RiftLevelMaxStar_hotfix;
    private LuaFunction m_set_ConfigableConstId_RiftLevelMaxStarInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_JobStartLeve_hotfix;
    private LuaFunction m_set_ConfigableConstId_JobStartLeveInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_FirstScenarioId_hotfix;
    private LuaFunction m_set_ConfigableConstId_FirstScenarioIdInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_FirstWayPointId_hotfix;
    private LuaFunction m_set_ConfigableConstId_FirstWayPointIdInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MailBoxMaxSize_hotfix;
    private LuaFunction m_set_ConfigableConstId_MailBoxMaxSizeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_WeaponSlotId_hotfix;
    private LuaFunction m_set_ConfigableConstId_WeaponSlotIdInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArmorSlotId_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArmorSlotIdInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_OrnamentSlotId_hotfix;
    private LuaFunction m_set_ConfigableConstId_OrnamentSlotIdInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ShoeSlotId_hotfix;
    private LuaFunction m_set_ConfigableConstId_ShoeSlotIdInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_FirstInitId_hotfix;
    private LuaFunction m_set_ConfigableConstId_FirstInitIdInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_PlayerLevelMaxLevel_hotfix;
    private LuaFunction m_set_ConfigableConstId_PlayerLevelMaxLevelInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ChatMessageMaxLength_hotfix;
    private LuaFunction m_set_ConfigableConstId_ChatMessageMaxLengthInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_WorldChatIntervalTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_WorldChatIntervalTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_AllowChatPlayerLevel_hotfix;
    private LuaFunction m_set_ConfigableConstId_AllowChatPlayerLevelInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_SystemSelectCardHeroBroadcast_hotfix;
    private LuaFunction m_set_ConfigableConstId_SystemSelectCardHeroBroadcastInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MagicStoneId_hotfix;
    private LuaFunction m_set_ConfigableConstId_MagicStoneIdInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_BuyEnergyCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_BuyEnergyCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroCommentMaxLength_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroCommentMaxLengthInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroCommentMaxNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroCommentMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_PlayerSingleHeroCommentMaxNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_PlayerSingleHeroCommentMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxPopularCommentEntry_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxPopularCommentEntryInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ThearchyTrailEverydayChallengeNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_ThearchyTrailEverydayChallengeNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_AnikiGymEverydayChallengeNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_AnikiGymEverydayChallengeNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_AllowArenaPlayerLevel_hotfix;
    private LuaFunction m_set_ConfigableConstId_AllowArenaPlayerLevelInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaGivenTicketMaxNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaGivenTicketMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_LevelRaidTicketID_hotfix;
    private LuaFunction m_set_ConfigableConstId_LevelRaidTicketIDInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaFightExpiredTimeInterval_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaFightExpiredTimeIntervalInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ProtagonistHeroID_hotfix;
    private LuaFunction m_set_ConfigableConstId_ProtagonistHeroIDInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_PlayerNameMaxLength_hotfix;
    private LuaFunction m_set_ConfigableConstId_PlayerNameMaxLengthInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaBattleReportMaxNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaBattleReportMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaVictoryPointsRewardMaxVictionaryPoints_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaVictoryPointsRewardMaxVictionaryPointsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaOneTimeGiveTicketsNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaOneTimeGiveTicketsNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaInitialPoints_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaInitialPointsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaOneTimeAttackUseTicketsNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaOneTimeAttackUseTicketsNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaAttackSuccessRandomDropRewardID_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaAttackSuccessRandomDropRewardIDInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaRealWeekSettleDeltaTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaRealWeekSettleDeltaTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaFindOpponentOneDirectionMaxOpponentNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaFindOpponentOneDirectionMaxOpponentNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ChangeNameCostNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_ChangeNameCostNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_PlayerInitialHeroInteractNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_PlayerInitialHeroInteractNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroInteractNumsRecoveryPeriod_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroInteractNumsRecoveryPeriodInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroIteractMaxNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroIteractMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroIntimateMaxValue_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroIntimateMaxValueInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_EnhanceEquipmentConsumeGoldPerExp_hotfix;
    private LuaFunction m_set_ConfigableConstId_EnhanceEquipmentConsumeGoldPerExpInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_LevelUpEquipmentStarConsumeGoldPerStar_hotfix;
    private LuaFunction m_set_ConfigableConstId_LevelUpEquipmentStarConsumeGoldPerStarInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_BuyArenaTicketCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_BuyArenaTicketCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroDungeonLevelMaxStar_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroDungeonLevelMaxStarInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaAddHeroExp_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaAddHeroExpInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaAddPlayerExp_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaAddPlayerExpInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_SystemSelectCardEquipmentBroadcast_hotfix;
    private LuaFunction m_set_ConfigableConstId_SystemSelectCardEquipmentBroadcastInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_AnikiGymLevelTicketID_hotfix;
    private LuaFunction m_set_ConfigableConstId_AnikiGymLevelTicketIDInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_TherachyTrialTicketID_hotfix;
    private LuaFunction m_set_ConfigableConstId_TherachyTrialTicketIDInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GoldRank_hotfix;
    private LuaFunction m_set_ConfigableConstId_GoldRankInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_CrystalRank_hotfix;
    private LuaFunction m_set_ConfigableConstId_CrystalRankInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_SkinTicketRank_hotfix;
    private LuaFunction m_set_ConfigableConstId_SkinTicketRankInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaHonourRank_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaHonourRankInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_EnergyRank_hotfix;
    private LuaFunction m_set_ConfigableConstId_EnergyRankInt32_hotfix;
    private LuaFunction m_get_EquipmentTableRankMax_hotfix;
    private LuaFunction m_set_EquipmentTableRankMaxInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_PlayerExpRank_hotfix;
    private LuaFunction m_set_ConfigableConstId_PlayerExpRankInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MemoryEssence_hotfix;
    private LuaFunction m_set_ConfigableConstId_MemoryEssenceInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MithralStoneRank_hotfix;
    private LuaFunction m_set_ConfigableConstId_MithralStoneRankInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_BrillianceMithralStoneRank_hotfix;
    private LuaFunction m_set_ConfigableConstId_BrillianceMithralStoneRankInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_FriendshipPointsRank_hotfix;
    private LuaFunction m_set_ConfigableConstId_FriendshipPointsRankInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ArenaTicketRank_hotfix;
    private LuaFunction m_set_ConfigableConstId_ArenaTicketRankInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_UserGuideRandomEventId_hotfix;
    private LuaFunction m_set_ConfigableConstId_UserGuideRandomEventIdInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_WriteSurveyPlayerLevel_hotfix;
    private LuaFunction m_set_ConfigableConstId_WriteSurveyPlayerLevelInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_AutoEquipmentDeltaTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_AutoEquipmentDeltaTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxLevelDanmakuEntryLength_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxLevelDanmakuEntryLengthInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxLevelDanmakuEntryNumsPerTurn_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxLevelDanmakuEntryNumsPerTurnInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_TeamRoomInviteDeltaTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_TeamRoomInviteDeltaTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_BusinessCardDescMaxLength_hotfix;
    private LuaFunction m_set_ConfigableConstId_BusinessCardDescMaxLengthInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_BusinessCardHeroMaxNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_BusinessCardHeroMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_SendFriendShipPointsEverytime_hotfix;
    private LuaFunction m_set_ConfigableConstId_SendFriendShipPointsEverytimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_SendFriendShipPointsMaxTimes_hotfix;
    private LuaFunction m_set_ConfigableConstId_SendFriendShipPointsMaxTimesInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ReceiveFriendShipPointsMaxTimes_hotfix;
    private LuaFunction m_set_ConfigableConstId_ReceiveFriendShipPointsMaxTimesInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_SearchUserByNameResultMax_hotfix;
    private LuaFunction m_set_ConfigableConstId_SearchUserByNameResultMaxInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxDomesticFriends_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxDomesticFriendsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxForeignFriends_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxForeignFriendsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxBlacklist_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxBlacklistInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxInvites_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxInvitesInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxSuggestedUsers_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxSuggestedUsersInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxLevelDiffMultiply_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxLevelDiffMultiplyInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_SuggestedUserLevelDiff_hotfix;
    private LuaFunction m_set_ConfigableConstId_SuggestedUserLevelDiffInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_InviteExpireSeconds_hotfix;
    private LuaFunction m_set_ConfigableConstId_InviteExpireSecondsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RecentContactsMaxCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_RecentContactsMaxCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroAssistantTaskHeroAssignMaxCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroAssistantTaskHeroAssignMaxCountInt32_hotfix;
    private LuaFunction m_get_BuyEnergyMaxNums_hotfix;
    private LuaFunction m_set_BuyEnergyMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxGroupsPerUser_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxGroupsPerUserInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxGroupMembers_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxGroupMembersInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_BattleInviteFriendsToPracticeMaxWaitInterval_hotfix;
    private LuaFunction m_set_ConfigableConstId_BattleInviteFriendsToPracticeMaxWaitIntervalInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_DayBonusNum_Aniki_hotfix;
    private LuaFunction m_set_ConfigableConstId_DayBonusNum_AnikiInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_DayBonusNum_ThearchyTrial_hotfix;
    private LuaFunction m_set_ConfigableConstId_DayBonusNum_ThearchyTrialInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_DayBonusNum_HeroTrain_hotfix;
    private LuaFunction m_set_ConfigableConstId_DayBonusNum_HeroTrainInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_DayBonusNum_MemoryCorridor_hotfix;
    private LuaFunction m_set_ConfigableConstId_DayBonusNum_MemoryCorridorInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_DayBonusNum_Crusade_hotfix;
    private LuaFunction m_set_ConfigableConstId_DayBonusNum_CrusadeInt32_hotfix;
    private LuaFunction m_get_BuyArenaTicketMaxNums_hotfix;
    private LuaFunction m_set_BuyArenaTicketMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_UseBoxItemMaxCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_UseBoxItemMaxCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ServerTolerateClientSpeedUpMaxNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_ServerTolerateClientSpeedUpMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ServerCheckClientHeartBeatReachMinPeriod_hotfix;
    private LuaFunction m_set_ConfigableConstId_ServerCheckClientHeartBeatReachMinPeriodInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ClientSendHeartBeatPeriod_hotfix;
    private LuaFunction m_set_ConfigableConstId_ClientSendHeartBeatPeriodInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPBattleStartTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPBattleStartTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPGoldBanHeroCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPGoldBanHeroCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPGoldCountdown_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPGoldCountdownInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPGoldHeroCount_Turn1_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPGoldHeroCount_Turn1Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPGoldHeroCount_Turn2_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPGoldHeroCount_Turn2Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPGoldHeroCount_Turn3_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPGoldHeroCount_Turn3Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RefluxBeginLevelLimit_hotfix;
    private LuaFunction m_set_ConfigableConstId_RefluxBeginLevelLimitInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPGoldHeroCount_Turn4_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPGoldHeroCount_Turn4Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPGoldHeroCount_Turn5_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPGoldHeroCount_Turn5Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPGoldHeroCount_Turn6_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPGoldHeroCount_Turn6Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPGoldHeroCount_Turn7_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPGoldHeroCount_Turn7Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPGoldProtectHeroCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPGoldProtectHeroCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPInitialScore_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPInitialScoreInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPMatchmakingExpectedTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPMatchmakingExpectedTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPMatchmakingExpectedTimeAdjust_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPMatchmakingExpectedTimeAdjustInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPMinRequiredLevel_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPMinRequiredLevelInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPSilverCountdown_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPSilverCountdownInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPSilverHeroCount_Turn1_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPSilverHeroCount_Turn1Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPSilverHeroCount_Turn2_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPSilverHeroCount_Turn2Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPSilverHeroCount_Turn3_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPSilverHeroCount_Turn3Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPSilverHeroCount_Turn4_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPSilverHeroCount_Turn4Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPSilverHeroCount_Turn5_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPSilverHeroCount_Turn5Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPSilverHeroCount_Turn6_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPSilverHeroCount_Turn6Int32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPTurnInterval_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPTurnIntervalInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPPromotionSucceedScoreBonus_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPPromotionSucceedScoreBonusInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPPromotionFailedScorePenalty_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPPromotionFailedScorePenaltyInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPScoreMax_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPScoreMaxInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_FriendPointsFightWithFriends_hotfix;
    private LuaFunction m_set_ConfigableConstId_FriendPointsFightWithFriendsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxFriendPointsFightWithFriendsEveryday_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxFriendPointsFightWithFriendsEverydayInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_BattleRoomPlayerReconnectTimeOutTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_BattleRoomPlayerReconnectTimeOutTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_TeamBattleRoomPlayerActionClientTimeOutTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_TeamBattleRoomPlayerActionClientTimeOutTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_PVPBattleRoomPlayerActionClientTimeOutTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_PVPBattleRoomPlayerActionClientTimeOutTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_TeamBattleRoomPlayerActionServerTimeOutTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_TeamBattleRoomPlayerActionServerTimeOutTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_PVPBattleRoomPlayerActionServerTimeOutTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_PVPBattleRoomPlayerActionServerTimeOutTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_TeamFriendShipPointRewardPerFriend_hotfix;
    private LuaFunction m_set_ConfigableConstId_TeamFriendShipPointRewardPerFriendInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_MaxEnchantSamePropertyNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_MaxEnchantSamePropertyNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_TeamInvitationTimeoutTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_TeamInvitationTimeoutTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_PVPInvitationTimeoutTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_PVPInvitationTimeoutTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_DecomposeEquipmentBackGoldPercent_hotfix;
    private LuaFunction m_set_ConfigableConstId_DecomposeEquipmentBackGoldPercentInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RecentContactsChatMaxCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_RecentContactsChatMaxCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RecentContactsTeamBattleMaxCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_RecentContactsTeamBattleMaxCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_DayBonusNum_CooperateBattle_hotfix;
    private LuaFunction m_set_ConfigableConstId_DayBonusNum_CooperateBattleInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroAssistantTaskSlotCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroAssistantTaskSlotCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ChatGroupNameMaxLength_hotfix;
    private LuaFunction m_set_ConfigableConstId_ChatGroupNameMaxLengthInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ChatGroupCreateMinUserCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_ChatGroupCreateMinUserCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_ChatGroupDisbandUserCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_ChatGroupDisbandUserCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_DefaultHeadFrameId_hotfix;
    private LuaFunction m_set_ConfigableConstId_DefaultHeadFrameIdInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RequestAppReviewInScenario_hotfix;
    private LuaFunction m_set_ConfigableConstId_RequestAppReviewInScenarioInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_IsRequestAppReviewOn_hotfix;
    private LuaFunction m_set_ConfigableConstId_IsRequestAppReviewOnInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GainMaximum_hotfix;
    private LuaFunction m_set_ConfigableConstId_GainMaximumInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_AlchemyMaxNum_hotfix;
    private LuaFunction m_set_ConfigableConstId_AlchemyMaxNumInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_PrivilegeItemRank_hotfix;
    private LuaFunction m_set_ConfigableConstId_PrivilegeItemRankInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_Battle_ClientCheckOnlinePeriod_hotfix;
    private LuaFunction m_set_ConfigableConstId_Battle_ClientCheckOnlinePeriodInt32_hotfix;
    private LuaFunction m_get_RealTimePVPInclusiveMinDan_hotfix;
    private LuaFunction m_set_RealTimePVPInclusiveMinDanInt32_hotfix;
    private LuaFunction m_get_RealTimePVPExclusiveMaxDan_hotfix;
    private LuaFunction m_set_RealTimePVPExclusiveMaxDanInt32_hotfix;
    private LuaFunction m_get_RealTimeArenaNewPlayerMatchCount_hotfix;
    private LuaFunction m_set_RealTimeArenaNewPlayerMatchCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPBotPlayDeadIntervalMin_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPBotPlayDeadIntervalMinInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPBotPlayDeadIntervalMax_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPBotPlayDeadIntervalMaxInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RealTimePVPBattleReportMaxNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_RealTimePVPBattleReportMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_NewUserAccumulatedMinValue_hotfix;
    private LuaFunction m_set_ConfigableConstId_NewUserAccumulatedMinValueInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_NewUserAccumulatedMaxValue_hotfix;
    private LuaFunction m_set_ConfigableConstId_NewUserAccumulatedMaxValueInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_OldUserAccumulatedMinValue_hotfix;
    private LuaFunction m_set_ConfigableConstId_OldUserAccumulatedMinValueInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_OldUserAccumulatedMaxValue_hotfix;
    private LuaFunction m_set_ConfigableConstId_OldUserAccumulatedMaxValueInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_BattleRoomPlayerActionCountdownBigNumberTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_BattleRoomPlayerActionCountdownBigNumberTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildCreateJoinLevel_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildCreateJoinLevelInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildCreateItemId_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildCreateItemIdInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildMemberCountMax_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildMemberCountMaxInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildMaxBattlePower_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildMaxBattlePowerInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildReJoinCoolDownTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildReJoinCoolDownTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildJoinApplicationPlayerCountMax_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildJoinApplicationPlayerCountMaxInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildInviteCountMax_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildInviteCountMaxInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildChatHistoryCount_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildChatHistoryCountInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildHiringDeclarationMaxLength_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildHiringDeclarationMaxLengthInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildVicePresidentMaxNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildVicePresidentMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildEliteMinTotalActivities_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildEliteMinTotalActivitiesInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RankListGuildNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_RankListGuildNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildVicePresidentCanUsurpTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildVicePresidentCanUsurpTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildEliteCanUsurpTime_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildEliteCanUsurpTimeInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildChangeNameCrystalCost_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildChangeNameCrystalCostInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildDailyMaxActivities_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildDailyMaxActivitiesInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildAnnouncementMaxLength_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildAnnouncementMaxLengthInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildWeeklyMaxActivities_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildWeeklyMaxActivitiesInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildNameMaxLength_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildNameMaxLengthInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildLogMaxNUms_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildLogMaxNUmsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildMedalRank_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildMedalRankInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_NewUserSecondAccumulatedMinValue_hotfix;
    private LuaFunction m_set_ConfigableConstId_NewUserSecondAccumulatedMinValueInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_NewUserSecondAccumulatedMaxValue_hotfix;
    private LuaFunction m_set_ConfigableConstId_NewUserSecondAccumulatedMaxValueInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_RandomStoreManualFlushMaxNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_RandomStoreManualFlushMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_HeroDungeonDailyChallengeMaxNums_hotfix;
    private LuaFunction m_set_ConfigableConstId_HeroDungeonDailyChallengeMaxNumsInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildMassiveCombatAvailableCountPerWeek_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildMassiveCombatAvailableCountPerWeekInt32_hotfix;
    private LuaFunction m_get_GuildMassiveCombatRandomHeroTagIds_hotfix;
    private LuaFunction m_set_GuildMassiveCombatRandomHeroTagIdsInt32be_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildMassiveCombatMinTitleToStart_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildMassiveCombatMinTitleToStartInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildMassiveCombatMinTitleToSurrender_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildMassiveCombatMinTitleToSurrenderInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_GuildMassiveCombatMaxHeroTagIdForStronghold_hotfix;
    private LuaFunction m_set_ConfigableConstId_GuildMassiveCombatMaxHeroTagIdForStrongholdInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_EternalShrineDailyChallengeCountMax_hotfix;
    private LuaFunction m_set_ConfigableConstId_EternalShrineDailyChallengeCountMaxInt32_hotfix;
    private LuaFunction m_get_ConfigableConstId_AppleSubscribeMonthCardCanRepeatlyBuy_hotfix;
    private LuaFunction m_set_ConfigableConstId_AppleSubscribeMonthCardCanRepeatlyBuyBoolean_hotfix;
    private LuaFunction m_get_RandomStoreData_hotfix;
    private LuaFunction m_set_RandomStoreDataRandomStoreData_hotfix;
    private LuaFunction m_get_RandomDropData_hotfix;
    private LuaFunction m_set_RandomDropDataRandomDropDataInfo_hotfix;
    private LuaFunction m_get_FixedStoreData_hotfix;
    private LuaFunction m_set_FixedStoreDataFixedStoreDataInfo_hotfix;
    private LuaFunction m_get_SignRewardData_hotfix;
    private LuaFunction m_set_SignRewardDataSignRewardDataInfo_hotfix;
    private LuaFunction m_get_SensitiveWords_hotfix;
    private LuaFunction m_set_SensitiveWordsSensitiveWords_hotfix;
    private LuaFunction m_get_MissionData_hotfix;
    private LuaFunction m_set_MissionDataMissionDataInfo_hotfix;
    private LuaFunction m_get_ThearchyTrialData_hotfix;
    private LuaFunction m_set_ThearchyTrialDataThearchyTrialDataInfo_hotfix;
    private LuaFunction m_get_EternalShrineData_hotfix;
    private LuaFunction m_set_EternalShrineDataEternalShrineDataInfo_hotfix;
    private LuaFunction m_get_AnikiGymData_hotfix;
    private LuaFunction m_set_AnikiGymDataAnikiGymDataInfo_hotfix;
    private LuaFunction m_get_MemoryCorridorData_hotfix;
    private LuaFunction m_set_MemoryCorridorDataMemoryCorridorDataInfo_hotfix;
    private LuaFunction m_get_HeroTrainningData_hotfix;
    private LuaFunction m_set_HeroTrainningDataHeroTrainningDataInfo_hotfix;
    private LuaFunction m_get_TicketId2TicketLimitInfo_hotfix;
    private LuaFunction m_set_TicketId2TicketLimitInfoDictionary`2_hotfix;
    private LuaFunction m_get_EnchantPropertyProbabilityInfos_hotfix;
    private LuaFunction m_set_EnchantPropertyProbabilityInfosDictionary`2_hotfix;
    private LuaFunction m_get_RealTimePVPAvailableTime_hotfix;
    private LuaFunction m_set_RealTimePVPAvailableTimeRealTimePVPAvailableTimebe_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientConfigDataLoader()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public override bool StartInitializeFromAsset(Action<bool> onEnd, out int initLoadDataCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override HashSet<string> GetAllInitLoadConfigDataAssetPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override Dictionary<string, List<ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo>> GetAllLazyLoadConfigDataAssetPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator OnInitLoadFromAssetEndWorker(
      int totalWorkerCount,
      int workerId,
      int loadCountForSingleYield,
      Action<bool> onEnd,
      List<string> skipTypeList = null,
      List<string> filterTypeList = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void AddConfigDataItemForLuaDummyType(string typeName, DummyType dataItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<string> GetAllInitLoadConfigDataTypeNameForLuaDummy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnLazyLoadFromAssetEnd(
      string configDataName,
      string configAssetName,
      UnityEngine.Object lasset,
      Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string GetLazyLoadConfigAssetNameByKey(string configDataName, int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataActivityCardPoolGroupInfo GetConfigDataActivityCardPoolGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataActivityCardPoolGroupInfo>> GetAllConfigDataActivityCardPoolGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataActivityCardPoolGroupInfo>>) this.m_ConfigDataActivityCardPoolGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataActivityCardPoolGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAnikiGymInfo GetConfigDataAnikiGymInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataAnikiGymInfo>> GetAllConfigDataAnikiGymInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataAnikiGymInfo>>) this.m_ConfigDataAnikiGymInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataAnikiGymInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAnikiLevelInfo GetConfigDataAnikiLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataAnikiLevelInfo>> GetAllConfigDataAnikiLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataAnikiLevelInfo>>) this.m_ConfigDataAnikiLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataAnikiLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaBattleInfo GetConfigDataArenaBattleInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataArenaBattleInfo>> GetAllConfigDataArenaBattleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaBattleInfo>>) this.m_ConfigDataArenaBattleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaDefendRuleInfo GetConfigDataArenaDefendRuleInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataArenaDefendRuleInfo>> GetAllConfigDataArenaDefendRuleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaDefendRuleInfo>>) this.m_ConfigDataArenaDefendRuleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaDefendRuleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaLevelInfo GetConfigDataArenaLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataArenaLevelInfo>> GetAllConfigDataArenaLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaLevelInfo>>) this.m_ConfigDataArenaLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaOpponentPointZoneInfo GetConfigDataArenaOpponentPointZoneInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataArenaOpponentPointZoneInfo>> GetAllConfigDataArenaOpponentPointZoneInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaOpponentPointZoneInfo>>) this.m_ConfigDataArenaOpponentPointZoneInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaOpponentPointZoneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaRankRewardInfo GetConfigDataArenaRankRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataArenaRankRewardInfo>> GetAllConfigDataArenaRankRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaRankRewardInfo>>) this.m_ConfigDataArenaRankRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaRankRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaRobotInfo GetConfigDataArenaRobotInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataArenaRobotInfo>> GetAllConfigDataArenaRobotInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaRobotInfo>>) this.m_ConfigDataArenaRobotInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaRobotInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaSettleTimeInfo GetConfigDataArenaSettleTimeInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataArenaSettleTimeInfo>> GetAllConfigDataArenaSettleTimeInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaSettleTimeInfo>>) this.m_ConfigDataArenaSettleTimeInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaSettleTimeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaVictoryPointsRewardInfo GetConfigDataArenaVictoryPointsRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataArenaVictoryPointsRewardInfo>> GetAllConfigDataArenaVictoryPointsRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaVictoryPointsRewardInfo>>) this.m_ConfigDataArenaVictoryPointsRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaVictoryPointsRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArmyInfo GetConfigDataArmyInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataArmyInfo>> GetAllConfigDataArmyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArmyInfo>>) this.m_ConfigDataArmyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArmyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArmyRelation GetConfigDataArmyRelation(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataArmyRelation>> GetAllConfigDataArmyRelation()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArmyRelation>>) this.m_ConfigDataArmyRelationData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAssetReplaceInfo GetConfigDataAssetReplaceInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataAssetReplaceInfo>> GetAllConfigDataAssetReplaceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataAssetReplaceInfo>>) this.m_ConfigDataAssetReplaceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataAssetReplaceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBanditInfo GetConfigDataBanditInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBanditInfo>> GetAllConfigDataBanditInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBanditInfo>>) this.m_ConfigDataBanditInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBanditInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleAchievementInfo GetConfigDataBattleAchievementInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattleAchievementInfo>> GetAllConfigDataBattleAchievementInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleAchievementInfo>>) this.m_ConfigDataBattleAchievementInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleAchievementInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleAchievementRelatedInfo GetConfigDataBattleAchievementRelatedInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattleAchievementRelatedInfo>> GetAllConfigDataBattleAchievementRelatedInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleAchievementRelatedInfo>>) this.m_ConfigDataBattleAchievementRelatedInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleAchievementRelatedInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleDialogInfo GetConfigDataBattleDialogInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattleDialogInfo>> GetAllConfigDataBattleDialogInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleDialogInfo>>) this.m_ConfigDataBattleDialogInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleEventActionInfo GetConfigDataBattleEventActionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattleEventActionInfo>> GetAllConfigDataBattleEventActionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleEventActionInfo>>) this.m_ConfigDataBattleEventActionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleEventActionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleEventTriggerInfo GetConfigDataBattleEventTriggerInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattleEventTriggerInfo>> GetAllConfigDataBattleEventTriggerInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleEventTriggerInfo>>) this.m_ConfigDataBattleEventTriggerInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleEventTriggerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattlefieldInfo GetConfigDataBattlefieldInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattlefieldInfo>> GetAllConfigDataBattlefieldInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattlefieldInfo>>) this.m_ConfigDataBattlefieldInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattlefieldInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleInfo GetConfigDataBattleInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattleInfo>> GetAllConfigDataBattleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleInfo>>) this.m_ConfigDataBattleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleLoseConditionInfo GetConfigDataBattleLoseConditionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattleLoseConditionInfo>> GetAllConfigDataBattleLoseConditionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleLoseConditionInfo>>) this.m_ConfigDataBattleLoseConditionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleLoseConditionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattlePerformInfo GetConfigDataBattlePerformInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattlePerformInfo>> GetAllConfigDataBattlePerformInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattlePerformInfo>>) this.m_ConfigDataBattlePerformInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattlePerformInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleRandomArmyInfo GetConfigDataBattleRandomArmyInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattleRandomArmyInfo>> GetAllConfigDataBattleRandomArmyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleRandomArmyInfo>>) this.m_ConfigDataBattleRandomArmyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleRandomArmyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleRandomTalentInfo GetConfigDataBattleRandomTalentInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattleRandomTalentInfo>> GetAllConfigDataBattleRandomTalentInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleRandomTalentInfo>>) this.m_ConfigDataBattleRandomTalentInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleRandomTalentInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleTreasureInfo GetConfigDataBattleTreasureInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattleTreasureInfo>> GetAllConfigDataBattleTreasureInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleTreasureInfo>>) this.m_ConfigDataBattleTreasureInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleTreasureInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleWinConditionInfo GetConfigDataBattleWinConditionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBattleWinConditionInfo>> GetAllConfigDataBattleWinConditionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleWinConditionInfo>>) this.m_ConfigDataBattleWinConditionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleWinConditionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBehavior GetConfigDataBehavior(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBehavior>> GetAllConfigDataBehavior()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBehavior>>) this.m_ConfigDataBehaviorData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBehavior()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBehaviorChangeRule GetConfigDataBehaviorChangeRule(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBehaviorChangeRule>> GetAllConfigDataBehaviorChangeRule()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBehaviorChangeRule>>) this.m_ConfigDataBehaviorChangeRuleData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBehaviorChangeRule()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBigExpressionInfo GetConfigDataBigExpressionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBigExpressionInfo>> GetAllConfigDataBigExpressionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBigExpressionInfo>>) this.m_ConfigDataBigExpressionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBigExpressionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBuffInfo GetConfigDataBuffInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBuffInfo>> GetAllConfigDataBuffInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBuffInfo>>) this.m_ConfigDataBuffInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBuffInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBuyArenaTicketInfo GetConfigDataBuyArenaTicketInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBuyArenaTicketInfo>> GetAllConfigDataBuyArenaTicketInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBuyArenaTicketInfo>>) this.m_ConfigDataBuyArenaTicketInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBuyArenaTicketInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBuyEnergyInfo GetConfigDataBuyEnergyInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataBuyEnergyInfo>> GetAllConfigDataBuyEnergyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBuyEnergyInfo>>) this.m_ConfigDataBuyEnergyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBuyEnergyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCardPoolGroupInfo GetConfigDataCardPoolGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCardPoolGroupInfo>> GetAllConfigDataCardPoolGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCardPoolGroupInfo>>) this.m_ConfigDataCardPoolGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCardPoolGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCardPoolInfo GetConfigDataCardPoolInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCardPoolInfo>> GetAllConfigDataCardPoolInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCardPoolInfo>>) this.m_ConfigDataCardPoolInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCardPoolInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataChallengeLevelInfo GetConfigDataChallengeLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataChallengeLevelInfo>> GetAllConfigDataChallengeLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataChallengeLevelInfo>>) this.m_ConfigDataChallengeLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataChallengeLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageInfo GetConfigDataCharImageInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCharImageInfo>> GetAllConfigDataCharImageInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCharImageInfo>>) this.m_ConfigDataCharImageInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCharImageInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageSkinResourceInfo GetConfigDataCharImageSkinResourceInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCharImageSkinResourceInfo>> GetAllConfigDataCharImageSkinResourceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCharImageSkinResourceInfo>>) this.m_ConfigDataCharImageSkinResourceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCharImageSkinResourceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityChallengeLevelInfo GetConfigDataCollectionActivityChallengeLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityChallengeLevelInfo>> GetAllConfigDataCollectionActivityChallengeLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityChallengeLevelInfo>>) this.m_ConfigDataCollectionActivityChallengeLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityChallengeLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityCurrencyItemExtInfo GetConfigDataCollectionActivityCurrencyItemExtInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityCurrencyItemExtInfo>> GetAllConfigDataCollectionActivityCurrencyItemExtInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityCurrencyItemExtInfo>>) this.m_ConfigDataCollectionActivityCurrencyItemExtInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityCurrencyItemExtInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityExchangeTableInfo GetConfigDataCollectionActivityExchangeTableInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityExchangeTableInfo>> GetAllConfigDataCollectionActivityExchangeTableInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityExchangeTableInfo>>) this.m_ConfigDataCollectionActivityExchangeTableInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityExchangeTableInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo GetConfigDataCollectionActivityInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityInfo>> GetAllConfigDataCollectionActivityInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityInfo>>) this.m_ConfigDataCollectionActivityInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityLootLevelInfo GetConfigDataCollectionActivityLootLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityLootLevelInfo>> GetAllConfigDataCollectionActivityLootLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityLootLevelInfo>>) this.m_ConfigDataCollectionActivityLootLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityLootLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityMapInfo GetConfigDataCollectionActivityMapInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityMapInfo>> GetAllConfigDataCollectionActivityMapInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityMapInfo>>) this.m_ConfigDataCollectionActivityMapInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityMapInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScenarioLevelInfo GetConfigDataCollectionActivityScenarioLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityScenarioLevelInfo>> GetAllConfigDataCollectionActivityScenarioLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityScenarioLevelInfo>>) this.m_ConfigDataCollectionActivityScenarioLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityScenarioLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityWaypointInfo GetConfigDataCollectionActivityWaypointInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityWaypointInfo>> GetAllConfigDataCollectionActivityWaypointInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityWaypointInfo>>) this.m_ConfigDataCollectionActivityWaypointInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataConfessionDialogInfo GetConfigDataConfessionDialogInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataConfessionDialogInfo>> GetAllConfigDataConfessionDialogInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataConfessionDialogInfo>>) this.m_ConfigDataConfessionDialogInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataConfessionDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataConfessionLetterInfo GetConfigDataConfessionLetterInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataConfessionLetterInfo>> GetAllConfigDataConfessionLetterInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataConfessionLetterInfo>>) this.m_ConfigDataConfessionLetterInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataConfessionLetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataConfigableConst GetConfigDataConfigableConst(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataConfigableConst>> GetAllConfigDataConfigableConst()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataConfigableConst>>) this.m_ConfigDataConfigableConstData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataConfigableConst()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataConfigIDRangeInfo GetConfigDataConfigIDRangeInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataConfigIDRangeInfo>> GetAllConfigDataConfigIDRangeInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataConfigIDRangeInfo>>) this.m_ConfigDataConfigIDRangeInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataConfigIDRangeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCooperateBattleInfo GetConfigDataCooperateBattleInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCooperateBattleInfo>> GetAllConfigDataCooperateBattleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCooperateBattleInfo>>) this.m_ConfigDataCooperateBattleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCooperateBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCooperateBattleLevelInfo GetConfigDataCooperateBattleLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCooperateBattleLevelInfo>> GetAllConfigDataCooperateBattleLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCooperateBattleLevelInfo>>) this.m_ConfigDataCooperateBattleLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCooperateBattleLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCrystalCardPoolGroupInfo GetConfigDataCrystalCardPoolGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCrystalCardPoolGroupInfo>> GetAllConfigDataCrystalCardPoolGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCrystalCardPoolGroupInfo>>) this.m_ConfigDataCrystalCardPoolGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCrystalCardPoolGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCutsceneInfo GetConfigDataCutsceneInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataCutsceneInfo>> GetAllConfigDataCutsceneInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCutsceneInfo>>) this.m_ConfigDataCutsceneInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCutsceneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataDailyPushNotification GetConfigDataDailyPushNotification(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataDailyPushNotification>> GetAllConfigDataDailyPushNotification()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataDailyPushNotification>>) this.m_ConfigDataDailyPushNotificationData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataDailyPushNotification()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataDeviceSetting GetConfigDataDeviceSetting(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataDeviceSetting>> GetAllConfigDataDeviceSetting()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataDeviceSetting>>) this.m_ConfigDataDeviceSettingData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataDeviceSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataDialogInfo GetConfigDataDialogInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataDialogInfo>> GetAllConfigDataDialogInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataDialogInfo>>) this.m_ConfigDataDialogInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEnchantStoneInfo GetConfigDataEnchantStoneInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataEnchantStoneInfo>> GetAllConfigDataEnchantStoneInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEnchantStoneInfo>>) this.m_ConfigDataEnchantStoneInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEnchantStoneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEnchantTemplateInfo GetConfigDataEnchantTemplateInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataEnchantTemplateInfo>> GetAllConfigDataEnchantTemplateInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEnchantTemplateInfo>>) this.m_ConfigDataEnchantTemplateInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEnchantTemplateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEquipmentInfo GetConfigDataEquipmentInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataEquipmentInfo>> GetAllConfigDataEquipmentInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEquipmentInfo>>) this.m_ConfigDataEquipmentInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEquipmentInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEquipmentLevelInfo GetConfigDataEquipmentLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataEquipmentLevelInfo>> GetAllConfigDataEquipmentLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEquipmentLevelInfo>>) this.m_ConfigDataEquipmentLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEquipmentLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEquipmentLevelLimitInfo GetConfigDataEquipmentLevelLimitInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataEquipmentLevelLimitInfo>> GetAllConfigDataEquipmentLevelLimitInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEquipmentLevelLimitInfo>>) this.m_ConfigDataEquipmentLevelLimitInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEquipmentLevelLimitInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataErrorCodeStringTable GetConfigDataErrorCodeStringTable(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataErrorCodeStringTable>> GetAllConfigDataErrorCodeStringTable()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataErrorCodeStringTable>>) this.m_ConfigDataErrorCodeStringTableData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataErrorCodeStringTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEternalShrineInfo GetConfigDataEternalShrineInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataEternalShrineInfo>> GetAllConfigDataEternalShrineInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEternalShrineInfo>>) this.m_ConfigDataEternalShrineInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEternalShrineInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEternalShrineLevelInfo GetConfigDataEternalShrineLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataEternalShrineLevelInfo>> GetAllConfigDataEternalShrineLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEternalShrineLevelInfo>>) this.m_ConfigDataEternalShrineLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEternalShrineLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEventInfo GetConfigDataEventInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataEventInfo>> GetAllConfigDataEventInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEventInfo>>) this.m_ConfigDataEventInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEventInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataExplanationInfo GetConfigDataExplanationInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataExplanationInfo>> GetAllConfigDataExplanationInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataExplanationInfo>>) this.m_ConfigDataExplanationInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataExplanationInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataFixedStoreItemInfo GetConfigDataFixedStoreItemInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataFixedStoreItemInfo>> GetAllConfigDataFixedStoreItemInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataFixedStoreItemInfo>>) this.m_ConfigDataFixedStoreItemInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataFixedStoreItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataFlyObjectInfo GetConfigDataFlyObjectInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataFlyObjectInfo>> GetAllConfigDataFlyObjectInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataFlyObjectInfo>>) this.m_ConfigDataFlyObjectInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataFlyObjectInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataFreeCardPoolGroupInfo GetConfigDataFreeCardPoolGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataFreeCardPoolGroupInfo>> GetAllConfigDataFreeCardPoolGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataFreeCardPoolGroupInfo>>) this.m_ConfigDataFreeCardPoolGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataFreeCardPoolGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataFxFlipInfo GetConfigDataFxFlipInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataFxFlipInfo>> GetAllConfigDataFxFlipInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataFxFlipInfo>>) this.m_ConfigDataFxFlipInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataFxFlipInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGameFunctionOpenInfo GetConfigDataGameFunctionOpenInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataGameFunctionOpenInfo>> GetAllConfigDataGameFunctionOpenInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGameFunctionOpenInfo>>) this.m_ConfigDataGameFunctionOpenInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGameFunctionOpenInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGamePlayTeamTypeInfo GetConfigDataGamePlayTeamTypeInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataGamePlayTeamTypeInfo>> GetAllConfigDataGamePlayTeamTypeInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGamePlayTeamTypeInfo>>) this.m_ConfigDataGamePlayTeamTypeInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGamePlayTeamTypeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGiftCDKeyInfo GetConfigDataGiftCDKeyInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataGiftCDKeyInfo>> GetAllConfigDataGiftCDKeyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGiftCDKeyInfo>>) this.m_ConfigDataGiftCDKeyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGiftCDKeyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGiftStoreItemInfo GetConfigDataGiftStoreItemInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataGiftStoreItemInfo>> GetAllConfigDataGiftStoreItemInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGiftStoreItemInfo>>) this.m_ConfigDataGiftStoreItemInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGiftStoreItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGoddessDialogInfo GetConfigDataGoddessDialogInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataGoddessDialogInfo>> GetAllConfigDataGoddessDialogInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGoddessDialogInfo>>) this.m_ConfigDataGoddessDialogInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGoddessDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGroupBehavior GetConfigDataGroupBehavior(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataGroupBehavior>> GetAllConfigDataGroupBehavior()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGroupBehavior>>) this.m_ConfigDataGroupBehaviorData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGroupBehavior()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatDifficultyInfo GetConfigDataGuildMassiveCombatDifficultyInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatDifficultyInfo>> GetAllConfigDataGuildMassiveCombatDifficultyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatDifficultyInfo>>) this.m_ConfigDataGuildMassiveCombatDifficultyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGuildMassiveCombatDifficultyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo GetConfigDataGuildMassiveCombatIndividualPointsRewardsInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo>> GetAllConfigDataGuildMassiveCombatIndividualPointsRewardsInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo>>) this.m_ConfigDataGuildMassiveCombatIndividualPointsRewardsInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGuildMassiveCombatIndividualPointsRewardsInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatLevelInfo GetConfigDataGuildMassiveCombatLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatLevelInfo>> GetAllConfigDataGuildMassiveCombatLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatLevelInfo>>) this.m_ConfigDataGuildMassiveCombatLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGuildMassiveCombatLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatRewardsInfo GetConfigDataGuildMassiveCombatRewardsInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatRewardsInfo>> GetAllConfigDataGuildMassiveCombatRewardsInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatRewardsInfo>>) this.m_ConfigDataGuildMassiveCombatRewardsInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGuildMassiveCombatRewardsInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatStrongholdInfo GetConfigDataGuildMassiveCombatStrongholdInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatStrongholdInfo>> GetAllConfigDataGuildMassiveCombatStrongholdInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatStrongholdInfo>>) this.m_ConfigDataGuildMassiveCombatStrongholdInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGuildMassiveCombatStrongholdInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeadFrameInfo GetConfigDataHeadFrameInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeadFrameInfo>> GetAllConfigDataHeadFrameInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeadFrameInfo>>) this.m_ConfigDataHeadFrameInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeadFrameInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskGeneralInfo GetConfigDataHeroAssistantTaskGeneralInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskGeneralInfo>> GetAllConfigDataHeroAssistantTaskGeneralInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskGeneralInfo>>) this.m_ConfigDataHeroAssistantTaskGeneralInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroAssistantTaskGeneralInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskInfo GetConfigDataHeroAssistantTaskInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskInfo>> GetAllConfigDataHeroAssistantTaskInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskInfo>>) this.m_ConfigDataHeroAssistantTaskInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroAssistantTaskInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskScheduleInfo GetConfigDataHeroAssistantTaskScheduleInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskScheduleInfo>> GetAllConfigDataHeroAssistantTaskScheduleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskScheduleInfo>>) this.m_ConfigDataHeroAssistantTaskScheduleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroAssistantTaskScheduleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroBiographyInfo GetConfigDataHeroBiographyInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroBiographyInfo>> GetAllConfigDataHeroBiographyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroBiographyInfo>>) this.m_ConfigDataHeroBiographyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroBiographyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroConfessionInfo GetConfigDataHeroConfessionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroConfessionInfo>> GetAllConfigDataHeroConfessionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroConfessionInfo>>) this.m_ConfigDataHeroConfessionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroConfessionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroDungeonLevelInfo GetConfigDataHeroDungeonLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroDungeonLevelInfo>> GetAllConfigDataHeroDungeonLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroDungeonLevelInfo>>) this.m_ConfigDataHeroDungeonLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroDungeonLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroFavorabilityLevelInfo GetConfigDataHeroFavorabilityLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroFavorabilityLevelInfo>> GetAllConfigDataHeroFavorabilityLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroFavorabilityLevelInfo>>) this.m_ConfigDataHeroFavorabilityLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroFavorabilityLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroFetterInfo GetConfigDataHeroFetterInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroFetterInfo>> GetAllConfigDataHeroFetterInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroFetterInfo>>) this.m_ConfigDataHeroFetterInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroFetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroHeartFetterInfo GetConfigDataHeroHeartFetterInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroHeartFetterInfo>> GetAllConfigDataHeroHeartFetterInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroHeartFetterInfo>>) this.m_ConfigDataHeroHeartFetterInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroHeartFetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInfo GetConfigDataHeroInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroInfo>> GetAllConfigDataHeroInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroInfo>>) this.m_ConfigDataHeroInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInformationInfo GetConfigDataHeroInformationInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroInformationInfo>> GetAllConfigDataHeroInformationInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroInformationInfo>>) this.m_ConfigDataHeroInformationInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroInformationInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInteractionInfo GetConfigDataHeroInteractionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroInteractionInfo>> GetAllConfigDataHeroInteractionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroInteractionInfo>>) this.m_ConfigDataHeroInteractionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroInteractionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroLevelInfo GetConfigDataHeroLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroLevelInfo>> GetAllConfigDataHeroLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroLevelInfo>>) this.m_ConfigDataHeroLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPerformanceInfo GetConfigDataHeroPerformanceInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroPerformanceInfo>> GetAllConfigDataHeroPerformanceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroPerformanceInfo>>) this.m_ConfigDataHeroPerformanceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroPerformanceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPerformanceWordInfo GetConfigDataHeroPerformanceWordInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroPerformanceWordInfo>> GetAllConfigDataHeroPerformanceWordInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroPerformanceWordInfo>>) this.m_ConfigDataHeroPerformanceWordInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroPerformanceWordInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPhantomInfo GetConfigDataHeroPhantomInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroPhantomInfo>> GetAllConfigDataHeroPhantomInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroPhantomInfo>>) this.m_ConfigDataHeroPhantomInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroPhantomInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPhantomLevelInfo GetConfigDataHeroPhantomLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroPhantomLevelInfo>> GetAllConfigDataHeroPhantomLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroPhantomLevelInfo>>) this.m_ConfigDataHeroPhantomLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroPhantomLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroSkinInfo GetConfigDataHeroSkinInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroSkinInfo>> GetAllConfigDataHeroSkinInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroSkinInfo>>) this.m_ConfigDataHeroSkinInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroSkinInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroSkinSelfSelectedBoxInfo GetConfigDataHeroSkinSelfSelectedBoxInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroSkinSelfSelectedBoxInfo>> GetAllConfigDataHeroSkinSelfSelectedBoxInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroSkinSelfSelectedBoxInfo>>) this.m_ConfigDataHeroSkinSelfSelectedBoxInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroSkinSelfSelectedBoxInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroStarInfo GetConfigDataHeroStarInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroStarInfo>> GetAllConfigDataHeroStarInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroStarInfo>>) this.m_ConfigDataHeroStarInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroStarInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroTagInfo GetConfigDataHeroTagInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroTagInfo>> GetAllConfigDataHeroTagInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroTagInfo>>) this.m_ConfigDataHeroTagInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroTagInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroTrainningInfo GetConfigDataHeroTrainningInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroTrainningInfo>> GetAllConfigDataHeroTrainningInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroTrainningInfo>>) this.m_ConfigDataHeroTrainningInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroTrainningInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroTrainningLevelInfo GetConfigDataHeroTrainningLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataHeroTrainningLevelInfo>> GetAllConfigDataHeroTrainningLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroTrainningLevelInfo>>) this.m_ConfigDataHeroTrainningLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroTrainningLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataInitInfo GetConfigDataInitInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataInitInfo>> GetAllConfigDataInitInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataInitInfo>>) this.m_ConfigDataInitInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataInitInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataItemInfo GetConfigDataItemInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataItemInfo>> GetAllConfigDataItemInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataItemInfo>>) this.m_ConfigDataItemInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobConnectionInfo GetConfigDataJobConnectionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataJobConnectionInfo>> GetAllConfigDataJobConnectionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataJobConnectionInfo>>) this.m_ConfigDataJobConnectionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataJobConnectionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobInfo GetConfigDataJobInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataJobInfo>> GetAllConfigDataJobInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataJobInfo>>) this.m_ConfigDataJobInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataJobInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobLevelInfo GetConfigDataJobLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataJobLevelInfo>> GetAllConfigDataJobLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataJobLevelInfo>>) this.m_ConfigDataJobLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataJobLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobMaterialInfo GetConfigDataJobMaterialInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataJobMaterialInfo>> GetAllConfigDataJobMaterialInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataJobMaterialInfo>>) this.m_ConfigDataJobMaterialInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataJobMaterialInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobUnlockConditionInfo GetConfigDataJobUnlockConditionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataJobUnlockConditionInfo>> GetAllConfigDataJobUnlockConditionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataJobUnlockConditionInfo>>) this.m_ConfigDataJobUnlockConditionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataJobUnlockConditionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataLanguageDataInfo GetConfigDataLanguageDataInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataLanguageDataInfo>> GetAllConfigDataLanguageDataInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataLanguageDataInfo>>) this.m_ConfigDataLanguageDataInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataLanguageDataInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMailInfo GetConfigDataMailInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataMailInfo>> GetAllConfigDataMailInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMailInfo>>) this.m_ConfigDataMailInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMailInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMemoryCorridorInfo GetConfigDataMemoryCorridorInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataMemoryCorridorInfo>> GetAllConfigDataMemoryCorridorInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMemoryCorridorInfo>>) this.m_ConfigDataMemoryCorridorInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMemoryCorridorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMemoryCorridorLevelInfo GetConfigDataMemoryCorridorLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataMemoryCorridorLevelInfo>> GetAllConfigDataMemoryCorridorLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMemoryCorridorLevelInfo>>) this.m_ConfigDataMemoryCorridorLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMemoryCorridorLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMissionExtNoviceInfo GetConfigDataMissionExtNoviceInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataMissionExtNoviceInfo>> GetAllConfigDataMissionExtNoviceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMissionExtNoviceInfo>>) this.m_ConfigDataMissionExtNoviceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMissionExtNoviceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMissionExtRefluxInfo GetConfigDataMissionExtRefluxInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataMissionExtRefluxInfo>> GetAllConfigDataMissionExtRefluxInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMissionExtRefluxInfo>>) this.m_ConfigDataMissionExtRefluxInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMissionExtRefluxInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMissionInfo GetConfigDataMissionInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataMissionInfo>> GetAllConfigDataMissionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMissionInfo>>) this.m_ConfigDataMissionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMissionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo GetConfigDataModelSkinResourceInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataModelSkinResourceInfo>> GetAllConfigDataModelSkinResourceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataModelSkinResourceInfo>>) this.m_ConfigDataModelSkinResourceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataModelSkinResourceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMonthCardInfo GetConfigDataMonthCardInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataMonthCardInfo>> GetAllConfigDataMonthCardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMonthCardInfo>>) this.m_ConfigDataMonthCardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMonthCardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataNoviceRewardInfo GetConfigDataNoviceRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataNoviceRewardInfo>> GetAllConfigDataNoviceRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataNoviceRewardInfo>>) this.m_ConfigDataNoviceRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataNoviceRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataOperationalActivityInfo GetConfigDataOperationalActivityInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataOperationalActivityInfo>> GetAllConfigDataOperationalActivityInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataOperationalActivityInfo>>) this.m_ConfigDataOperationalActivityInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataOperationalActivityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataOperationalActivityItemGroupInfo GetConfigDataOperationalActivityItemGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataOperationalActivityItemGroupInfo>> GetAllConfigDataOperationalActivityItemGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataOperationalActivityItemGroupInfo>>) this.m_ConfigDataOperationalActivityItemGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataOperationalActivityItemGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPerformanceInfo GetConfigDataPerformanceInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataPerformanceInfo>> GetAllConfigDataPerformanceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPerformanceInfo>>) this.m_ConfigDataPerformanceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPerformanceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPlayerLevelInfo GetConfigDataPlayerLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataPlayerLevelInfo>> GetAllConfigDataPlayerLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPlayerLevelInfo>>) this.m_ConfigDataPlayerLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPlayerLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPrefabStateInfo GetConfigDataPrefabStateInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataPrefabStateInfo>> GetAllConfigDataPrefabStateInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPrefabStateInfo>>) this.m_ConfigDataPrefabStateInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPrefabStateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPropertyModifyInfo GetConfigDataPropertyModifyInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataPropertyModifyInfo>> GetAllConfigDataPropertyModifyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPropertyModifyInfo>>) this.m_ConfigDataPropertyModifyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPropertyModifyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataProtagonistInfo GetConfigDataProtagonistInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataProtagonistInfo>> GetAllConfigDataProtagonistInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataProtagonistInfo>>) this.m_ConfigDataProtagonistInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataProtagonistInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPVPBattleInfo GetConfigDataPVPBattleInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataPVPBattleInfo>> GetAllConfigDataPVPBattleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPVPBattleInfo>>) this.m_ConfigDataPVPBattleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPVPBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRafflePoolInfo GetConfigDataRafflePoolInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRafflePoolInfo>> GetAllConfigDataRafflePoolInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRafflePoolInfo>>) this.m_ConfigDataRafflePoolInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRafflePoolInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomBoxInfo GetConfigDataRandomBoxInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRandomBoxInfo>> GetAllConfigDataRandomBoxInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomBoxInfo>>) this.m_ConfigDataRandomBoxInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomBoxInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomDropRewardInfo GetConfigDataRandomDropRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRandomDropRewardInfo>> GetAllConfigDataRandomDropRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomDropRewardInfo>>) this.m_ConfigDataRandomDropRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomDropRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomNameHead GetConfigDataRandomNameHead(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRandomNameHead>> GetAllConfigDataRandomNameHead()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomNameHead>>) this.m_ConfigDataRandomNameHeadData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomNameHead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomNameMiddle GetConfigDataRandomNameMiddle(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRandomNameMiddle>> GetAllConfigDataRandomNameMiddle()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomNameMiddle>>) this.m_ConfigDataRandomNameMiddleData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomNameMiddle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomNameTail GetConfigDataRandomNameTail(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRandomNameTail>> GetAllConfigDataRandomNameTail()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomNameTail>>) this.m_ConfigDataRandomNameTailData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomNameTail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomStoreInfo GetConfigDataRandomStoreInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRandomStoreInfo>> GetAllConfigDataRandomStoreInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomStoreInfo>>) this.m_ConfigDataRandomStoreInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomStoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomStoreItemInfo GetConfigDataRandomStoreItemInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRandomStoreItemInfo>> GetAllConfigDataRandomStoreItemInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomStoreItemInfo>>) this.m_ConfigDataRandomStoreItemInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomStoreItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomStoreLevelZoneInfo GetConfigDataRandomStoreLevelZoneInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRandomStoreLevelZoneInfo>> GetAllConfigDataRandomStoreLevelZoneInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomStoreLevelZoneInfo>>) this.m_ConfigDataRandomStoreLevelZoneInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomStoreLevelZoneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRankInfo GetConfigDataRankInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRankInfo>> GetAllConfigDataRankInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRankInfo>>) this.m_ConfigDataRankInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRankInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPBattleInfo GetConfigDataRealTimePVPBattleInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPBattleInfo>> GetAllConfigDataRealTimePVPBattleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPBattleInfo>>) this.m_ConfigDataRealTimePVPBattleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPDanInfo GetConfigDataRealTimePVPDanInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPDanInfo>> GetAllConfigDataRealTimePVPDanInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPDanInfo>>) this.m_ConfigDataRealTimePVPDanInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPDanInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPDanRewardInfo GetConfigDataRealTimePVPDanRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPDanRewardInfo>> GetAllConfigDataRealTimePVPDanRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPDanRewardInfo>>) this.m_ConfigDataRealTimePVPDanRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPDanRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPLocalRankingRewardInfo GetConfigDataRealTimePVPLocalRankingRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPLocalRankingRewardInfo>> GetAllConfigDataRealTimePVPLocalRankingRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPLocalRankingRewardInfo>>) this.m_ConfigDataRealTimePVPLocalRankingRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPLocalRankingRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPNoviceMatchmakingInfo GetConfigDataRealTimePVPNoviceMatchmakingInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPNoviceMatchmakingInfo>> GetAllConfigDataRealTimePVPNoviceMatchmakingInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPNoviceMatchmakingInfo>>) this.m_ConfigDataRealTimePVPNoviceMatchmakingInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPNoviceMatchmakingInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPRankingRewardInfo GetConfigDataRealTimePVPRankingRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPRankingRewardInfo>> GetAllConfigDataRealTimePVPRankingRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPRankingRewardInfo>>) this.m_ConfigDataRealTimePVPRankingRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPRankingRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPSettleTimeInfo GetConfigDataRealTimePVPSettleTimeInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPSettleTimeInfo>> GetAllConfigDataRealTimePVPSettleTimeInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPSettleTimeInfo>>) this.m_ConfigDataRealTimePVPSettleTimeInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPSettleTimeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPWinsBonusInfo GetConfigDataRealTimePVPWinsBonusInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPWinsBonusInfo>> GetAllConfigDataRealTimePVPWinsBonusInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPWinsBonusInfo>>) this.m_ConfigDataRealTimePVPWinsBonusInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPWinsBonusInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRechargeStoreItemInfo GetConfigDataRechargeStoreItemInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRechargeStoreItemInfo>> GetAllConfigDataRechargeStoreItemInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRechargeStoreItemInfo>>) this.m_ConfigDataRechargeStoreItemInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRechargeStoreItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRefluxRewardInfo GetConfigDataRefluxRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRefluxRewardInfo>> GetAllConfigDataRefluxRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRefluxRewardInfo>>) this.m_ConfigDataRefluxRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRefluxRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRegionInfo GetConfigDataRegionInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRegionInfo>> GetAllConfigDataRegionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRegionInfo>>) this.m_ConfigDataRegionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRegionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataResonanceInfo GetConfigDataResonanceInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataResonanceInfo>> GetAllConfigDataResonanceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataResonanceInfo>>) this.m_ConfigDataResonanceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataResonanceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRiftChapterInfo GetConfigDataRiftChapterInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRiftChapterInfo>> GetAllConfigDataRiftChapterInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRiftChapterInfo>>) this.m_ConfigDataRiftChapterInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRiftChapterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRiftLevelInfo GetConfigDataRiftLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataRiftLevelInfo>> GetAllConfigDataRiftLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRiftLevelInfo>>) this.m_ConfigDataRiftLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRiftLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScenarioInfo GetConfigDataScenarioInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataScenarioInfo>> GetAllConfigDataScenarioInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataScenarioInfo>>) this.m_ConfigDataScenarioInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScoreLevelInfo GetConfigDataScoreLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataScoreLevelInfo>> GetAllConfigDataScoreLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataScoreLevelInfo>>) this.m_ConfigDataScoreLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataScoreLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSelectContentInfo GetConfigDataSelectContentInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSelectContentInfo>> GetAllConfigDataSelectContentInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSelectContentInfo>>) this.m_ConfigDataSelectContentInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSelectContentInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSelectProbabilityInfo GetConfigDataSelectProbabilityInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSelectProbabilityInfo>> GetAllConfigDataSelectProbabilityInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSelectProbabilityInfo>>) this.m_ConfigDataSelectProbabilityInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSelectProbabilityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSelfSelectedBoxInfo GetConfigDataSelfSelectedBoxInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSelfSelectedBoxInfo>> GetAllConfigDataSelfSelectedBoxInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSelfSelectedBoxInfo>>) this.m_ConfigDataSelfSelectedBoxInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSelfSelectedBoxInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSensitiveWords GetConfigDataSensitiveWords(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSensitiveWords>> GetAllConfigDataSensitiveWords()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSensitiveWords>>) this.m_ConfigDataSensitiveWordsData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSensitiveWords()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSignRewardInfo GetConfigDataSignRewardInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSignRewardInfo>> GetAllConfigDataSignRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSignRewardInfo>>) this.m_ConfigDataSignRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSignRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetConfigDataSkillInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSkillInfo>> GetAllConfigDataSkillInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSkillInfo>>) this.m_ConfigDataSkillInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSmallExpressionPathInfo GetConfigDataSmallExpressionPathInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSmallExpressionPathInfo>> GetAllConfigDataSmallExpressionPathInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSmallExpressionPathInfo>>) this.m_ConfigDataSmallExpressionPathInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSmallExpressionPathInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSoldierInfo GetConfigDataSoldierInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSoldierInfo>> GetAllConfigDataSoldierInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSoldierInfo>>) this.m_ConfigDataSoldierInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSoldierInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSoldierSkinInfo GetConfigDataSoldierSkinInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSoldierSkinInfo>> GetAllConfigDataSoldierSkinInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSoldierSkinInfo>>) this.m_ConfigDataSoldierSkinInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSoldierSkinInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSoundTable GetConfigDataSoundTable(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSoundTable>> GetAllConfigDataSoundTable()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSoundTable>>) this.m_ConfigDataSoundTableData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSoundTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSpineAnimationSoundTable GetConfigDataSpineAnimationSoundTable(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSpineAnimationSoundTable>> GetAllConfigDataSpineAnimationSoundTable()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSpineAnimationSoundTable>>) this.m_ConfigDataSpineAnimationSoundTableData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSpineAnimationSoundTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSST_0_CN GetConfigDataSST_0_CN(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSST_0_CN>> GetAllConfigDataSST_0_CN()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSST_0_CN>>) this.m_ConfigDataSST_0_CNData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSST_0_CN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSST_0_EN GetConfigDataSST_0_EN(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSST_0_EN>> GetAllConfigDataSST_0_EN()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSST_0_EN>>) this.m_ConfigDataSST_0_ENData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSST_0_EN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSST_1_CN GetConfigDataSST_1_CN(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSST_1_CN>> GetAllConfigDataSST_1_CN()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSST_1_CN>>) this.m_ConfigDataSST_1_CNData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSST_1_CN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSST_1_EN GetConfigDataSST_1_EN(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSST_1_EN>> GetAllConfigDataSST_1_EN()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSST_1_EN>>) this.m_ConfigDataSST_1_ENData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSST_1_EN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataStaticBoxInfo GetConfigDataStaticBoxInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataStaticBoxInfo>> GetAllConfigDataStaticBoxInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataStaticBoxInfo>>) this.m_ConfigDataStaticBoxInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataStaticBoxInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataStoreInfo GetConfigDataStoreInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataStoreInfo>> GetAllConfigDataStoreInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataStoreInfo>>) this.m_ConfigDataStoreInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataStoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataStringTable GetConfigDataStringTable(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataStringTable>> GetAllConfigDataStringTable()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataStringTable>>) this.m_ConfigDataStringTableData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataStringTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataStringTableForListInfo GetConfigDataStringTableForListInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataStringTableForListInfo>> GetAllConfigDataStringTableForListInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataStringTableForListInfo>>) this.m_ConfigDataStringTableForListInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataStringTableForListInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSurveyInfo GetConfigDataSurveyInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSurveyInfo>> GetAllConfigDataSurveyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSurveyInfo>>) this.m_ConfigDataSurveyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSurveyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSystemBroadcastInfo GetConfigDataSystemBroadcastInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataSystemBroadcastInfo>> GetAllConfigDataSystemBroadcastInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSystemBroadcastInfo>>) this.m_ConfigDataSystemBroadcastInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSystemBroadcastInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTarotInfo GetConfigDataTarotInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTarotInfo>> GetAllConfigDataTarotInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTarotInfo>>) this.m_ConfigDataTarotInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTarotInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo GetConfigDataTerrainInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTerrainInfo>> GetAllConfigDataTerrainInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTerrainInfo>>) this.m_ConfigDataTerrainInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTerrainInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataThearchyTrialInfo GetConfigDataThearchyTrialInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataThearchyTrialInfo>> GetAllConfigDataThearchyTrialInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataThearchyTrialInfo>>) this.m_ConfigDataThearchyTrialInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataThearchyTrialInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataThearchyTrialLevelInfo GetConfigDataThearchyTrialLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataThearchyTrialLevelInfo>> GetAllConfigDataThearchyTrialLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataThearchyTrialLevelInfo>>) this.m_ConfigDataThearchyTrialLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataThearchyTrialLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTicketLimitGameFunctionTypeInfo GetConfigDataTicketLimitGameFunctionTypeInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTicketLimitGameFunctionTypeInfo>> GetAllConfigDataTicketLimitGameFunctionTypeInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTicketLimitGameFunctionTypeInfo>>) this.m_ConfigDataTicketLimitGameFunctionTypeInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTicketLimitGameFunctionTypeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTicketLimitInfo GetConfigDataTicketLimitInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTicketLimitInfo>> GetAllConfigDataTicketLimitInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTicketLimitInfo>>) this.m_ConfigDataTicketLimitInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTicketLimitInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerBattleRuleInfo GetConfigDataTowerBattleRuleInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTowerBattleRuleInfo>> GetAllConfigDataTowerBattleRuleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTowerBattleRuleInfo>>) this.m_ConfigDataTowerBattleRuleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTowerBattleRuleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerBonusHeroGroupInfo GetConfigDataTowerBonusHeroGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTowerBonusHeroGroupInfo>> GetAllConfigDataTowerBonusHeroGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTowerBonusHeroGroupInfo>>) this.m_ConfigDataTowerBonusHeroGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTowerBonusHeroGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerFloorInfo GetConfigDataTowerFloorInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTowerFloorInfo>> GetAllConfigDataTowerFloorInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTowerFloorInfo>>) this.m_ConfigDataTowerFloorInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTowerFloorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerLevelInfo GetConfigDataTowerLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTowerLevelInfo>> GetAllConfigDataTowerLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTowerLevelInfo>>) this.m_ConfigDataTowerLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTowerLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingCourseInfo GetConfigDataTrainingCourseInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTrainingCourseInfo>> GetAllConfigDataTrainingCourseInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTrainingCourseInfo>>) this.m_ConfigDataTrainingCourseInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTrainingCourseInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingRoomInfo GetConfigDataTrainingRoomInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTrainingRoomInfo>> GetAllConfigDataTrainingRoomInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTrainingRoomInfo>>) this.m_ConfigDataTrainingRoomInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTrainingRoomInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingRoomLevelInfo GetConfigDataTrainingRoomLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTrainingRoomLevelInfo>> GetAllConfigDataTrainingRoomLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTrainingRoomLevelInfo>>) this.m_ConfigDataTrainingRoomLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTrainingRoomLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingTechInfo GetConfigDataTrainingTechInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTrainingTechInfo>> GetAllConfigDataTrainingTechInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTrainingTechInfo>>) this.m_ConfigDataTrainingTechInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTrainingTechInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingTechLevelInfo GetConfigDataTrainingTechLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTrainingTechLevelInfo>> GetAllConfigDataTrainingTechLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTrainingTechLevelInfo>>) this.m_ConfigDataTrainingTechLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTrainingTechLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTranslate GetConfigDataTranslate(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTranslate>> GetAllConfigDataTranslate()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTranslate>>) this.m_ConfigDataTranslateData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTranslate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTreasureLevelInfo GetConfigDataTreasureLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataTreasureLevelInfo>> GetAllConfigDataTreasureLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTreasureLevelInfo>>) this.m_ConfigDataTreasureLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTreasureLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreInfo GetConfigDataUnchartedScoreInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreInfo>> GetAllConfigDataUnchartedScoreInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreInfo>>) this.m_ConfigDataUnchartedScoreInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataUnchartedScoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreModelInfo GetConfigDataUnchartedScoreModelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreModelInfo>> GetAllConfigDataUnchartedScoreModelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreModelInfo>>) this.m_ConfigDataUnchartedScoreModelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataUnchartedScoreModelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreRewardGroupInfo GetConfigDataUnchartedScoreRewardGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreRewardGroupInfo>> GetAllConfigDataUnchartedScoreRewardGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreRewardGroupInfo>>) this.m_ConfigDataUnchartedScoreRewardGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataUnchartedScoreRewardGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUserGuide GetConfigDataUserGuide(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataUserGuide>> GetAllConfigDataUserGuide()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataUserGuide>>) this.m_ConfigDataUserGuideData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUserGuideDialogInfo GetConfigDataUserGuideDialogInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataUserGuideDialogInfo>> GetAllConfigDataUserGuideDialogInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataUserGuideDialogInfo>>) this.m_ConfigDataUserGuideDialogInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataUserGuideDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUserGuideStep GetConfigDataUserGuideStep(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataUserGuideStep>> GetAllConfigDataUserGuideStep()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataUserGuideStep>>) this.m_ConfigDataUserGuideStepData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataUserGuideStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataVersionInfo GetConfigDataVersionInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataVersionInfo>> GetAllConfigDataVersionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataVersionInfo>>) this.m_ConfigDataVersionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataVersionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWaypointInfo GetConfigDataWaypointInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataWaypointInfo>> GetAllConfigDataWaypointInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataWaypointInfo>>) this.m_ConfigDataWaypointInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWorldMapInfo GetConfigDataWorldMapInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataWorldMapInfo>> GetAllConfigDataWorldMapInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataWorldMapInfo>>) this.m_ConfigDataWorldMapInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataWorldMapInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataST_CN GetConfigDataST_CN(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator GetConfigDataST_CN(int key, Action<ConfigDataST_CN> onResult)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataST_CN>> GetAllConfigDataST_CN()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataST_CN>>) this.m_ConfigDataST_CNData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataST_CN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsConfigDataST_CNReady(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearConfigDataST_CN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataST_EN GetConfigDataST_EN(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator GetConfigDataST_EN(int key, Action<ConfigDataST_EN> onResult)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public IEnumerable<KeyValuePair<int, ConfigDataST_EN>> GetAllConfigDataST_EN()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataST_EN>>) this.m_ConfigDataST_ENData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataST_EN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsConfigDataST_ENReady(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearConfigDataST_EN()
    {
      // ISSUE: unable to decompile the method.
    }

    public float LoadingProgress
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UtilityInitialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int EditorInitialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ReleaseInitialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int PreInitialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReplaceAssetPaths()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ReplaceAssetPath(string oldPath, Dictionary<string, string> replacePath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArmyRelationData UtilityGetArmyRelationData(
      ArmyTag attacker,
      ArmyTag target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UtilityGetConfigableConst(ConfigableConstId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UtilityGetConfigableConstString(ConfigableConstId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UtilityGetStringByStringTable(StringTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UtilityGetFxFlipName(string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UtilityGetVersion(VersionInfoId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UtilityGetVersionString(VersionInfoId id)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Version
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UtilityGetSound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGoods(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CheckGoodsList(List<Goods> goodsList, int incrementValue, string descPrefix)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> WeightGoodsToGoods(List<WeightGoods> weightGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int PreCheckData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int PostCheckData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckAssetsExist()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckAssetExist(string assetName, string tableName, string fieldName, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void CheckBattlePositionDuplicate(
      int battleId,
      List<GridPosition> positions,
      int x,
      int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UtilityGetErrorCodeString(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CheckRandomBoxGoodsList(List<RandomBoxGoods> randomBoxGoodsList, string descPrefix)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddArmyRelationDataAttack(ArmyTag a, ArmyTag b, int attack, int magic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddArmyRelationDataDefend(ArmyTag a, ArmyTag b, int defend, int magicDefend)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SensitiveWords UtilityGetSensitiveWords()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Const_BattleActorMoveSpeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_CombatHeroDistance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_CombatSplitScreenDistance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_MeleeATKPunish_Mult
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_SoldierMoveDelay
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_SoldierReturnDelay
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_SkillPauseTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_SkillPreCastDelay
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_DamagePostDelay
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_BuffHitPostDelay
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_SoldierCountMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_HPBarFxAccumulateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_HPBarFxAccumulateDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Const_CriticalDamageBase
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_FlushTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_FlushPeriodDay
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_TimeEventStartTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_TimeEventEndTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_TimeEventMinCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_TimeEventMaxCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RiftLevelActionEventProbality
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ScenarioActionEventProbality
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_EventActionEventProbality
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ProbalityMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ActionEventMaxCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MapRandomEventMaxCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_EnergyAddPeriod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_EnergyMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroJobMaterialMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_BagMaxCapacity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroJobRankMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroLevelUpCeiling
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_SelectSkillsMaxCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_BagGridMaxCapacity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroStarLevelMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RiftLevelMaxStar
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_JobStartLeve
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_FirstScenarioId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_FirstWayPointId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MailBoxMaxSize
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_WeaponSlotId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArmorSlotId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_OrnamentSlotId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ShoeSlotId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_FirstInitId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_PlayerLevelMaxLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ChatMessageMaxLength
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_WorldChatIntervalTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_AllowChatPlayerLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_SystemSelectCardHeroBroadcast
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MagicStoneId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_BuyEnergyCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroCommentMaxLength
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroCommentMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_PlayerSingleHeroCommentMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxPopularCommentEntry
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ThearchyTrailEverydayChallengeNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_AnikiGymEverydayChallengeNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_AllowArenaPlayerLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaGivenTicketMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_LevelRaidTicketID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaFightExpiredTimeInterval
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ProtagonistHeroID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_PlayerNameMaxLength
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaBattleReportMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaVictoryPointsRewardMaxVictionaryPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaOneTimeGiveTicketsNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaInitialPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaOneTimeAttackUseTicketsNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaAttackSuccessRandomDropRewardID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaRealWeekSettleDeltaTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaFindOpponentOneDirectionMaxOpponentNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ChangeNameCostNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_PlayerInitialHeroInteractNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroInteractNumsRecoveryPeriod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroIteractMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroIntimateMaxValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_EnhanceEquipmentConsumeGoldPerExp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_LevelUpEquipmentStarConsumeGoldPerStar
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_BuyArenaTicketCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroDungeonLevelMaxStar
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaAddHeroExp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaAddPlayerExp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_SystemSelectCardEquipmentBroadcast
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_AnikiGymLevelTicketID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_TherachyTrialTicketID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GoldRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_CrystalRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_SkinTicketRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaHonourRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_EnergyRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int EquipmentTableRankMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_PlayerExpRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MemoryEssence
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MithralStoneRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_BrillianceMithralStoneRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_FriendshipPointsRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ArenaTicketRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_UserGuideRandomEventId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_WriteSurveyPlayerLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_AutoEquipmentDeltaTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxLevelDanmakuEntryLength
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxLevelDanmakuEntryNumsPerTurn
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_TeamRoomInviteDeltaTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_BusinessCardDescMaxLength
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_BusinessCardHeroMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_SendFriendShipPointsEverytime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_SendFriendShipPointsMaxTimes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ReceiveFriendShipPointsMaxTimes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_SearchUserByNameResultMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxDomesticFriends
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxForeignFriends
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxBlacklist
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxInvites
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxSuggestedUsers
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxLevelDiffMultiply
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_SuggestedUserLevelDiff
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_InviteExpireSeconds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RecentContactsMaxCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroAssistantTaskHeroAssignMaxCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BuyEnergyMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxGroupsPerUser
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxGroupMembers
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_BattleInviteFriendsToPracticeMaxWaitInterval
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_DayBonusNum_Aniki
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_DayBonusNum_ThearchyTrial
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_DayBonusNum_HeroTrain
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_DayBonusNum_MemoryCorridor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_DayBonusNum_Crusade
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BuyArenaTicketMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_UseBoxItemMaxCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ServerTolerateClientSpeedUpMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ServerCheckClientHeartBeatReachMinPeriod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ClientSendHeartBeatPeriod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPBattleStartTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPGoldBanHeroCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPGoldCountdown
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn1
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn2
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn3
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RefluxBeginLevelLimit
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn4
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn5
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn6
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn7
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPGoldProtectHeroCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPInitialScore
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPMatchmakingExpectedTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPMatchmakingExpectedTimeAdjust
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPMinRequiredLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPSilverCountdown
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPSilverHeroCount_Turn1
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPSilverHeroCount_Turn2
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPSilverHeroCount_Turn3
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPSilverHeroCount_Turn4
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPSilverHeroCount_Turn5
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPSilverHeroCount_Turn6
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPTurnInterval
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPPromotionSucceedScoreBonus
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPPromotionFailedScorePenalty
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPScoreMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_FriendPointsFightWithFriends
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxFriendPointsFightWithFriendsEveryday
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_BattleRoomPlayerReconnectTimeOutTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_TeamBattleRoomPlayerActionClientTimeOutTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_PVPBattleRoomPlayerActionClientTimeOutTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_TeamBattleRoomPlayerActionServerTimeOutTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_PVPBattleRoomPlayerActionServerTimeOutTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_TeamFriendShipPointRewardPerFriend
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_MaxEnchantSamePropertyNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_TeamInvitationTimeoutTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_PVPInvitationTimeoutTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_DecomposeEquipmentBackGoldPercent
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RecentContactsChatMaxCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RecentContactsTeamBattleMaxCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_DayBonusNum_CooperateBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroAssistantTaskSlotCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ChatGroupNameMaxLength
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ChatGroupCreateMinUserCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_ChatGroupDisbandUserCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_DefaultHeadFrameId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RequestAppReviewInScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_IsRequestAppReviewOn
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GainMaximum
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_AlchemyMaxNum
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_PrivilegeItemRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private int ConfigableConstId_Battle_ClientCheckOnlinePeriod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RealTimePVPInclusiveMinDan
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RealTimePVPExclusiveMaxDan
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RealTimeArenaNewPlayerMatchCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPBotPlayDeadIntervalMin
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPBotPlayDeadIntervalMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RealTimePVPBattleReportMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_NewUserAccumulatedMinValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_NewUserAccumulatedMaxValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_OldUserAccumulatedMinValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_OldUserAccumulatedMaxValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_BattleRoomPlayerActionCountdownBigNumberTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildCreateJoinLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildCreateItemId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildMemberCountMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildMaxBattlePower
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildReJoinCoolDownTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildJoinApplicationPlayerCountMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildInviteCountMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildChatHistoryCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildHiringDeclarationMaxLength
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildVicePresidentMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildEliteMinTotalActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RankListGuildNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildVicePresidentCanUsurpTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildEliteCanUsurpTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildChangeNameCrystalCost
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildDailyMaxActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildAnnouncementMaxLength
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildWeeklyMaxActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildNameMaxLength
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildLogMaxNUms
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildMedalRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_NewUserSecondAccumulatedMinValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_NewUserSecondAccumulatedMaxValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_RandomStoreManualFlushMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_HeroDungeonDailyChallengeMaxNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildMassiveCombatAvailableCountPerWeek
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int[] GuildMassiveCombatRandomHeroTagIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildMassiveCombatMinTitleToStart
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildMassiveCombatMinTitleToSurrender
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_GuildMassiveCombatMaxHeroTagIdForStronghold
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConfigableConstId_EternalShrineDailyChallengeCountMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool ConfigableConstId_AppleSubscribeMonthCardCanRepeatlyBuy
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomStoreData RandomStoreData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomDropDataInfo RandomDropData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public FixedStoreDataInfo FixedStoreData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public SignRewardDataInfo SignRewardData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public SensitiveWords SensitiveWords
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MissionDataInfo MissionData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ThearchyTrialDataInfo ThearchyTrialData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public EternalShrineDataInfo EternalShrineData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public AnikiGymDataInfo AnikiGymData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MemoryCorridorDataInfo MemoryCorridorData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HeroTrainningDataInfo HeroTrainningData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Dictionary<int, ConfigDataTicketLimitInfo> TicketId2TicketLimitInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Dictionary<int, List<EnchantPropertyProbabilityInfo>> EnchantPropertyProbabilityInfos
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BlackJack.ConfigData.RealTimePVPAvailableTime[] RealTimePVPAvailableTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ClientConfigDataLoader.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnInitLoadFromAssetEndWorkerForLuaDummyType()
    {
      return this.OnInitLoadFromAssetEndWorkerForLuaDummyType();
    }

    private void __callBase_AddConfigDataItemForLuaDummyType(string typeName, DummyType dataItem)
    {
      base.AddConfigDataItemForLuaDummyType(typeName, dataItem);
    }

    private List<string> __callBase_GetAllInitLoadConfigDataTypeNameForLuaDummy()
    {
      return base.GetAllInitLoadConfigDataTypeNameForLuaDummy();
    }

    private HashSet<string> __callBase_GetAllInitLoadConfigDataAssetPath()
    {
      return base.GetAllInitLoadConfigDataAssetPath();
    }

    private void __callBase_LoadLazyLoadConfigDataAsset(
      string configDataName,
      string configAssetName,
      Action<bool> onEnd)
    {
      this.LoadLazyLoadConfigDataAsset(configDataName, configAssetName, onEnd);
    }

    private void __callBase_OnLazyLoadFromAssetEnd(
      string configDataName,
      string configAssetName,
      UnityEngine.Object lasset,
      Action<bool> onEnd)
    {
      base.OnLazyLoadFromAssetEnd(configDataName, configAssetName, lasset, onEnd);
    }

    private string __callBase_GetLazyLoadConfigAssetNameByKey(string configDataName, int key)
    {
      return base.GetLazyLoadConfigAssetNameByKey(configDataName, key);
    }

    private string __callBase_GetConfigDataPath()
    {
      return this.GetConfigDataPath();
    }

    private Dictionary<string, List<ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo>> __callBase_GetAllLazyLoadConfigDataAssetPath()
    {
      return base.GetAllLazyLoadConfigDataAssetPath();
    }

    private ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo __callBase_GetLazyLoadConfigAssetInfo(
      string configDataName,
      string configAssetName)
    {
      return this.GetLazyLoadConfigAssetInfo(configDataName, configAssetName);
    }

    private List<DummyType> __callBase_DeserializeExtensionTableOnLoadFromAssetEnd(
      BytesScriptableObjectMD5 dataObj,
      string assetPath,
      string typeName)
    {
      return this.DeserializeExtensionTableOnLoadFromAssetEnd(dataObj, assetPath, typeName);
    }

    private void __callBase_FireEventOnConfigDataTableLoadEnd()
    {
      this.FireEventOnConfigDataTableLoadEnd();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ClientConfigDataLoader m_owner;

      public LuaExportHelper(ClientConfigDataLoader owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnInitLoadFromAssetEndWorkerForLuaDummyType()
      {
        return this.m_owner.__callBase_OnInitLoadFromAssetEndWorkerForLuaDummyType();
      }

      public void __callBase_AddConfigDataItemForLuaDummyType(string typeName, DummyType dataItem)
      {
        this.m_owner.__callBase_AddConfigDataItemForLuaDummyType(typeName, dataItem);
      }

      public List<string> __callBase_GetAllInitLoadConfigDataTypeNameForLuaDummy()
      {
        return this.m_owner.__callBase_GetAllInitLoadConfigDataTypeNameForLuaDummy();
      }

      public HashSet<string> __callBase_GetAllInitLoadConfigDataAssetPath()
      {
        return this.m_owner.__callBase_GetAllInitLoadConfigDataAssetPath();
      }

      public void __callBase_LoadLazyLoadConfigDataAsset(
        string configDataName,
        string configAssetName,
        Action<bool> onEnd)
      {
        this.m_owner.__callBase_LoadLazyLoadConfigDataAsset(configDataName, configAssetName, onEnd);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OnLazyLoadFromAssetEnd(
        string configDataName,
        string configAssetName,
        UnityEngine.Object lasset,
        Action<bool> onEnd)
      {
        // ISSUE: unable to decompile the method.
      }

      public string __callBase_GetLazyLoadConfigAssetNameByKey(string configDataName, int key)
      {
        return this.m_owner.__callBase_GetLazyLoadConfigAssetNameByKey(configDataName, key);
      }

      public string __callBase_GetConfigDataPath()
      {
        return this.m_owner.__callBase_GetConfigDataPath();
      }

      public Dictionary<string, List<ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo>> __callBase_GetAllLazyLoadConfigDataAssetPath()
      {
        return this.m_owner.__callBase_GetAllLazyLoadConfigDataAssetPath();
      }

      public ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo __callBase_GetLazyLoadConfigAssetInfo(
        string configDataName,
        string configAssetName)
      {
        return this.m_owner.__callBase_GetLazyLoadConfigAssetInfo(configDataName, configAssetName);
      }

      public List<DummyType> __callBase_DeserializeExtensionTableOnLoadFromAssetEnd(
        BytesScriptableObjectMD5 dataObj,
        string assetPath,
        string typeName)
      {
        return this.m_owner.__callBase_DeserializeExtensionTableOnLoadFromAssetEnd(dataObj, assetPath, typeName);
      }

      public void __callBase_FireEventOnConfigDataTableLoadEnd()
      {
        this.m_owner.__callBase_FireEventOnConfigDataTableLoadEnd();
      }

      public int m_allConfigsCount
      {
        get
        {
          return this.m_owner.m_allConfigsCount;
        }
        set
        {
          this.m_owner.m_allConfigsCount = value;
        }
      }

      public int m_loadingConfigIndex
      {
        get
        {
          return this.m_owner.m_loadingConfigIndex;
        }
        set
        {
          this.m_owner.m_loadingConfigIndex = value;
        }
      }

      public Dictionary<int, ConfigDataActivityCardPoolGroupInfo> m_ConfigDataActivityCardPoolGroupInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataActivityCardPoolGroupInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataActivityCardPoolGroupInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataAnikiGymInfo> m_ConfigDataAnikiGymInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataAnikiGymInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataAnikiGymInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataAnikiLevelInfo> m_ConfigDataAnikiLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataAnikiLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataAnikiLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataArenaBattleInfo> m_ConfigDataArenaBattleInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataArenaBattleInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataArenaBattleInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataArenaDefendRuleInfo> m_ConfigDataArenaDefendRuleInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataArenaDefendRuleInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataArenaDefendRuleInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataArenaLevelInfo> m_ConfigDataArenaLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataArenaLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataArenaLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataArenaOpponentPointZoneInfo> m_ConfigDataArenaOpponentPointZoneInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataArenaOpponentPointZoneInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataArenaOpponentPointZoneInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataArenaRankRewardInfo> m_ConfigDataArenaRankRewardInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataArenaRankRewardInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataArenaRankRewardInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataArenaRobotInfo> m_ConfigDataArenaRobotInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataArenaRobotInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataArenaRobotInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataArenaSettleTimeInfo> m_ConfigDataArenaSettleTimeInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataArenaSettleTimeInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataArenaSettleTimeInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataArenaVictoryPointsRewardInfo> m_ConfigDataArenaVictoryPointsRewardInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataArenaVictoryPointsRewardInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataArenaVictoryPointsRewardInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataArmyInfo> m_ConfigDataArmyInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataArmyInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataArmyInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataArmyRelation> m_ConfigDataArmyRelationData
      {
        get
        {
          return this.m_owner.m_ConfigDataArmyRelationData;
        }
        set
        {
          this.m_owner.m_ConfigDataArmyRelationData = value;
        }
      }

      public Dictionary<int, ConfigDataAssetReplaceInfo> m_ConfigDataAssetReplaceInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataAssetReplaceInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataAssetReplaceInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBanditInfo> m_ConfigDataBanditInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBanditInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBanditInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattleAchievementInfo> m_ConfigDataBattleAchievementInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattleAchievementInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattleAchievementInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattleAchievementRelatedInfo> m_ConfigDataBattleAchievementRelatedInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattleAchievementRelatedInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattleAchievementRelatedInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattleDialogInfo> m_ConfigDataBattleDialogInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattleDialogInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattleDialogInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattleEventActionInfo> m_ConfigDataBattleEventActionInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattleEventActionInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattleEventActionInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattleEventTriggerInfo> m_ConfigDataBattleEventTriggerInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattleEventTriggerInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattleEventTriggerInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattlefieldInfo> m_ConfigDataBattlefieldInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattlefieldInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattlefieldInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattleInfo> m_ConfigDataBattleInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattleInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattleInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattleLoseConditionInfo> m_ConfigDataBattleLoseConditionInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattleLoseConditionInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattleLoseConditionInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattlePerformInfo> m_ConfigDataBattlePerformInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattlePerformInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattlePerformInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattleRandomArmyInfo> m_ConfigDataBattleRandomArmyInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattleRandomArmyInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattleRandomArmyInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattleRandomTalentInfo> m_ConfigDataBattleRandomTalentInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattleRandomTalentInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattleRandomTalentInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattleTreasureInfo> m_ConfigDataBattleTreasureInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattleTreasureInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattleTreasureInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBattleWinConditionInfo> m_ConfigDataBattleWinConditionInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBattleWinConditionInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBattleWinConditionInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBehavior> m_ConfigDataBehaviorData
      {
        get
        {
          return this.m_owner.m_ConfigDataBehaviorData;
        }
        set
        {
          this.m_owner.m_ConfigDataBehaviorData = value;
        }
      }

      public Dictionary<int, ConfigDataBehaviorChangeRule> m_ConfigDataBehaviorChangeRuleData
      {
        get
        {
          return this.m_owner.m_ConfigDataBehaviorChangeRuleData;
        }
        set
        {
          this.m_owner.m_ConfigDataBehaviorChangeRuleData = value;
        }
      }

      public Dictionary<int, ConfigDataBigExpressionInfo> m_ConfigDataBigExpressionInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBigExpressionInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBigExpressionInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBuffInfo> m_ConfigDataBuffInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBuffInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBuffInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBuyArenaTicketInfo> m_ConfigDataBuyArenaTicketInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBuyArenaTicketInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBuyArenaTicketInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataBuyEnergyInfo> m_ConfigDataBuyEnergyInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataBuyEnergyInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataBuyEnergyInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCardPoolGroupInfo> m_ConfigDataCardPoolGroupInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCardPoolGroupInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCardPoolGroupInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCardPoolInfo> m_ConfigDataCardPoolInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCardPoolInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCardPoolInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataChallengeLevelInfo> m_ConfigDataChallengeLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataChallengeLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataChallengeLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCharImageInfo> m_ConfigDataCharImageInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCharImageInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCharImageInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCharImageSkinResourceInfo> m_ConfigDataCharImageSkinResourceInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCharImageSkinResourceInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCharImageSkinResourceInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCollectionActivityChallengeLevelInfo> m_ConfigDataCollectionActivityChallengeLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCollectionActivityChallengeLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCollectionActivityChallengeLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCollectionActivityCurrencyItemExtInfo> m_ConfigDataCollectionActivityCurrencyItemExtInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCollectionActivityCurrencyItemExtInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCollectionActivityCurrencyItemExtInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCollectionActivityExchangeTableInfo> m_ConfigDataCollectionActivityExchangeTableInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCollectionActivityExchangeTableInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCollectionActivityExchangeTableInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCollectionActivityInfo> m_ConfigDataCollectionActivityInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCollectionActivityInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCollectionActivityInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCollectionActivityLootLevelInfo> m_ConfigDataCollectionActivityLootLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCollectionActivityLootLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCollectionActivityLootLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCollectionActivityMapInfo> m_ConfigDataCollectionActivityMapInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCollectionActivityMapInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCollectionActivityMapInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCollectionActivityScenarioLevelInfo> m_ConfigDataCollectionActivityScenarioLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCollectionActivityScenarioLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCollectionActivityScenarioLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCollectionActivityWaypointInfo> m_ConfigDataCollectionActivityWaypointInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCollectionActivityWaypointInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCollectionActivityWaypointInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataConfessionDialogInfo> m_ConfigDataConfessionDialogInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataConfessionDialogInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataConfessionDialogInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataConfessionLetterInfo> m_ConfigDataConfessionLetterInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataConfessionLetterInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataConfessionLetterInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataConfigableConst> m_ConfigDataConfigableConstData
      {
        get
        {
          return this.m_owner.m_ConfigDataConfigableConstData;
        }
        set
        {
          this.m_owner.m_ConfigDataConfigableConstData = value;
        }
      }

      public Dictionary<int, ConfigDataConfigIDRangeInfo> m_ConfigDataConfigIDRangeInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataConfigIDRangeInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataConfigIDRangeInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCooperateBattleInfo> m_ConfigDataCooperateBattleInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCooperateBattleInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCooperateBattleInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCooperateBattleLevelInfo> m_ConfigDataCooperateBattleLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCooperateBattleLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCooperateBattleLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCrystalCardPoolGroupInfo> m_ConfigDataCrystalCardPoolGroupInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCrystalCardPoolGroupInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCrystalCardPoolGroupInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataCutsceneInfo> m_ConfigDataCutsceneInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataCutsceneInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataCutsceneInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataDailyPushNotification> m_ConfigDataDailyPushNotificationData
      {
        get
        {
          return this.m_owner.m_ConfigDataDailyPushNotificationData;
        }
        set
        {
          this.m_owner.m_ConfigDataDailyPushNotificationData = value;
        }
      }

      public Dictionary<int, ConfigDataDeviceSetting> m_ConfigDataDeviceSettingData
      {
        get
        {
          return this.m_owner.m_ConfigDataDeviceSettingData;
        }
        set
        {
          this.m_owner.m_ConfigDataDeviceSettingData = value;
        }
      }

      public Dictionary<int, ConfigDataDialogInfo> m_ConfigDataDialogInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataDialogInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataDialogInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataEnchantStoneInfo> m_ConfigDataEnchantStoneInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataEnchantStoneInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataEnchantStoneInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataEnchantTemplateInfo> m_ConfigDataEnchantTemplateInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataEnchantTemplateInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataEnchantTemplateInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataEquipmentInfo> m_ConfigDataEquipmentInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataEquipmentInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataEquipmentInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataEquipmentLevelInfo> m_ConfigDataEquipmentLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataEquipmentLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataEquipmentLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataEquipmentLevelLimitInfo> m_ConfigDataEquipmentLevelLimitInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataEquipmentLevelLimitInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataEquipmentLevelLimitInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataErrorCodeStringTable> m_ConfigDataErrorCodeStringTableData
      {
        get
        {
          return this.m_owner.m_ConfigDataErrorCodeStringTableData;
        }
        set
        {
          this.m_owner.m_ConfigDataErrorCodeStringTableData = value;
        }
      }

      public Dictionary<int, ConfigDataEternalShrineInfo> m_ConfigDataEternalShrineInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataEternalShrineInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataEternalShrineInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataEternalShrineLevelInfo> m_ConfigDataEternalShrineLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataEternalShrineLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataEternalShrineLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataEventInfo> m_ConfigDataEventInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataEventInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataEventInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataExplanationInfo> m_ConfigDataExplanationInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataExplanationInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataExplanationInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataFixedStoreItemInfo> m_ConfigDataFixedStoreItemInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataFixedStoreItemInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataFixedStoreItemInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataFlyObjectInfo> m_ConfigDataFlyObjectInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataFlyObjectInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataFlyObjectInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataFreeCardPoolGroupInfo> m_ConfigDataFreeCardPoolGroupInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataFreeCardPoolGroupInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataFreeCardPoolGroupInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataFxFlipInfo> m_ConfigDataFxFlipInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataFxFlipInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataFxFlipInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataGameFunctionOpenInfo> m_ConfigDataGameFunctionOpenInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataGameFunctionOpenInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataGameFunctionOpenInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataGamePlayTeamTypeInfo> m_ConfigDataGamePlayTeamTypeInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataGamePlayTeamTypeInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataGamePlayTeamTypeInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataGiftCDKeyInfo> m_ConfigDataGiftCDKeyInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataGiftCDKeyInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataGiftCDKeyInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataGiftStoreItemInfo> m_ConfigDataGiftStoreItemInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataGiftStoreItemInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataGiftStoreItemInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataGoddessDialogInfo> m_ConfigDataGoddessDialogInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataGoddessDialogInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataGoddessDialogInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataGroupBehavior> m_ConfigDataGroupBehaviorData
      {
        get
        {
          return this.m_owner.m_ConfigDataGroupBehaviorData;
        }
        set
        {
          this.m_owner.m_ConfigDataGroupBehaviorData = value;
        }
      }

      public Dictionary<int, ConfigDataGuildMassiveCombatDifficultyInfo> m_ConfigDataGuildMassiveCombatDifficultyInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataGuildMassiveCombatDifficultyInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataGuildMassiveCombatDifficultyInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo> m_ConfigDataGuildMassiveCombatIndividualPointsRewardsInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataGuildMassiveCombatIndividualPointsRewardsInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataGuildMassiveCombatIndividualPointsRewardsInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataGuildMassiveCombatLevelInfo> m_ConfigDataGuildMassiveCombatLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataGuildMassiveCombatLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataGuildMassiveCombatLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataGuildMassiveCombatRewardsInfo> m_ConfigDataGuildMassiveCombatRewardsInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataGuildMassiveCombatRewardsInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataGuildMassiveCombatRewardsInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataGuildMassiveCombatStrongholdInfo> m_ConfigDataGuildMassiveCombatStrongholdInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataGuildMassiveCombatStrongholdInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataGuildMassiveCombatStrongholdInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeadFrameInfo> m_ConfigDataHeadFrameInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeadFrameInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeadFrameInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroAssistantTaskGeneralInfo> m_ConfigDataHeroAssistantTaskGeneralInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroAssistantTaskGeneralInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroAssistantTaskGeneralInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroAssistantTaskInfo> m_ConfigDataHeroAssistantTaskInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroAssistantTaskInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroAssistantTaskInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroAssistantTaskScheduleInfo> m_ConfigDataHeroAssistantTaskScheduleInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroAssistantTaskScheduleInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroAssistantTaskScheduleInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroBiographyInfo> m_ConfigDataHeroBiographyInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroBiographyInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroBiographyInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroConfessionInfo> m_ConfigDataHeroConfessionInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroConfessionInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroConfessionInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroDungeonLevelInfo> m_ConfigDataHeroDungeonLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroDungeonLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroDungeonLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroFavorabilityLevelInfo> m_ConfigDataHeroFavorabilityLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroFavorabilityLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroFavorabilityLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroFetterInfo> m_ConfigDataHeroFetterInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroFetterInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroFetterInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroHeartFetterInfo> m_ConfigDataHeroHeartFetterInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroHeartFetterInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroHeartFetterInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroInfo> m_ConfigDataHeroInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroInformationInfo> m_ConfigDataHeroInformationInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroInformationInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroInformationInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroInteractionInfo> m_ConfigDataHeroInteractionInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroInteractionInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroInteractionInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroLevelInfo> m_ConfigDataHeroLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroPerformanceInfo> m_ConfigDataHeroPerformanceInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroPerformanceInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroPerformanceInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroPerformanceWordInfo> m_ConfigDataHeroPerformanceWordInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroPerformanceWordInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroPerformanceWordInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroPhantomInfo> m_ConfigDataHeroPhantomInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroPhantomInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroPhantomInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroPhantomLevelInfo> m_ConfigDataHeroPhantomLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroPhantomLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroPhantomLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroSkinInfo> m_ConfigDataHeroSkinInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroSkinInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroSkinInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroSkinSelfSelectedBoxInfo> m_ConfigDataHeroSkinSelfSelectedBoxInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroSkinSelfSelectedBoxInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroSkinSelfSelectedBoxInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroStarInfo> m_ConfigDataHeroStarInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroStarInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroStarInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroTagInfo> m_ConfigDataHeroTagInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroTagInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroTagInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroTrainningInfo> m_ConfigDataHeroTrainningInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroTrainningInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroTrainningInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataHeroTrainningLevelInfo> m_ConfigDataHeroTrainningLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataHeroTrainningLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataHeroTrainningLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataInitInfo> m_ConfigDataInitInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataInitInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataInitInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataItemInfo> m_ConfigDataItemInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataItemInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataItemInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataJobConnectionInfo> m_ConfigDataJobConnectionInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataJobConnectionInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataJobConnectionInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataJobInfo> m_ConfigDataJobInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataJobInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataJobInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataJobLevelInfo> m_ConfigDataJobLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataJobLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataJobLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataJobMaterialInfo> m_ConfigDataJobMaterialInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataJobMaterialInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataJobMaterialInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataJobUnlockConditionInfo> m_ConfigDataJobUnlockConditionInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataJobUnlockConditionInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataJobUnlockConditionInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataLanguageDataInfo> m_ConfigDataLanguageDataInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataLanguageDataInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataLanguageDataInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataMailInfo> m_ConfigDataMailInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataMailInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataMailInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataMemoryCorridorInfo> m_ConfigDataMemoryCorridorInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataMemoryCorridorInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataMemoryCorridorInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataMemoryCorridorLevelInfo> m_ConfigDataMemoryCorridorLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataMemoryCorridorLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataMemoryCorridorLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataMissionExtNoviceInfo> m_ConfigDataMissionExtNoviceInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataMissionExtNoviceInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataMissionExtNoviceInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataMissionExtRefluxInfo> m_ConfigDataMissionExtRefluxInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataMissionExtRefluxInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataMissionExtRefluxInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataMissionInfo> m_ConfigDataMissionInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataMissionInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataMissionInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataModelSkinResourceInfo> m_ConfigDataModelSkinResourceInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataModelSkinResourceInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataModelSkinResourceInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataMonthCardInfo> m_ConfigDataMonthCardInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataMonthCardInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataMonthCardInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataNoviceRewardInfo> m_ConfigDataNoviceRewardInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataNoviceRewardInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataNoviceRewardInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataOperationalActivityInfo> m_ConfigDataOperationalActivityInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataOperationalActivityInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataOperationalActivityInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataOperationalActivityItemGroupInfo> m_ConfigDataOperationalActivityItemGroupInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataOperationalActivityItemGroupInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataOperationalActivityItemGroupInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataPerformanceInfo> m_ConfigDataPerformanceInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataPerformanceInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataPerformanceInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataPlayerLevelInfo> m_ConfigDataPlayerLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataPlayerLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataPlayerLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataPrefabStateInfo> m_ConfigDataPrefabStateInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataPrefabStateInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataPrefabStateInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataPropertyModifyInfo> m_ConfigDataPropertyModifyInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataPropertyModifyInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataPropertyModifyInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataProtagonistInfo> m_ConfigDataProtagonistInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataProtagonistInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataProtagonistInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataPVPBattleInfo> m_ConfigDataPVPBattleInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataPVPBattleInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataPVPBattleInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRafflePoolInfo> m_ConfigDataRafflePoolInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRafflePoolInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRafflePoolInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRandomBoxInfo> m_ConfigDataRandomBoxInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRandomBoxInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRandomBoxInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRandomDropRewardInfo> m_ConfigDataRandomDropRewardInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRandomDropRewardInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRandomDropRewardInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRandomNameHead> m_ConfigDataRandomNameHeadData
      {
        get
        {
          return this.m_owner.m_ConfigDataRandomNameHeadData;
        }
        set
        {
          this.m_owner.m_ConfigDataRandomNameHeadData = value;
        }
      }

      public Dictionary<int, ConfigDataRandomNameMiddle> m_ConfigDataRandomNameMiddleData
      {
        get
        {
          return this.m_owner.m_ConfigDataRandomNameMiddleData;
        }
        set
        {
          this.m_owner.m_ConfigDataRandomNameMiddleData = value;
        }
      }

      public Dictionary<int, ConfigDataRandomNameTail> m_ConfigDataRandomNameTailData
      {
        get
        {
          return this.m_owner.m_ConfigDataRandomNameTailData;
        }
        set
        {
          this.m_owner.m_ConfigDataRandomNameTailData = value;
        }
      }

      public Dictionary<int, ConfigDataRandomStoreInfo> m_ConfigDataRandomStoreInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRandomStoreInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRandomStoreInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRandomStoreItemInfo> m_ConfigDataRandomStoreItemInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRandomStoreItemInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRandomStoreItemInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRandomStoreLevelZoneInfo> m_ConfigDataRandomStoreLevelZoneInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRandomStoreLevelZoneInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRandomStoreLevelZoneInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRankInfo> m_ConfigDataRankInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRankInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRankInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRealTimePVPBattleInfo> m_ConfigDataRealTimePVPBattleInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRealTimePVPBattleInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRealTimePVPBattleInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRealTimePVPDanInfo> m_ConfigDataRealTimePVPDanInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRealTimePVPDanInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRealTimePVPDanInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRealTimePVPDanRewardInfo> m_ConfigDataRealTimePVPDanRewardInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRealTimePVPDanRewardInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRealTimePVPDanRewardInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRealTimePVPLocalRankingRewardInfo> m_ConfigDataRealTimePVPLocalRankingRewardInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRealTimePVPLocalRankingRewardInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRealTimePVPLocalRankingRewardInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRealTimePVPNoviceMatchmakingInfo> m_ConfigDataRealTimePVPNoviceMatchmakingInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRealTimePVPNoviceMatchmakingInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRealTimePVPNoviceMatchmakingInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRealTimePVPRankingRewardInfo> m_ConfigDataRealTimePVPRankingRewardInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRealTimePVPRankingRewardInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRealTimePVPRankingRewardInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRealTimePVPSettleTimeInfo> m_ConfigDataRealTimePVPSettleTimeInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRealTimePVPSettleTimeInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRealTimePVPSettleTimeInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRealTimePVPWinsBonusInfo> m_ConfigDataRealTimePVPWinsBonusInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRealTimePVPWinsBonusInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRealTimePVPWinsBonusInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRechargeStoreItemInfo> m_ConfigDataRechargeStoreItemInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRechargeStoreItemInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRechargeStoreItemInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRefluxRewardInfo> m_ConfigDataRefluxRewardInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRefluxRewardInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRefluxRewardInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRegionInfo> m_ConfigDataRegionInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRegionInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRegionInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataResonanceInfo> m_ConfigDataResonanceInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataResonanceInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataResonanceInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRiftChapterInfo> m_ConfigDataRiftChapterInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRiftChapterInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRiftChapterInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataRiftLevelInfo> m_ConfigDataRiftLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataRiftLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataRiftLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataScenarioInfo> m_ConfigDataScenarioInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataScenarioInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataScenarioInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataScoreLevelInfo> m_ConfigDataScoreLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataScoreLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataScoreLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataSelectContentInfo> m_ConfigDataSelectContentInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataSelectContentInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataSelectContentInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataSelectProbabilityInfo> m_ConfigDataSelectProbabilityInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataSelectProbabilityInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataSelectProbabilityInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataSelfSelectedBoxInfo> m_ConfigDataSelfSelectedBoxInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataSelfSelectedBoxInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataSelfSelectedBoxInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataSensitiveWords> m_ConfigDataSensitiveWordsData
      {
        get
        {
          return this.m_owner.m_ConfigDataSensitiveWordsData;
        }
        set
        {
          this.m_owner.m_ConfigDataSensitiveWordsData = value;
        }
      }

      public Dictionary<int, ConfigDataSignRewardInfo> m_ConfigDataSignRewardInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataSignRewardInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataSignRewardInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataSkillInfo> m_ConfigDataSkillInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataSkillInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataSkillInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataSmallExpressionPathInfo> m_ConfigDataSmallExpressionPathInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataSmallExpressionPathInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataSmallExpressionPathInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataSoldierInfo> m_ConfigDataSoldierInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataSoldierInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataSoldierInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataSoldierSkinInfo> m_ConfigDataSoldierSkinInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataSoldierSkinInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataSoldierSkinInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataSoundTable> m_ConfigDataSoundTableData
      {
        get
        {
          return this.m_owner.m_ConfigDataSoundTableData;
        }
        set
        {
          this.m_owner.m_ConfigDataSoundTableData = value;
        }
      }

      public Dictionary<int, ConfigDataSpineAnimationSoundTable> m_ConfigDataSpineAnimationSoundTableData
      {
        get
        {
          return this.m_owner.m_ConfigDataSpineAnimationSoundTableData;
        }
        set
        {
          this.m_owner.m_ConfigDataSpineAnimationSoundTableData = value;
        }
      }

      public Dictionary<int, ConfigDataSST_0_CN> m_ConfigDataSST_0_CNData
      {
        get
        {
          return this.m_owner.m_ConfigDataSST_0_CNData;
        }
        set
        {
          this.m_owner.m_ConfigDataSST_0_CNData = value;
        }
      }

      public Dictionary<int, ConfigDataSST_0_EN> m_ConfigDataSST_0_ENData
      {
        get
        {
          return this.m_owner.m_ConfigDataSST_0_ENData;
        }
        set
        {
          this.m_owner.m_ConfigDataSST_0_ENData = value;
        }
      }

      public Dictionary<int, ConfigDataSST_1_CN> m_ConfigDataSST_1_CNData
      {
        get
        {
          return this.m_owner.m_ConfigDataSST_1_CNData;
        }
        set
        {
          this.m_owner.m_ConfigDataSST_1_CNData = value;
        }
      }

      public Dictionary<int, ConfigDataSST_1_EN> m_ConfigDataSST_1_ENData
      {
        get
        {
          return this.m_owner.m_ConfigDataSST_1_ENData;
        }
        set
        {
          this.m_owner.m_ConfigDataSST_1_ENData = value;
        }
      }

      public Dictionary<int, ConfigDataStaticBoxInfo> m_ConfigDataStaticBoxInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataStaticBoxInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataStaticBoxInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataStoreInfo> m_ConfigDataStoreInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataStoreInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataStoreInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataStringTable> m_ConfigDataStringTableData
      {
        get
        {
          return this.m_owner.m_ConfigDataStringTableData;
        }
        set
        {
          this.m_owner.m_ConfigDataStringTableData = value;
        }
      }

      public Dictionary<int, ConfigDataStringTableForListInfo> m_ConfigDataStringTableForListInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataStringTableForListInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataStringTableForListInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataSurveyInfo> m_ConfigDataSurveyInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataSurveyInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataSurveyInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataSystemBroadcastInfo> m_ConfigDataSystemBroadcastInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataSystemBroadcastInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataSystemBroadcastInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTarotInfo> m_ConfigDataTarotInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTarotInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTarotInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTerrainInfo> m_ConfigDataTerrainInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTerrainInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTerrainInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataThearchyTrialInfo> m_ConfigDataThearchyTrialInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataThearchyTrialInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataThearchyTrialInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataThearchyTrialLevelInfo> m_ConfigDataThearchyTrialLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataThearchyTrialLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataThearchyTrialLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTicketLimitGameFunctionTypeInfo> m_ConfigDataTicketLimitGameFunctionTypeInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTicketLimitGameFunctionTypeInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTicketLimitGameFunctionTypeInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTicketLimitInfo> m_ConfigDataTicketLimitInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTicketLimitInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTicketLimitInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTowerBattleRuleInfo> m_ConfigDataTowerBattleRuleInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTowerBattleRuleInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTowerBattleRuleInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTowerBonusHeroGroupInfo> m_ConfigDataTowerBonusHeroGroupInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTowerBonusHeroGroupInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTowerBonusHeroGroupInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTowerFloorInfo> m_ConfigDataTowerFloorInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTowerFloorInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTowerFloorInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTowerLevelInfo> m_ConfigDataTowerLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTowerLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTowerLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTrainingCourseInfo> m_ConfigDataTrainingCourseInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTrainingCourseInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTrainingCourseInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTrainingRoomInfo> m_ConfigDataTrainingRoomInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTrainingRoomInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTrainingRoomInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTrainingRoomLevelInfo> m_ConfigDataTrainingRoomLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTrainingRoomLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTrainingRoomLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTrainingTechInfo> m_ConfigDataTrainingTechInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTrainingTechInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTrainingTechInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTrainingTechLevelInfo> m_ConfigDataTrainingTechLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTrainingTechLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTrainingTechLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataTranslate> m_ConfigDataTranslateData
      {
        get
        {
          return this.m_owner.m_ConfigDataTranslateData;
        }
        set
        {
          this.m_owner.m_ConfigDataTranslateData = value;
        }
      }

      public Dictionary<int, ConfigDataTreasureLevelInfo> m_ConfigDataTreasureLevelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataTreasureLevelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataTreasureLevelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataUnchartedScoreInfo> m_ConfigDataUnchartedScoreInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataUnchartedScoreInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataUnchartedScoreInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataUnchartedScoreModelInfo> m_ConfigDataUnchartedScoreModelInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataUnchartedScoreModelInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataUnchartedScoreModelInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataUnchartedScoreRewardGroupInfo> m_ConfigDataUnchartedScoreRewardGroupInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataUnchartedScoreRewardGroupInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataUnchartedScoreRewardGroupInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataUserGuide> m_ConfigDataUserGuideData
      {
        get
        {
          return this.m_owner.m_ConfigDataUserGuideData;
        }
        set
        {
          this.m_owner.m_ConfigDataUserGuideData = value;
        }
      }

      public Dictionary<int, ConfigDataUserGuideDialogInfo> m_ConfigDataUserGuideDialogInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataUserGuideDialogInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataUserGuideDialogInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataUserGuideStep> m_ConfigDataUserGuideStepData
      {
        get
        {
          return this.m_owner.m_ConfigDataUserGuideStepData;
        }
        set
        {
          this.m_owner.m_ConfigDataUserGuideStepData = value;
        }
      }

      public Dictionary<int, ConfigDataVersionInfo> m_ConfigDataVersionInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataVersionInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataVersionInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataWaypointInfo> m_ConfigDataWaypointInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataWaypointInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataWaypointInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataWorldMapInfo> m_ConfigDataWorldMapInfoData
      {
        get
        {
          return this.m_owner.m_ConfigDataWorldMapInfoData;
        }
        set
        {
          this.m_owner.m_ConfigDataWorldMapInfoData = value;
        }
      }

      public Dictionary<int, ConfigDataST_CN> m_ConfigDataST_CNData
      {
        get
        {
          return this.m_owner.m_ConfigDataST_CNData;
        }
        set
        {
          this.m_owner.m_ConfigDataST_CNData = value;
        }
      }

      public Dictionary<int, ConfigDataST_EN> m_ConfigDataST_ENData
      {
        get
        {
          return this.m_owner.m_ConfigDataST_ENData;
        }
        set
        {
          this.m_owner.m_ConfigDataST_ENData = value;
        }
      }

      public static int SignRewardSpecificErrorIdIncrementValue
      {
        get
        {
          return -1;
        }
      }

      public static int ChapterSpecificErrorIdIncrementValue
      {
        get
        {
          return -2;
        }
      }

      public static int EventSpecificErrorIdIncrementValue
      {
        get
        {
          return -3;
        }
      }

      public static int RiftLevelSpecificErrorIdIncrementValue
      {
        get
        {
          return -4;
        }
      }

      public static int InitInfoSpecificErrorIdIncrementValue
      {
        get
        {
          return -5;
        }
      }

      public static int RandomDropSpecificErrorIdIncrementValue
      {
        get
        {
          return -6;
        }
      }

      public static int ScenarioSpecificErrorIdIncrementValue
      {
        get
        {
          return -7;
        }
      }

      public static int FreeCardPoolSpecificErrorIdIncrementValue
      {
        get
        {
          return -8;
        }
      }

      public static int CrystalCardPoolSpecificErrorIdIncrementValue
      {
        get
        {
          return -9;
        }
      }

      public static int ActivityCardPoolSpecificErrorIdIncrementValue
      {
        get
        {
          return -10;
        }
      }

      public static int HeroJobUnlockConditionsSpecificErrorIdIncrementValue
      {
        get
        {
          return -23;
        }
      }

      public static int ErrCodeFixedStoreItemFirstRewardSpecificErrorIdIncrementValue
      {
        get
        {
          return -33;
        }
      }

      public static int ErrCodeFixedStoreSellItemSpecificErrorIdIncrementValue
      {
        get
        {
          return -34;
        }
      }

      public static int ErrCodeCoachFavorabilityLevelUpCostItemSpecificErrorIdIncrementValue
      {
        get
        {
          return -40;
        }
      }

      public static int ErrCodeHeroDungeonStarRewardSpecificErrorIdIncrementValue
      {
        get
        {
          return -53;
        }
      }

      public static int ErrCodeHeroDungeonLevelAchievementRewardSpecificErrorIdIncrementValue
      {
        get
        {
          return -54;
        }
      }

      public static int ErrCodeOperationalActivityItemGroupItemSpecificErrorIdIncreasementValue
      {
        get
        {
          return -65;
        }
      }

      public static int ErrCodeStaticBoxItemSpecificErrorIdIncreasementValue
      {
        get
        {
          return -68;
        }
      }

      public static int ErrCodeBattleTreasureRewardSpecificErrorIdIncreasementValue
      {
        get
        {
          return -70;
        }
      }

      public Dictionary<string, string> m_fxFlipTable
      {
        get
        {
          return this.m_owner.m_fxFlipTable;
        }
        set
        {
          this.m_owner.m_fxFlipTable = value;
        }
      }

      public int ConfigableConstId_Battle_ClientCheckOnlinePeriod
      {
        get
        {
          return this.m_owner.ConfigableConstId_Battle_ClientCheckOnlinePeriod;
        }
        set
        {
          this.m_owner.ConfigableConstId_Battle_ClientCheckOnlinePeriod = value;
        }
      }

      public HashSet<string> GetAllInitLoadConfigDataAssetPath()
      {
        return this.m_owner.GetAllInitLoadConfigDataAssetPath();
      }

      public Dictionary<string, List<ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo>> GetAllLazyLoadConfigDataAssetPath()
      {
        return this.m_owner.GetAllLazyLoadConfigDataAssetPath();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public IEnumerator OnInitLoadFromAssetEndWorker(
        int totalWorkerCount,
        int workerId,
        int loadCountForSingleYield,
        Action<bool> onEnd,
        List<string> skipTypeList,
        List<string> filterTypeList)
      {
        // ISSUE: unable to decompile the method.
      }

      public void AddConfigDataItemForLuaDummyType(string typeName, DummyType dataItem)
      {
        this.m_owner.AddConfigDataItemForLuaDummyType(typeName, dataItem);
      }

      public List<string> GetAllInitLoadConfigDataTypeNameForLuaDummy()
      {
        return this.m_owner.GetAllInitLoadConfigDataTypeNameForLuaDummy();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void OnLazyLoadFromAssetEnd(
        string configDataName,
        string configAssetName,
        UnityEngine.Object lasset,
        Action<bool> onEnd)
      {
        // ISSUE: unable to decompile the method.
      }

      public int EditorInitialize()
      {
        return this.m_owner.EditorInitialize();
      }

      public int ReleaseInitialize()
      {
        return this.m_owner.ReleaseInitialize();
      }

      public void ReplaceAssetPaths()
      {
        this.m_owner.ReplaceAssetPaths();
      }

      public static string ReplaceAssetPath(string oldPath, Dictionary<string, string> replacePath)
      {
        return ClientConfigDataLoader.ReplaceAssetPath(oldPath, replacePath);
      }

      public int CheckGoodsList(List<Goods> goodsList, int incrementValue, string descPrefix)
      {
        return this.m_owner.CheckGoodsList(goodsList, incrementValue, descPrefix);
      }

      public List<Goods> WeightGoodsToGoods(List<WeightGoods> weightGoodsList)
      {
        return this.m_owner.WeightGoodsToGoods(weightGoodsList);
      }

      public int PreCheckData()
      {
        return this.m_owner.PreCheckData();
      }

      public int PostCheckData()
      {
        return this.m_owner.PostCheckData();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool CheckAssetExist(string assetName, string tableName, string fieldName, int id)
      {
        // ISSUE: unable to decompile the method.
      }

      public static void CheckBattlePositionDuplicate(
        int battleId,
        List<GridPosition> positions,
        int x,
        int y)
      {
        ClientConfigDataLoader.CheckBattlePositionDuplicate(battleId, positions, x, y);
      }

      public int CheckRandomBoxGoodsList(List<RandomBoxGoods> randomBoxGoodsList, string descPrefix)
      {
        return this.m_owner.CheckRandomBoxGoodsList(randomBoxGoodsList, descPrefix);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void AddArmyRelationDataAttack(ArmyTag a, ArmyTag b, int attack, int magic)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void AddArmyRelationDataDefend(ArmyTag a, ArmyTag b, int defend, int magicDefend)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
