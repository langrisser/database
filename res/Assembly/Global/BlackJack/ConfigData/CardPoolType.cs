﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CardPoolType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CardPoolType")]
  public enum CardPoolType
  {
    [ProtoEnum(Name = "CardPoolType_FreeCardPool", Value = 1)] CardPoolType_FreeCardPool = 1,
    [ProtoEnum(Name = "CardPoolType_CrystalCardPool", Value = 2)] CardPoolType_CrystalCardPool = 2,
    [ProtoEnum(Name = "CardPoolType_ActivityCardPool", Value = 3)] CardPoolType_ActivityCardPool = 3,
  }
}
