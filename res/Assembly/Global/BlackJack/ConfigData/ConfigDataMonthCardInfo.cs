﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMonthCardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMonthCardInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataMonthCardInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _ValidDays;
    private string _Desc;
    private List<Goods> _Reward;
    private int _OpenEverydayTaskId;
    private int _HeroExpAddRate;
    private int _HeroFavourabilityExpAddRate;
    private List<GameFuncDayBonus> _DayBonusAdd;
    private GameFunctionType _OpenGameFunc;
    private bool _IsAppleSubscribe;
    private int _FightFailEnergyDecreaseRate;
    private string _Icon;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMonthCardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ValidDays")]
    public int ValidDays
    {
      get
      {
        return this._ValidDays;
      }
      set
      {
        this._ValidDays = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "Reward")]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OpenEverydayTaskId")]
    public int OpenEverydayTaskId
    {
      get
      {
        return this._OpenEverydayTaskId;
      }
      set
      {
        this._OpenEverydayTaskId = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroExpAddRate")]
    public int HeroExpAddRate
    {
      get
      {
        return this._HeroExpAddRate;
      }
      set
      {
        this._HeroExpAddRate = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroFavourabilityExpAddRate")]
    public int HeroFavourabilityExpAddRate
    {
      get
      {
        return this._HeroFavourabilityExpAddRate;
      }
      set
      {
        this._HeroFavourabilityExpAddRate = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "DayBonusAdd")]
    public List<GameFuncDayBonus> DayBonusAdd
    {
      get
      {
        return this._DayBonusAdd;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OpenGameFunc")]
    public GameFunctionType OpenGameFunc
    {
      get
      {
        return this._OpenGameFunc;
      }
      set
      {
        this._OpenGameFunc = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsAppleSubscribe")]
    public bool IsAppleSubscribe
    {
      get
      {
        return this._IsAppleSubscribe;
      }
      set
      {
        this._IsAppleSubscribe = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FightFailEnergyDecreaseRate")]
    public int FightFailEnergyDecreaseRate
    {
      get
      {
        return this._FightFailEnergyDecreaseRate;
      }
      set
      {
        this._FightFailEnergyDecreaseRate = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
