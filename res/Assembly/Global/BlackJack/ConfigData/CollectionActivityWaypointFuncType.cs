﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityWaypointFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityWaypointFuncType")]
  public enum CollectionActivityWaypointFuncType
  {
    [ProtoEnum(Name = "CollectionActivityWaypointFuncType_None", Value = 0)] CollectionActivityWaypointFuncType_None,
    [ProtoEnum(Name = "CollectionActivityWaypointFuncType_Default", Value = 1)] CollectionActivityWaypointFuncType_Default,
    [ProtoEnum(Name = "CollectionActivityWaypointFuncType_Empty", Value = 2)] CollectionActivityWaypointFuncType_Empty,
  }
}
