﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCollectionActivityMapInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataCollectionActivityMapInfo")]
  [Serializable]
  public class ConfigDataCollectionActivityMapInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private List<int> _Waypoints_ID;
    private int _StartWaypoint_ID;
    private int _FirstScenario_ID;
    private string _Background;
    private int _BackgroundWidth;
    private int _BackgroundHeight;
    private int _CameraHeight;
    private int _CameraHeightMin;
    private int _CameraHeightMax;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityMapInfo()
    {
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, Name = "Waypoints_ID")]
    public List<int> Waypoints_ID
    {
      get
      {
        return this._Waypoints_ID;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StartWaypoint_ID")]
    public int StartWaypoint_ID
    {
      get
      {
        return this._StartWaypoint_ID;
      }
      set
      {
        this._StartWaypoint_ID = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FirstScenario_ID")]
    public int FirstScenario_ID
    {
      get
      {
        return this._FirstScenario_ID;
      }
      set
      {
        this._FirstScenario_ID = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "Background")]
    public string Background
    {
      get
      {
        return this._Background;
      }
      set
      {
        this._Background = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BackgroundWidth")]
    public int BackgroundWidth
    {
      get
      {
        return this._BackgroundWidth;
      }
      set
      {
        this._BackgroundWidth = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BackgroundHeight")]
    public int BackgroundHeight
    {
      get
      {
        return this._BackgroundHeight;
      }
      set
      {
        this._BackgroundHeight = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CameraHeight")]
    public int CameraHeight
    {
      get
      {
        return this._CameraHeight;
      }
      set
      {
        this._CameraHeight = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CameraHeightMin")]
    public int CameraHeightMin
    {
      get
      {
        return this._CameraHeightMin;
      }
      set
      {
        this._CameraHeightMin = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CameraHeightMax")]
    public int CameraHeightMax
    {
      get
      {
        return this._CameraHeightMax;
      }
      set
      {
        this._CameraHeightMax = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
