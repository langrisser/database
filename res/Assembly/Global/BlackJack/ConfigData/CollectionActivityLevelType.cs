﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityLevelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityLevelType")]
  public enum CollectionActivityLevelType
  {
    [ProtoEnum(Name = "CollectionActivityLevelType_None", Value = 0)] CollectionActivityLevelType_None,
    [ProtoEnum(Name = "CollectionActivityLevelType_Scenario", Value = 1)] CollectionActivityLevelType_Scenario,
    [ProtoEnum(Name = "CollectionActivityLevelType_Challenge", Value = 2)] CollectionActivityLevelType_Challenge,
    [ProtoEnum(Name = "CollectionActivityLevelType_Loot", Value = 3)] CollectionActivityLevelType_Loot,
  }
}
