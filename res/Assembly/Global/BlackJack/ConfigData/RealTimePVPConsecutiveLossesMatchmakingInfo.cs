﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RealTimePVPConsecutiveLossesMatchmakingInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RealTimePVPConsecutiveLossesMatchmakingInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class RealTimePVPConsecutiveLossesMatchmakingInfo : IExtensible
  {
    private int _Count;
    private int _DanMin;
    private int _DanMax;
    private bool _IsBot;
    private int _BotLevelAdjustment;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPConsecutiveLossesMatchmakingInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Count")]
    public int Count
    {
      get
      {
        return this._Count;
      }
      set
      {
        this._Count = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanMin")]
    public int DanMin
    {
      get
      {
        return this._DanMin;
      }
      set
      {
        this._DanMin = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanMax")]
    public int DanMax
    {
      get
      {
        return this._DanMax;
      }
      set
      {
        this._DanMax = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsBot")]
    public bool IsBot
    {
      get
      {
        return this._IsBot;
      }
      set
      {
        this._IsBot = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BotLevelAdjustment")]
    public int BotLevelAdjustment
    {
      get
      {
        return this._BotLevelAdjustment;
      }
      set
      {
        this._BotLevelAdjustment = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
