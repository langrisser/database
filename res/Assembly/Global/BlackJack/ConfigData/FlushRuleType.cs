﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FlushRuleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FlushRuleType")]
  public enum FlushRuleType
  {
    [ProtoEnum(Name = "FlushRuleType_None", Value = 0)] FlushRuleType_None,
    [ProtoEnum(Name = "FlushRuleType_Period", Value = 1)] FlushRuleType_Period,
    [ProtoEnum(Name = "FlushRuleType_FixedTime", Value = 2)] FlushRuleType_FixedTime,
  }
}
