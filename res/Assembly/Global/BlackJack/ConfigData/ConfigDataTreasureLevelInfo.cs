﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTreasureLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTreasureLevelInfo")]
  [CustomLuaClass]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataTreasureLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _IconResource;
    private int _OpenPlayerLevel;
    private int _EnergyFail;
    private int _EnergySuccess;
    private int _TicketCost;
    private int _Battle_ID;
    private int _MonsterLevel;
    private int _PlayerExp;
    private int _HeroExp;
    private List<int> _Bandits_ID;
    private List<Goods> _RewardList;
    private string _Strategy;
    private IExtension extensionObject;
    public List<ConfigDataBanditInfo> m_banditInfos;
    public ConfigDataBattleInfo m_battleInfo;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTreasureLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "IconResource")]
    public string IconResource
    {
      get
      {
        return this._IconResource;
      }
      set
      {
        this._IconResource = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OpenPlayerLevel")]
    public int OpenPlayerLevel
    {
      get
      {
        return this._OpenPlayerLevel;
      }
      set
      {
        this._OpenPlayerLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyFail")]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergySuccess")]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TicketCost")]
    public int TicketCost
    {
      get
      {
        return this._TicketCost;
      }
      set
      {
        this._TicketCost = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Battle_ID")]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MonsterLevel")]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerExp")]
    public int PlayerExp
    {
      get
      {
        return this._PlayerExp;
      }
      set
      {
        this._PlayerExp = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroExp")]
    public int HeroExp
    {
      get
      {
        return this._HeroExp;
      }
      set
      {
        this._HeroExp = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, Name = "Bandits_ID")]
    public List<int> Bandits_ID
    {
      get
      {
        return this._Bandits_ID;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, Name = "RewardList")]
    public List<Goods> RewardList
    {
      get
      {
        return this._RewardList;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = true, Name = "Strategy")]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
