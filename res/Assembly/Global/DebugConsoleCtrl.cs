﻿// Decompiled with JetBrains decompiler
// Type: DebugConsoleCtrl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Runtime.CompilerServices;

public class DebugConsoleCtrl
{
  private DebugConsoleView _consoleView;
  private DebugConsoleMode _consoleMode;

  private DebugConsoleCtrl()
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static DebugConsoleCtrl Create(
    DebugConsoleView consoleView,
    DebugConsoleMode consoleMode)
  {
    DebugConsoleCtrl debugConsoleCtrl = new DebugConsoleCtrl();
    debugConsoleCtrl._Init(consoleView, consoleMode);
    return debugConsoleCtrl;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _Init(DebugConsoleView consoleView, DebugConsoleMode consoleMode)
  {
    this._consoleView = consoleView;
    this._consoleMode = consoleMode;
    this._consoleView.OnKeyDown += new EventHandler<KeyDownEventArgs>(this._ConsoleView_OnKeyDown);
    this._consoleView.OnKeyUp += new EventHandler<KeyUpEventArgs>(this._ConsoleView_OnKeyUp);
    this._consoleView.OnOKButtonClick = new Action(this._ConsoleView_OnBtnOKClick);
    this._consoleView.OnCloseButtonClick = new Action(this._ConsoleView_OnBtnCloseClick);
    this._consoleMode.refreshEvent += new DebugConsoleMode.refreshDelegate(this._ConsoleModeLogRefresh);
  }

  private void _ConsoleView_OnKeyUp(object sender, KeyUpEventArgs e)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ConsoleView_OnKeyDown(object sender, KeyDownEventArgs e)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ConsoleView_OnBtnCloseClick()
  {
  }

  private void _ConsoleView_OnBtnOKClick()
  {
    this._ProcessCmd();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ProcessCmd()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ConsoleModeLogRefresh()
  {
    this._consoleView.LogText = this._consoleMode.GetLogText();
  }
}
