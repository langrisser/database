﻿// Decompiled with JetBrains decompiler
// Type: ReloginCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.UI;

public class ReloginCmd : IDebugCmd
{
  public void Execute(string strParams)
  {
    LoginUITask.Relogin();
  }

  public string GetHelpDesc()
  {
    return "Relogin : 重新登陆.";
  }

  public string GetName()
  {
    return "Relogin";
  }
}
