﻿// Decompiled with JetBrains decompiler
// Type: SLua.UnityExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace SLua
{
  public static class UnityExtension
  {
    public static void StartCoroutine(this MonoBehaviour mb, LuaFunction func)
    {
      mb.StartCoroutine(UnityExtension.LuaCoroutine(func));
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    internal static IEnumerator LuaCoroutine(LuaFunction func)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
