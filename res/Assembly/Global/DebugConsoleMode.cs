﻿// Decompiled with JetBrains decompiler
// Type: DebugConsoleMode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using UnityEngine;

public class DebugConsoleMode : IConsoleMode
{
  private StringBuilder _logText = new StringBuilder(20000);
  private StringBuilder _removedLogText = new StringBuilder(50000);
  private const int _maxLogLength = 10000;
  private DebugCmdManager _debugCmdManager;
  private static DebugConsoleMode _instance;
  private List<string> _includeFilterStrings;
  private List<string> _excludeFilterStrings;
  private bool _enableRuntimeLogFile;
  private StreamWriter _runtimeLogWriter;

  [MethodImpl((MethodImplOptions) 32768)]
  private DebugConsoleMode()
  {
    DebugConsoleMode._instance = this;
  }

  public event DebugConsoleMode.refreshDelegate refreshEvent
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      DebugConsoleMode.refreshDelegate comparand = this.refreshEvent;
      DebugConsoleMode.refreshDelegate refreshDelegate;
      do
      {
        refreshDelegate = comparand;
        comparand = Interlocked.CompareExchange<DebugConsoleMode.refreshDelegate>(ref this.refreshEvent, refreshDelegate + value, comparand);
      }
      while (comparand != refreshDelegate);
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void SetFilterString(List<string> includeStrings, List<string> excludeStrings)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static DebugConsoleMode Create()
  {
    DebugConsoleMode debugConsoleMode = new DebugConsoleMode();
    debugConsoleMode._Init();
    return debugConsoleMode;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _Init()
  {
    this._debugCmdManager = DebugCmdManager.Create((IConsoleMode) this);
    Application.logMessageReceived += new Application.LogCallback(this._LogCallback);
  }

  public void _LogReceived(string log)
  {
    this.Log(log);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void _LogCallback(string condition, string stackTrace, LogType type)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ClearLog()
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Log(string info)
  {
    if (this._includeFilterStrings != null && this._includeFilterStrings.Count > 0)
    {
      int num = 0;
      foreach (string includeFilterString in this._includeFilterStrings)
      {
        if (info.IndexOf(includeFilterString) >= 0)
          ++num;
      }
      if (num == 0)
        return;
    }
    if (this._excludeFilterStrings != null && this._excludeFilterStrings.Count > 0)
    {
      foreach (string excludeFilterString in this._excludeFilterStrings)
      {
        if (info.IndexOf(excludeFilterString) >= 0)
          return;
      }
    }
    if (this._logText != null && this._logText.Length > 10000)
    {
      int length = (int) ((double) this._logText.Length * 0.2);
      this._removedLogText.Append(this._logText.ToString(0, length));
      this._logText.Remove(0, length);
    }
    this._logText.AppendLine(info);
    this.Log2File(info);
    if (this.refreshEvent == null)
      return;
    this.refreshEvent();
  }

  public string GetLogText()
  {
    return this._logText.ToString();
  }

  public void ProcessCmd(string instruction)
  {
    this._debugCmdManager.RunInstruct(instruction);
  }

  public static DebugConsoleMode instance
  {
    get
    {
      return DebugConsoleMode._instance;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Save()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Log2File(string log)
  {
    if (!this._enableRuntimeLogFile)
      return;
    this._runtimeLogWriter.WriteLine(log);
    this._runtimeLogWriter.Flush();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void EnableRuntimeLogFile(bool isEnable)
  {
    // ISSUE: unable to decompile the method.
  }

  public delegate void refreshDelegate();
}
