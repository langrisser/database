﻿// Decompiled with JetBrains decompiler
// Type: SimpleJson.IJsonSerializerStrategy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.CodeDom.Compiler;

namespace SimpleJson
{
  [GeneratedCode("simple-json", "1.0.0")]
  public interface IJsonSerializerStrategy
  {
    bool TrySerializeNonPrimitiveObject(object input, out object output);

    object DeserializeObject(object value, Type type);
  }
}
