﻿// Decompiled with JetBrains decompiler
// Type: HelpCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

public class HelpCmd : IDebugCmd
{
  public const string _NAME = "help";
  public const string _DESC = "help: print all command's description";

  public void Execute(string strParams)
  {
    DebugCmdManager.instance.PringAllCmdDescription();
  }

  public string GetHelpDesc()
  {
    return "help: print all command's description";
  }

  public string GetName()
  {
    return "help";
  }
}
