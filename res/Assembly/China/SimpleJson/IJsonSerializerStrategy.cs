﻿// Decompiled with JetBrains decompiler
// Type: SimpleJson.IJsonSerializerStrategy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.CodeDom.Compiler;

namespace SimpleJson
{
  [GeneratedCode("simple-json", "1.0.0")]
  public interface IJsonSerializerStrategy
  {
    bool TrySerializeNonPrimitiveObject(object input, out object output);

    object DeserializeObject(object value, Type type);
  }
}
