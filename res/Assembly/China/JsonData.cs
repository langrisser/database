﻿// Decompiled with JetBrains decompiler
// Type: JsonData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Runtime.CompilerServices;

public class JsonData : IJsonWrapper, IEquatable<JsonData>, IList, IOrderedDictionary, ICollection, IEnumerable, IDictionary
{
  private IList<JsonData> inst_array;
  private bool inst_boolean;
  private double inst_double;
  private int inst_int;
  private long inst_long;
  private IDictionary<string, JsonData> inst_object;
  private string inst_string;
  private string json;
  private JsonType type;
  private IList<KeyValuePair<string, JsonData>> object_list;

  public JsonData()
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonData(bool boolean)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonData(double number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonData(int number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonData(long number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonData(object obj)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonData(string str)
  {
    this.type = JsonType.String;
    this.inst_string = str;
  }

  public int Count
  {
    get
    {
      return this.EnsureCollection().Count;
    }
  }

  public bool IsArray
  {
    get
    {
      return this.type == JsonType.Array;
    }
  }

  public bool IsBoolean
  {
    get
    {
      return this.type == JsonType.Boolean;
    }
  }

  public bool IsDouble
  {
    get
    {
      return this.type == JsonType.Double;
    }
  }

  public bool IsInt
  {
    get
    {
      return this.type == JsonType.Int;
    }
  }

  public bool IsLong
  {
    get
    {
      return this.type == JsonType.Long;
    }
  }

  public bool IsObject
  {
    get
    {
      return this.type == JsonType.Object;
    }
  }

  public bool IsString
  {
    get
    {
      return this.type == JsonType.String;
    }
  }

  int ICollection.Count
  {
    get
    {
      return this.Count;
    }
  }

  bool ICollection.IsSynchronized
  {
    get
    {
      return this.EnsureCollection().IsSynchronized;
    }
  }

  object ICollection.SyncRoot
  {
    get
    {
      return this.EnsureCollection().SyncRoot;
    }
  }

  bool IDictionary.IsFixedSize
  {
    get
    {
      return this.EnsureDictionary().IsFixedSize;
    }
  }

  bool IDictionary.IsReadOnly
  {
    get
    {
      return this.EnsureDictionary().IsReadOnly;
    }
  }

  ICollection IDictionary.Keys
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  ICollection IDictionary.Values
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  bool IJsonWrapper.IsArray
  {
    get
    {
      return this.IsArray;
    }
  }

  bool IJsonWrapper.IsBoolean
  {
    get
    {
      return this.IsBoolean;
    }
  }

  bool IJsonWrapper.IsDouble
  {
    get
    {
      return this.IsDouble;
    }
  }

  bool IJsonWrapper.IsInt
  {
    get
    {
      return this.IsInt;
    }
  }

  bool IJsonWrapper.IsLong
  {
    get
    {
      return this.IsLong;
    }
  }

  bool IJsonWrapper.IsObject
  {
    get
    {
      return this.IsObject;
    }
  }

  bool IJsonWrapper.IsString
  {
    get
    {
      return this.IsString;
    }
  }

  bool IList.IsFixedSize
  {
    get
    {
      return this.EnsureList().IsFixedSize;
    }
  }

  bool IList.IsReadOnly
  {
    get
    {
      return this.EnsureList().IsReadOnly;
    }
  }

  object IDictionary.this[object key]
  {
    get
    {
      return this.EnsureDictionary()[key];
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      if (!(key is string))
        throw new ArgumentException("The key has to be a string");
      JsonData jsonData = this.ToJsonData(value);
      this[(string) key] = jsonData;
    }
  }

  object IOrderedDictionary.this[int idx]
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
  }

  object IList.this[int index]
  {
    get
    {
      return this.EnsureList()[index];
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public JsonData this[string prop_name]
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      this.EnsureDictionary();
      return this.inst_object[prop_name];
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      this.EnsureDictionary();
      KeyValuePair<string, JsonData> keyValuePair = new KeyValuePair<string, JsonData>(prop_name, value);
      if (this.inst_object.ContainsKey(prop_name))
      {
        for (int index = 0; index < this.object_list.Count; ++index)
        {
          if (this.object_list[index].Key == prop_name)
          {
            this.object_list[index] = keyValuePair;
            break;
          }
        }
      }
      else
        this.object_list.Add(keyValuePair);
      this.inst_object[prop_name] = value;
      this.json = (string) null;
    }
  }

  public JsonData this[int index]
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public static implicit operator JsonData(bool data)
  {
    return new JsonData(data);
  }

  public static implicit operator JsonData(double data)
  {
    return new JsonData(data);
  }

  public static implicit operator JsonData(int data)
  {
    return new JsonData(data);
  }

  public static implicit operator JsonData(long data)
  {
    return new JsonData(data);
  }

  public static implicit operator JsonData(string data)
  {
    return new JsonData(data);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static explicit operator bool(JsonData data)
  {
    if (data.type != JsonType.Boolean)
      throw new InvalidCastException("Instance of JsonData doesn't hold a double");
    return data.inst_boolean;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static explicit operator double(JsonData data)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static explicit operator int(JsonData data)
  {
    if (data.type != JsonType.Int)
      throw new InvalidCastException("Instance of JsonData doesn't hold an int");
    return data.inst_int;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static explicit operator long(JsonData data)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static explicit operator string(JsonData data)
  {
    if (data.type != JsonType.String)
      throw new InvalidCastException("Instance of JsonData doesn't hold a string");
    return data.inst_string;
  }

  void ICollection.CopyTo(Array array, int index)
  {
    this.EnsureCollection().CopyTo(array, index);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IDictionary.Add(object key, object value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IDictionary.Clear()
  {
    // ISSUE: unable to decompile the method.
  }

  bool IDictionary.Contains(object key)
  {
    return this.EnsureDictionary().Contains(key);
  }

  IDictionaryEnumerator IDictionary.GetEnumerator()
  {
    return ((IOrderedDictionary) this).GetEnumerator();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IDictionary.Remove(object key)
  {
    // ISSUE: unable to decompile the method.
  }

  IEnumerator IEnumerable.GetEnumerator()
  {
    return this.EnsureCollection().GetEnumerator();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  bool IJsonWrapper.GetBoolean()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  double IJsonWrapper.GetDouble()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  int IJsonWrapper.GetInt()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  long IJsonWrapper.GetLong()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  string IJsonWrapper.GetString()
  {
    if (this.type != JsonType.String)
      throw new InvalidOperationException("JsonData instance doesn't hold a string");
    return this.inst_string;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IJsonWrapper.SetBoolean(bool val)
  {
    this.type = JsonType.Boolean;
    this.inst_boolean = val;
    this.json = (string) null;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IJsonWrapper.SetDouble(double val)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IJsonWrapper.SetInt(int val)
  {
    this.type = JsonType.Int;
    this.inst_int = val;
    this.json = (string) null;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IJsonWrapper.SetLong(long val)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IJsonWrapper.SetString(string val)
  {
    this.type = JsonType.String;
    this.inst_string = val;
    this.json = (string) null;
  }

  string IJsonWrapper.ToJson()
  {
    return this.ToJson();
  }

  void IJsonWrapper.ToJson(JsonWriter writer)
  {
    this.ToJson(writer);
  }

  int IList.Add(object value)
  {
    return this.Add(value);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IList.Clear()
  {
    // ISSUE: unable to decompile the method.
  }

  bool IList.Contains(object value)
  {
    return this.EnsureList().Contains(value);
  }

  int IList.IndexOf(object value)
  {
    return this.EnsureList().IndexOf(value);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IList.Insert(int index, object value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IList.Remove(object value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IList.RemoveAt(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  IDictionaryEnumerator IOrderedDictionary.GetEnumerator()
  {
    this.EnsureDictionary();
    return (IDictionaryEnumerator) new OrderedDictionaryEnumerator(this.object_list.GetEnumerator());
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IOrderedDictionary.Insert(int idx, object key, object value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IOrderedDictionary.RemoveAt(int idx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private ICollection EnsureCollection()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private IDictionary EnsureDictionary()
  {
    if (this.type == JsonType.Object)
      return (IDictionary) this.inst_object;
    if (this.type != JsonType.None)
      throw new InvalidOperationException("Instance of JsonData is not a dictionary");
    this.type = JsonType.Object;
    this.inst_object = (IDictionary<string, JsonData>) new Dictionary<string, JsonData>();
    this.object_list = (IList<KeyValuePair<string, JsonData>>) new List<KeyValuePair<string, JsonData>>();
    return (IDictionary) this.inst_object;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private IList EnsureList()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private JsonData ToJsonData(object obj)
  {
    if (obj == null)
      return (JsonData) null;
    if (obj is JsonData)
      return (JsonData) obj;
    return new JsonData(obj);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void WriteJson(IJsonWrapper obj, JsonWriter writer)
  {
    if (obj.IsString)
      writer.Write(obj.GetString());
    else if (obj.IsBoolean)
      writer.Write(obj.GetBoolean());
    else if (obj.IsDouble)
      writer.Write(obj.GetDouble());
    else if (obj.IsInt)
      writer.Write(obj.GetInt());
    else if (obj.IsLong)
      writer.Write(obj.GetLong());
    else if (obj.IsArray)
    {
      writer.WriteArrayStart();
      IEnumerator enumerator = obj.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
          JsonData.WriteJson((IJsonWrapper) enumerator.Current, writer);
      }
      finally
      {
        if (enumerator is IDisposable disposable)
          disposable.Dispose();
      }
      writer.WriteArrayEnd();
    }
    else
    {
      if (!obj.IsObject)
        return;
      writer.WriteObjectStart();
      IDictionaryEnumerator enumerator = ((IDictionary) obj).GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          DictionaryEntry current = (DictionaryEntry) enumerator.Current;
          writer.WritePropertyName((string) current.Key);
          JsonData.WriteJson((IJsonWrapper) current.Value, writer);
        }
      }
      finally
      {
        if (enumerator is IDisposable disposable)
          disposable.Dispose();
      }
      writer.WriteObjectEnd();
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public int Add(object value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Clear()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public bool Equals(JsonData x)
  {
    // ISSUE: unable to decompile the method.
  }

  public JsonType GetJsonType()
  {
    return this.type;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void SetJsonType(JsonType type)
  {
    if (this.type == type)
      return;
    switch (type)
    {
      case JsonType.Object:
        this.inst_object = (IDictionary<string, JsonData>) new Dictionary<string, JsonData>();
        this.object_list = (IList<KeyValuePair<string, JsonData>>) new List<KeyValuePair<string, JsonData>>();
        break;
      case JsonType.Array:
        this.inst_array = (IList<JsonData>) new List<JsonData>();
        break;
      case JsonType.String:
        this.inst_string = (string) null;
        break;
      case JsonType.Int:
        this.inst_int = 0;
        break;
      case JsonType.Long:
        this.inst_long = 0L;
        break;
      case JsonType.Double:
        this.inst_double = 0.0;
        break;
      case JsonType.Boolean:
        this.inst_boolean = false;
        break;
    }
    this.type = type;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public string ToJson()
  {
    if (this.json != null)
      return this.json;
    StringWriter stringWriter = new StringWriter();
    JsonData.WriteJson((IJsonWrapper) this, new JsonWriter((TextWriter) stringWriter)
    {
      Validate = false
    });
    this.json = stringWriter.ToString();
    return this.json;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ToJson(JsonWriter writer)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override string ToString()
  {
    // ISSUE: unable to decompile the method.
  }
}
