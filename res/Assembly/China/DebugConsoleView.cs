﻿// Decompiled with JetBrains decompiler
// Type: DebugConsoleView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;

public class DebugConsoleView
{
  private int _preViewWidth = -1;
  private int _preViewHeight = -1;
  private string _instructionText = string.Empty;
  private Vector2 _outputScrollViewVector = Vector2.zero;
  private string _logText = string.Empty;
  private bool _isAutoScrollView = true;
  private float _fps = 60f;
  public Action OnOKButtonClick;
  public Action OnCloseButtonClick;
  private const int CONSOLE_MARGIN_X = 5;
  private const int CONSOLE_MARGIN_Y = 5;
  private const int SWITCHBTN_MARGIN_X = 5;
  private const int SWITCHBTN_MARGIN_Y = 5;
  private const int SWITCHBTN_WIDTH = 30;
  private const int SWITCHBTN_HEIGHT = 30;
  private int _lastFrameTouchTapCount;
  private const string CONSOLE_INPUTFIELD_NAME = "ConsoleInputField";
  private bool _ConsoleInputFieldFocusIsSet;
  private const int OKBUTTON_SIZE = 50;
  private int _consoleWidth;
  private int _consoleHeight;
  private Rect _windowRect;
  private bool _isSwitchOn;
  private bool m_isForceHide;
  private static DebugConsoleView _instance;
  private float _leftAndRightButtonDownTime;
  private float _fiveFingersDownTime;
  private float _nextUpdateFPSTime;
  private double _allocatedMemorySize;
  private double _maxAllocatedMemorySize;
  private GUIStyle m_labelStyle;
  private GUIStyle m_textfieldStyle;
  private const float ScreenDpi2FontSize = 0.14f;

  [MethodImpl((MethodImplOptions) 32768)]
  private DebugConsoleView()
  {
    DebugConsoleView._instance = this;
  }

  public event EventHandler<KeyDownEventArgs> OnKeyDown
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      EventHandler<KeyDownEventArgs> comparand = this.OnKeyDown;
      EventHandler<KeyDownEventArgs> eventHandler;
      do
      {
        eventHandler = comparand;
        comparand = Interlocked.CompareExchange<EventHandler<KeyDownEventArgs>>(ref this.OnKeyDown, eventHandler + value, comparand);
      }
      while (comparand != eventHandler);
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public event EventHandler<KeyUpEventArgs> OnKeyUp
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      EventHandler<KeyUpEventArgs> comparand = this.OnKeyUp;
      EventHandler<KeyUpEventArgs> eventHandler;
      do
      {
        eventHandler = comparand;
        comparand = Interlocked.CompareExchange<EventHandler<KeyUpEventArgs>>(ref this.OnKeyUp, eventHandler + value, comparand);
      }
      while (comparand != eventHandler);
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public bool IsForceHide
  {
    get
    {
      return this.m_isForceHide;
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      if (this.m_isForceHide == value)
        return;
      this.m_isForceHide = value;
      if (!this.m_isForceHide)
        return;
      this.IsSwitchOn = false;
    }
  }

  public bool IsSwitchOn
  {
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      if (this.IsForceHide && value || this._isSwitchOn == value)
        return;
      this._isSwitchOn = value;
      this.SaveData();
    }
    get
    {
      return this._isSwitchOn;
    }
  }

  private bool IsAutoScrollView
  {
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
    get
    {
      return this._isAutoScrollView;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static DebugConsoleView Create()
  {
    DebugConsoleView debugConsoleView = new DebugConsoleView();
    debugConsoleView._Init();
    return debugConsoleView;
  }

  private void _Init()
  {
    this.LoadData();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Show()
  {
    if (this.m_labelStyle == null)
    {
      this.m_labelStyle = new GUIStyle(GUI.skin.label);
      this.m_labelStyle.fontSize = (int) ((double) Screen.dpi * 0.140000000596046);
    }
    if (this.m_textfieldStyle == null)
    {
      this.m_textfieldStyle = new GUIStyle(GUI.skin.textField);
      this.m_textfieldStyle.fontSize = (int) ((double) Screen.dpi * 0.140000000596046);
    }
    int width = Screen.width;
    int height = Screen.height;
    this._consoleWidth = width / 2 - 20;
    this._consoleHeight = height / 2;
    if (this._preViewWidth == -1 || this._preViewWidth != width || (this._preViewHeight == -1 || this._preViewHeight != height))
    {
      this._windowRect = new Rect(5f, 5f, (float) this._consoleWidth, (float) this._consoleHeight);
      this._preViewWidth = width;
      this._preViewHeight = height;
    }
    this._ShowConsole(5, 5, this._consoleWidth, this._consoleHeight);
    this._ShowSwitchInput();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ShowSwitchInput()
  {
    if (Input.GetMouseButton(0) && Input.GetMouseButton(1))
      this._leftAndRightButtonDownTime += Time.deltaTime;
    else
      this._leftAndRightButtonDownTime = 0.0f;
    if ((double) this._leftAndRightButtonDownTime > 2.0 && (double) this._leftAndRightButtonDownTime - (double) Time.deltaTime < 2.0)
      this.IsSwitchOn = !this.IsSwitchOn;
    LeftMouseButtonTapCount.Update();
    if (!this.IsSwitchOn || LeftMouseButtonTapCount.Count != 3)
      return;
    this.IsAutoScrollView = !this.IsAutoScrollView;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ShowSwitchTouch()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ShowConsole(int x, int y, int width, int height)
  {
    if (this.IsSwitchOn)
      this._windowRect = GUILayout.Window(0, this._windowRect, new GUI.WindowFunction(this._WindowFunction), "Console Window");
    else
      this._ConsoleInputFieldFocusIsSet = false;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private Rect _ComputeSwitchBtnRect(int viewWidth, int viewHeight)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private string _GetSwitchButtonText()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ProcessKeyboard()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _WindowFunction(int id)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _DrawMainConsoleCtrl()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _DrawInputBox_And_OKButton()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _DrawInputBox()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _DrawOKButton()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _DrawCloseButton()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _DrawOutputArea()
  {
    // ISSUE: unable to decompile the method.
  }

  public string LogText
  {
    set
    {
      this._logText = value;
    }
  }

  public string InstructionText
  {
    get
    {
      return this._instructionText;
    }
    set
    {
      this._instructionText = value;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void SetFontSize(int size)
  {
    // ISSUE: unable to decompile the method.
  }

  public static DebugConsoleView instance
  {
    get
    {
      return DebugConsoleView._instance;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ShowNextCommand()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ShowPreviousCommand()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void SaveData()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void LoadData()
  {
    string path = string.Format("{0}{1}DebugConsoleView.txt", (object) Application.persistentDataPath, (object) Path.AltDirectorySeparatorChar);
    try
    {
      if (!File.Exists(path))
        return;
      StreamReader streamReader = new StreamReader(path);
      this.IsSwitchOn = bool.Parse(streamReader.ReadLine());
      streamReader.Close();
    }
    catch (FileNotFoundException ex)
    {
      Debug.LogError(string.Format("DebugConsoleView.LoadData [{0}] not found.", (object) path));
    }
    catch (FileLoadException ex)
    {
      Debug.LogError(string.Format("DebugConsoleView.LoadDataFailed to load [{0}].", (object) path));
    }
    catch (Exception ex)
    {
      Debug.LogError(string.Format("DebugConsoleView.LoadData exception={0}", (object) ex.Message));
    }
  }
}
