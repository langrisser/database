﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.Utility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectLBasic
{
  [CustomLuaClass]
  public class Utility
  {
    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator GetUpdateClientURL(GameObject parentObj = null)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new Utility.\u003CGetUpdateClientURL\u003Ec__Iterator0()
      {
        parentObj = parentObj
      };
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Texture2D ResizeTexture(
      Texture2D pSource,
      ImageFilterMode pFilterMode,
      float pScale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIStateOpen(
      CommonUIStateController ctrl,
      string stateName,
      Action onEnd = null,
      Action<bool> EnableInput = null,
      bool allowToRefreshSameState = true)
    {
      if ((UnityEngine.Object) ctrl == (UnityEngine.Object) null || string.IsNullOrEmpty(stateName))
        return;
      if (EnableInput != null)
        EnableInput(false);
      ctrl.gameObject.SetActive(true);
      ctrl.SetActionForUIStateFinshed(stateName, (Action) (() =>
      {
        if (EnableInput != null)
          EnableInput(true);
        if (onEnd == null)
          return;
        onEnd();
      }));
      ctrl.SetToUIState(stateName, false, allowToRefreshSameState);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIStateClose(
      CommonUIStateController ctrl,
      string stateName,
      Action onEnd = null,
      Action<bool> EnableInput = null,
      bool allowToRefreshSameState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator<object> Co_UIStateCloseFinished(
      CommonUIStateController ctrl,
      Action onEnd,
      Action<bool> EnableInput)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T FindChildComponent<T>(GameObject go, string name, bool warning = true) where T : Component
    {
      if ((UnityEngine.Object) go == (UnityEngine.Object) null)
        return (T) null;
      Transform transform = go.transform.Find(name);
      if ((UnityEngine.Object) transform == (UnityEngine.Object) null)
      {
        if (warning)
          Debug.LogError(name + " not found.");
        return (T) null;
      }
      T component = transform.GetComponent<T>();
      if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
        return component;
      if (warning)
        Debug.LogError(typeof (T).Name + " componnent not found.");
      return (T) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject FindChildGameObject(
      GameObject go,
      string name,
      bool warning = true)
    {
      if ((UnityEngine.Object) go == (UnityEngine.Object) null)
        return (GameObject) null;
      Transform transform = go.transform.Find(name);
      if (!((UnityEngine.Object) transform == (UnityEngine.Object) null))
        return transform.gameObject;
      if (warning)
        Debug.LogError(name + " not found.");
      return (GameObject) null;
    }
  }
}
