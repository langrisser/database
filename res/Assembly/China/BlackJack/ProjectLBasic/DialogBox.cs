﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.DialogBox
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackJack.ProjectLBasic
{
  [CustomLuaClass]
  public class DialogBox : MonoBehaviour
  {
    private Action<DialogBoxResult> m_callback;
    private CommonUIStateController m_uiStateController;
    private CommonUIStateController m_panelUIStateController;
    private Text m_msgText;
    private Button m_okButton;
    private Text m_okButtonText;
    private Button m_cancelButton;
    private Text m_cancelButtonText;
    private GameObject m_backgroundGameObject;
    private DialogBoxResult m_ret;

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator Show(
      GameObject parentObj,
      string prefabPath,
      string msgText,
      string okText,
      string cancelText,
      Action<DialogBoxResult> onClose)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new DialogBox.\u003CShow\u003Ec__Iterator0()
      {
        parentObj = parentObj,
        prefabPath = prefabPath,
        msgText = msgText,
        okText = okText,
        cancelText = cancelText,
        onClose = onClose
      };
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show(
      string msgText,
      string okText,
      string cancelText,
      Action<DialogBoxResult> callback)
    {
      if ((UnityEngine.Object) this.m_uiStateController == (UnityEngine.Object) null)
        this.Initialize();
      this.m_msgText.text = msgText;
      this.m_okButtonText.text = okText;
      if (cancelText.Length == 0)
      {
        this.m_panelUIStateController.SetToUIState("OneButton", false, true);
      }
      else
      {
        this.m_panelUIStateController.SetToUIState("TwoButton", false, true);
        this.m_cancelButtonText.text = cancelText;
      }
      this.m_callback = callback;
      Utility.SetUIStateOpen(this.m_uiStateController, "Open", (Action) null, (Action<bool>) null, true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize()
    {
      this.gameObject.SetActive(false);
      this.m_uiStateController = this.gameObject.GetComponent<CommonUIStateController>();
      this.m_panelUIStateController = Utility.FindChildComponent<CommonUIStateController>(this.gameObject, "Panel", true);
      this.m_msgText = Utility.FindChildComponent<Text>(this.gameObject, "Panel/FrameImage/Text", true);
      this.m_okButton = Utility.FindChildComponent<Button>(this.gameObject, "Panel/FrameImage/ButtonGroup/OkButton", true);
      this.m_okButtonText = Utility.FindChildComponent<Text>(this.gameObject, "Panel/FrameImage/ButtonGroup/OkButton/Text", true);
      this.m_cancelButton = Utility.FindChildComponent<Button>(this.gameObject, "Panel/FrameImage/ButtonGroup/CancelButton", true);
      this.m_cancelButtonText = Utility.FindChildComponent<Text>(this.gameObject, "Panel/FrameImage/ButtonGroup/CancelButton/Text", true);
      this.m_backgroundGameObject = Utility.FindChildGameObject(this.gameObject, "Background", true);
      this.m_okButton.onClick.AddListener(new UnityAction(this.OnOK));
      this.m_cancelButton.onClick.AddListener(new UnityAction(this.OnCancel));
      this.m_backgroundGameObject.SetActive(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Cancel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnCancel()
    {
      this.OnClose(DialogBoxResult.Cancel);
    }

    private void OnOK()
    {
      this.OnClose(DialogBoxResult.Ok);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClose(DialogBoxResult ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseAnimationFinished()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
