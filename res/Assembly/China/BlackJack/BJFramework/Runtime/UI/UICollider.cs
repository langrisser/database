﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UICollider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [AddComponentMenu("UIExtend/UICollider", 16)]
  public class UICollider : Graphic
  {
    public override bool Raycast(Vector2 sp, Camera eventCamera)
    {
      return true;
    }

    protected override void OnPopulateMesh(VertexHelper vh)
    {
      vh.Clear();
    }
  }
}
