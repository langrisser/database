﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIIntentCustom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  [CustomLuaClassWithProtected]
  public class UIIntentCustom : UIIntent
  {
    private Dictionary<string, object> m_params;
    [DoNotToLua]
    private UIIntentCustom.LuaExportHelper luaExportHelper;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIIntentCustom(string targetTaskName, string targetMode = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetParam(string key, object value)
    {
      this.m_params[key] = value;
    }

    public bool TryGetParam(string key, out object value)
    {
      return this.m_params.TryGetValue(key, out value);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetClassParam<T>(string key) where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetStructParam<T>(string key) where T : struct
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public UIIntentCustom.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    public class LuaExportHelper
    {
      private UIIntentCustom m_owner;

      public LuaExportHelper(UIIntentCustom owner)
      {
        this.m_owner = owner;
      }

      public Dictionary<string, object> m_params
      {
        get
        {
          return this.m_owner.m_params;
        }
        set
        {
          this.m_owner.m_params = value;
        }
      }
    }
  }
}
