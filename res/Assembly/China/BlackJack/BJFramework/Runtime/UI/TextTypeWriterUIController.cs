﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.TextTypeWriterUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [AddComponentMenu("UI/BlackJack/TextTypeWriterUIController")]
  public class TextTypeWriterUIController : MonoBehaviour
  {
    public int charsPerSecond;
    public float fadeInTime;
    public bool AutoStart;
    private Text mText;
    protected Color mTextColor;
    private string mFullText;
    private int mCurrentOffset;
    private float mNextChar;
    private bool mReset;
    private bool mActive;
    private List<TextTypeWriterUIController.FadeEntry> mFade;

    [MethodImpl((MethodImplOptions) 32768)]
    public TextTypeWriterUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool isActive
    {
      get
      {
        return this.mActive;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetToBeginning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Finish()
    {
      // ISSUE: unable to decompile the method.
    }

    public void StartTypeWriter()
    {
      this.mReset = true;
      this.mActive = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetAlphaColor(float alpha)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    private struct FadeEntry
    {
      public int index;
      public string text;
      public float alpha;
    }
  }
}
