﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UITaskPipeLineCtx
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  public class UITaskPipeLineCtx
  {
    public bool m_isInitPipeLine = true;
    public bool m_blockGlobalUIInput = true;
    public bool m_isTaskResume;
    public bool m_isRuning;
    public bool m_layerLoadedInPipe;
    public Action m_redirectPipLineOnAllResReady;
    protected ulong m_updateMask;

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskPipeLineCtx()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Clear()
    {
      this.m_isInitPipeLine = false;
      this.m_layerLoadedInPipe = false;
      this.m_isTaskResume = false;
      this.m_redirectPipLineOnAllResReady = (Action) null;
      this.m_updateMask = 0UL;
      this.m_isRuning = false;
    }

    public void SetUpdateMask(ulong mask)
    {
      this.m_updateMask = mask;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUpdateMask(params int[] indexs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUpdateMask(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearUpdateMask(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedUpdate(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsUpdateMaskClear()
    {
      return this.m_updateMask == 0UL;
    }
  }
}
