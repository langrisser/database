﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIIntent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  [CustomLuaClassWithProtected]
  public class UIIntent
  {
    [DoNotToLua]
    private UIIntent.LuaExportHelper luaExportHelper;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIIntent(string targetTaskName, string targetMode = null)
    {
      this.TargetTaskName = targetTaskName;
      this.TargetMode = targetMode;
    }

    public string TargetTaskName { get; private set; }

    public string TargetMode { get; set; }

    [DoNotToLua]
    public UIIntent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    public class LuaExportHelper
    {
      private UIIntent m_owner;

      public LuaExportHelper(UIIntent owner)
      {
        this.m_owner = owner;
      }

      public string TargetTaskName
      {
        set
        {
          this.m_owner.TargetTaskName = value;
        }
      }
    }
  }
}
