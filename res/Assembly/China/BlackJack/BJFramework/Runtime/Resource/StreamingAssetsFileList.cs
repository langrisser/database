﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Resource.StreamingAssetsFileList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Resource
{
  public class StreamingAssetsFileList : ScriptableObject
  {
    public List<StreamingAssetsFileList.ListItem> m_fileList = new List<StreamingAssetsFileList.ListItem>();
    public int m_version;

    [MethodImpl((MethodImplOptions) 32768)]
    public StreamingAssetsFileList()
    {
    }

    [Serializable]
    public class ListItem
    {
      public string m_bundleName;
      public int m_bundleVersion;
      public string m_filePath;

      [MethodImpl((MethodImplOptions) 32768)]
      public ListItem(string path, int version)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
