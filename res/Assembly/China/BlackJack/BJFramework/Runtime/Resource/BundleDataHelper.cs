﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Resource.BundleDataHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Resource
{
  public class BundleDataHelper
  {
    private Dictionary<string, BundleData.SingleBundleData> m_bundleDataDict = new Dictionary<string, BundleData.SingleBundleData>();
    private Dictionary<string, BundleData.SingleBundleData> m_assetPath2BundleDataDict = new Dictionary<string, BundleData.SingleBundleData>((IEqualityComparer<string>) StringComparer.OrdinalIgnoreCase);
    private BundleData m_internalBundleData;
    private bool m_assetPathIgnoreCase;

    [MethodImpl((MethodImplOptions) 32768)]
    public BundleDataHelper(BundleData bundleData, bool assetPathIgnoreCase)
    {
      this.m_internalBundleData = bundleData;
      this.m_assetPathIgnoreCase = assetPathIgnoreCase;
      this.m_internalBundleData.m_bundleList.ForEach((Action<BundleData.SingleBundleData>) (elem => this.m_bundleDataDict.Add(elem.m_bundleName, elem)));
      this.m_internalBundleData.m_bundleList.ForEach((Action<BundleData.SingleBundleData>) (elem => elem.m_assetList.ForEach((Action<string>) (assetPath => this.m_assetPath2BundleDataDict.Add(!this.m_assetPathIgnoreCase ? assetPath : assetPath.ToLower(), elem)))));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBundleVersionByName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BundleData.SingleBundleData GetBundleDataByName(string name)
    {
      BundleData.SingleBundleData singleBundleData;
      if (!this.m_bundleDataDict.TryGetValue(name, out singleBundleData))
        return (BundleData.SingleBundleData) null;
      return singleBundleData;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BundleData.SingleBundleData GetBundleDataByAssetPath(string assetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    public BundleData.SingleBundleData GetResaveFileBundleDataByPath(string relativePath)
    {
      return this.GetBundleDataByAssetPath(relativePath);
    }
  }
}
