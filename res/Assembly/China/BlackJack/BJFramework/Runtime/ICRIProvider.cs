﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ICRIProvider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  public interface ICRIProvider
  {
    GameObject GetCRIManagerObject();

    void CriRegisterAcf(string acfFullPath);

    void AddCRIComponentSources();

    void CRIRemoveCueSheet(string sheetName);

    void CRIAddCueSheet(string sheetName, string acbFullPath, string awbFullPath);

    float GetCueLength(string sheetName, string cueName);

    void SetCategoryVolume(string category, float volume);

    float GetCategoryVolume(string category);

    IAudioPlayback PlaySound(string sound);

    void Pause(bool pause);
  }
}
