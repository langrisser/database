﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.AudioSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class AudioSettings
  {
    public string AudioMixerAssetPath;
    public string AudioMixerBGMGroupSubPath;
    public string AudioMixerSoundEffectGroupSubPath;
    public string AudioMixerPlayerVoiceGroupSubPath;
    public string AudioMixerSpeechGroupSubPath;
    public string AudioMixerBGMVolumeParamName;
    public string AudioMixerMovieBGMVolumeParamName;
    public string AudioMixerSoundEffectParamName;
    public string AudioMixerPlayerVoiceVolumeParamName;
    public string AudioMixerSpeechVolumeParamName;
    public bool EnableCRI;
    public string CRIAudioManagerAsset;
    public List<string> Languages;
    public int DefaultLanguageIndex;
    public string CRIVideoAssetPathRoot;
    public string CRIVideoAssetPathRootForEditor;
    public bool EnableDownload;
  }
}
