﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Timer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  public static class Timer
  {
    public static DateTime m_lastTickTime = DateTime.MaxValue;
    public static DateTime m_currTime;
    public static ulong m_currTick;

    [MethodImpl((MethodImplOptions) 32768)]
    public static uint GetLastFrameDeltaTimeMs()
    {
      // ISSUE: unable to decompile the method.
    }

    public static int GetCurrFrameCount()
    {
      return Time.frameCount;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Tick()
    {
      Timer.m_lastTickTime = Timer.m_currTime;
      Timer.m_currTime = DateTime.Now;
      ++Timer.m_currTick;
    }
  }
}
