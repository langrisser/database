﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.UtilityTools.ListExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.UtilityTools
{
  public static class ListExtensions
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static void Remove<T>(this List<T> list, Predicate<T> match)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int InsertInOrder<T>(this List<T> list, T item, bool unique = true, bool reverse = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int InsertInOrder<T>(
      this List<T> list,
      T item,
      Func<T, T, int> compare,
      bool unique = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int BinarySearch<T>(
      this List<T> list,
      T item,
      Func<T, T, int> compare = null,
      bool reverse = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AddUnique<T>(this List<T> list, T item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AddRangeUnique<T>(this List<T> list, List<T> items)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool NoDuplicateItems<T>(this List<T> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<T> RandomList<T>(this List<T> srcList)
    {
      // ISSUE: unable to decompile the method.
    }

    public class ComparisonComparer<T> : IComparer<T>
    {
      private readonly Comparison<T> comparison;
      private readonly bool reverse;

      [MethodImpl((MethodImplOptions) 32768)]
      public ComparisonComparer(Func<T, T, int> compare, bool reverse = false)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int Compare(T x, T y)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
