﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.UtilityTools.HeadIconTools
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;

namespace BlackJack.UtilityTools
{
  [CustomLuaClass]
  public class HeadIconTools
  {
    public static int GetHeadPortrait(int headIcon)
    {
      return headIcon >> 16;
    }

    public static int GetHeadFrame(int headIcon)
    {
      return headIcon & (int) ushort.MaxValue;
    }

    public static int GetHeadIcon(int headPortraitId, int headFrameId)
    {
      return headPortraitId << 16 | headFrameId;
    }
  }
}
