﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.Utils.DelayExecHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.Utils
{
  public class DelayExecHelper
  {
    private List<DelayExecHelper.DelayExecItem> m_delayExecList = new List<DelayExecHelper.DelayExecItem>();

    [MethodImpl((MethodImplOptions) 32768)]
    public DelayExecHelper()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DelayExecHelper.IDelayExecItem DelayExec(
      Action action,
      int delayTime,
      uint currTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DelayExec(uint execTime, DelayExecHelper.DelayExecItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(uint currTime)
    {
      int num;
      for (int index = 0; index < this.m_delayExecList.Count; index = num + 1)
      {
        DelayExecHelper.DelayExecItem delayExec = this.m_delayExecList[index];
        if (delayExec.m_execTime >= currTime)
          break;
        this.m_delayExecList.Remove(delayExec);
        num = index - 1;
        delayExec.m_action();
        if (delayExec.m_continueItem != null)
          this.DelayExec(delayExec.m_continueItem.m_execTime, delayExec.m_continueItem);
      }
    }

    public interface IDelayExecItem
    {
      DelayExecHelper.IDelayExecItem ContinueWith(Action action, int delayTime);
    }

    private class DelayExecItem : DelayExecHelper.IDelayExecItem
    {
      public Action m_action;
      public uint m_execTime;
      public DelayExecHelper.DelayExecItem m_continueItem;

      DelayExecHelper.IDelayExecItem DelayExecHelper.IDelayExecItem.ContinueWith(
        Action action,
        int delayTime)
      {
        this.m_continueItem = new DelayExecHelper.DelayExecItem()
        {
          m_action = action,
          m_execTime = this.m_execTime + (uint) delayTime
        };
        return (DelayExecHelper.IDelayExecItem) this.m_continueItem;
      }
    }
  }
}
