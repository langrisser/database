﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroDungeonLevellUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroDungeonLevellUnlockConditionType")]
  public enum HeroDungeonLevellUnlockConditionType
  {
    [ProtoEnum(Name = "HeroDungeonLevellUnlockConditionType_None", Value = 0)] HeroDungeonLevellUnlockConditionType_None,
    [ProtoEnum(Name = "HeroDungeonLevellUnlockConditionType_HeroFavorabilityLevel", Value = 1)] HeroDungeonLevellUnlockConditionType_HeroFavorabilityLevel,
  }
}
