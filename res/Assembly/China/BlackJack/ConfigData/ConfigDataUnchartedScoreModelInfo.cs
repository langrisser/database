﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataUnchartedScoreModelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataUnchartedScoreModelInfo")]
  [Serializable]
  public class ConfigDataUnchartedScoreModelInfo : IExtensible
  {
    private int _ID;
    private string _Model;
    private int _UI_ModelScale;
    private int _UI_ModelOffsetX;
    private int _UI_ModelOffsetY;
    private int _UI_ModelDir;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreModelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Model")]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UI_ModelScale")]
    public int UI_ModelScale
    {
      get
      {
        return this._UI_ModelScale;
      }
      set
      {
        this._UI_ModelScale = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UI_ModelOffsetX")]
    public int UI_ModelOffsetX
    {
      get
      {
        return this._UI_ModelOffsetX;
      }
      set
      {
        this._UI_ModelOffsetX = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UI_ModelOffsetY")]
    public int UI_ModelOffsetY
    {
      get
      {
        return this._UI_ModelOffsetY;
      }
      set
      {
        this._UI_ModelOffsetY = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UI_ModelDir")]
    public int UI_ModelDir
    {
      get
      {
        return this._UI_ModelDir;
      }
      set
      {
        this._UI_ModelDir = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
