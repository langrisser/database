﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCollectionActivityExchangeTableInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCollectionActivityExchangeTableInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataCollectionActivityExchangeTableInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _DisplayID;
    private List<LevelInfo> _PrevLevel;
    private List<Goods> _Rewards;
    private List<Goods> _Requirements;
    private int _ExchangeCountMax;
    private int _HoursBeforeActivate;
    private int _CoverHero;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityExchangeTableInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DisplayID")]
    public int DisplayID
    {
      get
      {
        return this._DisplayID;
      }
      set
      {
        this._DisplayID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "PrevLevel")]
    public List<LevelInfo> PrevLevel
    {
      get
      {
        return this._PrevLevel;
      }
      set
      {
        this._PrevLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "Rewards")]
    public List<Goods> Rewards
    {
      get
      {
        return this._Rewards;
      }
      set
      {
        this._Rewards = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "Requirements")]
    public List<Goods> Requirements
    {
      get
      {
        return this._Requirements;
      }
      set
      {
        this._Requirements = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExchangeCountMax")]
    public int ExchangeCountMax
    {
      get
      {
        return this._ExchangeCountMax;
      }
      set
      {
        this._ExchangeCountMax = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HoursBeforeActivate")]
    public int HoursBeforeActivate
    {
      get
      {
        return this._HoursBeforeActivate;
      }
      set
      {
        this._HoursBeforeActivate = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CoverHero")]
    public int CoverHero
    {
      get
      {
        return this._CoverHero;
      }
      set
      {
        this._CoverHero = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
