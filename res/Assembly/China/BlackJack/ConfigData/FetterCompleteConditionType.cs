﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FetterCompleteConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FetterCompleteConditionType")]
  public enum FetterCompleteConditionType
  {
    [ProtoEnum(Name = "FetterCompleteConditionType_None", Value = 0)] FetterCompleteConditionType_None,
    [ProtoEnum(Name = "FetterCompleteConditionType_HeroFavorabilityLevel", Value = 1)] FetterCompleteConditionType_HeroFavorabilityLevel,
    [ProtoEnum(Name = "FetterCompleteConditionType_Mission", Value = 2)] FetterCompleteConditionType_Mission,
  }
}
