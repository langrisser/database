﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroPerformanceUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroPerformanceUnlockConditionType")]
  public enum HeroPerformanceUnlockConditionType
  {
    [ProtoEnum(Name = "HeroPerformanceUnlockConditionType_None", Value = 0)] HeroPerformanceUnlockConditionType_None,
    [ProtoEnum(Name = "HeroPerformanceUnlockConditionType_HeroFavourabilityLevel", Value = 1)] HeroPerformanceUnlockConditionType_HeroFavourabilityLevel,
  }
}
