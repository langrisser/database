﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FlushRuleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FlushRuleType")]
  public enum FlushRuleType
  {
    [ProtoEnum(Name = "FlushRuleType_None", Value = 0)] FlushRuleType_None,
    [ProtoEnum(Name = "FlushRuleType_Period", Value = 1)] FlushRuleType_Period,
    [ProtoEnum(Name = "FlushRuleType_FixedTime", Value = 2)] FlushRuleType_FixedTime,
  }
}
