﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ItemDisplayType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ItemDisplayType")]
  public enum ItemDisplayType
  {
    [ProtoEnum(Name = "ItemDisplayType_None", Value = 0)] ItemDisplayType_None,
    [ProtoEnum(Name = "ItemDisplayType_Consumable", Value = 1)] ItemDisplayType_Consumable,
    [ProtoEnum(Name = "ItemDisplayType_HeroFragment", Value = 2)] ItemDisplayType_HeroFragment,
    [ProtoEnum(Name = "ItemDisplayType_JobMaterialFragment", Value = 3)] ItemDisplayType_JobMaterialFragment,
    [ProtoEnum(Name = "ItemDisplayType_Goblin", Value = 4)] ItemDisplayType_Goblin,
    [ProtoEnum(Name = "ItemDisplayType_UnchartedScore", Value = 5)] ItemDisplayType_UnchartedScore,
  }
}
