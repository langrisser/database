﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionEventAppearConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionEventAppearConditionType")]
  public enum CollectionEventAppearConditionType
  {
    [ProtoEnum(Name = "CollectionEventAppearConditionType_None", Value = 0)] CollectionEventAppearConditionType_None,
    [ProtoEnum(Name = "CollectionEventAppearConditionType_CompleteScenario", Value = 1)] CollectionEventAppearConditionType_CompleteScenario,
    [ProtoEnum(Name = "CollectionEventAppearConditionType_CompleteChallengeLevel", Value = 2)] CollectionEventAppearConditionType_CompleteChallengeLevel,
  }
}
