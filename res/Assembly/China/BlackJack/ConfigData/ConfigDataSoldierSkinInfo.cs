﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSoldierSkinInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSoldierSkinInfo")]
  [CustomLuaClass]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataSoldierSkinInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<Soldier2SkinResource> _ShowInListSkinResInfo;
    private List<Soldier2SkinResource> _SpecifiedSoldier;
    private string _Desc;
    private string _Icon;
    private List<GetPathData> _GetPathList;
    private string _GetPathDesc;
    private bool _IsShowBeforeGet;
    private IExtension extensionObject;
    public int FixedStoreItemId;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSoldierSkinInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "ShowInListSkinResInfo")]
    public List<Soldier2SkinResource> ShowInListSkinResInfo
    {
      get
      {
        return this._ShowInListSkinResInfo;
      }
      set
      {
        this._ShowInListSkinResInfo = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "SpecifiedSoldier")]
    public List<Soldier2SkinResource> SpecifiedSoldier
    {
      get
      {
        return this._SpecifiedSoldier;
      }
      set
      {
        this._SpecifiedSoldier = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "GetPathList")]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
      set
      {
        this._GetPathList = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "GetPathDesc")]
    public string GetPathDesc
    {
      get
      {
        return this._GetPathDesc;
      }
      set
      {
        this._GetPathDesc = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsShowBeforeGet")]
    public bool IsShowBeforeGet
    {
      get
      {
        return this._IsShowBeforeGet;
      }
      set
      {
        this._IsShowBeforeGet = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
