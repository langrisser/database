﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.GoddessDialogFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "GoddessDialogFuncType")]
  public enum GoddessDialogFuncType
  {
    [ProtoEnum(Name = "GoddessDialogFuncType_None", Value = 0)] GoddessDialogFuncType_None,
    [ProtoEnum(Name = "GoddessDialogFuncType_Start", Value = 1)] GoddessDialogFuncType_Start,
    [ProtoEnum(Name = "GoddessDialogFuncType_QuestionStart", Value = 2)] GoddessDialogFuncType_QuestionStart,
    [ProtoEnum(Name = "GoddessDialogFuncType_Result", Value = 3)] GoddessDialogFuncType_Result,
    [ProtoEnum(Name = "GoddessDialogFuncType_Select", Value = 4)] GoddessDialogFuncType_Select,
    [ProtoEnum(Name = "GoddessDialogFuncType_Final", Value = 5)] GoddessDialogFuncType_Final,
  }
}
