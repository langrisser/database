﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroHeartFetterUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroHeartFetterUnlockConditionType")]
  public enum HeroHeartFetterUnlockConditionType
  {
    [ProtoEnum(Name = "HeroHeartFetterUnlockConditionType_None", Value = 0)] HeroHeartFetterUnlockConditionType_None,
    [ProtoEnum(Name = "HeroHeartFetterUnlockConditionType_HeroFetterLevel", Value = 1)] HeroHeartFetterUnlockConditionType_HeroFetterLevel,
    [ProtoEnum(Name = "HeroHeartFetterUnlockConditionType_HeroLevel", Value = 2)] HeroHeartFetterUnlockConditionType_HeroLevel,
  }
}
