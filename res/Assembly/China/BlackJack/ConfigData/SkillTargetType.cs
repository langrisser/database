﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.SkillTargetType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "SkillTargetType")]
  public enum SkillTargetType
  {
    [ProtoEnum(Name = "SkillTargetType_None", Value = 0)] SkillTargetType_None,
    [ProtoEnum(Name = "SkillTargetType_Enemy", Value = 1)] SkillTargetType_Enemy,
    [ProtoEnum(Name = "SkillTargetType_Ally", Value = 2)] SkillTargetType_Ally,
    [ProtoEnum(Name = "SkillTargetType_Any", Value = 3)] SkillTargetType_Any,
    [ProtoEnum(Name = "SkillTargetType_Position", Value = 4)] SkillTargetType_Position,
    [ProtoEnum(Name = "SkillTargetType_EmptyPosition", Value = 5)] SkillTargetType_EmptyPosition,
    [ProtoEnum(Name = "SkillTargetType_NoNpcAlly", Value = 6)] SkillTargetType_NoNpcAlly,
    [ProtoEnum(Name = "SkillTargetType_NoSelfNpcAlly", Value = 7)] SkillTargetType_NoSelfNpcAlly,
    [ProtoEnum(Name = "SkillTargetType_Orientation", Value = 8)] SkillTargetType_Orientation,
  }
}
