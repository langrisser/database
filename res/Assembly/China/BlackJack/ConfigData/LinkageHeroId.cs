﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.LinkageHeroId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "LinkageHeroId")]
  public enum LinkageHeroId
  {
    [ProtoEnum(Name = "LinkageHeroId_None", Value = 0)] LinkageHeroId_None,
    [ProtoEnum(Name = "LinkageHeroId_KongGui", Value = 1)] LinkageHeroId_KongGui,
    [ProtoEnum(Name = "LinkageHeroId_YingZhan", Value = 2)] LinkageHeroId_YingZhan,
  }
}
