﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattleArmyRandomRuleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattleArmyRandomRuleType")]
  public enum BattleArmyRandomRuleType
  {
    [ProtoEnum(Name = "BattleArmyRandomRuleType_None", Value = 0)] BattleArmyRandomRuleType_None,
    [ProtoEnum(Name = "BattleArmyRandomRuleType_EveryTime", Value = 1)] BattleArmyRandomRuleType_EveryTime,
    [ProtoEnum(Name = "BattleArmyRandomRuleType_DailyTime", Value = 2)] BattleArmyRandomRuleType_DailyTime,
    [ProtoEnum(Name = "BattleArmyRandomRuleType_ClimbTower", Value = 3)] BattleArmyRandomRuleType_ClimbTower,
  }
}
