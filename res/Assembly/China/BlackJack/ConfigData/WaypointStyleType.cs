﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.WaypointStyleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "WaypointStyleType")]
  public enum WaypointStyleType
  {
    [ProtoEnum(Name = "WaypointStyleType_None", Value = 0)] WaypointStyleType_None,
    [ProtoEnum(Name = "WaypointStyleType_Forest", Value = 1)] WaypointStyleType_Forest,
    [ProtoEnum(Name = "WaypointStyleType_Mountain", Value = 2)] WaypointStyleType_Mountain,
    [ProtoEnum(Name = "WaypointStyleType_Cave", Value = 3)] WaypointStyleType_Cave,
    [ProtoEnum(Name = "WaypointStyleType_Village", Value = 4)] WaypointStyleType_Village,
    [ProtoEnum(Name = "WaypointStyleType_ActivityEvent", Value = 5)] WaypointStyleType_ActivityEvent,
  }
}
