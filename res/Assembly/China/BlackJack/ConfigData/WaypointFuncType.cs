﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.WaypointFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "WaypointFuncType")]
  public enum WaypointFuncType
  {
    [ProtoEnum(Name = "WaypointFuncType_None", Value = 0)] WaypointFuncType_None,
    [ProtoEnum(Name = "WaypointFuncType_Scenario", Value = 1)] WaypointFuncType_Scenario,
    [ProtoEnum(Name = "WaypointFuncType_Portal", Value = 2)] WaypointFuncType_Portal,
    [ProtoEnum(Name = "WaypointFuncType_Event", Value = 3)] WaypointFuncType_Event,
  }
}
