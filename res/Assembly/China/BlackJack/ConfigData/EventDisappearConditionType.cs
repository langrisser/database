﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EventDisappearConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "EventDisappearConditionType")]
  public enum EventDisappearConditionType
  {
    [ProtoEnum(Name = "EventDisappearConditionType_None", Value = 0)] EventDisappearConditionType_None,
    [ProtoEnum(Name = "EventDisappearConditionType_Complete", Value = 1)] EventDisappearConditionType_Complete,
    [ProtoEnum(Name = "EventDisappearConditionType_LifeTime", Value = 2)] EventDisappearConditionType_LifeTime,
  }
}
