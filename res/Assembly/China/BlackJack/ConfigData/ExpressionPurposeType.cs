﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ExpressionPurposeType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ExpressionPurposeType")]
  public enum ExpressionPurposeType
  {
    [ProtoEnum(Name = "ExpressionPurposeType_Chat", Value = 1)] ExpressionPurposeType_Chat = 1,
    [ProtoEnum(Name = "ExpressionPurposeType_Combat", Value = 2)] ExpressionPurposeType_Combat = 2,
  }
}
