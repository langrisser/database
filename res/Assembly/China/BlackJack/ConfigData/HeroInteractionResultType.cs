﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroInteractionResultType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroInteractionResultType")]
  public enum HeroInteractionResultType
  {
    [ProtoEnum(Name = "HeroInteractionResultType_None", Value = 0)] HeroInteractionResultType_None,
    [ProtoEnum(Name = "HeroInteractionResultType_Norml", Value = 1)] HeroInteractionResultType_Norml,
    [ProtoEnum(Name = "HeroInteractionResultType_SmallUp", Value = 2)] HeroInteractionResultType_SmallUp,
    [ProtoEnum(Name = "HeroInteractionResultType_BigUp", Value = 3)] HeroInteractionResultType_BigUp,
  }
}
