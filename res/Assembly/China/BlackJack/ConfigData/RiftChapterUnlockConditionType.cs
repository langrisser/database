﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftChapterUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftChapterUnlockConditionType")]
  public enum RiftChapterUnlockConditionType
  {
    [ProtoEnum(Name = "RiftChapterUnlockConditionType_None", Value = 0)] RiftChapterUnlockConditionType_None,
    [ProtoEnum(Name = "RiftChapterUnlockConditionType_PlayerLevel", Value = 1)] RiftChapterUnlockConditionType_PlayerLevel,
    [ProtoEnum(Name = "RiftChapterUnlockConditionType_Scenario", Value = 2)] RiftChapterUnlockConditionType_Scenario,
    [ProtoEnum(Name = "RiftChapterUnlockConditionType_ChapterStar", Value = 3)] RiftChapterUnlockConditionType_ChapterStar,
  }
}
