﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataOperationalActivityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataOperationalActivityInfo")]
  [Serializable]
  public class ConfigDataOperationalActivityInfo : IExtensible
  {
    private int _ID;
    private string _Title;
    private string _Content;
    private OperationalActivityType _ActivityType;
    private List<ActivityParam> _OperationalActivityParms;
    private int _SkipPageID;
    private int _SortID;
    private string _Image;
    private string _AdvertisingImage;
    private string _Desc;
    private int _DaysAfterServerOpen;
    private int _DaysAfterPlayerCreated;
    private int _NoEffectLastDays;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataOperationalActivityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Title")]
    public string Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Content")]
    public string Content
    {
      get
      {
        return this._Content;
      }
      set
      {
        this._Content = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActivityType")]
    public OperationalActivityType ActivityType
    {
      get
      {
        return this._ActivityType;
      }
      set
      {
        this._ActivityType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "OperationalActivityParms")]
    public List<ActivityParam> OperationalActivityParms
    {
      get
      {
        return this._OperationalActivityParms;
      }
      set
      {
        this._OperationalActivityParms = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkipPageID")]
    public int SkipPageID
    {
      get
      {
        return this._SkipPageID;
      }
      set
      {
        this._SkipPageID = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SortID")]
    public int SortID
    {
      get
      {
        return this._SortID;
      }
      set
      {
        this._SortID = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "Image")]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "AdvertisingImage")]
    public string AdvertisingImage
    {
      get
      {
        return this._AdvertisingImage;
      }
      set
      {
        this._AdvertisingImage = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DaysAfterServerOpen")]
    public int DaysAfterServerOpen
    {
      get
      {
        return this._DaysAfterServerOpen;
      }
      set
      {
        this._DaysAfterServerOpen = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DaysAfterPlayerCreated")]
    public int DaysAfterPlayerCreated
    {
      get
      {
        return this._DaysAfterPlayerCreated;
      }
      set
      {
        this._DaysAfterPlayerCreated = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NoEffectLastDays")]
    public int NoEffectLastDays
    {
      get
      {
        return this._NoEffectLastDays;
      }
      set
      {
        this._NoEffectLastDays = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
