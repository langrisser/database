﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRandomDropRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataRandomDropRewardInfo")]
  [Serializable]
  public class ConfigDataRandomDropRewardInfo : IExtensible
  {
    private int _ID;
    private int _DropID;
    private int _GroupIndex;
    private int _DropCount;
    private List<WeightGoods> _DropRewards;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomDropRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DropID")]
    public int DropID
    {
      get
      {
        return this._DropID;
      }
      set
      {
        this._DropID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GroupIndex")]
    public int GroupIndex
    {
      get
      {
        return this._GroupIndex;
      }
      set
      {
        this._GroupIndex = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DropCount")]
    public int DropCount
    {
      get
      {
        return this._DropCount;
      }
      set
      {
        this._DropCount = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "DropRewards")]
    public List<WeightGoods> DropRewards
    {
      get
      {
        return this._DropRewards;
      }
      set
      {
        this._DropRewards = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
