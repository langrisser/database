﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattleLoseConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattleLoseConditionType")]
  public enum BattleLoseConditionType
  {
    [ProtoEnum(Name = "BattleLoseConditionType_None", Value = 0)] BattleLoseConditionType_None,
    [ProtoEnum(Name = "BattleLoseConditionType_DieAll", Value = 1)] BattleLoseConditionType_DieAll,
    [ProtoEnum(Name = "BattleLoseConditionType_ActorDie", Value = 2)] BattleLoseConditionType_ActorDie,
    [ProtoEnum(Name = "BattleLoseConditionType_ActorReachPosition", Value = 3)] BattleLoseConditionType_ActorReachPosition,
    [ProtoEnum(Name = "BattleLoseConditionType_DieCount", Value = 4)] BattleLoseConditionType_DieCount,
    [ProtoEnum(Name = "BattleLoseConditionType_PlayerDieCount", Value = 5)] BattleLoseConditionType_PlayerDieCount,
    [ProtoEnum(Name = "BattleLoseConditionType_NpcDieCount", Value = 6)] BattleLoseConditionType_NpcDieCount,
    [ProtoEnum(Name = "BattleLoseConditionType_EventTrigger", Value = 7)] BattleLoseConditionType_EventTrigger,
  }
}
