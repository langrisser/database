﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Static.ProjectLGameEntryUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.BJFramework.Runtime.Resource;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectLBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Static
{
  public class ProjectLGameEntryUITask : GameEntryUITaskBase
  {
    private UITaskBase.LayerDesc[] m_layerDescArray = new UITaskBase.LayerDesc[1]
    {
      new UITaskBase.LayerDesc()
      {
        m_layerName = "EntryUILayer",
        m_layerResPath = "Assets/GameProject/Resources/UI/EntryUIPrefab.prefab",
        m_isUILayer = true
      }
    };
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray = new UITaskBase.UIControllerDesc[1]
    {
      new UITaskBase.UIControllerDesc()
      {
        m_attachLayerName = "EntryUILayer",
        m_attachPath = "./",
        m_ctrlTypeDNName = new TypeDNName("BlackJack.ProjectL.Static.ProjectLGameEntryUIController"),
        m_ctrlName = "ProjectLGameEntryUIController"
      }
    };
    private List<string> m_preloadMessage = new List<string>(10);
    private ProjectLGameEntryUIController m_mainCtrl;
    private float m_lastUpdateProgress;
    private long m_lastDownloadedBytes;
    private float m_lastUpdateProgressTime;
    private bool m_isUpdatePreloadProgress;
    private bool m_isUpdateLoadConfigProgress;
    private float m_lastUpdateMessageTime;
    private long m_downloadAudioLength;
    private const double m_byte2MB = 9.5367431640625E-07;
    public const string m_isUseLowResFlagName = "IsUseLowResFlagName";

    [MethodImpl((MethodImplOptions) 32768)]
    public ProjectLGameEntryUITask(string taskName)
      : base(taskName)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      base.OnTick();
      if ((UnityEngine.Object) this.m_mainCtrl == (UnityEngine.Object) null)
        return;
      this.UpdatePreloadProgress();
      this.UpdateLoadConfigProgress();
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CheckWorkingPathChar()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ProjectLGameEntryUITask.\u003CCheckWorkingPathChar\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator OnBasicVersionUnmatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      base.InitAllUIControllers();
      if (!((UnityEngine.Object) this.m_mainCtrl == (UnityEngine.Object) null))
        return;
      if (this.m_uiCtrlArray.Length > 0)
        this.m_mainCtrl = this.m_uiCtrlArray[0] as ProjectLGameEntryUIController;
      if (!((UnityEngine.Object) this.m_mainCtrl == (UnityEngine.Object) null))
        return;
      Debug.LogError("ProjectLGameEntryUIController is null");
    }

    protected override void UpdateView()
    {
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CheckNetwork()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ProjectLGameEntryUITask.\u003CCheckNetwork\u003Ec__Iterator2()
      {
        \u0024this = this
      };
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CheckIfUseLowResource()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ProjectLGameEntryUITask.\u003CCheckIfUseLowResource\u003Ec__Iterator3()
      {
        \u0024this = this
      };
    }

    public static bool IsUseLowResource
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static bool HasEverDownloadLowResource
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator EntryPipeLine()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ProjectLGameEntryUITask.\u003CEntryPipeLine\u003Ec__Iterator4()
      {
        \u0024this = this
      };
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator DownloadAudioFiles()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStreamingAssetsFilesProcessingStart()
    {
      this.m_mainCtrl.SetMesssage(StringTable.Get("MsgCheckingLocalResources"));
    }

    protected override void OnStreamingAssetsFilesProcessingEnd(bool ret)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBundleDataLoadingStart()
    {
      this.m_mainCtrl.SetMesssage(StringTable.Get("MsgConnectingServer"));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBundleDataLoadingEnd(bool ret)
    {
      if (!ret)
        this.m_mainCtrl.StartCoroutine(DialogBox.Show(this.m_mainCtrl.gameObject, "Assets/GameProject/Resources/UI/DialogBox.prefab", StringTable.Get("MsgUpdateClinetFailed"), StringTable.Get("ButtonOK"), string.Empty, (Action<DialogBoxResult>) (result => Application.Quit())));
      else
        this.m_mainCtrl.AddProgress(0.1f, 1f);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void NotifyUserDownloadAndWait(long totalDownloadByte)
    {
      this.m_mainCtrl.StartCoroutine(this.GetAudioDownloadLengthAndNotifyUser(totalDownloadByte));
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator GetAudioDownloadLengthAndNotifyUser(long downloadAssetbundleBytes)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ProjectLGameEntryUITask.\u003CGetAudioDownloadLengthAndNotifyUser\u003Ec__Iterator6()
      {
        downloadAssetbundleBytes = downloadAssetbundleBytes,
        \u0024this = this
      };
    }

    protected override void OnAssetBundlePreUpdateingRefuse()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnAssetBundlePreUpdateingStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnAssetBundlePreUpdateingEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnAssetBundleManifestLoadingStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnAssetBundleManifestLoadingEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnLoadDynamicAssemblysStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnLoadDynamicAssemblysEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStartLuaManagerStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStartLuaManagerEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnLoadConfigDataStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnLoadConfigDataEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStartAudioManagerStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStartAudioManagerEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPreloadMessage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RandomUpdatePreloadMessage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void LaunchLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateLoadConfigProgress()
    {
      if (!this.m_isUpdateLoadConfigProgress || (UnityEngine.Object) this.m_mainCtrl == (UnityEngine.Object) null)
        return;
      ClientConfigDataLoader configDataLoader = GameManager.Instance.ConfigDataLoader as ClientConfigDataLoader;
      if (configDataLoader == null || (double) configDataLoader.LoadingProgress > 0.999989986419678)
        return;
      this.m_mainCtrl.SetProgress((float) (0.699999988079071 + 0.28999999165535 * (double) configDataLoader.LoadingProgress));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePreloadProgress()
    {
      if (!this.m_isUpdatePreloadProgress)
        return;
      if ((double) Time.unscaledTime > (double) this.m_lastUpdateMessageTime + 4.0)
      {
        this.RandomUpdatePreloadMessage();
        this.m_lastUpdateMessageTime = Time.unscaledTime;
      }
      if ((double) Time.unscaledTime > (double) this.m_lastUpdateProgressTime + 0.200000002980232)
      {
        this.m_mainCtrl.SetProgress(ResourceManager.Instance.m_loadingProgress);
        this.m_lastUpdateProgressTime = Time.unscaledTime;
      }
      if ((double) ResourceManager.Instance.m_loadingProgress <= 0.949999988079071)
        return;
      this.m_isUpdatePreloadProgress = false;
      Shader.WarmupAllShaders();
    }

    protected override void UpdateView4StreamingAssetsFilesProcessing()
    {
    }

    protected override void UpdateView4BundleDataLoading()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView4AssetBundlePreUpdateing()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void UpdateView4AssetBundleManifestLoading()
    {
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
