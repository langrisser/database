﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomViewNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TeamRoomViewNetTask : UINetTask
  {
    private GameFunctionType m_gameFunctionTypeId;
    private int m_chapterId;
    private int m_locationId;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomViewNetTask(GameFunctionType gameFunctionTypeId, int chapterId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnTeamRoomViewAck(int result, List<TeamRoom> rooms)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<TeamRoom> TeamRooms { private set; get; }
  }
}
