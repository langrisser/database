﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PreCombatUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PreCombatUIController : UIControllerBase
  {
    [AutoBind("./BackgroundImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./BattlePreview", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battlePreviewStateCtrl;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/TerrainInfo0/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0TerrainText;
    [AutoBind("./Margin/TerrainInfo0/Def/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0TerrainDefText;
    [AutoBind("./Margin/TerrainInfo0/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0TerrainImage;
    [AutoBind("./Margin/Actor0", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0UIStateController;
    [AutoBind("./Margin/Actor0/CharPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0CharUIStateController;
    [AutoBind("./Margin/Actor0/CharPanel/CharIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0CharIcon;
    [AutoBind("./Margin/Actor0/CharInfoGroup/SkillGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor0SkillGo;
    [AutoBind("./Margin/Actor0/CharInfoGroup/SkillGroup/SkillIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0SkillIconImage;
    [AutoBind("./Margin/Actor0/CharInfoGroup/TypeIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0TypeIcon;
    [AutoBind("./Margin/Actor0/CharInfoGroup/HpGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0HpText;
    [AutoBind("./Margin/Actor0/CharInfoGroup/HpGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0HpProgressBar;
    [AutoBind("./Margin/Actor0/CharInfoGroup/HpGroup/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor0HeroHpFxGameObeject;
    [AutoBind("./Margin/Actor0/CharInfoGroup/Proprety1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0Proprety1NameText;
    [AutoBind("./Margin/Actor0/CharInfoGroup/Proprety1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0Proprety1ValueText;
    [AutoBind("./Margin/Actor0/CharInfoGroup/Proprety2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0Proprety2NameText;
    [AutoBind("./Margin/Actor0/CharInfoGroup/Proprety2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0Proprety2ValueText;
    [AutoBind("./Margin/Actor0/CharInfoGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0NameText;
    [AutoBind("./Margin/Actor0/CharInfoGroup/LvText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0LvValueText;
    [AutoBind("./Margin/Actor0/SituationGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0SituationGroup;
    [AutoBind("./Margin/Actor0/SoldierInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor0SoldierGroupGameObject;
    [AutoBind("./Margin/Actor0/SoldierInfoGroup/TypeIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0SoldierTypeIcon;
    [AutoBind("./Margin/Actor0/SoldierInfoGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0SoldierNameText;
    [AutoBind("./Margin/Actor0/SoldierInfoGroup/SoldierHpGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0SoldierHpProgressBar;
    [AutoBind("./Margin/Actor0/SoldierInfoGroup/SoldierHpGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0SoldierHpText;
    [AutoBind("./Margin/Actor0/SoldierInfoGroup/SoldierHpGroup/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor0SoldierHpFxGameObeject;
    [AutoBind("./Margin/Actor0/Damage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0DamageUIStateController;
    [AutoBind("./Margin/Actor0/Damage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0DamageText;
    [AutoBind("./Margin/Actor0/Damage/CriticalText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0DamageCriticalText;
    [AutoBind("./Margin/Actor0/PassiveSkill", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0PassiveSkillUIStateController;
    [AutoBind("./Margin/Actor0/PassiveSkill/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0PassiveSkillIconImage;
    [AutoBind("./Margin/Actor0/PassiveSkill/TalentIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0PassiveSkillTalentIconImage;
    [AutoBind("./Margin/Actor0/PassiveSkill/EquipIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor0PassiveSkillEquipIconGameObject;
    [AutoBind("./Margin/Actor0/PassiveSkill/EquipIconImage/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0PassiveSkillEquipIconImage;
    [AutoBind("./Margin/Actor0/PassiveSkill/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0PassiveSkillNameText;
    [AutoBind("./Margin/TerrainInfo1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1TerrainText;
    [AutoBind("./Margin/TerrainInfo1/Def/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1TerrainDefText;
    [AutoBind("./Margin/TerrainInfo1/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1TerrainImage;
    [AutoBind("./Margin/Actor1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1UIStateController;
    [AutoBind("./Margin/Actor1/CharPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1CharUIStateController;
    [AutoBind("./Margin/Actor1/CharPanel/CharIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1CharIcon;
    [AutoBind("./Margin/Actor1/CharInfoGroup/SkillGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor1SkillGo;
    [AutoBind("./Margin/Actor1/CharInfoGroup/SkillGroup/SkillIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1SkillIconImage;
    [AutoBind("./Margin/Actor1/CharInfoGroup/TypeIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1TypeIcon;
    [AutoBind("./Margin/Actor1/CharInfoGroup/HpGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1HpText;
    [AutoBind("./Margin/Actor1/CharInfoGroup/HpGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1HpProgressBar;
    [AutoBind("./Margin/Actor1/CharInfoGroup/HpGroup/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor1HeroHpFxGameObeject;
    [AutoBind("./Margin/Actor1/CharInfoGroup/Proprety1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1Proprety1NameText;
    [AutoBind("./Margin/Actor1/CharInfoGroup/Proprety1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1Proprety1ValueText;
    [AutoBind("./Margin/Actor1/CharInfoGroup/Proprety2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1Proprety2NameText;
    [AutoBind("./Margin/Actor1/CharInfoGroup/Proprety2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1Proprety2ValueText;
    [AutoBind("./Margin/Actor1/CharInfoGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1NameText;
    [AutoBind("./Margin/Actor1/CharInfoGroup/LvText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1LvValueText;
    [AutoBind("./Margin/Actor1/SituationGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1SituationGroup;
    [AutoBind("./Margin/Actor1/SoldierInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor1SoldierGroupGameObject;
    [AutoBind("./Margin/Actor1/SoldierInfoGroup/TypeIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1SoldierTypeIcon;
    [AutoBind("./Margin/Actor1/SoldierInfoGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1SoldierNameText;
    [AutoBind("./Margin/Actor1/SoldierInfoGroup/SoldierHpGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1SoldierHpProgressBar;
    [AutoBind("./Margin/Actor1/SoldierInfoGroup/SoldierHpGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1SoldierHpText;
    [AutoBind("./Margin/Actor1/SoldierInfoGroup/SoldierHpGroup/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor1SoldierHpFxGameObeject;
    [AutoBind("./Margin/Actor1/Damage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1DamageUIStateController;
    [AutoBind("./Margin/Actor1/Damage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1DamageText;
    [AutoBind("./Margin/Actor1/Damage/CriticalText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1DamageCriticalText;
    [AutoBind("./Margin/Actor1/PassiveSkill", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1PassiveSkillUIStateController;
    [AutoBind("./Margin/Actor1/PassiveSkill/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1PassiveSkillIconImage;
    [AutoBind("./Margin/Actor1/PassiveSkill/TalentIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1PassiveSkillTalentIconImage;
    [AutoBind("./Margin/Actor1/PassiveSkill/EquipIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor1PassiveSkillEquipIconGameObject;
    [AutoBind("./Margin/Actor1/PassiveSkill/EquipIconImage/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1PassiveSkillEquipIconImage;
    [AutoBind("./Margin/Actor1/PassiveSkill/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1PassiveSkillNameText;
    [AutoBind("./ButtonGroup/OkButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_okButton;
    [AutoBind("./ButtonGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelButton;
    private int m_armyRelationValue;
    private int m_totalDamage0;
    private int m_totalDamage1;
    private FxPlayer m_fxPlayer;
    private bool m_isOpened;
    private bool m_isFastCombat;
    private HeroPropertyComputer m_propertyComputer;
    [DoNotToLua]
    private PreCombatUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnDisable_hotfix;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitializeFxPlayer_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_IsOpened_hotfix;
    private LuaFunction m_IsFastCombat_hotfix;
    private LuaFunction m_Show_hotfix;
    private LuaFunction m_ShowOkCancelButtonBoolean_hotfix;
    private LuaFunction m_SetBattleActorInfoBattleActorBattleActorConfigDataTerrainInfoConfigDataTerrainInfoBooleanBooleanInt32ConfigDataSkillInfoInt32_hotfix;
    private LuaFunction m_SetHeroHpInt32Int32Int32_hotfix;
    private LuaFunction m_SetSoldierHpInt32Int32Int32_hotfix;
    private LuaFunction m_OpenAndShowFastCombatFastCombatActorInfoFastCombatActorInfo_hotfix;
    private LuaFunction m_Co_OpenAndFastCombatFastCombatActorInfoFastCombatActorInfo_hotfix;
    private LuaFunction m_ShowFastCombatFastCombatActorInfoFastCombatActorInfo_hotfix;
    private LuaFunction m_Co_FastCombatFastCombatActorInfoFastCombatActorInfo_hotfix;
    private LuaFunction m_Co_PlayDamageInt32FastCombatActorInfo_hotfix;
    private LuaFunction m_Co_PlayDamageGameObjectInt32Int32Int32Int32BooleanInt32Boolean_hotfix;
    private LuaFunction m_PlayDamageFxGameObjectInt32Int32Int32Int32Boolean_hotfix;
    private LuaFunction m_OnActorPassiveSkillInt32BuffState_hotfix;
    private LuaFunction m_Close_hotfix;
    private LuaFunction m_OnOkButtonClick_hotfix;
    private LuaFunction m_OnCancelButtonClick_hotfix;
    private LuaFunction m_OnBackgroundButtonClick_hotfix;
    private LuaFunction m_add_EventOnOkAction_hotfix;
    private LuaFunction m_remove_EventOnOkAction_hotfix;
    private LuaFunction m_add_EventOnCancelAction_hotfix;
    private LuaFunction m_remove_EventOnCancelAction_hotfix;
    private LuaFunction m_add_EventOnStopAction_hotfix;
    private LuaFunction m_remove_EventOnStopAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private PreCombatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(FxPlayer fxPlayer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFastCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Show()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowOkCancelButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleActorInfo(
      BattleActor a0,
      BattleActor a1,
      ConfigDataTerrainInfo terrain0,
      ConfigDataTerrainInfo terrain1,
      bool isMagicAttack0,
      bool isMagicAttack1,
      int armyRelationValue,
      ConfigDataSkillInfo attackerSkillInfo,
      int attackerSide)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroHp(int side, int hp, int hpMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierHp(int side, int hp, int hpMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenAndShowFastCombat(FastCombatActorInfo a0, FastCombatActorInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_OpenAndFastCombat(
      FastCombatActorInfo a0,
      FastCombatActorInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFastCombat(FastCombatActorInfo a0, FastCombatActorInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_FastCombat(FastCombatActorInfo a0, FastCombatActorInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PlayDamage(int side, FastCombatActorInfo a)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PlayDamage(
      GameObject fxParent,
      int side,
      int beforeHp,
      int afterHp,
      int hpMax,
      bool isHero,
      int totalDamage,
      bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayDamageFx(
      GameObject fxParent,
      int side,
      int hp,
      int hpMax,
      int damage,
      bool isLargeFx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorPassiveSkill(int side, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOkButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnOk
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCancel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnStop
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PreCombatUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnOk()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnOk()
    {
      this.EventOnOk = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCancel()
    {
      this.EventOnCancel = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStop()
    {
      this.EventOnStop = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PreCombatUIController m_owner;

      public LuaExportHelper(PreCombatUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnOk()
      {
        this.m_owner.__callDele_EventOnOk();
      }

      public void __clearDele_EventOnOk()
      {
        this.m_owner.__clearDele_EventOnOk();
      }

      public void __callDele_EventOnCancel()
      {
        this.m_owner.__callDele_EventOnCancel();
      }

      public void __clearDele_EventOnCancel()
      {
        this.m_owner.__clearDele_EventOnCancel();
      }

      public void __callDele_EventOnStop()
      {
        this.m_owner.__callDele_EventOnStop();
      }

      public void __clearDele_EventOnStop()
      {
        this.m_owner.__clearDele_EventOnStop();
      }

      public Button m_backgroundButton
      {
        get
        {
          return this.m_owner.m_backgroundButton;
        }
        set
        {
          this.m_owner.m_backgroundButton = value;
        }
      }

      public CommonUIStateController m_battlePreviewStateCtrl
      {
        get
        {
          return this.m_owner.m_battlePreviewStateCtrl;
        }
        set
        {
          this.m_owner.m_battlePreviewStateCtrl = value;
        }
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Text m_actor0TerrainText
      {
        get
        {
          return this.m_owner.m_actor0TerrainText;
        }
        set
        {
          this.m_owner.m_actor0TerrainText = value;
        }
      }

      public Text m_actor0TerrainDefText
      {
        get
        {
          return this.m_owner.m_actor0TerrainDefText;
        }
        set
        {
          this.m_owner.m_actor0TerrainDefText = value;
        }
      }

      public Image m_actor0TerrainImage
      {
        get
        {
          return this.m_owner.m_actor0TerrainImage;
        }
        set
        {
          this.m_owner.m_actor0TerrainImage = value;
        }
      }

      public CommonUIStateController m_actor0UIStateController
      {
        get
        {
          return this.m_owner.m_actor0UIStateController;
        }
        set
        {
          this.m_owner.m_actor0UIStateController = value;
        }
      }

      public CommonUIStateController m_actor0CharUIStateController
      {
        get
        {
          return this.m_owner.m_actor0CharUIStateController;
        }
        set
        {
          this.m_owner.m_actor0CharUIStateController = value;
        }
      }

      public Image m_actor0CharIcon
      {
        get
        {
          return this.m_owner.m_actor0CharIcon;
        }
        set
        {
          this.m_owner.m_actor0CharIcon = value;
        }
      }

      public GameObject m_actor0SkillGo
      {
        get
        {
          return this.m_owner.m_actor0SkillGo;
        }
        set
        {
          this.m_owner.m_actor0SkillGo = value;
        }
      }

      public Image m_actor0SkillIconImage
      {
        get
        {
          return this.m_owner.m_actor0SkillIconImage;
        }
        set
        {
          this.m_owner.m_actor0SkillIconImage = value;
        }
      }

      public Image m_actor0TypeIcon
      {
        get
        {
          return this.m_owner.m_actor0TypeIcon;
        }
        set
        {
          this.m_owner.m_actor0TypeIcon = value;
        }
      }

      public Text m_actor0HpText
      {
        get
        {
          return this.m_owner.m_actor0HpText;
        }
        set
        {
          this.m_owner.m_actor0HpText = value;
        }
      }

      public Image m_actor0HpProgressBar
      {
        get
        {
          return this.m_owner.m_actor0HpProgressBar;
        }
        set
        {
          this.m_owner.m_actor0HpProgressBar = value;
        }
      }

      public GameObject m_actor0HeroHpFxGameObeject
      {
        get
        {
          return this.m_owner.m_actor0HeroHpFxGameObeject;
        }
        set
        {
          this.m_owner.m_actor0HeroHpFxGameObeject = value;
        }
      }

      public CommonUIStateController m_actor0Proprety1NameText
      {
        get
        {
          return this.m_owner.m_actor0Proprety1NameText;
        }
        set
        {
          this.m_owner.m_actor0Proprety1NameText = value;
        }
      }

      public Text m_actor0Proprety1ValueText
      {
        get
        {
          return this.m_owner.m_actor0Proprety1ValueText;
        }
        set
        {
          this.m_owner.m_actor0Proprety1ValueText = value;
        }
      }

      public CommonUIStateController m_actor0Proprety2NameText
      {
        get
        {
          return this.m_owner.m_actor0Proprety2NameText;
        }
        set
        {
          this.m_owner.m_actor0Proprety2NameText = value;
        }
      }

      public Text m_actor0Proprety2ValueText
      {
        get
        {
          return this.m_owner.m_actor0Proprety2ValueText;
        }
        set
        {
          this.m_owner.m_actor0Proprety2ValueText = value;
        }
      }

      public Text m_actor0NameText
      {
        get
        {
          return this.m_owner.m_actor0NameText;
        }
        set
        {
          this.m_owner.m_actor0NameText = value;
        }
      }

      public Text m_actor0LvValueText
      {
        get
        {
          return this.m_owner.m_actor0LvValueText;
        }
        set
        {
          this.m_owner.m_actor0LvValueText = value;
        }
      }

      public CommonUIStateController m_actor0SituationGroup
      {
        get
        {
          return this.m_owner.m_actor0SituationGroup;
        }
        set
        {
          this.m_owner.m_actor0SituationGroup = value;
        }
      }

      public GameObject m_actor0SoldierGroupGameObject
      {
        get
        {
          return this.m_owner.m_actor0SoldierGroupGameObject;
        }
        set
        {
          this.m_owner.m_actor0SoldierGroupGameObject = value;
        }
      }

      public Image m_actor0SoldierTypeIcon
      {
        get
        {
          return this.m_owner.m_actor0SoldierTypeIcon;
        }
        set
        {
          this.m_owner.m_actor0SoldierTypeIcon = value;
        }
      }

      public Text m_actor0SoldierNameText
      {
        get
        {
          return this.m_owner.m_actor0SoldierNameText;
        }
        set
        {
          this.m_owner.m_actor0SoldierNameText = value;
        }
      }

      public Image m_actor0SoldierHpProgressBar
      {
        get
        {
          return this.m_owner.m_actor0SoldierHpProgressBar;
        }
        set
        {
          this.m_owner.m_actor0SoldierHpProgressBar = value;
        }
      }

      public Text m_actor0SoldierHpText
      {
        get
        {
          return this.m_owner.m_actor0SoldierHpText;
        }
        set
        {
          this.m_owner.m_actor0SoldierHpText = value;
        }
      }

      public GameObject m_actor0SoldierHpFxGameObeject
      {
        get
        {
          return this.m_owner.m_actor0SoldierHpFxGameObeject;
        }
        set
        {
          this.m_owner.m_actor0SoldierHpFxGameObeject = value;
        }
      }

      public CommonUIStateController m_actor0DamageUIStateController
      {
        get
        {
          return this.m_owner.m_actor0DamageUIStateController;
        }
        set
        {
          this.m_owner.m_actor0DamageUIStateController = value;
        }
      }

      public Text m_actor0DamageText
      {
        get
        {
          return this.m_owner.m_actor0DamageText;
        }
        set
        {
          this.m_owner.m_actor0DamageText = value;
        }
      }

      public Text m_actor0DamageCriticalText
      {
        get
        {
          return this.m_owner.m_actor0DamageCriticalText;
        }
        set
        {
          this.m_owner.m_actor0DamageCriticalText = value;
        }
      }

      public CommonUIStateController m_actor0PassiveSkillUIStateController
      {
        get
        {
          return this.m_owner.m_actor0PassiveSkillUIStateController;
        }
        set
        {
          this.m_owner.m_actor0PassiveSkillUIStateController = value;
        }
      }

      public Image m_actor0PassiveSkillIconImage
      {
        get
        {
          return this.m_owner.m_actor0PassiveSkillIconImage;
        }
        set
        {
          this.m_owner.m_actor0PassiveSkillIconImage = value;
        }
      }

      public Image m_actor0PassiveSkillTalentIconImage
      {
        get
        {
          return this.m_owner.m_actor0PassiveSkillTalentIconImage;
        }
        set
        {
          this.m_owner.m_actor0PassiveSkillTalentIconImage = value;
        }
      }

      public GameObject m_actor0PassiveSkillEquipIconGameObject
      {
        get
        {
          return this.m_owner.m_actor0PassiveSkillEquipIconGameObject;
        }
        set
        {
          this.m_owner.m_actor0PassiveSkillEquipIconGameObject = value;
        }
      }

      public Image m_actor0PassiveSkillEquipIconImage
      {
        get
        {
          return this.m_owner.m_actor0PassiveSkillEquipIconImage;
        }
        set
        {
          this.m_owner.m_actor0PassiveSkillEquipIconImage = value;
        }
      }

      public Text m_actor0PassiveSkillNameText
      {
        get
        {
          return this.m_owner.m_actor0PassiveSkillNameText;
        }
        set
        {
          this.m_owner.m_actor0PassiveSkillNameText = value;
        }
      }

      public Text m_actor1TerrainText
      {
        get
        {
          return this.m_owner.m_actor1TerrainText;
        }
        set
        {
          this.m_owner.m_actor1TerrainText = value;
        }
      }

      public Text m_actor1TerrainDefText
      {
        get
        {
          return this.m_owner.m_actor1TerrainDefText;
        }
        set
        {
          this.m_owner.m_actor1TerrainDefText = value;
        }
      }

      public Image m_actor1TerrainImage
      {
        get
        {
          return this.m_owner.m_actor1TerrainImage;
        }
        set
        {
          this.m_owner.m_actor1TerrainImage = value;
        }
      }

      public CommonUIStateController m_actor1UIStateController
      {
        get
        {
          return this.m_owner.m_actor1UIStateController;
        }
        set
        {
          this.m_owner.m_actor1UIStateController = value;
        }
      }

      public CommonUIStateController m_actor1CharUIStateController
      {
        get
        {
          return this.m_owner.m_actor1CharUIStateController;
        }
        set
        {
          this.m_owner.m_actor1CharUIStateController = value;
        }
      }

      public Image m_actor1CharIcon
      {
        get
        {
          return this.m_owner.m_actor1CharIcon;
        }
        set
        {
          this.m_owner.m_actor1CharIcon = value;
        }
      }

      public GameObject m_actor1SkillGo
      {
        get
        {
          return this.m_owner.m_actor1SkillGo;
        }
        set
        {
          this.m_owner.m_actor1SkillGo = value;
        }
      }

      public Image m_actor1SkillIconImage
      {
        get
        {
          return this.m_owner.m_actor1SkillIconImage;
        }
        set
        {
          this.m_owner.m_actor1SkillIconImage = value;
        }
      }

      public Image m_actor1TypeIcon
      {
        get
        {
          return this.m_owner.m_actor1TypeIcon;
        }
        set
        {
          this.m_owner.m_actor1TypeIcon = value;
        }
      }

      public Text m_actor1HpText
      {
        get
        {
          return this.m_owner.m_actor1HpText;
        }
        set
        {
          this.m_owner.m_actor1HpText = value;
        }
      }

      public Image m_actor1HpProgressBar
      {
        get
        {
          return this.m_owner.m_actor1HpProgressBar;
        }
        set
        {
          this.m_owner.m_actor1HpProgressBar = value;
        }
      }

      public GameObject m_actor1HeroHpFxGameObeject
      {
        get
        {
          return this.m_owner.m_actor1HeroHpFxGameObeject;
        }
        set
        {
          this.m_owner.m_actor1HeroHpFxGameObeject = value;
        }
      }

      public CommonUIStateController m_actor1Proprety1NameText
      {
        get
        {
          return this.m_owner.m_actor1Proprety1NameText;
        }
        set
        {
          this.m_owner.m_actor1Proprety1NameText = value;
        }
      }

      public Text m_actor1Proprety1ValueText
      {
        get
        {
          return this.m_owner.m_actor1Proprety1ValueText;
        }
        set
        {
          this.m_owner.m_actor1Proprety1ValueText = value;
        }
      }

      public CommonUIStateController m_actor1Proprety2NameText
      {
        get
        {
          return this.m_owner.m_actor1Proprety2NameText;
        }
        set
        {
          this.m_owner.m_actor1Proprety2NameText = value;
        }
      }

      public Text m_actor1Proprety2ValueText
      {
        get
        {
          return this.m_owner.m_actor1Proprety2ValueText;
        }
        set
        {
          this.m_owner.m_actor1Proprety2ValueText = value;
        }
      }

      public Text m_actor1NameText
      {
        get
        {
          return this.m_owner.m_actor1NameText;
        }
        set
        {
          this.m_owner.m_actor1NameText = value;
        }
      }

      public Text m_actor1LvValueText
      {
        get
        {
          return this.m_owner.m_actor1LvValueText;
        }
        set
        {
          this.m_owner.m_actor1LvValueText = value;
        }
      }

      public CommonUIStateController m_actor1SituationGroup
      {
        get
        {
          return this.m_owner.m_actor1SituationGroup;
        }
        set
        {
          this.m_owner.m_actor1SituationGroup = value;
        }
      }

      public GameObject m_actor1SoldierGroupGameObject
      {
        get
        {
          return this.m_owner.m_actor1SoldierGroupGameObject;
        }
        set
        {
          this.m_owner.m_actor1SoldierGroupGameObject = value;
        }
      }

      public Image m_actor1SoldierTypeIcon
      {
        get
        {
          return this.m_owner.m_actor1SoldierTypeIcon;
        }
        set
        {
          this.m_owner.m_actor1SoldierTypeIcon = value;
        }
      }

      public Text m_actor1SoldierNameText
      {
        get
        {
          return this.m_owner.m_actor1SoldierNameText;
        }
        set
        {
          this.m_owner.m_actor1SoldierNameText = value;
        }
      }

      public Image m_actor1SoldierHpProgressBar
      {
        get
        {
          return this.m_owner.m_actor1SoldierHpProgressBar;
        }
        set
        {
          this.m_owner.m_actor1SoldierHpProgressBar = value;
        }
      }

      public Text m_actor1SoldierHpText
      {
        get
        {
          return this.m_owner.m_actor1SoldierHpText;
        }
        set
        {
          this.m_owner.m_actor1SoldierHpText = value;
        }
      }

      public GameObject m_actor1SoldierHpFxGameObeject
      {
        get
        {
          return this.m_owner.m_actor1SoldierHpFxGameObeject;
        }
        set
        {
          this.m_owner.m_actor1SoldierHpFxGameObeject = value;
        }
      }

      public CommonUIStateController m_actor1DamageUIStateController
      {
        get
        {
          return this.m_owner.m_actor1DamageUIStateController;
        }
        set
        {
          this.m_owner.m_actor1DamageUIStateController = value;
        }
      }

      public Text m_actor1DamageText
      {
        get
        {
          return this.m_owner.m_actor1DamageText;
        }
        set
        {
          this.m_owner.m_actor1DamageText = value;
        }
      }

      public Text m_actor1DamageCriticalText
      {
        get
        {
          return this.m_owner.m_actor1DamageCriticalText;
        }
        set
        {
          this.m_owner.m_actor1DamageCriticalText = value;
        }
      }

      public CommonUIStateController m_actor1PassiveSkillUIStateController
      {
        get
        {
          return this.m_owner.m_actor1PassiveSkillUIStateController;
        }
        set
        {
          this.m_owner.m_actor1PassiveSkillUIStateController = value;
        }
      }

      public Image m_actor1PassiveSkillIconImage
      {
        get
        {
          return this.m_owner.m_actor1PassiveSkillIconImage;
        }
        set
        {
          this.m_owner.m_actor1PassiveSkillIconImage = value;
        }
      }

      public Image m_actor1PassiveSkillTalentIconImage
      {
        get
        {
          return this.m_owner.m_actor1PassiveSkillTalentIconImage;
        }
        set
        {
          this.m_owner.m_actor1PassiveSkillTalentIconImage = value;
        }
      }

      public GameObject m_actor1PassiveSkillEquipIconGameObject
      {
        get
        {
          return this.m_owner.m_actor1PassiveSkillEquipIconGameObject;
        }
        set
        {
          this.m_owner.m_actor1PassiveSkillEquipIconGameObject = value;
        }
      }

      public Image m_actor1PassiveSkillEquipIconImage
      {
        get
        {
          return this.m_owner.m_actor1PassiveSkillEquipIconImage;
        }
        set
        {
          this.m_owner.m_actor1PassiveSkillEquipIconImage = value;
        }
      }

      public Text m_actor1PassiveSkillNameText
      {
        get
        {
          return this.m_owner.m_actor1PassiveSkillNameText;
        }
        set
        {
          this.m_owner.m_actor1PassiveSkillNameText = value;
        }
      }

      public Button m_okButton
      {
        get
        {
          return this.m_owner.m_okButton;
        }
        set
        {
          this.m_owner.m_okButton = value;
        }
      }

      public Button m_cancelButton
      {
        get
        {
          return this.m_owner.m_cancelButton;
        }
        set
        {
          this.m_owner.m_cancelButton = value;
        }
      }

      public int m_armyRelationValue
      {
        get
        {
          return this.m_owner.m_armyRelationValue;
        }
        set
        {
          this.m_owner.m_armyRelationValue = value;
        }
      }

      public int m_totalDamage0
      {
        get
        {
          return this.m_owner.m_totalDamage0;
        }
        set
        {
          this.m_owner.m_totalDamage0 = value;
        }
      }

      public int m_totalDamage1
      {
        get
        {
          return this.m_owner.m_totalDamage1;
        }
        set
        {
          this.m_owner.m_totalDamage1 = value;
        }
      }

      public FxPlayer m_fxPlayer
      {
        get
        {
          return this.m_owner.m_fxPlayer;
        }
        set
        {
          this.m_owner.m_fxPlayer = value;
        }
      }

      public bool m_isOpened
      {
        get
        {
          return this.m_owner.m_isOpened;
        }
        set
        {
          this.m_owner.m_isOpened = value;
        }
      }

      public bool m_isFastCombat
      {
        get
        {
          return this.m_owner.m_isFastCombat;
        }
        set
        {
          this.m_owner.m_isFastCombat = value;
        }
      }

      public HeroPropertyComputer m_propertyComputer
      {
        get
        {
          return this.m_owner.m_propertyComputer;
        }
        set
        {
          this.m_owner.m_propertyComputer = value;
        }
      }

      public void OnDisable()
      {
        this.m_owner.OnDisable();
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void Show()
      {
        this.m_owner.Show();
      }

      public void ShowOkCancelButton(bool show)
      {
        this.m_owner.ShowOkCancelButton(show);
      }

      public void SetHeroHp(int side, int hp, int hpMax)
      {
        this.m_owner.SetHeroHp(side, hp, hpMax);
      }

      public void SetSoldierHp(int side, int hp, int hpMax)
      {
        this.m_owner.SetSoldierHp(side, hp, hpMax);
      }

      public IEnumerator Co_OpenAndFastCombat(
        FastCombatActorInfo a0,
        FastCombatActorInfo a1)
      {
        return this.m_owner.Co_OpenAndFastCombat(a0, a1);
      }

      public IEnumerator Co_FastCombat(FastCombatActorInfo a0, FastCombatActorInfo a1)
      {
        return this.m_owner.Co_FastCombat(a0, a1);
      }

      public IEnumerator Co_PlayDamage(int side, FastCombatActorInfo a)
      {
        return this.m_owner.Co_PlayDamage(side, a);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public IEnumerator Co_PlayDamage(
        GameObject fxParent,
        int side,
        int beforeHp,
        int afterHp,
        int hpMax,
        bool isHero,
        int totalDamage,
        bool isCritical)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void PlayDamageFx(
        GameObject fxParent,
        int side,
        int hp,
        int hpMax,
        int damage,
        bool isLargeFx)
      {
        // ISSUE: unable to decompile the method.
      }

      public void OnOkButtonClick()
      {
        this.m_owner.OnOkButtonClick();
      }

      public void OnCancelButtonClick()
      {
        this.m_owner.OnCancelButtonClick();
      }

      public void OnBackgroundButtonClick()
      {
        this.m_owner.OnBackgroundButtonClick();
      }
    }
  }
}
