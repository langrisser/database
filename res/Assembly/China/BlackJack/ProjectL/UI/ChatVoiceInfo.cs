﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ChatVoiceInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ChatVoiceInfo
  {
    public int m_voiceRecordId;
    public byte[] m_voiceBytes;
    public float m_voiceLength;
    public int m_voiceSendChannel;
    public int m_voiceFrequency;
    public int m_sampleLength;
  }
}
