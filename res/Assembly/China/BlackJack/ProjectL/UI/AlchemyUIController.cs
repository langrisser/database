﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AlchemyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using MarchingBytes;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class AlchemyUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./PlayerResource/MithralStone/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_mithralStoneText;
    [AutoBind("./PlayerResource/BrillianceMithralStone/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_brillianceMithralStoneText;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Item", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_itemToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Equipment", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_equipmentToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Fragment", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_fragmentToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/JobMaterial", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_jobMaterialToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Strengthen", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_strengthToggle;
    [AutoBind("./AlchemyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_alchemyButton;
    [AutoBind("./AlchemyButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_alchemyButtonStateCtrl;
    [AutoBind("./L_gold metallurgy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_effectGameObject;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./BagListPanel/BagListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_loopScrollView;
    [AutoBind("./BagListPanel/BagListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_listItemPool;
    [AutoBind("./BagListPanel/BagListScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listScrollViewItemTemplateRoot;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewBagItemContent;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport/BgContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bagListPointBgContent;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport/Point", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bagListPointItem;
    [AutoBind("./RewardListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardListPanelGo;
    [AutoBind("./RewardListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardListPanelStateCtrl;
    [AutoBind("./RewardListPanel/BGImage/FrameImage/RewardGroup/Viewport/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardsContent;
    [AutoBind("./RewardListPanel/BGImage/FrameImage/Nothing", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardsNothingGo;
    [AutoBind("./RewardListPanel/BGImage/FrameImage/InfoGroup/MaterialCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rewardsMaterialCountText;
    [AutoBind("./RewardListPanel/BGImage/FrameImage/InfoGroup/GoldenValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rewardsGoldenValueText;
    [AutoBind("./RewardListPanel/BGImage/FrameImage/NoticText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardsNoticTextGo;
    [AutoBind("./RewardListPanel/BGImage/FrameImage/NoticText/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rewardsNoticText;
    [AutoBind("./UseItemsPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_subBagInfoPanelObj;
    [AutoBind("./UseItemsPanel/PanelDetail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_subItemNumInputField;
    [AutoBind("./UseItemsPanel/PanelDetail/UseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemUseButton;
    [AutoBind("./UseItemsPanel/ReturnImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemPanelReturnButton;
    [AutoBind("./UseItemsPanel/PanelDetail/Minus", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemMinusButton;
    [AutoBind("./UseItemsPanel/PanelDetail/Add", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemAddButton;
    [AutoBind("./UseItemsPanel/PanelDetail/Max", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemMaxButton;
    [AutoBind("./UseItemsPanel/PanelDetail/AvailalbeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_subItemAvailalbeValueText;
    [AutoBind("./Desc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descGo;
    [AutoBind("./Desc/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descReturnBgButton;
    [AutoBind("./Desc/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descLockButton;
    [AutoBind("./Desc/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descLockButtonStateCtrl;
    [AutoBind("./Desc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descTitleText;
    [AutoBind("./Desc/Lay/FrameImage/Top/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIcon;
    [AutoBind("./Desc/Lay/FrameImage/Top/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIconBg;
    [AutoBind("./Desc/Lay/FrameImage/Top/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSSREffect;
    [AutoBind("./Desc/Lay/FrameImage/Top/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descStarGroup;
    [AutoBind("./Desc/Lay/FrameImage/Top/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descLvText;
    [AutoBind("./Desc/Lay/FrameImage/Top/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descExpText;
    [AutoBind("./Desc/Lay/FrameImage/Top/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descProgressImage;
    [AutoBind("./Desc/Lay/FrameImage/Top/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descEquipLimitContent;
    [AutoBind("./Desc/Lay/FrameImage/Top/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSkillContent;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descSkillContentStateCtrl;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descUnlockCoditionText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillNameText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillLvText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillOwnerText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSkillOwnerBGImage;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillDescText;
    [AutoBind("./Desc/Lay/FrameImage/Button/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descNotEquipSkillTip;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropContent;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropATGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropATValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropDFGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropDFValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropHPGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropHPValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropMagiccGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropMagicValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropMagicDFGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropMagicDFValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropDexGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropDexValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropGroupStateCtrl;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropEnchantmentGroup;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance/RuneIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descPropEnchantmentGroupRuneIconImage;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropEnchantmentGroupRuneNameText;
    private AlchemyUIController.DisplayType m_displayType;
    private ulong m_curDescEquipmentInstanceId;
    private List<BagItemBase> m_bagItemCache;
    private Dictionary<int, List<BagItemBase>> m_enhancementItemCacheMap;
    private List<BagItemUIController> m_bagItemCtrlList;
    private Dictionary<BagItemBase, int> m_checkBagItemToCountDict;
    private BagItemBase m_lastCheckBagItem;
    private bool m_isNeedResetToTop;
    private int m_canAlchemyListIndex;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private AlchemyUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitLoopScrollViewRect_hotfix;
    private LuaFunction m_OnPoolObjectCreatedStringGameObject_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_SetPointBgContent_hotfix;
    private LuaFunction m_IsBagItemOfDisplayTypeBagItemBase_hotfix;
    private LuaFunction m_BagItemComparerBagItemBaseBagItemBase_hotfix;
    private LuaFunction m_OnBagItemClickUIControllerBase_hotfix;
    private LuaFunction m_ShowRewardListPanel_hotfix;
    private LuaFunction m_CloseRewardListPanel_hotfix;
    private LuaFunction m_OnBagItemNeedFillUIControllerBase_hotfix;
    private LuaFunction m_OpenSubUseItemPanel_hotfix;
    private LuaFunction m_SetLastAlchemyCountString_hotfix;
    private LuaFunction m_GetLastAlchemyCount_hotfix;
    private LuaFunction m_OnSubItemUseItemClick_hotfix;
    private LuaFunction m_OnItemMinusButtonClick_hotfix;
    private LuaFunction m_OnItemAddButtonClick_hotfix;
    private LuaFunction m_OnItemMaxButtonClick_hotfix;
    private LuaFunction m_CloseSubItemUsePanel_hotfix;
    private LuaFunction m_OnInputEditEndString_hotfix;
    private LuaFunction m_SetEquipmentItemDescEquipmentBagItem_hotfix;
    private LuaFunction m_SetDescEquipmentLimitEquipmentBagItem_hotfix;
    private LuaFunction m_SetDescEquipmentSkillEquipmentBagItem_hotfix;
    private LuaFunction m_SetDescEquipmentEnchantEquipmentBagItem_hotfix;
    private LuaFunction m_SetPropItemsPropertyModifyTypeInt32Int32Int32_hotfix;
    private LuaFunction m_CloseEquipmentDescPanel_hotfix;
    private LuaFunction m_OnDescLockButtonClick_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnAlchemyButtonClick_hotfix;
    private LuaFunction m_OnAlchemySucceedList`1_hotfix;
    private LuaFunction m_ResetAlchemyUIView_hotfix;
    private LuaFunction m_GetEnhancementItemCacheInt32_hotfix;
    private LuaFunction m_AddEnhancementItemCacheInt32BagItemBase_hotfix;
    private LuaFunction m_RemoveEnhancementItemCacheInt32BagItemBase_hotfix;
    private LuaFunction m_HandleUseEnhancementItemBagItemBaseInt32_hotfix;
    private LuaFunction m_SortEnhancementItemCache_hotfix;
    private LuaFunction m_ClearEnhancementItemCacheMap_hotfix;
    private LuaFunction m_GetItemCountBagItemBase_hotfix;
    private LuaFunction m_IsEquipmentItemBagItemBase_hotfix;
    private LuaFunction m_IsEnhancementItemBagItemBase_hotfix;
    private LuaFunction m_IsLockedEquipmentItemBagItemBase_hotfix;
    private LuaFunction m_ResetScrollViewToTop_hotfix;
    private LuaFunction m_OnItemToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnJobMaterialToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnEquipmentToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnFragmentToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnStrengthenToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnToggleChanged_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnAlchemyButtonClickAction`2_hotfix;
    private LuaFunction m_remove_EventOnAlchemyButtonClickAction`2_hotfix;
    private LuaFunction m_add_EventOnLockButtonClickAction`2_hotfix;
    private LuaFunction m_remove_EventOnLockButtonClickAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public AlchemyUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoopScrollViewRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPointBgContent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBagItemOfDisplayType(BagItemBase itm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int BagItemComparer(BagItemBase item1, BagItemBase item2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBagItemClick(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowRewardListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseRewardListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBagItemNeedFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OpenSubUseItemPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLastAlchemyCount(string inputString = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetLastAlchemyCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSubItemUseItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemMinusButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemMaxButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSubItemUsePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInputEditEnd(string str)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentItemDesc(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDescEquipmentLimit(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDescEquipmentSkill(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDescEquipmentEnchant(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseEquipmentDescPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAlchemyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator OnAlchemySucceed(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetAlchemyUIView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BagItemBase> GetEnhancementItemCache(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddEnhancementItemCache(int id, BagItemBase item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveEnhancementItemCache(int id, BagItemBase item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleUseEnhancementItem(BagItemBase item, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortEnhancementItemCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearEnhancementItemCacheMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetItemCount(BagItemBase item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEquipmentItem(BagItemBase item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEnhancementItem(BagItemBase item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLockedEquipmentItem(BagItemBase item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobMaterialToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFragmentToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStrengthenToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<ProGoods>, Action<List<Goods>>> EventOnAlchemyButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, Action> EventOnLockButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public AlchemyUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAlchemyButtonClick(List<ProGoods> arg1, Action<List<Goods>> arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAlchemyButtonClick(
      List<ProGoods> arg1,
      Action<List<Goods>> arg2)
    {
      this.EventOnAlchemyButtonClick = (Action<List<ProGoods>, Action<List<Goods>>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnLockButtonClick(ulong arg1, Action arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnLockButtonClick(ulong arg1, Action arg2)
    {
      this.EventOnLockButtonClick = (Action<ulong, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum DisplayType
    {
      None,
      Item,
      Fragment,
      JobMaterial,
      Equipment,
      Strengthen,
    }

    public class LuaExportHelper
    {
      private AlchemyUIController m_owner;

      public LuaExportHelper(AlchemyUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnAlchemyButtonClick(
        List<ProGoods> arg1,
        Action<List<Goods>> arg2)
      {
        this.m_owner.__callDele_EventOnAlchemyButtonClick(arg1, arg2);
      }

      public void __clearDele_EventOnAlchemyButtonClick(
        List<ProGoods> arg1,
        Action<List<Goods>> arg2)
      {
        this.m_owner.__clearDele_EventOnAlchemyButtonClick(arg1, arg2);
      }

      public void __callDele_EventOnLockButtonClick(ulong arg1, Action arg2)
      {
        this.m_owner.__callDele_EventOnLockButtonClick(arg1, arg2);
      }

      public void __clearDele_EventOnLockButtonClick(ulong arg1, Action arg2)
      {
        this.m_owner.__clearDele_EventOnLockButtonClick(arg1, arg2);
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Text m_mithralStoneText
      {
        get
        {
          return this.m_owner.m_mithralStoneText;
        }
        set
        {
          this.m_owner.m_mithralStoneText = value;
        }
      }

      public Text m_brillianceMithralStoneText
      {
        get
        {
          return this.m_owner.m_brillianceMithralStoneText;
        }
        set
        {
          this.m_owner.m_brillianceMithralStoneText = value;
        }
      }

      public Toggle m_itemToggle
      {
        get
        {
          return this.m_owner.m_itemToggle;
        }
        set
        {
          this.m_owner.m_itemToggle = value;
        }
      }

      public Toggle m_equipmentToggle
      {
        get
        {
          return this.m_owner.m_equipmentToggle;
        }
        set
        {
          this.m_owner.m_equipmentToggle = value;
        }
      }

      public Toggle m_fragmentToggle
      {
        get
        {
          return this.m_owner.m_fragmentToggle;
        }
        set
        {
          this.m_owner.m_fragmentToggle = value;
        }
      }

      public Toggle m_jobMaterialToggle
      {
        get
        {
          return this.m_owner.m_jobMaterialToggle;
        }
        set
        {
          this.m_owner.m_jobMaterialToggle = value;
        }
      }

      public Toggle m_strengthToggle
      {
        get
        {
          return this.m_owner.m_strengthToggle;
        }
        set
        {
          this.m_owner.m_strengthToggle = value;
        }
      }

      public Button m_alchemyButton
      {
        get
        {
          return this.m_owner.m_alchemyButton;
        }
        set
        {
          this.m_owner.m_alchemyButton = value;
        }
      }

      public CommonUIStateController m_alchemyButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_alchemyButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_alchemyButtonStateCtrl = value;
        }
      }

      public GameObject m_effectGameObject
      {
        get
        {
          return this.m_owner.m_effectGameObject;
        }
        set
        {
          this.m_owner.m_effectGameObject = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public LoopVerticalScrollRect m_loopScrollView
      {
        get
        {
          return this.m_owner.m_loopScrollView;
        }
        set
        {
          this.m_owner.m_loopScrollView = value;
        }
      }

      public EasyObjectPool m_listItemPool
      {
        get
        {
          return this.m_owner.m_listItemPool;
        }
        set
        {
          this.m_owner.m_listItemPool = value;
        }
      }

      public GameObject m_listScrollViewItemTemplateRoot
      {
        get
        {
          return this.m_owner.m_listScrollViewItemTemplateRoot;
        }
        set
        {
          this.m_owner.m_listScrollViewItemTemplateRoot = value;
        }
      }

      public GameObject m_scrollViewBagItemContent
      {
        get
        {
          return this.m_owner.m_scrollViewBagItemContent;
        }
        set
        {
          this.m_owner.m_scrollViewBagItemContent = value;
        }
      }

      public GameObject m_bagListPointBgContent
      {
        get
        {
          return this.m_owner.m_bagListPointBgContent;
        }
        set
        {
          this.m_owner.m_bagListPointBgContent = value;
        }
      }

      public GameObject m_bagListPointItem
      {
        get
        {
          return this.m_owner.m_bagListPointItem;
        }
        set
        {
          this.m_owner.m_bagListPointItem = value;
        }
      }

      public GameObject m_rewardListPanelGo
      {
        get
        {
          return this.m_owner.m_rewardListPanelGo;
        }
        set
        {
          this.m_owner.m_rewardListPanelGo = value;
        }
      }

      public CommonUIStateController m_rewardListPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_rewardListPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_rewardListPanelStateCtrl = value;
        }
      }

      public GameObject m_rewardsContent
      {
        get
        {
          return this.m_owner.m_rewardsContent;
        }
        set
        {
          this.m_owner.m_rewardsContent = value;
        }
      }

      public GameObject m_rewardsNothingGo
      {
        get
        {
          return this.m_owner.m_rewardsNothingGo;
        }
        set
        {
          this.m_owner.m_rewardsNothingGo = value;
        }
      }

      public Text m_rewardsMaterialCountText
      {
        get
        {
          return this.m_owner.m_rewardsMaterialCountText;
        }
        set
        {
          this.m_owner.m_rewardsMaterialCountText = value;
        }
      }

      public Text m_rewardsGoldenValueText
      {
        get
        {
          return this.m_owner.m_rewardsGoldenValueText;
        }
        set
        {
          this.m_owner.m_rewardsGoldenValueText = value;
        }
      }

      public GameObject m_rewardsNoticTextGo
      {
        get
        {
          return this.m_owner.m_rewardsNoticTextGo;
        }
        set
        {
          this.m_owner.m_rewardsNoticTextGo = value;
        }
      }

      public Text m_rewardsNoticText
      {
        get
        {
          return this.m_owner.m_rewardsNoticText;
        }
        set
        {
          this.m_owner.m_rewardsNoticText = value;
        }
      }

      public GameObject m_subBagInfoPanelObj
      {
        get
        {
          return this.m_owner.m_subBagInfoPanelObj;
        }
        set
        {
          this.m_owner.m_subBagInfoPanelObj = value;
        }
      }

      public InputField m_subItemNumInputField
      {
        get
        {
          return this.m_owner.m_subItemNumInputField;
        }
        set
        {
          this.m_owner.m_subItemNumInputField = value;
        }
      }

      public Button m_subItemUseButton
      {
        get
        {
          return this.m_owner.m_subItemUseButton;
        }
        set
        {
          this.m_owner.m_subItemUseButton = value;
        }
      }

      public Button m_subItemPanelReturnButton
      {
        get
        {
          return this.m_owner.m_subItemPanelReturnButton;
        }
        set
        {
          this.m_owner.m_subItemPanelReturnButton = value;
        }
      }

      public Button m_subItemMinusButton
      {
        get
        {
          return this.m_owner.m_subItemMinusButton;
        }
        set
        {
          this.m_owner.m_subItemMinusButton = value;
        }
      }

      public Button m_subItemAddButton
      {
        get
        {
          return this.m_owner.m_subItemAddButton;
        }
        set
        {
          this.m_owner.m_subItemAddButton = value;
        }
      }

      public Button m_subItemMaxButton
      {
        get
        {
          return this.m_owner.m_subItemMaxButton;
        }
        set
        {
          this.m_owner.m_subItemMaxButton = value;
        }
      }

      public Text m_subItemAvailalbeValueText
      {
        get
        {
          return this.m_owner.m_subItemAvailalbeValueText;
        }
        set
        {
          this.m_owner.m_subItemAvailalbeValueText = value;
        }
      }

      public GameObject m_descGo
      {
        get
        {
          return this.m_owner.m_descGo;
        }
        set
        {
          this.m_owner.m_descGo = value;
        }
      }

      public Button m_descReturnBgButton
      {
        get
        {
          return this.m_owner.m_descReturnBgButton;
        }
        set
        {
          this.m_owner.m_descReturnBgButton = value;
        }
      }

      public Button m_descLockButton
      {
        get
        {
          return this.m_owner.m_descLockButton;
        }
        set
        {
          this.m_owner.m_descLockButton = value;
        }
      }

      public CommonUIStateController m_descLockButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_descLockButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_descLockButtonStateCtrl = value;
        }
      }

      public Text m_descTitleText
      {
        get
        {
          return this.m_owner.m_descTitleText;
        }
        set
        {
          this.m_owner.m_descTitleText = value;
        }
      }

      public Image m_descIcon
      {
        get
        {
          return this.m_owner.m_descIcon;
        }
        set
        {
          this.m_owner.m_descIcon = value;
        }
      }

      public Image m_descIconBg
      {
        get
        {
          return this.m_owner.m_descIconBg;
        }
        set
        {
          this.m_owner.m_descIconBg = value;
        }
      }

      public GameObject m_descSSREffect
      {
        get
        {
          return this.m_owner.m_descSSREffect;
        }
        set
        {
          this.m_owner.m_descSSREffect = value;
        }
      }

      public GameObject m_descStarGroup
      {
        get
        {
          return this.m_owner.m_descStarGroup;
        }
        set
        {
          this.m_owner.m_descStarGroup = value;
        }
      }

      public Text m_descLvText
      {
        get
        {
          return this.m_owner.m_descLvText;
        }
        set
        {
          this.m_owner.m_descLvText = value;
        }
      }

      public Text m_descExpText
      {
        get
        {
          return this.m_owner.m_descExpText;
        }
        set
        {
          this.m_owner.m_descExpText = value;
        }
      }

      public Image m_descProgressImage
      {
        get
        {
          return this.m_owner.m_descProgressImage;
        }
        set
        {
          this.m_owner.m_descProgressImage = value;
        }
      }

      public GameObject m_descEquipLimitContent
      {
        get
        {
          return this.m_owner.m_descEquipLimitContent;
        }
        set
        {
          this.m_owner.m_descEquipLimitContent = value;
        }
      }

      public Text m_descEquipUnlimitText
      {
        get
        {
          return this.m_owner.m_descEquipUnlimitText;
        }
        set
        {
          this.m_owner.m_descEquipUnlimitText = value;
        }
      }

      public GameObject m_descSkillContent
      {
        get
        {
          return this.m_owner.m_descSkillContent;
        }
        set
        {
          this.m_owner.m_descSkillContent = value;
        }
      }

      public CommonUIStateController m_descSkillContentStateCtrl
      {
        get
        {
          return this.m_owner.m_descSkillContentStateCtrl;
        }
        set
        {
          this.m_owner.m_descSkillContentStateCtrl = value;
        }
      }

      public Text m_descUnlockCoditionText
      {
        get
        {
          return this.m_owner.m_descUnlockCoditionText;
        }
        set
        {
          this.m_owner.m_descUnlockCoditionText = value;
        }
      }

      public Text m_descSkillNameText
      {
        get
        {
          return this.m_owner.m_descSkillNameText;
        }
        set
        {
          this.m_owner.m_descSkillNameText = value;
        }
      }

      public Text m_descSkillLvText
      {
        get
        {
          return this.m_owner.m_descSkillLvText;
        }
        set
        {
          this.m_owner.m_descSkillLvText = value;
        }
      }

      public Text m_descSkillOwnerText
      {
        get
        {
          return this.m_owner.m_descSkillOwnerText;
        }
        set
        {
          this.m_owner.m_descSkillOwnerText = value;
        }
      }

      public GameObject m_descSkillOwnerBGImage
      {
        get
        {
          return this.m_owner.m_descSkillOwnerBGImage;
        }
        set
        {
          this.m_owner.m_descSkillOwnerBGImage = value;
        }
      }

      public Text m_descSkillDescText
      {
        get
        {
          return this.m_owner.m_descSkillDescText;
        }
        set
        {
          this.m_owner.m_descSkillDescText = value;
        }
      }

      public GameObject m_descNotEquipSkillTip
      {
        get
        {
          return this.m_owner.m_descNotEquipSkillTip;
        }
        set
        {
          this.m_owner.m_descNotEquipSkillTip = value;
        }
      }

      public GameObject m_descPropContent
      {
        get
        {
          return this.m_owner.m_descPropContent;
        }
        set
        {
          this.m_owner.m_descPropContent = value;
        }
      }

      public GameObject m_descPropATGo
      {
        get
        {
          return this.m_owner.m_descPropATGo;
        }
        set
        {
          this.m_owner.m_descPropATGo = value;
        }
      }

      public Text m_descPropATValueText
      {
        get
        {
          return this.m_owner.m_descPropATValueText;
        }
        set
        {
          this.m_owner.m_descPropATValueText = value;
        }
      }

      public GameObject m_descPropDFGo
      {
        get
        {
          return this.m_owner.m_descPropDFGo;
        }
        set
        {
          this.m_owner.m_descPropDFGo = value;
        }
      }

      public Text m_descPropDFValueText
      {
        get
        {
          return this.m_owner.m_descPropDFValueText;
        }
        set
        {
          this.m_owner.m_descPropDFValueText = value;
        }
      }

      public GameObject m_descPropHPGo
      {
        get
        {
          return this.m_owner.m_descPropHPGo;
        }
        set
        {
          this.m_owner.m_descPropHPGo = value;
        }
      }

      public Text m_descPropHPValueText
      {
        get
        {
          return this.m_owner.m_descPropHPValueText;
        }
        set
        {
          this.m_owner.m_descPropHPValueText = value;
        }
      }

      public GameObject m_descPropMagiccGo
      {
        get
        {
          return this.m_owner.m_descPropMagiccGo;
        }
        set
        {
          this.m_owner.m_descPropMagiccGo = value;
        }
      }

      public Text m_descPropMagicValueText
      {
        get
        {
          return this.m_owner.m_descPropMagicValueText;
        }
        set
        {
          this.m_owner.m_descPropMagicValueText = value;
        }
      }

      public GameObject m_descPropMagicDFGo
      {
        get
        {
          return this.m_owner.m_descPropMagicDFGo;
        }
        set
        {
          this.m_owner.m_descPropMagicDFGo = value;
        }
      }

      public Text m_descPropMagicDFValueText
      {
        get
        {
          return this.m_owner.m_descPropMagicDFValueText;
        }
        set
        {
          this.m_owner.m_descPropMagicDFValueText = value;
        }
      }

      public GameObject m_descPropDexGo
      {
        get
        {
          return this.m_owner.m_descPropDexGo;
        }
        set
        {
          this.m_owner.m_descPropDexGo = value;
        }
      }

      public Text m_descPropDexValueText
      {
        get
        {
          return this.m_owner.m_descPropDexValueText;
        }
        set
        {
          this.m_owner.m_descPropDexValueText = value;
        }
      }

      public CommonUIStateController m_descPropGroupStateCtrl
      {
        get
        {
          return this.m_owner.m_descPropGroupStateCtrl;
        }
        set
        {
          this.m_owner.m_descPropGroupStateCtrl = value;
        }
      }

      public GameObject m_descPropEnchantmentGroup
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroup;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroup = value;
        }
      }

      public CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroupRuneStateCtrl;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroupRuneStateCtrl = value;
        }
      }

      public Image m_descPropEnchantmentGroupRuneIconImage
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroupRuneIconImage;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroupRuneIconImage = value;
        }
      }

      public Text m_descPropEnchantmentGroupRuneNameText
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroupRuneNameText;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroupRuneNameText = value;
        }
      }

      public AlchemyUIController.DisplayType m_displayType
      {
        get
        {
          return this.m_owner.m_displayType;
        }
        set
        {
          this.m_owner.m_displayType = value;
        }
      }

      public ulong m_curDescEquipmentInstanceId
      {
        get
        {
          return this.m_owner.m_curDescEquipmentInstanceId;
        }
        set
        {
          this.m_owner.m_curDescEquipmentInstanceId = value;
        }
      }

      public List<BagItemBase> m_bagItemCache
      {
        get
        {
          return this.m_owner.m_bagItemCache;
        }
        set
        {
          this.m_owner.m_bagItemCache = value;
        }
      }

      public Dictionary<int, List<BagItemBase>> m_enhancementItemCacheMap
      {
        get
        {
          return this.m_owner.m_enhancementItemCacheMap;
        }
        set
        {
          this.m_owner.m_enhancementItemCacheMap = value;
        }
      }

      public List<BagItemUIController> m_bagItemCtrlList
      {
        get
        {
          return this.m_owner.m_bagItemCtrlList;
        }
        set
        {
          this.m_owner.m_bagItemCtrlList = value;
        }
      }

      public Dictionary<BagItemBase, int> m_checkBagItemToCountDict
      {
        get
        {
          return this.m_owner.m_checkBagItemToCountDict;
        }
        set
        {
          this.m_owner.m_checkBagItemToCountDict = value;
        }
      }

      public BagItemBase m_lastCheckBagItem
      {
        get
        {
          return this.m_owner.m_lastCheckBagItem;
        }
        set
        {
          this.m_owner.m_lastCheckBagItem = value;
        }
      }

      public bool m_isNeedResetToTop
      {
        get
        {
          return this.m_owner.m_isNeedResetToTop;
        }
        set
        {
          this.m_owner.m_isNeedResetToTop = value;
        }
      }

      public int m_canAlchemyListIndex
      {
        get
        {
          return this.m_owner.m_canAlchemyListIndex;
        }
        set
        {
          this.m_owner.m_canAlchemyListIndex = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void InitLoopScrollViewRect()
      {
        this.m_owner.InitLoopScrollViewRect();
      }

      public void OnPoolObjectCreated(string poolName, GameObject go)
      {
        this.m_owner.OnPoolObjectCreated(poolName, go);
      }

      public void SetPointBgContent()
      {
        this.m_owner.SetPointBgContent();
      }

      public bool IsBagItemOfDisplayType(BagItemBase itm)
      {
        return this.m_owner.IsBagItemOfDisplayType(itm);
      }

      public int BagItemComparer(BagItemBase item1, BagItemBase item2)
      {
        return this.m_owner.BagItemComparer(item1, item2);
      }

      public void OnBagItemClick(UIControllerBase itemCtrl)
      {
        this.m_owner.OnBagItemClick(itemCtrl);
      }

      public void ShowRewardListPanel()
      {
        this.m_owner.ShowRewardListPanel();
      }

      public void CloseRewardListPanel()
      {
        this.m_owner.CloseRewardListPanel();
      }

      public void OnBagItemNeedFill(UIControllerBase itemCtrl)
      {
        this.m_owner.OnBagItemNeedFill(itemCtrl);
      }

      public void OpenSubUseItemPanel()
      {
        this.m_owner.OpenSubUseItemPanel();
      }

      public void SetLastAlchemyCount(string inputString)
      {
        this.m_owner.SetLastAlchemyCount(inputString);
      }

      public int GetLastAlchemyCount()
      {
        return this.m_owner.GetLastAlchemyCount();
      }

      public void OnSubItemUseItemClick()
      {
        this.m_owner.OnSubItemUseItemClick();
      }

      public void OnItemMinusButtonClick()
      {
        this.m_owner.OnItemMinusButtonClick();
      }

      public void OnItemAddButtonClick()
      {
        this.m_owner.OnItemAddButtonClick();
      }

      public void OnItemMaxButtonClick()
      {
        this.m_owner.OnItemMaxButtonClick();
      }

      public void CloseSubItemUsePanel()
      {
        this.m_owner.CloseSubItemUsePanel();
      }

      public void OnInputEditEnd(string str)
      {
        this.m_owner.OnInputEditEnd(str);
      }

      public void SetEquipmentItemDesc(EquipmentBagItem equipment)
      {
        this.m_owner.SetEquipmentItemDesc(equipment);
      }

      public void SetDescEquipmentLimit(EquipmentBagItem equipment)
      {
        this.m_owner.SetDescEquipmentLimit(equipment);
      }

      public void SetDescEquipmentSkill(EquipmentBagItem equipment)
      {
        this.m_owner.SetDescEquipmentSkill(equipment);
      }

      public void SetDescEquipmentEnchant(EquipmentBagItem equipment)
      {
        this.m_owner.SetDescEquipmentEnchant(equipment);
      }

      public void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
      {
        this.m_owner.SetPropItems(type, value, addValue, level);
      }

      public void CloseEquipmentDescPanel()
      {
        this.m_owner.CloseEquipmentDescPanel();
      }

      public void OnDescLockButtonClick()
      {
        this.m_owner.OnDescLockButtonClick();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnAlchemyButtonClick()
      {
        this.m_owner.OnAlchemyButtonClick();
      }

      public IEnumerator OnAlchemySucceed(List<Goods> rewards)
      {
        return this.m_owner.OnAlchemySucceed(rewards);
      }

      public List<BagItemBase> GetEnhancementItemCache(int id)
      {
        return this.m_owner.GetEnhancementItemCache(id);
      }

      public void AddEnhancementItemCache(int id, BagItemBase item)
      {
        this.m_owner.AddEnhancementItemCache(id, item);
      }

      public void RemoveEnhancementItemCache(int id, BagItemBase item)
      {
        this.m_owner.RemoveEnhancementItemCache(id, item);
      }

      public void HandleUseEnhancementItem(BagItemBase item, int count)
      {
        this.m_owner.HandleUseEnhancementItem(item, count);
      }

      public void SortEnhancementItemCache()
      {
        this.m_owner.SortEnhancementItemCache();
      }

      public void ClearEnhancementItemCacheMap()
      {
        this.m_owner.ClearEnhancementItemCacheMap();
      }

      public int GetItemCount(BagItemBase item)
      {
        return this.m_owner.GetItemCount(item);
      }

      public bool IsEquipmentItem(BagItemBase item)
      {
        return this.m_owner.IsEquipmentItem(item);
      }

      public bool IsEnhancementItem(BagItemBase item)
      {
        return this.m_owner.IsEnhancementItem(item);
      }

      public bool IsLockedEquipmentItem(BagItemBase item)
      {
        return this.m_owner.IsLockedEquipmentItem(item);
      }

      public void ResetScrollViewToTop()
      {
        this.m_owner.ResetScrollViewToTop();
      }

      public void OnItemToggleValueChanged(bool on)
      {
        this.m_owner.OnItemToggleValueChanged(on);
      }

      public void OnJobMaterialToggleValueChanged(bool on)
      {
        this.m_owner.OnJobMaterialToggleValueChanged(on);
      }

      public void OnEquipmentToggleValueChanged(bool on)
      {
        this.m_owner.OnEquipmentToggleValueChanged(on);
      }

      public void OnFragmentToggleValueChanged(bool on)
      {
        this.m_owner.OnFragmentToggleValueChanged(on);
      }

      public void OnStrengthenToggleValueChanged(bool on)
      {
        this.m_owner.OnStrengthenToggleValueChanged(on);
      }

      public void OnToggleChanged()
      {
        this.m_owner.OnToggleChanged();
      }
    }
  }
}
