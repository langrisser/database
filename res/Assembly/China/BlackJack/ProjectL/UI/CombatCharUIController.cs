﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CombatCharUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class CombatCharUIController : UIControllerBase
  {
    [AutoBind("./Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    private UISpineGraphic m_spineGraphic;
    private string m_animationName;
    private string m_loopAnimationName;

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    private void OnDisable()
    {
      this.DestroyGraphic();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGraphic(
      ConfigDataCharImageInfo charImageInfo,
      ConfigDataCharImageSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(string animation, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasAnimation(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetAnimationName()
    {
      return this.m_animationName;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ForceUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
