﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GoddessDialogBoxUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Art;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GoddessDialogBoxUIController : UIControllerBase
  {
    private const int WordsSpeed = 30;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private DialogText m_text;
    [AutoBind("./Arrow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_waitGameObject;
    private float m_time;
    private float m_voicePlayTime;
    private int m_wordsDisplayLength;
    private int m_wordsDisplayLengthMax;
    private bool m_isOpened;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsOpened()
    {
      return this.m_isOpened;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWords(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetVoicePlayTime(float voicePlayTime)
    {
      this.m_voicePlayTime = voicePlayTime;
    }

    public float GetWordsDisplayTime()
    {
      return (float) this.m_wordsDisplayLengthMax / 30f;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DisplayAllWords()
    {
    }

    public bool IsAllWordsDisplayed()
    {
      return this.m_wordsDisplayLength == this.m_wordsDisplayLengthMax;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
