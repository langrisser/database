﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.WaitPVPInviteUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class WaitPVPInviteUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private WaitPVPInviteUIController m_waitPVPInviteUIController;
    private string m_targetUserId;
    private int m_battleInfoId;
    private PracticeMode m_practiceMode;
    private DateTime m_waitTimeout;

    [MethodImpl((MethodImplOptions) 32768)]
    public WaitPVPInviteUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~WaitPVPInviteUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUITask(
      string targetUserId,
      int battleInfoID = -1,
      PracticeMode practiceMode = PracticeMode.NormalPVP)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool OnStart(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnStart(intent);
    }

    protected override bool OnResume(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnResume(intent);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void InitAllUIControllers()
    {
      base.InitAllUIControllers();
      this.InitWaitPVPInviteUIController();
    }

    protected override void ClearAllContextAndRes()
    {
      base.ClearAllContextAndRes();
      this.UninitWaitPVPInviteUIController();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitWaitPVPInviteUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitWaitPVPInviteUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattlePracticeInviteNetTask(
      string userId,
      int battleInfoId,
      PracticeMode practiceMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAndPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WaitPVPInviteUIController_OnCancel()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnOnBattlePracticeFailNtf(PracticeMode practiceMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattlePracticeDeclinedNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
