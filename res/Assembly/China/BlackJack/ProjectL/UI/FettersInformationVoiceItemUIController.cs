﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersInformationVoiceItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class FettersInformationVoiceItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateCtrl;
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockNameText;
    [AutoBind("./VoiceButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_voiceButton;
    [AutoBind("./Lock/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lockText;
    public ConfigDataHeroPerformanceInfo HeroPerformanceInfo;
    public bool? IsNew;
    private bool m_isLock;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitVoiceItem(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsPerformanceRead(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnVoiceButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateIsNewValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNewTagActive()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<FettersInformationVoiceItemUIController> EventOnVoiceButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
