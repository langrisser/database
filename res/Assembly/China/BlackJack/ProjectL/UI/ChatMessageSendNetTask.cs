﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ChatMessageSendNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ChatMessageSendNetTask : UINetTask
  {
    private ChatChannel m_channel;
    private string m_content;
    private string m_translateText;
    private byte[] m_voiceBytes;
    private int m_length;
    private int m_frequency;
    private int m_samples;
    private string m_userID;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessageSendNetTask(ChatChannel channel, string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessageSendNetTask(
      ChatChannel channel,
      byte[] voiceBytes,
      int length,
      int frequency,
      int samples,
      string translateText,
      string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnChatMessageAck(int result)
    {
      this.Result = result;
      this.OnTransactionComplete();
    }
  }
}
