﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleAchievementItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BattleAchievementItemUIController : UIControllerBase
  {
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descText;
    [AutoBind("./RewardGoods", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardGoodsGameObject;
    [AutoBind("./Complete", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_completeGameObject;
    private RewardGoodsUIController m_rewardGoodsUIController;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAchievement(ConfigDataBattleAchievementInfo a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRewards(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetComplete(bool complete)
    {
      this.m_completeGameObject.SetActive(complete);
    }
  }
}
