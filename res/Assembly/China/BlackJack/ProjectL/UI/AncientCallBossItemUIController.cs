﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallBossItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class AncientCallBossItemUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./NormalPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_clickButton;
    [AutoBind("./NormalPanel/BossNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bossNameText;
    [AutoBind("./NormalPanel/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bossIconImage;
    [AutoBind("./NormalPanel/TimeLayGroup/TextValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_closeTimeText;
    [AutoBind("./NormalPanel/AdvantageGroup/Group", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_campGroupTransform;
    [AutoBind("./NormalPanel/AdvantageGroup/Group/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_campGameObject;
    [AutoBind("./LockPanel/OpenTime/TextTimeValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_openTimeText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int m_bossId;
    private ConfigDataAncientCallBossInfo m_ancientCallBossInfo;
    private GameObjectPool m_campPool;
    private float m_lastUpdateTime;
    [DoNotToLua]
    private AncientCallBossItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetBossIDInt32_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_UpdateViewTime_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_OnClick_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBossID(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public AncientCallBossItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private AncientCallBossItemUIController m_owner;

      public LuaExportHelper(AncientCallBossItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_uiState
      {
        get
        {
          return this.m_owner.m_uiState;
        }
        set
        {
          this.m_owner.m_uiState = value;
        }
      }

      public Button m_clickButton
      {
        get
        {
          return this.m_owner.m_clickButton;
        }
        set
        {
          this.m_owner.m_clickButton = value;
        }
      }

      public Text m_bossNameText
      {
        get
        {
          return this.m_owner.m_bossNameText;
        }
        set
        {
          this.m_owner.m_bossNameText = value;
        }
      }

      public Image m_bossIconImage
      {
        get
        {
          return this.m_owner.m_bossIconImage;
        }
        set
        {
          this.m_owner.m_bossIconImage = value;
        }
      }

      public Text m_closeTimeText
      {
        get
        {
          return this.m_owner.m_closeTimeText;
        }
        set
        {
          this.m_owner.m_closeTimeText = value;
        }
      }

      public Transform m_campGroupTransform
      {
        get
        {
          return this.m_owner.m_campGroupTransform;
        }
        set
        {
          this.m_owner.m_campGroupTransform = value;
        }
      }

      public GameObject m_campGameObject
      {
        get
        {
          return this.m_owner.m_campGameObject;
        }
        set
        {
          this.m_owner.m_campGameObject = value;
        }
      }

      public Text m_openTimeText
      {
        get
        {
          return this.m_owner.m_openTimeText;
        }
        set
        {
          this.m_owner.m_openTimeText = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public int m_bossId
      {
        get
        {
          return this.m_owner.m_bossId;
        }
        set
        {
          this.m_owner.m_bossId = value;
        }
      }

      public ConfigDataAncientCallBossInfo m_ancientCallBossInfo
      {
        get
        {
          return this.m_owner.m_ancientCallBossInfo;
        }
        set
        {
          this.m_owner.m_ancientCallBossInfo = value;
        }
      }

      public GameObjectPool m_campPool
      {
        get
        {
          return this.m_owner.m_campPool;
        }
        set
        {
          this.m_owner.m_campPool = value;
        }
      }

      public float m_lastUpdateTime
      {
        get
        {
          return this.m_owner.m_lastUpdateTime;
        }
        set
        {
          this.m_owner.m_lastUpdateTime = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void UpdateViewTime()
      {
        this.m_owner.UpdateViewTime();
      }

      public void Update()
      {
        this.m_owner.Update();
      }

      public void OnClick()
      {
        this.m_owner.OnClick();
      }
    }
  }
}
