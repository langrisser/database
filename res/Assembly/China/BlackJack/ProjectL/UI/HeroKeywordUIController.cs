﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroKeywordUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroKeywordUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_tagPanelStateCtrl;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tagPanelBGButton;
    [AutoBind("./KeywordPanelDetil/InfoPanel/TitleText1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tagPanelTitleText;
    [AutoBind("./KeywordPanelDetil/InfoPanel/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tagPanelDescText;
    [AutoBind("./KeywordPanelDetil/InfoPanel/KeywordIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_tagPanelIconImage;
    [AutoBind("./KeywordPanelDetil/InfoPanel/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tagPanelListScrollViewContent;
    [AutoBind("./KeywordPanelDetil/InfoPanel/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_tagPanelListScrollRect;
    [AutoBind("./Prefab/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tagPanelHeroIconImagePrefab;
    [AutoBind("./KeywordPanelDetil/InfoPanel/CloseBth", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tagPanelCloseBtn;
    [AutoBind("./KeywordPanelDetil/RuleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bottomText;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private HeroKeywordUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Show_hotfix;
    private LuaFunction m_CloseTagPanel_hotfix;
    private LuaFunction m_SetTagPanelInfoConfigDataHeroTagInfoBooleanString_hotfix;
    private LuaFunction m_SortRelatedHeroIdByRankInt32Int32_hotfix;
    private LuaFunction m_OnHideEnd_hotfix;
    private LuaFunction m_add_eventOnHideEndAction_hotfix;
    private LuaFunction m_remove_eventOnHideEndAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseTagPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTagPanelInfo(
      ConfigDataHeroTagInfo tagInfo,
      bool showSuperMark,
      string bottomTextStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortRelatedHeroIdByRank(int heroId1, int heroId2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHideEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action eventOnHideEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroKeywordUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_eventOnHideEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_eventOnHideEnd()
    {
      this.eventOnHideEnd = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroKeywordUIController m_owner;

      public LuaExportHelper(HeroKeywordUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_eventOnHideEnd()
      {
        this.m_owner.__callDele_eventOnHideEnd();
      }

      public void __clearDele_eventOnHideEnd()
      {
        this.m_owner.__clearDele_eventOnHideEnd();
      }

      public CommonUIStateController m_tagPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_tagPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_tagPanelStateCtrl = value;
        }
      }

      public Button m_tagPanelBGButton
      {
        get
        {
          return this.m_owner.m_tagPanelBGButton;
        }
        set
        {
          this.m_owner.m_tagPanelBGButton = value;
        }
      }

      public Text m_tagPanelTitleText
      {
        get
        {
          return this.m_owner.m_tagPanelTitleText;
        }
        set
        {
          this.m_owner.m_tagPanelTitleText = value;
        }
      }

      public Text m_tagPanelDescText
      {
        get
        {
          return this.m_owner.m_tagPanelDescText;
        }
        set
        {
          this.m_owner.m_tagPanelDescText = value;
        }
      }

      public Image m_tagPanelIconImage
      {
        get
        {
          return this.m_owner.m_tagPanelIconImage;
        }
        set
        {
          this.m_owner.m_tagPanelIconImage = value;
        }
      }

      public GameObject m_tagPanelListScrollViewContent
      {
        get
        {
          return this.m_owner.m_tagPanelListScrollViewContent;
        }
        set
        {
          this.m_owner.m_tagPanelListScrollViewContent = value;
        }
      }

      public ScrollRect m_tagPanelListScrollRect
      {
        get
        {
          return this.m_owner.m_tagPanelListScrollRect;
        }
        set
        {
          this.m_owner.m_tagPanelListScrollRect = value;
        }
      }

      public GameObject m_tagPanelHeroIconImagePrefab
      {
        get
        {
          return this.m_owner.m_tagPanelHeroIconImagePrefab;
        }
        set
        {
          this.m_owner.m_tagPanelHeroIconImagePrefab = value;
        }
      }

      public Button m_tagPanelCloseBtn
      {
        get
        {
          return this.m_owner.m_tagPanelCloseBtn;
        }
        set
        {
          this.m_owner.m_tagPanelCloseBtn = value;
        }
      }

      public Text m_bottomText
      {
        get
        {
          return this.m_owner.m_bottomText;
        }
        set
        {
          this.m_owner.m_bottomText = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void CloseTagPanel()
      {
        this.m_owner.CloseTagPanel();
      }

      public int SortRelatedHeroIdByRank(int heroId1, int heroId2)
      {
        return this.m_owner.SortRelatedHeroIdByRank(heroId1, heroId2);
      }

      public void OnHideEnd()
      {
        this.m_owner.OnHideEnd();
      }
    }
  }
}
