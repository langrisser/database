﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleDanmakuUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BattleDanmakuUIController : UIControllerBase
  {
    [AutoBind("./Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_danmakuContent;
    [AutoBind("./Prefab/CommonText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_danmakuCommonText;
    [AutoBind("./Prefab/MyText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_danmakuMyText;
    private List<int> m_danmakuYPosList;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleDanmakuUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCurTurnDanmaku(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowDanmakus(List<DanmakuEntry> danmakus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOneDanmaku(DanmakuEntry danmaku, bool quantitativeRestriction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetRandomYPOsition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsYPositonAvailable(int y, int listCount)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
