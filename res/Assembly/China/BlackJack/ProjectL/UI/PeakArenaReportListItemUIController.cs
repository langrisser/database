﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaReportListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PeakArenaReportListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./Player_Lt/PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_leftPlayerIconImage;
    [AutoBind("./Player_Lt/ModeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_leftPlayerModeStateCtrl;
    [AutoBind("./Player_Lt/MatchGroup/WinPlayerGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_leftPlayerMatchStateCtrl;
    [AutoBind("./Player_Lt/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_leftPlayerNameText;
    [AutoBind("./Player_Rt/PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rightPlayerIconImage;
    [AutoBind("./Player_Rt/ModeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rightPlayerModeStateCtrl;
    [AutoBind("./Player_Rt/MatchGroup/WinPlayerGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rightPlayerMatchStateCtrl;
    [AutoBind("./Player_Rt/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rightPlayerNameText;
    [AutoBind("./Date", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleDateText;
    [AutoBind("./Date/TextTime", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleDateTimesText;
    [AutoBind("./TitleText/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    [AutoBind("./BattleTime/Text_Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleNameText;
    [AutoBind("./BattleTime/ChangeName/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleNameChangeButton;
    [AutoBind("./ButtonGroup/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareButton;
    [AutoBind("./ButtonGroup/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_shareButtonStateCtrl;
    [AutoBind("./ButtonGroup/ReplayButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_replayButton;
    [AutoBind("./ButtonGroup/CancelShareButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelShareButton;
    private ProjectLPlayerContext m_playerContext;
    private BattleReportHead m_battleReport;
    private ulong m_battleReportId;
    private string m_sourceUserId;
    private PeakArenaPlayOffInfoGetNetTask m_curNetTask;
    [DoNotToLua]
    private PeakArenaReportListItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetPeakArenaReportListItemBattleReportHeadStringStringReportType_hotfix;
    private LuaFunction m_SetReportTypeReportType_hotfix;
    private LuaFunction m_SetPeakArenaReportListItemInfoPeakArenaLadderBattleReportString_hotfix;
    private LuaFunction m_SetBattleReportNameBattleReportHeadString_hotfix;
    private LuaFunction m_GetBattleReportInstanceId_hotfix;
    private LuaFunction m_GetMatchStateCtrlNameByWinCountBooleanInt32Int32_hotfix;
    private LuaFunction m_OnReplyButtonClick_hotfix;
    private LuaFunction m_OnShareButtonClick_hotfix;
    private LuaFunction m_OnCancelShareClick_hotfix;
    private LuaFunction m_OnBattleNameChangeButtonClick_hotfix;
    private LuaFunction m_OnReportRenameSuccessUInt64String_hotfix;
    private LuaFunction m_add_EventOnReplyButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnReplyButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnShareButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnShareButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnCancelShareClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnCancelShareClickAction`1_hotfix;
    private LuaFunction m_add_EventOnBattleReportNameChangeSuccessAction`1_hotfix;
    private LuaFunction m_remove_EventOnBattleReportNameChangeSuccessAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaReportListItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaReportListItem(
      BattleReportHead report,
      string reportName,
      string sourceUserId,
      PeakArenaReportListItemUIController.ReportType reportType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetReportType(
      PeakArenaReportListItemUIController.ReportType reportType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaReportListItemInfo(
      PeakArenaLadderBattleReport battleReport,
      string reportName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleReportName(BattleReportHead report, string reportName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong GetBattleReportInstanceId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetMatchStateCtrlNameByWinCount(bool isLeft, int totalCount, int winCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReplyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelShareClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleNameChangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReportRenameSuccess(ulong reportId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ulong> EventOnReplyButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnShareButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnCancelShareClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnBattleReportNameChangeSuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PeakArenaReportListItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReplyButtonClick(ulong obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReplyButtonClick(ulong obj)
    {
      this.EventOnReplyButtonClick = (Action<ulong>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShareButtonClick(ulong obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShareButtonClick(ulong obj)
    {
      this.EventOnShareButtonClick = (Action<ulong>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCancelShareClick(ulong obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCancelShareClick(ulong obj)
    {
      this.EventOnCancelShareClick = (Action<ulong>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBattleReportNameChangeSuccess(ulong obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBattleReportNameChangeSuccess(ulong obj)
    {
      this.EventOnBattleReportNameChangeSuccess = (Action<ulong>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum ReportType
    {
      Replay,
      MyShare,
      ReportShare,
    }

    public class LuaExportHelper
    {
      private PeakArenaReportListItemUIController m_owner;

      public LuaExportHelper(PeakArenaReportListItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReplyButtonClick(ulong obj)
      {
        this.m_owner.__callDele_EventOnReplyButtonClick(obj);
      }

      public void __clearDele_EventOnReplyButtonClick(ulong obj)
      {
        this.m_owner.__clearDele_EventOnReplyButtonClick(obj);
      }

      public void __callDele_EventOnShareButtonClick(ulong obj)
      {
        this.m_owner.__callDele_EventOnShareButtonClick(obj);
      }

      public void __clearDele_EventOnShareButtonClick(ulong obj)
      {
        this.m_owner.__clearDele_EventOnShareButtonClick(obj);
      }

      public void __callDele_EventOnCancelShareClick(ulong obj)
      {
        this.m_owner.__callDele_EventOnCancelShareClick(obj);
      }

      public void __clearDele_EventOnCancelShareClick(ulong obj)
      {
        this.m_owner.__clearDele_EventOnCancelShareClick(obj);
      }

      public void __callDele_EventOnBattleReportNameChangeSuccess(ulong obj)
      {
        this.m_owner.__callDele_EventOnBattleReportNameChangeSuccess(obj);
      }

      public void __clearDele_EventOnBattleReportNameChangeSuccess(ulong obj)
      {
        this.m_owner.__clearDele_EventOnBattleReportNameChangeSuccess(obj);
      }

      public CommonUIStateController m_stateCtrl
      {
        get
        {
          return this.m_owner.m_stateCtrl;
        }
        set
        {
          this.m_owner.m_stateCtrl = value;
        }
      }

      public Image m_leftPlayerIconImage
      {
        get
        {
          return this.m_owner.m_leftPlayerIconImage;
        }
        set
        {
          this.m_owner.m_leftPlayerIconImage = value;
        }
      }

      public CommonUIStateController m_leftPlayerModeStateCtrl
      {
        get
        {
          return this.m_owner.m_leftPlayerModeStateCtrl;
        }
        set
        {
          this.m_owner.m_leftPlayerModeStateCtrl = value;
        }
      }

      public CommonUIStateController m_leftPlayerMatchStateCtrl
      {
        get
        {
          return this.m_owner.m_leftPlayerMatchStateCtrl;
        }
        set
        {
          this.m_owner.m_leftPlayerMatchStateCtrl = value;
        }
      }

      public Text m_leftPlayerNameText
      {
        get
        {
          return this.m_owner.m_leftPlayerNameText;
        }
        set
        {
          this.m_owner.m_leftPlayerNameText = value;
        }
      }

      public Image m_rightPlayerIconImage
      {
        get
        {
          return this.m_owner.m_rightPlayerIconImage;
        }
        set
        {
          this.m_owner.m_rightPlayerIconImage = value;
        }
      }

      public CommonUIStateController m_rightPlayerModeStateCtrl
      {
        get
        {
          return this.m_owner.m_rightPlayerModeStateCtrl;
        }
        set
        {
          this.m_owner.m_rightPlayerModeStateCtrl = value;
        }
      }

      public CommonUIStateController m_rightPlayerMatchStateCtrl
      {
        get
        {
          return this.m_owner.m_rightPlayerMatchStateCtrl;
        }
        set
        {
          this.m_owner.m_rightPlayerMatchStateCtrl = value;
        }
      }

      public Text m_rightPlayerNameText
      {
        get
        {
          return this.m_owner.m_rightPlayerNameText;
        }
        set
        {
          this.m_owner.m_rightPlayerNameText = value;
        }
      }

      public Text m_battleDateText
      {
        get
        {
          return this.m_owner.m_battleDateText;
        }
        set
        {
          this.m_owner.m_battleDateText = value;
        }
      }

      public Text m_battleDateTimesText
      {
        get
        {
          return this.m_owner.m_battleDateTimesText;
        }
        set
        {
          this.m_owner.m_battleDateTimesText = value;
        }
      }

      public Text m_titleText
      {
        get
        {
          return this.m_owner.m_titleText;
        }
        set
        {
          this.m_owner.m_titleText = value;
        }
      }

      public Text m_battleNameText
      {
        get
        {
          return this.m_owner.m_battleNameText;
        }
        set
        {
          this.m_owner.m_battleNameText = value;
        }
      }

      public Button m_battleNameChangeButton
      {
        get
        {
          return this.m_owner.m_battleNameChangeButton;
        }
        set
        {
          this.m_owner.m_battleNameChangeButton = value;
        }
      }

      public Button m_shareButton
      {
        get
        {
          return this.m_owner.m_shareButton;
        }
        set
        {
          this.m_owner.m_shareButton = value;
        }
      }

      public CommonUIStateController m_shareButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_shareButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_shareButtonStateCtrl = value;
        }
      }

      public Button m_replayButton
      {
        get
        {
          return this.m_owner.m_replayButton;
        }
        set
        {
          this.m_owner.m_replayButton = value;
        }
      }

      public Button m_cancelShareButton
      {
        get
        {
          return this.m_owner.m_cancelShareButton;
        }
        set
        {
          this.m_owner.m_cancelShareButton = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public BattleReportHead m_battleReport
      {
        get
        {
          return this.m_owner.m_battleReport;
        }
        set
        {
          this.m_owner.m_battleReport = value;
        }
      }

      public ulong m_battleReportId
      {
        get
        {
          return this.m_owner.m_battleReportId;
        }
        set
        {
          this.m_owner.m_battleReportId = value;
        }
      }

      public string m_sourceUserId
      {
        get
        {
          return this.m_owner.m_sourceUserId;
        }
        set
        {
          this.m_owner.m_sourceUserId = value;
        }
      }

      public PeakArenaPlayOffInfoGetNetTask m_curNetTask
      {
        get
        {
          return this.m_owner.m_curNetTask;
        }
        set
        {
          this.m_owner.m_curNetTask = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetReportType(
        PeakArenaReportListItemUIController.ReportType reportType)
      {
        this.m_owner.SetReportType(reportType);
      }

      public string GetMatchStateCtrlNameByWinCount(bool isLeft, int totalCount, int winCount)
      {
        return this.m_owner.GetMatchStateCtrlNameByWinCount(isLeft, totalCount, winCount);
      }

      public void OnReplyButtonClick()
      {
        this.m_owner.OnReplyButtonClick();
      }

      public void OnShareButtonClick()
      {
        this.m_owner.OnShareButtonClick();
      }

      public void OnCancelShareClick()
      {
        this.m_owner.OnCancelShareClick();
      }

      public void OnBattleNameChangeButtonClick()
      {
        this.m_owner.OnBattleNameChangeButtonClick();
      }

      public void OnReportRenameSuccess(ulong reportId, string name)
      {
        this.m_owner.OnReportRenameSuccess(reportId, name);
      }
    }
  }
}
