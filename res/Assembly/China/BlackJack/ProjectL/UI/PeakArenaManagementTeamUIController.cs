﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaManagementTeamUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PeakArenaManagementTeamUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./TeamCompiledButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_teamCompiledButton;
    [AutoBind("./TeamCompiledButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teamCompiledButtonAnimation;
    [AutoBind("./RightPanel/ToggleGroup/HeroToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_heroToggle;
    [AutoBind("./RightPanel/ToggleGroup/MercenaryToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mercenaryToggle;
    [AutoBind("./RightPanel/ToggleGroup/HeroToggle/Image/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroSelectCountText;
    [AutoBind("./RightPanel/ToggleGroup/MercenaryToggle/Image/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_mercenarySelectCountText;
    [AutoBind("./RightPanel/FilterButton/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sortButton;
    [AutoBind("./RightPanel/FilterButton/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_sortButtonText;
    [AutoBind("./RightPanel/FilterButton/SortTypes/BGImages/BgButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroSortBGButton;
    [AutoBind("./RightPanel/FilterButton/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroSortPanelAnimation;
    [AutoBind("./RightPanel/FilterButton/SortTypes/GridLayout/Power", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_powerToggle;
    [AutoBind("./RightPanel/FilterButton/SortTypes/GridLayout/Rank", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_rankToggle;
    [AutoBind("./RightPanel/FilterButton/SortTypes/GridLayout/LV", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_lvToggle;
    [AutoBind("./RightPanel/HeroListScroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private InfinityGridLayoutGroup m_heroListInfinityGrid;
    [AutoBind("./RightPanel/MercenaryProhibit", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mercenaryProhibitGameObject;
    [AutoBind("./Prefab/HeroListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroItemPrefab;
    [AutoBind("./LeftPanel/ChoosenHeroList/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_heroContentLeft;
    [AutoBind("./Prefab/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroItemCirclePrefab;
    [AutoBind("./LeftPanel/HeroCountGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectCountText;
    [AutoBind("./LeftPanel/HeroCountGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_maxCountText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private PeakArenaManagementTeamUITask m_peakArenaManagementTeamUITask;
    private List<PeakArenaManagementTeamUIController.HeroWrap> m_heroWrapList;
    private List<PeakArenaManagementTeamUIController.HeroWrap> m_mercenaryWrapList;
    private List<PeakArenaManagementTeamUIController.HeroWrap> m_selectHeroWrapList;
    private Toggle m_selectHeroListToggle;
    private Toggle m_selectSortToggle;
    private List<PeakArenaHeroCardUIController> m_heroCardUIControllerList;
    private GameObjectPool<PeakArenaHeroCardCircleUIController> m_heroCardCirclePool;
    private int m_selectHeroCount;
    [DoNotToLua]
    private PeakArenaManagementTeamUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_UpdateHeroData_hotfix;
    private LuaFunction m_Refresh_hotfix;
    private LuaFunction m_LeftHeroListRefresh_hotfix;
    private LuaFunction m_HeroListRefresh_hotfix;
    private LuaFunction m_UpdateDataHeroSelectHeroWrap_hotfix;
    private LuaFunction m_IsBattleTeamModify_hotfix;
    private LuaFunction m_GetSelectHeroCountList`1_hotfix;
    private LuaFunction m_HeroRankSortHeroWrapHeroWrap_hotfix;
    private LuaFunction m_HeroPowerSortHeroWrapHeroWrap_hotfix;
    private LuaFunction m_HeroLvSortHeroWrapHeroWrap_hotfix;
    private LuaFunction m_SortHeroToggle_hotfix;
    private LuaFunction m_OnReturnClick_hotfix;
    private LuaFunction m_OnHelpClick_hotfix;
    private LuaFunction m_OnTeamCompiledClick_hotfix;
    private LuaFunction m_OnHeroTabClickGameObject_hotfix;
    private LuaFunction m_OnSortClick_hotfix;
    private LuaFunction m_OnSortBgClick_hotfix;
    private LuaFunction m_OnSortTabClickToggle_hotfix;
    private LuaFunction m_OnHeroItemClickPeakArenaHeroCardUIController_hotfix;
    private LuaFunction m_OnHeroDetailClickPeakArenaHeroCardUIController_hotfix;
    private LuaFunction m_OnHeroItemCircleClickPeakArenaHeroCardCircleUIController_hotfix;
    private LuaFunction m_UpdateInfinityHeroItemCallbackInt32Transform_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaManagementTeamUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateHeroData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LeftHeroListRefresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroListRefresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateDataHeroSelect(
      PeakArenaManagementTeamUIController.HeroWrap selectHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBattleTeamModify()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetSelectHeroCount(
      List<PeakArenaManagementTeamUIController.HeroWrap> heroWrapList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroRankSort(
      PeakArenaManagementTeamUIController.HeroWrap hero1,
      PeakArenaManagementTeamUIController.HeroWrap hero2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroPowerSort(
      PeakArenaManagementTeamUIController.HeroWrap hero1,
      PeakArenaManagementTeamUIController.HeroWrap hero2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroLvSort(
      PeakArenaManagementTeamUIController.HeroWrap hero1,
      PeakArenaManagementTeamUIController.HeroWrap hero2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortHero(Toggle selectSortToggle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTeamCompiledClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroTabClick(GameObject selectToggle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSortClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSortBgClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSortTabClick(Toggle selectToggle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroItemClick(PeakArenaHeroCardUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroDetailClick(PeakArenaHeroCardUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroItemCircleClick(PeakArenaHeroCardCircleUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateInfinityHeroItemCallback(int index, Transform trans)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public PeakArenaManagementTeamUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class HeroWrap
    {
      public Hero hero;
      public List<EquipmentBagItem> equipmentList;
      public bool isSelect;
      public bool otherSameHeroSelect;
      public bool isMercenary;
      public string serverName;
      public string playerName;

      [MethodImpl((MethodImplOptions) 32768)]
      public EquipmentBagItem GetEquipmentByID(ulong equipmentId)
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class LuaExportHelper
    {
      private PeakArenaManagementTeamUIController m_owner;

      public LuaExportHelper(PeakArenaManagementTeamUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Button m_teamCompiledButton
      {
        get
        {
          return this.m_owner.m_teamCompiledButton;
        }
        set
        {
          this.m_owner.m_teamCompiledButton = value;
        }
      }

      public CommonUIStateController m_teamCompiledButtonAnimation
      {
        get
        {
          return this.m_owner.m_teamCompiledButtonAnimation;
        }
        set
        {
          this.m_owner.m_teamCompiledButtonAnimation = value;
        }
      }

      public Toggle m_heroToggle
      {
        get
        {
          return this.m_owner.m_heroToggle;
        }
        set
        {
          this.m_owner.m_heroToggle = value;
        }
      }

      public Toggle m_mercenaryToggle
      {
        get
        {
          return this.m_owner.m_mercenaryToggle;
        }
        set
        {
          this.m_owner.m_mercenaryToggle = value;
        }
      }

      public Text m_heroSelectCountText
      {
        get
        {
          return this.m_owner.m_heroSelectCountText;
        }
        set
        {
          this.m_owner.m_heroSelectCountText = value;
        }
      }

      public Text m_mercenarySelectCountText
      {
        get
        {
          return this.m_owner.m_mercenarySelectCountText;
        }
        set
        {
          this.m_owner.m_mercenarySelectCountText = value;
        }
      }

      public Button m_sortButton
      {
        get
        {
          return this.m_owner.m_sortButton;
        }
        set
        {
          this.m_owner.m_sortButton = value;
        }
      }

      public Text m_sortButtonText
      {
        get
        {
          return this.m_owner.m_sortButtonText;
        }
        set
        {
          this.m_owner.m_sortButtonText = value;
        }
      }

      public Button m_heroSortBGButton
      {
        get
        {
          return this.m_owner.m_heroSortBGButton;
        }
        set
        {
          this.m_owner.m_heroSortBGButton = value;
        }
      }

      public CommonUIStateController m_heroSortPanelAnimation
      {
        get
        {
          return this.m_owner.m_heroSortPanelAnimation;
        }
        set
        {
          this.m_owner.m_heroSortPanelAnimation = value;
        }
      }

      public Toggle m_powerToggle
      {
        get
        {
          return this.m_owner.m_powerToggle;
        }
        set
        {
          this.m_owner.m_powerToggle = value;
        }
      }

      public Toggle m_rankToggle
      {
        get
        {
          return this.m_owner.m_rankToggle;
        }
        set
        {
          this.m_owner.m_rankToggle = value;
        }
      }

      public Toggle m_lvToggle
      {
        get
        {
          return this.m_owner.m_lvToggle;
        }
        set
        {
          this.m_owner.m_lvToggle = value;
        }
      }

      public InfinityGridLayoutGroup m_heroListInfinityGrid
      {
        get
        {
          return this.m_owner.m_heroListInfinityGrid;
        }
        set
        {
          this.m_owner.m_heroListInfinityGrid = value;
        }
      }

      public GameObject m_mercenaryProhibitGameObject
      {
        get
        {
          return this.m_owner.m_mercenaryProhibitGameObject;
        }
        set
        {
          this.m_owner.m_mercenaryProhibitGameObject = value;
        }
      }

      public GameObject m_heroItemPrefab
      {
        get
        {
          return this.m_owner.m_heroItemPrefab;
        }
        set
        {
          this.m_owner.m_heroItemPrefab = value;
        }
      }

      public Transform m_heroContentLeft
      {
        get
        {
          return this.m_owner.m_heroContentLeft;
        }
        set
        {
          this.m_owner.m_heroContentLeft = value;
        }
      }

      public GameObject m_heroItemCirclePrefab
      {
        get
        {
          return this.m_owner.m_heroItemCirclePrefab;
        }
        set
        {
          this.m_owner.m_heroItemCirclePrefab = value;
        }
      }

      public Text m_selectCountText
      {
        get
        {
          return this.m_owner.m_selectCountText;
        }
        set
        {
          this.m_owner.m_selectCountText = value;
        }
      }

      public Text m_maxCountText
      {
        get
        {
          return this.m_owner.m_maxCountText;
        }
        set
        {
          this.m_owner.m_maxCountText = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public PeakArenaManagementTeamUITask m_peakArenaManagementTeamUITask
      {
        get
        {
          return this.m_owner.m_peakArenaManagementTeamUITask;
        }
        set
        {
          this.m_owner.m_peakArenaManagementTeamUITask = value;
        }
      }

      public List<PeakArenaManagementTeamUIController.HeroWrap> m_heroWrapList
      {
        get
        {
          return this.m_owner.m_heroWrapList;
        }
        set
        {
          this.m_owner.m_heroWrapList = value;
        }
      }

      public List<PeakArenaManagementTeamUIController.HeroWrap> m_mercenaryWrapList
      {
        get
        {
          return this.m_owner.m_mercenaryWrapList;
        }
        set
        {
          this.m_owner.m_mercenaryWrapList = value;
        }
      }

      public List<PeakArenaManagementTeamUIController.HeroWrap> m_selectHeroWrapList
      {
        get
        {
          return this.m_owner.m_selectHeroWrapList;
        }
        set
        {
          this.m_owner.m_selectHeroWrapList = value;
        }
      }

      public Toggle m_selectHeroListToggle
      {
        get
        {
          return this.m_owner.m_selectHeroListToggle;
        }
        set
        {
          this.m_owner.m_selectHeroListToggle = value;
        }
      }

      public Toggle m_selectSortToggle
      {
        get
        {
          return this.m_owner.m_selectSortToggle;
        }
        set
        {
          this.m_owner.m_selectSortToggle = value;
        }
      }

      public List<PeakArenaHeroCardUIController> m_heroCardUIControllerList
      {
        get
        {
          return this.m_owner.m_heroCardUIControllerList;
        }
        set
        {
          this.m_owner.m_heroCardUIControllerList = value;
        }
      }

      public GameObjectPool<PeakArenaHeroCardCircleUIController> m_heroCardCirclePool
      {
        get
        {
          return this.m_owner.m_heroCardCirclePool;
        }
        set
        {
          this.m_owner.m_heroCardCirclePool = value;
        }
      }

      public int m_selectHeroCount
      {
        get
        {
          return this.m_owner.m_selectHeroCount;
        }
        set
        {
          this.m_owner.m_selectHeroCount = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void UpdateDataHeroSelect(
        PeakArenaManagementTeamUIController.HeroWrap selectHero)
      {
        this.m_owner.UpdateDataHeroSelect(selectHero);
      }

      public bool IsBattleTeamModify()
      {
        return this.m_owner.IsBattleTeamModify();
      }

      public int GetSelectHeroCount(
        List<PeakArenaManagementTeamUIController.HeroWrap> heroWrapList)
      {
        return this.m_owner.GetSelectHeroCount(heroWrapList);
      }

      public int HeroRankSort(
        PeakArenaManagementTeamUIController.HeroWrap hero1,
        PeakArenaManagementTeamUIController.HeroWrap hero2)
      {
        return this.m_owner.HeroRankSort(hero1, hero2);
      }

      public int HeroPowerSort(
        PeakArenaManagementTeamUIController.HeroWrap hero1,
        PeakArenaManagementTeamUIController.HeroWrap hero2)
      {
        return this.m_owner.HeroPowerSort(hero1, hero2);
      }

      public int HeroLvSort(
        PeakArenaManagementTeamUIController.HeroWrap hero1,
        PeakArenaManagementTeamUIController.HeroWrap hero2)
      {
        return this.m_owner.HeroLvSort(hero1, hero2);
      }

      public void SortHero(Toggle selectSortToggle)
      {
        this.m_owner.SortHero(selectSortToggle);
      }

      public void OnReturnClick()
      {
        this.m_owner.OnReturnClick();
      }

      public void OnHelpClick()
      {
        this.m_owner.OnHelpClick();
      }

      public void OnTeamCompiledClick()
      {
        this.m_owner.OnTeamCompiledClick();
      }

      public void OnHeroTabClick(GameObject selectToggle)
      {
        this.m_owner.OnHeroTabClick(selectToggle);
      }

      public void OnSortClick()
      {
        this.m_owner.OnSortClick();
      }

      public void OnSortBgClick()
      {
        this.m_owner.OnSortBgClick();
      }

      public void OnSortTabClick(Toggle selectToggle)
      {
        this.m_owner.OnSortTabClick(selectToggle);
      }

      public void OnHeroItemClick(PeakArenaHeroCardUIController ctrl)
      {
        this.m_owner.OnHeroItemClick(ctrl);
      }

      public void OnHeroDetailClick(PeakArenaHeroCardUIController ctrl)
      {
        this.m_owner.OnHeroDetailClick(ctrl);
      }

      public void OnHeroItemCircleClick(PeakArenaHeroCardCircleUIController ctrl)
      {
        this.m_owner.OnHeroItemCircleClick(ctrl);
      }

      public void UpdateInfinityHeroItemCallback(int index, Transform trans)
      {
        this.m_owner.UpdateInfinityHeroItemCallback(index, trans);
      }
    }
  }
}
