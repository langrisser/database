﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipMasterRefineOpenPanelUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using MarchingBytes;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EquipMasterRefineOpenPanelUIController : UIControllerBase
  {
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private Hero m_hero;
    private EquipMasterEquipType m_equipType;
    private int m_propertyIndex;
    private HeroJobSlotRefineryProperty m_originProperty;
    private HeroJobSlotRefineryProperty m_newProperty;
    private List<BagItemBase> m_refineryStoneList;
    private EquipMasterEquipPropertyFilterType m_curFilterType;
    private int m_lastClickRefineryStoneIndex;
    private EquipmentDepotListItemUIController m_lastClickRefineryStoneUICtrl;
    private bool m_isFilterTypePanelOpen;
    private Action m_onReturn;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController RefineOpenPanelUIStateCtrl;
    [AutoBind("./EquipmentList/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public EasyObjectPool RefineStoneListObjectPool;
    [AutoBind("./EquipmentList/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public LoopScrollRect RefineStoneListScrollRect;
    [AutoBind("./EquipmentList/ListScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ItemRoot;
    [AutoBind("./EquipmentList/Filter", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject FilterGameObject;
    [AutoBind("./EquipmentList/Filter/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button FilterButton;
    [AutoBind("./EquipmentList/Filter/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    public Text FilterTypeText;
    [AutoBind("./EquipmentList/Filter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController FilterTypesUIStateCtrl;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/Hp", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeHPToggle;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/AT", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeATToggle;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/DF", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeDFToggle;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/MagicAT", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeMagicATToggle;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeMagicDFToggle;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/Dex", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeDexToggle;
    [AutoBind("./RefineInfoPanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    public Text RefineInfoTitleText;
    [AutoBind("./RefineInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button CloseButton;
    [AutoBind("./RefineInfoPanel/Button", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController RefineButtonUIStateCtrl;
    [AutoBind("./RefineInfoPanel/Button/RefineButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button RefineButton;
    [AutoBind("./RefineInfoPanel/Button/AgainButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button RefineAgainButton;
    [AutoBind("./RefineInfoPanel/Button/AgainButton", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController RefineAgainButtonUIStateCtrl;
    [AutoBind("./RefineInfoPanel/Button/ConserveButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ConserveButton;
    [AutoBind("./RefineInfoPanel/StoneValue/Value", AutoBindAttribute.InitState.NotInit, false)]
    public Text RefineStoneValueText;
    [AutoBind("./RefineInfoPanel/GoldValue/Value", AutoBindAttribute.InitState.NotInit, false)]
    public Text GoldConsumeText;
    [AutoBind("./RefineInfoPanel/Proprety_Original", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController OriginalPropertyUIStateCtrl;
    [AutoBind("./RefineInfoPanel/Proprety_Original/NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text OriginPropertyNameText;
    [AutoBind("./RefineInfoPanel/Proprety_Original/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text OriginPropertyValueText;
    [AutoBind("./RefineInfoPanel/Proprety_Original/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    public Image OriginPropertyProgressImage;
    [AutoBind("./RefineInfoPanel/Proprety_New", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController NewPropertyUIStateCtrl;
    [AutoBind("./RefineInfoPanel/Proprety_New/NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text NewPropertyNameText;
    [AutoBind("./RefineInfoPanel/Proprety_New/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text NewPropertyValueText;
    [AutoBind("./RefineInfoPanel/Proprety_New/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    public Image NewPropertyProgressImage;
    [AutoBind("./CloseConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController CloseConfirmPanelUIStateCtrl;
    [AutoBind("./CloseConfirmPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button CloseConfirmCancelButton;
    [AutoBind("./CloseConfirmPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button CloseConfirmConfirmButton;
    [AutoBind("./CloseConfirmPanel/Detail/ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle CloseConfirmDontshowToggle;
    [AutoBind("./SaveConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SaveConfirmPanelUIStateCtrl;
    [AutoBind("./SaveConfirmPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button SaveConfirmCancelButton;
    [AutoBind("./SaveConfirmPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button SaveConfirmConfirmButton;
    [AutoBind("./SaveConfirmPanel/Detail/ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle SaveConfirmDontshowToggle;
    public GameObject m_refineStoneListItemTemplate;
    private const string RefineStoneListObjectPoolName = "EquipMasterRefineryStoneListItem";

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipMasterRefineOpenPanelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show(
      Hero hero,
      EquipMasterEquipType equipType,
      int propertyIndex,
      HeroJobSlotRefineryProperty originProperty,
      Action onReturn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    public void UpdateUI()
    {
      this.UpdateRefineStoneList();
      this.UpdateRefineInfo();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRefineStoneList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateSingleRefineryStone(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRefineInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCloseConfirmPanel(Action onCanel, Action onClose)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseCloseConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSaveConfirmPanel(Action onCanel, Action onSave)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSaveConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowFilterTypesPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseFilterTypesPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterTypeToggleValueChanged(EquipMasterEquipPropertyFilterType filterType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineStoneListItemFill(UIControllerBase uiCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineStoneListItemClick(UIControllerBase uiCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineComplete(HeroJobSlotRefineryProperty newProperty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveRefineProperty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSaveRefinePropertyComplete(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConserveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateRefineStoneListPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetFilterButtonName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BagItemBase> GetRefineryStoneList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsRefineryStoneFitToEquipType(RefineryStoneBagItem refineryStone)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsRefineryStonePropertyHasExistInEquipSlot(RefineryStoneBagItem refineryStone)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFilterByArmyType(RefineryStoneBagItem refineryStone)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFilterByEquipMasterEquipPropertyFilterType(RefineryStoneBagItem bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int RefineryStoneSorter(BagItemBase item1, BagItemBase item2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BagItemBase GetCurrentSlectedRefineryStone()
    {
      // ISSUE: unable to decompile the method.
    }

    public event EquipMasterRefineOpenPanelUIController.HeroJobRefineDelegate<int, int, int, int, int, Action<HeroJobSlotRefineryProperty>> EventOnRefineButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action<int>> EventOnConserveButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public delegate void HeroJobRefineDelegate<T1, T2, T3, T4, T5, T6>(
      T1 arg1,
      T2 arg2,
      T3 arg3,
      T4 arg4,
      T5 arg5,
      T6 arg6);
  }
}
