﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BagListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using MarchingBytes;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BagListUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Item", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_itemToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Equipment", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_equipmentToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Fragment", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_fragmentToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/JobMaterial", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_jobMaterialToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Strengthen", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_strengthToggle;
    [AutoBind("./GoldMetallurgyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_alchemyButton;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./BagListPanel/BagListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_loopScrollView;
    [AutoBind("./BagListPanel/BagListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_listItemPool;
    [AutoBind("./BagListPanel/BagListScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listScrollViewItemTemplateRoot;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewBagItemContent;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport/BgContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bagListPointBgContent;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport/Point", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bagListPointItem;
    [AutoBind("./BagListPanel/NoItemPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noItemPanelObj;
    [AutoBind("./BagListPanel/BagInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bagInfoPanelObj;
    [AutoBind("./BagListPanel/BagInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bagInfoStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/UseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_useButton;
    [AutoBind("./BagListPanel/BagInfoPanel/ComposeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_composeButton;
    [AutoBind("./BagListPanel/BagInfoPanel/ComposeButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_composeButtonStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemNameText;
    [AutoBind("./BagListPanel/BagInfoPanel/Count/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemCountText;
    [AutoBind("./BagListPanel/BagInfoPanel/Desc/ValueTextScrollView/Mask/Content/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemDescText;
    [AutoBind("./BagListPanel/BagInfoPanel/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemIconImage;
    [AutoBind("./BagListPanel/BagInfoPanel/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemGetButton;
    [AutoBind("./CountLimit", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bagCountLimitStateCtrl;
    [AutoBind("./CountLimit/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bagCountText;
    [AutoBind("./CountLimit/MaxText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bagMaxCountText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/ExplainFrontToggle/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentInfoExplainText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/ForgeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipmentForgeButton;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipmentSkillStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentSkillNameText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentSkillLvValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentSkillUnlockCoditionText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentSkillCharNameText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentSkillCharNameBGImage;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent/DescScrollView/Mask/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentSkillDescText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipmentSkillContentStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/EquipGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipmentLimitStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/EquipGroup/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentLimitContent;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/EquipGroup/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentEquipUnlimitText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropertyGroup;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropATGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropATValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropDFGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropDFValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropHPGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropHPValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropMagiccGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropMagicValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropMagicDFGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropMagicDFValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropDexGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropDexValueText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipmentPropGroupStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/PropertyFrontToggle/Detail/PropGroup/EnchantmentGroup/PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropEnchantmentGroup;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentPropEnchantmentGroupResonanceGo;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipmentPropEnchantmentGroupRuneStateCtrl;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/Resonance/RuneIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipmentPropEnchantmentGroupRuneIconImage;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/Resonance/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentPropEnchantmentGroupRuneNameText;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descLockButton;
    [AutoBind("./BagListPanel/BagInfoPanel/EquipInfo/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareButton;
    [AutoBind("./UseItemsPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_subBagInfoPanelObj;
    [AutoBind("./UseItemsPanel/PanelDetail/Minus", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemMinusButton;
    [AutoBind("./UseItemsPanel/PanelDetail/Add", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemAddButton;
    [AutoBind("./UseItemsPanel/PanelDetail/Max", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemMaxButton;
    [AutoBind("./UseItemsPanel/PanelDetail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_subItemNumInputField;
    [AutoBind("./UseItemsPanel/PanelDetail/UseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemUseButton;
    [AutoBind("./UseItemsPanel/ReturnImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemPanelReturnButton;
    [AutoBind("./AddAllItemButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_addAllItemButton;
    [AutoBind("./AddAllEquipmentButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_addAllEquipmentButton;
    [AutoBind("./AddBagItemButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_addItemButton;
    [AutoBind("./ClearBagButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_clearBagItemButton;
    [AutoBind("./BagItemInputField", AutoBindAttribute.InitState.Inactive, false)]
    private InputField m_bagItemInputField;
    [AutoBind("./SpeedUpButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_speedUpButton;
    private BagListUIController.DisplayType m_displayType;
    private BagItemBase m_lastClickBagItem;
    private BagListUIController.DisplayType m_lastClickBagItemType;
    private List<BagItemBase> m_bagItemCache;
    private List<BagItemUIController> m_bagItemCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public BagListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoopScrollViewRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInBagList(
      BagListUIController.DisplayType displayType,
      ulong clickInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBagItemOfDisplayType(BagItemBase itm)
    {
      // ISSUE: unable to decompile the method.
    }

    private void ShowNoItemPanelObj(bool isShow)
    {
      this.m_noItemPanelObj.SetActive(isShow);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int BagItemComparer(BagItemBase item1, BagItemBase item2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBagItemClick(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBagItemNeedFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInfoPanel(BagItemBase bagItemBase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentInfo(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentSkillInfo(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentLimitInfo(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentEnchantInfo(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentPropItem(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBagCountLimit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetItemButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentForgeButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetBagUIView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAlchemyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnComposeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSubItemUseItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSubItemUsePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInputEditEnd(string inputString)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetSelectableMaxCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemMinusButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemMaxButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddAllItemButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddAllEquipmentButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddItemButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClearBagButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSpeedUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseBagInfoPanel()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobMaterialToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFragmentToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStrengthenToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClearBag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ulong, BagListUIController.DisplayType> EventOnSpeedUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddAllItem
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddAllEquipment
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAlchemyButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnAddItem
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int, BagListUIController.DisplayType> EventOnUse
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ulong, BagListUIController.DisplayType> EventOnEquipmentForge
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BagItemBase, BagListUIController.DisplayType> EventOnShowGetPath
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnLockButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, string> EventOnShareButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum DisplayType
    {
      None,
      Item,
      Fragment,
      JobMaterial,
      Equipment,
      Strengthen,
    }
  }
}
