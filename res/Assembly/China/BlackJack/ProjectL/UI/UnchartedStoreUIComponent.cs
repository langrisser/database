﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedStoreUIComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class UnchartedStoreUIComponent
  {
    private StoreId m_fixedStoreId;
    private GameObject m_itemPrefab;
    private GameObject m_content;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private UnchartedStoreUITask m_unchartedStoreUITask;
    private UnchartedStoreUIController m_unchartedStoreUIController;
    private CurrencyUIController m_currencyUIController;
    private GameObjectPool<StoreItemUIController> m_fixedStoreItemPool;
    [DoNotToLua]
    private UnchartedStoreUIComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitGameObjectGameObjectCurrencyUIControllerUnchartedStoreUIController_hotfix;
    private LuaFunction m_SetFixedStoreInfoStoreIdInt32_hotfix;
    private LuaFunction m_ItemSortFixedStoreItemFixedStoreItem_hotfix;
    private LuaFunction m_SetCurrencyStateList`1_hotfix;
    private LuaFunction m_ClearFiexdStoreItem_hotfix;
    private LuaFunction m_OnStoreItemClickStoreItemUIController_hotfix;
    private LuaFunction m_OnBuyItemSuccess_hotfix;
    private LuaFunction m_add_EventOnBuyItemSuccessAction_hotfix;
    private LuaFunction m_remove_EventOnBuyItemSuccessAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedStoreUIComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(
      GameObject itemPrefab,
      GameObject itemContent,
      CurrencyUIController currencyUIController,
      UnchartedStoreUIController unchartedStoreUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFixedStoreInfo(StoreId fixedStoreID, int goodsId = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ItemSort(FixedStoreItem item1, FixedStoreItem item2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurrencyState(List<GoodsType> currencyList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearFiexdStoreItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStoreItemClick(StoreItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyItemSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnBuyItemSuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public UnchartedStoreUIComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBuyItemSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBuyItemSuccess()
    {
      this.EventOnBuyItemSuccess = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private UnchartedStoreUIComponent m_owner;

      public LuaExportHelper(UnchartedStoreUIComponent owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_EventOnBuyItemSuccess()
      {
        this.m_owner.__callDele_EventOnBuyItemSuccess();
      }

      public void __clearDele_EventOnBuyItemSuccess()
      {
        this.m_owner.__clearDele_EventOnBuyItemSuccess();
      }

      public StoreId m_fixedStoreId
      {
        get
        {
          return this.m_owner.m_fixedStoreId;
        }
        set
        {
          this.m_owner.m_fixedStoreId = value;
        }
      }

      public GameObject m_itemPrefab
      {
        get
        {
          return this.m_owner.m_itemPrefab;
        }
        set
        {
          this.m_owner.m_itemPrefab = value;
        }
      }

      public GameObject m_content
      {
        get
        {
          return this.m_owner.m_content;
        }
        set
        {
          this.m_owner.m_content = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public UnchartedStoreUITask m_unchartedStoreUITask
      {
        get
        {
          return this.m_owner.m_unchartedStoreUITask;
        }
        set
        {
          this.m_owner.m_unchartedStoreUITask = value;
        }
      }

      public UnchartedStoreUIController m_unchartedStoreUIController
      {
        get
        {
          return this.m_owner.m_unchartedStoreUIController;
        }
        set
        {
          this.m_owner.m_unchartedStoreUIController = value;
        }
      }

      public CurrencyUIController m_currencyUIController
      {
        get
        {
          return this.m_owner.m_currencyUIController;
        }
        set
        {
          this.m_owner.m_currencyUIController = value;
        }
      }

      public GameObjectPool<StoreItemUIController> m_fixedStoreItemPool
      {
        get
        {
          return this.m_owner.m_fixedStoreItemPool;
        }
        set
        {
          this.m_owner.m_fixedStoreItemPool = value;
        }
      }

      public int ItemSort(FixedStoreItem item1, FixedStoreItem item2)
      {
        return this.m_owner.ItemSort(item1, item2);
      }

      public void SetCurrencyState(List<GoodsType> currencyList)
      {
        this.m_owner.SetCurrencyState(currencyList);
      }

      public void OnBuyItemSuccess()
      {
        this.m_owner.OnBuyItemSuccess();
      }
    }
  }
}
