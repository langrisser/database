﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildInviteItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GuildInviteItemUIController : UIControllerBase
  {
    [AutoBind("./Char/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_charLevelText;
    [AutoBind("./Char/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_charIcon;
    [AutoBind("./Char/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameDummy;
    [AutoBind("./Char/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_charName;
    [AutoBind("./Char/SociatyNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteGuildName;
    [AutoBind("./InfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detialButton;
    [AutoBind("./AgreeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_acceptButton;
    [AutoBind("./RefuseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_refuseButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private GuildJoinInvitation m_guildJoinInvitation;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Init(GuildJoinInvitation guildJoinInvitation)
    {
      this.m_guildJoinInvitation = guildJoinInvitation;
      this.Refresh();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAcceptClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefuseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDetailClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action ItemRefreshEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
