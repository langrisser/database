﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TouchFx
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Scene;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TouchFx
  {
    private string m_hitFxName;
    private string m_loopFxName;
    private Camera m_camera;
    private GameObject m_disableInputGameObject;
    private GraphicPool m_graphicPool;
    private FxPlayer m_fxPlayer;
    private List<TouchFx.TouchState> m_touchStates;
    private GenericGraphic[] m_loopFxs;

    [MethodImpl((MethodImplOptions) 32768)]
    public TouchFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(GameObject parent, Camera camera)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetHitFxName(string name)
    {
      this.m_hitFxName = name;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLoopFxName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetDisableInputGameObject(GameObject go)
    {
      this.m_disableInputGameObject = go;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTouchFXParentActive(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    public struct TouchState
    {
      public int fingerId;
      public Vector2 position;
      public TouchPhase phase;
    }
  }
}
