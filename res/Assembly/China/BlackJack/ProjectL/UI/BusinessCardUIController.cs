﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BusinessCardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BusinessCardUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateController;
    [AutoBind("./PlayerInfoPanel/Detail/Title/PlayerImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeadIcon;
    [AutoBind("./PlayerInfoPanel/Detail/Title/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./PlayerInfoPanel/Detail/Title/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./PlayerInfoPanel/Detail/Title/PointValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerPointText;
    [AutoBind("./PlayerInfoPanel/Detail/Title/LikeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLikesCountText;
    [AutoBind("./PlayerInfoPanel/Detail/UnderGroup/RandomToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_randomShowToggle;
    [AutoBind("./PlayerInfoPanel/Detail/GoodImageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_likeButton;
    [AutoBind("./PlayerInfoPanel/Detail/GoodImageButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_likeUIStateController;
    [AutoBind("./PlayerInfoPanel/Detail/UnderGroup/HeroMomentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroMomentButton;
    [AutoBind("./PlayerInfoPanel/Detail/UnderGroup/PeakGloryButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakGloryButton;
    [AutoBind("./PlayerInfoPanel/Detail/PlayerTitle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakPlayerTitleGo;
    [AutoBind("./PlayerInfoPanel/Detail/PlayerTitle/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakPlayerTitleText;
    [AutoBind("./PlayerInfoPanel/Detail/PlayerTitle/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakPlayerTitleImage;
    [AutoBind("./PlayerInfoPanel/Detail/Graphics", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teamGraphicsParentGameObject;
    [AutoBind("./PlayerInfoPanel/Detail/Graphics/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char0Dummy;
    [AutoBind("./PlayerInfoPanel/Detail/Graphics/1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char1Dummy;
    [AutoBind("./PlayerInfoPanel/Detail/Graphics/2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char2Dummy;
    [AutoBind("./PlayerInfoPanel/Detail/Graphics/3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char3Dummy;
    [AutoBind("./PlayerInfoPanel/Detail/Graphics/4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char4Dummy;
    [AutoBind("./PersonalSign/SignDetailText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerSignText;
    [AutoBind("./PersonalSign/SignInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_playerInputField;
    [AutoBind("./PersonalSign/ChangeSignButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeSignButton;
    [AutoBind("./PersonalPanel/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGroupObj;
    [AutoBind("./PersonalPanel/AllHeroPower", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerAllHeroPowerText;
    [AutoBind("./PersonalPanel/ReachedAchievement", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerReachedAchievementText;
    [AutoBind("./PersonalPanel/MasterJob", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerMasterJobCountText;
    [AutoBind("./PersonalPanel/Stage", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_stageGoalText;
    [AutoBind("./PersonalPanel/Rift", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_riftGoalText;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./PlayerInfoPanel/ARButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_arButton;
    [AutoBind("./SetPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_setPanelUIStateController;
    [AutoBind("./SetPanel/BlackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_setPanelBGButton;
    [AutoBind("./SetPanel/HeroInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroInfoPanelUIStateController;
    [AutoBind("./SetPanel/HeroInfoPanel/SetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroInfoPanelSettingButton;
    [AutoBind("./SetPanel/HeroInfoPanel/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroInfoPanelChangeButton;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/ArmyImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoArmyImage;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Lv/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoLevelText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoNameText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Job/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoJobText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Power/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPowerText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroInfoGraphicDummy;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Equip/EquipmentListGroup/Equipment1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipment1Dummy;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Equip/EquipmentListGroup/Equipment2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipment2Dummy;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Equip/EquipmentListGroup/Equipment3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipment3Dummy;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Equip/EquipmentListGroup/Equipment4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipment4Dummy;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/HP/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoPropHPImg;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/DF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoPropDFImg;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/AT/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoPropATImg;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/MagicDF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoPropMagicDFImg;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/Magic/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoPropMagicImg;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/DEX/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoPropDEXImg;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/HP/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropHPValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/DF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropDFValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/AT/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropATValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/MagicDF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropMagicDFValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/Magic/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropMagicValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/DEX/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropDEXValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/HP/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropHPAddText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/DF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropDFAddText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/AT/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropATAddText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/MagicDF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropMagicDFAddText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/Magic/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropMagicAddText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/DEX/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropDEXAddText;
    [AutoBind("./Prefab/EquipmentItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentItemPrefab;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipItemDescBGButton;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipItemDescStateController;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescTitleText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescLvText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescExpText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescIconImage;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescIconBg;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSSREffect;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescProgressImage;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescStarGroup;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescEquipLimitContent;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropContent;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropATGo;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropATValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropDFGo;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropDFValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropHPGo;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropHPValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropMagiccGo;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropMagicValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropMagicDFGo;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropMagicDFValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropDexGo;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropDexValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSkillContent;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/SkillContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillNameText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/SkillContent/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillLvText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/SkillContent/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillOwnerText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/SkillContent/DescScrollView/Viewport/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillDescText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescNotEquipSkillTip;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Talent&Skill/Talent", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_talentIconImage;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Talent&Skill/Talent", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_talentButton;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Talent&Skill/HeroSkillItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroCurSkillGroup;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Talent&Skill/SkillItemDescPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroInfoCurSkillDescPanel;
    [AutoBind("./SetPanel/SetCharPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_setCharInfoPanelUIStateController;
    [AutoBind("./SetPanel/SetCharPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_setCharInfoPanelConfirmButton;
    [AutoBind("./SetPanel/SetCharPanel/CharFace", AutoBindAttribute.InitState.NotInit, false)]
    private Dropdown m_charFaceDropdown;
    [AutoBind("./SetPanel/SetCharPanel/CharActive", AutoBindAttribute.InitState.NotInit, false)]
    private Dropdown m_charActiveDropdown;
    [AutoBind("./SetPanel/SetCharPanel/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGraphicParentDummy;
    [AutoBind("./SetPanel/SetCharPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_charNameText;
    [AutoBind("./SetPanel/HeroSelectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroSelectPanelUIStateController;
    [AutoBind("./SetPanel/HeroSelectPanel/HeroListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroListContent;
    [AutoBind("./SetPanel/HeroSelectPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroSelectPanelConfirmButton;
    [AutoBind("./Prefab/HeroListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroListItemPrefab;
    [AutoBind("./Prefab/HeroCharItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroCharItemPrefab;
    [AutoBind("./Prefab/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGroupItemPrefab;
    private UISpineGraphic m_currentCharSpineGraphic;
    private HeroDirectionType m_currentHeroDirectionType;
    private HeroActionType m_currentHeroAnimationType;
    private BusinessCard m_businessCard;
    private bool m_isMe;
    private int m_currentSelectPositionIndex;
    private BattleHero m_currentSelectBattleHero;
    private List<GameObject> m_skillDummyList;
    private List<BusinessCardHeroSet> m_currentHeroSetList;
    private List<BusinessCardUIController.GraphicInfo> m_graphicList;
    private List<Hero> m_heroSortList;
    private bool[] m_isSelectArray;
    private const int HeroTeamMaxCount = 5;
    private float m_fDebugReportTime;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private BusinessCardUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_ShowOpenState_hotfix;
    private LuaFunction m_SetPlayerInfoBusinessCardBooleanBooleanBoolean_hotfix;
    private LuaFunction m_CheckARState_hotfix;
    private LuaFunction m_SetPeakArenaTitleInt32_hotfix;
    private LuaFunction m_GetPointTextBusinessCard_hotfix;
    private LuaFunction m_GetRankStrBusinessCard_hotfix;
    private LuaFunction m_SetTeamBusinessCardBoolean_hotfix;
    private LuaFunction m_OnHeroCharClickBusinessCardHeroCharItemUIController_hotfix;
    private LuaFunction m_ClearTeam_hotfix;
    private LuaFunction m_SetRandomShowToggleOff_hotfix;
    private LuaFunction m_SetHeroInfoPanelBattleHeroBoolean_hotfix;
    private LuaFunction m_SetHeroTalentBattleHero_hotfix;
    private LuaFunction m_SetHeroPropertyBattleHero_hotfix;
    private LuaFunction m_SetCurSelectSkillsBattleHero_hotfix;
    private LuaFunction m_OnSkillItemClickHeroSkillItemUIController_hotfix;
    private LuaFunction m_OnTalentItemClick_hotfix;
    private LuaFunction m_SetEquipmentList`1_hotfix;
    private LuaFunction m_ClearEquipmentItem_hotfix;
    private LuaFunction m_SetEquipmentItemInfoBattleHeroEquipmentGameObject_hotfix;
    private LuaFunction m_OnEquipmentItemClickBusinessCardHeroEquipmentItemController_hotfix;
    private LuaFunction m_OnEquipmentItemBGButtonClick_hotfix;
    private LuaFunction m_SetEquipmentItemDescBattleHeroEquipment_hotfix;
    private LuaFunction m_SetPropItemsPropertyModifyTypeInt32Int32Int32_hotfix;
    private LuaFunction m_SetCharInfoPanelBattleHeroInt32_hotfix;
    private LuaFunction m_OnCharDiretionValueChangedInt32_hotfix;
    private LuaFunction m_OnCharAnimationValueChangedInt32_hotfix;
    private LuaFunction m_CreateSpineGraphicConfigDataJobConnectionInfoConfigDataModelSkinResourceInfoBooleanHeroDirectionTypeHeroActionType_hotfix;
    private LuaFunction m_SetCharActionUISpineGraphicHeroDirectionTypeHeroActionType_hotfix;
    private LuaFunction m_SetHeroSelectPanelBusinessCard_hotfix;
    private LuaFunction m_HeroListItemCompareHeroHero_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnLikeButtonClick_hotfix;
    private LuaFunction m_OnHeroMomentClick_hotfix;
    private LuaFunction m_OnPeakGloryClick_hotfix;
    private LuaFunction m_OnRandowShowToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnSetPanelBGButtonClick_hotfix;
    private LuaFunction m_OnChangeSignButtonClick_hotfix;
    private LuaFunction m_OnArButtonClick_hotfix;
    private LuaFunction m_OnPlayerSignEditEndInputField_hotfix;
    private LuaFunction m_PlayerSignUpdateSuccessString_hotfix;
    private LuaFunction m_PlayerSignUpdateEnd_hotfix;
    private LuaFunction m_OnHeroItemClickBusinessCardHeroListItemUIController_hotfix;
    private LuaFunction m_OnHeroInfoPanelSettingButtonClick_hotfix;
    private LuaFunction m_OnHeroInfoPanelChangeButtonClick_hotfix;
    private LuaFunction m_OnSetCharPanelConfirmButtonClick_hotfix;
    private LuaFunction m_OnHeroSelectPanelConfirmButtonClick_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_add_EventOnArClickAction_hotfix;
    private LuaFunction m_remove_EventOnArClickAction_hotfix;
    private LuaFunction m_add_EventOnReturnAction`1_hotfix;
    private LuaFunction m_remove_EventOnReturnAction`1_hotfix;
    private LuaFunction m_add_EventOnLikeAction`1_hotfix;
    private LuaFunction m_remove_EventOnLikeAction`1_hotfix;
    private LuaFunction m_add_EventOnHeroMomentAction_hotfix;
    private LuaFunction m_remove_EventOnHeroMomentAction_hotfix;
    private LuaFunction m_add_EventOnPeakGloryAction_hotfix;
    private LuaFunction m_remove_EventOnPeakGloryAction_hotfix;
    private LuaFunction m_add_EventOnChangeSignAction`1_hotfix;
    private LuaFunction m_remove_EventOnChangeSignAction`1_hotfix;
    private LuaFunction m_add_EventOnUpdateHeroSetAction`1_hotfix;
    private LuaFunction m_remove_EventOnUpdateHeroSetAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private BusinessCardUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOpenState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerInfo(
      BusinessCard businessCard,
      bool isMe,
      bool canSendLike,
      bool isRandomShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckARState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakArenaTitle(int titleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetPointText(BusinessCard businessCard)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetRankStr(BusinessCard businessCard)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTeam(BusinessCard businessCard, bool isRandomShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroCharClick(BusinessCardHeroCharItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomShowToggleOff()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroInfoPanel(BattleHero hero, bool canSetAndChange)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroTalent(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroProperty(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurSelectSkills(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemClick(HeroSkillItemUIController skillCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalentItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipment(List<BattleHeroEquipment> equipments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearEquipmentItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentItemInfo(BattleHeroEquipment equipment, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentItemClick(BusinessCardHeroEquipmentItemController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentItemBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentItemDesc(BattleHeroEquipment equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCharInfoPanel(BattleHero hero, int positionIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCharDiretionValueChanged(int direction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCharAnimationValueChanged(int animation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UISpineGraphic CreateSpineGraphic(
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ConfigDataModelSkinResourceInfo heroSkinResInfo,
      bool isUIModelScale,
      HeroDirectionType directionType = HeroDirectionType.Left,
      HeroActionType actionType = HeroActionType.Idle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCharAction(
      UISpineGraphic g,
      HeroDirectionType direction,
      HeroActionType animation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroSelectPanel(BusinessCard businessCard)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroListItemCompare(Hero h1, Hero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLikeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroMomentClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakGloryClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRandowShowToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSetPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeSignButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerSignEditEnd(InputField input)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerSignUpdateSuccess(string sign)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerSignUpdateEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroItemClick(BusinessCardHeroListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroInfoPanelSettingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroInfoPanelChangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSetCharPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroSelectPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnArClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnLike
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroMoment
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakGlory
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnChangeSign
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<BusinessCardHeroSet>> EventOnUpdateHeroSet
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BusinessCardUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnArClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnArClick()
    {
      this.EventOnArClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn(bool obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn(bool obj)
    {
      this.EventOnReturn = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnLike(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnLike(string obj)
    {
      this.EventOnLike = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHeroMoment()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnHeroMoment()
    {
      this.EventOnHeroMoment = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPeakGlory()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPeakGlory()
    {
      this.EventOnPeakGlory = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangeSign(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChangeSign(string obj)
    {
      this.EventOnChangeSign = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnUpdateHeroSet(List<BusinessCardHeroSet> obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnUpdateHeroSet(List<BusinessCardHeroSet> obj)
    {
      this.EventOnUpdateHeroSet = (Action<List<BusinessCardHeroSet>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [HotFix]
    public class GraphicInfo
    {
      public UISpineGraphic m_graphic;
      public HeroDirectionType m_directionType;
      public HeroActionType m_actionType;

      [MethodImpl((MethodImplOptions) 32768)]
      public GraphicInfo(
        UISpineGraphic graphic,
        HeroDirectionType directionType,
        HeroActionType actionType)
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class LuaExportHelper
    {
      private BusinessCardUIController m_owner;

      public LuaExportHelper(BusinessCardUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnArClick()
      {
        this.m_owner.__callDele_EventOnArClick();
      }

      public void __clearDele_EventOnArClick()
      {
        this.m_owner.__clearDele_EventOnArClick();
      }

      public void __callDele_EventOnReturn(bool obj)
      {
        this.m_owner.__callDele_EventOnReturn(obj);
      }

      public void __clearDele_EventOnReturn(bool obj)
      {
        this.m_owner.__clearDele_EventOnReturn(obj);
      }

      public void __callDele_EventOnLike(string obj)
      {
        this.m_owner.__callDele_EventOnLike(obj);
      }

      public void __clearDele_EventOnLike(string obj)
      {
        this.m_owner.__clearDele_EventOnLike(obj);
      }

      public void __callDele_EventOnHeroMoment()
      {
        this.m_owner.__callDele_EventOnHeroMoment();
      }

      public void __clearDele_EventOnHeroMoment()
      {
        this.m_owner.__clearDele_EventOnHeroMoment();
      }

      public void __callDele_EventOnPeakGlory()
      {
        this.m_owner.__callDele_EventOnPeakGlory();
      }

      public void __clearDele_EventOnPeakGlory()
      {
        this.m_owner.__clearDele_EventOnPeakGlory();
      }

      public void __callDele_EventOnChangeSign(string obj)
      {
        this.m_owner.__callDele_EventOnChangeSign(obj);
      }

      public void __clearDele_EventOnChangeSign(string obj)
      {
        this.m_owner.__clearDele_EventOnChangeSign(obj);
      }

      public void __callDele_EventOnUpdateHeroSet(List<BusinessCardHeroSet> obj)
      {
        this.m_owner.__callDele_EventOnUpdateHeroSet(obj);
      }

      public void __clearDele_EventOnUpdateHeroSet(List<BusinessCardHeroSet> obj)
      {
        this.m_owner.__clearDele_EventOnUpdateHeroSet(obj);
      }

      public CommonUIStateController m_commonUIStateController
      {
        get
        {
          return this.m_owner.m_commonUIStateController;
        }
        set
        {
          this.m_owner.m_commonUIStateController = value;
        }
      }

      public Image m_playerHeadIcon
      {
        get
        {
          return this.m_owner.m_playerHeadIcon;
        }
        set
        {
          this.m_owner.m_playerHeadIcon = value;
        }
      }

      public Text m_playerNameText
      {
        get
        {
          return this.m_owner.m_playerNameText;
        }
        set
        {
          this.m_owner.m_playerNameText = value;
        }
      }

      public Text m_playerLevelText
      {
        get
        {
          return this.m_owner.m_playerLevelText;
        }
        set
        {
          this.m_owner.m_playerLevelText = value;
        }
      }

      public Text m_playerPointText
      {
        get
        {
          return this.m_owner.m_playerPointText;
        }
        set
        {
          this.m_owner.m_playerPointText = value;
        }
      }

      public Text m_playerLikesCountText
      {
        get
        {
          return this.m_owner.m_playerLikesCountText;
        }
        set
        {
          this.m_owner.m_playerLikesCountText = value;
        }
      }

      public Toggle m_randomShowToggle
      {
        get
        {
          return this.m_owner.m_randomShowToggle;
        }
        set
        {
          this.m_owner.m_randomShowToggle = value;
        }
      }

      public Button m_likeButton
      {
        get
        {
          return this.m_owner.m_likeButton;
        }
        set
        {
          this.m_owner.m_likeButton = value;
        }
      }

      public CommonUIStateController m_likeUIStateController
      {
        get
        {
          return this.m_owner.m_likeUIStateController;
        }
        set
        {
          this.m_owner.m_likeUIStateController = value;
        }
      }

      public Button m_heroMomentButton
      {
        get
        {
          return this.m_owner.m_heroMomentButton;
        }
        set
        {
          this.m_owner.m_heroMomentButton = value;
        }
      }

      public Button m_peakGloryButton
      {
        get
        {
          return this.m_owner.m_peakGloryButton;
        }
        set
        {
          this.m_owner.m_peakGloryButton = value;
        }
      }

      public GameObject m_peakPlayerTitleGo
      {
        get
        {
          return this.m_owner.m_peakPlayerTitleGo;
        }
        set
        {
          this.m_owner.m_peakPlayerTitleGo = value;
        }
      }

      public Text m_peakPlayerTitleText
      {
        get
        {
          return this.m_owner.m_peakPlayerTitleText;
        }
        set
        {
          this.m_owner.m_peakPlayerTitleText = value;
        }
      }

      public Image m_peakPlayerTitleImage
      {
        get
        {
          return this.m_owner.m_peakPlayerTitleImage;
        }
        set
        {
          this.m_owner.m_peakPlayerTitleImage = value;
        }
      }

      public GameObject m_teamGraphicsParentGameObject
      {
        get
        {
          return this.m_owner.m_teamGraphicsParentGameObject;
        }
        set
        {
          this.m_owner.m_teamGraphicsParentGameObject = value;
        }
      }

      public GameObject m_char0Dummy
      {
        get
        {
          return this.m_owner.m_char0Dummy;
        }
        set
        {
          this.m_owner.m_char0Dummy = value;
        }
      }

      public GameObject m_char1Dummy
      {
        get
        {
          return this.m_owner.m_char1Dummy;
        }
        set
        {
          this.m_owner.m_char1Dummy = value;
        }
      }

      public GameObject m_char2Dummy
      {
        get
        {
          return this.m_owner.m_char2Dummy;
        }
        set
        {
          this.m_owner.m_char2Dummy = value;
        }
      }

      public GameObject m_char3Dummy
      {
        get
        {
          return this.m_owner.m_char3Dummy;
        }
        set
        {
          this.m_owner.m_char3Dummy = value;
        }
      }

      public GameObject m_char4Dummy
      {
        get
        {
          return this.m_owner.m_char4Dummy;
        }
        set
        {
          this.m_owner.m_char4Dummy = value;
        }
      }

      public Text m_playerSignText
      {
        get
        {
          return this.m_owner.m_playerSignText;
        }
        set
        {
          this.m_owner.m_playerSignText = value;
        }
      }

      public InputField m_playerInputField
      {
        get
        {
          return this.m_owner.m_playerInputField;
        }
        set
        {
          this.m_owner.m_playerInputField = value;
        }
      }

      public Button m_changeSignButton
      {
        get
        {
          return this.m_owner.m_changeSignButton;
        }
        set
        {
          this.m_owner.m_changeSignButton = value;
        }
      }

      public GameObject m_heroGroupObj
      {
        get
        {
          return this.m_owner.m_heroGroupObj;
        }
        set
        {
          this.m_owner.m_heroGroupObj = value;
        }
      }

      public Text m_playerAllHeroPowerText
      {
        get
        {
          return this.m_owner.m_playerAllHeroPowerText;
        }
        set
        {
          this.m_owner.m_playerAllHeroPowerText = value;
        }
      }

      public Text m_playerReachedAchievementText
      {
        get
        {
          return this.m_owner.m_playerReachedAchievementText;
        }
        set
        {
          this.m_owner.m_playerReachedAchievementText = value;
        }
      }

      public Text m_playerMasterJobCountText
      {
        get
        {
          return this.m_owner.m_playerMasterJobCountText;
        }
        set
        {
          this.m_owner.m_playerMasterJobCountText = value;
        }
      }

      public Text m_stageGoalText
      {
        get
        {
          return this.m_owner.m_stageGoalText;
        }
        set
        {
          this.m_owner.m_stageGoalText = value;
        }
      }

      public Text m_riftGoalText
      {
        get
        {
          return this.m_owner.m_riftGoalText;
        }
        set
        {
          this.m_owner.m_riftGoalText = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public ButtonEx m_arButton
      {
        get
        {
          return this.m_owner.m_arButton;
        }
        set
        {
          this.m_owner.m_arButton = value;
        }
      }

      public CommonUIStateController m_setPanelUIStateController
      {
        get
        {
          return this.m_owner.m_setPanelUIStateController;
        }
        set
        {
          this.m_owner.m_setPanelUIStateController = value;
        }
      }

      public Button m_setPanelBGButton
      {
        get
        {
          return this.m_owner.m_setPanelBGButton;
        }
        set
        {
          this.m_owner.m_setPanelBGButton = value;
        }
      }

      public CommonUIStateController m_heroInfoPanelUIStateController
      {
        get
        {
          return this.m_owner.m_heroInfoPanelUIStateController;
        }
        set
        {
          this.m_owner.m_heroInfoPanelUIStateController = value;
        }
      }

      public Button m_heroInfoPanelSettingButton
      {
        get
        {
          return this.m_owner.m_heroInfoPanelSettingButton;
        }
        set
        {
          this.m_owner.m_heroInfoPanelSettingButton = value;
        }
      }

      public Button m_heroInfoPanelChangeButton
      {
        get
        {
          return this.m_owner.m_heroInfoPanelChangeButton;
        }
        set
        {
          this.m_owner.m_heroInfoPanelChangeButton = value;
        }
      }

      public Image m_heroInfoArmyImage
      {
        get
        {
          return this.m_owner.m_heroInfoArmyImage;
        }
        set
        {
          this.m_owner.m_heroInfoArmyImage = value;
        }
      }

      public Text m_heroInfoLevelText
      {
        get
        {
          return this.m_owner.m_heroInfoLevelText;
        }
        set
        {
          this.m_owner.m_heroInfoLevelText = value;
        }
      }

      public Text m_heroInfoNameText
      {
        get
        {
          return this.m_owner.m_heroInfoNameText;
        }
        set
        {
          this.m_owner.m_heroInfoNameText = value;
        }
      }

      public Text m_heroInfoJobText
      {
        get
        {
          return this.m_owner.m_heroInfoJobText;
        }
        set
        {
          this.m_owner.m_heroInfoJobText = value;
        }
      }

      public Text m_heroInfoPowerText
      {
        get
        {
          return this.m_owner.m_heroInfoPowerText;
        }
        set
        {
          this.m_owner.m_heroInfoPowerText = value;
        }
      }

      public GameObject m_heroInfoGraphicDummy
      {
        get
        {
          return this.m_owner.m_heroInfoGraphicDummy;
        }
        set
        {
          this.m_owner.m_heroInfoGraphicDummy = value;
        }
      }

      public GameObject m_equipment1Dummy
      {
        get
        {
          return this.m_owner.m_equipment1Dummy;
        }
        set
        {
          this.m_owner.m_equipment1Dummy = value;
        }
      }

      public GameObject m_equipment2Dummy
      {
        get
        {
          return this.m_owner.m_equipment2Dummy;
        }
        set
        {
          this.m_owner.m_equipment2Dummy = value;
        }
      }

      public GameObject m_equipment3Dummy
      {
        get
        {
          return this.m_owner.m_equipment3Dummy;
        }
        set
        {
          this.m_owner.m_equipment3Dummy = value;
        }
      }

      public GameObject m_equipment4Dummy
      {
        get
        {
          return this.m_owner.m_equipment4Dummy;
        }
        set
        {
          this.m_owner.m_equipment4Dummy = value;
        }
      }

      public Image m_heroInfoPropHPImg
      {
        get
        {
          return this.m_owner.m_heroInfoPropHPImg;
        }
        set
        {
          this.m_owner.m_heroInfoPropHPImg = value;
        }
      }

      public Image m_heroInfoPropDFImg
      {
        get
        {
          return this.m_owner.m_heroInfoPropDFImg;
        }
        set
        {
          this.m_owner.m_heroInfoPropDFImg = value;
        }
      }

      public Image m_heroInfoPropATImg
      {
        get
        {
          return this.m_owner.m_heroInfoPropATImg;
        }
        set
        {
          this.m_owner.m_heroInfoPropATImg = value;
        }
      }

      public Image m_heroInfoPropMagicDFImg
      {
        get
        {
          return this.m_owner.m_heroInfoPropMagicDFImg;
        }
        set
        {
          this.m_owner.m_heroInfoPropMagicDFImg = value;
        }
      }

      public Image m_heroInfoPropMagicImg
      {
        get
        {
          return this.m_owner.m_heroInfoPropMagicImg;
        }
        set
        {
          this.m_owner.m_heroInfoPropMagicImg = value;
        }
      }

      public Image m_heroInfoPropDEXImg
      {
        get
        {
          return this.m_owner.m_heroInfoPropDEXImg;
        }
        set
        {
          this.m_owner.m_heroInfoPropDEXImg = value;
        }
      }

      public Text m_heroInfoPropHPValueText
      {
        get
        {
          return this.m_owner.m_heroInfoPropHPValueText;
        }
        set
        {
          this.m_owner.m_heroInfoPropHPValueText = value;
        }
      }

      public Text m_heroInfoPropDFValueText
      {
        get
        {
          return this.m_owner.m_heroInfoPropDFValueText;
        }
        set
        {
          this.m_owner.m_heroInfoPropDFValueText = value;
        }
      }

      public Text m_heroInfoPropATValueText
      {
        get
        {
          return this.m_owner.m_heroInfoPropATValueText;
        }
        set
        {
          this.m_owner.m_heroInfoPropATValueText = value;
        }
      }

      public Text m_heroInfoPropMagicDFValueText
      {
        get
        {
          return this.m_owner.m_heroInfoPropMagicDFValueText;
        }
        set
        {
          this.m_owner.m_heroInfoPropMagicDFValueText = value;
        }
      }

      public Text m_heroInfoPropMagicValueText
      {
        get
        {
          return this.m_owner.m_heroInfoPropMagicValueText;
        }
        set
        {
          this.m_owner.m_heroInfoPropMagicValueText = value;
        }
      }

      public Text m_heroInfoPropDEXValueText
      {
        get
        {
          return this.m_owner.m_heroInfoPropDEXValueText;
        }
        set
        {
          this.m_owner.m_heroInfoPropDEXValueText = value;
        }
      }

      public Text m_heroInfoPropHPAddText
      {
        get
        {
          return this.m_owner.m_heroInfoPropHPAddText;
        }
        set
        {
          this.m_owner.m_heroInfoPropHPAddText = value;
        }
      }

      public Text m_heroInfoPropDFAddText
      {
        get
        {
          return this.m_owner.m_heroInfoPropDFAddText;
        }
        set
        {
          this.m_owner.m_heroInfoPropDFAddText = value;
        }
      }

      public Text m_heroInfoPropATAddText
      {
        get
        {
          return this.m_owner.m_heroInfoPropATAddText;
        }
        set
        {
          this.m_owner.m_heroInfoPropATAddText = value;
        }
      }

      public Text m_heroInfoPropMagicDFAddText
      {
        get
        {
          return this.m_owner.m_heroInfoPropMagicDFAddText;
        }
        set
        {
          this.m_owner.m_heroInfoPropMagicDFAddText = value;
        }
      }

      public Text m_heroInfoPropMagicAddText
      {
        get
        {
          return this.m_owner.m_heroInfoPropMagicAddText;
        }
        set
        {
          this.m_owner.m_heroInfoPropMagicAddText = value;
        }
      }

      public Text m_heroInfoPropDEXAddText
      {
        get
        {
          return this.m_owner.m_heroInfoPropDEXAddText;
        }
        set
        {
          this.m_owner.m_heroInfoPropDEXAddText = value;
        }
      }

      public GameObject m_equipmentItemPrefab
      {
        get
        {
          return this.m_owner.m_equipmentItemPrefab;
        }
        set
        {
          this.m_owner.m_equipmentItemPrefab = value;
        }
      }

      public Button m_equipItemDescBGButton
      {
        get
        {
          return this.m_owner.m_equipItemDescBGButton;
        }
        set
        {
          this.m_owner.m_equipItemDescBGButton = value;
        }
      }

      public CommonUIStateController m_equipItemDescStateController
      {
        get
        {
          return this.m_owner.m_equipItemDescStateController;
        }
        set
        {
          this.m_owner.m_equipItemDescStateController = value;
        }
      }

      public Text m_equipItemDescTitleText
      {
        get
        {
          return this.m_owner.m_equipItemDescTitleText;
        }
        set
        {
          this.m_owner.m_equipItemDescTitleText = value;
        }
      }

      public Text m_equipItemDescLvText
      {
        get
        {
          return this.m_owner.m_equipItemDescLvText;
        }
        set
        {
          this.m_owner.m_equipItemDescLvText = value;
        }
      }

      public Text m_equipItemDescExpText
      {
        get
        {
          return this.m_owner.m_equipItemDescExpText;
        }
        set
        {
          this.m_owner.m_equipItemDescExpText = value;
        }
      }

      public Image m_equipItemDescIconImage
      {
        get
        {
          return this.m_owner.m_equipItemDescIconImage;
        }
        set
        {
          this.m_owner.m_equipItemDescIconImage = value;
        }
      }

      public Image m_equipItemDescIconBg
      {
        get
        {
          return this.m_owner.m_equipItemDescIconBg;
        }
        set
        {
          this.m_owner.m_equipItemDescIconBg = value;
        }
      }

      public GameObject m_equipItemDescSSREffect
      {
        get
        {
          return this.m_owner.m_equipItemDescSSREffect;
        }
        set
        {
          this.m_owner.m_equipItemDescSSREffect = value;
        }
      }

      public Image m_equipItemDescProgressImage
      {
        get
        {
          return this.m_owner.m_equipItemDescProgressImage;
        }
        set
        {
          this.m_owner.m_equipItemDescProgressImage = value;
        }
      }

      public GameObject m_equipItemDescStarGroup
      {
        get
        {
          return this.m_owner.m_equipItemDescStarGroup;
        }
        set
        {
          this.m_owner.m_equipItemDescStarGroup = value;
        }
      }

      public GameObject m_equipItemDescEquipLimitContent
      {
        get
        {
          return this.m_owner.m_equipItemDescEquipLimitContent;
        }
        set
        {
          this.m_owner.m_equipItemDescEquipLimitContent = value;
        }
      }

      public GameObject m_equipItemDescPropContent
      {
        get
        {
          return this.m_owner.m_equipItemDescPropContent;
        }
        set
        {
          this.m_owner.m_equipItemDescPropContent = value;
        }
      }

      public GameObject m_equipItemDescPropATGo
      {
        get
        {
          return this.m_owner.m_equipItemDescPropATGo;
        }
        set
        {
          this.m_owner.m_equipItemDescPropATGo = value;
        }
      }

      public Text m_equipItemDescPropATValueText
      {
        get
        {
          return this.m_owner.m_equipItemDescPropATValueText;
        }
        set
        {
          this.m_owner.m_equipItemDescPropATValueText = value;
        }
      }

      public GameObject m_equipItemDescPropDFGo
      {
        get
        {
          return this.m_owner.m_equipItemDescPropDFGo;
        }
        set
        {
          this.m_owner.m_equipItemDescPropDFGo = value;
        }
      }

      public Text m_equipItemDescPropDFValueText
      {
        get
        {
          return this.m_owner.m_equipItemDescPropDFValueText;
        }
        set
        {
          this.m_owner.m_equipItemDescPropDFValueText = value;
        }
      }

      public GameObject m_equipItemDescPropHPGo
      {
        get
        {
          return this.m_owner.m_equipItemDescPropHPGo;
        }
        set
        {
          this.m_owner.m_equipItemDescPropHPGo = value;
        }
      }

      public Text m_equipItemDescPropHPValueText
      {
        get
        {
          return this.m_owner.m_equipItemDescPropHPValueText;
        }
        set
        {
          this.m_owner.m_equipItemDescPropHPValueText = value;
        }
      }

      public GameObject m_equipItemDescPropMagiccGo
      {
        get
        {
          return this.m_owner.m_equipItemDescPropMagiccGo;
        }
        set
        {
          this.m_owner.m_equipItemDescPropMagiccGo = value;
        }
      }

      public Text m_equipItemDescPropMagicValueText
      {
        get
        {
          return this.m_owner.m_equipItemDescPropMagicValueText;
        }
        set
        {
          this.m_owner.m_equipItemDescPropMagicValueText = value;
        }
      }

      public GameObject m_equipItemDescPropMagicDFGo
      {
        get
        {
          return this.m_owner.m_equipItemDescPropMagicDFGo;
        }
        set
        {
          this.m_owner.m_equipItemDescPropMagicDFGo = value;
        }
      }

      public Text m_equipItemDescPropMagicDFValueText
      {
        get
        {
          return this.m_owner.m_equipItemDescPropMagicDFValueText;
        }
        set
        {
          this.m_owner.m_equipItemDescPropMagicDFValueText = value;
        }
      }

      public GameObject m_equipItemDescPropDexGo
      {
        get
        {
          return this.m_owner.m_equipItemDescPropDexGo;
        }
        set
        {
          this.m_owner.m_equipItemDescPropDexGo = value;
        }
      }

      public Text m_equipItemDescPropDexValueText
      {
        get
        {
          return this.m_owner.m_equipItemDescPropDexValueText;
        }
        set
        {
          this.m_owner.m_equipItemDescPropDexValueText = value;
        }
      }

      public GameObject m_equipItemDescSkillContent
      {
        get
        {
          return this.m_owner.m_equipItemDescSkillContent;
        }
        set
        {
          this.m_owner.m_equipItemDescSkillContent = value;
        }
      }

      public Text m_equipItemDescSkillNameText
      {
        get
        {
          return this.m_owner.m_equipItemDescSkillNameText;
        }
        set
        {
          this.m_owner.m_equipItemDescSkillNameText = value;
        }
      }

      public Text m_equipItemDescSkillLvText
      {
        get
        {
          return this.m_owner.m_equipItemDescSkillLvText;
        }
        set
        {
          this.m_owner.m_equipItemDescSkillLvText = value;
        }
      }

      public Text m_equipItemDescSkillOwnerText
      {
        get
        {
          return this.m_owner.m_equipItemDescSkillOwnerText;
        }
        set
        {
          this.m_owner.m_equipItemDescSkillOwnerText = value;
        }
      }

      public Text m_equipItemDescSkillDescText
      {
        get
        {
          return this.m_owner.m_equipItemDescSkillDescText;
        }
        set
        {
          this.m_owner.m_equipItemDescSkillDescText = value;
        }
      }

      public GameObject m_equipItemDescNotEquipSkillTip
      {
        get
        {
          return this.m_owner.m_equipItemDescNotEquipSkillTip;
        }
        set
        {
          this.m_owner.m_equipItemDescNotEquipSkillTip = value;
        }
      }

      public Image m_talentIconImage
      {
        get
        {
          return this.m_owner.m_talentIconImage;
        }
        set
        {
          this.m_owner.m_talentIconImage = value;
        }
      }

      public Button m_talentButton
      {
        get
        {
          return this.m_owner.m_talentButton;
        }
        set
        {
          this.m_owner.m_talentButton = value;
        }
      }

      public GameObject m_heroCurSkillGroup
      {
        get
        {
          return this.m_owner.m_heroCurSkillGroup;
        }
        set
        {
          this.m_owner.m_heroCurSkillGroup = value;
        }
      }

      public GameObject m_heroInfoCurSkillDescPanel
      {
        get
        {
          return this.m_owner.m_heroInfoCurSkillDescPanel;
        }
        set
        {
          this.m_owner.m_heroInfoCurSkillDescPanel = value;
        }
      }

      public CommonUIStateController m_setCharInfoPanelUIStateController
      {
        get
        {
          return this.m_owner.m_setCharInfoPanelUIStateController;
        }
        set
        {
          this.m_owner.m_setCharInfoPanelUIStateController = value;
        }
      }

      public Button m_setCharInfoPanelConfirmButton
      {
        get
        {
          return this.m_owner.m_setCharInfoPanelConfirmButton;
        }
        set
        {
          this.m_owner.m_setCharInfoPanelConfirmButton = value;
        }
      }

      public Dropdown m_charFaceDropdown
      {
        get
        {
          return this.m_owner.m_charFaceDropdown;
        }
        set
        {
          this.m_owner.m_charFaceDropdown = value;
        }
      }

      public Dropdown m_charActiveDropdown
      {
        get
        {
          return this.m_owner.m_charActiveDropdown;
        }
        set
        {
          this.m_owner.m_charActiveDropdown = value;
        }
      }

      public GameObject m_charGraphicParentDummy
      {
        get
        {
          return this.m_owner.m_charGraphicParentDummy;
        }
        set
        {
          this.m_owner.m_charGraphicParentDummy = value;
        }
      }

      public Text m_charNameText
      {
        get
        {
          return this.m_owner.m_charNameText;
        }
        set
        {
          this.m_owner.m_charNameText = value;
        }
      }

      public CommonUIStateController m_heroSelectPanelUIStateController
      {
        get
        {
          return this.m_owner.m_heroSelectPanelUIStateController;
        }
        set
        {
          this.m_owner.m_heroSelectPanelUIStateController = value;
        }
      }

      public GameObject m_heroListContent
      {
        get
        {
          return this.m_owner.m_heroListContent;
        }
        set
        {
          this.m_owner.m_heroListContent = value;
        }
      }

      public Button m_heroSelectPanelConfirmButton
      {
        get
        {
          return this.m_owner.m_heroSelectPanelConfirmButton;
        }
        set
        {
          this.m_owner.m_heroSelectPanelConfirmButton = value;
        }
      }

      public GameObject m_heroListItemPrefab
      {
        get
        {
          return this.m_owner.m_heroListItemPrefab;
        }
        set
        {
          this.m_owner.m_heroListItemPrefab = value;
        }
      }

      public GameObject m_heroCharItemPrefab
      {
        get
        {
          return this.m_owner.m_heroCharItemPrefab;
        }
        set
        {
          this.m_owner.m_heroCharItemPrefab = value;
        }
      }

      public GameObject m_heroGroupItemPrefab
      {
        get
        {
          return this.m_owner.m_heroGroupItemPrefab;
        }
        set
        {
          this.m_owner.m_heroGroupItemPrefab = value;
        }
      }

      public UISpineGraphic m_currentCharSpineGraphic
      {
        get
        {
          return this.m_owner.m_currentCharSpineGraphic;
        }
        set
        {
          this.m_owner.m_currentCharSpineGraphic = value;
        }
      }

      public HeroDirectionType m_currentHeroDirectionType
      {
        get
        {
          return this.m_owner.m_currentHeroDirectionType;
        }
        set
        {
          this.m_owner.m_currentHeroDirectionType = value;
        }
      }

      public HeroActionType m_currentHeroAnimationType
      {
        get
        {
          return this.m_owner.m_currentHeroAnimationType;
        }
        set
        {
          this.m_owner.m_currentHeroAnimationType = value;
        }
      }

      public BusinessCard m_businessCard
      {
        get
        {
          return this.m_owner.m_businessCard;
        }
        set
        {
          this.m_owner.m_businessCard = value;
        }
      }

      public bool m_isMe
      {
        get
        {
          return this.m_owner.m_isMe;
        }
        set
        {
          this.m_owner.m_isMe = value;
        }
      }

      public int m_currentSelectPositionIndex
      {
        get
        {
          return this.m_owner.m_currentSelectPositionIndex;
        }
        set
        {
          this.m_owner.m_currentSelectPositionIndex = value;
        }
      }

      public BattleHero m_currentSelectBattleHero
      {
        get
        {
          return this.m_owner.m_currentSelectBattleHero;
        }
        set
        {
          this.m_owner.m_currentSelectBattleHero = value;
        }
      }

      public List<GameObject> m_skillDummyList
      {
        get
        {
          return this.m_owner.m_skillDummyList;
        }
        set
        {
          this.m_owner.m_skillDummyList = value;
        }
      }

      public List<BusinessCardHeroSet> m_currentHeroSetList
      {
        get
        {
          return this.m_owner.m_currentHeroSetList;
        }
        set
        {
          this.m_owner.m_currentHeroSetList = value;
        }
      }

      public List<BusinessCardUIController.GraphicInfo> m_graphicList
      {
        get
        {
          return this.m_owner.m_graphicList;
        }
        set
        {
          this.m_owner.m_graphicList = value;
        }
      }

      public List<Hero> m_heroSortList
      {
        get
        {
          return this.m_owner.m_heroSortList;
        }
        set
        {
          this.m_owner.m_heroSortList = value;
        }
      }

      public bool[] m_isSelectArray
      {
        get
        {
          return this.m_owner.m_isSelectArray;
        }
        set
        {
          this.m_owner.m_isSelectArray = value;
        }
      }

      public static int HeroTeamMaxCount
      {
        get
        {
          return 5;
        }
      }

      public float m_fDebugReportTime
      {
        get
        {
          return this.m_owner.m_fDebugReportTime;
        }
        set
        {
          this.m_owner.m_fDebugReportTime = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public bool CheckARState()
      {
        return this.m_owner.CheckARState();
      }

      public void SetPeakArenaTitle(int titleId)
      {
        this.m_owner.SetPeakArenaTitle(titleId);
      }

      public string GetPointText(BusinessCard businessCard)
      {
        return this.m_owner.GetPointText(businessCard);
      }

      public string GetRankStr(BusinessCard businessCard)
      {
        return this.m_owner.GetRankStr(businessCard);
      }

      public void SetTeam(BusinessCard businessCard, bool isRandomShow)
      {
        this.m_owner.SetTeam(businessCard, isRandomShow);
      }

      public void OnHeroCharClick(BusinessCardHeroCharItemUIController ctrl)
      {
        this.m_owner.OnHeroCharClick(ctrl);
      }

      public void ClearTeam()
      {
        this.m_owner.ClearTeam();
      }

      public void SetHeroInfoPanel(BattleHero hero, bool canSetAndChange)
      {
        this.m_owner.SetHeroInfoPanel(hero, canSetAndChange);
      }

      public void SetHeroTalent(BattleHero battleHero)
      {
        this.m_owner.SetHeroTalent(battleHero);
      }

      public void SetHeroProperty(BattleHero battleHero)
      {
        this.m_owner.SetHeroProperty(battleHero);
      }

      public void SetCurSelectSkills(BattleHero hero)
      {
        this.m_owner.SetCurSelectSkills(hero);
      }

      public void OnSkillItemClick(HeroSkillItemUIController skillCtrl)
      {
        this.m_owner.OnSkillItemClick(skillCtrl);
      }

      public void OnTalentItemClick()
      {
        this.m_owner.OnTalentItemClick();
      }

      public void SetEquipment(List<BattleHeroEquipment> equipments)
      {
        this.m_owner.SetEquipment(equipments);
      }

      public void ClearEquipmentItem()
      {
        this.m_owner.ClearEquipmentItem();
      }

      public void SetEquipmentItemInfo(BattleHeroEquipment equipment, GameObject parent)
      {
        this.m_owner.SetEquipmentItemInfo(equipment, parent);
      }

      public void OnEquipmentItemClick(BusinessCardHeroEquipmentItemController ctrl)
      {
        this.m_owner.OnEquipmentItemClick(ctrl);
      }

      public void OnEquipmentItemBGButtonClick()
      {
        this.m_owner.OnEquipmentItemBGButtonClick();
      }

      public void SetEquipmentItemDesc(BattleHeroEquipment equipment)
      {
        this.m_owner.SetEquipmentItemDesc(equipment);
      }

      public void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
      {
        this.m_owner.SetPropItems(type, value, addValue, level);
      }

      public void SetCharInfoPanel(BattleHero hero, int positionIndex)
      {
        this.m_owner.SetCharInfoPanel(hero, positionIndex);
      }

      public void OnCharDiretionValueChanged(int direction)
      {
        this.m_owner.OnCharDiretionValueChanged(direction);
      }

      public void OnCharAnimationValueChanged(int animation)
      {
        this.m_owner.OnCharAnimationValueChanged(animation);
      }

      public UISpineGraphic CreateSpineGraphic(
        ConfigDataJobConnectionInfo jobConnectionInfo,
        ConfigDataModelSkinResourceInfo heroSkinResInfo,
        bool isUIModelScale,
        HeroDirectionType directionType,
        HeroActionType actionType)
      {
        return this.m_owner.CreateSpineGraphic(jobConnectionInfo, heroSkinResInfo, isUIModelScale, directionType, actionType);
      }

      public void SetCharAction(
        UISpineGraphic g,
        HeroDirectionType direction,
        HeroActionType animation)
      {
        this.m_owner.SetCharAction(g, direction, animation);
      }

      public void SetHeroSelectPanel(BusinessCard businessCard)
      {
        this.m_owner.SetHeroSelectPanel(businessCard);
      }

      public int HeroListItemCompare(Hero h1, Hero h2)
      {
        return this.m_owner.HeroListItemCompare(h1, h2);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnLikeButtonClick()
      {
        this.m_owner.OnLikeButtonClick();
      }

      public void OnHeroMomentClick()
      {
        this.m_owner.OnHeroMomentClick();
      }

      public void OnPeakGloryClick()
      {
        this.m_owner.OnPeakGloryClick();
      }

      public void OnRandowShowToggleValueChanged(bool isOn)
      {
        this.m_owner.OnRandowShowToggleValueChanged(isOn);
      }

      public void OnSetPanelBGButtonClick()
      {
        this.m_owner.OnSetPanelBGButtonClick();
      }

      public void OnChangeSignButtonClick()
      {
        this.m_owner.OnChangeSignButtonClick();
      }

      public void OnArButtonClick()
      {
        this.m_owner.OnArButtonClick();
      }

      public void OnPlayerSignEditEnd(InputField input)
      {
        this.m_owner.OnPlayerSignEditEnd(input);
      }

      public void OnHeroItemClick(BusinessCardHeroListItemUIController ctrl)
      {
        this.m_owner.OnHeroItemClick(ctrl);
      }

      public void OnHeroInfoPanelSettingButtonClick()
      {
        this.m_owner.OnHeroInfoPanelSettingButtonClick();
      }

      public void OnHeroInfoPanelChangeButtonClick()
      {
        this.m_owner.OnHeroInfoPanelChangeButtonClick();
      }

      public void OnSetCharPanelConfirmButtonClick()
      {
        this.m_owner.OnSetCharPanelConfirmButtonClick();
      }

      public void OnHeroSelectPanelConfirmButtonClick()
      {
        this.m_owner.OnHeroSelectPanelConfirmButtonClick();
      }

      public void Update()
      {
        this.m_owner.Update();
      }
    }
  }
}
