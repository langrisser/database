﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogOptionUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class DialogOptionUIController : UIControllerBase
  {
    [AutoBind("./Choice", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./Choice/1", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_choiceButton1;
    [AutoBind("./Choice/2", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_choiceButton2;
    [AutoBind("./Choice/3", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_choiceButton3;
    [AutoBind("./Choice/1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_choiceText1;
    [AutoBind("./Choice/2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_choiceText2;
    [AutoBind("./Choice/3/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_choiceText3;
    [AutoBind("./Time/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./Time", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGo;
    private ConfigDataDialogInfo m_dialogInfo;

    private DialogOptionUIController()
    {
    }

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChoicesData(ConfigDataDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetTimeText(string timeTxt)
    {
      this.m_timeText.text = timeTxt;
    }

    public event Action<int, int> EventOnChoiceClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
