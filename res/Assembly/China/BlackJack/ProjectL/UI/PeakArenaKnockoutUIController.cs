﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaKnockoutUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PeakArenaKnockoutUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scheduleButton;
    [AutoBind("./SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectGroupButton;
    [AutoBind("./SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_selectPanelState;
    [AutoBind("./SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectGroupText;
    [AutoBind("./DescButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descButton;
    [AutoBind("./NextMatchTime", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nextMatchTimeText;
    [AutoBind("./MatchScrollView/Viewport/Content/MatchFlow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_knockoutPlanPanel;
    [AutoBind("./SortTypes/ButtonGroup/FinalButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_finalButton;
    [AutoBind("./SortTypes/ButtonGroup/FinalButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_finalButtonState;
    [AutoBind("./SortTypes/ButtonGroup/GridLayout", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_buttonGroup;
    [AutoBind("./SortTypes/BgButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_groupSelectBgButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private PeakArenaUITask m_peakArenaUITask;
    private PeakArenaKnockoutPlanUIController m_peakArenaKnockoutPlanUIController;
    private List<Button> m_selectGroupButtonList;
    private int m_lastSelectGroupId;
    private int FinalGroupID;
    private int GroupStateFinalRound;
    private float m_lastUpdateTime;
    private DateTime m_lastUpdateServerTime;
    private long m_peakArenaSeasonInfoVersion;
    [DoNotToLua]
    private PeakArenaKnockoutUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OpenInt32_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_HasSelectedGroup_hotfix;
    private LuaFunction m_SetGroupInt32_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_UpdateViewNextMatchTime_hotfix;
    private LuaFunction m_UpdateViewFinalButtonState_hotfix;
    private LuaFunction m_IsFinalMatchActive_hotfix;
    private LuaFunction m_OnFlush_hotfix;
    private LuaFunction m_OnScheduleClick_hotfix;
    private LuaFunction m_OnDescClick_hotfix;
    private LuaFunction m_OnSelectGroupClick_hotfix;
    private LuaFunction m_OnGroupClickInt32_hotfix;
    private LuaFunction m_OnGroupSelectBGClick_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaKnockoutUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(int selectGroupId = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasSelectedGroup()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroup(int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewNextMatchTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewFinalButtonState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFinalMatchActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScheduleClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectGroupClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGroupClick(int selectGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGroupSelectBGClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public PeakArenaKnockoutUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaKnockoutUIController m_owner;

      public LuaExportHelper(PeakArenaKnockoutUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_uiState
      {
        get
        {
          return this.m_owner.m_uiState;
        }
        set
        {
          this.m_owner.m_uiState = value;
        }
      }

      public Button m_scheduleButton
      {
        get
        {
          return this.m_owner.m_scheduleButton;
        }
        set
        {
          this.m_owner.m_scheduleButton = value;
        }
      }

      public Button m_selectGroupButton
      {
        get
        {
          return this.m_owner.m_selectGroupButton;
        }
        set
        {
          this.m_owner.m_selectGroupButton = value;
        }
      }

      public CommonUIStateController m_selectPanelState
      {
        get
        {
          return this.m_owner.m_selectPanelState;
        }
        set
        {
          this.m_owner.m_selectPanelState = value;
        }
      }

      public Text m_selectGroupText
      {
        get
        {
          return this.m_owner.m_selectGroupText;
        }
        set
        {
          this.m_owner.m_selectGroupText = value;
        }
      }

      public Button m_descButton
      {
        get
        {
          return this.m_owner.m_descButton;
        }
        set
        {
          this.m_owner.m_descButton = value;
        }
      }

      public Text m_nextMatchTimeText
      {
        get
        {
          return this.m_owner.m_nextMatchTimeText;
        }
        set
        {
          this.m_owner.m_nextMatchTimeText = value;
        }
      }

      public GameObject m_knockoutPlanPanel
      {
        get
        {
          return this.m_owner.m_knockoutPlanPanel;
        }
        set
        {
          this.m_owner.m_knockoutPlanPanel = value;
        }
      }

      public Button m_finalButton
      {
        get
        {
          return this.m_owner.m_finalButton;
        }
        set
        {
          this.m_owner.m_finalButton = value;
        }
      }

      public CommonUIStateController m_finalButtonState
      {
        get
        {
          return this.m_owner.m_finalButtonState;
        }
        set
        {
          this.m_owner.m_finalButtonState = value;
        }
      }

      public Transform m_buttonGroup
      {
        get
        {
          return this.m_owner.m_buttonGroup;
        }
        set
        {
          this.m_owner.m_buttonGroup = value;
        }
      }

      public Button m_groupSelectBgButton
      {
        get
        {
          return this.m_owner.m_groupSelectBgButton;
        }
        set
        {
          this.m_owner.m_groupSelectBgButton = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public PeakArenaUITask m_peakArenaUITask
      {
        get
        {
          return this.m_owner.m_peakArenaUITask;
        }
        set
        {
          this.m_owner.m_peakArenaUITask = value;
        }
      }

      public PeakArenaKnockoutPlanUIController m_peakArenaKnockoutPlanUIController
      {
        get
        {
          return this.m_owner.m_peakArenaKnockoutPlanUIController;
        }
        set
        {
          this.m_owner.m_peakArenaKnockoutPlanUIController = value;
        }
      }

      public List<Button> m_selectGroupButtonList
      {
        get
        {
          return this.m_owner.m_selectGroupButtonList;
        }
        set
        {
          this.m_owner.m_selectGroupButtonList = value;
        }
      }

      public int m_lastSelectGroupId
      {
        get
        {
          return this.m_owner.m_lastSelectGroupId;
        }
        set
        {
          this.m_owner.m_lastSelectGroupId = value;
        }
      }

      public int FinalGroupID
      {
        get
        {
          return this.m_owner.FinalGroupID;
        }
        set
        {
          this.m_owner.FinalGroupID = value;
        }
      }

      public int GroupStateFinalRound
      {
        get
        {
          return this.m_owner.GroupStateFinalRound;
        }
        set
        {
          this.m_owner.GroupStateFinalRound = value;
        }
      }

      public float m_lastUpdateTime
      {
        get
        {
          return this.m_owner.m_lastUpdateTime;
        }
        set
        {
          this.m_owner.m_lastUpdateTime = value;
        }
      }

      public DateTime m_lastUpdateServerTime
      {
        get
        {
          return this.m_owner.m_lastUpdateServerTime;
        }
        set
        {
          this.m_owner.m_lastUpdateServerTime = value;
        }
      }

      public long m_peakArenaSeasonInfoVersion
      {
        get
        {
          return this.m_owner.m_peakArenaSeasonInfoVersion;
        }
        set
        {
          this.m_owner.m_peakArenaSeasonInfoVersion = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public bool IsFinalMatchActive()
      {
        return this.m_owner.IsFinalMatchActive();
      }

      public void OnFlush()
      {
        this.m_owner.OnFlush();
      }

      public void OnScheduleClick()
      {
        this.m_owner.OnScheduleClick();
      }

      public void OnDescClick()
      {
        this.m_owner.OnDescClick();
      }

      public void OnSelectGroupClick()
      {
        this.m_owner.OnSelectGroupClick();
      }

      public void OnGroupClick(int selectGroupId)
      {
        this.m_owner.OnGroupClick(selectGroupId);
      }

      public void OnGroupSelectBGClick()
      {
        this.m_owner.OnGroupSelectBGClick();
      }
    }
  }
}
