﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RaffleRewardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using MarchingBytes;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RaffleRewardUIController : UIControllerBase
  {
    protected Dictionary<int, List<RaffleItem>> m_raffleRewardLevelInfoDict;
    protected List<RaffleRewardItemUIController> m_rewardItemCtrlList;
    public Action EventOnCloseButtonClick;
    protected bool m_isShow;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController UIStateCtrl;
    [AutoBind("./PrefabRoot", AutoBindAttribute.InitState.NotInit, false)]
    public Transform PrefabRoot;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public EasyObjectPool EasyPool;
    [AutoBind("./LayoutRoot/RewardScrollView/Viewport/RewardItemLayContent", AutoBindAttribute.InitState.NotInit, false)]
    public Transform RewardItemRoot;
    [AutoBind("./LayoutRoot/CloseBth", AutoBindAttribute.InitState.NotInit, false)]
    public Button CloseButton;
    [AutoBind("./LayoutRoot/RewardScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public ScrollRect RewardScrollRect;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button BgButton;

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleRewardUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitRaffleRewardItemPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRaffleRewardPanel(RafflePool rafflePool, bool refreshAll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRaffleRewardPanel(bool isShow, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsRaffleRewardPanelShow()
    {
      return this.m_isShow;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void GainRaffleRewardLevelInfo(RafflePool rafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AllocRewardItemCtrl(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
