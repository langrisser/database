﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerInfoHeadIconUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class PlayerInfoHeadIconUIController : UIControllerBase
  {
    [AutoBind("./ListPanel/ToggleGroup/HeadFrameToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_headFrameToggle;
    [AutoBind("./ListPanel/ToggleGroup/HeadToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_headPortraitToggle;
    [AutoBind("./ListPanel/HeadListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changeHeadPortraitScrollViewContent;
    [AutoBind("./ListPanel/HeadFrameListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changeHeadFrameScrollViewContent;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_changeIconStateCtrl;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeIconPanelBGButton;
    [AutoBind("./ListPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmChangeIconButton;
    [AutoBind("./PreviewPanel/PlayerHeadImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_previewHeadPortraitImage;
    [AutoBind("./PreviewPanel/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_previewHeadFrameTransform;
    [AutoBind("./PreviewPanel/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_previewLevelText;
    [AutoBind("./PreviewPanel/GetCondition/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_previewFrameUnlockText;
    [AutoBind("./PreviewPanel/TitleText1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_previewPortraitNameText;
    [AutoBind("./PreviewPanel/TitleText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_previewFrameNameText;
    private GameObject m_headPortraitItemPrefab;
    private GameObject m_headFrameItemPrefab;
    private List<Hero> m_heroList;
    private PlayerInfoHeadPortraitItemUIController m_curSelectedHeadPortraitItem;
    private PlayerInfoHeadFrameItemUIController m_curSelectedHeadFrameItem;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerInfoHeadIconUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChangeHeadIconPanel(
      GameObject headPortraitItemPrefab,
      GameObject headFrameItemPrefab)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeHeadIconTogglePanel(PlayerInfoHeadIconPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnChangeIconPanelCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChangHeadIconPreview()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmChangeIconButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private PlayerInfoHeadIconPanelType GetCurPanelType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeadPortraitToggleValueChanged(bool isOn, UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeadFrameToggleValueChanged(bool isOn, UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeadFrameValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeadPortraitValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckHeadFrameValid()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> EventOnChangeHeadPortraitAndHeadFrame
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
