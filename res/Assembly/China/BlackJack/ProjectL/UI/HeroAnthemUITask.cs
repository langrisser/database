﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroAnthemUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroAnthemUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private HeroAnthemBackground3DLayerCtrl m_heroAnthemBackground3DLayerCtrl;
    private HeroAnthemUIController m_heroAnthemUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private HeroAnthemUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_PostUpdateView_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_CollectHeroAnthemLevelAcievementsAssetsList`1_hotfix;
    private LuaFunction m_InitLayerStateOnLoadAllResCompleted_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_InitHeroAnthemBackground3DLayerCtrl_hotfix;
    private LuaFunction m_InitHeroAnthemUIController_hotfix;
    private LuaFunction m_UninitHeroAnthemUIController_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_HeroAnthemUIController_OnReturn_hotfix;
    private LuaFunction m_HeroAnthemUIController_OnShowHelp_hotfix;
    private LuaFunction m_HeroAnthemUIController_OnGotoHeroAnthemChapterConfigDataHeroAnthemInfo_hotfix;
    private LuaFunction m_HeroAnthemLevelsUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_HeroAnthemUIController_OnRankButtonClick_hotfix;
    private LuaFunction m_GetHeadIconAssets_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAnthemUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeroAnthemLevelAcievementsAssets(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitHeroAnthemBackground3DLayerCtrl()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitHeroAnthemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitHeroAnthemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemUIController_OnGotoHeroAnthemChapter(ConfigDataHeroAnthemInfo anthemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemLevelsUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemUIController_OnRankButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<string> GetHeadIconAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroAnthemUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      base.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroAnthemUITask m_owner;

      public LuaExportHelper(HeroAnthemUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public HeroAnthemBackground3DLayerCtrl m_heroAnthemBackground3DLayerCtrl
      {
        get
        {
          return this.m_owner.m_heroAnthemBackground3DLayerCtrl;
        }
        set
        {
          this.m_owner.m_heroAnthemBackground3DLayerCtrl = value;
        }
      }

      public HeroAnthemUIController m_heroAnthemUIController
      {
        get
        {
          return this.m_owner.m_heroAnthemUIController;
        }
        set
        {
          this.m_owner.m_heroAnthemUIController = value;
        }
      }

      public PlayerResourceUIController m_playerResourceUIController
      {
        get
        {
          return this.m_owner.m_playerResourceUIController;
        }
        set
        {
          this.m_owner.m_playerResourceUIController = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void PostUpdateView()
      {
        this.m_owner.PostUpdateView();
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void CollectHeroAnthemLevelAcievementsAssets(List<Goods> goods)
      {
        this.m_owner.CollectHeroAnthemLevelAcievementsAssets(goods);
      }

      public void InitLayerStateOnLoadAllResCompleted()
      {
        this.m_owner.InitLayerStateOnLoadAllResCompleted();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void InitHeroAnthemBackground3DLayerCtrl()
      {
        this.m_owner.InitHeroAnthemBackground3DLayerCtrl();
      }

      public void InitHeroAnthemUIController()
      {
        this.m_owner.InitHeroAnthemUIController();
      }

      public void UninitHeroAnthemUIController()
      {
        this.m_owner.UninitHeroAnthemUIController();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void HeroAnthemUIController_OnReturn()
      {
        this.m_owner.HeroAnthemUIController_OnReturn();
      }

      public void HeroAnthemUIController_OnShowHelp()
      {
        this.m_owner.HeroAnthemUIController_OnShowHelp();
      }

      public void HeroAnthemUIController_OnGotoHeroAnthemChapter(ConfigDataHeroAnthemInfo anthemInfo)
      {
        this.m_owner.HeroAnthemUIController_OnGotoHeroAnthemChapter(anthemInfo);
      }

      public void HeroAnthemLevelsUITask_OnLoadAllResCompleted()
      {
        this.m_owner.HeroAnthemLevelsUITask_OnLoadAllResCompleted();
      }

      public void HeroAnthemUIController_OnRankButtonClick()
      {
        this.m_owner.HeroAnthemUIController_OnRankButtonClick();
      }

      public List<string> GetHeadIconAssets()
      {
        return this.m_owner.GetHeadIconAssets();
      }
    }
  }
}
