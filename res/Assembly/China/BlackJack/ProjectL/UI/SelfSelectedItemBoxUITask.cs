﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelfSelectedItemBoxUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SelfSelectedItemBoxUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private SelfSelectedItemBoxUIController m_selfSelectedItemBoxUIConroller;
    private StoreId m_storeId;
    private int m_fixedStoreItemId;
    private int m_itemId;
    private SelfSelectedItemBoxUITask.SelfSelectedItemBoxMode m_mode;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelfSelectedItemBoxUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static SelfSelectedItemBoxUITask StartBuyMode(
      StoreId storeId,
      int fixedStoreItemId,
      SelfSelectedItemBoxUITask.SelfSelectedItemBoxMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static SelfSelectedItemBoxUITask StartUseMode(int itemID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void UpdateView()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadRes(List<string> assetList, Action onComplete)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnCloseClick()
    {
      this.Close();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyClick(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseClick(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<List<Goods>> EventOnBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<Goods>> EventOnUseClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCloseClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    public enum SelfSelectedItemBoxMode
    {
      None,
      Use,
      Buy,
    }
  }
}
