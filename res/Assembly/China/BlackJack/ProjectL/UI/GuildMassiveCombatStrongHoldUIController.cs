﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildMassiveCombatStrongHoldUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GuildMassiveCombatStrongHoldUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_buttonEx;
    [AutoBind("./SuppressGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_suppressValueText;
    [AutoBind("./CampImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_campImage;
    [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_icon;
    [AutoBind("./DifficultLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    public GuildMassiveCombatStronghold m_strongHold;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitGuildMassiveCombatStrongHoldInfo(GuildMassiveCombatStronghold strongHold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuppressText()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GuildMassiveCombatStrongHoldUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
