﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MailItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class MailItemUIController : UIControllerBase
  {
    public Mail CurrentMailInfo;
    private ProjectLPlayerContext m_playerCtx;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public Button MailItemButton;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_uiStateCtrl;
    [AutoBind("./MailTitleText", AutoBindAttribute.InitState.NotInit, false)]
    public Text MailTitleText;
    [AutoBind("./SendTimeText", AutoBindAttribute.InitState.NotInit, false)]
    public Text SendTimeText;
    [AutoBind("./SelectedImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image SelectedImage;

    [MethodImpl((MethodImplOptions) 32768)]
    public MailItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateMailInfo(Mail mailInfo, bool isSelected = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetStateIconSpriteByMailState(Mail mailInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMailItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<MailItemUIController> EventOnMailItemButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
