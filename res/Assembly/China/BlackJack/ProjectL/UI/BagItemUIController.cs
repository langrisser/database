﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BagItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BagItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_stateCtrl;
    [AutoBind("./CountText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_countText;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_iconImage;
    [AutoBind("./SelectFrameImg", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_selectFrameImage;
    [AutoBind("./FragmentSelectFrameImg", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_fragmentSelectFrameImg;
    [AutoBind("./SelectFrameImg2", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_checkFrameImage;
    [AutoBind("./BgImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_bgImage;
    [AutoBind("./SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_ssrEffect;
    [AutoBind("./LockImage", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_lockImage;
    [AutoBind("./Lv/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_lvValueText;
    [AutoBind("./EquipingTag", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_equipingTag;
    [AutoBind("./EquipingTag/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_equipingTagHeadIcon;
    [AutoBind("./StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_starGroup;
    [AutoBind("./GreyMaskItem", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_greyMaskItem;
    [AutoBind("./GreyMaskPiece", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_greyMaskPiece;
    [AutoBind("./EnchantmentIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_enchantmentIcon;
    public ConfigDataItemInfo m_itemInfo;
    public ConfigDataJobMaterialInfo m_jobMaterialInfo;
    public ConfigDataEquipmentInfo m_equipmentInfo;
    public ConfigDataEnchantStoneInfo m_enchantStoneInfo;
    public ConfigDataRefineryStoneInfo m_refineryStoneInfo;
    public ScrollItemBaseUIController ScrollItemBaseUICtrl;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBagItemInfo(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetItemInfoByType(GoodsType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetItemStateByType(GoodsType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFrame(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ShowCheckFrame(bool isShow)
    {
      this.m_checkFrameImage.SetActive(isShow);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowGreyMask(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Init()
    {
      this.ScrollItemBaseUICtrl.Init((UIControllerBase) this, true);
    }

    public void RegisterItemClickEvent(Action<UIControllerBase> action)
    {
      this.ScrollItemBaseUICtrl.EventOnUIItemClick = action;
    }

    public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
    {
      this.ScrollItemBaseUICtrl.EventOnUIItemNeedFill = action;
    }

    public BagItemBase BagItem { private set; get; }
  }
}
