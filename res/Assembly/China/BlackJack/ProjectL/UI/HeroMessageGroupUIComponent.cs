﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroMessageGroupUIComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroMessageGroupUIComponent
  {
    private GameObject m_ssrGameObject;
    private GameObject m_srGameObject;
    private GameObject m_rGameObject;
    private Text m_heroNameText;
    private Text m_heroEnglishNameText;
    private Text m_cvNameText;
    private GameObject m_root;
    [DoNotToLua]
    private HeroMessageGroupUIComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitGameObject_hotfix;
    private LuaFunction m_SetEnglishNameTextText_hotfix;
    private LuaFunction m_RefreshConfigDataHeroInfo_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroMessageGroupUIComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEnglishNameText(Text heroEnglishNameText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public HeroMessageGroupUIComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroMessageGroupUIComponent m_owner;

      public LuaExportHelper(HeroMessageGroupUIComponent owner)
      {
        this.m_owner = owner;
      }

      public GameObject m_ssrGameObject
      {
        get
        {
          return this.m_owner.m_ssrGameObject;
        }
        set
        {
          this.m_owner.m_ssrGameObject = value;
        }
      }

      public GameObject m_srGameObject
      {
        get
        {
          return this.m_owner.m_srGameObject;
        }
        set
        {
          this.m_owner.m_srGameObject = value;
        }
      }

      public GameObject m_rGameObject
      {
        get
        {
          return this.m_owner.m_rGameObject;
        }
        set
        {
          this.m_owner.m_rGameObject = value;
        }
      }

      public Text m_heroNameText
      {
        get
        {
          return this.m_owner.m_heroNameText;
        }
        set
        {
          this.m_owner.m_heroNameText = value;
        }
      }

      public Text m_heroEnglishNameText
      {
        get
        {
          return this.m_owner.m_heroEnglishNameText;
        }
        set
        {
          this.m_owner.m_heroEnglishNameText = value;
        }
      }

      public Text m_cvNameText
      {
        get
        {
          return this.m_owner.m_cvNameText;
        }
        set
        {
          this.m_owner.m_cvNameText = value;
        }
      }

      public GameObject m_root
      {
        get
        {
          return this.m_owner.m_root;
        }
        set
        {
          this.m_owner.m_root = value;
        }
      }
    }
  }
}
