﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PVPBattlePrepareUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class PVPBattlePrepareUIController : UIControllerBase
  {
    [AutoBind("./Panel/FirstOrAfter", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_orderUIStateController;
    [AutoBind("./Panel/ShowFirst", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_orderFirstGameObject;
    [AutoBind("./Panel/ShowAfter", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_orderAfterGameObject;
    [AutoBind("./Panel/PrepareState", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_prepareStateGameObject;
    [AutoBind("./Panel/PrepareState/TimeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prepareMyTimeUIStateController;
    [AutoBind("./Panel/PrepareState/TimeGroup/Detail/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_prepareMyTimeText;
    [AutoBind("./Panel/PrepareState/DetailGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prepareMyStateUIStateController;
    [AutoBind("./Panel/PrepareState/DetailGroup/TextAndFigure", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prepareMyStateTextUIStateController;
    [AutoBind("./Panel/PrepareState/DetailGroup/TextAndFigure/EffecctImage/Figure1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prepareMyFigure1UIStateController;
    [AutoBind("./Panel/PrepareState/DetailGroup/TextAndFigure/EffecctImage/Figure2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prepareMyFigure2UIStateController;
    [AutoBind("./Panel/PrepareState/DetailGroup/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_prepareConfirmButton;
    [AutoBind("./Panel/PrepareState/DetailGroup/Button", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prepareConfirmButtonUIStateController;
    [AutoBind("./Panel/OpponentPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prepareOpponentUIStateController;
    [AutoBind("./Panel/OpponentPanel/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_prepareOpponentTimeText;

    private PVPBattlePrepareUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~PVPBattlePrepareUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOrder(bool isFirst, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ShowPrepareState(bool show)
    {
      this.m_prepareStateGameObject.SetActive(show);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPickMyHero(int heroCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitOpponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowProtectIndicator(int side)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBanIndicator(int side)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPrepareCompleteCountdown(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPickHeroCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetProtectHeroCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBanHeroCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetFigureState(int idx, string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPrepareConfirmButtonState(bool isEenable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPrepareCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPrepareConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPrepareConfirm
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
