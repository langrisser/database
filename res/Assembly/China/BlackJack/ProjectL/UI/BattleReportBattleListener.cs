﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleReportBattleListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleReportBattleListener : NullBattleListener
  {
    private BattleBase m_battle;
    private List<RegretStep> m_regretSteps;
    [DoNotToLua]
    private BattleReportBattleListener.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitBattleBase_hotfix;
    private LuaFunction m_GetRegretSteps_hotfix;
    private LuaFunction m_OnBattleActorActiveBattleActorBoolean_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReportBattleListener()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(BattleBase battle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<RegretStep> GetRegretSteps()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorActive(BattleActor a, bool newStep)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleReportBattleListener.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_OnBattleStart()
    {
      this.OnBattleStart();
    }

    private void __callBase_OnBattleNextTurn(int turn)
    {
      this.OnBattleNextTurn(turn);
    }

    private void __callBase_OnBattleNextTeam(int team, bool isNpc)
    {
      this.OnBattleNextTeam(team, isNpc);
    }

    private void __callBase_OnBattleNextPlayer(int prevPlayerIndex, int playerIndex)
    {
      this.OnBattleNextPlayer(prevPlayerIndex, playerIndex);
    }

    private void __callBase_OnBattleNextActor(BattleActor actor)
    {
      this.OnBattleNextActor(actor);
    }

    private void __callBase_OnBattleActorCreate(BattleActor a, bool visible)
    {
      this.OnBattleActorCreate(a, visible);
    }

    private void __callBase_OnBattleActorCreateEnd(BattleActor a)
    {
      this.OnBattleActorCreateEnd(a);
    }

    private void __callBase_OnBattleActorActive(BattleActor a, bool newStep)
    {
      base.OnBattleActorActive(a, newStep);
    }

    private void __callBase_OnBattleActorActionBegin(BattleActor a)
    {
      this.OnBattleActorActionBegin(a);
    }

    private void __callBase_OnBattleActorActionEnd(BattleActor a)
    {
      this.OnBattleActorActionEnd(a);
    }

    private void __callBase_OnBattleActorMove(BattleActor a, GridPosition p, int dir)
    {
      this.OnBattleActorMove(a, p, dir);
    }

    private void __callBase_OnBattleActorMoveEnd(BattleActor a)
    {
      this.OnBattleActorMoveEnd(a);
    }

    private void __callBase_OnBattleActorPerformMove(
      BattleActor a,
      GridPosition p,
      int dir,
      bool cameraFollow)
    {
      this.OnBattleActorPerformMove(a, p, dir, cameraFollow);
    }

    private void __callBase_OnBattleActorPunchMove(
      BattleActor a,
      string fxName,
      bool isDragExchange)
    {
      this.OnBattleActorPunchMove(a, fxName, isDragExchange);
    }

    private void __callBase_OnBattleActorExchangeMove(
      BattleActor a,
      BattleActor b,
      int moveType,
      string fxName)
    {
      this.OnBattleActorExchangeMove(a, b, moveType, fxName);
    }

    private void __callBase_OnBattleActorSetDir(BattleActor a, int dir)
    {
      this.OnBattleActorSetDir(a, dir);
    }

    private void __callBase_OnBattleActorPlayFx(BattleActor a, string fxName, int attachMode)
    {
      this.OnBattleActorPlayFx(a, fxName, attachMode);
    }

    private void __callBase_OnBattleActorPlayAnimation(
      BattleActor a,
      string animationName,
      int animationTime)
    {
      this.OnBattleActorPlayAnimation(a, animationName, animationTime);
    }

    private void __callBase_OnBattleActorChangeIdleAnimation(
      BattleActor a,
      string idleAnimationName)
    {
      this.OnBattleActorChangeIdleAnimation(a, idleAnimationName);
    }

    private void __callBase_OnBattleActorSkill(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition p,
      List<BattleActor> targets,
      List<BattleActor> combineActor)
    {
      this.OnBattleActorSkill(a, skillInfo, p, targets, combineActor);
    }

    private void __callBase_OnBattleActorSkillHitBegin(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      bool isRebound)
    {
      this.OnBattleActorSkillHitBegin(a, skillInfo, isRebound);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callBase_OnBattleActorSkillHit(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType,
      bool isRebound)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_OnBattleActorSkillHitEnd(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      bool isRebound)
    {
      this.OnBattleActorSkillHitEnd(a, skillInfo, isRebound);
    }

    private void __callBase_OnBattleActorAttachBuff(BattleActor a, BuffState buffState)
    {
      this.OnBattleActorAttachBuff(a, buffState);
    }

    private void __callBase_OnBattleActorDetachBuff(BattleActor a, BuffState buffState)
    {
      this.OnBattleActorDetachBuff(a, buffState);
    }

    private void __callBase_OnBattleActorImmune(BattleActor a)
    {
      this.OnBattleActorImmune(a);
    }

    private void __callBase_OnBattleActorPassiveSkill(
      BattleActor a,
      BattleActor target,
      BuffState sourceBuffState)
    {
      this.OnBattleActorPassiveSkill(a, target, sourceBuffState);
    }

    private void __callBase_OnBattleActorBuffHit(
      BattleActor a,
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      this.OnBattleActorBuffHit(a, buffState, heroHpModify, soldierHpModify, damageNumberType);
    }

    private void __callBase_OnBattleActorTerrainHit(
      BattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      this.OnBattleActorTerrainHit(a, heroHpModify, soldierHpModify, damageNumberType);
    }

    private void __callBase_OnBattleActorTeleport(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition p)
    {
      this.OnBattleActorTeleport(a, skillInfo, p);
    }

    private void __callBase_OnBattleActorSummon(BattleActor a, ConfigDataSkillInfo skillInfo)
    {
      this.OnBattleActorSummon(a, skillInfo);
    }

    private void __callBase_OnBattleActorDie(BattleActor a, bool isAfterCombat)
    {
      this.OnBattleActorDie(a, isAfterCombat);
    }

    private void __callBase_OnBattleActorAppear(BattleActor a, int effectType, string fxName)
    {
      this.OnBattleActorAppear(a, effectType, fxName);
    }

    private void __callBase_OnBattleActorDisappear(BattleActor a, int effectType, string fxName)
    {
      this.OnBattleActorDisappear(a, effectType, fxName);
    }

    private void __callBase_OnBattleActorChangeTeam(BattleActor a)
    {
      this.OnBattleActorChangeTeam(a);
    }

    private void __callBase_OnBattleActorChangeArmy(BattleActor a)
    {
      this.OnBattleActorChangeArmy(a);
    }

    private void __callBase_OnBattleActorReplace(BattleActor a0, BattleActor a1, string fxName)
    {
      this.OnBattleActorReplace(a0, a1, fxName);
    }

    private void __callBase_OnBattleActorCameraFocus(BattleActor a)
    {
      this.OnBattleActorCameraFocus(a);
    }

    private void __callBase_OnBattleActorGainBattleTreasure(
      BattleActor a,
      ConfigDataBattleTreasureInfo treasureInfo)
    {
      this.OnBattleActorGainBattleTreasure(a, treasureInfo);
    }

    private void __callBase_OnStartGuard(BattleActor a, BattleActor target)
    {
      this.OnStartGuard(a, target);
    }

    private void __callBase_OnStopGuard(BattleActor a, BattleActor target)
    {
      this.OnStopGuard(a, target);
    }

    private void __callBase_OnBeforeStartCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      this.OnBeforeStartCombat(a, b, attackerSkillInfo);
    }

    private void __callBase_OnCancelCombat()
    {
      this.OnCancelCombat();
    }

    private void __callBase_OnStartCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      this.OnStartCombat(a, b, attackerSkillInfo);
    }

    private void __callBase_OnPreStopCombat()
    {
      this.OnPreStopCombat();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callBase_OnStopCombat(
      int teamAHeroTotalDamage,
      int teamASoldierTotalDamage,
      bool teamACriticalAttack,
      int teamBHeroTotalDamage,
      int teamBSoldierTotalDamage,
      bool teamBCriticalAttack)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_OnCombatActorSkill(CombatActor a, ConfigDataSkillInfo skillInfo)
    {
      this.OnCombatActorSkill(a, skillInfo);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callBase_OnCombatActorHit(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      int totalDamage,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_OnCombatActorDie(CombatActor a)
    {
      this.OnCombatActorDie(a);
    }

    private void __callBase_OnStartSkillCutscene(
      ConfigDataSkillInfo skillInfo,
      ConfigDataCutsceneInfo cutsceneInfo,
      ConfigDataCutsceneInfo cutsceneInfo2,
      int team)
    {
      this.OnStartSkillCutscene(skillInfo, cutsceneInfo, cutsceneInfo2, team);
    }

    private void __callBase_OnStartPassiveSkillCutscene(BuffState sourceBuffState, int team)
    {
      this.OnStartPassiveSkillCutscene(sourceBuffState, team);
    }

    private void __callBase_OnStopSkillCutscene()
    {
      this.OnStopSkillCutscene();
    }

    private void __callBase_OnStartBattleDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
      this.OnStartBattleDialog(dialogInfo);
    }

    private void __callBase_OnStartBattlePerform(ConfigDataBattlePerformInfo performInfo)
    {
      this.OnStartBattlePerform(performInfo);
    }

    private void __callBase_OnStopBattlePerform()
    {
      this.OnStopBattlePerform();
    }

    private void __callBase_OnChangeMapTerrain(
      List<GridPosition> positions,
      ConfigDataTerrainInfo terrainInfo)
    {
      this.OnChangeMapTerrain(positions, terrainInfo);
    }

    private void __callBase_OnChangeMapTerrainEffect(
      List<GridPosition> positions,
      ConfigDataTerrainEffectInfo terrainEffectInfo)
    {
      this.OnChangeMapTerrainEffect(positions, terrainEffectInfo);
    }

    private void __callBase_OnCameraFocus(GridPosition p)
    {
      this.OnCameraFocus(p);
    }

    private void __callBase_OnPlayMusic(string musicName)
    {
      this.OnPlayMusic(musicName);
    }

    private void __callBase_OnPlaySound(string soundName)
    {
      this.OnPlaySound(soundName);
    }

    private void __callBase_OnPlayFx(string fxName, GridPosition p)
    {
      this.OnPlayFx(fxName, p);
    }

    private void __callBase_OnWaitTime(int timeInMs)
    {
      this.OnWaitTime(timeInMs);
    }

    private void __callBase_OnBattleTreasureCreate(
      ConfigDataBattleTreasureInfo treasureInfo,
      bool isOpened)
    {
      this.OnBattleTreasureCreate(treasureInfo, isOpened);
    }

    private IBattleGraphic __callBase_CreateCombatGraphic(
      string assetName,
      float scale)
    {
      return this.CreateCombatGraphic(assetName, scale);
    }

    private void __callBase_DestroyCombatGraphic(IBattleGraphic model)
    {
      this.DestroyCombatGraphic(model);
    }

    private IBattleGraphic __callBase_PlayCombatFx(string assetName, float scale)
    {
      return this.PlayCombatFx(assetName, scale);
    }

    private void __callBase_PlaySound(string name)
    {
      this.PlaySound(name);
    }

    private void __callBase_DrawLine(Vector2i p0, Vector2i p1, Colori color)
    {
      this.DrawLine(p0, p1, color);
    }

    private void __callBase_DrawLine(Vector2i p0, Fix64 z0, Vector2i p1, Fix64 z1, Colori color)
    {
      this.DrawLine(p0, z0, p1, z1, color);
    }

    private void __callBase_LogBattleStart()
    {
      this.LogBattleStart();
    }

    private void __callBase_LogBattleStop(bool isWin)
    {
      this.LogBattleStop(isWin);
    }

    private void __callBase_LogBattleTeam(BattleTeam team0, BattleTeam team1)
    {
      this.LogBattleTeam(team0, team1);
    }

    private void __callBase_LogActorMove(
      BattleActor actor,
      GridPosition fromPos,
      GridPosition toPos)
    {
      this.LogActorMove(actor, fromPos, toPos);
    }

    private void __callBase_LogActorStandby(BattleActor actor)
    {
      this.LogActorStandby(actor);
    }

    private void __callBase_LogActorAttack(BattleActor actor, BattleActor targetActor)
    {
      this.LogActorAttack(actor, targetActor);
    }

    private void __callBase_LogActorSkill(
      BattleActor actor,
      ConfigDataSkillInfo skillInfo,
      BattleActor targetActor,
      GridPosition targetPos)
    {
      this.LogActorSkill(actor, skillInfo, targetActor, targetPos);
    }

    private void __callBase_LogActorDie(BattleActor actor, BattleActor killerActor)
    {
      this.LogActorDie(actor, killerActor);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleReportBattleListener m_owner;

      public LuaExportHelper(BattleReportBattleListener owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_OnBattleStart()
      {
        this.m_owner.__callBase_OnBattleStart();
      }

      public void __callBase_OnBattleNextTurn(int turn)
      {
        this.m_owner.__callBase_OnBattleNextTurn(turn);
      }

      public void __callBase_OnBattleNextTeam(int team, bool isNpc)
      {
        this.m_owner.__callBase_OnBattleNextTeam(team, isNpc);
      }

      public void __callBase_OnBattleNextPlayer(int prevPlayerIndex, int playerIndex)
      {
        this.m_owner.__callBase_OnBattleNextPlayer(prevPlayerIndex, playerIndex);
      }

      public void __callBase_OnBattleNextActor(BattleActor actor)
      {
        this.m_owner.__callBase_OnBattleNextActor(actor);
      }

      public void __callBase_OnBattleActorCreate(BattleActor a, bool visible)
      {
        this.m_owner.__callBase_OnBattleActorCreate(a, visible);
      }

      public void __callBase_OnBattleActorCreateEnd(BattleActor a)
      {
        this.m_owner.__callBase_OnBattleActorCreateEnd(a);
      }

      public void __callBase_OnBattleActorActive(BattleActor a, bool newStep)
      {
        this.m_owner.__callBase_OnBattleActorActive(a, newStep);
      }

      public void __callBase_OnBattleActorActionBegin(BattleActor a)
      {
        this.m_owner.__callBase_OnBattleActorActionBegin(a);
      }

      public void __callBase_OnBattleActorActionEnd(BattleActor a)
      {
        this.m_owner.__callBase_OnBattleActorActionEnd(a);
      }

      public void __callBase_OnBattleActorMove(BattleActor a, GridPosition p, int dir)
      {
        this.m_owner.__callBase_OnBattleActorMove(a, p, dir);
      }

      public void __callBase_OnBattleActorMoveEnd(BattleActor a)
      {
        this.m_owner.__callBase_OnBattleActorMoveEnd(a);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OnBattleActorPerformMove(
        BattleActor a,
        GridPosition p,
        int dir,
        bool cameraFollow)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_OnBattleActorPunchMove(
        BattleActor a,
        string fxName,
        bool isDragExchange)
      {
        this.m_owner.__callBase_OnBattleActorPunchMove(a, fxName, isDragExchange);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OnBattleActorExchangeMove(
        BattleActor a,
        BattleActor b,
        int moveType,
        string fxName)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_OnBattleActorSetDir(BattleActor a, int dir)
      {
        this.m_owner.__callBase_OnBattleActorSetDir(a, dir);
      }

      public void __callBase_OnBattleActorPlayFx(BattleActor a, string fxName, int attachMode)
      {
        this.m_owner.__callBase_OnBattleActorPlayFx(a, fxName, attachMode);
      }

      public void __callBase_OnBattleActorPlayAnimation(
        BattleActor a,
        string animationName,
        int animationTime)
      {
        this.m_owner.__callBase_OnBattleActorPlayAnimation(a, animationName, animationTime);
      }

      public void __callBase_OnBattleActorChangeIdleAnimation(
        BattleActor a,
        string idleAnimationName)
      {
        this.m_owner.__callBase_OnBattleActorChangeIdleAnimation(a, idleAnimationName);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OnBattleActorSkill(
        BattleActor a,
        ConfigDataSkillInfo skillInfo,
        GridPosition p,
        List<BattleActor> targets,
        List<BattleActor> combineActor)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_OnBattleActorSkillHitBegin(
        BattleActor a,
        ConfigDataSkillInfo skillInfo,
        bool isRebound)
      {
        this.m_owner.__callBase_OnBattleActorSkillHitBegin(a, skillInfo, isRebound);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OnBattleActorSkillHit(
        BattleActor a,
        ConfigDataSkillInfo skillInfo,
        int heroHpModify,
        int soldierHpModify,
        DamageNumberType damageNumberType,
        bool isRebound)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_OnBattleActorSkillHitEnd(
        BattleActor a,
        ConfigDataSkillInfo skillInfo,
        bool isRebound)
      {
        this.m_owner.__callBase_OnBattleActorSkillHitEnd(a, skillInfo, isRebound);
      }

      public void __callBase_OnBattleActorAttachBuff(BattleActor a, BuffState buffState)
      {
        this.m_owner.__callBase_OnBattleActorAttachBuff(a, buffState);
      }

      public void __callBase_OnBattleActorDetachBuff(BattleActor a, BuffState buffState)
      {
        this.m_owner.__callBase_OnBattleActorDetachBuff(a, buffState);
      }

      public void __callBase_OnBattleActorImmune(BattleActor a)
      {
        this.m_owner.__callBase_OnBattleActorImmune(a);
      }

      public void __callBase_OnBattleActorPassiveSkill(
        BattleActor a,
        BattleActor target,
        BuffState sourceBuffState)
      {
        this.m_owner.__callBase_OnBattleActorPassiveSkill(a, target, sourceBuffState);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OnBattleActorBuffHit(
        BattleActor a,
        BuffState buffState,
        int heroHpModify,
        int soldierHpModify,
        DamageNumberType damageNumberType)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OnBattleActorTerrainHit(
        BattleActor a,
        int heroHpModify,
        int soldierHpModify,
        DamageNumberType damageNumberType)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_OnBattleActorTeleport(
        BattleActor a,
        ConfigDataSkillInfo skillInfo,
        GridPosition p)
      {
        this.m_owner.__callBase_OnBattleActorTeleport(a, skillInfo, p);
      }

      public void __callBase_OnBattleActorSummon(BattleActor a, ConfigDataSkillInfo skillInfo)
      {
        this.m_owner.__callBase_OnBattleActorSummon(a, skillInfo);
      }

      public void __callBase_OnBattleActorDie(BattleActor a, bool isAfterCombat)
      {
        this.m_owner.__callBase_OnBattleActorDie(a, isAfterCombat);
      }

      public void __callBase_OnBattleActorAppear(BattleActor a, int effectType, string fxName)
      {
        this.m_owner.__callBase_OnBattleActorAppear(a, effectType, fxName);
      }

      public void __callBase_OnBattleActorDisappear(BattleActor a, int effectType, string fxName)
      {
        this.m_owner.__callBase_OnBattleActorDisappear(a, effectType, fxName);
      }

      public void __callBase_OnBattleActorChangeTeam(BattleActor a)
      {
        this.m_owner.__callBase_OnBattleActorChangeTeam(a);
      }

      public void __callBase_OnBattleActorChangeArmy(BattleActor a)
      {
        this.m_owner.__callBase_OnBattleActorChangeArmy(a);
      }

      public void __callBase_OnBattleActorReplace(BattleActor a0, BattleActor a1, string fxName)
      {
        this.m_owner.__callBase_OnBattleActorReplace(a0, a1, fxName);
      }

      public void __callBase_OnBattleActorCameraFocus(BattleActor a)
      {
        this.m_owner.__callBase_OnBattleActorCameraFocus(a);
      }

      public void __callBase_OnBattleActorGainBattleTreasure(
        BattleActor a,
        ConfigDataBattleTreasureInfo treasureInfo)
      {
        this.m_owner.__callBase_OnBattleActorGainBattleTreasure(a, treasureInfo);
      }

      public void __callBase_OnStartGuard(BattleActor a, BattleActor target)
      {
        this.m_owner.__callBase_OnStartGuard(a, target);
      }

      public void __callBase_OnStopGuard(BattleActor a, BattleActor target)
      {
        this.m_owner.__callBase_OnStopGuard(a, target);
      }

      public void __callBase_OnBeforeStartCombat(
        BattleActor a,
        BattleActor b,
        ConfigDataSkillInfo attackerSkillInfo)
      {
        this.m_owner.__callBase_OnBeforeStartCombat(a, b, attackerSkillInfo);
      }

      public void __callBase_OnCancelCombat()
      {
        this.m_owner.__callBase_OnCancelCombat();
      }

      public void __callBase_OnStartCombat(
        BattleActor a,
        BattleActor b,
        ConfigDataSkillInfo attackerSkillInfo)
      {
        this.m_owner.__callBase_OnStartCombat(a, b, attackerSkillInfo);
      }

      public void __callBase_OnPreStopCombat()
      {
        this.m_owner.__callBase_OnPreStopCombat();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OnStopCombat(
        int teamAHeroTotalDamage,
        int teamASoldierTotalDamage,
        bool teamACriticalAttack,
        int teamBHeroTotalDamage,
        int teamBSoldierTotalDamage,
        bool teamBCriticalAttack)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_OnCombatActorSkill(CombatActor a, ConfigDataSkillInfo skillInfo)
      {
        this.m_owner.__callBase_OnCombatActorSkill(a, skillInfo);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OnCombatActorHit(
        CombatActor a,
        CombatActor attacker,
        ConfigDataSkillInfo skillInfo,
        int hpModify,
        int totalDamage,
        DamageNumberType damageNumberType)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_OnCombatActorDie(CombatActor a)
      {
        this.m_owner.__callBase_OnCombatActorDie(a);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OnStartSkillCutscene(
        ConfigDataSkillInfo skillInfo,
        ConfigDataCutsceneInfo cutsceneInfo,
        ConfigDataCutsceneInfo cutsceneInfo2,
        int team)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_OnStartPassiveSkillCutscene(BuffState sourceBuffState, int team)
      {
        this.m_owner.__callBase_OnStartPassiveSkillCutscene(sourceBuffState, team);
      }

      public void __callBase_OnStopSkillCutscene()
      {
        this.m_owner.__callBase_OnStopSkillCutscene();
      }

      public void __callBase_OnStartBattleDialog(ConfigDataBattleDialogInfo dialogInfo)
      {
        this.m_owner.__callBase_OnStartBattleDialog(dialogInfo);
      }

      public void __callBase_OnStartBattlePerform(ConfigDataBattlePerformInfo performInfo)
      {
        this.m_owner.__callBase_OnStartBattlePerform(performInfo);
      }

      public void __callBase_OnStopBattlePerform()
      {
        this.m_owner.__callBase_OnStopBattlePerform();
      }

      public void __callBase_OnChangeMapTerrain(
        List<GridPosition> positions,
        ConfigDataTerrainInfo terrainInfo)
      {
        this.m_owner.__callBase_OnChangeMapTerrain(positions, terrainInfo);
      }

      public void __callBase_OnChangeMapTerrainEffect(
        List<GridPosition> positions,
        ConfigDataTerrainEffectInfo terrainEffectInfo)
      {
        this.m_owner.__callBase_OnChangeMapTerrainEffect(positions, terrainEffectInfo);
      }

      public void __callBase_OnCameraFocus(GridPosition p)
      {
        this.m_owner.__callBase_OnCameraFocus(p);
      }

      public void __callBase_OnPlayMusic(string musicName)
      {
        this.m_owner.__callBase_OnPlayMusic(musicName);
      }

      public void __callBase_OnPlaySound(string soundName)
      {
        this.m_owner.__callBase_OnPlaySound(soundName);
      }

      public void __callBase_OnPlayFx(string fxName, GridPosition p)
      {
        this.m_owner.__callBase_OnPlayFx(fxName, p);
      }

      public void __callBase_OnWaitTime(int timeInMs)
      {
        this.m_owner.__callBase_OnWaitTime(timeInMs);
      }

      public void __callBase_OnBattleTreasureCreate(
        ConfigDataBattleTreasureInfo treasureInfo,
        bool isOpened)
      {
        this.m_owner.__callBase_OnBattleTreasureCreate(treasureInfo, isOpened);
      }

      public IBattleGraphic __callBase_CreateCombatGraphic(
        string assetName,
        float scale)
      {
        return this.m_owner.__callBase_CreateCombatGraphic(assetName, scale);
      }

      public void __callBase_DestroyCombatGraphic(IBattleGraphic model)
      {
        this.m_owner.__callBase_DestroyCombatGraphic(model);
      }

      public IBattleGraphic __callBase_PlayCombatFx(string assetName, float scale)
      {
        return this.m_owner.__callBase_PlayCombatFx(assetName, scale);
      }

      public void __callBase_PlaySound(string name)
      {
        this.m_owner.__callBase_PlaySound(name);
      }

      public void __callBase_DrawLine(Vector2i p0, Vector2i p1, Colori color)
      {
        this.m_owner.__callBase_DrawLine(p0, p1, color);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_DrawLine(Vector2i p0, Fix64 z0, Vector2i p1, Fix64 z1, Colori color)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_LogBattleStart()
      {
        this.m_owner.__callBase_LogBattleStart();
      }

      public void __callBase_LogBattleStop(bool isWin)
      {
        this.m_owner.__callBase_LogBattleStop(isWin);
      }

      public void __callBase_LogBattleTeam(BattleTeam team0, BattleTeam team1)
      {
        this.m_owner.__callBase_LogBattleTeam(team0, team1);
      }

      public void __callBase_LogActorMove(
        BattleActor actor,
        GridPosition fromPos,
        GridPosition toPos)
      {
        this.m_owner.__callBase_LogActorMove(actor, fromPos, toPos);
      }

      public void __callBase_LogActorStandby(BattleActor actor)
      {
        this.m_owner.__callBase_LogActorStandby(actor);
      }

      public void __callBase_LogActorAttack(BattleActor actor, BattleActor targetActor)
      {
        this.m_owner.__callBase_LogActorAttack(actor, targetActor);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_LogActorSkill(
        BattleActor actor,
        ConfigDataSkillInfo skillInfo,
        BattleActor targetActor,
        GridPosition targetPos)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_LogActorDie(BattleActor actor, BattleActor killerActor)
      {
        this.m_owner.__callBase_LogActorDie(actor, killerActor);
      }

      public BattleBase m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public List<RegretStep> m_regretSteps
      {
        get
        {
          return this.m_owner.m_regretSteps;
        }
        set
        {
          this.m_owner.m_regretSteps = value;
        }
      }
    }
  }
}
