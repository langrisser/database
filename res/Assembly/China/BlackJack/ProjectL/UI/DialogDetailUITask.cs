﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogDetailUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public sealed class DialogDetailUITask : UITask
  {
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private const string ParamKeyLog = "ParamKeyLog";
    private List<DialogDetailUITask.LogData> m_logDataList;
    private DialogDetailUIController m_dialogDetailUIController;
    private readonly UITaskBase.LayerDesc[] m_layerDescArray;
    private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public DialogDetailUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUITask(
      UIIntent fromIntent,
      List<DialogDetailUITask.LogData> logDataList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnStop()
    {
      base.OnStop();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool IsNeedUpdateDataCache()
    {
      return false;
    }

    protected override void UpdateDataCache()
    {
    }

    protected override bool IsNeedLoadDynamicRes()
    {
      return false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<string> CollectAllDynamicResForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      base.InitLayerStateOnLoadAllResCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutomationInitUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void PostUpdateView()
    {
    }

    private void DialogDetailUIController_Close()
    {
      this.Pause();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromIntentForPrepare(UIIntentCustom intent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    public class LogData
    {
      public DialogDetailUITask.LogData.LogDataType m_dataType;
      public string m_data;

      [MethodImpl((MethodImplOptions) 32768)]
      public LogData(DialogDetailUITask.LogData.LogDataType dataType, string data)
      {
        // ISSUE: unable to decompile the method.
      }

      public enum LogDataType
      {
        Name,
        Desc,
        Option,
      }
    }
  }
}
