﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailAddExpUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroDetailAddExpUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rootGo;
    [AutoBind("./ReturnBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeAddExpButton;
    [AutoBind("./Detail/ExpItemScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addExpScrollViewContent;
    [AutoBind("./Detail/HeroInfoGroup/Lv/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addExpPanelLvValueText;
    [AutoBind("./Detail/HeroInfoGroup/Lv/ValueText/MaxText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addExpPanelLvValueMaxText;
    [AutoBind("./Detail/HeroInfoGroup/HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addExpPanelHeroNameText;
    [AutoBind("./Detail/LevelUpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addExpPanelLevelUpEffect;
    [AutoBind("./Detail/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_addExpPanelHeroIconImg;
    [AutoBind("./Detail/Exp/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addExpPanelExpValueText;
    [AutoBind("./Detail/Exp/ProgressImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_addExpPanelExpProgressImg;
    [AutoBind("./Detail/U_LevelUp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addExpPanelLevelUp3DEffect;
    [AutoBind("./Detail/NoExpItemTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addExpPanelNoExpItemTip;
    [AutoBind("./Detail/ConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addExpConfirmPanel;
    [AutoBind("./Detail/ConfirmPanel/Tip1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addExpConfirmPanelTip1;
    [AutoBind("./Detail/ConfirmPanel/Tip2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addExpConfirmPanelTip2;
    [AutoBind("./Detail/ConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addExpConfirmPanelConfirmButton;
    [AutoBind("./Detail/ConfirmPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addExpConfirmPanelCancelButton;
    [AutoBind("./Detail/ConfirmPanel/BgButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addExpConfirmPanelBgButton;
    private bool m_isHeroLvUp;
    private bool m_isFirstIn;
    private bool m_canLastClickExpItemUse;
    private Hero m_hero;
    private float m_expProgressBarWidth2;
    private int m_totalUseExpItemCount;
    private int m_tempHeroLevel;
    private int m_tempHeroExp;
    private int m_tempAddExp;
    private int m_heroLastLevel;
    private HeroExpItemUIController m_curExpBagItemCtrl;
    public List<HeroExpItemUIController> m_expCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int m_addImageCoroutineCount;
    [DoNotToLua]
    private HeroDetailAddExpUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_UpdateViewInAddExpStateHero_hotfix;
    private LuaFunction m_UpdateAddExpPanel_hotfix;
    private LuaFunction m_HeroAddExpLvUpEffect_hotfix;
    private LuaFunction m_OnExpItemClickHeroExpItemUIController_hotfix;
    private LuaFunction m_LocalAddExpTick_hotfix;
    private LuaFunction m_AddProgressImageWidthSingleSingleSingle_hotfix;
    private LuaFunction m_OnLocalAddExpFinishedSendReqInt32_hotfix;
    private LuaFunction m_WaitTimeThenDoEventInt32_hotfix;
    private LuaFunction m_CanExpItemUseInt32Int32_hotfix;
    private LuaFunction m_OnAddExpConfirmButtonClick_hotfix;
    private LuaFunction m_OnAddExpCanelButtonClick_hotfix;
    private LuaFunction m_CloseAddExpPanel_hotfix;
    private LuaFunction m_UseFirstExpBagItemForUserGuideHeroExpItemUIController_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnHeroMaxLevelUseExpItemAction_hotfix;
    private LuaFunction m_remove_EventOnHeroMaxLevelUseExpItemAction_hotfix;
    private LuaFunction m_add_EventOnHeroAddExpAction`4_hotfix;
    private LuaFunction m_remove_EventOnHeroAddExpAction`4_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDetailAddExpUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInAddExpState(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateAddExpPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAddExpLvUpEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpItemClick(HeroExpItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LocalAddExpTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator AddProgressImageWidth(
      float totalWidth,
      float DesWidth,
      float intervalTime = 0.1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLocalAddExpFinishedSendReq(int usedCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator WaitTimeThenDoEvent(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanExpItemUse(int heroLv, int herpExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddExpConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddExpCanelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAddExpPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UseFirstExpBagItemForUserGuide(HeroExpItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroMaxLevelUseExpItem
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BagItemBase, int, Action> EventOnHeroAddExp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroDetailAddExpUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHeroMaxLevelUseExpItem()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnHeroMaxLevelUseExpItem()
    {
      this.EventOnHeroMaxLevelUseExpItem = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHeroAddExp(int arg1, BagItemBase arg2, int arg3, Action arg4)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnHeroAddExp(int arg1, BagItemBase arg2, int arg3, Action arg4)
    {
      this.EventOnHeroAddExp = (Action<int, BagItemBase, int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroDetailAddExpUIController m_owner;

      public LuaExportHelper(HeroDetailAddExpUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnHeroMaxLevelUseExpItem()
      {
        this.m_owner.__callDele_EventOnHeroMaxLevelUseExpItem();
      }

      public void __clearDele_EventOnHeroMaxLevelUseExpItem()
      {
        this.m_owner.__clearDele_EventOnHeroMaxLevelUseExpItem();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callDele_EventOnHeroAddExp(int arg1, BagItemBase arg2, int arg3, Action arg4)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __clearDele_EventOnHeroAddExp(int arg1, BagItemBase arg2, int arg3, Action arg4)
      {
        // ISSUE: unable to decompile the method.
      }

      public GameObject m_rootGo
      {
        get
        {
          return this.m_owner.m_rootGo;
        }
        set
        {
          this.m_owner.m_rootGo = value;
        }
      }

      public Button m_closeAddExpButton
      {
        get
        {
          return this.m_owner.m_closeAddExpButton;
        }
        set
        {
          this.m_owner.m_closeAddExpButton = value;
        }
      }

      public GameObject m_addExpScrollViewContent
      {
        get
        {
          return this.m_owner.m_addExpScrollViewContent;
        }
        set
        {
          this.m_owner.m_addExpScrollViewContent = value;
        }
      }

      public Text m_addExpPanelLvValueText
      {
        get
        {
          return this.m_owner.m_addExpPanelLvValueText;
        }
        set
        {
          this.m_owner.m_addExpPanelLvValueText = value;
        }
      }

      public Text m_addExpPanelLvValueMaxText
      {
        get
        {
          return this.m_owner.m_addExpPanelLvValueMaxText;
        }
        set
        {
          this.m_owner.m_addExpPanelLvValueMaxText = value;
        }
      }

      public Text m_addExpPanelHeroNameText
      {
        get
        {
          return this.m_owner.m_addExpPanelHeroNameText;
        }
        set
        {
          this.m_owner.m_addExpPanelHeroNameText = value;
        }
      }

      public GameObject m_addExpPanelLevelUpEffect
      {
        get
        {
          return this.m_owner.m_addExpPanelLevelUpEffect;
        }
        set
        {
          this.m_owner.m_addExpPanelLevelUpEffect = value;
        }
      }

      public Image m_addExpPanelHeroIconImg
      {
        get
        {
          return this.m_owner.m_addExpPanelHeroIconImg;
        }
        set
        {
          this.m_owner.m_addExpPanelHeroIconImg = value;
        }
      }

      public Text m_addExpPanelExpValueText
      {
        get
        {
          return this.m_owner.m_addExpPanelExpValueText;
        }
        set
        {
          this.m_owner.m_addExpPanelExpValueText = value;
        }
      }

      public Image m_addExpPanelExpProgressImg
      {
        get
        {
          return this.m_owner.m_addExpPanelExpProgressImg;
        }
        set
        {
          this.m_owner.m_addExpPanelExpProgressImg = value;
        }
      }

      public GameObject m_addExpPanelLevelUp3DEffect
      {
        get
        {
          return this.m_owner.m_addExpPanelLevelUp3DEffect;
        }
        set
        {
          this.m_owner.m_addExpPanelLevelUp3DEffect = value;
        }
      }

      public GameObject m_addExpPanelNoExpItemTip
      {
        get
        {
          return this.m_owner.m_addExpPanelNoExpItemTip;
        }
        set
        {
          this.m_owner.m_addExpPanelNoExpItemTip = value;
        }
      }

      public GameObject m_addExpConfirmPanel
      {
        get
        {
          return this.m_owner.m_addExpConfirmPanel;
        }
        set
        {
          this.m_owner.m_addExpConfirmPanel = value;
        }
      }

      public Text m_addExpConfirmPanelTip1
      {
        get
        {
          return this.m_owner.m_addExpConfirmPanelTip1;
        }
        set
        {
          this.m_owner.m_addExpConfirmPanelTip1 = value;
        }
      }

      public Text m_addExpConfirmPanelTip2
      {
        get
        {
          return this.m_owner.m_addExpConfirmPanelTip2;
        }
        set
        {
          this.m_owner.m_addExpConfirmPanelTip2 = value;
        }
      }

      public Button m_addExpConfirmPanelConfirmButton
      {
        get
        {
          return this.m_owner.m_addExpConfirmPanelConfirmButton;
        }
        set
        {
          this.m_owner.m_addExpConfirmPanelConfirmButton = value;
        }
      }

      public Button m_addExpConfirmPanelCancelButton
      {
        get
        {
          return this.m_owner.m_addExpConfirmPanelCancelButton;
        }
        set
        {
          this.m_owner.m_addExpConfirmPanelCancelButton = value;
        }
      }

      public Button m_addExpConfirmPanelBgButton
      {
        get
        {
          return this.m_owner.m_addExpConfirmPanelBgButton;
        }
        set
        {
          this.m_owner.m_addExpConfirmPanelBgButton = value;
        }
      }

      public bool m_isHeroLvUp
      {
        get
        {
          return this.m_owner.m_isHeroLvUp;
        }
        set
        {
          this.m_owner.m_isHeroLvUp = value;
        }
      }

      public bool m_isFirstIn
      {
        get
        {
          return this.m_owner.m_isFirstIn;
        }
        set
        {
          this.m_owner.m_isFirstIn = value;
        }
      }

      public bool m_canLastClickExpItemUse
      {
        get
        {
          return this.m_owner.m_canLastClickExpItemUse;
        }
        set
        {
          this.m_owner.m_canLastClickExpItemUse = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public float m_expProgressBarWidth2
      {
        get
        {
          return this.m_owner.m_expProgressBarWidth2;
        }
        set
        {
          this.m_owner.m_expProgressBarWidth2 = value;
        }
      }

      public int m_totalUseExpItemCount
      {
        get
        {
          return this.m_owner.m_totalUseExpItemCount;
        }
        set
        {
          this.m_owner.m_totalUseExpItemCount = value;
        }
      }

      public int m_tempHeroLevel
      {
        get
        {
          return this.m_owner.m_tempHeroLevel;
        }
        set
        {
          this.m_owner.m_tempHeroLevel = value;
        }
      }

      public int m_tempHeroExp
      {
        get
        {
          return this.m_owner.m_tempHeroExp;
        }
        set
        {
          this.m_owner.m_tempHeroExp = value;
        }
      }

      public int m_tempAddExp
      {
        get
        {
          return this.m_owner.m_tempAddExp;
        }
        set
        {
          this.m_owner.m_tempAddExp = value;
        }
      }

      public int m_heroLastLevel
      {
        get
        {
          return this.m_owner.m_heroLastLevel;
        }
        set
        {
          this.m_owner.m_heroLastLevel = value;
        }
      }

      public HeroExpItemUIController m_curExpBagItemCtrl
      {
        get
        {
          return this.m_owner.m_curExpBagItemCtrl;
        }
        set
        {
          this.m_owner.m_curExpBagItemCtrl = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public int m_addImageCoroutineCount
      {
        get
        {
          return this.m_owner.m_addImageCoroutineCount;
        }
        set
        {
          this.m_owner.m_addImageCoroutineCount = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void HeroAddExpLvUpEffect()
      {
        this.m_owner.HeroAddExpLvUpEffect();
      }

      public void OnExpItemClick(HeroExpItemUIController ctrl)
      {
        this.m_owner.OnExpItemClick(ctrl);
      }

      public void LocalAddExpTick()
      {
        this.m_owner.LocalAddExpTick();
      }

      public IEnumerator AddProgressImageWidth(
        float totalWidth,
        float DesWidth,
        float intervalTime)
      {
        return this.m_owner.AddProgressImageWidth(totalWidth, DesWidth, intervalTime);
      }

      public void OnLocalAddExpFinishedSendReq(int usedCount)
      {
        this.m_owner.OnLocalAddExpFinishedSendReq(usedCount);
      }

      public IEnumerator WaitTimeThenDoEvent(int count)
      {
        return this.m_owner.WaitTimeThenDoEvent(count);
      }

      public bool CanExpItemUse(int heroLv, int herpExp)
      {
        return this.m_owner.CanExpItemUse(heroLv, herpExp);
      }

      public void OnAddExpConfirmButtonClick()
      {
        this.m_owner.OnAddExpConfirmButtonClick();
      }

      public void OnAddExpCanelButtonClick()
      {
        this.m_owner.OnAddExpCanelButtonClick();
      }

      public void CloseAddExpPanel()
      {
        this.m_owner.CloseAddExpPanel();
      }
    }
  }
}
