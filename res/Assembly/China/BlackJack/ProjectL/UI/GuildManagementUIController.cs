﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildManagementUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class GuildManagementUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./InfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoButton;
    [AutoBind("./TitleImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_sociatyName;
    [AutoBind("./TitleImage/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sociatyNameChangeButton;
    [AutoBind("./MainPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_mainPanelStateCtrl;
    [AutoBind("./PlayerResource/GuildCoin/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildCoinText;
    [AutoBind("./PlayerResource/GuildCoin/StatusButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildCoinDescButton;
    [AutoBind("./MainPanel/ListPanel/SociatyListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_sociatyListScrollRect;
    [AutoBind("./MainPanel/ListPanel/SociatyListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sociatyListScrollContent;
    [AutoBind("./MainPanel/ListPanel/TopButtonGroup/PowerButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sociatyListPowerButton;
    [AutoBind("./MainPanel/ListPanel/TopButtonGroup/PowerButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sociatyListPowerButtonStateCtrl;
    [AutoBind("./MainPanel/ListPanel/TopButtonGroup/ActiveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sociatyListActiveButton;
    [AutoBind("./MainPanel/ListPanel/TopButtonGroup/ActiveButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sociatyListActiveButtonStateCtrl;
    [AutoBind("./MainPanel/ListPanel/TopButtonGroup/OnlineButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sociatyListOnlineButton;
    [AutoBind("./MainPanel/ListPanel/TopButtonGroup/OnlineButton/OnlineValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_sociatyListOnlineValueText;
    [AutoBind("./Prefab/ListPlayerInfoItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerInfoPrefabItem;
    [AutoBind("./MainPanel/InfoPanel/SociatyID/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoSociatyIDText;
    [AutoBind("./MainPanel/InfoPanel/PeopleNumber/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoPeopleNumberText;
    [AutoBind("./MainPanel/InfoPanel/Power/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoPowerText;
    [AutoBind("./MainPanel/InfoPanel/Active/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoActiveText;
    [AutoBind("./MainPanel/InfoPanel/Coin/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoCoinText;
    [AutoBind("./MainPanel/InfoPanel/Declaration/DeclarationText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoDeclarationText;
    [AutoBind("./MainPanel/InfoPanel/Declaration/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoDeclarationButton;
    [AutoBind("./MainPanel/InfoPanel/MessageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_messageButton;
    [AutoBind("./MainPanel/InfoPanel/MessageButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageButtonRedPoint;
    [AutoBind("./MainPanel/InfoPanel/QuiteButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_quitButton;
    [AutoBind("./MainPanel/ListPanel/BottomButtonPanel/InviteButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteButton;
    [AutoBind("./MainPanel/ListPanel/BottomButtonPanel/WealButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_wealButton;
    [AutoBind("./MainPanel/ListPanel/BottomButtonPanel/ShopButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shopButton;
    [AutoBind("./MainPanel/ListPanel/BottomButtonPanel/SociatyPlayButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sociatyPlayButton;
    [AutoBind("./MainPanel/ListPanel/BottomButtonPanel/SociatyPlayButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sociatyPlayButtonRedPoint;
    [AutoBind("./DeclarationChangePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_declarationChangePanelStateCtrl;
    [AutoBind("./DeclarationChangePanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_declarationChangePanelBackButton;
    [AutoBind("./DeclarationChangePanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_declarationChangePanelCloseButton;
    [AutoBind("./DeclarationChangePanel/Detail/SaveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_declarationChangePanelSaveButton;
    [AutoBind("./DeclarationChangePanel/Detail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_declarationChangePanelInputField;
    [AutoBind("./AnnouncementChangePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_recruitPanelStateCtrl;
    [AutoBind("./AnnouncementChangePanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recruitPanelBackButton;
    [AutoBind("./AnnouncementChangePanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recruitPanelCloseButton;
    [AutoBind("./AnnouncementChangePanel/Detail/SaveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recruitPanelSaveButton;
    [AutoBind("./AnnouncementChangePanel/Detail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_recruitPanelInputField;
    [AutoBind("./MainPanel/InfoPanel/SetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_invitePanelSetButton;
    [AutoBind("./SetPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildSetPanelStateCtrl;
    [AutoBind("./SetPanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildSetPanelBackButton;
    [AutoBind("./SetPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildSetPanelCloseButton;
    [AutoBind("./SetPanel/Detail/SaveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildSetPanelSaveButton;
    [AutoBind("./SetPanel/Detail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_guildSetPanelHireDeclarationInputField;
    [AutoBind("./SetPanel/Detail/LevelInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_guildSetPanelLevelInputField;
    [AutoBind("./SetPanel/Detail/LevelInputField/PreButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildSetPanelLevelInputFieldLeftButton;
    [AutoBind("./SetPanel/Detail/LevelInputField/AftButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildSetPanelLevelInputFieldRightButton;
    [AutoBind("./SetPanel/Detail/ApproveGroup/AutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_guildSetPanelApproveGroupAutoToggle;
    [AutoBind("./SetPanel/Detail/ApproveGroup/ChairmanButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_guildSetPanelApproveGroupChairmanToggle;
    [AutoBind("./SociatyNameChangePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildChangeNamePanelStateCtrl;
    [AutoBind("./SociatyNameChangePanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildChangeNamePanelBackButton;
    [AutoBind("./SociatyNameChangePanel/Detail/ChangeNameButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildChangeNamePanelChangeNameButton;
    [AutoBind("./SociatyNameChangePanel/Detail/ChangeNameButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildChangeNamePanelChangeNameButtonStateCtrl;
    [AutoBind("./SociatyNameChangePanel/Detail/DeclarationInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_guildChangeNamePanelInputField;
    [AutoBind("./SociatyNameChangePanel/Detail/ChangeNameButton/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildChangeNamePanelValueText;
    [AutoBind("./MessagePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_messagePanelStateCtrl;
    [AutoBind("./MessagePanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_messagePanelBackButton;
    [AutoBind("./MessagePanel/Detail/SideToggleGroup/ApplyToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_messagePanelApplyToggle;
    [AutoBind("./MessagePanel/Detail/SideToggleGroup/JournalToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_messagePanelJournalToggle;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/TopButtonGroup/PowerButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_messageApplyPanelPowerButton;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/TopButtonGroup/PowerButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_messageApplyPanelPowerButtonStateCtrl;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/MessageListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_messageApplyPanelMessageListScrollRect;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/MessageListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageApplyPanelMessageListScrollContent;
    [AutoBind("./Prefab/MessagePlayerItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messagePlayerItemPrafeb;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/AllRefuseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_messageApplyPanelAllRefuseButton;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/CountGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_messageApplyPanelApplyTotalCountValueText;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/PeopleNumber/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_messageApplyPanelTotalPeopleNumberValueText;
    [AutoBind("./MessagePanel/Detail/JournalPanel/JournalListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_messageJournalPanelListScrollRect;
    [AutoBind("./MessagePanel/Detail/JournalPanel/JournalListScrollView/Viewport/Content/JournalInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageJournalPanelListScrollContent;
    [AutoBind("./MessagePanel/Detail/NoItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageNoItemGameObject;
    [AutoBind("./Prefab/MessageJournalItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageJournalItemPrafeb;
    [AutoBind("./InvitePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_invitePanelStateCtrl;
    [AutoBind("./InvitePanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_invitePanelBackButton;
    [AutoBind("./InvitePanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_invitePanelCloseButton;
    [AutoBind("./InvitePanel/Detail/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_invitePanelChangeListButton;
    [AutoBind("./InvitePanel/Detail/InfoPanel/PeopleNumber/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteInfoPanelPeopleNumberText;
    [AutoBind("./InvitePanel/Detail/InfoPanel/Active/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteInfoPanelActiveText;
    [AutoBind("./InvitePanel/Detail/InfoPanel/Level/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteInfoPanelLevelText;
    [AutoBind("./InvitePanel/Detail/InfoPanel/Declaration/DeclarationText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteInfoPanelDeclarationText;
    [AutoBind("./InvitePanel/Detail/InfoPanel/Declaration/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteInfoPanelDeclarationChangeButton;
    [AutoBind("./InvitePanel/Detail/ListPanel/SociatyListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_inviteListPanelSociatyListScrollRect;
    [AutoBind("./InvitePanel/Detail/ListPanel/SociatyListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_inviteListPanelSociatyListScrollContent;
    [AutoBind("./InvitePanel/Detail/ListPanel/TopButtonGroup/PowerButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteListPanelPowerButton;
    [AutoBind("./InvitePanel/Detail/ListPanel/TopButtonGroup/PowerButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_inviteListPanelPowerButtonStateCtrl;
    [AutoBind("./Prefab/InvitePlayerInfoItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_invitePlayerInfoItemPrefab;
    [AutoBind("./QuitDialog", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_quitDialogStateCtrl;
    [AutoBind("./QuitDialog/Panel/Title", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_quitDialogTitleText;
    [AutoBind("./QuitDialog/Panel/Tip", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_quitDialogTipText;
    [AutoBind("./QuitDialog/Panel/ButtonGroup/OkButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_quitDialogConfirmButton;
    [AutoBind("./QuitDialog/Panel/ButtonGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_quitDialogCancelButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private const string StateName_Up = "Up";
    private const string StateName_Down = "Down";
    private const string StateName_Hide = "Hide";
    private bool m_isManageListPowerAscend;
    private bool m_isManageListActiveAscend;
    private bool m_isMessageApplyListPowerAscend;
    private bool m_isInviteListPanelPowerAscend;
    private bool m_isMessageApplyPanelSortByPower;
    private List<GuildMemberInfoItemUIController> m_playerInfoCtrlList;
    private List<GuildApplyMemberInfoItemUIController> m_applyPlayerInfoCtrlList;
    private List<GuildJournalItemUIController> m_guildJournalItemCtrlList;
    private List<GuildInviteMemberInfoItemUIController> m_inviteMemberInfoCtrlList;
    private Guild m_guild;
    private GuildTitle m_guildTitle;
    private List<UserSummary> m_canInvitePlayerList;
    private bool m_isGuildAutoJoin;
    private int m_guildJoinLevel;
    private List<UserSummary> m_messageApplyUserSummarys;
    private GuildManagementUIController.GuildListSortType m_guildListSortType;
    [DoNotToLua]
    private GuildManagementUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_Close_hotfix;
    private LuaFunction m_UpdateViewGuild_hotfix;
    private LuaFunction m_RefreshGuildTitle_hotfix;
    private LuaFunction m_SetStateByGuildTitle_hotfix;
    private LuaFunction m_SetSociatyInfoPanel_hotfix;
    private LuaFunction m_SetSociatyListPanelList`1_hotfix;
    private LuaFunction m_OnGuildManagementListPlayeItemClickGuildMemberInfoItemUIController_hotfix;
    private LuaFunction m_SetMessageRedPointBoolean_hotfix;
    private LuaFunction m_OnReturnImgButtonClick_hotfix;
    private LuaFunction m_OnInfoButtonClick_hotfix;
    private LuaFunction m_OnGuildCoinButtonClick_hotfix;
    private LuaFunction m_ClearData_hotfix;
    private LuaFunction m_CloseAllSmallPanel_hotfix;
    private LuaFunction m_OnOnlineButtonClick_hotfix;
    private LuaFunction m_OnActiveButtonClick_hotfix;
    private LuaFunction m_OnPowerButtonClick_hotfix;
    private LuaFunction m_SetSortButtonStateGuildListSortType_hotfix;
    private LuaFunction m_SortByTitleGuildMemberCacheObjectGuildMemberCacheObject_hotfix;
    private LuaFunction m_SortByPowerGuildMemberCacheObjectGuildMemberCacheObject_hotfix;
    private LuaFunction m_SortByActiveGuildMemberCacheObjectGuildMemberCacheObject_hotfix;
    private LuaFunction m_SortGuildListByDefaultList`1_hotfix;
    private LuaFunction m_SortByDefaultTypeGuildMemberCacheObjectGuildMemberCacheObject_hotfix;
    private LuaFunction m_SortByActiveTypeGuildMemberCacheObjectGuildMemberCacheObject_hotfix;
    private LuaFunction m_SortByBattlePowerTypeGuildMemberCacheObjectGuildMemberCacheObject_hotfix;
    private LuaFunction m_OnSociatyNameChangeButtonClick_hotfix;
    private LuaFunction m_OnGuildChangeNamePanelChangeNameButtonClick_hotfix;
    private LuaFunction m_OnGuildChangeNamePanelBackButtonClick_hotfix;
    private LuaFunction m_OnDeclarationButtonClick_hotfix;
    private LuaFunction m_OnDeclarationChangePanelSaveButtonClick_hotfix;
    private LuaFunction m_OnDeclarationChangePanelBackButtonClick_hotfix;
    private LuaFunction m_OnQuitButtonClick_hotfix;
    private LuaFunction m_OnQuitGuildConfirmButtonClick_hotfix;
    private LuaFunction m_CloseQuitGuildConfirmPanel_hotfix;
    private LuaFunction m_OnSociatyPlayButtonClick_hotfix;
    private LuaFunction m_OnShopButtonClick_hotfix;
    private LuaFunction m_OnWealButtonClick_hotfix;
    private LuaFunction m_OnMessageButtonClick_hotfix;
    private LuaFunction m_SendNtfBeforeOpenMessagePanelBooleanAction_hotfix;
    private LuaFunction m_SetMessageApplyPanelList`1_hotfix;
    private LuaFunction m_OnGuildApplyMemberAcceptResultGuildApplyMemberInfoItemUIControllerBoolean_hotfix;
    private LuaFunction m_SetMessageJournalPanelList`1_hotfix;
    private LuaFunction m_OnMssageApplyPanelPowerButtonClick_hotfix;
    private LuaFunction m_OnMessagePanelBackButtonClick_hotfix;
    private LuaFunction m_CloseMessagePanelAction_hotfix;
    private LuaFunction m_OnMessagePanelApplyToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnMessagePanelJournalToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnMessageApplyPanelAllRefuseButtonClick_hotfix;
    private LuaFunction m_OnInviteButtonClick_hotfix;
    private LuaFunction m_SetInvitePanelList`1_hotfix;
    private LuaFunction m_SetInviteInfoPanel_hotfix;
    private LuaFunction m_SetInviteListPanel_hotfix;
    private LuaFunction m_OnInviteMemberInfoItemClickGuildInviteMemberInfoItemUIController_hotfix;
    private LuaFunction m_OnInviteListPanelPowerButtonClick_hotfix;
    private LuaFunction m_OnInvitePanelChangeListButtonClick_hotfix;
    private LuaFunction m_OnInvitePanelBackButton_hotfix;
    private LuaFunction m_CloseInvitePanelAction_hotfix;
    private LuaFunction m_OnInviteInfoPanelDeclarationChangeButtonClick_hotfix;
    private LuaFunction m_SetRecruitChangeSubPanelInfo_hotfix;
    private LuaFunction m_OnRecruitPanelSaveButtonClick_hotfix;
    private LuaFunction m_OnRecruitPanelBackButtonClick_hotfix;
    private LuaFunction m_OnInvitePanelSetButtonClick_hotfix;
    private LuaFunction m_SetGuildSetSubPanel_hotfix;
    private LuaFunction m_OnGuildSetPanelChairmanToggleClickBoolean_hotfix;
    private LuaFunction m_OnGuildSetPanelAutoToggleClickBoolean_hotfix;
    private LuaFunction m_OnGuildSetPanelLevelInputFieldRightButtonClick_hotfix;
    private LuaFunction m_OnGuildSetPanelLevelInputFieldLeftButtonClick_hotfix;
    private LuaFunction m_OnGuildSetPanelLevelInputFieldEditEndString_hotfix;
    private LuaFunction m_OnGuildSetPanelSaveButtonClick_hotfix;
    private LuaFunction m_OnGuildSetPanelBackButtonClick_hotfix;
    private LuaFunction m_CloseGuildSetPanelAction_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;
    private LuaFunction m_add_EventOnChangeGuildNameAction`2_hotfix;
    private LuaFunction m_remove_EventOnChangeGuildNameAction`2_hotfix;
    private LuaFunction m_add_EventOnGuildAnnouncementSetAction`2_hotfix;
    private LuaFunction m_remove_EventOnGuildAnnouncementSetAction`2_hotfix;
    private LuaFunction m_add_EventOnQuitGuildAction`1_hotfix;
    private LuaFunction m_remove_EventOnQuitGuildAction`1_hotfix;
    private LuaFunction m_add_EventOnGetCanInvitePlayerListAction`2_hotfix;
    private LuaFunction m_remove_EventOnGetCanInvitePlayerListAction`2_hotfix;
    private LuaFunction m_add_EventOnGuildHiringDeclarationSetAction`2_hotfix;
    private LuaFunction m_remove_EventOnGuildHiringDeclarationSetAction`2_hotfix;
    private LuaFunction m_add_EventOnGuildInfoSetAction`4_hotfix;
    private LuaFunction m_remove_EventOnGuildInfoSetAction`4_hotfix;
    private LuaFunction m_add_EventOnGetGuildJoinApplyAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetGuildJoinApplyAction`1_hotfix;
    private LuaFunction m_add_EventOnGetGuildJournalAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetGuildJournalAction`1_hotfix;
    private LuaFunction m_add_EventOnGuildJoinConfirmOrRefuseAction`3_hotfix;
    private LuaFunction m_remove_EventOnGuildJoinConfirmOrRefuseAction`3_hotfix;
    private LuaFunction m_add_EventOnGuildInviteMemberAction`2_hotfix;
    private LuaFunction m_remove_EventOnGuildInviteMemberAction`2_hotfix;
    private LuaFunction m_add_EventOnGuildMemberClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnGuildMemberClickAction`3_hotfix;
    private LuaFunction m_add_EventOnAllRefuseButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnAllRefuseButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnGotoGuildStoreAction_hotfix;
    private LuaFunction m_remove_EventOnGotoGuildStoreAction_hotfix;
    private LuaFunction m_add_EventOnGotoGuildGameListPanelAction_hotfix;
    private LuaFunction m_remove_EventOnGotoGuildGameListPanelAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView(Guild guild)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshGuildTitle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetStateByGuildTitle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSociatyInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSociatyListPanel(List<GuildMemberCacheObject> memberList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildManagementListPlayeItemClick(GuildMemberInfoItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMessageRedPoint(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnImgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildCoinButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAllSmallPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActiveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPowerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSortButtonState(
      GuildManagementUIController.GuildListSortType sortType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortByTitle(GuildMemberCacheObject a, GuildMemberCacheObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortByPower(GuildMemberCacheObject a, GuildMemberCacheObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortByActive(GuildMemberCacheObject a, GuildMemberCacheObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortGuildListByDefault(List<GuildMemberCacheObject> guildList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortByDefaultType(GuildMemberCacheObject a, GuildMemberCacheObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortByActiveType(GuildMemberCacheObject a, GuildMemberCacheObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortByBattlePowerType(GuildMemberCacheObject a, GuildMemberCacheObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSociatyNameChangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildChangeNamePanelChangeNameButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildChangeNamePanelBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDeclarationButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDeclarationChangePanelSaveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDeclarationChangePanelBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnQuitButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnQuitGuildConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseQuitGuildConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSociatyPlayButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShopButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWealButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessageButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendNtfBeforeOpenMessagePanel(bool isManager, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMessageApplyPanel(List<UserSummary> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildApplyMemberAcceptResult(
      GuildApplyMemberInfoItemUIController ctrl,
      bool result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMessageJournalPanel(List<GuildLog> journal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMssageApplyPanelPowerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessagePanelBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseMessagePanel(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessagePanelApplyToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessagePanelJournalToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessageApplyPanelAllRefuseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInvitePanel(List<UserSummary> playerList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInviteInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInviteListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteMemberInfoItemClick(GuildInviteMemberInfoItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteListPanelPowerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInvitePanelChangeListButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInvitePanelBackButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseInvitePanel(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteInfoPanelDeclarationChangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRecruitChangeSubPanelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecruitPanelSaveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecruitPanelBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInvitePanelSetButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGuildSetSubPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelChairmanToggleClick(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelAutoToggleClick(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelLevelInputFieldRightButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelLevelInputFieldLeftButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelLevelInputFieldEditEnd(string inputStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelSaveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseGuildSetPanel(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, Action> EventOnChangeGuildName
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, Action> EventOnGuildAnnouncementSet
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action> EventOnQuitGuild
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action<List<UserSummary>>, bool> EventOnGetCanInvitePlayerList
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, Action> EventOnGuildHiringDeclarationSet
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool, int, string, Action> EventOnGuildInfoSet
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action<List<UserSummary>>> EventOnGetGuildJoinApply
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action<List<GuildLog>>> EventOnGetGuildJournal
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, bool, Action> EventOnGuildJoinConfirmOrRefuse
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, Action> EventOnGuildInviteMember
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, Vector3, PlayerSimpleInfoUITask.PostionType> EventOnGuildMemberClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action> EventOnAllRefuseButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGotoGuildStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGotoGuildGameListPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public GuildManagementUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangeGuildName(string arg1, Action arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChangeGuildName(string arg1, Action arg2)
    {
      this.EventOnChangeGuildName = (Action<string, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGuildAnnouncementSet(string arg1, Action arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGuildAnnouncementSet(string arg1, Action arg2)
    {
      this.EventOnGuildAnnouncementSet = (Action<string, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnQuitGuild(Action obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnQuitGuild(Action obj)
    {
      this.EventOnQuitGuild = (Action<Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetCanInvitePlayerList(Action<List<UserSummary>> arg1, bool arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetCanInvitePlayerList(
      Action<List<UserSummary>> arg1,
      bool arg2)
    {
      this.EventOnGetCanInvitePlayerList = (Action<Action<List<UserSummary>>, bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGuildHiringDeclarationSet(string arg1, Action arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGuildHiringDeclarationSet(string arg1, Action arg2)
    {
      this.EventOnGuildHiringDeclarationSet = (Action<string, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGuildInfoSet(bool arg1, int arg2, string arg3, Action arg4)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGuildInfoSet(bool arg1, int arg2, string arg3, Action arg4)
    {
      this.EventOnGuildInfoSet = (Action<bool, int, string, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetGuildJoinApply(Action<List<UserSummary>> obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetGuildJoinApply(Action<List<UserSummary>> obj)
    {
      this.EventOnGetGuildJoinApply = (Action<Action<List<UserSummary>>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetGuildJournal(Action<List<GuildLog>> obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetGuildJournal(Action<List<GuildLog>> obj)
    {
      this.EventOnGetGuildJournal = (Action<Action<List<GuildLog>>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGuildJoinConfirmOrRefuse(string arg1, bool arg2, Action arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGuildJoinConfirmOrRefuse(string arg1, bool arg2, Action arg3)
    {
      this.EventOnGuildJoinConfirmOrRefuse = (Action<string, bool, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGuildInviteMember(string arg1, Action arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGuildInviteMember(string arg1, Action arg2)
    {
      this.EventOnGuildInviteMember = (Action<string, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGuildMemberClick(
      string arg1,
      Vector3 arg2,
      PlayerSimpleInfoUITask.PostionType arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGuildMemberClick(
      string arg1,
      Vector3 arg2,
      PlayerSimpleInfoUITask.PostionType arg3)
    {
      this.EventOnGuildMemberClick = (Action<string, Vector3, PlayerSimpleInfoUITask.PostionType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAllRefuseButtonClick(Action obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAllRefuseButtonClick(Action obj)
    {
      this.EventOnAllRefuseButtonClick = (Action<Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGotoGuildStore()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGotoGuildStore()
    {
      this.EventOnGotoGuildStore = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGotoGuildGameListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGotoGuildGameListPanel()
    {
      this.EventOnGotoGuildGameListPanel = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum GuildListSortType
    {
      Default,
      Active,
      BattlePower,
    }

    public class LuaExportHelper
    {
      private GuildManagementUIController m_owner;

      public LuaExportHelper(GuildManagementUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public void __callDele_EventOnChangeGuildName(string arg1, Action arg2)
      {
        this.m_owner.__callDele_EventOnChangeGuildName(arg1, arg2);
      }

      public void __clearDele_EventOnChangeGuildName(string arg1, Action arg2)
      {
        this.m_owner.__clearDele_EventOnChangeGuildName(arg1, arg2);
      }

      public void __callDele_EventOnGuildAnnouncementSet(string arg1, Action arg2)
      {
        this.m_owner.__callDele_EventOnGuildAnnouncementSet(arg1, arg2);
      }

      public void __clearDele_EventOnGuildAnnouncementSet(string arg1, Action arg2)
      {
        this.m_owner.__clearDele_EventOnGuildAnnouncementSet(arg1, arg2);
      }

      public void __callDele_EventOnQuitGuild(Action obj)
      {
        this.m_owner.__callDele_EventOnQuitGuild(obj);
      }

      public void __clearDele_EventOnQuitGuild(Action obj)
      {
        this.m_owner.__clearDele_EventOnQuitGuild(obj);
      }

      public void __callDele_EventOnGetCanInvitePlayerList(
        Action<List<UserSummary>> arg1,
        bool arg2)
      {
        this.m_owner.__callDele_EventOnGetCanInvitePlayerList(arg1, arg2);
      }

      public void __clearDele_EventOnGetCanInvitePlayerList(
        Action<List<UserSummary>> arg1,
        bool arg2)
      {
        this.m_owner.__clearDele_EventOnGetCanInvitePlayerList(arg1, arg2);
      }

      public void __callDele_EventOnGuildHiringDeclarationSet(string arg1, Action arg2)
      {
        this.m_owner.__callDele_EventOnGuildHiringDeclarationSet(arg1, arg2);
      }

      public void __clearDele_EventOnGuildHiringDeclarationSet(string arg1, Action arg2)
      {
        this.m_owner.__clearDele_EventOnGuildHiringDeclarationSet(arg1, arg2);
      }

      public void __callDele_EventOnGuildInfoSet(bool arg1, int arg2, string arg3, Action arg4)
      {
        this.m_owner.__callDele_EventOnGuildInfoSet(arg1, arg2, arg3, arg4);
      }

      public void __clearDele_EventOnGuildInfoSet(bool arg1, int arg2, string arg3, Action arg4)
      {
        this.m_owner.__clearDele_EventOnGuildInfoSet(arg1, arg2, arg3, arg4);
      }

      public void __callDele_EventOnGetGuildJoinApply(Action<List<UserSummary>> obj)
      {
        this.m_owner.__callDele_EventOnGetGuildJoinApply(obj);
      }

      public void __clearDele_EventOnGetGuildJoinApply(Action<List<UserSummary>> obj)
      {
        this.m_owner.__clearDele_EventOnGetGuildJoinApply(obj);
      }

      public void __callDele_EventOnGetGuildJournal(Action<List<GuildLog>> obj)
      {
        this.m_owner.__callDele_EventOnGetGuildJournal(obj);
      }

      public void __clearDele_EventOnGetGuildJournal(Action<List<GuildLog>> obj)
      {
        this.m_owner.__clearDele_EventOnGetGuildJournal(obj);
      }

      public void __callDele_EventOnGuildJoinConfirmOrRefuse(string arg1, bool arg2, Action arg3)
      {
        this.m_owner.__callDele_EventOnGuildJoinConfirmOrRefuse(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnGuildJoinConfirmOrRefuse(string arg1, bool arg2, Action arg3)
      {
        this.m_owner.__clearDele_EventOnGuildJoinConfirmOrRefuse(arg1, arg2, arg3);
      }

      public void __callDele_EventOnGuildInviteMember(string arg1, Action arg2)
      {
        this.m_owner.__callDele_EventOnGuildInviteMember(arg1, arg2);
      }

      public void __clearDele_EventOnGuildInviteMember(string arg1, Action arg2)
      {
        this.m_owner.__clearDele_EventOnGuildInviteMember(arg1, arg2);
      }

      public void __callDele_EventOnGuildMemberClick(
        string arg1,
        Vector3 arg2,
        PlayerSimpleInfoUITask.PostionType arg3)
      {
        this.m_owner.__callDele_EventOnGuildMemberClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnGuildMemberClick(
        string arg1,
        Vector3 arg2,
        PlayerSimpleInfoUITask.PostionType arg3)
      {
        this.m_owner.__clearDele_EventOnGuildMemberClick(arg1, arg2, arg3);
      }

      public void __callDele_EventOnAllRefuseButtonClick(Action obj)
      {
        this.m_owner.__callDele_EventOnAllRefuseButtonClick(obj);
      }

      public void __clearDele_EventOnAllRefuseButtonClick(Action obj)
      {
        this.m_owner.__clearDele_EventOnAllRefuseButtonClick(obj);
      }

      public void __callDele_EventOnGotoGuildStore()
      {
        this.m_owner.__callDele_EventOnGotoGuildStore();
      }

      public void __clearDele_EventOnGotoGuildStore()
      {
        this.m_owner.__clearDele_EventOnGotoGuildStore();
      }

      public void __callDele_EventOnGotoGuildGameListPanel()
      {
        this.m_owner.__callDele_EventOnGotoGuildGameListPanel();
      }

      public void __clearDele_EventOnGotoGuildGameListPanel()
      {
        this.m_owner.__clearDele_EventOnGotoGuildGameListPanel();
      }

      public CommonUIStateController m_commonUIStateController
      {
        get
        {
          return this.m_owner.m_commonUIStateController;
        }
        set
        {
          this.m_owner.m_commonUIStateController = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_infoButton
      {
        get
        {
          return this.m_owner.m_infoButton;
        }
        set
        {
          this.m_owner.m_infoButton = value;
        }
      }

      public Text m_sociatyName
      {
        get
        {
          return this.m_owner.m_sociatyName;
        }
        set
        {
          this.m_owner.m_sociatyName = value;
        }
      }

      public Button m_sociatyNameChangeButton
      {
        get
        {
          return this.m_owner.m_sociatyNameChangeButton;
        }
        set
        {
          this.m_owner.m_sociatyNameChangeButton = value;
        }
      }

      public CommonUIStateController m_mainPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_mainPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_mainPanelStateCtrl = value;
        }
      }

      public Text m_guildCoinText
      {
        get
        {
          return this.m_owner.m_guildCoinText;
        }
        set
        {
          this.m_owner.m_guildCoinText = value;
        }
      }

      public Button m_guildCoinDescButton
      {
        get
        {
          return this.m_owner.m_guildCoinDescButton;
        }
        set
        {
          this.m_owner.m_guildCoinDescButton = value;
        }
      }

      public ScrollRect m_sociatyListScrollRect
      {
        get
        {
          return this.m_owner.m_sociatyListScrollRect;
        }
        set
        {
          this.m_owner.m_sociatyListScrollRect = value;
        }
      }

      public GameObject m_sociatyListScrollContent
      {
        get
        {
          return this.m_owner.m_sociatyListScrollContent;
        }
        set
        {
          this.m_owner.m_sociatyListScrollContent = value;
        }
      }

      public Button m_sociatyListPowerButton
      {
        get
        {
          return this.m_owner.m_sociatyListPowerButton;
        }
        set
        {
          this.m_owner.m_sociatyListPowerButton = value;
        }
      }

      public CommonUIStateController m_sociatyListPowerButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_sociatyListPowerButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_sociatyListPowerButtonStateCtrl = value;
        }
      }

      public Button m_sociatyListActiveButton
      {
        get
        {
          return this.m_owner.m_sociatyListActiveButton;
        }
        set
        {
          this.m_owner.m_sociatyListActiveButton = value;
        }
      }

      public CommonUIStateController m_sociatyListActiveButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_sociatyListActiveButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_sociatyListActiveButtonStateCtrl = value;
        }
      }

      public Button m_sociatyListOnlineButton
      {
        get
        {
          return this.m_owner.m_sociatyListOnlineButton;
        }
        set
        {
          this.m_owner.m_sociatyListOnlineButton = value;
        }
      }

      public Text m_sociatyListOnlineValueText
      {
        get
        {
          return this.m_owner.m_sociatyListOnlineValueText;
        }
        set
        {
          this.m_owner.m_sociatyListOnlineValueText = value;
        }
      }

      public GameObject m_playerInfoPrefabItem
      {
        get
        {
          return this.m_owner.m_playerInfoPrefabItem;
        }
        set
        {
          this.m_owner.m_playerInfoPrefabItem = value;
        }
      }

      public Text m_infoSociatyIDText
      {
        get
        {
          return this.m_owner.m_infoSociatyIDText;
        }
        set
        {
          this.m_owner.m_infoSociatyIDText = value;
        }
      }

      public Text m_infoPeopleNumberText
      {
        get
        {
          return this.m_owner.m_infoPeopleNumberText;
        }
        set
        {
          this.m_owner.m_infoPeopleNumberText = value;
        }
      }

      public Text m_infoPowerText
      {
        get
        {
          return this.m_owner.m_infoPowerText;
        }
        set
        {
          this.m_owner.m_infoPowerText = value;
        }
      }

      public Text m_infoActiveText
      {
        get
        {
          return this.m_owner.m_infoActiveText;
        }
        set
        {
          this.m_owner.m_infoActiveText = value;
        }
      }

      public Text m_infoCoinText
      {
        get
        {
          return this.m_owner.m_infoCoinText;
        }
        set
        {
          this.m_owner.m_infoCoinText = value;
        }
      }

      public Text m_infoDeclarationText
      {
        get
        {
          return this.m_owner.m_infoDeclarationText;
        }
        set
        {
          this.m_owner.m_infoDeclarationText = value;
        }
      }

      public Button m_infoDeclarationButton
      {
        get
        {
          return this.m_owner.m_infoDeclarationButton;
        }
        set
        {
          this.m_owner.m_infoDeclarationButton = value;
        }
      }

      public Button m_messageButton
      {
        get
        {
          return this.m_owner.m_messageButton;
        }
        set
        {
          this.m_owner.m_messageButton = value;
        }
      }

      public GameObject m_messageButtonRedPoint
      {
        get
        {
          return this.m_owner.m_messageButtonRedPoint;
        }
        set
        {
          this.m_owner.m_messageButtonRedPoint = value;
        }
      }

      public Button m_quitButton
      {
        get
        {
          return this.m_owner.m_quitButton;
        }
        set
        {
          this.m_owner.m_quitButton = value;
        }
      }

      public Button m_inviteButton
      {
        get
        {
          return this.m_owner.m_inviteButton;
        }
        set
        {
          this.m_owner.m_inviteButton = value;
        }
      }

      public Button m_wealButton
      {
        get
        {
          return this.m_owner.m_wealButton;
        }
        set
        {
          this.m_owner.m_wealButton = value;
        }
      }

      public Button m_shopButton
      {
        get
        {
          return this.m_owner.m_shopButton;
        }
        set
        {
          this.m_owner.m_shopButton = value;
        }
      }

      public Button m_sociatyPlayButton
      {
        get
        {
          return this.m_owner.m_sociatyPlayButton;
        }
        set
        {
          this.m_owner.m_sociatyPlayButton = value;
        }
      }

      public GameObject m_sociatyPlayButtonRedPoint
      {
        get
        {
          return this.m_owner.m_sociatyPlayButtonRedPoint;
        }
        set
        {
          this.m_owner.m_sociatyPlayButtonRedPoint = value;
        }
      }

      public CommonUIStateController m_declarationChangePanelStateCtrl
      {
        get
        {
          return this.m_owner.m_declarationChangePanelStateCtrl;
        }
        set
        {
          this.m_owner.m_declarationChangePanelStateCtrl = value;
        }
      }

      public Button m_declarationChangePanelBackButton
      {
        get
        {
          return this.m_owner.m_declarationChangePanelBackButton;
        }
        set
        {
          this.m_owner.m_declarationChangePanelBackButton = value;
        }
      }

      public Button m_declarationChangePanelCloseButton
      {
        get
        {
          return this.m_owner.m_declarationChangePanelCloseButton;
        }
        set
        {
          this.m_owner.m_declarationChangePanelCloseButton = value;
        }
      }

      public Button m_declarationChangePanelSaveButton
      {
        get
        {
          return this.m_owner.m_declarationChangePanelSaveButton;
        }
        set
        {
          this.m_owner.m_declarationChangePanelSaveButton = value;
        }
      }

      public InputField m_declarationChangePanelInputField
      {
        get
        {
          return this.m_owner.m_declarationChangePanelInputField;
        }
        set
        {
          this.m_owner.m_declarationChangePanelInputField = value;
        }
      }

      public CommonUIStateController m_recruitPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_recruitPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_recruitPanelStateCtrl = value;
        }
      }

      public Button m_recruitPanelBackButton
      {
        get
        {
          return this.m_owner.m_recruitPanelBackButton;
        }
        set
        {
          this.m_owner.m_recruitPanelBackButton = value;
        }
      }

      public Button m_recruitPanelCloseButton
      {
        get
        {
          return this.m_owner.m_recruitPanelCloseButton;
        }
        set
        {
          this.m_owner.m_recruitPanelCloseButton = value;
        }
      }

      public Button m_recruitPanelSaveButton
      {
        get
        {
          return this.m_owner.m_recruitPanelSaveButton;
        }
        set
        {
          this.m_owner.m_recruitPanelSaveButton = value;
        }
      }

      public InputField m_recruitPanelInputField
      {
        get
        {
          return this.m_owner.m_recruitPanelInputField;
        }
        set
        {
          this.m_owner.m_recruitPanelInputField = value;
        }
      }

      public Button m_invitePanelSetButton
      {
        get
        {
          return this.m_owner.m_invitePanelSetButton;
        }
        set
        {
          this.m_owner.m_invitePanelSetButton = value;
        }
      }

      public CommonUIStateController m_guildSetPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_guildSetPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_guildSetPanelStateCtrl = value;
        }
      }

      public Button m_guildSetPanelBackButton
      {
        get
        {
          return this.m_owner.m_guildSetPanelBackButton;
        }
        set
        {
          this.m_owner.m_guildSetPanelBackButton = value;
        }
      }

      public Button m_guildSetPanelCloseButton
      {
        get
        {
          return this.m_owner.m_guildSetPanelCloseButton;
        }
        set
        {
          this.m_owner.m_guildSetPanelCloseButton = value;
        }
      }

      public Button m_guildSetPanelSaveButton
      {
        get
        {
          return this.m_owner.m_guildSetPanelSaveButton;
        }
        set
        {
          this.m_owner.m_guildSetPanelSaveButton = value;
        }
      }

      public InputField m_guildSetPanelHireDeclarationInputField
      {
        get
        {
          return this.m_owner.m_guildSetPanelHireDeclarationInputField;
        }
        set
        {
          this.m_owner.m_guildSetPanelHireDeclarationInputField = value;
        }
      }

      public InputField m_guildSetPanelLevelInputField
      {
        get
        {
          return this.m_owner.m_guildSetPanelLevelInputField;
        }
        set
        {
          this.m_owner.m_guildSetPanelLevelInputField = value;
        }
      }

      public Button m_guildSetPanelLevelInputFieldLeftButton
      {
        get
        {
          return this.m_owner.m_guildSetPanelLevelInputFieldLeftButton;
        }
        set
        {
          this.m_owner.m_guildSetPanelLevelInputFieldLeftButton = value;
        }
      }

      public Button m_guildSetPanelLevelInputFieldRightButton
      {
        get
        {
          return this.m_owner.m_guildSetPanelLevelInputFieldRightButton;
        }
        set
        {
          this.m_owner.m_guildSetPanelLevelInputFieldRightButton = value;
        }
      }

      public Toggle m_guildSetPanelApproveGroupAutoToggle
      {
        get
        {
          return this.m_owner.m_guildSetPanelApproveGroupAutoToggle;
        }
        set
        {
          this.m_owner.m_guildSetPanelApproveGroupAutoToggle = value;
        }
      }

      public Toggle m_guildSetPanelApproveGroupChairmanToggle
      {
        get
        {
          return this.m_owner.m_guildSetPanelApproveGroupChairmanToggle;
        }
        set
        {
          this.m_owner.m_guildSetPanelApproveGroupChairmanToggle = value;
        }
      }

      public CommonUIStateController m_guildChangeNamePanelStateCtrl
      {
        get
        {
          return this.m_owner.m_guildChangeNamePanelStateCtrl;
        }
        set
        {
          this.m_owner.m_guildChangeNamePanelStateCtrl = value;
        }
      }

      public Button m_guildChangeNamePanelBackButton
      {
        get
        {
          return this.m_owner.m_guildChangeNamePanelBackButton;
        }
        set
        {
          this.m_owner.m_guildChangeNamePanelBackButton = value;
        }
      }

      public Button m_guildChangeNamePanelChangeNameButton
      {
        get
        {
          return this.m_owner.m_guildChangeNamePanelChangeNameButton;
        }
        set
        {
          this.m_owner.m_guildChangeNamePanelChangeNameButton = value;
        }
      }

      public CommonUIStateController m_guildChangeNamePanelChangeNameButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_guildChangeNamePanelChangeNameButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_guildChangeNamePanelChangeNameButtonStateCtrl = value;
        }
      }

      public InputField m_guildChangeNamePanelInputField
      {
        get
        {
          return this.m_owner.m_guildChangeNamePanelInputField;
        }
        set
        {
          this.m_owner.m_guildChangeNamePanelInputField = value;
        }
      }

      public Text m_guildChangeNamePanelValueText
      {
        get
        {
          return this.m_owner.m_guildChangeNamePanelValueText;
        }
        set
        {
          this.m_owner.m_guildChangeNamePanelValueText = value;
        }
      }

      public CommonUIStateController m_messagePanelStateCtrl
      {
        get
        {
          return this.m_owner.m_messagePanelStateCtrl;
        }
        set
        {
          this.m_owner.m_messagePanelStateCtrl = value;
        }
      }

      public Button m_messagePanelBackButton
      {
        get
        {
          return this.m_owner.m_messagePanelBackButton;
        }
        set
        {
          this.m_owner.m_messagePanelBackButton = value;
        }
      }

      public Toggle m_messagePanelApplyToggle
      {
        get
        {
          return this.m_owner.m_messagePanelApplyToggle;
        }
        set
        {
          this.m_owner.m_messagePanelApplyToggle = value;
        }
      }

      public Toggle m_messagePanelJournalToggle
      {
        get
        {
          return this.m_owner.m_messagePanelJournalToggle;
        }
        set
        {
          this.m_owner.m_messagePanelJournalToggle = value;
        }
      }

      public Button m_messageApplyPanelPowerButton
      {
        get
        {
          return this.m_owner.m_messageApplyPanelPowerButton;
        }
        set
        {
          this.m_owner.m_messageApplyPanelPowerButton = value;
        }
      }

      public CommonUIStateController m_messageApplyPanelPowerButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_messageApplyPanelPowerButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_messageApplyPanelPowerButtonStateCtrl = value;
        }
      }

      public ScrollRect m_messageApplyPanelMessageListScrollRect
      {
        get
        {
          return this.m_owner.m_messageApplyPanelMessageListScrollRect;
        }
        set
        {
          this.m_owner.m_messageApplyPanelMessageListScrollRect = value;
        }
      }

      public GameObject m_messageApplyPanelMessageListScrollContent
      {
        get
        {
          return this.m_owner.m_messageApplyPanelMessageListScrollContent;
        }
        set
        {
          this.m_owner.m_messageApplyPanelMessageListScrollContent = value;
        }
      }

      public GameObject m_messagePlayerItemPrafeb
      {
        get
        {
          return this.m_owner.m_messagePlayerItemPrafeb;
        }
        set
        {
          this.m_owner.m_messagePlayerItemPrafeb = value;
        }
      }

      public Button m_messageApplyPanelAllRefuseButton
      {
        get
        {
          return this.m_owner.m_messageApplyPanelAllRefuseButton;
        }
        set
        {
          this.m_owner.m_messageApplyPanelAllRefuseButton = value;
        }
      }

      public Text m_messageApplyPanelApplyTotalCountValueText
      {
        get
        {
          return this.m_owner.m_messageApplyPanelApplyTotalCountValueText;
        }
        set
        {
          this.m_owner.m_messageApplyPanelApplyTotalCountValueText = value;
        }
      }

      public Text m_messageApplyPanelTotalPeopleNumberValueText
      {
        get
        {
          return this.m_owner.m_messageApplyPanelTotalPeopleNumberValueText;
        }
        set
        {
          this.m_owner.m_messageApplyPanelTotalPeopleNumberValueText = value;
        }
      }

      public ScrollRect m_messageJournalPanelListScrollRect
      {
        get
        {
          return this.m_owner.m_messageJournalPanelListScrollRect;
        }
        set
        {
          this.m_owner.m_messageJournalPanelListScrollRect = value;
        }
      }

      public GameObject m_messageJournalPanelListScrollContent
      {
        get
        {
          return this.m_owner.m_messageJournalPanelListScrollContent;
        }
        set
        {
          this.m_owner.m_messageJournalPanelListScrollContent = value;
        }
      }

      public GameObject m_messageNoItemGameObject
      {
        get
        {
          return this.m_owner.m_messageNoItemGameObject;
        }
        set
        {
          this.m_owner.m_messageNoItemGameObject = value;
        }
      }

      public GameObject m_messageJournalItemPrafeb
      {
        get
        {
          return this.m_owner.m_messageJournalItemPrafeb;
        }
        set
        {
          this.m_owner.m_messageJournalItemPrafeb = value;
        }
      }

      public CommonUIStateController m_invitePanelStateCtrl
      {
        get
        {
          return this.m_owner.m_invitePanelStateCtrl;
        }
        set
        {
          this.m_owner.m_invitePanelStateCtrl = value;
        }
      }

      public Button m_invitePanelBackButton
      {
        get
        {
          return this.m_owner.m_invitePanelBackButton;
        }
        set
        {
          this.m_owner.m_invitePanelBackButton = value;
        }
      }

      public Button m_invitePanelCloseButton
      {
        get
        {
          return this.m_owner.m_invitePanelCloseButton;
        }
        set
        {
          this.m_owner.m_invitePanelCloseButton = value;
        }
      }

      public Button m_invitePanelChangeListButton
      {
        get
        {
          return this.m_owner.m_invitePanelChangeListButton;
        }
        set
        {
          this.m_owner.m_invitePanelChangeListButton = value;
        }
      }

      public Text m_inviteInfoPanelPeopleNumberText
      {
        get
        {
          return this.m_owner.m_inviteInfoPanelPeopleNumberText;
        }
        set
        {
          this.m_owner.m_inviteInfoPanelPeopleNumberText = value;
        }
      }

      public Text m_inviteInfoPanelActiveText
      {
        get
        {
          return this.m_owner.m_inviteInfoPanelActiveText;
        }
        set
        {
          this.m_owner.m_inviteInfoPanelActiveText = value;
        }
      }

      public Text m_inviteInfoPanelLevelText
      {
        get
        {
          return this.m_owner.m_inviteInfoPanelLevelText;
        }
        set
        {
          this.m_owner.m_inviteInfoPanelLevelText = value;
        }
      }

      public Text m_inviteInfoPanelDeclarationText
      {
        get
        {
          return this.m_owner.m_inviteInfoPanelDeclarationText;
        }
        set
        {
          this.m_owner.m_inviteInfoPanelDeclarationText = value;
        }
      }

      public Button m_inviteInfoPanelDeclarationChangeButton
      {
        get
        {
          return this.m_owner.m_inviteInfoPanelDeclarationChangeButton;
        }
        set
        {
          this.m_owner.m_inviteInfoPanelDeclarationChangeButton = value;
        }
      }

      public ScrollRect m_inviteListPanelSociatyListScrollRect
      {
        get
        {
          return this.m_owner.m_inviteListPanelSociatyListScrollRect;
        }
        set
        {
          this.m_owner.m_inviteListPanelSociatyListScrollRect = value;
        }
      }

      public GameObject m_inviteListPanelSociatyListScrollContent
      {
        get
        {
          return this.m_owner.m_inviteListPanelSociatyListScrollContent;
        }
        set
        {
          this.m_owner.m_inviteListPanelSociatyListScrollContent = value;
        }
      }

      public Button m_inviteListPanelPowerButton
      {
        get
        {
          return this.m_owner.m_inviteListPanelPowerButton;
        }
        set
        {
          this.m_owner.m_inviteListPanelPowerButton = value;
        }
      }

      public CommonUIStateController m_inviteListPanelPowerButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_inviteListPanelPowerButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_inviteListPanelPowerButtonStateCtrl = value;
        }
      }

      public GameObject m_invitePlayerInfoItemPrefab
      {
        get
        {
          return this.m_owner.m_invitePlayerInfoItemPrefab;
        }
        set
        {
          this.m_owner.m_invitePlayerInfoItemPrefab = value;
        }
      }

      public CommonUIStateController m_quitDialogStateCtrl
      {
        get
        {
          return this.m_owner.m_quitDialogStateCtrl;
        }
        set
        {
          this.m_owner.m_quitDialogStateCtrl = value;
        }
      }

      public Text m_quitDialogTitleText
      {
        get
        {
          return this.m_owner.m_quitDialogTitleText;
        }
        set
        {
          this.m_owner.m_quitDialogTitleText = value;
        }
      }

      public Text m_quitDialogTipText
      {
        get
        {
          return this.m_owner.m_quitDialogTipText;
        }
        set
        {
          this.m_owner.m_quitDialogTipText = value;
        }
      }

      public Button m_quitDialogConfirmButton
      {
        get
        {
          return this.m_owner.m_quitDialogConfirmButton;
        }
        set
        {
          this.m_owner.m_quitDialogConfirmButton = value;
        }
      }

      public Button m_quitDialogCancelButton
      {
        get
        {
          return this.m_owner.m_quitDialogCancelButton;
        }
        set
        {
          this.m_owner.m_quitDialogCancelButton = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public static string StateName_Up
      {
        get
        {
          return "Up";
        }
      }

      public static string StateName_Down
      {
        get
        {
          return "Down";
        }
      }

      public static string StateName_Hide
      {
        get
        {
          return "Hide";
        }
      }

      public bool m_isManageListPowerAscend
      {
        get
        {
          return this.m_owner.m_isManageListPowerAscend;
        }
        set
        {
          this.m_owner.m_isManageListPowerAscend = value;
        }
      }

      public bool m_isManageListActiveAscend
      {
        get
        {
          return this.m_owner.m_isManageListActiveAscend;
        }
        set
        {
          this.m_owner.m_isManageListActiveAscend = value;
        }
      }

      public bool m_isMessageApplyListPowerAscend
      {
        get
        {
          return this.m_owner.m_isMessageApplyListPowerAscend;
        }
        set
        {
          this.m_owner.m_isMessageApplyListPowerAscend = value;
        }
      }

      public bool m_isInviteListPanelPowerAscend
      {
        get
        {
          return this.m_owner.m_isInviteListPanelPowerAscend;
        }
        set
        {
          this.m_owner.m_isInviteListPanelPowerAscend = value;
        }
      }

      public bool m_isMessageApplyPanelSortByPower
      {
        get
        {
          return this.m_owner.m_isMessageApplyPanelSortByPower;
        }
        set
        {
          this.m_owner.m_isMessageApplyPanelSortByPower = value;
        }
      }

      public List<GuildMemberInfoItemUIController> m_playerInfoCtrlList
      {
        get
        {
          return this.m_owner.m_playerInfoCtrlList;
        }
        set
        {
          this.m_owner.m_playerInfoCtrlList = value;
        }
      }

      public List<GuildApplyMemberInfoItemUIController> m_applyPlayerInfoCtrlList
      {
        get
        {
          return this.m_owner.m_applyPlayerInfoCtrlList;
        }
        set
        {
          this.m_owner.m_applyPlayerInfoCtrlList = value;
        }
      }

      public List<GuildJournalItemUIController> m_guildJournalItemCtrlList
      {
        get
        {
          return this.m_owner.m_guildJournalItemCtrlList;
        }
        set
        {
          this.m_owner.m_guildJournalItemCtrlList = value;
        }
      }

      public List<GuildInviteMemberInfoItemUIController> m_inviteMemberInfoCtrlList
      {
        get
        {
          return this.m_owner.m_inviteMemberInfoCtrlList;
        }
        set
        {
          this.m_owner.m_inviteMemberInfoCtrlList = value;
        }
      }

      public Guild m_guild
      {
        get
        {
          return this.m_owner.m_guild;
        }
        set
        {
          this.m_owner.m_guild = value;
        }
      }

      public GuildTitle m_guildTitle
      {
        get
        {
          return this.m_owner.m_guildTitle;
        }
        set
        {
          this.m_owner.m_guildTitle = value;
        }
      }

      public List<UserSummary> m_canInvitePlayerList
      {
        get
        {
          return this.m_owner.m_canInvitePlayerList;
        }
        set
        {
          this.m_owner.m_canInvitePlayerList = value;
        }
      }

      public bool m_isGuildAutoJoin
      {
        get
        {
          return this.m_owner.m_isGuildAutoJoin;
        }
        set
        {
          this.m_owner.m_isGuildAutoJoin = value;
        }
      }

      public int m_guildJoinLevel
      {
        get
        {
          return this.m_owner.m_guildJoinLevel;
        }
        set
        {
          this.m_owner.m_guildJoinLevel = value;
        }
      }

      public List<UserSummary> m_messageApplyUserSummarys
      {
        get
        {
          return this.m_owner.m_messageApplyUserSummarys;
        }
        set
        {
          this.m_owner.m_messageApplyUserSummarys = value;
        }
      }

      public GuildManagementUIController.GuildListSortType m_guildListSortType
      {
        get
        {
          return this.m_owner.m_guildListSortType;
        }
        set
        {
          this.m_owner.m_guildListSortType = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void RefreshGuildTitle()
      {
        this.m_owner.RefreshGuildTitle();
      }

      public void SetStateByGuildTitle()
      {
        this.m_owner.SetStateByGuildTitle();
      }

      public void SetSociatyInfoPanel()
      {
        this.m_owner.SetSociatyInfoPanel();
      }

      public void SetSociatyListPanel(List<GuildMemberCacheObject> memberList)
      {
        this.m_owner.SetSociatyListPanel(memberList);
      }

      public void OnGuildManagementListPlayeItemClick(GuildMemberInfoItemUIController ctrl)
      {
        this.m_owner.OnGuildManagementListPlayeItemClick(ctrl);
      }

      public void OnReturnImgButtonClick()
      {
        this.m_owner.OnReturnImgButtonClick();
      }

      public void OnInfoButtonClick()
      {
        this.m_owner.OnInfoButtonClick();
      }

      public void OnGuildCoinButtonClick()
      {
        this.m_owner.OnGuildCoinButtonClick();
      }

      public void ClearData()
      {
        this.m_owner.ClearData();
      }

      public void CloseAllSmallPanel()
      {
        this.m_owner.CloseAllSmallPanel();
      }

      public void OnOnlineButtonClick()
      {
        this.m_owner.OnOnlineButtonClick();
      }

      public void OnActiveButtonClick()
      {
        this.m_owner.OnActiveButtonClick();
      }

      public void OnPowerButtonClick()
      {
        this.m_owner.OnPowerButtonClick();
      }

      public void SetSortButtonState(
        GuildManagementUIController.GuildListSortType sortType)
      {
        this.m_owner.SetSortButtonState(sortType);
      }

      public int SortByTitle(GuildMemberCacheObject a, GuildMemberCacheObject b)
      {
        return this.m_owner.SortByTitle(a, b);
      }

      public int SortByPower(GuildMemberCacheObject a, GuildMemberCacheObject b)
      {
        return this.m_owner.SortByPower(a, b);
      }

      public int SortByActive(GuildMemberCacheObject a, GuildMemberCacheObject b)
      {
        return this.m_owner.SortByActive(a, b);
      }

      public void SortGuildListByDefault(List<GuildMemberCacheObject> guildList)
      {
        this.m_owner.SortGuildListByDefault(guildList);
      }

      public int SortByDefaultType(GuildMemberCacheObject a, GuildMemberCacheObject b)
      {
        return this.m_owner.SortByDefaultType(a, b);
      }

      public int SortByActiveType(GuildMemberCacheObject a, GuildMemberCacheObject b)
      {
        return this.m_owner.SortByActiveType(a, b);
      }

      public int SortByBattlePowerType(GuildMemberCacheObject a, GuildMemberCacheObject b)
      {
        return this.m_owner.SortByBattlePowerType(a, b);
      }

      public void OnSociatyNameChangeButtonClick()
      {
        this.m_owner.OnSociatyNameChangeButtonClick();
      }

      public void OnGuildChangeNamePanelChangeNameButtonClick()
      {
        this.m_owner.OnGuildChangeNamePanelChangeNameButtonClick();
      }

      public void OnGuildChangeNamePanelBackButtonClick()
      {
        this.m_owner.OnGuildChangeNamePanelBackButtonClick();
      }

      public void OnDeclarationButtonClick()
      {
        this.m_owner.OnDeclarationButtonClick();
      }

      public void OnDeclarationChangePanelSaveButtonClick()
      {
        this.m_owner.OnDeclarationChangePanelSaveButtonClick();
      }

      public void OnDeclarationChangePanelBackButtonClick()
      {
        this.m_owner.OnDeclarationChangePanelBackButtonClick();
      }

      public void OnQuitButtonClick()
      {
        this.m_owner.OnQuitButtonClick();
      }

      public void OnQuitGuildConfirmButtonClick()
      {
        this.m_owner.OnQuitGuildConfirmButtonClick();
      }

      public void CloseQuitGuildConfirmPanel()
      {
        this.m_owner.CloseQuitGuildConfirmPanel();
      }

      public void OnSociatyPlayButtonClick()
      {
        this.m_owner.OnSociatyPlayButtonClick();
      }

      public void OnShopButtonClick()
      {
        this.m_owner.OnShopButtonClick();
      }

      public void OnWealButtonClick()
      {
        this.m_owner.OnWealButtonClick();
      }

      public void OnMessageButtonClick()
      {
        this.m_owner.OnMessageButtonClick();
      }

      public void SendNtfBeforeOpenMessagePanel(bool isManager, Action onEnd)
      {
        this.m_owner.SendNtfBeforeOpenMessagePanel(isManager, onEnd);
      }

      public void SetMessageApplyPanel(List<UserSummary> players)
      {
        this.m_owner.SetMessageApplyPanel(players);
      }

      public void OnGuildApplyMemberAcceptResult(
        GuildApplyMemberInfoItemUIController ctrl,
        bool result)
      {
        this.m_owner.OnGuildApplyMemberAcceptResult(ctrl, result);
      }

      public void SetMessageJournalPanel(List<GuildLog> journal)
      {
        this.m_owner.SetMessageJournalPanel(journal);
      }

      public void OnMssageApplyPanelPowerButtonClick()
      {
        this.m_owner.OnMssageApplyPanelPowerButtonClick();
      }

      public void OnMessagePanelBackButtonClick()
      {
        this.m_owner.OnMessagePanelBackButtonClick();
      }

      public void CloseMessagePanel(Action onEnd)
      {
        this.m_owner.CloseMessagePanel(onEnd);
      }

      public void OnMessagePanelApplyToggleValueChanged(bool isOn)
      {
        this.m_owner.OnMessagePanelApplyToggleValueChanged(isOn);
      }

      public void OnMessagePanelJournalToggleValueChanged(bool isOn)
      {
        this.m_owner.OnMessagePanelJournalToggleValueChanged(isOn);
      }

      public void OnMessageApplyPanelAllRefuseButtonClick()
      {
        this.m_owner.OnMessageApplyPanelAllRefuseButtonClick();
      }

      public void OnInviteButtonClick()
      {
        this.m_owner.OnInviteButtonClick();
      }

      public void SetInvitePanel(List<UserSummary> playerList)
      {
        this.m_owner.SetInvitePanel(playerList);
      }

      public void SetInviteInfoPanel()
      {
        this.m_owner.SetInviteInfoPanel();
      }

      public void SetInviteListPanel()
      {
        this.m_owner.SetInviteListPanel();
      }

      public void OnInviteMemberInfoItemClick(GuildInviteMemberInfoItemUIController ctrl)
      {
        this.m_owner.OnInviteMemberInfoItemClick(ctrl);
      }

      public void OnInviteListPanelPowerButtonClick()
      {
        this.m_owner.OnInviteListPanelPowerButtonClick();
      }

      public void OnInvitePanelChangeListButtonClick()
      {
        this.m_owner.OnInvitePanelChangeListButtonClick();
      }

      public void OnInvitePanelBackButton()
      {
        this.m_owner.OnInvitePanelBackButton();
      }

      public void CloseInvitePanel(Action onEnd)
      {
        this.m_owner.CloseInvitePanel(onEnd);
      }

      public void OnInviteInfoPanelDeclarationChangeButtonClick()
      {
        this.m_owner.OnInviteInfoPanelDeclarationChangeButtonClick();
      }

      public void SetRecruitChangeSubPanelInfo()
      {
        this.m_owner.SetRecruitChangeSubPanelInfo();
      }

      public void OnRecruitPanelSaveButtonClick()
      {
        this.m_owner.OnRecruitPanelSaveButtonClick();
      }

      public void OnRecruitPanelBackButtonClick()
      {
        this.m_owner.OnRecruitPanelBackButtonClick();
      }

      public void OnInvitePanelSetButtonClick()
      {
        this.m_owner.OnInvitePanelSetButtonClick();
      }

      public void SetGuildSetSubPanel()
      {
        this.m_owner.SetGuildSetSubPanel();
      }

      public void OnGuildSetPanelChairmanToggleClick(bool isOn)
      {
        this.m_owner.OnGuildSetPanelChairmanToggleClick(isOn);
      }

      public void OnGuildSetPanelAutoToggleClick(bool isOn)
      {
        this.m_owner.OnGuildSetPanelAutoToggleClick(isOn);
      }

      public void OnGuildSetPanelLevelInputFieldRightButtonClick()
      {
        this.m_owner.OnGuildSetPanelLevelInputFieldRightButtonClick();
      }

      public void OnGuildSetPanelLevelInputFieldLeftButtonClick()
      {
        this.m_owner.OnGuildSetPanelLevelInputFieldLeftButtonClick();
      }

      public void OnGuildSetPanelLevelInputFieldEditEnd(string inputStr)
      {
        this.m_owner.OnGuildSetPanelLevelInputFieldEditEnd(inputStr);
      }

      public void OnGuildSetPanelSaveButtonClick()
      {
        this.m_owner.OnGuildSetPanelSaveButtonClick();
      }

      public void OnGuildSetPanelBackButtonClick()
      {
        this.m_owner.OnGuildSetPanelBackButtonClick();
      }

      public void CloseGuildSetPanel(Action onEnd)
      {
        this.m_owner.CloseGuildSetPanel(onEnd);
      }
    }
  }
}
