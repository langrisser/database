﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ActivityNoticeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Misc;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ActivityNoticeUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./LayoutRoot/LampGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lampGroupObj;
    [AutoBind("./LayoutRoot/TopImageGroup/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bigActiveScrollViewContentObject;
    [AutoBind("./LayoutRoot/List/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_smallActiveScrollViewContentObject;
    [AutoBind("./Prefabs/Lamp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lampPrefabObj;
    [AutoBind("./Prefabs/BigActive", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bigActivePrefabObj;
    [AutoBind("./Prefabs/SmallActive", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_smallActivePrefabObj;
    [AutoBind("./LayoutRoot/Closebutton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./LayoutRoot/TopImageGroup", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_bigItemScrollRect;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgCloseButton;
    private GameObjectPool<ActivityNoticeBigItemUIController> m_bigItemPool;
    private GameObjectPool<ActivityNoticeSmallItemUIController> m_smallItemPool;
    private GameObjectPool m_lampItemPool;
    private List<CommonUIStateController> m_lampUIStateCtrlList;
    private ScrollSnapCenter m_bigItemScrollSnapCenter;
    private int m_curBigItemIndex;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SignOpenTween()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateActivityList(List<ActivityNoticeInfo> activityList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshLampActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActivityItemClick(int activityId, string linkPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, string> EventOnActivityClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCloseButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
