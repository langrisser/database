﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftLevelUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class RiftLevelUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ChapterBG", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chapterBGObject;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./WorldButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_worldButton;
    [AutoBind("./Hard", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_hardUIStateController;
    [AutoBind("./Hard/SwitchButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_hardButton;
    [AutoBind("./Hard/SwitchButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_hardButtonUIStateController;
    [AutoBind("./Margin/StarReward/Button1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starRewardButton1GameObject;
    [AutoBind("./Margin/StarReward/Button2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starRewardButton2GameObject;
    [AutoBind("./Margin/StarReward/Button3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starRewardButton3GameObject;
    [AutoBind("./Margin/StarReward/Got/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_starRewardGotText;
    [AutoBind("./Margin/StarReward/ProgressBar/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_progressBarImage;
    [AutoBind("./StarRewardPreview", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_starRewardPreviewUIStateController;
    [AutoBind("./StarRewardPreview/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_starRewardPreviewBackgroundButton;
    [AutoBind("./StarRewardPreview/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_starRewardPreviewScrollRect;
    [AutoBind("./UnlockCondition", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockConditionUIStateController;
    [AutoBind("./UnlockCondition/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockConditionBackgroundButton;
    [AutoBind("./UnlockCondition/Panel/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unlockConditionGroupGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/RiftLevelUnlockConditionItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_riftLevelUnlockConditionItemPrefab;
    [AutoBind("./Prefabs/RiftLevelButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_riftLevelButtonPrefab;
    [AutoBind("./Prefabs/EventRiftLevelButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_eventRiftLevelButtonPrefab;
    [AutoBind("./Prefabs/ElitePoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_eventRiftLevelDummyPrefab;
    private List<RiftLevelButton> m_riftLevelButtons;
    private List<EventRiftLevelButton> m_eventRiftLevelButtons;
    private List<RiftLevelUnlockConditionItemUIController> m_riftLevelUnlockConditionItems;
    private GameObject m_createChapterBGObject;
    private GainRewardButton[] m_starRewardButtons;
    private ConfigDataRiftChapterInfo m_chapterInfo;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private RiftLevelUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_SetChapterConfigDataRiftChapterInfoBoolean_hotfix;
    private LuaFunction m_SetStarRewardStatusInt32GainRewardStatus_hotfix;
    private LuaFunction m_SetStarRewardProressInt32Int32_hotfix;
    private LuaFunction m_ClearRiftLevelButtons_hotfix;
    private LuaFunction m_ClearEventRiftLevelButtons_hotfix;
    private LuaFunction m_AddRiftLevelButtonConfigDataRiftLevelInfoRiftLevelStatusInt32Int32BooleanInt32Int32Int32Int32_hotfix;
    private LuaFunction m_AddEventRiftLevelButtonConfigDataRiftLevelInfoRiftLevelStatus_hotfix;
    private LuaFunction m_AddEventRiftLevelDummys_hotfix;
    private LuaFunction m_ClearRiftLevelUnlockContitionItems_hotfix;
    private LuaFunction m_AddRiftLevelUnlockContitionItemsConfigDataRiftLevelInfo_hotfix;
    private LuaFunction m_GoToRiftLevelInt32Int32_hotfix;
    private LuaFunction m_Co_ShowRiftLevelConfigDataRiftChapterInfoConfigDataRiftLevelInfo_hotfix;
    private LuaFunction m_AddStarRewardPreviewGoodsConfigDataRiftChapterInfoInt32_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnWorldButtonClick_hotfix;
    private LuaFunction m_OnHardButtonClick_hotfix;
    private LuaFunction m_OnStarRewardButtonClickGainRewardButton_hotfix;
    private LuaFunction m_OnRiftLevelButtonClickRiftLevelButton_hotfix;
    private LuaFunction m_OnEventRiftLevelButtonClickEventRiftLevelButton_hotfix;
    private LuaFunction m_OnUnlockConditionBackgroundButtonClick_hotfix;
    private LuaFunction m_OnStarRewardPreviewBackgroundButtonClick_hotfix;
    private LuaFunction m_SetUnlockConditionClose_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnReturnToWorldAction_hotfix;
    private LuaFunction m_remove_EventOnReturnToWorldAction_hotfix;
    private LuaFunction m_add_EventOnChangeHardAction_hotfix;
    private LuaFunction m_remove_EventOnChangeHardAction_hotfix;
    private LuaFunction m_add_EventOnSelectRiftLevelAction`2_hotfix;
    private LuaFunction m_remove_EventOnSelectRiftLevelAction`2_hotfix;
    private LuaFunction m_add_EventOnGetStarRewardAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetStarRewardAction`1_hotfix;
    private LuaFunction m_add_EventOnGoToScenarioAction`1_hotfix;
    private LuaFunction m_remove_EventOnGoToScenarioAction`1_hotfix;
    private LuaFunction m_add_EventOnSelectChapterAction`1_hotfix;
    private LuaFunction m_remove_EventOnSelectChapterAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private RiftLevelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChapter(ConfigDataRiftChapterInfo chapterInfo, bool canSwitchHard)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStarRewardStatus(int idx, GainRewardStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStarRewardProress(int star, int allStar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearRiftLevelButtons()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearEventRiftLevelButtons()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRiftLevelButton(
      ConfigDataRiftLevelInfo riftLevelInfo,
      RiftLevelStatus status,
      int challengeCount,
      int challengeCountMax,
      bool challenged,
      int star,
      int achievementCount,
      int treasureCount,
      int treasureCountMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddEventRiftLevelButton(
      ConfigDataRiftLevelInfo riftLevelInfo,
      RiftLevelStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddEventRiftLevelDummys()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearRiftLevelUnlockContitionItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddRiftLevelUnlockContitionItems(ConfigDataRiftLevelInfo riftLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoToRiftLevel(int chapterID, int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowRiftLevel(
      ConfigDataRiftChapterInfo riftChapterInfo,
      ConfigDataRiftLevelInfo riftLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddStarRewardPreviewGoods(ConfigDataRiftChapterInfo chapterInfo, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWorldButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStarRewardButtonClick(GainRewardButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnRiftLevelButtonClick(RiftLevelButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEventRiftLevelButtonClick(EventRiftLevelButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockConditionBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStarRewardPreviewBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUnlockConditionClose()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturnToWorld
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnChangeHard
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataRiftLevelInfo, NeedGoods> EventOnSelectRiftLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGetStarReward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGoToScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataRiftChapterInfo> EventOnSelectChapter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public RiftLevelUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturnToWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturnToWorld()
    {
      this.EventOnReturnToWorld = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangeHard()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChangeHard()
    {
      this.EventOnChangeHard = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSelectRiftLevel(ConfigDataRiftLevelInfo arg1, NeedGoods arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSelectRiftLevel(ConfigDataRiftLevelInfo arg1, NeedGoods arg2)
    {
      this.EventOnSelectRiftLevel = (Action<ConfigDataRiftLevelInfo, NeedGoods>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetStarReward(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetStarReward(int obj)
    {
      this.EventOnGetStarReward = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGoToScenario(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGoToScenario(int obj)
    {
      this.EventOnGoToScenario = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSelectChapter(ConfigDataRiftChapterInfo obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSelectChapter(ConfigDataRiftChapterInfo obj)
    {
      this.EventOnSelectChapter = (Action<ConfigDataRiftChapterInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private RiftLevelUIController m_owner;

      public LuaExportHelper(RiftLevelUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnReturnToWorld()
      {
        this.m_owner.__callDele_EventOnReturnToWorld();
      }

      public void __clearDele_EventOnReturnToWorld()
      {
        this.m_owner.__clearDele_EventOnReturnToWorld();
      }

      public void __callDele_EventOnChangeHard()
      {
        this.m_owner.__callDele_EventOnChangeHard();
      }

      public void __clearDele_EventOnChangeHard()
      {
        this.m_owner.__clearDele_EventOnChangeHard();
      }

      public void __callDele_EventOnSelectRiftLevel(ConfigDataRiftLevelInfo arg1, NeedGoods arg2)
      {
        this.m_owner.__callDele_EventOnSelectRiftLevel(arg1, arg2);
      }

      public void __clearDele_EventOnSelectRiftLevel(ConfigDataRiftLevelInfo arg1, NeedGoods arg2)
      {
        this.m_owner.__clearDele_EventOnSelectRiftLevel(arg1, arg2);
      }

      public void __callDele_EventOnGetStarReward(int obj)
      {
        this.m_owner.__callDele_EventOnGetStarReward(obj);
      }

      public void __clearDele_EventOnGetStarReward(int obj)
      {
        this.m_owner.__clearDele_EventOnGetStarReward(obj);
      }

      public void __callDele_EventOnGoToScenario(int obj)
      {
        this.m_owner.__callDele_EventOnGoToScenario(obj);
      }

      public void __clearDele_EventOnGoToScenario(int obj)
      {
        this.m_owner.__clearDele_EventOnGoToScenario(obj);
      }

      public void __callDele_EventOnSelectChapter(ConfigDataRiftChapterInfo obj)
      {
        this.m_owner.__callDele_EventOnSelectChapter(obj);
      }

      public void __clearDele_EventOnSelectChapter(ConfigDataRiftChapterInfo obj)
      {
        this.m_owner.__clearDele_EventOnSelectChapter(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public GameObject m_chapterBGObject
      {
        get
        {
          return this.m_owner.m_chapterBGObject;
        }
        set
        {
          this.m_owner.m_chapterBGObject = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Button m_worldButton
      {
        get
        {
          return this.m_owner.m_worldButton;
        }
        set
        {
          this.m_owner.m_worldButton = value;
        }
      }

      public CommonUIStateController m_hardUIStateController
      {
        get
        {
          return this.m_owner.m_hardUIStateController;
        }
        set
        {
          this.m_owner.m_hardUIStateController = value;
        }
      }

      public Button m_hardButton
      {
        get
        {
          return this.m_owner.m_hardButton;
        }
        set
        {
          this.m_owner.m_hardButton = value;
        }
      }

      public CommonUIStateController m_hardButtonUIStateController
      {
        get
        {
          return this.m_owner.m_hardButtonUIStateController;
        }
        set
        {
          this.m_owner.m_hardButtonUIStateController = value;
        }
      }

      public GameObject m_starRewardButton1GameObject
      {
        get
        {
          return this.m_owner.m_starRewardButton1GameObject;
        }
        set
        {
          this.m_owner.m_starRewardButton1GameObject = value;
        }
      }

      public GameObject m_starRewardButton2GameObject
      {
        get
        {
          return this.m_owner.m_starRewardButton2GameObject;
        }
        set
        {
          this.m_owner.m_starRewardButton2GameObject = value;
        }
      }

      public GameObject m_starRewardButton3GameObject
      {
        get
        {
          return this.m_owner.m_starRewardButton3GameObject;
        }
        set
        {
          this.m_owner.m_starRewardButton3GameObject = value;
        }
      }

      public Text m_starRewardGotText
      {
        get
        {
          return this.m_owner.m_starRewardGotText;
        }
        set
        {
          this.m_owner.m_starRewardGotText = value;
        }
      }

      public Image m_progressBarImage
      {
        get
        {
          return this.m_owner.m_progressBarImage;
        }
        set
        {
          this.m_owner.m_progressBarImage = value;
        }
      }

      public CommonUIStateController m_starRewardPreviewUIStateController
      {
        get
        {
          return this.m_owner.m_starRewardPreviewUIStateController;
        }
        set
        {
          this.m_owner.m_starRewardPreviewUIStateController = value;
        }
      }

      public Button m_starRewardPreviewBackgroundButton
      {
        get
        {
          return this.m_owner.m_starRewardPreviewBackgroundButton;
        }
        set
        {
          this.m_owner.m_starRewardPreviewBackgroundButton = value;
        }
      }

      public ScrollRect m_starRewardPreviewScrollRect
      {
        get
        {
          return this.m_owner.m_starRewardPreviewScrollRect;
        }
        set
        {
          this.m_owner.m_starRewardPreviewScrollRect = value;
        }
      }

      public CommonUIStateController m_unlockConditionUIStateController
      {
        get
        {
          return this.m_owner.m_unlockConditionUIStateController;
        }
        set
        {
          this.m_owner.m_unlockConditionUIStateController = value;
        }
      }

      public Button m_unlockConditionBackgroundButton
      {
        get
        {
          return this.m_owner.m_unlockConditionBackgroundButton;
        }
        set
        {
          this.m_owner.m_unlockConditionBackgroundButton = value;
        }
      }

      public GameObject m_unlockConditionGroupGameObject
      {
        get
        {
          return this.m_owner.m_unlockConditionGroupGameObject;
        }
        set
        {
          this.m_owner.m_unlockConditionGroupGameObject = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_riftLevelUnlockConditionItemPrefab
      {
        get
        {
          return this.m_owner.m_riftLevelUnlockConditionItemPrefab;
        }
        set
        {
          this.m_owner.m_riftLevelUnlockConditionItemPrefab = value;
        }
      }

      public GameObject m_riftLevelButtonPrefab
      {
        get
        {
          return this.m_owner.m_riftLevelButtonPrefab;
        }
        set
        {
          this.m_owner.m_riftLevelButtonPrefab = value;
        }
      }

      public GameObject m_eventRiftLevelButtonPrefab
      {
        get
        {
          return this.m_owner.m_eventRiftLevelButtonPrefab;
        }
        set
        {
          this.m_owner.m_eventRiftLevelButtonPrefab = value;
        }
      }

      public GameObject m_eventRiftLevelDummyPrefab
      {
        get
        {
          return this.m_owner.m_eventRiftLevelDummyPrefab;
        }
        set
        {
          this.m_owner.m_eventRiftLevelDummyPrefab = value;
        }
      }

      public List<RiftLevelButton> m_riftLevelButtons
      {
        get
        {
          return this.m_owner.m_riftLevelButtons;
        }
        set
        {
          this.m_owner.m_riftLevelButtons = value;
        }
      }

      public List<EventRiftLevelButton> m_eventRiftLevelButtons
      {
        get
        {
          return this.m_owner.m_eventRiftLevelButtons;
        }
        set
        {
          this.m_owner.m_eventRiftLevelButtons = value;
        }
      }

      public List<RiftLevelUnlockConditionItemUIController> m_riftLevelUnlockConditionItems
      {
        get
        {
          return this.m_owner.m_riftLevelUnlockConditionItems;
        }
        set
        {
          this.m_owner.m_riftLevelUnlockConditionItems = value;
        }
      }

      public GameObject m_createChapterBGObject
      {
        get
        {
          return this.m_owner.m_createChapterBGObject;
        }
        set
        {
          this.m_owner.m_createChapterBGObject = value;
        }
      }

      public GainRewardButton[] m_starRewardButtons
      {
        get
        {
          return this.m_owner.m_starRewardButtons;
        }
        set
        {
          this.m_owner.m_starRewardButtons = value;
        }
      }

      public ConfigDataRiftChapterInfo m_chapterInfo
      {
        get
        {
          return this.m_owner.m_chapterInfo;
        }
        set
        {
          this.m_owner.m_chapterInfo = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ClearRiftLevelUnlockContitionItems()
      {
        this.m_owner.ClearRiftLevelUnlockContitionItems();
      }

      public void AddRiftLevelUnlockContitionItems(ConfigDataRiftLevelInfo riftLevelInfo)
      {
        this.m_owner.AddRiftLevelUnlockContitionItems(riftLevelInfo);
      }

      public void GoToRiftLevel(int chapterID, int levelID)
      {
        this.m_owner.GoToRiftLevel(chapterID, levelID);
      }

      public IEnumerator Co_ShowRiftLevel(
        ConfigDataRiftChapterInfo riftChapterInfo,
        ConfigDataRiftLevelInfo riftLevelInfo)
      {
        return this.m_owner.Co_ShowRiftLevel(riftChapterInfo, riftLevelInfo);
      }

      public void AddStarRewardPreviewGoods(ConfigDataRiftChapterInfo chapterInfo, int idx)
      {
        this.m_owner.AddStarRewardPreviewGoods(chapterInfo, idx);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnWorldButtonClick()
      {
        this.m_owner.OnWorldButtonClick();
      }

      public void OnHardButtonClick()
      {
        this.m_owner.OnHardButtonClick();
      }

      public void OnStarRewardButtonClick(GainRewardButton b)
      {
        this.m_owner.OnStarRewardButtonClick(b);
      }

      public void OnEventRiftLevelButtonClick(EventRiftLevelButton b)
      {
        this.m_owner.OnEventRiftLevelButtonClick(b);
      }

      public void OnUnlockConditionBackgroundButtonClick()
      {
        this.m_owner.OnUnlockConditionBackgroundButtonClick();
      }

      public void OnStarRewardPreviewBackgroundButtonClick()
      {
        this.m_owner.OnStarRewardPreviewBackgroundButtonClick();
      }
    }
  }
}
