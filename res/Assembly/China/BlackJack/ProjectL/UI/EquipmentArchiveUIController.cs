﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentArchiveUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class EquipmentArchiveUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prefabAnimation;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./EquipmentInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentInfoPanel;
    [AutoBind("./EquipmentInfoPanel/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getButton;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/All", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_aLLItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Weapon", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_weaponItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Armour", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_armorItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Helmet", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_helmetItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Ornament", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_ornamentItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Other", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_otherItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_filterText;
    [AutoBind("./EquipmentList/EquipmentFilter/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipmentFilterButton;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_filterPanel;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/BGImages/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterBGButton;
    [AutoBind("./EquipmentList/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private InfinityGridLayoutGroup m_itemContentInfinityGrid;
    [AutoBind("./EquipmentList/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemContent;
    [AutoBind("./Prefab/ItemPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemPrefab;
    [AutoBind("./CountLimit/CountGroup/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currentItemCountText;
    [AutoBind("./CountLimit/CountGroup/MaxText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_maxItemCountText;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_selectBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_allBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_armorBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_helmetBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_ornamentBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_weaponBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_otherBagItemList;
    private ArchiveUITask m_task;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private List<ArchiveItemUIController> m_itemUIControllerList;
    private EquipmentArchiveUIController.EquipmentInfoWrap m_selectEquipmentInfoWrap;
    private ArchiveItemDetailInfoController m_itemDetailInfoController;
    private int m_allOwnItemCount;
    private int m_armorOwnItemCount;
    private int m_helmetOwnItemCount;
    private int m_ornamentOwnItemCount;
    private int m_weaponOwnItemCount;
    private int m_otherOwnItemCount;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentArchiveUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetTask(ArchiveUITask task)
    {
      this.m_task = task;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnterController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ItemListCompare(
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentWrap1,
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentWrap2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshItem(ArchiveItemUIController archiveItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshItemCount(int currentCount, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateItemList(
      List<EquipmentArchiveUIController.EquipmentInfoWrap> equipmentInfoWrapList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyItemBagList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshItemBagWithHeroData(
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentInfoWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnItemClick(ArchiveItemUIController archiveItemUIController)
    {
      this.RefreshItem(archiveItemUIController);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentFilterSwitchClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnEquipmentFilterClick()
    {
      this.m_filterPanel.SetActive(true);
    }

    private void OnFilterBGClick()
    {
      this.m_filterPanel.SetActive(false);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetItemPathClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChildrenCallbackDelegate(int index, Transform trans)
    {
      // ISSUE: unable to decompile the method.
    }

    [CustomLuaClass]
    public class EquipmentInfoWrap
    {
      public ConfigDataEquipmentInfo equipmentInfo;
      public bool isUnlocked;
      public bool isSelect;
    }
  }
}
