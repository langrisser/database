﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BuyEnergyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BuyEnergyUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/MoneyBuy/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyCountText;
    [AutoBind("./Panel/MoneyBuy/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buyButton;
    [AutoBind("./Panel/MoneyBuy/BuyButton/Price/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_priceText;
    [AutoBind("./Panel/MoneyBuy/BuyButton/GrayImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_buyGrayImage;
    [AutoBind("./Panel/PropBuy/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyCountMedicineText;
    [AutoBind("./Panel/PropBuy/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_useMedicineButton;
    [AutoBind("./Panel/PropBuy/BuyButton/Price/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_useMedicineCountText;
    [AutoBind("./Panel/PropBuy/ResidueValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_redidueMedicineCountText;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./EnergyNotEnoughTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_energyNotEnoughTipStateCtrl;
    [AutoBind("./EnergyNotEnoughTips/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyNotEnoughTipText;

    private BuyEnergyUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEnergy(int energyCount, int price, bool IsBoughtNumsUsedOut)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMedicine(int energyCount, int useMedicineCount, int redidueMedicineCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnergyNotEnoughTip(string s)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseEnergyMedicineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnUseEnergyMedicine
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBuyEnergy
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
