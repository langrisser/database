﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MissionUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using MarchingBytes;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class MissionUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Margin/FilterToggles/DayMission", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_dayMissionToggle;
    [AutoBind("./Margin/FilterToggles/DayMission/Click/RedTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dayMissionToggleClickTag;
    [AutoBind("./Margin/FilterToggles/DayMission/Click/RedTag/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dayMissionToggleClickTagNum;
    [AutoBind("./Margin/FilterToggles/DayMission/UnClick/RedTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dayMissionToggleUnClickTag;
    [AutoBind("./Margin/FilterToggles/DayMission/UnClick/RedTag/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dayMissionToggleUnClickTagNum;
    [AutoBind("./Margin/FilterToggles/ChallengeMission", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_challengeMissionToggle;
    [AutoBind("./Margin/FilterToggles/ChallengeMission/Click/RedTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_challengeMissionToggleClickTag;
    [AutoBind("./Margin/FilterToggles/ChallengeMission/Click/RedTag/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeMissionToggleClickTagNum;
    [AutoBind("./Margin/FilterToggles/ChallengeMission/UnClick/RedTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_challengeMissionToggleUnClickTag;
    [AutoBind("./Margin/FilterToggles/ChallengeMission/UnClick/RedTag/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeMissionToggleUnClickTagNum;
    [AutoBind("./Margin/FilterToggles/Achievement", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_achievementToggle;
    [AutoBind("./Margin/FilterToggles/Achievement/Click/RedTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_achievementToggleClickTag;
    [AutoBind("./Margin/FilterToggles/Achievement/Click/RedTag/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementToggleClickTagNum;
    [AutoBind("./Margin/FilterToggles/Achievement/UnClick/RedTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_achievementToggleUnClickTag;
    [AutoBind("./Margin/FilterToggles/Achievement/UnClick/RedTag/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementToggleUnClickTagNum;
    [AutoBind("./MissionList", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_missionListScrollView;
    [AutoBind("./MissionList", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_missionListItemPool;
    [AutoBind("./Tips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_tipsUIStateController;
    [AutoBind("./Tips/Panel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tipsCancelButton;
    [AutoBind("./Tips/Panel/OkButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tipsConfirmButton;
    [AutoBind("./Prefab/MissionItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_missionItemPrefab;
    private List<Mission> m_cachedMissionList;
    private int m_cachedMissionProcessingStartIndex;
    private int m_cachedMissionFinishedStartIndex;
    private int m_dayCompleteNum;
    private int m_challengeCompleteNum;
    private int m_achievementCompleteNum;
    private MissionColumnType m_curMissionColumnType;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int m_oldPlayerLevel;
    private int m_missionID;
    private const string c_missionListItemPrefabName = "MissionListItemUIPrefab";

    [MethodImpl((MethodImplOptions) 32768)]
    public MissionUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitItemListPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnListItemFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMissionListByMissionColumnType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowToggleTagNumText(bool day, bool challenge, bool achievement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CompareMissionBySortId(Mission m1, Mission m2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddMissionColumnRedTagNum(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetRedTagNum()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetRewardButtonClick(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowRewards(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetRewardGoodsUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGotoLayerButtonClick(GetPathData pathInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTipsBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTipsConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDayMissionToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChallengeToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, Action<List<Goods>>> EventOnGetRewardButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GetPathData> EventOnGotoLayerButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
