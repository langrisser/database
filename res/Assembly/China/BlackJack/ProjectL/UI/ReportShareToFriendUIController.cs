﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ReportShareToFriendUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ReportShareToFriendUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail/ToggleGroup/Friend", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_friendToggle;
    [AutoBind("./Detail/ToggleGroup/Recently", AutoBindAttribute.InitState.Inactive, false)]
    private Toggle m_recentlyToggle;
    [AutoBind("./Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmButton;
    [AutoBind("./Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelButton;
    [AutoBind("./Detail/PlayerListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_playerListScrollView;
    [AutoBind("./Prefab/PlayerSmallListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendSmallItemPrefab;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private List<string> m_selectUserIdList;
    [DoNotToLua]
    private ReportShareToFriendUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_UpdateViewInReportShareToFriend_hotfix;
    private LuaFunction m_OnFriendoggleValueChangedBooleanString_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_Close_hotfix;
    private LuaFunction m_OnBGButtonClick_hotfix;
    private LuaFunction m_OnCancelButtonClick_hotfix;
    private LuaFunction m_OnConfirmButtonClick_hotfix;
    private LuaFunction m_OnRecentlyToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnFriendToggleValueChangedBoolean_hotfix;
    private LuaFunction m_ClearData_hotfix;
    private LuaFunction m_add_EventOnSendButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnSendButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ReportShareToFriendUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInReportShareToFriend()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendoggleValueChanged(bool isOn, string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecentlyToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearData()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<List<string>> EventOnSendButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ReportShareToFriendUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSendButtonClick(List<string> obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSendButtonClick(List<string> obj)
    {
      this.EventOnSendButtonClick = (Action<List<string>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ReportShareToFriendUIController m_owner;

      public LuaExportHelper(ReportShareToFriendUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnSendButtonClick(List<string> obj)
      {
        this.m_owner.__callDele_EventOnSendButtonClick(obj);
      }

      public void __clearDele_EventOnSendButtonClick(List<string> obj)
      {
        this.m_owner.__clearDele_EventOnSendButtonClick(obj);
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public CommonUIStateController m_stateCtrl
      {
        get
        {
          return this.m_owner.m_stateCtrl;
        }
        set
        {
          this.m_owner.m_stateCtrl = value;
        }
      }

      public Button m_bgButton
      {
        get
        {
          return this.m_owner.m_bgButton;
        }
        set
        {
          this.m_owner.m_bgButton = value;
        }
      }

      public Toggle m_friendToggle
      {
        get
        {
          return this.m_owner.m_friendToggle;
        }
        set
        {
          this.m_owner.m_friendToggle = value;
        }
      }

      public Toggle m_recentlyToggle
      {
        get
        {
          return this.m_owner.m_recentlyToggle;
        }
        set
        {
          this.m_owner.m_recentlyToggle = value;
        }
      }

      public Button m_confirmButton
      {
        get
        {
          return this.m_owner.m_confirmButton;
        }
        set
        {
          this.m_owner.m_confirmButton = value;
        }
      }

      public Button m_cancelButton
      {
        get
        {
          return this.m_owner.m_cancelButton;
        }
        set
        {
          this.m_owner.m_cancelButton = value;
        }
      }

      public ScrollRect m_playerListScrollView
      {
        get
        {
          return this.m_owner.m_playerListScrollView;
        }
        set
        {
          this.m_owner.m_playerListScrollView = value;
        }
      }

      public GameObject m_friendSmallItemPrefab
      {
        get
        {
          return this.m_owner.m_friendSmallItemPrefab;
        }
        set
        {
          this.m_owner.m_friendSmallItemPrefab = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public List<string> m_selectUserIdList
      {
        get
        {
          return this.m_owner.m_selectUserIdList;
        }
        set
        {
          this.m_owner.m_selectUserIdList = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnFriendoggleValueChanged(bool isOn, string userID)
      {
        this.m_owner.OnFriendoggleValueChanged(isOn, userID);
      }

      public void Close()
      {
        this.m_owner.Close();
      }

      public void OnBGButtonClick()
      {
        this.m_owner.OnBGButtonClick();
      }

      public void OnCancelButtonClick()
      {
        this.m_owner.OnCancelButtonClick();
      }

      public void OnConfirmButtonClick()
      {
        this.m_owner.OnConfirmButtonClick();
      }

      public void OnRecentlyToggleValueChanged(bool isOn)
      {
        this.m_owner.OnRecentlyToggleValueChanged(isOn);
      }

      public void OnFriendToggleValueChanged(bool isOn)
      {
        this.m_owner.OnFriendToggleValueChanged(isOn);
      }

      public void ClearData()
      {
        this.m_owner.ClearData();
      }
    }
  }
}
