﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ARHeroListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ARHeroListUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_arHeroListAnimation;
    [AutoBind("./ARHeroSelectPanel/HeroListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private InfinityGridLayoutGroup m_heroListInfinityGrid;
    [AutoBind("./ARHeroSelectPanel/SingleAndTeamPanel/BattleTypeButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_battleTypeButton;
    [AutoBind("./ARHeroSelectPanel/SingleAndTeamPanel/HeroDrawButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_heroDrawButton;
    [AutoBind("./ARHeroSelectPanel/SingleAndTeamPanel/TeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_teamShowButton;
    [AutoBind("./BlackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Prefab/HeroListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroListItemPrefab;
    private List<ARHeroListUIController.HeroWrap> m_heroWrapList;
    private List<ARHeroCardUIController> m_arHeroCardUIControllerList;
    private ARHeroListUIController.HeroWrap m_selectHeroWrap;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private UITask m_task;

    [MethodImpl((MethodImplOptions) 32768)]
    private ARHeroListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetUITask(UITask task)
    {
      this.m_task = task;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SelectHero(ARHeroListUIController.HeroWrap selectHeroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshSelectHeroShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBattleTypeShowClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroDrawShowClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnTeamShowClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroItemClick(ARHeroCardUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateInfinityHeroItemCallback(int index, Transform trans)
    {
      // ISSUE: unable to decompile the method.
    }

    public class HeroWrap
    {
      public Hero hero;
      public bool isSelect;
    }
  }
}
