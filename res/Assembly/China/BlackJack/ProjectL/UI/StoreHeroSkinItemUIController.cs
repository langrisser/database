﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreHeroSkinItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class StoreHeroSkinItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public Button m_storeItemButton;
    [AutoBind("./HeroIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_heroSkinIcon;
    [AutoBind("./HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_heroName;
    [AutoBind("./OtherNameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_skinName;
    [AutoBind("./PricePanel/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_priceText;
    [AutoBind("./Tape/TSurplusText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_restTime;
    [AutoBind("./TimeLimit", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_timeGo;
    [AutoBind("./TimeLimit/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_timeValueText;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_itemStateCtrl;
    public ConfigDataFixedStoreItemInfo m_storeItemConfig;
    public StoreType m_storeType;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreHeroSkinItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroSkinItemInfo(FixedStoreItem fixedStoreItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetItemState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLimitTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStoreItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<StoreHeroSkinItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSkinItemOutOfDate
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
