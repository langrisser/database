﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleTeamPlayerUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectLBasic;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BattleTeamPlayerUIController : UIControllerBase
  {
    [AutoBind("./PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIconImage;
    [AutoBind("./PlayerIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerTagImage;
    [AutoBind("./Chat", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_chatUIStateController;
    [AutoBind("./Chat/BGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private EmojiText m_dialogText;
    [AutoBind("./Chat/BGImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_expressionImage;
    [AutoBind("./Chat/BGImage/Voice", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_voiceButton;
    [AutoBind("./Chat/BGImage/Voice/Voice/SpeakImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_voiceSpeakImageStateCtrl;
    [AutoBind("./Chat/BGImage/Voice/Voice/TimeButton/ContentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_voiceTimeButtonText;
    [AutoBind("./Chat/BGImage/Voice/ContentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_voiceContentText;
    [AutoBind("./StateGroup/ActionImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusActionGameObject;
    [AutoBind("./StateGroup/ReadyImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusReadyGameObject;
    [AutoBind("./StateGroup/AutoImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusAutoGameObject;
    [AutoBind("./StateGroup/OfflineImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusOfflineGameObject;
    private float m_hideChatTime;
    private const int HeroPointCountMax = 5;
    private ChatVoiceMessage m_voiceMessage;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitEmojiText(SmallExpressionParseDesc expressionDesc, Image image)
    {
      this.m_dialogText.Init(expressionDesc, image);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStatus(PlayerBattleStatus status, bool isOffline)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetAction(bool isAction)
    {
      this.m_statusActionGameObject.SetActive(isAction);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeadIcon(int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetName(string name)
    {
      this.m_nameText.text = name;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerIndex(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerIndex(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroAlive(int heroIdx, bool isAlive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChat(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBigExpression(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoice(ChatVoiceMessage voiceMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnVoiceTimeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideBigExpression()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject GetHeroPointBGGameObject(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject GetHeroPointGameObject(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject GetHeroRedPointGameObject(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
