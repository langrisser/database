﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RankingListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using MarchingBytes;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RankingListUIController : UIControllerBase
  {
    protected GameObject m_itemTemplete;
    protected RankingListInfo m_cachedRankList;
    private int m_curSelectSeasonId;
    protected const string RankListItemPrefabName = "RankingListItemUIPrefab";
    [AutoBind("./ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
    public Transform ItemRoot;
    [AutoBind("./RankingListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public EasyObjectPool RankListItemPool;
    [AutoBind("./PlayerInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    public SelfRankingListItemUIController SelfRankItemUICtrl;
    [AutoBind("./RankingListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public LoopVerticalScrollRect RankListScrollRect;
    [AutoBind("./RankingListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController RankListStateCtrl;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateItemListPool(int poolSize)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRankingListInfo(RankingListInfo rankList, int curSelectSeasonId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateSelfRankInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRankItemFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRankItemClick(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
