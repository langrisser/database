﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LongPressComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class LongPressComponent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IEventSystemHandler
  {
    private bool hasInvokeLongPressEvent;
    private bool IsNeedEventOnLongPressLoop;
    private bool m_isPointDown;
    private float m_lastInvokeTime;
    private float m_delay;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBasicParam(
      float delay = 1f,
      float interval = 0.3f,
      Action eventOnLongPress = null,
      Action eventOnPointDown = null,
      Action eventOnPointUp = null,
      bool isNeedEventOnLongPressLoop = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnPointerExit(PointerEventData eventData)
    {
      this.m_isPointDown = false;
    }

    public float Interval { private get; set; }

    public float Delay { private get; set; }

    private event Action EventOnLongPress
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private event Action EventOnPointDown
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private event Action EventOnPointUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
