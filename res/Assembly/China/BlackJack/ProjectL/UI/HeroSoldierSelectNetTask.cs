﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroSoldierSelectNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroSoldierSelectNetTask : UINetTask
  {
    private int m_heroId;
    private int m_soldierId;
    private SkillAndSoldierSelectMode m_mode;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroSoldierSelectNetTask(int heroId, int soldierId, SkillAndSoldierSelectMode mode = SkillAndSoldierSelectMode.NoneOfAbove)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnHeroSoldierSelectAck(int result)
    {
      this.Result = result;
      this.OnTransactionComplete();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
