﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.NormalItemSelfChooseBuyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class NormalItemSelfChooseBuyUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./LayoutRoot/Prefab/Item", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemTemplate;
    [AutoBind("./LayoutRoot/ListScrollView /Mask/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_content;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private List<GoodsUIController> m_goodUIControllerList;
    private NormalItemBuyUITask m_normalItemBuyUITask;
    private StoreId m_storeId;
    private int m_fixedStoreItemId;

    [MethodImpl((MethodImplOptions) 32768)]
    public NormalItemSelfChooseBuyUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Init(NormalItemBuyUITask normalItemBuyUITask)
    {
      this.m_normalItemBuyUITask = normalItemBuyUITask;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelfChooseItemBuyPlane(StoreId storeId, int fixedStoreItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoodsClick(GoodsUIController goodsUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<StoreId, int, int> EventOnSelfBoxChoose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
