﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BuffDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BuffDesc : MonoBehaviour
  {
    private Image m_image;
    private Text m_text;
    private Text m_timeText;
    private ConfigDataBuffInfo m_buffInfo;
    [DoNotToLua]
    private BuffDesc.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_Awake_hotfix;
    private LuaFunction m_SetBuffConfigDataBuffInfo_hotfix;
    private LuaFunction m_SetDebugTextString_hotfix;
    private LuaFunction m_SetTimeInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBuff(ConfigDataBuffInfo buffInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDebugText(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTime(int buffTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BuffDesc.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BuffDesc m_owner;

      public LuaExportHelper(BuffDesc owner)
      {
        this.m_owner = owner;
      }

      public Image m_image
      {
        get
        {
          return this.m_owner.m_image;
        }
        set
        {
          this.m_owner.m_image = value;
        }
      }

      public Text m_text
      {
        get
        {
          return this.m_owner.m_text;
        }
        set
        {
          this.m_owner.m_text = value;
        }
      }

      public Text m_timeText
      {
        get
        {
          return this.m_owner.m_timeText;
        }
        set
        {
          this.m_owner.m_timeText = value;
        }
      }

      public ConfigDataBuffInfo m_buffInfo
      {
        get
        {
          return this.m_owner.m_buffInfo;
        }
        set
        {
          this.m_owner.m_buffInfo = value;
        }
      }

      public void Awake()
      {
        this.m_owner.Awake();
      }
    }
  }
}
