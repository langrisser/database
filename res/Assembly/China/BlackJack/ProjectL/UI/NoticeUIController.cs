﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.NoticeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class NoticeUIController : UIControllerBase
  {
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_noticeButton;
    [AutoBind("./Button/Detail", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noticeDetailGo;
    [AutoBind("./Button/Detail/Text", AutoBindAttribute.InitState.NotInit, false)]
    private EmojiText m_noticeText;
    [AutoBind("./Button/Detail/VoiceImage", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_noticeVoiceImage;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private SmallExpressionParseDesc m_expressionParseDesc;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_expressionResContainer;
    private static bool s_isForceHide;
    private static NoticeUIController s_instance;
    private const float NoticeTweenSpeed = 100f;
    private const float ShowNoticeInterval = 1f;
    private const float ShowNoticeTime = 5f;
    private Vector3 m_noticeTextInitPos;
    private float m_noticeTextInitWidth;
    private Coroutine m_delayHideNoticeCoroutine;
    private TweenPos m_noticeShowTween;
    private TweenPos m_noticeMoveTween;
    private NoticeText m_currentNoticeText;
    private ProjectLPlayerContext m_playerContext;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsNoticeAvailable(ChatChannel chatChannel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNotice(
      string txt,
      string stateName,
      ChatChannel chatChannel,
      ChatContentType chatContentType)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPositionState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowNextNotice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeShowTweenFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnNoticeMoveTweenFinished()
    {
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayHideNotice(float time, float additiveTime = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsForceHide
    {
      get
      {
        return NoticeUIController.s_isForceHide;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<NoticeText> EventOnNoticeClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
