﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ChatExpressionUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ChatExpressionUIController : UIControllerBase
  {
    public BigExpressionInChatController m_bigExpressionInChatController;
    private float m_emptySpaceNormalRatio;
    private int m_currSmallPageIndex;
    private List<CommonUIStateController> m_smallExpressionPagePointList;
    private List<KeyValuePair<float, float>> m_smallExpressionPageSubSectionList;
    private List<SmallExpressionItemContrller> m_smallExpressionCtrlList;
    [AutoBind("./Detail/ExpressionToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle SmallExpressionToggle;
    [AutoBind("./ExpressionBGButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ExpressionBgButton;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController PanelStateCtrl;
    [AutoBind("./Detail/ExpressionGroup/ExpressionScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SmallExpressionRoot;
    [AutoBind("./Detail/ExpressionGroup/ExpressionScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SmallExpressionContent;
    [AutoBind("./Detail/ExpressionGroup/ExpressionScrollView/Viewport/Content/OnePage/ExpressionItem", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SmallExpressionItem;
    [AutoBind("./Detail/ExpressionGroup/ExpressionScrollView/Viewport/Content/OnePage", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SmallExpressionPage;
    [AutoBind("./Detail/ExpressionGroup/PagePoint/PagePoint", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ExpressionPagePoint;
    [AutoBind("./Detail/ExpressionGroup/PagePoint", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ExpressionPagePointRoot;
    [AutoBind("./Detail/BigExressionGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject BigExpressionGroupDummy;
    [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ExpressionDetailRoot;
    private const int SmallExpressionCount4OnePage = 24;
    private const int ExpressionPageEmptySpaceWidth = 100;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatExpressionUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    private void Update()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOrHideExpressionPanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SwitchExpressionPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator DelayInstancePrefab()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPointerUp4ExpressionScrollRect(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSmallExpressionClick(SmallExpressionItemContrller uCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionBgButtonClick(UIControllerBase uCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnSmallExpressionToggleValueChanged(bool isSelected)
    {
      if (!isSelected)
        return;
      this.SetSmallExpressionPageImmediate(0);
    }

    private void OnBigExpressionClick(int id)
    {
      this.ShowOrHideExpressionPanel(false);
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator InitExpressionPageAndItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSmallExpressionPageImmediate(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator SetSmallExpressionPage(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<SmallExpressionItemContrller> EventOnSmallExpressionClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnExpressionBgButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
