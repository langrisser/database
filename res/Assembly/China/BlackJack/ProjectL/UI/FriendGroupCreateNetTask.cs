﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendGroupCreateNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class FriendGroupCreateNetTask : UINetTask
  {
    private string m_groupNmae;
    private List<string> m_userIDList;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendGroupCreateNetTask(string groupName, List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnFriendGroupCreateAck(
      int result,
      ProChatGroupInfo chatGroupInfo,
      ProChatUserInfo userInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public ProChatGroupInfo ChatGroupInfo { private set; get; }

    public ProChatUserInfo FailedUser { private set; get; }
  }
}
