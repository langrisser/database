﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogGetRewardNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class DialogGetRewardNetTask : UINetTask
  {
    private int m_dialogId;
    [DoNotToLua]
    private DialogGetRewardNetTask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorInt32_hotfix;
    private LuaFunction m_RegisterNetworkEvent_hotfix;
    private LuaFunction m_UnregisterNetworkEvent_hotfix;
    private LuaFunction m_OnGainDialogRewardAckInt32List`1_hotfix;
    private LuaFunction m_StartNetWorking_hotfix;
    private LuaFunction m_get_Rewards_hotfix;
    private LuaFunction m_set_RewardsList`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DialogGetRewardNetTask(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnGainDialogRewardAck(int result, List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<Goods> Rewards
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public DialogGetRewardNetTask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(object param)
    {
      return this.OnStart(param);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnTimeOut()
    {
      this.OnTimeOut();
    }

    private void __callBase_OnReLoginSuccess()
    {
      this.OnReLoginSuccess();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private DialogGetRewardNetTask m_owner;

      public LuaExportHelper(DialogGetRewardNetTask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(object param)
      {
        return this.m_owner.__callBase_OnStart(param);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnTimeOut()
      {
        this.m_owner.__callBase_OnTimeOut();
      }

      public void __callBase_OnReLoginSuccess()
      {
        this.m_owner.__callBase_OnReLoginSuccess();
      }

      public int m_dialogId
      {
        get
        {
          return this.m_owner.m_dialogId;
        }
        set
        {
          this.m_owner.m_dialogId = value;
        }
      }

      public List<Goods> Rewards
      {
        set
        {
          this.m_owner.Rewards = value;
        }
      }

      public void RegisterNetworkEvent()
      {
        this.m_owner.RegisterNetworkEvent();
      }

      public void UnregisterNetworkEvent()
      {
        this.m_owner.UnregisterNetworkEvent();
      }

      public void OnGainDialogRewardAck(int result, List<Goods> rewards)
      {
        this.m_owner.OnGainDialogRewardAck(result, rewards);
      }

      public bool StartNetWorking()
      {
        return this.m_owner.StartNetWorking();
      }
    }
  }
}
