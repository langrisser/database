﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ControllerDescriptionAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  [AttributeUsage(AttributeTargets.All)]
  public class ControllerDescriptionAttribute : Attribute
  {
    public string prefabPath;
    public string controllerName;

    [MethodImpl((MethodImplOptions) 32768)]
    public ControllerDescriptionAttribute(string prefabPath, string controllerName)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
