﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.JobMaterialEquipedUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class JobMaterialEquipedUIController : UIControllerBase
  {
    [AutoBind("./ImageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImg;
    [AutoBind("./BgImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bgImg;
    [AutoBind("./ValueGroup/NeedCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_needCountText;
    [AutoBind("./ValueGroup/HaveCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_haveCountText;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitJobMaterial(Goods jobMaterialGood)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobMaterialClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<JobMaterialEquipedUIController> EventOnJobMaterialClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int NeedCount { private set; get; }

    public int HaveCount { private set; get; }

    public ConfigDataJobMaterialInfo JobMaterialInfo { private set; get; }
  }
}
