﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleActorBuffUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleActorBuffUIController
  {
    private Image m_image;
    private Text m_text;
    private ConfigDataBuffInfo m_buffInfo;
    private int m_time;
    [DoNotToLua]
    private BattleActorBuffUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitGameObjectString_hotfix;
    private LuaFunction m_ShowBoolean_hotfix;
    private LuaFunction m_SetBuffConfigDataBuffInfoInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorBuffUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(GameObject go, string imageName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBuff(ConfigDataBuffInfo buffInfo, int time)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleActorBuffUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleActorBuffUIController m_owner;

      public LuaExportHelper(BattleActorBuffUIController owner)
      {
        this.m_owner = owner;
      }

      public Image m_image
      {
        get
        {
          return this.m_owner.m_image;
        }
        set
        {
          this.m_owner.m_image = value;
        }
      }

      public Text m_text
      {
        get
        {
          return this.m_owner.m_text;
        }
        set
        {
          this.m_owner.m_text = value;
        }
      }

      public ConfigDataBuffInfo m_buffInfo
      {
        get
        {
          return this.m_owner.m_buffInfo;
        }
        set
        {
          this.m_owner.m_buffInfo = value;
        }
      }

      public int m_time
      {
        get
        {
          return this.m_owner.m_time;
        }
        set
        {
          this.m_owner.m_time = value;
        }
      }
    }
  }
}
