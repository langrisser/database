﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ScrollItemBaseUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ScrollItemBaseUIController : UIControllerBase
  {
    public Action<UIControllerBase> EventOnUIItemNeedFill;
    public Action<UIControllerBase> EventOnUIItemClick;
    public Action<UIControllerBase> EventOnUIItemDoubleClick;
    public Action<UIControllerBase> EventOnUIItem3DTouch;
    public int m_itemIndex;
    public UIControllerBase m_ownerCtrl;
    private ThreeDTouchEventListener m_3dTouchEventTrigger;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(UIControllerBase ownerCtrl, bool isCareItemClick = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ScrollCellIndex(int itemIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScrollItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScrollItemDoubleClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScrollItem3DTouch()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ItemIndex
    {
      get
      {
        return this.m_itemIndex;
      }
      set
      {
        this.m_itemIndex = value;
      }
    }
  }
}
