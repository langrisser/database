﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RaidLevelUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RaidLevelUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/Exp/LevelUpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelUpEffectObj;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Panel/RaidAgainButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_raidAgainButton;
    [AutoBind("./Panel/CannelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cannelButton;
    [AutoBind("./Panel/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./Panel/Exp/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerExpText;
    [AutoBind("./Panel/Exp/Bar/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerExpImage;
    [AutoBind("./Panel/Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldText;
    [AutoBind("./Panel/Rewards/Group", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardGoodsGroupGameObject;
    [AutoBind("./Panel/ExtraRewards/Group", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_extraRewardGoodsGroupGameObject;
    [AutoBind("./Panel/RaidTicketGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_raidTicketText;
    [AutoBind("./NeedGoodsPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_needGoodsPanelCommonUIStateCtrl;
    [AutoBind("./NeedGoodsPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_needGoodsPanel;
    [AutoBind("./NeedGoodsPanel/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_needGoodsIconImage;
    [AutoBind("./NeedGoodsPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_needGoodsNameText;
    [AutoBind("./NeedGoodsPanel/LackAndEnoughGroup/BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lackBGImageGameObject;
    [AutoBind("./NeedGoodsPanel/LackAndEnoughGroup/LackTitleText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lackTitleGameObject;
    [AutoBind("./NeedGoodsPanel/LackAndEnoughGroup/LackText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_needGoodsLackText;
    [AutoBind("./NeedGoodsPanel/LackAndEnoughGroup/EnoughText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_needGoodsEnoughGameObject;
    [AutoBind("./NeedGoodsPanel/GainText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_needGoodsGainText;
    private List<RewardGoodsUIController> m_rewardGoods;
    private bool m_isClick;
    private PlayerLevelUpUITask m_playerLevelUpUITask;
    private int m_gainNeedGoodsCount;
    private ProjectLPlayerContext m_playerContext;

    [MethodImpl((MethodImplOptions) 32768)]
    private RaidLevelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowReward(BattleReward battleReward, List<Goods> extraReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowReward(BattleReward battleReward, List<Goods> extraReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowPlayerLevelUp(int oldLevel, int newLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearRewardGoods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRaidTicketCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNeedGoods(
      NeedGoods needGoods,
      BattleReward battleReward,
      List<Goods> extraReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRaidAgainButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelUpUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRaidAgain
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
