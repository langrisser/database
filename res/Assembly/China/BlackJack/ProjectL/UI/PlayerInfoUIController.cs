﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using PD.SDK;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class PlayerInfoUIController : UIControllerBase
  {
    [AutoBind("./PlayerInfoContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_playerInfoContentStateCtrl;
    [AutoBind("./PlayerInfoContent/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./PlayerInfoContent/TopPanel/PlayerIcon/Change", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeIconButton;
    [AutoBind("./PlayerInfoContent/TopPanel/PlayerName/Change", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeNameButton;
    [AutoBind("./PlayerInfoContent/TopPanel/NameCardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchMyBusinessCardButton;
    [AutoBind("./PlayerInfoContent/TopPanel/CopyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_copyButton;
    [AutoBind("./PlayerInfoContent/MiddlePanel/SignButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_signButton;
    [AutoBind("./PlayerInfoContent/MiddlePanel/SettingButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_settingButton;
    [AutoBind("./PlayerInfoContent/MiddlePanel/RECToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_recToggle;
    [AutoBind("./PlayerInfoContent/TopPanel/PlayerIcon/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconImage;
    [AutoBind("./PlayerInfoContent/TopPanel/PlayerIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    [AutoBind("./PlayerInfoContent/TopPanel/PlayerName/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./PlayerInfoContent/TopPanel/PlayerLevel/Level/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./PlayerInfoContent/TopPanel/PlayerLevel/ProgressText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerExpProgressText;
    [AutoBind("./PlayerInfoContent/TopPanel/PlayerLevel/LevelProgress/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerExpProgressImage;
    [AutoBind("./PlayerInfoContent/TopPanel/AccountID/IDText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_userIDText;
    [AutoBind("./PlayerInfoContent/Version/VersionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_versionText;
    [AutoBind("./ChangeNamePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_changeNameStateCtrl;
    [AutoBind("./ChangeNamePanel/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_changeNameInputField;
    [AutoBind("./ChangeNamePanel/ConfirmButton/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_changeNamePriceImage;
    [AutoBind("./ChangeNamePanel/ConfirmButton/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_changeNamePriceText;
    [AutoBind("./ChangeNamePanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeNamePanelBGButton;
    [AutoBind("./ChangeNamePanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmChangeNameButton;
    [AutoBind("./PlayerInfoContent/DownPanel/VoicePackage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_voicePackageButton;
    [AutoBind("./PlayerInfoContent/DownPanel/RedeemCode", AutoBindAttribute.InitState.Active, false)]
    private Button m_redeemCodeButton;
    [AutoBind("./PlayerInfoContent/DownPanel/Forum", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_forumButton;
    [AutoBind("./PlayerInfoContent/DownPanel/ChangeAccount", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeAccountButton;
    [AutoBind("./PlayerInfoContent/DownPanel/UserCenter", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_userCenterButton;
    [AutoBind("./PlayerInfoContent/DownPanel/PlayerCenter", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerCenterButton;
    [AutoBind("./RedemptionCodePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_redemptionCodePanelUIStateCtrl;
    [AutoBind("./RedemptionCodePanel/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_redemptionCodeReturnButton;
    [AutoBind("./RedemptionCodePanel/Content/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_redemptionCodeInputField;
    [AutoBind("./RedemptionCodePanel/Content/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_redemptionCodeCancelButton;
    [AutoBind("./RedemptionCodePanel/Content/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_redemptionCodeConfirmButton;
    [AutoBind("./ChangeIconPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changeHeadIconPanelGameObj;
    [AutoBind("./Prefab/PlayerIconPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerHeadPortraitPrefab;
    [AutoBind("./Prefab/HeadFramePrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerHeadFramePrefab;
    private PlayerInfoHeadIconUIController m_headIconUIController;
    private ClientConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private int m_playerNameLimit;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerInfoUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetChangeNamePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerNameLimit(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerInfoOpenTween()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRECToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnUserCenterButtonClick()
    {
      PDSDK.Instance.callCustomerServiceWeb();
    }

    private void OnPlayerCenterButtonClick()
    {
      PDSDK.Instance.userCenter();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSignButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSettingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeNameButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeNameInputField_OnEndEdit(string str)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchMyBusinessCardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmChangeNameButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnChangeNamePanelCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCopyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnVoicePackageButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeIconButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRedeemCodeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRedemptionCodeConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRedemptionCodeCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnForumButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnChangeAccountButtonClick()
    {
      LoginUITask.ReturnToLoginAndSwitchUser();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChangeHeadIconPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnChangeIconPanelCloseButtonClick()
    {
      this.m_headIconUIController.OnChangeIconPanelCloseButtonClick();
    }

    public event Action EventOnShowChangeIconPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnChangeHeadPortraitAndHeadFrame
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnChangeName
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowSign
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowPlayerSetting
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowMyBusinessCard
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnChangeRECState
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, Action<int>> EventOnRedemptionCodeUse
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCrystalNotEnough
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
