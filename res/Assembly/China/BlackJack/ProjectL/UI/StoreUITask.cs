﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class StoreUITask : UITask
  {
    private static List<PDSDKGood> m_pdsdkGoods = new List<PDSDKGood>();
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private StoreListUIController m_storeListUIController;
    private int m_nowSecond;
    private ProjectLPlayerContext m_playerContext;
    private ClientConfigDataLoader m_configdataLoader;
    private string m_curBuyingGoodsId;
    public const string ParamsKey_StoreInfoId = "StoreInfoID";
    public const string ParamsKey_StoreItemId = "StoreItemId";
    public const string ParamsKey_IsShowExtractionPanel = "IsShowExtractionPanel";
    private StoreId m_storeId;
    public static GetRewardGoodsUITask m_orderGetRewardGoodsUITask;
    public static Action m_orderGetRewardUICloseCallback;
    [DoNotToLua]
    private StoreUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_OnPDSDKPayCancel_hotfix;
    private LuaFunction m_OnPDSDKPayFailed_hotfix;
    private LuaFunction m_OnPDSDKPaySuccess_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnNewIntentUIIntent_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_InitDataFromUIIntentUIIntent_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_OnMemoryWarning_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_GetStoreTypeByStoreIdStoreId_hotfix;
    private LuaFunction m_StoreUIController_OnClose_hotfix;
    private LuaFunction m_StoreUIController_OnChangeStoreStoreId_hotfix;
    private LuaFunction m_StoreUIController_OnFixedStoreHeroSkinItemClickStoreHeroSkinItemUIController_hotfix;
    private LuaFunction m_StoreUIController_OnFixedStoreSoldierSkinItemClickStoreSoldierSkinItemUIController_hotfix;
    private LuaFunction m_StoreUIController_OnFixedStoreItemBuyStoreIdInt32Int32Int32List`1_hotfix;
    private LuaFunction m_StoreUIController_OnFixedStoreBoxBuyStoreIdInt32Int32List`1_hotfix;
    private LuaFunction m_StoreUIController_OnRandomStoreItemBuyStoreIdInt32List`1_hotfix;
    private LuaFunction m_StoreUIController_OnRandomStoreBoxBuyStoreIdInt32Int32List`1_hotfix;
    private LuaFunction m_StoreUIController_GetRandomStoreStoreId_hotfix;
    private LuaFunction m_StoreUIController_RefreshRandomStoreStoreId_hotfix;
    private LuaFunction m_StoreUIController_OMemoryExtractionButtonClickAction_hotfix;
    private LuaFunction m_StoreListUIController_OnSkinItemOutOfDate_hotfix;
    private LuaFunction m_StoreUIController_OnRechargeStoreBuyStoreTypeInt32Int32_hotfix;
    private LuaFunction m_SendStoreItemCancelBuyReq_hotfix;
    private LuaFunction m_SendCheckOnlineReq_hotfix;
    private LuaFunction m_ClearBuyingGoodsCache_hotfix;
    private LuaFunction m_StoreUIController_OnGiftStoreBuyConfigDataGiftStoreItemInfoBooleanInt32_hotfix;
    private LuaFunction m_SendGiftStoreBuyStoreItemReqStoreTypeInt32Int32PDSDKGoodType_hotfix;
    private LuaFunction m_SendGiftStoreAppleSubscribeItemReqStoreTypeInt32Int32PDSDKGoodType_hotfix;
    private LuaFunction m_SendRechargeStoreBuyStoreItemReqStoreTypeInt32Int32PDSDKGoodType_hotfix;
    private LuaFunction m_PlayerContex_OnGiftStoreBuyNtfString_hotfix;
    private LuaFunction m_PlayerContex_OnGiftStoreOperationalGoodsUpdateNtf_hotfix;
    private LuaFunction m_PlayerContext_OnPlayerInfoInitEnd_hotfix;
    private LuaFunction m_HandleBoxOpenNetTaskGoodsTypeInt32Int32Action`1Action_hotfix;
    private LuaFunction m_PlayerContext_OnRechargeNoughtSuccessNtfString_hotfix;
    private LuaFunction m_StoreListUIController_OpenAddGoldStore_hotfix;
    private LuaFunction m_StoreListUIController_OpenAddCrystalStore_hotfix;
    private LuaFunction m_StoreUIController_CrystalNotEnough_hotfix;
    private LuaFunction m_StoreUIController_OnExtraCurrencyBuyGoodsType_hotfix;
    private LuaFunction m_UpdateViewAndOpenRewardGoodsUIList`1_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUITask(
      UIIntent fromIntent,
      StoreId storeId,
      int? itemId = null,
      bool isNeedOpenMemoryExtractionPanel = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SpecialStartStoreUITask(UIIntent uiIntent, StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StoreUIUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PDSDKRequestBuyGoods(string goodID, PDSDKGoodType goodType, int number)
    {
      // ISSUE: unable to decompile the method.
    }

    public static List<PDSDKGood> PDSDKGetGoods()
    {
      return StoreUITask.m_pdsdkGoods;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PDSDKRequestGoods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPDSDKPayCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPDSDKPayFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPDSDKPaySuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnPDSDKReqGoodsAck(bool isSuccess)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private StoreType GetStoreTypeByStoreId(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnChangeStore(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StoreUIController_OnFixedStoreHeroSkinItemClick(
      StoreHeroSkinItemUIController itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StoreUIController_OnFixedStoreSoldierSkinItemClick(
      StoreSoldierSkinItemUIController itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnFixedStoreItemBuy(
      StoreId fixedStoreID,
      int fixedStoreItemGoodsID,
      int goodsId,
      int selfChooseItemIndex,
      List<Goods> gainGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnFixedStoreBoxBuy(
      StoreId fixedStoreID,
      int fixedStoreItemGoodsID,
      int goodsID,
      List<Goods> gainGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnRandomStoreItemBuy(
      StoreId randomStoreId,
      int index,
      List<Goods> gainGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnRandomStoreBoxBuy(
      StoreId randomStoreId,
      int index,
      int goodsID,
      List<Goods> gainGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_GetRandomStore(StoreId randomStoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_RefreshRandomStore(StoreId randomStoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OMemoryExtractionButtonClick(Action OnSucced)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreListUIController_OnSkinItemOutOfDate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnRechargeStoreBuy(
      StoreType storeType,
      int itemGoodsId,
      int number)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendStoreItemCancelBuyReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendCheckOnlineReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearBuyingGoodsCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnGiftStoreBuy(
      ConfigDataGiftStoreItemInfo giftStoreItem,
      bool isfristBuy,
      int number)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendGiftStoreBuyStoreItemReq(
      StoreType storeType,
      int goodsId,
      int number,
      PDSDKGoodType pdsdkGoodType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendGiftStoreAppleSubscribeItemReq(
      StoreType storeType,
      int goodsId,
      int number,
      PDSDKGoodType pdsdkGoodType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendRechargeStoreBuyStoreItemReq(
      StoreType storeType,
      int goodsId,
      int number,
      PDSDKGoodType pdsdkGoodType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContex_OnGiftStoreBuyNtf(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContex_OnGiftStoreOperationalGoodsUpdateNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleBoxOpenNetTask(
      GoodsType type,
      int id,
      int count,
      Action<List<Goods>> successedCallback,
      Action failedCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnRechargeNoughtSuccessNtf(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreListUIController_OpenAddGoldStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreListUIController_OpenAddCrystalStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_CrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnExtraCurrencyBuy(GoodsType goodsType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewAndOpenRewardGoodsUI(List<Goods> rewardGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckOrderReward(Action OnGetReward = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SendPullOrderRewardReq(
      string orderId,
      Action successedCallback = null,
      Action failedCallback = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator ProcessOrderRewards(
      List<Goods> rewardList,
      Action successedCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator ProcessOrderRewardsSub(
      List<Goods> rewardsDisplay,
      List<Goods> rewardList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void HandleOneGiftStoreBuyItems(List<Goods> rewards, Action successedCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StaticGetRewardGoodsUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StaticGetRewardGoodsUITask_OnClose()
    {
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action<int> m_onBuyGiftStoreGoodsEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public StoreUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return base.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      base.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void __callDele_m_onBuyGiftStoreGoodsEvent(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private static void __clearDele_m_onBuyGiftStoreGoodsEvent(int obj)
    {
      StoreUITask.m_onBuyGiftStoreGoodsEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private StoreUITask m_owner;

      public LuaExportHelper(StoreUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public static void __callDele_m_onBuyGiftStoreGoodsEvent(int obj)
      {
        StoreUITask.__callDele_m_onBuyGiftStoreGoodsEvent(obj);
      }

      public static void __clearDele_m_onBuyGiftStoreGoodsEvent(int obj)
      {
        StoreUITask.__clearDele_m_onBuyGiftStoreGoodsEvent(obj);
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public StoreListUIController m_storeListUIController
      {
        get
        {
          return this.m_owner.m_storeListUIController;
        }
        set
        {
          this.m_owner.m_storeListUIController = value;
        }
      }

      public int m_nowSecond
      {
        get
        {
          return this.m_owner.m_nowSecond;
        }
        set
        {
          this.m_owner.m_nowSecond = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public ClientConfigDataLoader m_configdataLoader
      {
        get
        {
          return this.m_owner.m_configdataLoader;
        }
        set
        {
          this.m_owner.m_configdataLoader = value;
        }
      }

      public static List<PDSDKGood> m_pdsdkGoods
      {
        get
        {
          return StoreUITask.m_pdsdkGoods;
        }
        set
        {
          StoreUITask.m_pdsdkGoods = value;
        }
      }

      public string m_curBuyingGoodsId
      {
        get
        {
          return this.m_owner.m_curBuyingGoodsId;
        }
        set
        {
          this.m_owner.m_curBuyingGoodsId = value;
        }
      }

      public StoreId m_storeId
      {
        get
        {
          return this.m_owner.m_storeId;
        }
        set
        {
          this.m_owner.m_storeId = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void InitDataFromUIIntent(UIIntent intent)
      {
        this.m_owner.InitDataFromUIIntent(intent);
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void OnMemoryWarning()
      {
        this.m_owner.OnMemoryWarning();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public StoreType GetStoreTypeByStoreId(StoreId storeId)
      {
        return this.m_owner.GetStoreTypeByStoreId(storeId);
      }

      public void StoreUIController_OnClose()
      {
        this.m_owner.StoreUIController_OnClose();
      }

      public void StoreUIController_OnChangeStore(StoreId storeId)
      {
        this.m_owner.StoreUIController_OnChangeStore(storeId);
      }

      public void StoreUIController_OnFixedStoreHeroSkinItemClick(
        StoreHeroSkinItemUIController itemCtrl)
      {
        this.m_owner.StoreUIController_OnFixedStoreHeroSkinItemClick(itemCtrl);
      }

      public void StoreUIController_OnFixedStoreSoldierSkinItemClick(
        StoreSoldierSkinItemUIController itemCtrl)
      {
        this.m_owner.StoreUIController_OnFixedStoreSoldierSkinItemClick(itemCtrl);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void StoreUIController_OnFixedStoreItemBuy(
        StoreId fixedStoreID,
        int fixedStoreItemGoodsID,
        int goodsId,
        int selfChooseItemIndex,
        List<Goods> gainGoodsList)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void StoreUIController_OnFixedStoreBoxBuy(
        StoreId fixedStoreID,
        int fixedStoreItemGoodsID,
        int goodsID,
        List<Goods> gainGoodsList)
      {
        // ISSUE: unable to decompile the method.
      }

      public void StoreUIController_OnRandomStoreItemBuy(
        StoreId randomStoreId,
        int index,
        List<Goods> gainGoodsList)
      {
        this.m_owner.StoreUIController_OnRandomStoreItemBuy(randomStoreId, index, gainGoodsList);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void StoreUIController_OnRandomStoreBoxBuy(
        StoreId randomStoreId,
        int index,
        int goodsID,
        List<Goods> gainGoodsList)
      {
        // ISSUE: unable to decompile the method.
      }

      public void StoreUIController_GetRandomStore(StoreId randomStoreId)
      {
        this.m_owner.StoreUIController_GetRandomStore(randomStoreId);
      }

      public void StoreUIController_RefreshRandomStore(StoreId randomStoreId)
      {
        this.m_owner.StoreUIController_RefreshRandomStore(randomStoreId);
      }

      public void StoreUIController_OMemoryExtractionButtonClick(Action OnSucced)
      {
        this.m_owner.StoreUIController_OMemoryExtractionButtonClick(OnSucced);
      }

      public void StoreListUIController_OnSkinItemOutOfDate()
      {
        this.m_owner.StoreListUIController_OnSkinItemOutOfDate();
      }

      public void StoreUIController_OnRechargeStoreBuy(
        StoreType storeType,
        int itemGoodsId,
        int number)
      {
        this.m_owner.StoreUIController_OnRechargeStoreBuy(storeType, itemGoodsId, number);
      }

      public void SendStoreItemCancelBuyReq()
      {
        this.m_owner.SendStoreItemCancelBuyReq();
      }

      public void SendCheckOnlineReq()
      {
        this.m_owner.SendCheckOnlineReq();
      }

      public void ClearBuyingGoodsCache()
      {
        this.m_owner.ClearBuyingGoodsCache();
      }

      public void StoreUIController_OnGiftStoreBuy(
        ConfigDataGiftStoreItemInfo giftStoreItem,
        bool isfristBuy,
        int number)
      {
        this.m_owner.StoreUIController_OnGiftStoreBuy(giftStoreItem, isfristBuy, number);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SendGiftStoreBuyStoreItemReq(
        StoreType storeType,
        int goodsId,
        int number,
        PDSDKGoodType pdsdkGoodType)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SendGiftStoreAppleSubscribeItemReq(
        StoreType storeType,
        int goodsId,
        int number,
        PDSDKGoodType pdsdkGoodType)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SendRechargeStoreBuyStoreItemReq(
        StoreType storeType,
        int goodsId,
        int number,
        PDSDKGoodType pdsdkGoodType)
      {
        // ISSUE: unable to decompile the method.
      }

      public void PlayerContex_OnGiftStoreBuyNtf(string orderId)
      {
        this.m_owner.PlayerContex_OnGiftStoreBuyNtf(orderId);
      }

      public void PlayerContex_OnGiftStoreOperationalGoodsUpdateNtf()
      {
        this.m_owner.PlayerContex_OnGiftStoreOperationalGoodsUpdateNtf();
      }

      public void PlayerContext_OnPlayerInfoInitEnd()
      {
        this.m_owner.PlayerContext_OnPlayerInfoInitEnd();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void HandleBoxOpenNetTask(
        GoodsType type,
        int id,
        int count,
        Action<List<Goods>> successedCallback,
        Action failedCallback)
      {
        // ISSUE: unable to decompile the method.
      }

      public void PlayerContext_OnRechargeNoughtSuccessNtf(string orderId)
      {
        this.m_owner.PlayerContext_OnRechargeNoughtSuccessNtf(orderId);
      }

      public void StoreListUIController_OpenAddGoldStore()
      {
        this.m_owner.StoreListUIController_OpenAddGoldStore();
      }

      public void StoreListUIController_OpenAddCrystalStore()
      {
        this.m_owner.StoreListUIController_OpenAddCrystalStore();
      }

      public void StoreUIController_CrystalNotEnough()
      {
        this.m_owner.StoreUIController_CrystalNotEnough();
      }

      public void StoreUIController_OnExtraCurrencyBuy(GoodsType goodsType)
      {
        this.m_owner.StoreUIController_OnExtraCurrencyBuy(goodsType);
      }

      public void UpdateViewAndOpenRewardGoodsUI(List<Goods> rewardGoods)
      {
        this.m_owner.UpdateViewAndOpenRewardGoodsUI(rewardGoods);
      }

      public static IEnumerator ProcessOrderRewardsSub(
        List<Goods> rewardsDisplay,
        List<Goods> rewardList)
      {
        return StoreUITask.ProcessOrderRewardsSub(rewardsDisplay, rewardList);
      }
    }
  }
}
