﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BeforeJoinGuildListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BeforeJoinGuildListUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_listPanelAnimation;
    [AutoBind("./SociatyListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_guildListContent;
    [AutoBind("./BottomButtonPanel/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeButton;
    [AutoBind("./BottomButtonPanel/GreatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildCreateButton;
    [AutoBind("./BottomButtonPanel/Search/SearchButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildSearchButton;
    [AutoBind("./BottomButtonPanel/Search/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_deleteSearchButton;
    [AutoBind("./Prefab/SociatyItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_guildItem;
    [AutoBind("./BottomButtonPanel/Search/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_guildNameInputField;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private BeforeJoinGuildUIController m_guildUIController;
    private List<GuildSearchInfo> m_guildSearchInfoList;
    private List<GuildSearchInfo> m_guildRecommendInfoList;
    private List<GuildSearchInfo> m_showGuildInfoList;
    private List<BeforeJoinSingleGuildUIController> m_guildInfoUIController;
    private BeforeJoinSingleGuildUIController m_selectGuildItemUIController;

    [MethodImpl((MethodImplOptions) 32768)]
    public BeforeJoinGuildListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(BeforeJoinGuildUIController guildUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetRandomGuildList(Action<int> OnReqFinish)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGuildItemSelect(
      BeforeJoinSingleGuildUIController guildItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshGuildList(List<GuildSearchInfo> guildSearchInfoList)
    {
      // ISSUE: unable to decompile the method.
    }

    public void RefreshGuildList()
    {
      this.RefreshGuildList(this.m_showGuildInfoList);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCreateGuildClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeGuildClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSearchClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDeleteSearchClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnGuildItemClick(
      BeforeJoinSingleGuildUIController guildItemUIController)
    {
      this.SetGuildItemSelect(guildItemUIController);
    }
  }
}
