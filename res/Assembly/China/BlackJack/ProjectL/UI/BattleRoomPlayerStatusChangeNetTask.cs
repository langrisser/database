﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleRoomPlayerStatusChangeNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BattleRoomPlayerStatusChangeNetTask : UINetTask
  {
    private PlayerBattleStatus m_status;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayerStatusChangeNetTask(PlayerBattleStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnBattleRoomPlayerStatusChangeAck(int result)
    {
      this.Result = result;
      this.OnTransactionComplete();
    }
  }
}
