﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RankingUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RankingUITask : UITask
  {
    protected static string ParamKey_RankingType = "RankingType";
    protected RankingListType m_currRankListType;
    protected RankingListInfo m_currRankList;
    private RankingUIController m_mainCtrl;
    private RankingListUIController m_rankingListUICtrl;
    private int m_curSelectSeasonId;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartRankingUITask(
      Action<bool> onPrepareEnd,
      UIIntent prevIntent,
      RankingListType rankingType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool IsNeedUpdateDataCache()
    {
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool IsNeedLoadDynamicRes()
    {
      return this.IsPipelineStateMaskNeedUpdate(RankingUITask.PipeLineStateMaskType.IsNeedLoadDynamic);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<string> CollectAllDynamicResForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRankingTypeMenuClick(RankingListType rankingType, int selectSeasonId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnResetRankingType()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnReturnButtonClick()
    {
      this.Pause();
      this.ReturnPrevUITask();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitPipeLineCtxStateFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected bool IsPipelineStateMaskNeedUpdate(RankingUITask.PipeLineStateMaskType state)
    {
      return this.m_currPipeLineCtx.IsNeedUpdate((int) state);
    }

    protected void EnablePipelineStateMask(RankingUITask.PipeLineStateMaskType state)
    {
      this.m_currPipeLineCtx.AddUpdateMask((int) state);
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    public enum PipeLineStateMaskType
    {
      IsNeedLoadDynamic,
      IsRefreshRankingList,
    }
  }
}
