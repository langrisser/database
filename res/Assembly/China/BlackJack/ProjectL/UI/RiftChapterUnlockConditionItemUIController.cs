﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftChapterUnlockConditionItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RiftChapterUnlockConditionItemUIController : UIControllerBase
  {
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_text;
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goButton;
    private int m_scenarioID;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCondition(RiftChapterUnlockConditionType condition, int param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGoToScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
