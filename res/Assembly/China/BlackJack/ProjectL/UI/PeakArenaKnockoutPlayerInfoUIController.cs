﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaKnockoutPlayerInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PeakArenaKnockoutPlayerInfoUIController : UIControllerBase
  {
    private int GroupStateFinalRound = 4;
    [AutoBind("./BGType", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bgState;
    [AutoBind("./State", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_playerItemState;
    [AutoBind("./State/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./State/ServerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_serverNameText;
    [AutoBind("./HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerIconButton;
    [AutoBind("./HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconImage;
    [AutoBind("./HeadIcon/HeadIconGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconGreyImage;
    [AutoBind("./HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    [AutoBind("./State/Idols", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_JettonButton;
    [AutoBind("./State/Idols", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_JettonButtonState;
    [AutoBind("./State/Idols/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jettonValueText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private CommonUIStateController m_headFrameState;
    private PeakArenaUITask m_peakArenaUITask;
    private PeakArenaPlayOffPlayerInfo m_knockoutPlayerInfo;
    private PeakArenaPlayOffMatchupInfo m_matchupInfo;
    [DoNotToLua]
    private PeakArenaKnockoutPlayerInfoUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetPlayerDefaultStatePeakArenaPlayOffMatchupInfoPeakArenaPlayOffPlayerInfo_hotfix;
    private LuaFunction m_SetByeState_hotfix;
    private LuaFunction m_SetMatchupInfoPeakArenaPlayOffMatchupInfo_hotfix;
    private LuaFunction m_UpdateViewPlayerInfo_hotfix;
    private LuaFunction m_UpdateViewGreyStateBoolean_hotfix;
    private LuaFunction m_UpdateViewFinishMatchStatePeakArenaPlayOffMatchupInfo_hotfix;
    private LuaFunction m_UpdateViewJetton_hotfix;
    private LuaFunction m_GetPlayerUserid_hotfix;
    private LuaFunction m_OnPlayerHeadClick_hotfix;
    private LuaFunction m_OnUseJettonClick_hotfix;
    private LuaFunction m_OnUseJettonSuccess_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerDefaultState(
      PeakArenaPlayOffMatchupInfo matchupInfo,
      PeakArenaPlayOffPlayerInfo playerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetByeState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMatchupInfo(PeakArenaPlayOffMatchupInfo matchupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewGreyState(bool isGrey)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewFinishMatchState(PeakArenaPlayOffMatchupInfo matchupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewJetton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPlayerUserid()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerHeadClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseJettonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseJettonSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public PeakArenaKnockoutPlayerInfoUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaKnockoutPlayerInfoUIController m_owner;

      public LuaExportHelper(PeakArenaKnockoutPlayerInfoUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_bgState
      {
        get
        {
          return this.m_owner.m_bgState;
        }
        set
        {
          this.m_owner.m_bgState = value;
        }
      }

      public CommonUIStateController m_playerItemState
      {
        get
        {
          return this.m_owner.m_playerItemState;
        }
        set
        {
          this.m_owner.m_playerItemState = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public Text m_serverNameText
      {
        get
        {
          return this.m_owner.m_serverNameText;
        }
        set
        {
          this.m_owner.m_serverNameText = value;
        }
      }

      public Button m_playerIconButton
      {
        get
        {
          return this.m_owner.m_playerIconButton;
        }
        set
        {
          this.m_owner.m_playerIconButton = value;
        }
      }

      public Image m_playerIconImage
      {
        get
        {
          return this.m_owner.m_playerIconImage;
        }
        set
        {
          this.m_owner.m_playerIconImage = value;
        }
      }

      public Image m_playerIconGreyImage
      {
        get
        {
          return this.m_owner.m_playerIconGreyImage;
        }
        set
        {
          this.m_owner.m_playerIconGreyImage = value;
        }
      }

      public Transform m_headFrameTransform
      {
        get
        {
          return this.m_owner.m_headFrameTransform;
        }
        set
        {
          this.m_owner.m_headFrameTransform = value;
        }
      }

      public Button m_JettonButton
      {
        get
        {
          return this.m_owner.m_JettonButton;
        }
        set
        {
          this.m_owner.m_JettonButton = value;
        }
      }

      public CommonUIStateController m_JettonButtonState
      {
        get
        {
          return this.m_owner.m_JettonButtonState;
        }
        set
        {
          this.m_owner.m_JettonButtonState = value;
        }
      }

      public Text m_jettonValueText
      {
        get
        {
          return this.m_owner.m_jettonValueText;
        }
        set
        {
          this.m_owner.m_jettonValueText = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public CommonUIStateController m_headFrameState
      {
        get
        {
          return this.m_owner.m_headFrameState;
        }
        set
        {
          this.m_owner.m_headFrameState = value;
        }
      }

      public PeakArenaUITask m_peakArenaUITask
      {
        get
        {
          return this.m_owner.m_peakArenaUITask;
        }
        set
        {
          this.m_owner.m_peakArenaUITask = value;
        }
      }

      public PeakArenaPlayOffPlayerInfo m_knockoutPlayerInfo
      {
        get
        {
          return this.m_owner.m_knockoutPlayerInfo;
        }
        set
        {
          this.m_owner.m_knockoutPlayerInfo = value;
        }
      }

      public PeakArenaPlayOffMatchupInfo m_matchupInfo
      {
        get
        {
          return this.m_owner.m_matchupInfo;
        }
        set
        {
          this.m_owner.m_matchupInfo = value;
        }
      }

      public int GroupStateFinalRound
      {
        get
        {
          return this.m_owner.GroupStateFinalRound;
        }
        set
        {
          this.m_owner.GroupStateFinalRound = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void UpdateViewPlayerInfo()
      {
        this.m_owner.UpdateViewPlayerInfo();
      }

      public void UpdateViewGreyState(bool isGrey)
      {
        this.m_owner.UpdateViewGreyState(isGrey);
      }

      public void UpdateViewFinishMatchState(PeakArenaPlayOffMatchupInfo matchupInfo)
      {
        this.m_owner.UpdateViewFinishMatchState(matchupInfo);
      }

      public void UpdateViewJetton()
      {
        this.m_owner.UpdateViewJetton();
      }

      public void OnPlayerHeadClick()
      {
        this.m_owner.OnPlayerHeadClick();
      }

      public void OnUseJettonClick()
      {
        this.m_owner.OnUseJettonClick();
      }

      public void OnUseJettonSuccess()
      {
        this.m_owner.OnUseJettonSuccess();
      }
    }
  }
}
