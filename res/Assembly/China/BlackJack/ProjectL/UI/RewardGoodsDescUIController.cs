﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RewardGoodsDescUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class RewardGoodsDescUIController : UIControllerBase
  {
    private GoodsType m_goodsType;
    private int m_goodsId;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    private GameObject HPGo;
    private GameObject ATGo;
    private GameObject DFGo;
    private GameObject MagicGo;
    private GameObject MagicDFGo;
    private GameObject DEXGo;
    private Text HPText;
    private Text ATText;
    private Text DFText;
    private Text MagicText;
    private Text MagicDFText;
    private Text DEXText;
    private RectTransform m_backgroundTransform;
    [DoNotToLua]
    private RewardGoodsDescUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitRewardGoodsDescInfoGoodsTypeInt32Boolean_hotfix;
    private LuaFunction m_SetEquipmentPropInfoInt32_hotfix;
    private LuaFunction m_SetPropItemsPropertyModifyTypeInt32_hotfix;
    private LuaFunction m_GetBackgroundTransform_hotfix;
    private LuaFunction m_ShowRewardGoodsDescPrefabControllerBaseGoodsTypeInt32Int32GameObjectBoolean_hotfix;
    private LuaFunction m_ShowPanel_hotfix;
    private LuaFunction m_ClosePanel_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRewardGoodsDescInfo(GoodsType goodsType, int goodsId, bool isNeedAutoClose)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentPropInfo(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform GetBackgroundTransform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRewardGoodsDesc(
      PrefabControllerBase ctrl,
      GoodsType goodsType,
      int goodsId,
      int alignType = 0,
      GameObject gameObjectForPosCalc = null,
      bool isNeedAutoClose = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator Co_ShowRewardGoodsDesc(
      RewardGoodsDescUIController descCtrl,
      GameObject gameObjectForPosCalc,
      int alignType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public RewardGoodsDescUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private RewardGoodsDescUIController m_owner;

      public LuaExportHelper(RewardGoodsDescUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public GoodsType m_goodsType
      {
        get
        {
          return this.m_owner.m_goodsType;
        }
        set
        {
          this.m_owner.m_goodsType = value;
        }
      }

      public int m_goodsId
      {
        get
        {
          return this.m_owner.m_goodsId;
        }
        set
        {
          this.m_owner.m_goodsId = value;
        }
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public GameObject HPGo
      {
        get
        {
          return this.m_owner.HPGo;
        }
        set
        {
          this.m_owner.HPGo = value;
        }
      }

      public GameObject ATGo
      {
        get
        {
          return this.m_owner.ATGo;
        }
        set
        {
          this.m_owner.ATGo = value;
        }
      }

      public GameObject DFGo
      {
        get
        {
          return this.m_owner.DFGo;
        }
        set
        {
          this.m_owner.DFGo = value;
        }
      }

      public GameObject MagicGo
      {
        get
        {
          return this.m_owner.MagicGo;
        }
        set
        {
          this.m_owner.MagicGo = value;
        }
      }

      public GameObject MagicDFGo
      {
        get
        {
          return this.m_owner.MagicDFGo;
        }
        set
        {
          this.m_owner.MagicDFGo = value;
        }
      }

      public GameObject DEXGo
      {
        get
        {
          return this.m_owner.DEXGo;
        }
        set
        {
          this.m_owner.DEXGo = value;
        }
      }

      public Text HPText
      {
        get
        {
          return this.m_owner.HPText;
        }
        set
        {
          this.m_owner.HPText = value;
        }
      }

      public Text ATText
      {
        get
        {
          return this.m_owner.ATText;
        }
        set
        {
          this.m_owner.ATText = value;
        }
      }

      public Text DFText
      {
        get
        {
          return this.m_owner.DFText;
        }
        set
        {
          this.m_owner.DFText = value;
        }
      }

      public Text MagicText
      {
        get
        {
          return this.m_owner.MagicText;
        }
        set
        {
          this.m_owner.MagicText = value;
        }
      }

      public Text MagicDFText
      {
        get
        {
          return this.m_owner.MagicDFText;
        }
        set
        {
          this.m_owner.MagicDFText = value;
        }
      }

      public Text DEXText
      {
        get
        {
          return this.m_owner.DEXText;
        }
        set
        {
          this.m_owner.DEXText = value;
        }
      }

      public RectTransform m_backgroundTransform
      {
        get
        {
          return this.m_owner.m_backgroundTransform;
        }
        set
        {
          this.m_owner.m_backgroundTransform = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void InitRewardGoodsDescInfo(GoodsType goodsType, int goodsId, bool isNeedAutoClose)
      {
        this.m_owner.InitRewardGoodsDescInfo(goodsType, goodsId, isNeedAutoClose);
      }

      public void SetEquipmentPropInfo(int goodsId)
      {
        this.m_owner.SetEquipmentPropInfo(goodsId);
      }

      public void SetPropItems(PropertyModifyType type, int value)
      {
        this.m_owner.SetPropItems(type, value);
      }

      public static IEnumerator Co_ShowRewardGoodsDesc(
        RewardGoodsDescUIController descCtrl,
        GameObject gameObjectForPosCalc,
        int alignType)
      {
        return RewardGoodsDescUIController.Co_ShowRewardGoodsDesc(descCtrl, gameObjectForPosCalc, alignType);
      }

      public void ClosePanel()
      {
        this.m_owner.ClosePanel();
      }
    }
  }
}
