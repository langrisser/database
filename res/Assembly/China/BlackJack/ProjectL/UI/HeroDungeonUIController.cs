﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDungeonUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroDungeonUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./HeroCharPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObjectRoot;
    [AutoBind("./EventGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelsScrollRect;
    [AutoBind("./ChallengeCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeCountValueText;
    [AutoBind("./Progress", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressGameObject;
    [AutoBind("./Progress/VictoryPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressText;
    [AutoBind("./Progress/VictoryPointsReward/BarFrame/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_progressImage;
    [AutoBind("./Progress/VictoryPointsReward/Button1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starRewardButton1GameObject;
    [AutoBind("./Progress/VictoryPointsReward/Button2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starRewardButton2GameObject;
    [AutoBind("./Progress/VictoryPointsReward/Button3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starRewardButton3GameObject;
    [AutoBind("./StarRewardPreview", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_starRewardPreviewUIStateController;
    [AutoBind("./StarRewardPreview/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_starRewardPreviewBackgroundButton;
    [AutoBind("./StarRewardPreview/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_starRewardPreviewScrollRect;
    [AutoBind("./EventGroup/TurntableImage/Circle1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_circle1GameObject;
    [AutoBind("./EventGroup/TurntableImage/Circle2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_circle2GameObject;
    [AutoBind("./Prefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_prefabGameObject;
    [AutoBind("./Prefab/HeroDungeonLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroDungeonLevelListItemPrefab;
    [AutoBind("./EventGroup/TurntableImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_ellipseImage;
    private List<HeroDungeonLevelListItemUIController> m_heroDungeonLevelListItems;
    private const int gearRollThreasholdValue = 20;
    private float m_rotationCount;
    private int m_curLevelIndex;
    private float dy2;
    private GainRewardButton[] m_starRewardButtons;
    private HeroCharUIController m_heroCharUIController;
    private ConfigDataHeroInformationInfo m_heroDungeonInfo;
    private ScrollSnapCenter m_levelsScrollViewVerticalSnapCenter;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    [DoNotToLua]
    private HeroDungeonUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_LateUpdate_hotfix;
    private LuaFunction m_GetLineOnEllipseIntersectionDoubleDoubleDoubleDoubleDoubleDoubleDoubleDouble_hotfix;
    private LuaFunction m_isPointInLineDoubleDoubleDoubleDoubleDoubleDouble_hotfix;
    private LuaFunction m_GetLineLineIntersectionVector2Vector2Vector2Vector2_hotfix;
    private LuaFunction m_SetCircleRotationSingle_hotfix;
    private LuaFunction m_AddHeroDungeonLevelListItemConfigDataHeroDungeonLevelInfo_hotfix;
    private LuaFunction m_ClearHeroDungeonLevelListItems_hotfix;
    private LuaFunction m_UpdateDungeonLevelListItemInt32_hotfix;
    private LuaFunction m_SetCurrentDungeonLevelConfigDataHeroDungeonLevelInfo_hotfix;
    private LuaFunction m_SetHeroCharInt32_hotfix;
    private LuaFunction m_SetDungeonStarsProgressConfigDataHeroInformationInfo_hotfix;
    private LuaFunction m_OnStarRewardButtonClickGainRewardButton_hotfix;
    private LuaFunction m_AddStarRewardPreviewGoodsConfigDataHeroInformationInfoInt32_hotfix;
    private LuaFunction m_SetChallengeCountValueTextInt32_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnHeroDungeonLevelButtonClickHeroDungeonLevelListItemUIController_hotfix;
    private LuaFunction m_OnStarRewardPreviewBackgroundButtonClick_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnGetStarRewardAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetStarRewardAction`1_hotfix;
    private LuaFunction m_add_EventOnSelectDungeonLevelAction`1_hotfix;
    private LuaFunction m_remove_EventOnSelectDungeonLevelAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Vector2> GetLineOnEllipseIntersection(
      double x1,
      double x2,
      double y1,
      double y2,
      double midX,
      double midY,
      double h,
      double v)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool isPointInLine(double x1, double x2, double y1, double y2, double px, double py)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 GetLineLineIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCircleRotation(float deltaRotation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroDungeonLevelListItem(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearHeroDungeonLevelListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateDungeonLevelListItem(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentDungeonLevel(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroChar(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDungeonStarsProgress(ConfigDataHeroInformationInfo heroDungeonInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStarRewardButtonClick(GainRewardButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddStarRewardPreviewGoods(ConfigDataHeroInformationInfo heroDungeonInfo, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChallengeCountValueText(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroDungeonLevelButtonClick(HeroDungeonLevelListItemUIController b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStarRewardPreviewBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGetStarReward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataHeroDungeonLevelInfo> EventOnSelectDungeonLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroDungeonUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetStarReward(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetStarReward(int obj)
    {
      this.EventOnGetStarReward = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSelectDungeonLevel(ConfigDataHeroDungeonLevelInfo obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSelectDungeonLevel(ConfigDataHeroDungeonLevelInfo obj)
    {
      this.EventOnSelectDungeonLevel = (Action<ConfigDataHeroDungeonLevelInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroDungeonUIController m_owner;

      public LuaExportHelper(HeroDungeonUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnGetStarReward(int obj)
      {
        this.m_owner.__callDele_EventOnGetStarReward(obj);
      }

      public void __clearDele_EventOnGetStarReward(int obj)
      {
        this.m_owner.__clearDele_EventOnGetStarReward(obj);
      }

      public void __callDele_EventOnSelectDungeonLevel(ConfigDataHeroDungeonLevelInfo obj)
      {
        this.m_owner.__callDele_EventOnSelectDungeonLevel(obj);
      }

      public void __clearDele_EventOnSelectDungeonLevel(ConfigDataHeroDungeonLevelInfo obj)
      {
        this.m_owner.__clearDele_EventOnSelectDungeonLevel(obj);
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public GameObject m_charGameObjectRoot
      {
        get
        {
          return this.m_owner.m_charGameObjectRoot;
        }
        set
        {
          this.m_owner.m_charGameObjectRoot = value;
        }
      }

      public ScrollRect m_levelsScrollRect
      {
        get
        {
          return this.m_owner.m_levelsScrollRect;
        }
        set
        {
          this.m_owner.m_levelsScrollRect = value;
        }
      }

      public Text m_challengeCountValueText
      {
        get
        {
          return this.m_owner.m_challengeCountValueText;
        }
        set
        {
          this.m_owner.m_challengeCountValueText = value;
        }
      }

      public GameObject m_progressGameObject
      {
        get
        {
          return this.m_owner.m_progressGameObject;
        }
        set
        {
          this.m_owner.m_progressGameObject = value;
        }
      }

      public Text m_progressText
      {
        get
        {
          return this.m_owner.m_progressText;
        }
        set
        {
          this.m_owner.m_progressText = value;
        }
      }

      public Image m_progressImage
      {
        get
        {
          return this.m_owner.m_progressImage;
        }
        set
        {
          this.m_owner.m_progressImage = value;
        }
      }

      public GameObject m_starRewardButton1GameObject
      {
        get
        {
          return this.m_owner.m_starRewardButton1GameObject;
        }
        set
        {
          this.m_owner.m_starRewardButton1GameObject = value;
        }
      }

      public GameObject m_starRewardButton2GameObject
      {
        get
        {
          return this.m_owner.m_starRewardButton2GameObject;
        }
        set
        {
          this.m_owner.m_starRewardButton2GameObject = value;
        }
      }

      public GameObject m_starRewardButton3GameObject
      {
        get
        {
          return this.m_owner.m_starRewardButton3GameObject;
        }
        set
        {
          this.m_owner.m_starRewardButton3GameObject = value;
        }
      }

      public CommonUIStateController m_starRewardPreviewUIStateController
      {
        get
        {
          return this.m_owner.m_starRewardPreviewUIStateController;
        }
        set
        {
          this.m_owner.m_starRewardPreviewUIStateController = value;
        }
      }

      public Button m_starRewardPreviewBackgroundButton
      {
        get
        {
          return this.m_owner.m_starRewardPreviewBackgroundButton;
        }
        set
        {
          this.m_owner.m_starRewardPreviewBackgroundButton = value;
        }
      }

      public ScrollRect m_starRewardPreviewScrollRect
      {
        get
        {
          return this.m_owner.m_starRewardPreviewScrollRect;
        }
        set
        {
          this.m_owner.m_starRewardPreviewScrollRect = value;
        }
      }

      public GameObject m_circle1GameObject
      {
        get
        {
          return this.m_owner.m_circle1GameObject;
        }
        set
        {
          this.m_owner.m_circle1GameObject = value;
        }
      }

      public GameObject m_circle2GameObject
      {
        get
        {
          return this.m_owner.m_circle2GameObject;
        }
        set
        {
          this.m_owner.m_circle2GameObject = value;
        }
      }

      public GameObject m_prefabGameObject
      {
        get
        {
          return this.m_owner.m_prefabGameObject;
        }
        set
        {
          this.m_owner.m_prefabGameObject = value;
        }
      }

      public GameObject m_heroDungeonLevelListItemPrefab
      {
        get
        {
          return this.m_owner.m_heroDungeonLevelListItemPrefab;
        }
        set
        {
          this.m_owner.m_heroDungeonLevelListItemPrefab = value;
        }
      }

      public Image m_ellipseImage
      {
        get
        {
          return this.m_owner.m_ellipseImage;
        }
        set
        {
          this.m_owner.m_ellipseImage = value;
        }
      }

      public List<HeroDungeonLevelListItemUIController> m_heroDungeonLevelListItems
      {
        get
        {
          return this.m_owner.m_heroDungeonLevelListItems;
        }
        set
        {
          this.m_owner.m_heroDungeonLevelListItems = value;
        }
      }

      public static int gearRollThreasholdValue
      {
        get
        {
          return 20;
        }
      }

      public float m_rotationCount
      {
        get
        {
          return this.m_owner.m_rotationCount;
        }
        set
        {
          this.m_owner.m_rotationCount = value;
        }
      }

      public int m_curLevelIndex
      {
        get
        {
          return this.m_owner.m_curLevelIndex;
        }
        set
        {
          this.m_owner.m_curLevelIndex = value;
        }
      }

      public float dy2
      {
        get
        {
          return this.m_owner.dy2;
        }
        set
        {
          this.m_owner.dy2 = value;
        }
      }

      public GainRewardButton[] m_starRewardButtons
      {
        get
        {
          return this.m_owner.m_starRewardButtons;
        }
        set
        {
          this.m_owner.m_starRewardButtons = value;
        }
      }

      public HeroCharUIController m_heroCharUIController
      {
        get
        {
          return this.m_owner.m_heroCharUIController;
        }
        set
        {
          this.m_owner.m_heroCharUIController = value;
        }
      }

      public ConfigDataHeroInformationInfo m_heroDungeonInfo
      {
        get
        {
          return this.m_owner.m_heroDungeonInfo;
        }
        set
        {
          this.m_owner.m_heroDungeonInfo = value;
        }
      }

      public ScrollSnapCenter m_levelsScrollViewVerticalSnapCenter
      {
        get
        {
          return this.m_owner.m_levelsScrollViewVerticalSnapCenter;
        }
        set
        {
          this.m_owner.m_levelsScrollViewVerticalSnapCenter = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void LateUpdate()
      {
        this.m_owner.LateUpdate();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public List<Vector2> GetLineOnEllipseIntersection(
        double x1,
        double x2,
        double y1,
        double y2,
        double midX,
        double midY,
        double h,
        double v)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool isPointInLine(
        double x1,
        double x2,
        double y1,
        double y2,
        double px,
        double py)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public Vector2 GetLineLineIntersection(
        Vector2 p1,
        Vector2 p2,
        Vector2 p3,
        Vector2 p4)
      {
        // ISSUE: unable to decompile the method.
      }

      public void SetCircleRotation(float deltaRotation)
      {
        this.m_owner.SetCircleRotation(deltaRotation);
      }

      public void OnStarRewardButtonClick(GainRewardButton b)
      {
        this.m_owner.OnStarRewardButtonClick(b);
      }

      public void AddStarRewardPreviewGoods(ConfigDataHeroInformationInfo heroDungeonInfo, int idx)
      {
        this.m_owner.AddStarRewardPreviewGoods(heroDungeonInfo, idx);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnHeroDungeonLevelButtonClick(HeroDungeonLevelListItemUIController b)
      {
        this.m_owner.OnHeroDungeonLevelButtonClick(b);
      }

      public void OnStarRewardPreviewBackgroundButtonClick()
      {
        this.m_owner.OnStarRewardPreviewBackgroundButtonClick();
      }
    }
  }
}
