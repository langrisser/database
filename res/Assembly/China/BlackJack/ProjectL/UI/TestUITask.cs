﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TestUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TestUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private TestUIController m_testUIController;

    [MethodImpl((MethodImplOptions) 32768)]
    public TestUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void InitAllUIControllers()
    {
      base.InitAllUIControllers();
      this.InitTestUIController();
    }

    protected override void ClearAllContextAndRes()
    {
      base.ClearAllContextAndRes();
      this.UninitTestUIController();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitTestUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitTestUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReportBug(string bugDesc, double logFileHours)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator ReportBug(string bugDesc, double logFileHours)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator UploadFile(
      string localFilepath,
      string uploadURL,
      string serverFilename)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static byte[] Byte2HexByte(byte[] bytes)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void UpdateView()
    {
      this.UpdateTestList();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTestList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartDialogUITask(ConfigDataDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleDialogUITask(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUserGuideDialogUITask(ConfigDataUserGuideDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGoddessDialogUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUIController_OnChangeTestList(TestListType listType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUIController_OnStartTest(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    private void TestUIController_OnReturn()
    {
      this.Pause();
      this.ReturnPrevUITask();
    }

    private void TestUIController_OnShowSystemInfo()
    {
      this.m_testUIController.ShowSystemInfo();
    }

    private void TestUIController_OnCloseSystemInfo()
    {
      this.m_testUIController.HideSystemInfo();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUIController_OnClearUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    private void TestUIController_OnCompleteUserGuide()
    {
      TestUI.SendGMCommand("COMPLETE_ALLUSERGUIDE");
    }

    private void TestUIController_OnClearStage()
    {
      TestUI.SendGMCommand("CLEAR_STAGE 0");
    }

    private void TestUIController_OnShowGoddessDialog()
    {
      this.StartGoddessDialogUITask();
    }

    public event Action<ConfigDataBattleInfo> EventOnStartBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
