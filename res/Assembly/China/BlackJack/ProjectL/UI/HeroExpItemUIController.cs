﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroExpItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroExpItemUIController : UIControllerBase, IPointerDownHandler, IPointerUpHandler, IDragHandler, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
  {
    [AutoBind("./CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_countText;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./FrameImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_frameImage;
    public bool IsPointDown;
    private int usedCount;
    private float m_delay;
    private float m_lastInvokeTime;
    private bool m_isDragging;
    private GameObject m_draggingGo;
    private float m_startPressTime;
    [DoNotToLua]
    private HeroExpItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitExpItemBagItemBaseSingleSingleAction_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_OnPointerDownPointerEventData_hotfix;
    private LuaFunction m_OnPointerUpPointerEventData_hotfix;
    private LuaFunction m_OnBeginDragPointerEventData_hotfix;
    private LuaFunction m_OnDragPointerEventData_hotfix;
    private LuaFunction m_OnEndDragPointerEventData_hotfix;
    private LuaFunction m_PassEventPointerEventDataEventFunction`1_hotfix;
    private LuaFunction m_add_OnItemPointUpAction`1_hotfix;
    private LuaFunction m_remove_OnItemPointUpAction`1_hotfix;
    private LuaFunction m_add_OnItemPointDownAction`1_hotfix;
    private LuaFunction m_remove_OnItemPointDownAction`1_hotfix;
    private LuaFunction m_set_BagItemBagItemBase_hotfix;
    private LuaFunction m_get_BagItem_hotfix;
    private LuaFunction m_set_AddExpValueInt32_hotfix;
    private LuaFunction m_get_AddExpValue_hotfix;
    private LuaFunction m_get_Interval_hotfix;
    private LuaFunction m_set_IntervalSingle_hotfix;
    private LuaFunction m_get_Delay_hotfix;
    private LuaFunction m_set_DelaySingle_hotfix;
    private LuaFunction m_add_EventOnLongPressAction_hotfix;
    private LuaFunction m_remove_EventOnLongPressAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitExpItem(
      BagItemBase bagItem,
      float delay,
      float interval,
      Action eventOnLongPress)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PassEvent<T>(PointerEventData data, ExecuteEvents.EventFunction<T> function) where T : IEventSystemHandler
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> OnItemPointUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<HeroExpItemUIController> OnItemPointDown
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BagItemBase BagItem
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int AddExpValue
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public float Interval
    {
      [MethodImpl((MethodImplOptions) 32768)] private get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public float Delay
    {
      [MethodImpl((MethodImplOptions) 32768)] private get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnLongPress
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroExpItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_OnItemPointUp(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_OnItemPointUp(int obj)
    {
      this.OnItemPointUp = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_OnItemPointDown(HeroExpItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_OnItemPointDown(HeroExpItemUIController obj)
    {
      this.OnItemPointDown = (Action<HeroExpItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnLongPress()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnLongPress()
    {
      this.EventOnLongPress = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroExpItemUIController m_owner;

      public LuaExportHelper(HeroExpItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_OnItemPointUp(int obj)
      {
        this.m_owner.__callDele_OnItemPointUp(obj);
      }

      public void __clearDele_OnItemPointUp(int obj)
      {
        this.m_owner.__clearDele_OnItemPointUp(obj);
      }

      public void __callDele_OnItemPointDown(HeroExpItemUIController obj)
      {
        this.m_owner.__callDele_OnItemPointDown(obj);
      }

      public void __clearDele_OnItemPointDown(HeroExpItemUIController obj)
      {
        this.m_owner.__clearDele_OnItemPointDown(obj);
      }

      public void __callDele_EventOnLongPress()
      {
        this.m_owner.__callDele_EventOnLongPress();
      }

      public void __clearDele_EventOnLongPress()
      {
        this.m_owner.__clearDele_EventOnLongPress();
      }

      public Text m_countText
      {
        get
        {
          return this.m_owner.m_countText;
        }
        set
        {
          this.m_owner.m_countText = value;
        }
      }

      public Image m_iconImage
      {
        get
        {
          return this.m_owner.m_iconImage;
        }
        set
        {
          this.m_owner.m_iconImage = value;
        }
      }

      public Image m_frameImage
      {
        get
        {
          return this.m_owner.m_frameImage;
        }
        set
        {
          this.m_owner.m_frameImage = value;
        }
      }

      public int usedCount
      {
        get
        {
          return this.m_owner.usedCount;
        }
        set
        {
          this.m_owner.usedCount = value;
        }
      }

      public float m_delay
      {
        get
        {
          return this.m_owner.m_delay;
        }
        set
        {
          this.m_owner.m_delay = value;
        }
      }

      public float m_lastInvokeTime
      {
        get
        {
          return this.m_owner.m_lastInvokeTime;
        }
        set
        {
          this.m_owner.m_lastInvokeTime = value;
        }
      }

      public bool m_isDragging
      {
        get
        {
          return this.m_owner.m_isDragging;
        }
        set
        {
          this.m_owner.m_isDragging = value;
        }
      }

      public GameObject m_draggingGo
      {
        get
        {
          return this.m_owner.m_draggingGo;
        }
        set
        {
          this.m_owner.m_draggingGo = value;
        }
      }

      public float m_startPressTime
      {
        get
        {
          return this.m_owner.m_startPressTime;
        }
        set
        {
          this.m_owner.m_startPressTime = value;
        }
      }

      public BagItemBase BagItem
      {
        set
        {
          this.m_owner.BagItem = value;
        }
      }

      public int AddExpValue
      {
        set
        {
          this.m_owner.AddExpValue = value;
        }
      }

      public float Interval
      {
        get
        {
          return this.m_owner.Interval;
        }
      }

      public float Delay
      {
        get
        {
          return this.m_owner.Delay;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void Update()
      {
        this.m_owner.Update();
      }
    }
  }
}
