﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SoldierManualItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SoldierManualItemUIController : UIControllerBase
  {
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./ClickImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_clickImage;
    [AutoBind("./LockImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lockObj;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGraphic;
    private UISpineGraphic m_graphic;
    public ConfigDataSoldierInfo SoldierInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierItem(ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetClickImageActive(bool isShow)
    {
      this.m_clickImage.SetActive(isShow);
    }

    public event Action<SoldierManualItemUIController> EventOnSoldierItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
