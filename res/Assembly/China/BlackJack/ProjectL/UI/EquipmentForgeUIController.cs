﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentForgeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using MarchingBytes;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class EquipmentForgeUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Margin/FilterToggles/EnhanceToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_enhanceToggle;
    [AutoBind("./Margin/FilterToggles/BreakToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_breakToggle;
    [AutoBind("./Margin/FilterToggles/EnchantmentToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_enchantmentToggle;
    [AutoBind("./Margin/FilterToggles/EnchantmentToggle/EnchantLockMask", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantLockMaskButton;
    [AutoBind("./PlayerResource/Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldText;
    [AutoBind("./PlayerResource/Gold/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goldAddButton;
    [AutoBind("./EquipmentList/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_loopScrollView;
    [AutoBind("./EquipmentList/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_listItemPool;
    [AutoBind("./EquipmentList/ListScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listScrollViewItemRoot;
    [AutoBind("./EquipmentList/EnchantmentFilter", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentFilter;
    [AutoBind("./EquipmentList/EnchantmentFilter/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantmentfilterSortButton;
    [AutoBind("./EquipmentList/EnchantmentFilter/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentfilterSortTypeText;
    [AutoBind("./EquipmentList/EnchantmentFilter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentfilterSortTypesGo;
    [AutoBind("./EquipmentList/EnchantmentFilter/SortTypes/GridLayout", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentfilterSortTypesGridLayout;
    [AutoBind("./EquipmentList/EnchantmentFilter/SortTypes/AllButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentfilterSortTypesAllToggle;
    [AutoBind("./EquipmentList/EnhanceAndBreakFilter", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceAndBreakFilter;
    [AutoBind("./EquipmentList/EnhanceAndBreakFilter/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterSortButton;
    [AutoBind("./EquipmentList/EnhanceAndBreakFilter/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_filterSortTypeText;
    [AutoBind("./EquipmentList/EnhanceAndBreakFilter/SortOrderButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterSortOrderButton;
    [AutoBind("./EquipmentList/EnhanceAndBreakFilter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_filterSortTypesGo;
    [AutoBind("./EquipmentList/EnhanceAndBreakFilter/SortTypes/GridLayout", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_filterSortTypesGridLayout;
    [AutoBind("./EquipmentList/NotBreakItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listNoBreakItemGo;
    [AutoBind("./EquipmentList/NoEnchantmentText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listNoEnchantmentItemGo;
    [AutoBind("./EquipmentList/Title", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_listTitleStateCtrl;
    [AutoBind("./EquipmentList/Text", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listLongPressTipGo;
    [AutoBind("./Desc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descGo;
    [AutoBind("./Desc/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descReturnBgButton;
    [AutoBind("./Desc/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descLockButton;
    [AutoBind("./Desc/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descLockButtonStateCtrl;
    [AutoBind("./Desc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descTitleText;
    [AutoBind("./Desc/Lay/FrameImage/Top/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIcon;
    [AutoBind("./Desc/Lay/FrameImage/Top/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIconBg;
    [AutoBind("./Desc/Lay/FrameImage/Top/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSSREffect;
    [AutoBind("./Desc/Lay/FrameImage/Top/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descStarGroup;
    [AutoBind("./Desc/Lay/FrameImage/Top/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descLvText;
    [AutoBind("./Desc/Lay/FrameImage/Top/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descExpText;
    [AutoBind("./Desc/Lay/FrameImage/Top/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descProgressImage;
    [AutoBind("./Desc/Lay/FrameImage/Top/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descEquipLimitContent;
    [AutoBind("./Desc/Lay/FrameImage/Top/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSkillContent;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descSkillContentStateCtrl;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descUnlockCoditionText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillNameText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillLvText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillOwnerText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSkillOwnerBGImage;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillDescText;
    [AutoBind("./Desc/Lay/FrameImage/Button/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descNotEquipSkillTip;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropContent;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropATGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropATValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropDFGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropDFValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropHPGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropHPValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropMagiccGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropMagicValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropMagicDFGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropMagicDFValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropDexGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropDexValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropGroupStateCtrl;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropEnchantmentGroup;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance/RuneIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descPropEnchantmentGroupRuneIconImage;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropEnchantmentGroupRuneNameText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceTitleText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enhanceIcon;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enhanceIconBg;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceIconSSREffect;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceStarGroup;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ProgressGroup/Lv/CurLv", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceCurLv;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ProgressGroup/Lv/CurLv1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceCurLv1;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ProgressGroup/Lv/MaxLV", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceMaxLV;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ProgressGroup/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enhanceProgressImage;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ProgressGroup/WillGetExpImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enhanceWillGetExpImage;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ExpValueText/CurValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceExpCurValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ExpValueText/AddValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceExpAddValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ExpValueText/NextLvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceExpNextLvText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/MaterialsContent/MaterialsGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceMaterialsContent;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/EnhanceGold/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceGoldText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/EnhanceButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceButton;
    [AutoBind("./EnhanceSuccessEffectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceSuccessEffectPanel;
    [AutoBind("./EnhanceSuccessEffectPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceSuccessEffectPanelCloseButton;
    [AutoBind("./ConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceConfirmPanel;
    [AutoBind("./ConfirmPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceConfirmButton;
    [AutoBind("./ConfirmPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceCancelButton;
    [AutoBind("./ConfirmPanel/Detail/Tip1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceConfirmPanelTip1;
    [AutoBind("./ConfirmPanel/Detail/Tip2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceConfirmPanelTip2;
    [AutoBind("./EnhaneConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceItemSRConfirmPanel;
    [AutoBind("./EnhaneConfirmPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceItemSRConfirmButton;
    [AutoBind("./EnhaneConfirmPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceItemSRCancelButton;
    [AutoBind("./EnhaneConfirmPanel/Detail/Tip2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceItemSRTip;
    [AutoBind("./IntensifySuccessPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_intensifySuccessPanelStateCtrl;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/LvGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_intensifySuccessPanelInfoStateCtrl;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/Equipment/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_intensifySuccessPanelInfoIconBg;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/Equipment/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_intensifySuccessPanelInfoIcon;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/Equipment/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_intensifySuccessPanelInfoStarGroup;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/LvGroup/CurLvGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_intensifySuccessPanelCurLvText;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/LvGroup/NextLvGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_intensifySuccessPanelNextLvText;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/LvGroup/SkillNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_intensifySuccessPanelSkillNameText;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/DescTextScrollView/Mask/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_intensifySuccessPanelSkillDescText;
    [AutoBind("./IntensifySuccessPanel/BlackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_intensifySuccessPanelBlackButton;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropGroupGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropHpGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/HP/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropHpOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/HP/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropHpNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropATGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/AT/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropATOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/AT/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropATNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropDFGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/DF/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropDFOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/DF/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropDFNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropMagicGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/Magic/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropMagicOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/Magic/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropMagicNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropMagicDFGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/MagicDF/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropMagicDFOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/MagicDF/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropMagicDFNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/DEX", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropDEXGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/DEX/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropDEXOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/DEX/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropDEXNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/SkillGroup/SkillChangeItem", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enhancePropSkillStateCtrl;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/SkillGroup/SkillChangeItem/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropSkillNameText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/SkillGroup/SkillChangeItem/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropSkillOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/SkillGroup/SkillChangeItem/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropSkillNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/SkillGroup/SkillChangeItem/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropSkillConditionText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/FastAddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceFastAddButton;
    [AutoBind("./EquipmentInfo/EquipmentBreak", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakGo;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakTitleText;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/CurLvEquipment/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakCurLvEquipmentIcon;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/CurLvEquipment/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakCurLvEquipmentIconBg;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/CurLvEquipment/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakCurLvEquipmentIconSSREffect;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/CurLvEquipment/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakCurLvEquipmentStarGroup;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/CurLvEquipment/LvGroup/OldLvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakCurLvEquipmentOldLvText;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/CurLvEquipment/LvGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakCurLvEquipmentMaxLvText;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/NextLvEquipment/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakNextLvEquipmentIcon;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/NextLvEquipment/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakNextLvEquipmentIconBg;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/NextLvEquipment/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakNextLvEquipmentIconSSREffect;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/NextLvEquipment/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakNextLvEquipmentStarGroup;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/NextLvEquipment/LvGroup/OldLvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakNextLvEquipmentOldLvText;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/NextLvEquipment/LvGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakNextLvEquipmentMaxLvText;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/MaterialIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakMaterialIconGo;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/MaterialIcon/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakMaterialIcon;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/MaterialIcon/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakMaterialIconBg;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/MaterialIcon/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakMaterialSSREffect;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/ItemsContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakItemsContent;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/BreakGold/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakGoldText;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/BreakButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_breakButton;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanNotBreak/CurLvEquipment/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakCantIcon;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanNotBreak/CurLvEquipment/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakCantIconBg;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanNotBreak/CurLvEquipment/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakCantStarGroup;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanNotBreak/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakCantTitleText;
    [AutoBind("./BreakSuccessPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakSuccessPanel;
    [AutoBind("./BreakSuccessPanel/BlackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_breakSuccessPanelBlackButton;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/Equipment/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakSuccessInfoIconBg;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/Equipment/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakSuccessInfoIcon;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/Equipment/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakSuccessInfoStarGroup;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/LvGroup/CurLvGroup/OldLvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakSuccessInfoCurOldLvText;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/LvGroup/CurLvGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakSuccessInfoCurMaxLvText;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/LvGroup/NextLvGroup/OldLvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakSuccessInfoNextOldLvText;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/LvGroup/NextLvGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakSuccessInfoNextMaxLvText;
    [AutoBind("./EquipmentInfo/Enchantment/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentItemNameText;
    [AutoBind("./EquipmentInfo/Enchantment/LevelGroup/LevelValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentItemLevelValueText;
    [AutoBind("./EquipmentInfo/Enchantment/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentIcon;
    [AutoBind("./EquipmentInfo/Enchantment/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentIconBg;
    [AutoBind("./EquipmentInfo/Enchantment/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentIconSSREffect;
    [AutoBind("./EquipmentInfo/Enchantment/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentStarGroup;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/EnchantPropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentEnchantPropertyGroup;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/EnchantPropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentEnchantPropertyGroupStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentSuitInfoStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentSuitInfoGroupStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentSuitInfoNowEffectStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect/RuneIcon", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentSuitInfoNowEffectRuneIconStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect/RuneIcon/Active", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentSuitInfoNowEffectRuneActiveIcon;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect/RuneIcon/Unactive", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentSuitInfoNowEffectRuneUnactiveIcon;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect/EffectTitleText/EffectNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentSuitInfoNowEffectRuneNameText;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect/SuitInfoGroup/2Suit/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentSuitInfoNowEffectRune2SuitInfoText;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect/SuitInfoGroup/4Suit/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentSuitInfoNowEffectRune4SuitInfoText;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/AfterEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentSuitInfoAfterEffectStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/AfterEffect/RuneIcon/Unactive", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentSuitInfoAfterEffectRuneUnactiveIcon;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/AfterEffect/EffectTitleText/EffectNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentSuitInfoAfterEffectRuneNameText;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/AfterEffect/SuitInfoGroup/2Suit/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentSuitInfoAfterEffectRune2SuitInfoText;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/AfterEffect/SuitInfoGroup/4Suit/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentSuitInfoAfterEffectRune4SuitInfoText;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentMaterialGroupStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup/ItemIcon/IconBG", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentStoneIconBg;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup/ItemIcon/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentStoneIcon;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup/StoneNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentStoneNameText;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentStoneDescText;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup/ItemIcon/ValueGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentStoneValueGroupStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup/ItemIcon/ValueGroup/HaveCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentStoneHaveCount;
    [AutoBind("./EquipmentInfo/Enchantment/GoldenValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentGoldenValueText;
    [AutoBind("./EquipmentInfo/Enchantment/GoldenValueText", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentGoldenValueTextStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/EnchantmentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantmentButton;
    [AutoBind("./EquipmentInfo/Enchantment/KeepPropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantKeepPropertyGroup;
    [AutoBind("./EquipmentInfo/Enchantment/EnchantSuccessEffectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantSuccessEffectPanel;
    [AutoBind("./EnchantmentResultPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelStateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantmentResultPanelCloseButton;
    [AutoBind("./EnchantmentResultPanel/Detail/OldPropretyGroup/PropretyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentResultPanelOldPropretyGroup;
    [AutoBind("./EnchantmentResultPanel/Detail/OldPropretyGroup/SuitInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelOldSuitInfoStateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/OldPropretyGroup/SuitInfoGroup/EffectTitleText/EffectNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelOldSuitInfoNameText;
    [AutoBind("./EnchantmentResultPanel/Detail/OldPropretyGroup/SuitInfoGroup/2Suit/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelOld2SuitInfoText;
    [AutoBind("./EnchantmentResultPanel/Detail/OldPropretyGroup/SuitInfoGroup/4Suit/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelOld4SuitInfoText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelNewProprety1StateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety1/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentResultPanelNewProprety1ProgressBar;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety1/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewProprety1NameText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety1/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewProprety1ValueText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelNewProprety2StateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety2/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentResultPanelNewProprety2ProgressBar;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety2/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewProprety2NameText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety2/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewProprety2ValueText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety3", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelNewProprety3StateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety3/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentResultPanelNewProprety3ProgressBar;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety3/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewProprety3NameText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety3/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewProprety3ValueText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/SuitInfoGroup/EffectTitleText/EffectNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewSuitInfoNameText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/SuitInfoGroup/2Suit/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNew2SuitInfoText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/SuitInfoGroup/4Suit/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNew4SuitInfoText;
    [AutoBind("./EnchantmentResultPanel/Detail/EnchantmentAgain/EnchantmentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantmentResultPanelEnchantmentAgainButton;
    [AutoBind("./EnchantmentResultPanel/Detail/EnchantmentAgain/EnchantmentButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelEnchantmentAgainButtonStateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/EnchantmentAgain/ItemNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNumberText;
    [AutoBind("./EnchantmentResultPanel/Detail/EnchantmentAgain/GoldenNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelGoldenNumberText;
    [AutoBind("./EnchantmentResultPanel/Detail/EnchantmentAgain/GoldenNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelGoldenNumberStateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/EnchantmentAgain/ItemImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentResultPanelItemImage;
    [AutoBind("./EnchantmentResultPanel/Detail/SaveProprety/SavePropretyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantmentResultPanelSavePropretyButton;
    [AutoBind("./EnchantmentResultPanel/ContinueButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantmentResultPanelContinueButton;
    [AutoBind("./SavePropretyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_savePropretyPanelStateCtrl;
    [AutoBind("./SavePropretyPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_savePropretyPanelCancelButton;
    [AutoBind("./SavePropretyPanel/Detail/ComfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_savePropretyPanelComfirmButton;
    [AutoBind("./SavePropretyPanel/Detail/ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_savePropretyPanelToggle;
    [AutoBind("./CancelPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_cancelPanelStateCtrl;
    [AutoBind("./CancelPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelPanelCancelButton;
    [AutoBind("./CancelPanel/Detail/ComfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelPanelComfirmButton;
    [AutoBind("./CancelPanel/Detail/ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_cancelPanelToggle;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int m_slot;
    private int m_isAscend;
    private bool m_isFirstIn;
    private bool m_isToggleChanged;
    private EquipmentForgeUIController.ForgeState m_curForgeState;
    private ulong m_curEquipmentInstanceId;
    private ulong m_curDescEquipmentInstanceId;
    private ulong m_curBreakMaterialEquipmentId;
    private ulong m_curAddEnhanceEquipmentInstanceId;
    private BagItemBase m_curSelectEnchantStoneItem;
    private ConfigDataEnchantStoneInfo m_lastSelectEnchantStoneInfo;
    private List<ulong> m_enhanceEquipmentInstanceIds;
    private List<EquipmentBagItem> m_equipmentItemCache;
    private List<EnchantStoneBagItem> m_enchantStoneItemCache;
    private List<EquipmentDepotListItemUIController> m_equipmentForgeCtrlList;
    private EquipmentDepotUIController.EquipmentSortTypeState m_curEquipmentSortType;
    private int m_curEnchantmentSortTypeId;
    private List<CommonBattleProperty> m_properties;
    private bool m_isAfter3DTouch;
    private string m_oldSkillLevelStr;
    [DoNotToLua]
    private EquipmentForgeUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitSortTypePanel_hotfix;
    private LuaFunction m_InitLoopScrollRect_hotfix;
    private LuaFunction m_OnPoolObjectCreatedStringGameObject_hotfix;
    private LuaFunction m_UpdateEquipmentForgeViewForgeStateInt32UInt64_hotfix;
    private LuaFunction m_CreateEquipmentList_hotfix;
    private LuaFunction m_SortEquipmentListByTypeList`1_hotfix;
    private LuaFunction m_DefaultEquipmentItemComparerEquipmentBagItemEquipmentBagItem_hotfix;
    private LuaFunction m_CollectEquipmentPropValueAndSortList`1PropertyModifyType_hotfix;
    private LuaFunction m_OnEquipmentListItemClickUIControllerBase_hotfix;
    private LuaFunction m_OnEquipmentItemClickInEnhanceEquipmentDepotListItemUIController_hotfix;
    private LuaFunction m_OnEquipmentItemClickInBreakEquipmentDepotListItemUIController_hotfix;
    private LuaFunction m_OnEquipmentItemClickInEnchantmentEquipmentDepotListItemUIController_hotfix;
    private LuaFunction m_OnEquipmentListItemNeedFillUIControllerBase_hotfix;
    private LuaFunction m_CloseEquipmentDescPanel_hotfix;
    private LuaFunction m_OnEquipmentListItem3DTouchUIControllerBase_hotfix;
    private LuaFunction m_OnFilterSortButtonClick_hotfix;
    private LuaFunction m_OnEnchantmentFilterSortButtonClick_hotfix;
    private LuaFunction m_OnCloseFilterSortTypeGo_hotfix;
    private LuaFunction m_OnCloseEnchantmentFilterSortTypeGo_hotfix;
    private LuaFunction m_OnFilterSortOrderButtonClick_hotfix;
    private LuaFunction m_OnFilterTypeButtonClickEquipmentSortItemUIControllerBoolean_hotfix;
    private LuaFunction m_OnEnchantmentFilterTypeButtonClickEquipmentSortItemUIControllerBoolean_hotfix;
    private LuaFunction m_SetEquipmentItemDescEquipmentBagItem_hotfix;
    private LuaFunction m_SetDescEquipmentLimitEquipmentBagItem_hotfix;
    private LuaFunction m_SetDescEquipmentSkillEquipmentBagItem_hotfix;
    private LuaFunction m_SetDescEquipmentEnchantEquipmentBagItem_hotfix;
    private LuaFunction m_SetPropItemColorTextText_hotfix;
    private LuaFunction m_SetPropItemsPropertyModifyTypeInt32Int32Int32_hotfix;
    private LuaFunction m_OnDescLockButtonClick_hotfix;
    private LuaFunction m_UpdateViewInEnhanceState_hotfix;
    private LuaFunction m_SetEnhancePropItemsPropertyModifyTypeInt32Int32Int32Int32_hotfix;
    private LuaFunction m_ResetEnhancePropItemActive_hotfix;
    private LuaFunction m_CalculateEquipmentEnhanceAddLvByExpEquipmentBagItemInt32Int32_hotfix;
    private LuaFunction m_OnEnhanceButtonClick_hotfix;
    private LuaFunction m_SendEnhanceEquipmentMsg_hotfix;
    private LuaFunction m_OnEnhanceSucceedString_hotfix;
    private LuaFunction m_StopEnhanceSucceedEffect_hotfix;
    private LuaFunction m_SetSkillLevelUpEffectEquipmentBagItemStringStringInt32_hotfix;
    private LuaFunction m_DelayActiveIntensifyContinueButton_hotfix;
    private LuaFunction m_OnIntensifySuccessPanelBlackButtonClick_hotfix;
    private LuaFunction m_ShowEnhanceItemSRConfirmPanel_hotfix;
    private LuaFunction m_ShowEnhanceItemsConfirmPanel_hotfix;
    private LuaFunction m_OnEnhanceItemSRConfirmButtonClick_hotfix;
    private LuaFunction m_OnEnhanceItemSRCancelButtonClick_hotfix;
    private LuaFunction m_OnEnhanceConfirmButtonClick_hotfix;
    private LuaFunction m_OnEnhanceCancelButtonClick_hotfix;
    private LuaFunction m_OnEnhanceFastAddButtonClick_hotfix;
    private LuaFunction m_IsEquipmentAddExpAtMaxLevelMaxExpEquipmentBagItemInt32Int32_hotfix;
    private LuaFunction m_UpdateViewInBreakState_hotfix;
    private LuaFunction m_OnBreakButtonClick_hotfix;
    private LuaFunction m_OnBreakSucceed_hotfix;
    private LuaFunction m_DelayActiveBreakContinueButton_hotfix;
    private LuaFunction m_OnBreakSuccessPanelContinueButtonClick_hotfix;
    private LuaFunction m_OnEquipmentBreakNeedItemClickGoodsTypeInt32Int32_hotfix;
    private LuaFunction m_UpdateViewInEnchantmentState_hotfix;
    private LuaFunction m_OnEnchantmentButtonClick_hotfix;
    private LuaFunction m_ShowEnchantmentResultPanelInt32List`1Boolean_hotfix;
    private LuaFunction m_SetEnchantmentResultPanelInfoInt32List`1_hotfix;
    private LuaFunction m_Co_PlayNewPropertyEffectList`1_hotfix;
    private LuaFunction m_Co_DynamicSetPropertyValueTextImageInt32PropertyModifyType_hotfix;
    private LuaFunction m_CalcPropertyPercentPropertyModifyTypeInt32_hotfix;
    private LuaFunction m_OnEnchantmentResultPanelContinueButtonClick_hotfix;
    private LuaFunction m_CloseEnchantmentResultPanelAction_hotfix;
    private LuaFunction m_OnEnchantmentResultSaveButtonClick_hotfix;
    private LuaFunction m_OnEnchantmentAgainButtonClick_hotfix;
    private LuaFunction m_ShowSavePanel_hotfix;
    private LuaFunction m_CloseSavePanel_hotfix;
    private LuaFunction m_OnSavePanlConfirmClick_hotfix;
    private LuaFunction m_PlaySucceedEffect_hotfix;
    private LuaFunction m_OnCloseEnchantResultPanelButtonClick_hotfix;
    private LuaFunction m_ShowCancelPanel_hotfix;
    private LuaFunction m_CloseCancelPanel_hotfix;
    private LuaFunction m_OnCancelPanlConfirmClick_hotfix;
    private LuaFunction m_OnSavePropertyToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnCancelPanelToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnEnhanceToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnBreakToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnEnchantmentToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnEnchantLockMaskButtonClick_hotfix;
    private LuaFunction m_OnGoldAddButtonClick_hotfix;
    private LuaFunction m_SetPlayerResourceGold_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_GetFirstEquipmentGoInListForUserGuide_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnGoldAddButtonClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnGoldAddButtonClickAction`3_hotfix;
    private LuaFunction m_add_EventOnEnhanceButtonClickAction`4_hotfix;
    private LuaFunction m_remove_EventOnEnhanceButtonClickAction`4_hotfix;
    private LuaFunction m_add_EventOnBreakButtonClickAction`4_hotfix;
    private LuaFunction m_remove_EventOnBreakButtonClickAction`4_hotfix;
    private LuaFunction m_add_EventOnEnchantButtonClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnEnchantButtonClickAction`3_hotfix;
    private LuaFunction m_add_EventOnBreakNeedItemClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnBreakNeedItemClickAction`3_hotfix;
    private LuaFunction m_add_EventOnEnchantSaveButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnEnchantSaveButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnCrystalNotEnoughAction_hotfix;
    private LuaFunction m_remove_EventOnCrystalNotEnoughAction_hotfix;
    private LuaFunction m_add_EventOnLockButtonClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnLockButtonClickAction`3_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentForgeUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitSortTypePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoopScrollRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateEquipmentForgeView(
      EquipmentForgeUIController.ForgeState forgeState,
      int slot,
      ulong equipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateEquipmentList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortEquipmentListByType(List<EquipmentBagItem> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int DefaultEquipmentItemComparer(EquipmentBagItem e1, EquipmentBagItem e2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEquipmentPropValueAndSort(
      List<EquipmentBagItem> list,
      PropertyModifyType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentListItemClick(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentItemClickInEnhance(EquipmentDepotListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentItemClickInBreak(EquipmentDepotListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentItemClickInEnchantment(EquipmentDepotListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentListItemNeedFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseEquipmentDescPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentListItem3DTouch(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterSortButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentFilterSortButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseFilterSortTypeGo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseEnchantmentFilterSortTypeGo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterSortOrderButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterTypeButtonClick(EquipmentSortItemUIController ctrl, bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentFilterTypeButtonClick(EquipmentSortItemUIController ctrl, bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentItemDesc(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDescEquipmentLimit(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDescEquipmentSkill(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDescEquipmentEnchant(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItemColor(Text oldText, Text newText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewInEnhanceState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEnhancePropItems(
      PropertyModifyType type,
      int originValue,
      int perAddValue,
      int curLv,
      int addLv)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetEnhancePropItemActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CalculateEquipmentEnhanceAddLvByExp(
      EquipmentBagItem equipment,
      int enhanceExp,
      int curLv)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendEnhanceEquipmentMsg()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceSucceed(string oldSkillLevelStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopEnhanceSucceedEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillLevelUpEffect(
      EquipmentBagItem equipment,
      string oldLv,
      string newLv,
      int newSkillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayActiveIntensifyContinueButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnIntensifySuccessPanelBlackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowEnhanceItemSRConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowEnhanceItemsConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceItemSRConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceItemSRCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEnhanceFastAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEquipmentAddExpAtMaxLevelMaxExp(
      EquipmentBagItem equipment,
      int addLv,
      int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewInBreakState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBreakButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBreakSucceed()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayActiveBreakContinueButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBreakSuccessPanelContinueButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentBreakNeedItemClick(GoodsType type, int id, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewInEnchantmentState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowEnchantmentResultPanel(
      int newResonanceId,
      List<CommonBattleProperty> properties,
      bool isFirstShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEnchantmentResultPanelInfo(
      int newResonanceId,
      List<CommonBattleProperty> properties)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PlayNewPropertyEffect(List<CommonBattleProperty> properties)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_DynamicSetPropertyValue(
      Text valueText,
      Image progressBar,
      int value,
      PropertyModifyType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private float CalcPropertyPercent(PropertyModifyType propertyTypeId, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentResultPanelContinueButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseEnchantmentResultPanel(Action succeedEffectEvent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentResultSaveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentAgainButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSavePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSavePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSavePanlConfirmClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySucceedEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseEnchantResultPanelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCancelPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseCancelPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelPanlConfirmClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSavePropertyToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelPanelToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBreakToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantLockMaskButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoldAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerResourceGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject GetFirstEquipmentGoInListForUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, int, EquipmentForgeUIController.ForgeState> EventOnGoldAddButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, List<ulong>, Action, int> EventOnEnhanceButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, ulong, Action, int> EventOnBreakButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, ulong, Action<int, List<CommonBattleProperty>>> EventOnEnchantButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int> EventOnBreakNeedItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action> EventOnEnchantSaveButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCrystalNotEnough
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, EquipmentForgeUIController.ForgeState, Action> EventOnLockButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public EquipmentForgeUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGoldAddButtonClick(
      ulong arg1,
      int arg2,
      EquipmentForgeUIController.ForgeState arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGoldAddButtonClick(
      ulong arg1,
      int arg2,
      EquipmentForgeUIController.ForgeState arg3)
    {
      this.EventOnGoldAddButtonClick = (Action<ulong, int, EquipmentForgeUIController.ForgeState>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEnhanceButtonClick(
      ulong arg1,
      List<ulong> arg2,
      Action arg3,
      int arg4)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEnhanceButtonClick(
      ulong arg1,
      List<ulong> arg2,
      Action arg3,
      int arg4)
    {
      this.EventOnEnhanceButtonClick = (Action<ulong, List<ulong>, Action, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBreakButtonClick(ulong arg1, ulong arg2, Action arg3, int arg4)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBreakButtonClick(
      ulong arg1,
      ulong arg2,
      Action arg3,
      int arg4)
    {
      this.EventOnBreakButtonClick = (Action<ulong, ulong, Action, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEnchantButtonClick(
      ulong arg1,
      ulong arg2,
      Action<int, List<CommonBattleProperty>> arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEnchantButtonClick(
      ulong arg1,
      ulong arg2,
      Action<int, List<CommonBattleProperty>> arg3)
    {
      this.EventOnEnchantButtonClick = (Action<ulong, ulong, Action<int, List<CommonBattleProperty>>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBreakNeedItemClick(GoodsType arg1, int arg2, int arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBreakNeedItemClick(GoodsType arg1, int arg2, int arg3)
    {
      this.EventOnBreakNeedItemClick = (Action<GoodsType, int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEnchantSaveButtonClick(Action obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEnchantSaveButtonClick(Action obj)
    {
      this.EventOnEnchantSaveButtonClick = (Action<Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCrystalNotEnough()
    {
      this.EventOnCrystalNotEnough = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnLockButtonClick(
      ulong arg1,
      EquipmentForgeUIController.ForgeState arg2,
      Action arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnLockButtonClick(
      ulong arg1,
      EquipmentForgeUIController.ForgeState arg2,
      Action arg3)
    {
      this.EventOnLockButtonClick = (Action<ulong, EquipmentForgeUIController.ForgeState, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum ForgeState
    {
      Enhance,
      Break,
      Enchantment,
    }

    public class LuaExportHelper
    {
      private EquipmentForgeUIController m_owner;

      public LuaExportHelper(EquipmentForgeUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnGoldAddButtonClick(
        ulong arg1,
        int arg2,
        EquipmentForgeUIController.ForgeState arg3)
      {
        this.m_owner.__callDele_EventOnGoldAddButtonClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnGoldAddButtonClick(
        ulong arg1,
        int arg2,
        EquipmentForgeUIController.ForgeState arg3)
      {
        this.m_owner.__clearDele_EventOnGoldAddButtonClick(arg1, arg2, arg3);
      }

      public void __callDele_EventOnEnhanceButtonClick(
        ulong arg1,
        List<ulong> arg2,
        Action arg3,
        int arg4)
      {
        this.m_owner.__callDele_EventOnEnhanceButtonClick(arg1, arg2, arg3, arg4);
      }

      public void __clearDele_EventOnEnhanceButtonClick(
        ulong arg1,
        List<ulong> arg2,
        Action arg3,
        int arg4)
      {
        this.m_owner.__clearDele_EventOnEnhanceButtonClick(arg1, arg2, arg3, arg4);
      }

      public void __callDele_EventOnBreakButtonClick(
        ulong arg1,
        ulong arg2,
        Action arg3,
        int arg4)
      {
        this.m_owner.__callDele_EventOnBreakButtonClick(arg1, arg2, arg3, arg4);
      }

      public void __clearDele_EventOnBreakButtonClick(
        ulong arg1,
        ulong arg2,
        Action arg3,
        int arg4)
      {
        this.m_owner.__clearDele_EventOnBreakButtonClick(arg1, arg2, arg3, arg4);
      }

      public void __callDele_EventOnEnchantButtonClick(
        ulong arg1,
        ulong arg2,
        Action<int, List<CommonBattleProperty>> arg3)
      {
        this.m_owner.__callDele_EventOnEnchantButtonClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnEnchantButtonClick(
        ulong arg1,
        ulong arg2,
        Action<int, List<CommonBattleProperty>> arg3)
      {
        this.m_owner.__clearDele_EventOnEnchantButtonClick(arg1, arg2, arg3);
      }

      public void __callDele_EventOnBreakNeedItemClick(GoodsType arg1, int arg2, int arg3)
      {
        this.m_owner.__callDele_EventOnBreakNeedItemClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnBreakNeedItemClick(GoodsType arg1, int arg2, int arg3)
      {
        this.m_owner.__clearDele_EventOnBreakNeedItemClick(arg1, arg2, arg3);
      }

      public void __callDele_EventOnEnchantSaveButtonClick(Action obj)
      {
        this.m_owner.__callDele_EventOnEnchantSaveButtonClick(obj);
      }

      public void __clearDele_EventOnEnchantSaveButtonClick(Action obj)
      {
        this.m_owner.__clearDele_EventOnEnchantSaveButtonClick(obj);
      }

      public void __callDele_EventOnCrystalNotEnough()
      {
        this.m_owner.__callDele_EventOnCrystalNotEnough();
      }

      public void __clearDele_EventOnCrystalNotEnough()
      {
        this.m_owner.__clearDele_EventOnCrystalNotEnough();
      }

      public void __callDele_EventOnLockButtonClick(
        ulong arg1,
        EquipmentForgeUIController.ForgeState arg2,
        Action arg3)
      {
        this.m_owner.__callDele_EventOnLockButtonClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnLockButtonClick(
        ulong arg1,
        EquipmentForgeUIController.ForgeState arg2,
        Action arg3)
      {
        this.m_owner.__clearDele_EventOnLockButtonClick(arg1, arg2, arg3);
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Toggle m_enhanceToggle
      {
        get
        {
          return this.m_owner.m_enhanceToggle;
        }
        set
        {
          this.m_owner.m_enhanceToggle = value;
        }
      }

      public Toggle m_breakToggle
      {
        get
        {
          return this.m_owner.m_breakToggle;
        }
        set
        {
          this.m_owner.m_breakToggle = value;
        }
      }

      public Toggle m_enchantmentToggle
      {
        get
        {
          return this.m_owner.m_enchantmentToggle;
        }
        set
        {
          this.m_owner.m_enchantmentToggle = value;
        }
      }

      public Button m_enchantLockMaskButton
      {
        get
        {
          return this.m_owner.m_enchantLockMaskButton;
        }
        set
        {
          this.m_owner.m_enchantLockMaskButton = value;
        }
      }

      public Text m_goldText
      {
        get
        {
          return this.m_owner.m_goldText;
        }
        set
        {
          this.m_owner.m_goldText = value;
        }
      }

      public Button m_goldAddButton
      {
        get
        {
          return this.m_owner.m_goldAddButton;
        }
        set
        {
          this.m_owner.m_goldAddButton = value;
        }
      }

      public LoopVerticalScrollRect m_loopScrollView
      {
        get
        {
          return this.m_owner.m_loopScrollView;
        }
        set
        {
          this.m_owner.m_loopScrollView = value;
        }
      }

      public EasyObjectPool m_listItemPool
      {
        get
        {
          return this.m_owner.m_listItemPool;
        }
        set
        {
          this.m_owner.m_listItemPool = value;
        }
      }

      public GameObject m_listScrollViewItemRoot
      {
        get
        {
          return this.m_owner.m_listScrollViewItemRoot;
        }
        set
        {
          this.m_owner.m_listScrollViewItemRoot = value;
        }
      }

      public GameObject m_enchantmentFilter
      {
        get
        {
          return this.m_owner.m_enchantmentFilter;
        }
        set
        {
          this.m_owner.m_enchantmentFilter = value;
        }
      }

      public Button m_enchantmentfilterSortButton
      {
        get
        {
          return this.m_owner.m_enchantmentfilterSortButton;
        }
        set
        {
          this.m_owner.m_enchantmentfilterSortButton = value;
        }
      }

      public Text m_enchantmentfilterSortTypeText
      {
        get
        {
          return this.m_owner.m_enchantmentfilterSortTypeText;
        }
        set
        {
          this.m_owner.m_enchantmentfilterSortTypeText = value;
        }
      }

      public GameObject m_enchantmentfilterSortTypesGo
      {
        get
        {
          return this.m_owner.m_enchantmentfilterSortTypesGo;
        }
        set
        {
          this.m_owner.m_enchantmentfilterSortTypesGo = value;
        }
      }

      public GameObject m_enchantmentfilterSortTypesGridLayout
      {
        get
        {
          return this.m_owner.m_enchantmentfilterSortTypesGridLayout;
        }
        set
        {
          this.m_owner.m_enchantmentfilterSortTypesGridLayout = value;
        }
      }

      public GameObject m_enchantmentfilterSortTypesAllToggle
      {
        get
        {
          return this.m_owner.m_enchantmentfilterSortTypesAllToggle;
        }
        set
        {
          this.m_owner.m_enchantmentfilterSortTypesAllToggle = value;
        }
      }

      public GameObject m_enhanceAndBreakFilter
      {
        get
        {
          return this.m_owner.m_enhanceAndBreakFilter;
        }
        set
        {
          this.m_owner.m_enhanceAndBreakFilter = value;
        }
      }

      public Button m_filterSortButton
      {
        get
        {
          return this.m_owner.m_filterSortButton;
        }
        set
        {
          this.m_owner.m_filterSortButton = value;
        }
      }

      public Text m_filterSortTypeText
      {
        get
        {
          return this.m_owner.m_filterSortTypeText;
        }
        set
        {
          this.m_owner.m_filterSortTypeText = value;
        }
      }

      public Button m_filterSortOrderButton
      {
        get
        {
          return this.m_owner.m_filterSortOrderButton;
        }
        set
        {
          this.m_owner.m_filterSortOrderButton = value;
        }
      }

      public GameObject m_filterSortTypesGo
      {
        get
        {
          return this.m_owner.m_filterSortTypesGo;
        }
        set
        {
          this.m_owner.m_filterSortTypesGo = value;
        }
      }

      public GameObject m_filterSortTypesGridLayout
      {
        get
        {
          return this.m_owner.m_filterSortTypesGridLayout;
        }
        set
        {
          this.m_owner.m_filterSortTypesGridLayout = value;
        }
      }

      public GameObject m_listNoBreakItemGo
      {
        get
        {
          return this.m_owner.m_listNoBreakItemGo;
        }
        set
        {
          this.m_owner.m_listNoBreakItemGo = value;
        }
      }

      public GameObject m_listNoEnchantmentItemGo
      {
        get
        {
          return this.m_owner.m_listNoEnchantmentItemGo;
        }
        set
        {
          this.m_owner.m_listNoEnchantmentItemGo = value;
        }
      }

      public CommonUIStateController m_listTitleStateCtrl
      {
        get
        {
          return this.m_owner.m_listTitleStateCtrl;
        }
        set
        {
          this.m_owner.m_listTitleStateCtrl = value;
        }
      }

      public GameObject m_listLongPressTipGo
      {
        get
        {
          return this.m_owner.m_listLongPressTipGo;
        }
        set
        {
          this.m_owner.m_listLongPressTipGo = value;
        }
      }

      public GameObject m_descGo
      {
        get
        {
          return this.m_owner.m_descGo;
        }
        set
        {
          this.m_owner.m_descGo = value;
        }
      }

      public Button m_descReturnBgButton
      {
        get
        {
          return this.m_owner.m_descReturnBgButton;
        }
        set
        {
          this.m_owner.m_descReturnBgButton = value;
        }
      }

      public Button m_descLockButton
      {
        get
        {
          return this.m_owner.m_descLockButton;
        }
        set
        {
          this.m_owner.m_descLockButton = value;
        }
      }

      public CommonUIStateController m_descLockButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_descLockButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_descLockButtonStateCtrl = value;
        }
      }

      public Text m_descTitleText
      {
        get
        {
          return this.m_owner.m_descTitleText;
        }
        set
        {
          this.m_owner.m_descTitleText = value;
        }
      }

      public Image m_descIcon
      {
        get
        {
          return this.m_owner.m_descIcon;
        }
        set
        {
          this.m_owner.m_descIcon = value;
        }
      }

      public Image m_descIconBg
      {
        get
        {
          return this.m_owner.m_descIconBg;
        }
        set
        {
          this.m_owner.m_descIconBg = value;
        }
      }

      public GameObject m_descSSREffect
      {
        get
        {
          return this.m_owner.m_descSSREffect;
        }
        set
        {
          this.m_owner.m_descSSREffect = value;
        }
      }

      public GameObject m_descStarGroup
      {
        get
        {
          return this.m_owner.m_descStarGroup;
        }
        set
        {
          this.m_owner.m_descStarGroup = value;
        }
      }

      public Text m_descLvText
      {
        get
        {
          return this.m_owner.m_descLvText;
        }
        set
        {
          this.m_owner.m_descLvText = value;
        }
      }

      public Text m_descExpText
      {
        get
        {
          return this.m_owner.m_descExpText;
        }
        set
        {
          this.m_owner.m_descExpText = value;
        }
      }

      public Image m_descProgressImage
      {
        get
        {
          return this.m_owner.m_descProgressImage;
        }
        set
        {
          this.m_owner.m_descProgressImage = value;
        }
      }

      public GameObject m_descEquipLimitContent
      {
        get
        {
          return this.m_owner.m_descEquipLimitContent;
        }
        set
        {
          this.m_owner.m_descEquipLimitContent = value;
        }
      }

      public Text m_descEquipUnlimitText
      {
        get
        {
          return this.m_owner.m_descEquipUnlimitText;
        }
        set
        {
          this.m_owner.m_descEquipUnlimitText = value;
        }
      }

      public GameObject m_descSkillContent
      {
        get
        {
          return this.m_owner.m_descSkillContent;
        }
        set
        {
          this.m_owner.m_descSkillContent = value;
        }
      }

      public CommonUIStateController m_descSkillContentStateCtrl
      {
        get
        {
          return this.m_owner.m_descSkillContentStateCtrl;
        }
        set
        {
          this.m_owner.m_descSkillContentStateCtrl = value;
        }
      }

      public Text m_descUnlockCoditionText
      {
        get
        {
          return this.m_owner.m_descUnlockCoditionText;
        }
        set
        {
          this.m_owner.m_descUnlockCoditionText = value;
        }
      }

      public Text m_descSkillNameText
      {
        get
        {
          return this.m_owner.m_descSkillNameText;
        }
        set
        {
          this.m_owner.m_descSkillNameText = value;
        }
      }

      public Text m_descSkillLvText
      {
        get
        {
          return this.m_owner.m_descSkillLvText;
        }
        set
        {
          this.m_owner.m_descSkillLvText = value;
        }
      }

      public Text m_descSkillOwnerText
      {
        get
        {
          return this.m_owner.m_descSkillOwnerText;
        }
        set
        {
          this.m_owner.m_descSkillOwnerText = value;
        }
      }

      public GameObject m_descSkillOwnerBGImage
      {
        get
        {
          return this.m_owner.m_descSkillOwnerBGImage;
        }
        set
        {
          this.m_owner.m_descSkillOwnerBGImage = value;
        }
      }

      public Text m_descSkillDescText
      {
        get
        {
          return this.m_owner.m_descSkillDescText;
        }
        set
        {
          this.m_owner.m_descSkillDescText = value;
        }
      }

      public GameObject m_descNotEquipSkillTip
      {
        get
        {
          return this.m_owner.m_descNotEquipSkillTip;
        }
        set
        {
          this.m_owner.m_descNotEquipSkillTip = value;
        }
      }

      public GameObject m_descPropContent
      {
        get
        {
          return this.m_owner.m_descPropContent;
        }
        set
        {
          this.m_owner.m_descPropContent = value;
        }
      }

      public GameObject m_descPropATGo
      {
        get
        {
          return this.m_owner.m_descPropATGo;
        }
        set
        {
          this.m_owner.m_descPropATGo = value;
        }
      }

      public Text m_descPropATValueText
      {
        get
        {
          return this.m_owner.m_descPropATValueText;
        }
        set
        {
          this.m_owner.m_descPropATValueText = value;
        }
      }

      public GameObject m_descPropDFGo
      {
        get
        {
          return this.m_owner.m_descPropDFGo;
        }
        set
        {
          this.m_owner.m_descPropDFGo = value;
        }
      }

      public Text m_descPropDFValueText
      {
        get
        {
          return this.m_owner.m_descPropDFValueText;
        }
        set
        {
          this.m_owner.m_descPropDFValueText = value;
        }
      }

      public GameObject m_descPropHPGo
      {
        get
        {
          return this.m_owner.m_descPropHPGo;
        }
        set
        {
          this.m_owner.m_descPropHPGo = value;
        }
      }

      public Text m_descPropHPValueText
      {
        get
        {
          return this.m_owner.m_descPropHPValueText;
        }
        set
        {
          this.m_owner.m_descPropHPValueText = value;
        }
      }

      public GameObject m_descPropMagiccGo
      {
        get
        {
          return this.m_owner.m_descPropMagiccGo;
        }
        set
        {
          this.m_owner.m_descPropMagiccGo = value;
        }
      }

      public Text m_descPropMagicValueText
      {
        get
        {
          return this.m_owner.m_descPropMagicValueText;
        }
        set
        {
          this.m_owner.m_descPropMagicValueText = value;
        }
      }

      public GameObject m_descPropMagicDFGo
      {
        get
        {
          return this.m_owner.m_descPropMagicDFGo;
        }
        set
        {
          this.m_owner.m_descPropMagicDFGo = value;
        }
      }

      public Text m_descPropMagicDFValueText
      {
        get
        {
          return this.m_owner.m_descPropMagicDFValueText;
        }
        set
        {
          this.m_owner.m_descPropMagicDFValueText = value;
        }
      }

      public GameObject m_descPropDexGo
      {
        get
        {
          return this.m_owner.m_descPropDexGo;
        }
        set
        {
          this.m_owner.m_descPropDexGo = value;
        }
      }

      public Text m_descPropDexValueText
      {
        get
        {
          return this.m_owner.m_descPropDexValueText;
        }
        set
        {
          this.m_owner.m_descPropDexValueText = value;
        }
      }

      public CommonUIStateController m_descPropGroupStateCtrl
      {
        get
        {
          return this.m_owner.m_descPropGroupStateCtrl;
        }
        set
        {
          this.m_owner.m_descPropGroupStateCtrl = value;
        }
      }

      public GameObject m_descPropEnchantmentGroup
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroup;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroup = value;
        }
      }

      public CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroupRuneStateCtrl;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroupRuneStateCtrl = value;
        }
      }

      public Image m_descPropEnchantmentGroupRuneIconImage
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroupRuneIconImage;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroupRuneIconImage = value;
        }
      }

      public Text m_descPropEnchantmentGroupRuneNameText
      {
        get
        {
          return this.m_owner.m_descPropEnchantmentGroupRuneNameText;
        }
        set
        {
          this.m_owner.m_descPropEnchantmentGroupRuneNameText = value;
        }
      }

      public Text m_enhanceTitleText
      {
        get
        {
          return this.m_owner.m_enhanceTitleText;
        }
        set
        {
          this.m_owner.m_enhanceTitleText = value;
        }
      }

      public Image m_enhanceIcon
      {
        get
        {
          return this.m_owner.m_enhanceIcon;
        }
        set
        {
          this.m_owner.m_enhanceIcon = value;
        }
      }

      public Image m_enhanceIconBg
      {
        get
        {
          return this.m_owner.m_enhanceIconBg;
        }
        set
        {
          this.m_owner.m_enhanceIconBg = value;
        }
      }

      public GameObject m_enhanceIconSSREffect
      {
        get
        {
          return this.m_owner.m_enhanceIconSSREffect;
        }
        set
        {
          this.m_owner.m_enhanceIconSSREffect = value;
        }
      }

      public GameObject m_enhanceStarGroup
      {
        get
        {
          return this.m_owner.m_enhanceStarGroup;
        }
        set
        {
          this.m_owner.m_enhanceStarGroup = value;
        }
      }

      public Text m_enhanceCurLv
      {
        get
        {
          return this.m_owner.m_enhanceCurLv;
        }
        set
        {
          this.m_owner.m_enhanceCurLv = value;
        }
      }

      public Text m_enhanceCurLv1
      {
        get
        {
          return this.m_owner.m_enhanceCurLv1;
        }
        set
        {
          this.m_owner.m_enhanceCurLv1 = value;
        }
      }

      public Text m_enhanceMaxLV
      {
        get
        {
          return this.m_owner.m_enhanceMaxLV;
        }
        set
        {
          this.m_owner.m_enhanceMaxLV = value;
        }
      }

      public Image m_enhanceProgressImage
      {
        get
        {
          return this.m_owner.m_enhanceProgressImage;
        }
        set
        {
          this.m_owner.m_enhanceProgressImage = value;
        }
      }

      public Image m_enhanceWillGetExpImage
      {
        get
        {
          return this.m_owner.m_enhanceWillGetExpImage;
        }
        set
        {
          this.m_owner.m_enhanceWillGetExpImage = value;
        }
      }

      public Text m_enhanceExpCurValueText
      {
        get
        {
          return this.m_owner.m_enhanceExpCurValueText;
        }
        set
        {
          this.m_owner.m_enhanceExpCurValueText = value;
        }
      }

      public Text m_enhanceExpAddValueText
      {
        get
        {
          return this.m_owner.m_enhanceExpAddValueText;
        }
        set
        {
          this.m_owner.m_enhanceExpAddValueText = value;
        }
      }

      public Text m_enhanceExpNextLvText
      {
        get
        {
          return this.m_owner.m_enhanceExpNextLvText;
        }
        set
        {
          this.m_owner.m_enhanceExpNextLvText = value;
        }
      }

      public GameObject m_enhanceMaterialsContent
      {
        get
        {
          return this.m_owner.m_enhanceMaterialsContent;
        }
        set
        {
          this.m_owner.m_enhanceMaterialsContent = value;
        }
      }

      public Text m_enhanceGoldText
      {
        get
        {
          return this.m_owner.m_enhanceGoldText;
        }
        set
        {
          this.m_owner.m_enhanceGoldText = value;
        }
      }

      public Button m_enhanceButton
      {
        get
        {
          return this.m_owner.m_enhanceButton;
        }
        set
        {
          this.m_owner.m_enhanceButton = value;
        }
      }

      public GameObject m_enhanceSuccessEffectPanel
      {
        get
        {
          return this.m_owner.m_enhanceSuccessEffectPanel;
        }
        set
        {
          this.m_owner.m_enhanceSuccessEffectPanel = value;
        }
      }

      public Button m_enhanceSuccessEffectPanelCloseButton
      {
        get
        {
          return this.m_owner.m_enhanceSuccessEffectPanelCloseButton;
        }
        set
        {
          this.m_owner.m_enhanceSuccessEffectPanelCloseButton = value;
        }
      }

      public GameObject m_enhanceConfirmPanel
      {
        get
        {
          return this.m_owner.m_enhanceConfirmPanel;
        }
        set
        {
          this.m_owner.m_enhanceConfirmPanel = value;
        }
      }

      public Button m_enhanceConfirmButton
      {
        get
        {
          return this.m_owner.m_enhanceConfirmButton;
        }
        set
        {
          this.m_owner.m_enhanceConfirmButton = value;
        }
      }

      public Button m_enhanceCancelButton
      {
        get
        {
          return this.m_owner.m_enhanceCancelButton;
        }
        set
        {
          this.m_owner.m_enhanceCancelButton = value;
        }
      }

      public Text m_enhanceConfirmPanelTip1
      {
        get
        {
          return this.m_owner.m_enhanceConfirmPanelTip1;
        }
        set
        {
          this.m_owner.m_enhanceConfirmPanelTip1 = value;
        }
      }

      public Text m_enhanceConfirmPanelTip2
      {
        get
        {
          return this.m_owner.m_enhanceConfirmPanelTip2;
        }
        set
        {
          this.m_owner.m_enhanceConfirmPanelTip2 = value;
        }
      }

      public GameObject m_enhanceItemSRConfirmPanel
      {
        get
        {
          return this.m_owner.m_enhanceItemSRConfirmPanel;
        }
        set
        {
          this.m_owner.m_enhanceItemSRConfirmPanel = value;
        }
      }

      public Button m_enhanceItemSRConfirmButton
      {
        get
        {
          return this.m_owner.m_enhanceItemSRConfirmButton;
        }
        set
        {
          this.m_owner.m_enhanceItemSRConfirmButton = value;
        }
      }

      public Button m_enhanceItemSRCancelButton
      {
        get
        {
          return this.m_owner.m_enhanceItemSRCancelButton;
        }
        set
        {
          this.m_owner.m_enhanceItemSRCancelButton = value;
        }
      }

      public Text m_enhanceItemSRTip
      {
        get
        {
          return this.m_owner.m_enhanceItemSRTip;
        }
        set
        {
          this.m_owner.m_enhanceItemSRTip = value;
        }
      }

      public CommonUIStateController m_intensifySuccessPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_intensifySuccessPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_intensifySuccessPanelStateCtrl = value;
        }
      }

      public CommonUIStateController m_intensifySuccessPanelInfoStateCtrl
      {
        get
        {
          return this.m_owner.m_intensifySuccessPanelInfoStateCtrl;
        }
        set
        {
          this.m_owner.m_intensifySuccessPanelInfoStateCtrl = value;
        }
      }

      public Image m_intensifySuccessPanelInfoIconBg
      {
        get
        {
          return this.m_owner.m_intensifySuccessPanelInfoIconBg;
        }
        set
        {
          this.m_owner.m_intensifySuccessPanelInfoIconBg = value;
        }
      }

      public Image m_intensifySuccessPanelInfoIcon
      {
        get
        {
          return this.m_owner.m_intensifySuccessPanelInfoIcon;
        }
        set
        {
          this.m_owner.m_intensifySuccessPanelInfoIcon = value;
        }
      }

      public GameObject m_intensifySuccessPanelInfoStarGroup
      {
        get
        {
          return this.m_owner.m_intensifySuccessPanelInfoStarGroup;
        }
        set
        {
          this.m_owner.m_intensifySuccessPanelInfoStarGroup = value;
        }
      }

      public Text m_intensifySuccessPanelCurLvText
      {
        get
        {
          return this.m_owner.m_intensifySuccessPanelCurLvText;
        }
        set
        {
          this.m_owner.m_intensifySuccessPanelCurLvText = value;
        }
      }

      public Text m_intensifySuccessPanelNextLvText
      {
        get
        {
          return this.m_owner.m_intensifySuccessPanelNextLvText;
        }
        set
        {
          this.m_owner.m_intensifySuccessPanelNextLvText = value;
        }
      }

      public Text m_intensifySuccessPanelSkillNameText
      {
        get
        {
          return this.m_owner.m_intensifySuccessPanelSkillNameText;
        }
        set
        {
          this.m_owner.m_intensifySuccessPanelSkillNameText = value;
        }
      }

      public Text m_intensifySuccessPanelSkillDescText
      {
        get
        {
          return this.m_owner.m_intensifySuccessPanelSkillDescText;
        }
        set
        {
          this.m_owner.m_intensifySuccessPanelSkillDescText = value;
        }
      }

      public Button m_intensifySuccessPanelBlackButton
      {
        get
        {
          return this.m_owner.m_intensifySuccessPanelBlackButton;
        }
        set
        {
          this.m_owner.m_intensifySuccessPanelBlackButton = value;
        }
      }

      public GameObject m_enhancePropGroupGo
      {
        get
        {
          return this.m_owner.m_enhancePropGroupGo;
        }
        set
        {
          this.m_owner.m_enhancePropGroupGo = value;
        }
      }

      public GameObject m_enhancePropHpGo
      {
        get
        {
          return this.m_owner.m_enhancePropHpGo;
        }
        set
        {
          this.m_owner.m_enhancePropHpGo = value;
        }
      }

      public Text m_enhancePropHpOldValueText
      {
        get
        {
          return this.m_owner.m_enhancePropHpOldValueText;
        }
        set
        {
          this.m_owner.m_enhancePropHpOldValueText = value;
        }
      }

      public Text m_enhancePropHpNewValueText
      {
        get
        {
          return this.m_owner.m_enhancePropHpNewValueText;
        }
        set
        {
          this.m_owner.m_enhancePropHpNewValueText = value;
        }
      }

      public GameObject m_enhancePropATGo
      {
        get
        {
          return this.m_owner.m_enhancePropATGo;
        }
        set
        {
          this.m_owner.m_enhancePropATGo = value;
        }
      }

      public Text m_enhancePropATOldValueText
      {
        get
        {
          return this.m_owner.m_enhancePropATOldValueText;
        }
        set
        {
          this.m_owner.m_enhancePropATOldValueText = value;
        }
      }

      public Text m_enhancePropATNewValueText
      {
        get
        {
          return this.m_owner.m_enhancePropATNewValueText;
        }
        set
        {
          this.m_owner.m_enhancePropATNewValueText = value;
        }
      }

      public GameObject m_enhancePropDFGo
      {
        get
        {
          return this.m_owner.m_enhancePropDFGo;
        }
        set
        {
          this.m_owner.m_enhancePropDFGo = value;
        }
      }

      public Text m_enhancePropDFOldValueText
      {
        get
        {
          return this.m_owner.m_enhancePropDFOldValueText;
        }
        set
        {
          this.m_owner.m_enhancePropDFOldValueText = value;
        }
      }

      public Text m_enhancePropDFNewValueText
      {
        get
        {
          return this.m_owner.m_enhancePropDFNewValueText;
        }
        set
        {
          this.m_owner.m_enhancePropDFNewValueText = value;
        }
      }

      public GameObject m_enhancePropMagicGo
      {
        get
        {
          return this.m_owner.m_enhancePropMagicGo;
        }
        set
        {
          this.m_owner.m_enhancePropMagicGo = value;
        }
      }

      public Text m_enhancePropMagicOldValueText
      {
        get
        {
          return this.m_owner.m_enhancePropMagicOldValueText;
        }
        set
        {
          this.m_owner.m_enhancePropMagicOldValueText = value;
        }
      }

      public Text m_enhancePropMagicNewValueText
      {
        get
        {
          return this.m_owner.m_enhancePropMagicNewValueText;
        }
        set
        {
          this.m_owner.m_enhancePropMagicNewValueText = value;
        }
      }

      public GameObject m_enhancePropMagicDFGo
      {
        get
        {
          return this.m_owner.m_enhancePropMagicDFGo;
        }
        set
        {
          this.m_owner.m_enhancePropMagicDFGo = value;
        }
      }

      public Text m_enhancePropMagicDFOldValueText
      {
        get
        {
          return this.m_owner.m_enhancePropMagicDFOldValueText;
        }
        set
        {
          this.m_owner.m_enhancePropMagicDFOldValueText = value;
        }
      }

      public Text m_enhancePropMagicDFNewValueText
      {
        get
        {
          return this.m_owner.m_enhancePropMagicDFNewValueText;
        }
        set
        {
          this.m_owner.m_enhancePropMagicDFNewValueText = value;
        }
      }

      public GameObject m_enhancePropDEXGo
      {
        get
        {
          return this.m_owner.m_enhancePropDEXGo;
        }
        set
        {
          this.m_owner.m_enhancePropDEXGo = value;
        }
      }

      public Text m_enhancePropDEXOldValueText
      {
        get
        {
          return this.m_owner.m_enhancePropDEXOldValueText;
        }
        set
        {
          this.m_owner.m_enhancePropDEXOldValueText = value;
        }
      }

      public Text m_enhancePropDEXNewValueText
      {
        get
        {
          return this.m_owner.m_enhancePropDEXNewValueText;
        }
        set
        {
          this.m_owner.m_enhancePropDEXNewValueText = value;
        }
      }

      public CommonUIStateController m_enhancePropSkillStateCtrl
      {
        get
        {
          return this.m_owner.m_enhancePropSkillStateCtrl;
        }
        set
        {
          this.m_owner.m_enhancePropSkillStateCtrl = value;
        }
      }

      public Text m_enhancePropSkillNameText
      {
        get
        {
          return this.m_owner.m_enhancePropSkillNameText;
        }
        set
        {
          this.m_owner.m_enhancePropSkillNameText = value;
        }
      }

      public Text m_enhancePropSkillOldValueText
      {
        get
        {
          return this.m_owner.m_enhancePropSkillOldValueText;
        }
        set
        {
          this.m_owner.m_enhancePropSkillOldValueText = value;
        }
      }

      public Text m_enhancePropSkillNewValueText
      {
        get
        {
          return this.m_owner.m_enhancePropSkillNewValueText;
        }
        set
        {
          this.m_owner.m_enhancePropSkillNewValueText = value;
        }
      }

      public Text m_enhancePropSkillConditionText
      {
        get
        {
          return this.m_owner.m_enhancePropSkillConditionText;
        }
        set
        {
          this.m_owner.m_enhancePropSkillConditionText = value;
        }
      }

      public Button m_enhanceFastAddButton
      {
        get
        {
          return this.m_owner.m_enhanceFastAddButton;
        }
        set
        {
          this.m_owner.m_enhanceFastAddButton = value;
        }
      }

      public GameObject m_breakGo
      {
        get
        {
          return this.m_owner.m_breakGo;
        }
        set
        {
          this.m_owner.m_breakGo = value;
        }
      }

      public Text m_breakTitleText
      {
        get
        {
          return this.m_owner.m_breakTitleText;
        }
        set
        {
          this.m_owner.m_breakTitleText = value;
        }
      }

      public Image m_breakCurLvEquipmentIcon
      {
        get
        {
          return this.m_owner.m_breakCurLvEquipmentIcon;
        }
        set
        {
          this.m_owner.m_breakCurLvEquipmentIcon = value;
        }
      }

      public Image m_breakCurLvEquipmentIconBg
      {
        get
        {
          return this.m_owner.m_breakCurLvEquipmentIconBg;
        }
        set
        {
          this.m_owner.m_breakCurLvEquipmentIconBg = value;
        }
      }

      public GameObject m_breakCurLvEquipmentIconSSREffect
      {
        get
        {
          return this.m_owner.m_breakCurLvEquipmentIconSSREffect;
        }
        set
        {
          this.m_owner.m_breakCurLvEquipmentIconSSREffect = value;
        }
      }

      public GameObject m_breakCurLvEquipmentStarGroup
      {
        get
        {
          return this.m_owner.m_breakCurLvEquipmentStarGroup;
        }
        set
        {
          this.m_owner.m_breakCurLvEquipmentStarGroup = value;
        }
      }

      public Text m_breakCurLvEquipmentOldLvText
      {
        get
        {
          return this.m_owner.m_breakCurLvEquipmentOldLvText;
        }
        set
        {
          this.m_owner.m_breakCurLvEquipmentOldLvText = value;
        }
      }

      public Text m_breakCurLvEquipmentMaxLvText
      {
        get
        {
          return this.m_owner.m_breakCurLvEquipmentMaxLvText;
        }
        set
        {
          this.m_owner.m_breakCurLvEquipmentMaxLvText = value;
        }
      }

      public Image m_breakNextLvEquipmentIcon
      {
        get
        {
          return this.m_owner.m_breakNextLvEquipmentIcon;
        }
        set
        {
          this.m_owner.m_breakNextLvEquipmentIcon = value;
        }
      }

      public Image m_breakNextLvEquipmentIconBg
      {
        get
        {
          return this.m_owner.m_breakNextLvEquipmentIconBg;
        }
        set
        {
          this.m_owner.m_breakNextLvEquipmentIconBg = value;
        }
      }

      public GameObject m_breakNextLvEquipmentIconSSREffect
      {
        get
        {
          return this.m_owner.m_breakNextLvEquipmentIconSSREffect;
        }
        set
        {
          this.m_owner.m_breakNextLvEquipmentIconSSREffect = value;
        }
      }

      public GameObject m_breakNextLvEquipmentStarGroup
      {
        get
        {
          return this.m_owner.m_breakNextLvEquipmentStarGroup;
        }
        set
        {
          this.m_owner.m_breakNextLvEquipmentStarGroup = value;
        }
      }

      public Text m_breakNextLvEquipmentOldLvText
      {
        get
        {
          return this.m_owner.m_breakNextLvEquipmentOldLvText;
        }
        set
        {
          this.m_owner.m_breakNextLvEquipmentOldLvText = value;
        }
      }

      public Text m_breakNextLvEquipmentMaxLvText
      {
        get
        {
          return this.m_owner.m_breakNextLvEquipmentMaxLvText;
        }
        set
        {
          this.m_owner.m_breakNextLvEquipmentMaxLvText = value;
        }
      }

      public GameObject m_breakMaterialIconGo
      {
        get
        {
          return this.m_owner.m_breakMaterialIconGo;
        }
        set
        {
          this.m_owner.m_breakMaterialIconGo = value;
        }
      }

      public Image m_breakMaterialIcon
      {
        get
        {
          return this.m_owner.m_breakMaterialIcon;
        }
        set
        {
          this.m_owner.m_breakMaterialIcon = value;
        }
      }

      public Image m_breakMaterialIconBg
      {
        get
        {
          return this.m_owner.m_breakMaterialIconBg;
        }
        set
        {
          this.m_owner.m_breakMaterialIconBg = value;
        }
      }

      public GameObject m_breakMaterialSSREffect
      {
        get
        {
          return this.m_owner.m_breakMaterialSSREffect;
        }
        set
        {
          this.m_owner.m_breakMaterialSSREffect = value;
        }
      }

      public GameObject m_breakItemsContent
      {
        get
        {
          return this.m_owner.m_breakItemsContent;
        }
        set
        {
          this.m_owner.m_breakItemsContent = value;
        }
      }

      public Text m_breakGoldText
      {
        get
        {
          return this.m_owner.m_breakGoldText;
        }
        set
        {
          this.m_owner.m_breakGoldText = value;
        }
      }

      public Button m_breakButton
      {
        get
        {
          return this.m_owner.m_breakButton;
        }
        set
        {
          this.m_owner.m_breakButton = value;
        }
      }

      public Image m_breakCantIcon
      {
        get
        {
          return this.m_owner.m_breakCantIcon;
        }
        set
        {
          this.m_owner.m_breakCantIcon = value;
        }
      }

      public Image m_breakCantIconBg
      {
        get
        {
          return this.m_owner.m_breakCantIconBg;
        }
        set
        {
          this.m_owner.m_breakCantIconBg = value;
        }
      }

      public GameObject m_breakCantStarGroup
      {
        get
        {
          return this.m_owner.m_breakCantStarGroup;
        }
        set
        {
          this.m_owner.m_breakCantStarGroup = value;
        }
      }

      public Text m_breakCantTitleText
      {
        get
        {
          return this.m_owner.m_breakCantTitleText;
        }
        set
        {
          this.m_owner.m_breakCantTitleText = value;
        }
      }

      public GameObject m_breakSuccessPanel
      {
        get
        {
          return this.m_owner.m_breakSuccessPanel;
        }
        set
        {
          this.m_owner.m_breakSuccessPanel = value;
        }
      }

      public Button m_breakSuccessPanelBlackButton
      {
        get
        {
          return this.m_owner.m_breakSuccessPanelBlackButton;
        }
        set
        {
          this.m_owner.m_breakSuccessPanelBlackButton = value;
        }
      }

      public Image m_breakSuccessInfoIconBg
      {
        get
        {
          return this.m_owner.m_breakSuccessInfoIconBg;
        }
        set
        {
          this.m_owner.m_breakSuccessInfoIconBg = value;
        }
      }

      public Image m_breakSuccessInfoIcon
      {
        get
        {
          return this.m_owner.m_breakSuccessInfoIcon;
        }
        set
        {
          this.m_owner.m_breakSuccessInfoIcon = value;
        }
      }

      public GameObject m_breakSuccessInfoStarGroup
      {
        get
        {
          return this.m_owner.m_breakSuccessInfoStarGroup;
        }
        set
        {
          this.m_owner.m_breakSuccessInfoStarGroup = value;
        }
      }

      public Text m_breakSuccessInfoCurOldLvText
      {
        get
        {
          return this.m_owner.m_breakSuccessInfoCurOldLvText;
        }
        set
        {
          this.m_owner.m_breakSuccessInfoCurOldLvText = value;
        }
      }

      public Text m_breakSuccessInfoCurMaxLvText
      {
        get
        {
          return this.m_owner.m_breakSuccessInfoCurMaxLvText;
        }
        set
        {
          this.m_owner.m_breakSuccessInfoCurMaxLvText = value;
        }
      }

      public Text m_breakSuccessInfoNextOldLvText
      {
        get
        {
          return this.m_owner.m_breakSuccessInfoNextOldLvText;
        }
        set
        {
          this.m_owner.m_breakSuccessInfoNextOldLvText = value;
        }
      }

      public Text m_breakSuccessInfoNextMaxLvText
      {
        get
        {
          return this.m_owner.m_breakSuccessInfoNextMaxLvText;
        }
        set
        {
          this.m_owner.m_breakSuccessInfoNextMaxLvText = value;
        }
      }

      public Text m_enchantmentItemNameText
      {
        get
        {
          return this.m_owner.m_enchantmentItemNameText;
        }
        set
        {
          this.m_owner.m_enchantmentItemNameText = value;
        }
      }

      public Text m_enchantmentItemLevelValueText
      {
        get
        {
          return this.m_owner.m_enchantmentItemLevelValueText;
        }
        set
        {
          this.m_owner.m_enchantmentItemLevelValueText = value;
        }
      }

      public Image m_enchantmentIcon
      {
        get
        {
          return this.m_owner.m_enchantmentIcon;
        }
        set
        {
          this.m_owner.m_enchantmentIcon = value;
        }
      }

      public Image m_enchantmentIconBg
      {
        get
        {
          return this.m_owner.m_enchantmentIconBg;
        }
        set
        {
          this.m_owner.m_enchantmentIconBg = value;
        }
      }

      public GameObject m_enchantmentIconSSREffect
      {
        get
        {
          return this.m_owner.m_enchantmentIconSSREffect;
        }
        set
        {
          this.m_owner.m_enchantmentIconSSREffect = value;
        }
      }

      public GameObject m_enchantmentStarGroup
      {
        get
        {
          return this.m_owner.m_enchantmentStarGroup;
        }
        set
        {
          this.m_owner.m_enchantmentStarGroup = value;
        }
      }

      public GameObject m_enchantmentEnchantPropertyGroup
      {
        get
        {
          return this.m_owner.m_enchantmentEnchantPropertyGroup;
        }
        set
        {
          this.m_owner.m_enchantmentEnchantPropertyGroup = value;
        }
      }

      public CommonUIStateController m_enchantmentEnchantPropertyGroupStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentEnchantPropertyGroupStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentEnchantPropertyGroupStateCtrl = value;
        }
      }

      public CommonUIStateController m_enchantmentSuitInfoStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoStateCtrl = value;
        }
      }

      public CommonUIStateController m_enchantmentSuitInfoGroupStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoGroupStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoGroupStateCtrl = value;
        }
      }

      public CommonUIStateController m_enchantmentSuitInfoNowEffectStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoNowEffectStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoNowEffectStateCtrl = value;
        }
      }

      public CommonUIStateController m_enchantmentSuitInfoNowEffectRuneIconStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoNowEffectRuneIconStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoNowEffectRuneIconStateCtrl = value;
        }
      }

      public Image m_enchantmentSuitInfoNowEffectRuneActiveIcon
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoNowEffectRuneActiveIcon;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoNowEffectRuneActiveIcon = value;
        }
      }

      public Image m_enchantmentSuitInfoNowEffectRuneUnactiveIcon
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoNowEffectRuneUnactiveIcon;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoNowEffectRuneUnactiveIcon = value;
        }
      }

      public Text m_enchantmentSuitInfoNowEffectRuneNameText
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoNowEffectRuneNameText;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoNowEffectRuneNameText = value;
        }
      }

      public Text m_enchantmentSuitInfoNowEffectRune2SuitInfoText
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoNowEffectRune2SuitInfoText;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoNowEffectRune2SuitInfoText = value;
        }
      }

      public Text m_enchantmentSuitInfoNowEffectRune4SuitInfoText
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoNowEffectRune4SuitInfoText;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoNowEffectRune4SuitInfoText = value;
        }
      }

      public CommonUIStateController m_enchantmentSuitInfoAfterEffectStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoAfterEffectStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoAfterEffectStateCtrl = value;
        }
      }

      public Image m_enchantmentSuitInfoAfterEffectRuneUnactiveIcon
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoAfterEffectRuneUnactiveIcon;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoAfterEffectRuneUnactiveIcon = value;
        }
      }

      public Text m_enchantmentSuitInfoAfterEffectRuneNameText
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoAfterEffectRuneNameText;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoAfterEffectRuneNameText = value;
        }
      }

      public Text m_enchantmentSuitInfoAfterEffectRune2SuitInfoText
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoAfterEffectRune2SuitInfoText;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoAfterEffectRune2SuitInfoText = value;
        }
      }

      public Text m_enchantmentSuitInfoAfterEffectRune4SuitInfoText
      {
        get
        {
          return this.m_owner.m_enchantmentSuitInfoAfterEffectRune4SuitInfoText;
        }
        set
        {
          this.m_owner.m_enchantmentSuitInfoAfterEffectRune4SuitInfoText = value;
        }
      }

      public CommonUIStateController m_enchantmentMaterialGroupStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentMaterialGroupStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentMaterialGroupStateCtrl = value;
        }
      }

      public Image m_enchantmentStoneIconBg
      {
        get
        {
          return this.m_owner.m_enchantmentStoneIconBg;
        }
        set
        {
          this.m_owner.m_enchantmentStoneIconBg = value;
        }
      }

      public Image m_enchantmentStoneIcon
      {
        get
        {
          return this.m_owner.m_enchantmentStoneIcon;
        }
        set
        {
          this.m_owner.m_enchantmentStoneIcon = value;
        }
      }

      public Text m_enchantmentStoneNameText
      {
        get
        {
          return this.m_owner.m_enchantmentStoneNameText;
        }
        set
        {
          this.m_owner.m_enchantmentStoneNameText = value;
        }
      }

      public Text m_enchantmentStoneDescText
      {
        get
        {
          return this.m_owner.m_enchantmentStoneDescText;
        }
        set
        {
          this.m_owner.m_enchantmentStoneDescText = value;
        }
      }

      public CommonUIStateController m_enchantmentStoneValueGroupStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentStoneValueGroupStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentStoneValueGroupStateCtrl = value;
        }
      }

      public Text m_enchantmentStoneHaveCount
      {
        get
        {
          return this.m_owner.m_enchantmentStoneHaveCount;
        }
        set
        {
          this.m_owner.m_enchantmentStoneHaveCount = value;
        }
      }

      public Text m_enchantmentGoldenValueText
      {
        get
        {
          return this.m_owner.m_enchantmentGoldenValueText;
        }
        set
        {
          this.m_owner.m_enchantmentGoldenValueText = value;
        }
      }

      public CommonUIStateController m_enchantmentGoldenValueTextStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentGoldenValueTextStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentGoldenValueTextStateCtrl = value;
        }
      }

      public Button m_enchantmentButton
      {
        get
        {
          return this.m_owner.m_enchantmentButton;
        }
        set
        {
          this.m_owner.m_enchantmentButton = value;
        }
      }

      public CommonUIStateController m_enchantKeepPropertyGroup
      {
        get
        {
          return this.m_owner.m_enchantKeepPropertyGroup;
        }
        set
        {
          this.m_owner.m_enchantKeepPropertyGroup = value;
        }
      }

      public CommonUIStateController m_enchantSuccessEffectPanel
      {
        get
        {
          return this.m_owner.m_enchantSuccessEffectPanel;
        }
        set
        {
          this.m_owner.m_enchantSuccessEffectPanel = value;
        }
      }

      public CommonUIStateController m_enchantmentResultPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelStateCtrl = value;
        }
      }

      public Button m_enchantmentResultPanelCloseButton
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelCloseButton;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelCloseButton = value;
        }
      }

      public GameObject m_enchantmentResultPanelOldPropretyGroup
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelOldPropretyGroup;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelOldPropretyGroup = value;
        }
      }

      public CommonUIStateController m_enchantmentResultPanelOldSuitInfoStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelOldSuitInfoStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelOldSuitInfoStateCtrl = value;
        }
      }

      public Text m_enchantmentResultPanelOldSuitInfoNameText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelOldSuitInfoNameText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelOldSuitInfoNameText = value;
        }
      }

      public Text m_enchantmentResultPanelOld2SuitInfoText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelOld2SuitInfoText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelOld2SuitInfoText = value;
        }
      }

      public Text m_enchantmentResultPanelOld4SuitInfoText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelOld4SuitInfoText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelOld4SuitInfoText = value;
        }
      }

      public CommonUIStateController m_enchantmentResultPanelNewProprety1StateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewProprety1StateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewProprety1StateCtrl = value;
        }
      }

      public Image m_enchantmentResultPanelNewProprety1ProgressBar
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewProprety1ProgressBar;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewProprety1ProgressBar = value;
        }
      }

      public Text m_enchantmentResultPanelNewProprety1NameText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewProprety1NameText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewProprety1NameText = value;
        }
      }

      public Text m_enchantmentResultPanelNewProprety1ValueText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewProprety1ValueText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewProprety1ValueText = value;
        }
      }

      public CommonUIStateController m_enchantmentResultPanelNewProprety2StateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewProprety2StateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewProprety2StateCtrl = value;
        }
      }

      public Image m_enchantmentResultPanelNewProprety2ProgressBar
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewProprety2ProgressBar;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewProprety2ProgressBar = value;
        }
      }

      public Text m_enchantmentResultPanelNewProprety2NameText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewProprety2NameText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewProprety2NameText = value;
        }
      }

      public Text m_enchantmentResultPanelNewProprety2ValueText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewProprety2ValueText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewProprety2ValueText = value;
        }
      }

      public CommonUIStateController m_enchantmentResultPanelNewProprety3StateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewProprety3StateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewProprety3StateCtrl = value;
        }
      }

      public Image m_enchantmentResultPanelNewProprety3ProgressBar
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewProprety3ProgressBar;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewProprety3ProgressBar = value;
        }
      }

      public Text m_enchantmentResultPanelNewProprety3NameText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewProprety3NameText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewProprety3NameText = value;
        }
      }

      public Text m_enchantmentResultPanelNewProprety3ValueText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewProprety3ValueText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewProprety3ValueText = value;
        }
      }

      public Text m_enchantmentResultPanelNewSuitInfoNameText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNewSuitInfoNameText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNewSuitInfoNameText = value;
        }
      }

      public Text m_enchantmentResultPanelNew2SuitInfoText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNew2SuitInfoText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNew2SuitInfoText = value;
        }
      }

      public Text m_enchantmentResultPanelNew4SuitInfoText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNew4SuitInfoText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNew4SuitInfoText = value;
        }
      }

      public Button m_enchantmentResultPanelEnchantmentAgainButton
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelEnchantmentAgainButton;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelEnchantmentAgainButton = value;
        }
      }

      public CommonUIStateController m_enchantmentResultPanelEnchantmentAgainButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelEnchantmentAgainButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelEnchantmentAgainButtonStateCtrl = value;
        }
      }

      public Text m_enchantmentResultPanelNumberText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelNumberText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelNumberText = value;
        }
      }

      public Text m_enchantmentResultPanelGoldenNumberText
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelGoldenNumberText;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelGoldenNumberText = value;
        }
      }

      public CommonUIStateController m_enchantmentResultPanelGoldenNumberStateCtrl
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelGoldenNumberStateCtrl;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelGoldenNumberStateCtrl = value;
        }
      }

      public Image m_enchantmentResultPanelItemImage
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelItemImage;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelItemImage = value;
        }
      }

      public Button m_enchantmentResultPanelSavePropretyButton
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelSavePropretyButton;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelSavePropretyButton = value;
        }
      }

      public Button m_enchantmentResultPanelContinueButton
      {
        get
        {
          return this.m_owner.m_enchantmentResultPanelContinueButton;
        }
        set
        {
          this.m_owner.m_enchantmentResultPanelContinueButton = value;
        }
      }

      public CommonUIStateController m_savePropretyPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_savePropretyPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_savePropretyPanelStateCtrl = value;
        }
      }

      public Button m_savePropretyPanelCancelButton
      {
        get
        {
          return this.m_owner.m_savePropretyPanelCancelButton;
        }
        set
        {
          this.m_owner.m_savePropretyPanelCancelButton = value;
        }
      }

      public Button m_savePropretyPanelComfirmButton
      {
        get
        {
          return this.m_owner.m_savePropretyPanelComfirmButton;
        }
        set
        {
          this.m_owner.m_savePropretyPanelComfirmButton = value;
        }
      }

      public Toggle m_savePropretyPanelToggle
      {
        get
        {
          return this.m_owner.m_savePropretyPanelToggle;
        }
        set
        {
          this.m_owner.m_savePropretyPanelToggle = value;
        }
      }

      public CommonUIStateController m_cancelPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_cancelPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_cancelPanelStateCtrl = value;
        }
      }

      public Button m_cancelPanelCancelButton
      {
        get
        {
          return this.m_owner.m_cancelPanelCancelButton;
        }
        set
        {
          this.m_owner.m_cancelPanelCancelButton = value;
        }
      }

      public Button m_cancelPanelComfirmButton
      {
        get
        {
          return this.m_owner.m_cancelPanelComfirmButton;
        }
        set
        {
          this.m_owner.m_cancelPanelComfirmButton = value;
        }
      }

      public Toggle m_cancelPanelToggle
      {
        get
        {
          return this.m_owner.m_cancelPanelToggle;
        }
        set
        {
          this.m_owner.m_cancelPanelToggle = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public int m_slot
      {
        get
        {
          return this.m_owner.m_slot;
        }
        set
        {
          this.m_owner.m_slot = value;
        }
      }

      public int m_isAscend
      {
        get
        {
          return this.m_owner.m_isAscend;
        }
        set
        {
          this.m_owner.m_isAscend = value;
        }
      }

      public bool m_isFirstIn
      {
        get
        {
          return this.m_owner.m_isFirstIn;
        }
        set
        {
          this.m_owner.m_isFirstIn = value;
        }
      }

      public bool m_isToggleChanged
      {
        get
        {
          return this.m_owner.m_isToggleChanged;
        }
        set
        {
          this.m_owner.m_isToggleChanged = value;
        }
      }

      public EquipmentForgeUIController.ForgeState m_curForgeState
      {
        get
        {
          return this.m_owner.m_curForgeState;
        }
        set
        {
          this.m_owner.m_curForgeState = value;
        }
      }

      public ulong m_curEquipmentInstanceId
      {
        get
        {
          return this.m_owner.m_curEquipmentInstanceId;
        }
        set
        {
          this.m_owner.m_curEquipmentInstanceId = value;
        }
      }

      public ulong m_curDescEquipmentInstanceId
      {
        get
        {
          return this.m_owner.m_curDescEquipmentInstanceId;
        }
        set
        {
          this.m_owner.m_curDescEquipmentInstanceId = value;
        }
      }

      public ulong m_curBreakMaterialEquipmentId
      {
        get
        {
          return this.m_owner.m_curBreakMaterialEquipmentId;
        }
        set
        {
          this.m_owner.m_curBreakMaterialEquipmentId = value;
        }
      }

      public ulong m_curAddEnhanceEquipmentInstanceId
      {
        get
        {
          return this.m_owner.m_curAddEnhanceEquipmentInstanceId;
        }
        set
        {
          this.m_owner.m_curAddEnhanceEquipmentInstanceId = value;
        }
      }

      public BagItemBase m_curSelectEnchantStoneItem
      {
        get
        {
          return this.m_owner.m_curSelectEnchantStoneItem;
        }
        set
        {
          this.m_owner.m_curSelectEnchantStoneItem = value;
        }
      }

      public ConfigDataEnchantStoneInfo m_lastSelectEnchantStoneInfo
      {
        get
        {
          return this.m_owner.m_lastSelectEnchantStoneInfo;
        }
        set
        {
          this.m_owner.m_lastSelectEnchantStoneInfo = value;
        }
      }

      public List<ulong> m_enhanceEquipmentInstanceIds
      {
        get
        {
          return this.m_owner.m_enhanceEquipmentInstanceIds;
        }
        set
        {
          this.m_owner.m_enhanceEquipmentInstanceIds = value;
        }
      }

      public List<EquipmentBagItem> m_equipmentItemCache
      {
        get
        {
          return this.m_owner.m_equipmentItemCache;
        }
        set
        {
          this.m_owner.m_equipmentItemCache = value;
        }
      }

      public List<EnchantStoneBagItem> m_enchantStoneItemCache
      {
        get
        {
          return this.m_owner.m_enchantStoneItemCache;
        }
        set
        {
          this.m_owner.m_enchantStoneItemCache = value;
        }
      }

      public List<EquipmentDepotListItemUIController> m_equipmentForgeCtrlList
      {
        get
        {
          return this.m_owner.m_equipmentForgeCtrlList;
        }
        set
        {
          this.m_owner.m_equipmentForgeCtrlList = value;
        }
      }

      public EquipmentDepotUIController.EquipmentSortTypeState m_curEquipmentSortType
      {
        get
        {
          return this.m_owner.m_curEquipmentSortType;
        }
        set
        {
          this.m_owner.m_curEquipmentSortType = value;
        }
      }

      public int m_curEnchantmentSortTypeId
      {
        get
        {
          return this.m_owner.m_curEnchantmentSortTypeId;
        }
        set
        {
          this.m_owner.m_curEnchantmentSortTypeId = value;
        }
      }

      public List<CommonBattleProperty> m_properties
      {
        get
        {
          return this.m_owner.m_properties;
        }
        set
        {
          this.m_owner.m_properties = value;
        }
      }

      public bool m_isAfter3DTouch
      {
        get
        {
          return this.m_owner.m_isAfter3DTouch;
        }
        set
        {
          this.m_owner.m_isAfter3DTouch = value;
        }
      }

      public string m_oldSkillLevelStr
      {
        get
        {
          return this.m_owner.m_oldSkillLevelStr;
        }
        set
        {
          this.m_owner.m_oldSkillLevelStr = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void InitSortTypePanel()
      {
        this.m_owner.InitSortTypePanel();
      }

      public void InitLoopScrollRect()
      {
        this.m_owner.InitLoopScrollRect();
      }

      public void OnPoolObjectCreated(string poolName, GameObject go)
      {
        this.m_owner.OnPoolObjectCreated(poolName, go);
      }

      public void CreateEquipmentList()
      {
        this.m_owner.CreateEquipmentList();
      }

      public void SortEquipmentListByType(List<EquipmentBagItem> list)
      {
        this.m_owner.SortEquipmentListByType(list);
      }

      public int DefaultEquipmentItemComparer(EquipmentBagItem e1, EquipmentBagItem e2)
      {
        return this.m_owner.DefaultEquipmentItemComparer(e1, e2);
      }

      public void CollectEquipmentPropValueAndSort(
        List<EquipmentBagItem> list,
        PropertyModifyType type)
      {
        this.m_owner.CollectEquipmentPropValueAndSort(list, type);
      }

      public void OnEquipmentListItemClick(UIControllerBase itemCtrl)
      {
        this.m_owner.OnEquipmentListItemClick(itemCtrl);
      }

      public void OnEquipmentItemClickInEnhance(EquipmentDepotListItemUIController ctrl)
      {
        this.m_owner.OnEquipmentItemClickInEnhance(ctrl);
      }

      public void OnEquipmentItemClickInBreak(EquipmentDepotListItemUIController ctrl)
      {
        this.m_owner.OnEquipmentItemClickInBreak(ctrl);
      }

      public void OnEquipmentItemClickInEnchantment(EquipmentDepotListItemUIController ctrl)
      {
        this.m_owner.OnEquipmentItemClickInEnchantment(ctrl);
      }

      public void OnEquipmentListItemNeedFill(UIControllerBase itemCtrl)
      {
        this.m_owner.OnEquipmentListItemNeedFill(itemCtrl);
      }

      public void CloseEquipmentDescPanel()
      {
        this.m_owner.CloseEquipmentDescPanel();
      }

      public void OnEquipmentListItem3DTouch(UIControllerBase itemCtrl)
      {
        this.m_owner.OnEquipmentListItem3DTouch(itemCtrl);
      }

      public void OnFilterSortButtonClick()
      {
        this.m_owner.OnFilterSortButtonClick();
      }

      public void OnEnchantmentFilterSortButtonClick()
      {
        this.m_owner.OnEnchantmentFilterSortButtonClick();
      }

      public void OnCloseFilterSortTypeGo()
      {
        this.m_owner.OnCloseFilterSortTypeGo();
      }

      public void OnCloseEnchantmentFilterSortTypeGo()
      {
        this.m_owner.OnCloseEnchantmentFilterSortTypeGo();
      }

      public void OnFilterSortOrderButtonClick()
      {
        this.m_owner.OnFilterSortOrderButtonClick();
      }

      public void OnFilterTypeButtonClick(EquipmentSortItemUIController ctrl, bool isOn)
      {
        this.m_owner.OnFilterTypeButtonClick(ctrl, isOn);
      }

      public void OnEnchantmentFilterTypeButtonClick(EquipmentSortItemUIController ctrl, bool isOn)
      {
        this.m_owner.OnEnchantmentFilterTypeButtonClick(ctrl, isOn);
      }

      public void SetEquipmentItemDesc(EquipmentBagItem equipment)
      {
        this.m_owner.SetEquipmentItemDesc(equipment);
      }

      public void SetDescEquipmentLimit(EquipmentBagItem equipment)
      {
        this.m_owner.SetDescEquipmentLimit(equipment);
      }

      public void SetDescEquipmentSkill(EquipmentBagItem equipment)
      {
        this.m_owner.SetDescEquipmentSkill(equipment);
      }

      public void SetDescEquipmentEnchant(EquipmentBagItem equipment)
      {
        this.m_owner.SetDescEquipmentEnchant(equipment);
      }

      public void SetPropItemColor(Text oldText, Text newText)
      {
        this.m_owner.SetPropItemColor(oldText, newText);
      }

      public void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
      {
        this.m_owner.SetPropItems(type, value, addValue, level);
      }

      public void OnDescLockButtonClick()
      {
        this.m_owner.OnDescLockButtonClick();
      }

      public void UpdateViewInEnhanceState()
      {
        this.m_owner.UpdateViewInEnhanceState();
      }

      public void SetEnhancePropItems(
        PropertyModifyType type,
        int originValue,
        int perAddValue,
        int curLv,
        int addLv)
      {
        this.m_owner.SetEnhancePropItems(type, originValue, perAddValue, curLv, addLv);
      }

      public void ResetEnhancePropItemActive()
      {
        this.m_owner.ResetEnhancePropItemActive();
      }

      public int CalculateEquipmentEnhanceAddLvByExp(
        EquipmentBagItem equipment,
        int enhanceExp,
        int curLv)
      {
        return this.m_owner.CalculateEquipmentEnhanceAddLvByExp(equipment, enhanceExp, curLv);
      }

      public void OnEnhanceButtonClick()
      {
        this.m_owner.OnEnhanceButtonClick();
      }

      public void SendEnhanceEquipmentMsg()
      {
        this.m_owner.SendEnhanceEquipmentMsg();
      }

      public void OnEnhanceSucceed(string oldSkillLevelStr)
      {
        this.m_owner.OnEnhanceSucceed(oldSkillLevelStr);
      }

      public void StopEnhanceSucceedEffect()
      {
        this.m_owner.StopEnhanceSucceedEffect();
      }

      public void SetSkillLevelUpEffect(
        EquipmentBagItem equipment,
        string oldLv,
        string newLv,
        int newSkillId)
      {
        this.m_owner.SetSkillLevelUpEffect(equipment, oldLv, newLv, newSkillId);
      }

      public IEnumerator DelayActiveIntensifyContinueButton()
      {
        return this.m_owner.DelayActiveIntensifyContinueButton();
      }

      public void OnIntensifySuccessPanelBlackButtonClick()
      {
        this.m_owner.OnIntensifySuccessPanelBlackButtonClick();
      }

      public void ShowEnhanceItemSRConfirmPanel()
      {
        this.m_owner.ShowEnhanceItemSRConfirmPanel();
      }

      public void ShowEnhanceItemsConfirmPanel()
      {
        this.m_owner.ShowEnhanceItemsConfirmPanel();
      }

      public void OnEnhanceItemSRConfirmButtonClick()
      {
        this.m_owner.OnEnhanceItemSRConfirmButtonClick();
      }

      public void OnEnhanceItemSRCancelButtonClick()
      {
        this.m_owner.OnEnhanceItemSRCancelButtonClick();
      }

      public void OnEnhanceConfirmButtonClick()
      {
        this.m_owner.OnEnhanceConfirmButtonClick();
      }

      public void OnEnhanceCancelButtonClick()
      {
        this.m_owner.OnEnhanceCancelButtonClick();
      }

      public bool IsEquipmentAddExpAtMaxLevelMaxExp(
        EquipmentBagItem equipment,
        int addLv,
        int addExp)
      {
        return this.m_owner.IsEquipmentAddExpAtMaxLevelMaxExp(equipment, addLv, addExp);
      }

      public void UpdateViewInBreakState()
      {
        this.m_owner.UpdateViewInBreakState();
      }

      public void OnBreakButtonClick()
      {
        this.m_owner.OnBreakButtonClick();
      }

      public void OnBreakSucceed()
      {
        this.m_owner.OnBreakSucceed();
      }

      public IEnumerator DelayActiveBreakContinueButton()
      {
        return this.m_owner.DelayActiveBreakContinueButton();
      }

      public void OnBreakSuccessPanelContinueButtonClick()
      {
        this.m_owner.OnBreakSuccessPanelContinueButtonClick();
      }

      public void OnEquipmentBreakNeedItemClick(GoodsType type, int id, int count)
      {
        this.m_owner.OnEquipmentBreakNeedItemClick(type, id, count);
      }

      public void UpdateViewInEnchantmentState()
      {
        this.m_owner.UpdateViewInEnchantmentState();
      }

      public void OnEnchantmentButtonClick()
      {
        this.m_owner.OnEnchantmentButtonClick();
      }

      public void ShowEnchantmentResultPanel(
        int newResonanceId,
        List<CommonBattleProperty> properties,
        bool isFirstShow)
      {
        this.m_owner.ShowEnchantmentResultPanel(newResonanceId, properties, isFirstShow);
      }

      public void SetEnchantmentResultPanelInfo(
        int newResonanceId,
        List<CommonBattleProperty> properties)
      {
        this.m_owner.SetEnchantmentResultPanelInfo(newResonanceId, properties);
      }

      public IEnumerator Co_PlayNewPropertyEffect(List<CommonBattleProperty> properties)
      {
        return this.m_owner.Co_PlayNewPropertyEffect(properties);
      }

      public IEnumerator Co_DynamicSetPropertyValue(
        Text valueText,
        Image progressBar,
        int value,
        PropertyModifyType type)
      {
        return this.m_owner.Co_DynamicSetPropertyValue(valueText, progressBar, value, type);
      }

      public float CalcPropertyPercent(PropertyModifyType propertyTypeId, int value)
      {
        return this.m_owner.CalcPropertyPercent(propertyTypeId, value);
      }

      public void OnEnchantmentResultPanelContinueButtonClick()
      {
        this.m_owner.OnEnchantmentResultPanelContinueButtonClick();
      }

      public void CloseEnchantmentResultPanel(Action succeedEffectEvent)
      {
        this.m_owner.CloseEnchantmentResultPanel(succeedEffectEvent);
      }

      public void OnEnchantmentResultSaveButtonClick()
      {
        this.m_owner.OnEnchantmentResultSaveButtonClick();
      }

      public void OnEnchantmentAgainButtonClick()
      {
        this.m_owner.OnEnchantmentAgainButtonClick();
      }

      public void ShowSavePanel()
      {
        this.m_owner.ShowSavePanel();
      }

      public void CloseSavePanel()
      {
        this.m_owner.CloseSavePanel();
      }

      public void OnSavePanlConfirmClick()
      {
        this.m_owner.OnSavePanlConfirmClick();
      }

      public void PlaySucceedEffect()
      {
        this.m_owner.PlaySucceedEffect();
      }

      public void OnCloseEnchantResultPanelButtonClick()
      {
        this.m_owner.OnCloseEnchantResultPanelButtonClick();
      }

      public void ShowCancelPanel()
      {
        this.m_owner.ShowCancelPanel();
      }

      public void CloseCancelPanel()
      {
        this.m_owner.CloseCancelPanel();
      }

      public void OnCancelPanlConfirmClick()
      {
        this.m_owner.OnCancelPanlConfirmClick();
      }

      public void OnSavePropertyToggleValueChanged(bool isOn)
      {
        this.m_owner.OnSavePropertyToggleValueChanged(isOn);
      }

      public void OnCancelPanelToggleValueChanged(bool isOn)
      {
        this.m_owner.OnCancelPanelToggleValueChanged(isOn);
      }

      public void OnEnhanceToggleValueChanged(bool isOn)
      {
        this.m_owner.OnEnhanceToggleValueChanged(isOn);
      }

      public void OnBreakToggleValueChanged(bool isOn)
      {
        this.m_owner.OnBreakToggleValueChanged(isOn);
      }

      public void OnEnchantmentToggleValueChanged(bool isOn)
      {
        this.m_owner.OnEnchantmentToggleValueChanged(isOn);
      }

      public void OnEnchantLockMaskButtonClick()
      {
        this.m_owner.OnEnchantLockMaskButtonClick();
      }

      public void OnGoldAddButtonClick()
      {
        this.m_owner.OnGoldAddButtonClick();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }
    }
  }
}
