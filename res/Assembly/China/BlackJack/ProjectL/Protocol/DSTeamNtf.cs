﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSTeamNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "DSTeamNtf")]
  [Serializable]
  public class DSTeamNtf : IExtensible
  {
    private int _RoomId;
    private int _GameFunctionTypeId;
    private int _LocationId;
    private int _WaitingFunctionTypeId;
    private int _WaitingLocationId;
    private readonly List<ProTeamRoomInviteInfo> _InviteInfos;
    private long _LastInviteSendTime;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSTeamNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "RoomId")]
    [DefaultValue(0)]
    public int RoomId
    {
      get
      {
        return this._RoomId;
      }
      set
      {
        this._RoomId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "GameFunctionTypeId")]
    [DefaultValue(0)]
    public int GameFunctionTypeId
    {
      get
      {
        return this._GameFunctionTypeId;
      }
      set
      {
        this._GameFunctionTypeId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "LocationId")]
    [DefaultValue(0)]
    public int LocationId
    {
      get
      {
        return this._LocationId;
      }
      set
      {
        this._LocationId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "WaitingFunctionTypeId")]
    [DefaultValue(0)]
    public int WaitingFunctionTypeId
    {
      get
      {
        return this._WaitingFunctionTypeId;
      }
      set
      {
        this._WaitingFunctionTypeId = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "WaitingLocationId")]
    [DefaultValue(0)]
    public int WaitingLocationId
    {
      get
      {
        return this._WaitingLocationId;
      }
      set
      {
        this._WaitingLocationId = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "InviteInfos")]
    public List<ProTeamRoomInviteInfo> InviteInfos
    {
      get
      {
        return this._InviteInfos;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "LastInviteSendTime")]
    public long LastInviteSendTime
    {
      get
      {
        return this._LastInviteSendTime;
      }
      set
      {
        this._LastInviteSendTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
