﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.OpenSelfSelectedBoxReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "OpenSelfSelectedBoxReq")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class OpenSelfSelectedBoxReq : IExtensible
  {
    private int _Index;
    private int _GoodsTypeId;
    private int _ContentId;
    private int _Nums;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public OpenSelfSelectedBoxReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Index")]
    public int Index
    {
      get
      {
        return this._Index;
      }
      set
      {
        this._Index = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GoodsTypeId")]
    public int GoodsTypeId
    {
      get
      {
        return this._GoodsTypeId;
      }
      set
      {
        this._GoodsTypeId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ContentId")]
    public int ContentId
    {
      get
      {
        return this._ContentId;
      }
      set
      {
        this._ContentId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Nums")]
    public int Nums
    {
      get
      {
        return this._Nums;
      }
      set
      {
        this._Nums = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
