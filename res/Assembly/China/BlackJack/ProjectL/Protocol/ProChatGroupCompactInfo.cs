﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProChatGroupCompactInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProChatGroupCompactInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProChatGroupCompactInfo : IExtensible
  {
    private string _ChatGroupId;
    private ProChatUserCompactInfo _Owner;
    private string _ChatGroupName;
    private int _UserCount;
    private int _OnlineUserCount;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatGroupCompactInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "ChatGroupId")]
    public string ChatGroupId
    {
      get
      {
        return this._ChatGroupId;
      }
      set
      {
        this._ChatGroupId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "Owner")]
    public ProChatUserCompactInfo Owner
    {
      get
      {
        return this._Owner;
      }
      set
      {
        this._Owner = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "ChatGroupName")]
    public string ChatGroupName
    {
      get
      {
        return this._ChatGroupName;
      }
      set
      {
        this._ChatGroupName = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UserCount")]
    public int UserCount
    {
      get
      {
        return this._UserCount;
      }
      set
      {
        this._UserCount = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OnlineUserCount")]
    public int OnlineUserCount
    {
      get
      {
        return this._OnlineUserCount;
      }
      set
      {
        this._OnlineUserCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
