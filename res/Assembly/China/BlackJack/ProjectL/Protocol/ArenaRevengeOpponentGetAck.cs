﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ArenaRevengeOpponentGetAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ArenaRevengeOpponentGetAck")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ArenaRevengeOpponentGetAck : IExtensible
  {
    private int _Result;
    private ulong _ArenaBattleReportInstanceId;
    private ProArenaOpponent _Opponent;
    private readonly List<ProBattleHero> _Heroes;
    private int _BattlePower;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaRevengeOpponentGetAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaBattleReportInstanceId")]
    public ulong ArenaBattleReportInstanceId
    {
      get
      {
        return this._ArenaBattleReportInstanceId;
      }
      set
      {
        this._ArenaBattleReportInstanceId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "Opponent")]
    [DefaultValue(null)]
    public ProArenaOpponent Opponent
    {
      get
      {
        return this._Opponent;
      }
      set
      {
        this._Opponent = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "Heroes")]
    public List<ProBattleHero> Heroes
    {
      get
      {
        return this._Heroes;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattlePower")]
    [DefaultValue(0)]
    public int BattlePower
    {
      get
      {
        return this._BattlePower;
      }
      set
      {
        this._BattlePower = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
