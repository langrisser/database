﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBattleHeroEquipment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProBattleHeroEquipment")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProBattleHeroEquipment : IExtensible
  {
    private int _Id;
    private int _Level;
    private int _Exp;
    private int _StarLevel;
    private readonly List<ProCommonBattleProperty> _EnchantProperties;
    private int _ResonanceId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleHeroEquipment()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Id")]
    public int Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Exp")]
    public int Exp
    {
      get
      {
        return this._Exp;
      }
      set
      {
        this._Exp = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StarLevel")]
    public int StarLevel
    {
      get
      {
        return this._StarLevel;
      }
      set
      {
        this._StarLevel = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "EnchantProperties")]
    public List<ProCommonBattleProperty> EnchantProperties
    {
      get
      {
        return this._EnchantProperties;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ResonanceId")]
    [DefaultValue(0)]
    public int ResonanceId
    {
      get
      {
        return this._ResonanceId;
      }
      set
      {
        this._ResonanceId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
