﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBattleCommand
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProBattleCommand")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProBattleCommand : IExtensible
  {
    private int _CommandType;
    private int _ActorId;
    private int _Step;
    private ProBattlePosition _TargetPosition;
    private ProBattlePosition _TargetPosition2;
    private int _SkillIndex;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CommandType")]
    public int CommandType
    {
      get
      {
        return this._CommandType;
      }
      set
      {
        this._CommandType = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActorId")]
    public int ActorId
    {
      get
      {
        return this._ActorId;
      }
      set
      {
        this._ActorId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Step")]
    public int Step
    {
      get
      {
        return this._Step;
      }
      set
      {
        this._Step = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "TargetPosition")]
    public ProBattlePosition TargetPosition
    {
      get
      {
        return this._TargetPosition;
      }
      set
      {
        this._TargetPosition = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "TargetPosition2")]
    public ProBattlePosition TargetPosition2
    {
      get
      {
        return this._TargetPosition2;
      }
      set
      {
        this._TargetPosition2 = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkillIndex")]
    public int SkillIndex
    {
      get
      {
        return this._SkillIndex;
      }
      set
      {
        this._SkillIndex = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
