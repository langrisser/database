﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.FansRewardsFromPBTCBTInfoAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "FansRewardsFromPBTCBTInfoAck")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class FansRewardsFromPBTCBTInfoAck : IExtensible
  {
    private int _SkinId;
    private int _HeadFrameId;
    private bool _Claimed;
    private long _StartDate;
    private long _EndDate;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FansRewardsFromPBTCBTInfoAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkinId")]
    public int SkinId
    {
      get
      {
        return this._SkinId;
      }
      set
      {
        this._SkinId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeadFrameId")]
    public int HeadFrameId
    {
      get
      {
        return this._HeadFrameId;
      }
      set
      {
        this._HeadFrameId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Claimed")]
    public bool Claimed
    {
      get
      {
        return this._Claimed;
      }
      set
      {
        this._Claimed = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StartDate")]
    public long StartDate
    {
      get
      {
        return this._StartDate;
      }
      set
      {
        this._StartDate = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EndDate")]
    public long EndDate
    {
      get
      {
        return this._EndDate;
      }
      set
      {
        this._EndDate = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
