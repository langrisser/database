﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPeakArenaPlayOffMatchupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProPeakArenaPlayOffMatchupInfo")]
  [Serializable]
  public class ProPeakArenaPlayOffMatchupInfo : IExtensible
  {
    private int _Round;
    private int _MatchupId;
    private ProPeakArenaPlayOffPlayerInfo _PlayerOnLeft;
    private ProPeakArenaPlayOffPlayerInfo _PlayerOnRight;
    private string _WinnerId;
    private readonly List<ProPeakArenaPlayOffBattleInfo> _BattleInfos;
    private ulong _BattleReportId;
    private int _NextMatchupId;
    private int _PrevLeftMatchupId;
    private int _PrevRightMatchupId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaPlayOffMatchupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Round")]
    public int Round
    {
      get
      {
        return this._Round;
      }
      set
      {
        this._Round = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MatchupId")]
    public int MatchupId
    {
      get
      {
        return this._MatchupId;
      }
      set
      {
        this._MatchupId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayerOnLeft")]
    public ProPeakArenaPlayOffPlayerInfo PlayerOnLeft
    {
      get
      {
        return this._PlayerOnLeft;
      }
      set
      {
        this._PlayerOnLeft = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayerOnRight")]
    public ProPeakArenaPlayOffPlayerInfo PlayerOnRight
    {
      get
      {
        return this._PlayerOnRight;
      }
      set
      {
        this._PlayerOnRight = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "WinnerId")]
    public string WinnerId
    {
      get
      {
        return this._WinnerId;
      }
      set
      {
        this._WinnerId = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "BattleInfos")]
    public List<ProPeakArenaPlayOffBattleInfo> BattleInfos
    {
      get
      {
        return this._BattleInfos;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleReportId")]
    public ulong BattleReportId
    {
      get
      {
        return this._BattleReportId;
      }
      set
      {
        this._BattleReportId = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextMatchupId")]
    public int NextMatchupId
    {
      get
      {
        return this._NextMatchupId;
      }
      set
      {
        this._NextMatchupId = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PrevLeftMatchupId")]
    public int PrevLeftMatchupId
    {
      get
      {
        return this._PrevLeftMatchupId;
      }
      set
      {
        this._PrevLeftMatchupId = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PrevRightMatchupId")]
    public int PrevRightMatchupId
    {
      get
      {
        return this._PrevRightMatchupId;
      }
      set
      {
        this._PrevRightMatchupId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
