﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProRafflePool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProRafflePool")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProRafflePool : IExtensible
  {
    private int _RafflePoolId;
    private int _DrawedCount;
    private readonly List<int> _DrawedItemIds;
    private int _FreeDrawedCount;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRafflePool()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RafflePoolId")]
    public int RafflePoolId
    {
      get
      {
        return this._RafflePoolId;
      }
      set
      {
        this._RafflePoolId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DrawedCount")]
    public int DrawedCount
    {
      get
      {
        return this._DrawedCount;
      }
      set
      {
        this._DrawedCount = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "DrawedItemIds")]
    public List<int> DrawedItemIds
    {
      get
      {
        return this._DrawedItemIds;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FreeDrawedCount")]
    public int FreeDrawedCount
    {
      get
      {
        return this._FreeDrawedCount;
      }
      set
      {
        this._FreeDrawedCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
