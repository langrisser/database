﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.BattleRoomPeakArenaBattleFinishNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "BattleRoomPeakArenaBattleFinishNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class BattleRoomPeakArenaBattleFinishNtf : IExtensible
  {
    private int _WinPlayerIndex;
    private int _Mode;
    private int _DanDiff;
    private int _RankDiff;
    private ulong _RoomId;
    private int _NewScore;
    private ProPeakArenaMatchStats _FriendlyMatchStats;
    private ProPeakArenaMatchStats _LadderMatchStats;
    private ProPeakArenaMatchStats _FriendlyCareerMatchStats;
    private ProPeakArenaMatchStats _LadderCareerMatchStats;
    private int _ScoreDiff;
    private bool _DanProtectionTriggered;
    private bool _DanGroupProtectionTriggered;
    private ulong _InstanceId;
    private ProCommonBattleReport _BattleReport;
    private ProPeakArenaBattleReportNameRecord _Record;
    private readonly List<int> _MinePickedHeroes;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPeakArenaBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WinPlayerIndex")]
    public int WinPlayerIndex
    {
      get
      {
        return this._WinPlayerIndex;
      }
      set
      {
        this._WinPlayerIndex = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Mode")]
    public int Mode
    {
      get
      {
        return this._Mode;
      }
      set
      {
        this._Mode = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanDiff")]
    public int DanDiff
    {
      get
      {
        return this._DanDiff;
      }
      set
      {
        this._DanDiff = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RankDiff")]
    public int RankDiff
    {
      get
      {
        return this._RankDiff;
      }
      set
      {
        this._RankDiff = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomId")]
    public ulong RoomId
    {
      get
      {
        return this._RoomId;
      }
      set
      {
        this._RoomId = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NewScore")]
    public int NewScore
    {
      get
      {
        return this._NewScore;
      }
      set
      {
        this._NewScore = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "FriendlyMatchStats")]
    public ProPeakArenaMatchStats FriendlyMatchStats
    {
      get
      {
        return this._FriendlyMatchStats;
      }
      set
      {
        this._FriendlyMatchStats = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "LadderMatchStats")]
    public ProPeakArenaMatchStats LadderMatchStats
    {
      get
      {
        return this._LadderMatchStats;
      }
      set
      {
        this._LadderMatchStats = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "FriendlyCareerMatchStats")]
    public ProPeakArenaMatchStats FriendlyCareerMatchStats
    {
      get
      {
        return this._FriendlyCareerMatchStats;
      }
      set
      {
        this._FriendlyCareerMatchStats = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "LadderCareerMatchStats")]
    public ProPeakArenaMatchStats LadderCareerMatchStats
    {
      get
      {
        return this._LadderCareerMatchStats;
      }
      set
      {
        this._LadderCareerMatchStats = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScoreDiff")]
    public int ScoreDiff
    {
      get
      {
        return this._ScoreDiff;
      }
      set
      {
        this._ScoreDiff = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "DanProtectionTriggered")]
    public bool DanProtectionTriggered
    {
      get
      {
        return this._DanProtectionTriggered;
      }
      set
      {
        this._DanProtectionTriggered = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "DanGroupProtectionTriggered")]
    public bool DanGroupProtectionTriggered
    {
      get
      {
        return this._DanGroupProtectionTriggered;
      }
      set
      {
        this._DanGroupProtectionTriggered = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "InstanceId")]
    public ulong InstanceId
    {
      get
      {
        return this._InstanceId;
      }
      set
      {
        this._InstanceId = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, IsRequired = false, Name = "BattleReport")]
    [DefaultValue(null)]
    public ProCommonBattleReport BattleReport
    {
      get
      {
        return this._BattleReport;
      }
      set
      {
        this._BattleReport = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(17, DataFormat = DataFormat.Default, IsRequired = false, Name = "Record")]
    public ProPeakArenaBattleReportNameRecord Record
    {
      get
      {
        return this._Record;
      }
      set
      {
        this._Record = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, Name = "MinePickedHeroes")]
    public List<int> MinePickedHeroes
    {
      get
      {
        return this._MinePickedHeroes;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
