﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPlayerHeroCommentEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProPlayerHeroCommentEntry")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProPlayerHeroCommentEntry : IExtensible
  {
    private int _HeroId;
    private readonly List<ulong> _CommentedEntryInstanceIds;
    private readonly List<ulong> _PraisedEntryInstanceIds;
    private int _CommentedNums;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPlayerHeroCommentEntry()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroId")]
    public int HeroId
    {
      get
      {
        return this._HeroId;
      }
      set
      {
        this._HeroId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "CommentedEntryInstanceIds")]
    public List<ulong> CommentedEntryInstanceIds
    {
      get
      {
        return this._CommentedEntryInstanceIds;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "PraisedEntryInstanceIds")]
    public List<ulong> PraisedEntryInstanceIds
    {
      get
      {
        return this._PraisedEntryInstanceIds;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CommentedNums")]
    public int CommentedNums
    {
      get
      {
        return this._CommentedNums;
      }
      set
      {
        this._CommentedNums = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
