﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProAncientCallPeriod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProAncientCallPeriod")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProAncientCallPeriod : IExtensible
  {
    private int _CurrentPeriodId;
    private int _CurrentBossIndex;
    private int _Score;
    private long _UpdateTime;
    private readonly List<ProAncientCallBoss> _Bosses;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAncientCallPeriod()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrentPeriodId")]
    public int CurrentPeriodId
    {
      get
      {
        return this._CurrentPeriodId;
      }
      set
      {
        this._CurrentPeriodId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrentBossIndex")]
    public int CurrentBossIndex
    {
      get
      {
        return this._CurrentBossIndex;
      }
      set
      {
        this._CurrentBossIndex = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Score")]
    public int Score
    {
      get
      {
        return this._Score;
      }
      set
      {
        this._Score = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UpdateTime")]
    public long UpdateTime
    {
      get
      {
        return this._UpdateTime;
      }
      set
      {
        this._UpdateTime = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "Bosses")]
    public List<ProAncientCallBoss> Bosses
    {
      get
      {
        return this._Bosses;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
