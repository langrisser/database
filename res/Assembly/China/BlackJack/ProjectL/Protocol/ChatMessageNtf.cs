﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ChatMessageNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ChatMessageNtf")]
  [Serializable]
  public class ChatMessageNtf : IExtensible
  {
    private int _ChannelId;
    private ProChatInfo _ChatInfo;
    private ProChatCententInfo _ChatCententInfo;
    private ProChatContentVoice _VoiceInfo;
    private ProChatVoiceSimpleInfo _VoiceSimpleInfo;
    private ProChatEnterRoomInfo _EnterRoomInfo;
    private string _DestGameUserId;
    private long _SendTime;
    private string _ChatGroupId;
    private bool _JustForSelf;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessageNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChannelId")]
    public int ChannelId
    {
      get
      {
        return this._ChannelId;
      }
      set
      {
        this._ChannelId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "ChatInfo")]
    public ProChatInfo ChatInfo
    {
      get
      {
        return this._ChatInfo;
      }
      set
      {
        this._ChatInfo = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "ChatCententInfo")]
    [DefaultValue(null)]
    public ProChatCententInfo ChatCententInfo
    {
      get
      {
        return this._ChatCententInfo;
      }
      set
      {
        this._ChatCententInfo = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "VoiceInfo")]
    [DefaultValue(null)]
    public ProChatContentVoice VoiceInfo
    {
      get
      {
        return this._VoiceInfo;
      }
      set
      {
        this._VoiceInfo = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "VoiceSimpleInfo")]
    public ProChatVoiceSimpleInfo VoiceSimpleInfo
    {
      get
      {
        return this._VoiceSimpleInfo;
      }
      set
      {
        this._VoiceSimpleInfo = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "EnterRoomInfo")]
    [DefaultValue(null)]
    public ProChatEnterRoomInfo EnterRoomInfo
    {
      get
      {
        return this._EnterRoomInfo;
      }
      set
      {
        this._EnterRoomInfo = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "DestGameUserId")]
    public string DestGameUserId
    {
      get
      {
        return this._DestGameUserId;
      }
      set
      {
        this._DestGameUserId = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "SendTime")]
    [DefaultValue(0)]
    public long SendTime
    {
      get
      {
        return this._SendTime;
      }
      set
      {
        this._SendTime = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = false, Name = "ChatGroupId")]
    [DefaultValue("")]
    public string ChatGroupId
    {
      get
      {
        return this._ChatGroupId;
      }
      set
      {
        this._ChatGroupId = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = false, Name = "JustForSelf")]
    [DefaultValue(false)]
    public bool JustForSelf
    {
      get
      {
        return this._JustForSelf;
      }
      set
      {
        this._JustForSelf = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
