﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.TeamRoomViewReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "TeamRoomViewReq")]
  [Serializable]
  public class TeamRoomViewReq : IExtensible
  {
    private int _GameFunctionTypeId;
    private int _ChapterId;
    private int _LocationId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomViewReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GameFunctionTypeId")]
    public int GameFunctionTypeId
    {
      get
      {
        return this._GameFunctionTypeId;
      }
      set
      {
        this._GameFunctionTypeId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ChapterId")]
    public int ChapterId
    {
      get
      {
        return this._ChapterId;
      }
      set
      {
        this._ChapterId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "LocationId")]
    public int LocationId
    {
      get
      {
        return this._LocationId;
      }
      set
      {
        this._LocationId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
