﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSHeroDungeonNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "DSHeroDungeonNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class DSHeroDungeonNtf : IExtensible
  {
    private uint _Version;
    private readonly List<int> _AchievementRelationIds;
    private readonly List<ProHeroDungeonChapter> _Chapters;
    private int _DailyChallengeNums;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSHeroDungeonNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "AchievementRelationIds")]
    public List<int> AchievementRelationIds
    {
      get
      {
        return this._AchievementRelationIds;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Chapters")]
    public List<ProHeroDungeonChapter> Chapters
    {
      get
      {
        return this._Chapters;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DailyChallengeNums")]
    public int DailyChallengeNums
    {
      get
      {
        return this._DailyChallengeNums;
      }
      set
      {
        this._DailyChallengeNums = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
