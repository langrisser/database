﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProUnchartedScore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProUnchartedScore")]
  [Serializable]
  public class ProUnchartedScore : IExtensible
  {
    private int _Id;
    private int _BonusCount;
    private int _Score;
    private readonly List<int> _RewardGains;
    private readonly List<int> _SocreLevelCompleteIds;
    private readonly List<int> _ChallengeLevelCompleteIds;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProUnchartedScore()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Id")]
    public int Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BonusCount")]
    public int BonusCount
    {
      get
      {
        return this._BonusCount;
      }
      set
      {
        this._BonusCount = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Score")]
    public int Score
    {
      get
      {
        return this._Score;
      }
      set
      {
        this._Score = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "RewardGains")]
    public List<int> RewardGains
    {
      get
      {
        return this._RewardGains;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, Name = "SocreLevelCompleteIds")]
    public List<int> SocreLevelCompleteIds
    {
      get
      {
        return this._SocreLevelCompleteIds;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "ChallengeLevelCompleteIds")]
    public List<int> ChallengeLevelCompleteIds
    {
      get
      {
        return this._ChallengeLevelCompleteIds;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
