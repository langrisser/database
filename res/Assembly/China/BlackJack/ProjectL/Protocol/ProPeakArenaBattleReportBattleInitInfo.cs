﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPeakArenaBattleReportBattleInitInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProPeakArenaBattleReportBattleInitInfo")]
  [Serializable]
  public class ProPeakArenaBattleReportBattleInitInfo : IExtensible
  {
    private readonly List<ProBPStageHero> _BPStageHeroes;
    private readonly List<ProBattleHero> _SetupHeroes;
    private readonly List<ProTrainingTech> _Techs;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaBattleReportBattleInitInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, Name = "BPStageHeroes")]
    public List<ProBPStageHero> BPStageHeroes
    {
      get
      {
        return this._BPStageHeroes;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "SetupHeroes")]
    public List<ProBattleHero> SetupHeroes
    {
      get
      {
        return this._SetupHeroes;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Techs")]
    public List<ProTrainingTech> Techs
    {
      get
      {
        return this._Techs;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
