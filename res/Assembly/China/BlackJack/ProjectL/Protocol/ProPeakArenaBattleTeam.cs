﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPeakArenaBattleTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProPeakArenaBattleTeam")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProPeakArenaBattleTeam : IExtensible
  {
    private readonly List<int> _MyHeroIds;
    private readonly List<ProPeakArenaHiredHero> _HiredHeroes;
    private readonly List<ProPeakArenaHiredHero> _HistoricalHiredHeroes;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaBattleTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, Name = "MyHeroIds")]
    public List<int> MyHeroIds
    {
      get
      {
        return this._MyHeroIds;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "HiredHeroes")]
    public List<ProPeakArenaHiredHero> HiredHeroes
    {
      get
      {
        return this._HiredHeroes;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "HistoricalHiredHeroes")]
    public List<ProPeakArenaHiredHero> HistoricalHiredHeroes
    {
      get
      {
        return this._HistoricalHiredHeroes;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
