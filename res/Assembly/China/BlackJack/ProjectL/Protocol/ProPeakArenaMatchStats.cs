﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPeakArenaMatchStats
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProPeakArenaMatchStats")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProPeakArenaMatchStats : IExtensible
  {
    private int _Matches;
    private int _Wins;
    private int _ConsecutiveWins;
    private int _ConsecutiveLosses;
    private int _WinsThisWeek;
    private readonly List<bool> _RecentMatchResults;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaMatchStats()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Matches")]
    public int Matches
    {
      get
      {
        return this._Matches;
      }
      set
      {
        this._Matches = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Wins")]
    public int Wins
    {
      get
      {
        return this._Wins;
      }
      set
      {
        this._Wins = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConsecutiveWins")]
    public int ConsecutiveWins
    {
      get
      {
        return this._ConsecutiveWins;
      }
      set
      {
        this._ConsecutiveWins = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConsecutiveLosses")]
    public int ConsecutiveLosses
    {
      get
      {
        return this._ConsecutiveLosses;
      }
      set
      {
        this._ConsecutiveLosses = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WinsThisWeek")]
    public int WinsThisWeek
    {
      get
      {
        return this._WinsThisWeek;
      }
      set
      {
        this._WinsThisWeek = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "RecentMatchResults")]
    public List<bool> RecentMatchResults
    {
      get
      {
        return this._RecentMatchResults;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
