﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBattleRoomBPStage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProBattleRoomBPStage")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProBattleRoomBPStage : IExtensible
  {
    private ProBPStage _BPStage;
    private long _TurnTimeSpan;
    private readonly List<long> _PublicTimeSpan;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleRoomBPStage()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "BPStage")]
    public ProBPStage BPStage
    {
      get
      {
        return this._BPStage;
      }
      set
      {
        this._BPStage = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TurnTimeSpan")]
    public long TurnTimeSpan
    {
      get
      {
        return this._TurnTimeSpan;
      }
      set
      {
        this._TurnTimeSpan = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "PublicTimeSpan")]
    public List<long> PublicTimeSpan
    {
      get
      {
        return this._PublicTimeSpan;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
