﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.RedeemInfoAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "RedeemInfoAck")]
  [Serializable]
  public class RedeemInfoAck : IExtensible
  {
    private int _TotalRecharge;
    private readonly List<RedeemCrystalInfo> _Crystals;
    private RedeemBundleInfo _Bundle;
    private bool _Claimed;
    private long _StartDate;
    private long _EndDate;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public RedeemInfoAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TotalRecharge")]
    public int TotalRecharge
    {
      get
      {
        return this._TotalRecharge;
      }
      set
      {
        this._TotalRecharge = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Crystals")]
    public List<RedeemCrystalInfo> Crystals
    {
      get
      {
        return this._Crystals;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Bundle")]
    public RedeemBundleInfo Bundle
    {
      get
      {
        return this._Bundle;
      }
      set
      {
        this._Bundle = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Claimed")]
    public bool Claimed
    {
      get
      {
        return this._Claimed;
      }
      set
      {
        this._Claimed = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StartDate")]
    public long StartDate
    {
      get
      {
        return this._StartDate;
      }
      set
      {
        this._StartDate = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EndDate")]
    public long EndDate
    {
      get
      {
        return this._EndDate;
      }
      set
      {
        this._EndDate = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
