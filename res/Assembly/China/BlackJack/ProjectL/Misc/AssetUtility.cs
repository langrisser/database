﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.AssetUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class AssetUtility
  {
    public const string DataPath = "Assets/GameProject/RuntimeAssets/";
    private static AssetUtility s_instance;
    private List<IDynamicAssetProvider> m_dynamicAssetProviders;
    private List<Dictionary<string, UnityEngine.Object>> m_dynamicAssetCaches;
    private List<LruAssetCache> m_lruAssetCaches;

    [MethodImpl((MethodImplOptions) 32768)]
    public AssetUtility()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string MakeSpriteAssetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string MakeSpriteAssetName(string name, string subName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AddAssetToList(string name, List<string> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AddSpriteAssetToList(string name, List<string> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterDynamicAssetProvider(IDynamicAssetProvider assetProvider)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnregisterDynamicAssetProvider(IDynamicAssetProvider assetProvider)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterDynamicAssetCache(Dictionary<string, UnityEngine.Object> assetCache)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnregisterDynamicAssetCache(Dictionary<string, UnityEngine.Object> assetCache)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetAsset<T>(string name) where T : UnityEngine.Object
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Sprite GetSprite(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object _GetAsset(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitLruAssetCache(int cacheType, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UninitAllLruAssetCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAssetToLruCache(int cacheType, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAssetToLruCache(int cacheType, UnityEngine.Object a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearLruCache(int cacheType)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAllLruCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private LruAssetCache GetLruAssetCache(int cacheType)
    {
      // ISSUE: unable to decompile the method.
    }

    public void TestMemoryWarning()
    {
      this.OnLowMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLowMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    public static AssetUtility Instance
    {
      set
      {
        AssetUtility.s_instance = value;
      }
      get
      {
        return AssetUtility.s_instance;
      }
    }

    public event Action EventOnMemoryWarning
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
