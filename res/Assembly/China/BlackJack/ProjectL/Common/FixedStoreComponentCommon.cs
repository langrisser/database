﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.FixedStoreComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class FixedStoreComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected BagComponentCommon m_bag;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected ResourceComponentCommon m_resource;
    protected DataSectionFixedStore m_fixedStoreDS;
    [DoNotToLua]
    private FixedStoreComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_OnFlushBoughtNums_hotfix;
    private LuaFunction m_IsSoldOutConfigDataFixedStoreItemInfoFixedStoreItem_hotfix;
    private LuaFunction m_IsInSaleTimeConfigDataFixedStoreItemInfo_hotfix;
    private LuaFunction m_CanBuyGoodsInt32Int32Int32_hotfix;
    private LuaFunction m_CanBuyFixedStoreItemConfigDataFixedStoreItemInfoFixedStoreItem_hotfix;
    private LuaFunction m_CaculateCurrencyCountConfigDataFixedStoreItemInfoBoolean_hotfix;
    private LuaFunction m_IsOnDiscountPeriodConfigDataFixedStoreItemInfo_hotfix;
    private LuaFunction m_GetStoreInt32_hotfix;
    private LuaFunction m_GetStoreOfferingByIdInt32_hotfix;
    private LuaFunction m_BuyStoreItemInt32FixedStoreItemInt32_hotfix;
    private LuaFunction m_OnBuyStoreItemFixedStoreItemInt32_hotfix;
    private LuaFunction m_OnAddSkinFixedStoreItem_hotfix;
    private LuaFunction m_add_BuyStoreItemEventAction`2_hotfix;
    private LuaFunction m_remove_BuyStoreItemEventAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStoreComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushBoughtNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSoldOut(ConfigDataFixedStoreItemInfo itemConfig, FixedStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInSaleTime(ConfigDataFixedStoreItemInfo itemConfig)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanBuyGoods(int storeId, int goodsId, int selectedIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanBuyFixedStoreItem(ConfigDataFixedStoreItemInfo itemConfig, FixedStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CaculateCurrencyCount(ConfigDataFixedStoreItemInfo storeItemConfig, bool isFirstBuy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOnDiscountPeriod(ConfigDataFixedStoreItemInfo storeItemConfig)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStore GetStore(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private FixedStore GetStoreOfferingById(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void BuyStoreItem(int storeId, FixedStoreItem storeItem, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyStoreItem(FixedStoreItem storeItem, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddSkin(FixedStoreItem storeItem)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> BuyStoreItemEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public FixedStoreComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_BuyStoreItemEvent(int arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_BuyStoreItemEvent(int arg1, int arg2)
    {
      this.BuyStoreItemEvent = (Action<int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private FixedStoreComponentCommon m_owner;

      public LuaExportHelper(FixedStoreComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_BuyStoreItemEvent(int arg1, int arg2)
      {
        this.m_owner.__callDele_BuyStoreItemEvent(arg1, arg2);
      }

      public void __clearDele_BuyStoreItemEvent(int arg1, int arg2)
      {
        this.m_owner.__clearDele_BuyStoreItemEvent(arg1, arg2);
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public ResourceComponentCommon m_resource
      {
        get
        {
          return this.m_owner.m_resource;
        }
        set
        {
          this.m_owner.m_resource = value;
        }
      }

      public DataSectionFixedStore m_fixedStoreDS
      {
        get
        {
          return this.m_owner.m_fixedStoreDS;
        }
        set
        {
          this.m_owner.m_fixedStoreDS = value;
        }
      }

      public void OnFlushBoughtNums()
      {
        this.m_owner.OnFlushBoughtNums();
      }

      public int CanBuyFixedStoreItem(ConfigDataFixedStoreItemInfo itemConfig, FixedStoreItem item)
      {
        return this.m_owner.CanBuyFixedStoreItem(itemConfig, item);
      }

      public FixedStore GetStoreOfferingById(int storeId)
      {
        return this.m_owner.GetStoreOfferingById(storeId);
      }

      public void BuyStoreItem(int storeId, FixedStoreItem storeItem, int count)
      {
        this.m_owner.BuyStoreItem(storeId, storeItem, count);
      }

      public void OnBuyStoreItem(FixedStoreItem storeItem, int count)
      {
        this.m_owner.OnBuyStoreItem(storeItem, count);
      }

      public void OnAddSkin(FixedStoreItem storeItem)
      {
        this.m_owner.OnAddSkin(storeItem);
      }
    }
  }
}
