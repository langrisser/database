﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaPlayerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ArenaPlayerInfo
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ArenaPlayerDefensiveTeam DefensiveTeam { get; set; }

    public List<ArenaOpponent> Opponents { get; set; }

    public int ArenaLevelId { get; set; }

    public ushort ArenaPoints { get; set; }

    public bool AttackedOpponent { get; set; }

    public DateTime WeekLastFlushTime { get; set; }

    public int VictoryPoints { get; set; }

    public List<int> ReceivedVictoryPointsRewardIndexs { get; set; }

    public List<int> ThisWeekBattleIds { get; set; }

    public int ThisWeekTotalBattleNums { get; set; }

    public int ThisWeekVictoryNums { get; set; }

    public bool IsAutoBattle { get; set; }

    public ArenaOpponent RevengeOpponent { get; set; }

    public ulong RevengeBattleReportInstanceId { get; set; }

    public ArenaOpponentDefensiveBattleInfo OpponentDefensiveBattleInfo { get; set; }

    public int MineRank { get; set; }

    public int ConsecutiveVictoryNums { get; set; }

    public long NextFlushOpponentTime { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaPlayerInfo PBArenaPlayerInfoToArenaPlayerInfo(
      ProArenaPlayerInfo pbArenaPlayerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaPlayerInfo ArenaPlayerInfoToPBArenaPlayerInfo(
      ArenaPlayerInfo arenaPlayerInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
