﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionSurvey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionSurvey : DataSection
  {
    private Survey m_currentSurvey;

    [MethodImpl((MethodImplOptions) 32768)]
    public SurveyStatus GetCurrentSurveyStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSurveyStatus(SurveyStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetCurrentSurvey(Survey survey)
    {
      this.m_currentSurvey = survey;
      this.SetDirty(true);
    }

    public void InitCurrentSurvey(Survey survey)
    {
      this.m_currentSurvey = survey;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public Survey GetCurrentSurvey()
    {
      return this.m_currentSurvey;
    }
  }
}
