﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GiftStoreComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class GiftStoreComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionGiftStore m_giftStoreDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    protected ResourceComponentCommon m_resource;

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "GiftStore";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushBoughtNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<GiftStoreItem> GetOfferedStoreItemsByConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GiftStoreItem> GetOfferedStoreItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsOnSaleTime(DateTime saleStartTime, DateTime saleEndTime, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasBought(int goodsId)
    {
      return this.m_giftStoreDS.HasBought(goodsId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanBuyGoods(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAppleSubscribeGoods(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddFirstBuyGoodsRecord(int goodsId, string goodsRegisterId)
    {
      this.m_giftStoreDS.AddFirstBuyGoodsRecord(goodsId, goodsRegisterId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OrderReward> GetAllOrderRewards()
    {
      // ISSUE: unable to decompile the method.
    }

    public void RemoveOrderReward(string orderId)
    {
      this.m_giftStoreDS.RemoveOrderReward(orderId);
    }

    public void AddOrderReward(string orderId, OrderReward reward)
    {
      this.m_giftStoreDS.AddOrderReward(orderId, reward);
    }

    public OrderReward FindOrderReward(string orderId)
    {
      return this.m_giftStoreDS.FindOrderReward(orderId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBuyGiftStoreGoods(ConfigDataGiftStoreItemInfo goodsInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> BuyGiftStoreGoodsEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> BuyStoreItemEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
