﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class GuildComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    protected DataSectionGuild m_guildDS;
    private DateTime m_latGuildSearchTime;
    private DateTime m_latGuildRandomListTime;
    private DateTime m_latGuildInvitePlayerListTime;
    protected MissionComponentCommon m_mission;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "Guild";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushPlayerGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOwnGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void QuitGuild(DateTime nextJoinTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetGuildId()
    {
      return this.m_guildDS.GuildId;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGuildId(string id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCreateGuild(string guildName, string hiringDeclaration, int joinLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanJoinGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanQuitGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanKickOutGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanApplyToJoinGuild()
    {
      return this.CanJoinGuild();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanConfirmJoinGuildInvitation(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void RefuseJoinGuildInvitation(string guildId)
    {
      this.m_guildDS.RemoveJoinGuildInvitation(guildId);
    }

    public void RefuseAllJoinGuildInvitation()
    {
      this.m_guildDS.ClearJoinGuildInvitation();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CheckGuildName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildSearch(string searchText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildRandomList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildInvitePlayerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetGuildHiringDeclaration(string hiringDeclaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildHiringDeclaration(string hiringDeclaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetGuildAnnouncement(string announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildAnnouncement(string announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanStartMassiveCombat(int difficulty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTheseHeroesAttackStronghold(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackStronghold(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> GetMassiveCombatUnusedHeroes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEliminateRate(GuildMassiveCombatInfo combat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStrongholdEliminateRate(GuildMassiveCombatStronghold stronghold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStartedCombatThisWeek(GuildMassiveCombatGeneral generalInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public string GuildId
    {
      get
      {
        return this.m_guildDS.GuildId;
      }
    }

    public DateTime NextJoinTime
    {
      get
      {
        return this.m_guildDS.NextJoinTime;
      }
    }

    public DataSectionGuild GuildDS
    {
      get
      {
        return this.m_guildDS;
      }
    }

    public long GetGuildLastUpdateTime { get; set; }

    public event Action JoinGuildEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
