﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BusinessCardStatisticalData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BusinessCardStatisticalData
  {
    public int MostSkilledHeroId { get; set; }

    public int HeroTotalPower { get; set; }

    public int AchievementMissionNums { get; set; }

    public int MasterJobNums { get; set; }

    public int RiftAchievementNums { get; set; }

    public int ChooseLevelAchievementNums { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBusinessCardStatisticalData ToProtocol(
      BusinessCardStatisticalData data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BusinessCardStatisticalData FromProtocol(
      ProBusinessCardStatisticalData pbData)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
