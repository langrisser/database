﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionMail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionMail : DataSection
  {
    private Dictionary<ulong, Mail> m_mails;
    private int m_globalMailId;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionMail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.m_mails.Clear();
    }

    public bool IsMailReaded(Mail mail)
    {
      return mail.Status >= 1;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail AddMail(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitMail(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveMail(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearMailBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail FindMailByTemplateId(int templateId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail FindMail(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadMail(Mail mail, DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGotAttachments(Mail mail, DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitGlobalMailId(int value)
    {
      this.m_globalMailId = value;
    }

    public void SetGlobalMailId(int value)
    {
      this.m_globalMailId = value;
      this.SetDirty(true);
    }

    public int GlobalMailId
    {
      get
      {
        return this.m_globalMailId;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail GetFirstMail()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetMailBoxSize()
    {
      return this.m_mails.Count;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mail> GetMails()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
