﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ProcessingBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ProcessingBattle
  {
    public List<int> Params;
    public List<ulong> ULongParams;
    public List<string> StrParams;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProcessingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleType Type { get; set; }

    public int TypeId { get; set; }

    public int RandomSeed { get; set; }

    public int ArmyRandomSeed { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleProcessing BattleProcessingToPbBattleProcessing(
      ProcessingBattle battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProcessingBattle PbBattleProcessingToBattleProcessing(
      ProBattleProcessing pbBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
