﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RandomEvent
  {
    public int EventId { get; set; }

    public int WayPointId { get; set; }

    public int Lives { get; set; }

    public int DeadLives { get; set; }

    public long ExpiredTime { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRandomEvent RandomEventToPBRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProRandomEvent> RandomEventsToPBRandomEvents(
      List<RandomEvent> randomEvents)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RandomEvent PBRandomEventToRandomEvent(ProRandomEvent pbRadomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<RandomEvent> PBRandomEventsToRandomEvents(
      List<ProRandomEvent> pbRadomEvents)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
