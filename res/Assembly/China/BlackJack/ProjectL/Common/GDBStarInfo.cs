﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GDBStarInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [ProtoContract(Name = "GDBStarInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class GDBStarInfo : IExtensible
  {
    private int _Id;
    private uint _age;
    private uint _spectrum;
    private double _brightness;
    private uint _radius;
    private uint _temperature;
    private uint _artResourcesId;
    private double _Mass;
    private uint _templateId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GDBStarInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Id")]
    public int Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "age")]
    public uint Age
    {
      get
      {
        return this._age;
      }
      set
      {
        this._age = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "spectrum")]
    public uint Spectrum
    {
      get
      {
        return this._spectrum;
      }
      set
      {
        this._spectrum = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "brightness")]
    public double Brightness
    {
      get
      {
        return this._brightness;
      }
      set
      {
        this._brightness = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "radius")]
    public uint Radius
    {
      get
      {
        return this._radius;
      }
      set
      {
        this._radius = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "temperature")]
    public uint Temperature
    {
      get
      {
        return this._temperature;
      }
      set
      {
        this._temperature = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "artResourcesId")]
    public uint ArtResourcesId
    {
      get
      {
        return this._artResourcesId;
      }
      set
      {
        this._artResourcesId = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Mass")]
    public double Mass
    {
      get
      {
        return this._Mass;
      }
      set
      {
        this._Mass = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "templateId")]
    public uint TemplateId
    {
      get
      {
        return this._templateId;
      }
      set
      {
        this._templateId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
