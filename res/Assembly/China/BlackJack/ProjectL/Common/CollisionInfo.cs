﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CollisionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "CollisionInfo")]
  [Serializable]
  public class CollisionInfo : IExtensible
  {
    private float _RadiusMax;
    private readonly List<SphereCollisionInfo> _sphereCollisions;
    private readonly List<CapsuleCollisionInfo> _capsuleCollisions;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollisionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "RadiusMax")]
    public float RadiusMax
    {
      get
      {
        return this._RadiusMax;
      }
      set
      {
        this._RadiusMax = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "sphereCollisions")]
    public List<SphereCollisionInfo> SphereCollisions
    {
      get
      {
        return this._sphereCollisions;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "capsuleCollisions")]
    public List<CapsuleCollisionInfo> CapsuleCollisions
    {
      get
      {
        return this._capsuleCollisions;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
