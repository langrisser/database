﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ChatBagItemMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  [Serializable]
  public class ChatBagItemMessage : ChatMessage
  {
    public string Text { get; set; }

    public int SystemContentTemplateId { get; set; }

    public List<ChatBagItemMessage.ChatBagItemData> ChatBagItemDataList { get; set; }

    public class ChatBagItemData
    {
      public ulong m_instanceId;
      public int m_resonanceCount;
      public BagItemBase m_bagItem;
    }
  }
}
