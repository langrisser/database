﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RandomStore
  {
    public List<RandomStoreItem> Items;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStore()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Id { get; set; }

    public DateTime NextFlushTime { get; set; }

    public int ManualFlushNums { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRandomStore StoreToPBStore(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProRandomStore> StoresToPBStores(List<RandomStore> stores)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RandomStore PBStoreToStore(ProRandomStore pbStore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<RandomStore> PBStoresToStores(List<ProRandomStore> pbStores)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
