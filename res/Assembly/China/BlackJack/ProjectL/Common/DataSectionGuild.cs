﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionGuild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionGuild : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.InvitedGuildIds.Clear();
    }

    public bool IsInInvitedGuild(string guildId)
    {
      return this.InvitedGuildIds.Contains(guildId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddJoinGuildInvitation(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveJoinGuildInvitation(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearJoinGuildInvitation()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GuildId { get; set; }

    public DateTime NextJoinTime { get; set; }

    public List<string> InvitedGuildIds { get; set; }

    public GuildPlayerMassiveCombatInfo MassiveCombat { get; set; }
  }
}
