﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionArena
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionArena : DataSection
  {
    public DataSectionArena()
    {
      this.ArenaPlayerInfo = (ArenaPlayerInfo) null;
    }

    public void InitArenaPlayerInfo(ArenaPlayerInfo info)
    {
      this.ArenaPlayerInfo = info;
    }

    public override void ClearInitedData()
    {
      this.ArenaPlayerInfo = (ArenaPlayerInfo) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsEmptyArenaPlayerInfo()
    {
      return this.ArenaPlayerInfo == null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefensiveTeam(ArenaPlayerDefensiveTeam team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWeekLastFlushTime(DateTime weekLastFlushTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetThisWeekBattleIds(List<int> battleIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefensiveBattleId(byte battleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefensiveRuleId(byte ruleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddVictoryPoints(int points)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetCurrentVictoryPoints()
    {
      return this.ArenaPlayerInfo.VictoryPoints;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetVictoryPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetReceivedVictoryPointsRewardedIndexs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasReceivedVictoryPointsRewardedIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddReceivedVictoryPointsRewardIndex(int victoryPointsIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponent FindOpponent(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAttackedOpponent(bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOpponents(List<ArenaOpponent> opponents)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetArenaDefensiveBattleInfo(ArenaOpponentDefensiveBattleInfo info)
    {
      this.ArenaPlayerInfo.OpponentDefensiveBattleInfo = info;
    }

    public ArenaOpponentDefensiveBattleInfo GetArenaDefensiveBattleInfo()
    {
      return this.ArenaPlayerInfo.OpponentDefensiveBattleInfo;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetOpponentUserId()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetArenaLevelId(int arenaLevelId)
    {
      this.ArenaPlayerInfo.ArenaLevelId = arenaLevelId;
    }

    public void SetArenaPoints(ushort ArenaPoints)
    {
      this.ArenaPlayerInfo.ArenaPoints = ArenaPoints;
    }

    public void SetArenaPlayerInfo(ArenaPlayerInfo info)
    {
      this.ArenaPlayerInfo = info;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFirstFindOpponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetConsecutiveVictoryNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddConsecutiveVictoryNums()
    {
      // ISSUE: unable to decompile the method.
    }

    public ArenaPlayerInfo ArenaPlayerInfo { get; private set; }
  }
}
