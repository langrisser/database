﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionRealTimePVP
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionRealTimePVP : DataSection
  {
    public LinkedList<RealTimePVPBattleReport> PromotionReports;
    public LinkedList<RealTimePVPBattleReport> Reports;
    public RealTimePVPMatchStats FriendlyMatchStats;
    public RealTimePVPMatchStats LadderMatchStats;
    public RealTimePVPMatchStats FriendlyCareerMatchStats;
    public RealTimePVPMatchStats LadderCareerMatchStats;
    public List<int> WinsBonusIdAcquired;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionRealTimePVP()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public void StartPromotion()
    {
      this.IsPromotion = true;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EndPromotion()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SavePromotionReport(RealTimePVPBattleReport Report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveReport(RealTimePVPBattleReport Report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartNewSeason()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AcquireWinsBonus(int BonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsPromotion { get; set; }
  }
}
