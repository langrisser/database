﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RealTimePVPConstDefine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RealTimePVPConstDefine
  {
    public const int NoRank = -1;
    public const DayOfWeek StartDayOfWeek = DayOfWeek.Monday;
    public const int RankingRewardSingleOperationTimeThreshold = 2000;
  }
}
