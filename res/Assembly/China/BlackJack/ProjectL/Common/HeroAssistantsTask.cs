﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroAssistantsTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class HeroAssistantsTask
  {
    public HeroAssistants WhichHeroAssistants;

    public int ConfigId { get; set; }

    public int Weekday
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> RecommendHeroIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> SoldierTypes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CompletePoints
    {
      get
      {
        return this.Config.CompletePoints;
      }
    }

    public int RequiredUserLevel
    {
      get
      {
        return this.Config.RequiredUserLevel;
      }
    }

    public List<int> RewardCompleteRate
    {
      get
      {
        return this.Config.m_rewardCompleteRate;
      }
    }

    public List<int> RewardDropId
    {
      get
      {
        return this.Config.m_rewardDropId;
      }
    }

    public List<CompleteValueDropID> Rewards
    {
      get
      {
        return this.Config.Rewards;
      }
    }

    public List<int> RewardWorkSeconds
    {
      get
      {
        return this.Config.m_rewardWorkSeconds;
      }
    }

    public List<int> RewardDropCount
    {
      get
      {
        return this.Config.m_rewardDropCount;
      }
    }

    public int RecommendHeroMultiply
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RecommendHeroAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Locked
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader { get; set; }

    public ConfigDataHeroAssistantTaskInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAssignPoints(Hero H)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAssignPoints(int HeroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAssignPoints(List<Hero> Heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAssignPoints(List<int> HeroIds)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
