﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleRoomBattleLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BattleRoomBattleLog
  {
    public ulong RoomId { get; set; }

    public int BattleRoomType { get; set; }

    public int BattleId { get; set; }

    public int RandomNumberSeed { get; set; }

    public int ArmyRandomNumberSeed { get; set; }

    public List<BattleActorSetup> Team0 { get; set; }

    public List<BattleActorSetup> Team1 { get; set; }

    public List<BattlePlayer> Players { get; set; }
  }
}
