﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionRaffle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionRaffle : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionRaffle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.RafflePools.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitRafflePools(List<RafflePool> rafflePools)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetRafflePools(List<RafflePool> rafflePools)
    {
      this.InitRafflePools(rafflePools);
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveRafflePool(int poolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RafflePool GetRafflePool(int rafflePoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Drawed(RafflePool pool, int darwedRaffleId)
    {
      pool.Drawed(darwedRaffleId);
      this.SetDirty(true);
    }

    public Dictionary<int, RafflePool> RafflePools { get; set; }
  }
}
