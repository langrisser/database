﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaBetInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class PeakArenaBetInfo
  {
    public List<PeakArenaMatchupBet> Bets;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBetInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public int SeasonId { get; set; }

    public int TodayBetJettonNums { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaBetInfo FromPB(ProPeakArenaBetInfo pbBetInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaBetInfo ToPB(PeakArenaBetInfo betInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool NeedSettleBet()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool NeedDailyFlush()
    {
      return this.TodayBetJettonNums != 0;
    }

    public void BetDailyFlush()
    {
      this.TodayBetJettonNums = 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBet(int matchupId, string userId, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SettlePeakArenaMatchupBetReward(int matchupId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
