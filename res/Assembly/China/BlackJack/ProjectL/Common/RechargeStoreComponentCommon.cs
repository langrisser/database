﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RechargeStoreComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RechargeStoreComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionRechargeStore m_rechargeStoreDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public RechargeStoreComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "RechargeStore";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRechargeStoreStatus(int currentId, bool sync2Client = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnResetRechargeStoreStatus(int currentId)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void Sync2ClientUpdateRechargeStoreStatus(int currentId)
    {
    }

    public bool IsGoodsBought(int goodsId)
    {
      return this.m_rechargeStoreDS.IsGoodsBought(goodsId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CaculateGotCrystalNums(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void BuyGoods(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> BuyRechargeStoreGoodsEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> BuyStoreItemEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
