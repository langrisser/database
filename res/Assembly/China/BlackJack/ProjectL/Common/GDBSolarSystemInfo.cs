﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GDBSolarSystemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [ProtoContract(Name = "GDBSolarSystemInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class GDBSolarSystemInfo : IExtensible
  {
    private int _Id;
    private string _name;
    private float _gLocationX;
    private float _gLocationZ;
    private GDBStarInfo _star;
    private readonly List<GDBPlanetInfo> _planets;
    private GDBAsteroidBeltInfo _asteroidBeltInfo;
    private readonly List<GDBSpaceStationInfo> _spaceStations;
    private readonly List<GDBStargateInfo> _starGates;
    private readonly List<GDBSceneDummyInfo> _staticScenes;
    private int _templateId;
    private bool _isNeedLocalization;
    private string _localizationKey;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GDBSolarSystemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Id")]
    public int Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "name")]
    public string Name
    {
      get
      {
        return this._name;
      }
      set
      {
        this._name = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "gLocationX")]
    public float GLocationX
    {
      get
      {
        return this._gLocationX;
      }
      set
      {
        this._gLocationX = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "gLocationZ")]
    public float GLocationZ
    {
      get
      {
        return this._gLocationZ;
      }
      set
      {
        this._gLocationZ = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "star")]
    public GDBStarInfo Star
    {
      get
      {
        return this._star;
      }
      set
      {
        this._star = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "planets")]
    public List<GDBPlanetInfo> Planets
    {
      get
      {
        return this._planets;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "asteroidBeltInfo")]
    public GDBAsteroidBeltInfo AsteroidBeltInfo
    {
      get
      {
        return this._asteroidBeltInfo;
      }
      set
      {
        this._asteroidBeltInfo = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "spaceStations")]
    public List<GDBSpaceStationInfo> SpaceStations
    {
      get
      {
        return this._spaceStations;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "starGates")]
    public List<GDBStargateInfo> StarGates
    {
      get
      {
        return this._starGates;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "staticScenes")]
    public List<GDBSceneDummyInfo> StaticScenes
    {
      get
      {
        return this._staticScenes;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "templateId")]
    public int TemplateId
    {
      get
      {
        return this._templateId;
      }
      set
      {
        this._templateId = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "isNeedLocalization")]
    public bool IsNeedLocalization
    {
      get
      {
        return this._isNeedLocalization;
      }
      set
      {
        this._isNeedLocalization = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = false, Name = "localizationKey")]
    public string LocalizationKey
    {
      get
      {
        return this._localizationKey;
      }
      set
      {
        this._localizationKey = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
