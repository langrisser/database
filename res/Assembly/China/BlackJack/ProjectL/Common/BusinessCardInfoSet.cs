﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BusinessCardInfoSet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BusinessCardInfoSet
  {
    public List<BusinessCardHeroSet> Heroes;

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCardInfoSet()
    {
      // ISSUE: unable to decompile the method.
    }

    public string Desc { get; set; }

    public bool IsActionRandom { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBusinessCardInfoSet ToProtocol(BusinessCardInfoSet set)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BusinessCardInfoSet FromProtocol(ProBusinessCardInfoSet pbSet)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
