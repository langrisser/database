﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.MissionComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class MissionComponentCommon : IComponentBase
  {
    protected HashSet<ConfigDataMissionInfo> m_lockedMissionConfigsInLogic;
    protected HashSet<int> m_existMissions;
    public DataSectionMission m_missionDS;
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected SelectCardComponentCommon m_selectCard;
    protected BagComponentCommon m_bag;
    protected LevelComponentCommon m_level;
    protected RiftComponentCommon m_rift;
    protected HeroComponentCommon m_hero;
    protected ThearchyTrialCompomentCommon m_thearchyTrial;
    protected AnikiGymComponentCommon m_anikiGym;
    protected BattleComponentCommon m_battle;
    protected ArenaComponentCommon m_arena;
    protected HeroDungeonComponentCommon m_heroDungeon;
    protected HeroTrainningComponentCommon m_heroTrainning;
    protected MemoryCorridorCompomentCommon m_memoryCorridor;
    protected TreasureMapComponentCommon m_treasureMap;
    protected HeroPhantomCompomentCommon m_heroPhantom;
    protected CooperateBattleCompomentCommon m_cooperateBattle;
    protected TrainingGroundCompomentCommon m_trainingGround;
    protected TeamComponentCommon m_team;
    protected NoviceComponentCommon m_novice;
    protected RefluxComponentCommon m_reflux;
    protected FriendComponentCommon m_friend;
    protected HeroAssistantsCompomentCommon m_heroAssistants;
    protected ResourceComponentCommon m_resource;
    protected RealTimePVPComponentCommon m_realTimePvp;
    protected GiftStoreComponentCommon m_giftStore;
    protected RechargeStoreComponentCommon m_rechargeStore;
    protected ClimbTowerComponentCommon m_climb;
    protected GuildComponentCommon m_guild;
    protected EternalShrineCompomentCommon m_eternalShrine;
    protected HeroAnthemComponentCommon m_heroAnthem;
    protected AncientCallComponentCommon m_ancientCall;
    [DoNotToLua]
    private MissionComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_RemoveProcessingMissionByIdInt32_hotfix;
    private LuaFunction m_OnMonthCardVaildEveryDayFlush_hotfix;
    private LuaFunction m_OnMonthCardVaildCallBackInt32_hotfix;
    private LuaFunction m_InitProcessingMissionList`1_hotfix;
    private LuaFunction m_GetProcessingMissionByMissionTypeMissionType_hotfix;
    private LuaFunction m_InitExistMissions_hotfix;
    private LuaFunction m_OnFlushEverydayMissionEvent_hotfix;
    private LuaFunction m_ResetEverydayMissions_hotfix;
    private LuaFunction m_InitMissionListByMissionPeriodTypeMissionPeriodType_hotfix;
    private LuaFunction m_IsRefluxActivityHasOpening_hotfix;
    private LuaFunction m_CanUnlockMissionInLogicFromInitConfigDataMissionInfo_hotfix;
    private LuaFunction m_OnDirectActivatedMissionLockedConfigDataMissionInfo_hotfix;
    private LuaFunction m_IsNoviceMissionMission_hotfix;
    private LuaFunction m_IsRefluxMissionMission_hotfix;
    private LuaFunction m_IsRefluxMissionConfigDataMissionInfo_hotfix;
    private LuaFunction m_IsNoviceMissionConfigDataMissionInfo_hotfix;
    private LuaFunction m_IsNoviceMissionActivatedConfigDataMissionInfo_hotfix;
    private LuaFunction m_IsNoviceMissionActivatedForRewardingConfigDataMissionInfo_hotfix;
    private LuaFunction m_IsNoviceMissionActivatedMission_hotfix;
    private LuaFunction m_IsRefluxMissionActivatedMission_hotfix;
    private LuaFunction m_IsRefluxMissionActivatedConfigDataMissionInfo_hotfix;
    private LuaFunction m_IsRefluxMissionActivatedForRewardingConfigDataMissionInfo_hotfix;
    private LuaFunction m_CanGetRewardingMission_hotfix;
    private LuaFunction m_CanUnlockMissionInLogicConfigDataMissionInfo_hotfix;
    private LuaFunction m_CanUnlockMissionConfigDataMissionInfo_hotfix;
    private LuaFunction m_CaculateDirectlyActivationMissionStatusConfigDataMissionInfo_hotfix;
    private LuaFunction m_AddMissionConfigDataMissionInfo_hotfix;
    private LuaFunction m_OnAddProcessingDirectelyActivitedMissionMission_hotfix;
    private LuaFunction m_GetProcessingMissionByMissionPeriodMissionPeriodTypeInt32_hotfix;
    private LuaFunction m_OnGetProcessingDirectActivationMissionFailMission_hotfix;
    private LuaFunction m_AddEverydayMissionMission_hotfix;
    private LuaFunction m_AddOneOffMissionMission_hotfix;
    private LuaFunction m_IsMissionExistInt32_hotfix;
    private LuaFunction m_IsMissionFinishedInt32_hotfix;
    private LuaFunction m_InitMissionsFromConfigList`1_hotfix;
    private LuaFunction m_FinishMissionMission_hotfix;
    private LuaFunction m_GetAllEverydayMissionConfigByPreMissionIdInt32_hotfix;
    private LuaFunction m_GetAllEverydayMissionListByPlayerLevelInt32_hotfix;
    private LuaFunction m_GetAllEverydayMissionListByScenarioInt32_hotfix;
    private LuaFunction m_GetAllProcessingRefluxMissionList_hotfix;
    private LuaFunction m_GetAllFinishedRefluxMissionList_hotfix;
    private LuaFunction m_GetAllProcessingNoviceMissionList_hotfix;
    private LuaFunction m_GetAllFinishedNoviceMissionList_hotfix;
    private LuaFunction m_UpdateMissionListByMissionCompleteInt32_hotfix;
    private LuaFunction m_UpdateMissionListByLevelUpInt32_hotfix;
    private LuaFunction m_UpdateMissionListByScenarioInt32_hotfix;
    private LuaFunction m_UpdateMissionListByNewMissionConfigListList`1_hotfix;
    private LuaFunction m_AddMissionProcessMissionInt64_hotfix;
    private LuaFunction m_GetMissionMaxProcessMission_hotfix;
    private LuaFunction m_IsMissionProcessFinishedInt32_hotfix;
    private LuaFunction m_CanGainMissionRewardInt32_hotfix;
    private LuaFunction m_RegisterMissionCallBack_hotfix;
    private LuaFunction m_IsCompletedInt32_hotfix;
    private LuaFunction m_OnMissionFinishCallBackInt32_hotfix;
    private LuaFunction m_OnKillGoblinCallBackInt32_hotfix;
    private LuaFunction m_OnEquipmentStarLevelUp_hotfix;
    private LuaFunction m_OnJoinGuildCallBack_hotfix;
    private LuaFunction m_GuildMassiveCombatAttack_hotfix;
    private LuaFunction m_OnCompleteTowerFloorCallBackInt32_hotfix;
    private LuaFunction m_OnSelectCardMissionCallBackInt32Int32_hotfix;
    private LuaFunction m_OnMissionSelectCardCallBackInt32Int32_hotfix;
    private LuaFunction m_SetStatisticalDataStatisticalDataTypeInt64_hotfix;
    private LuaFunction m_AddStatisticalDataStatisticalDataTypeInt32_hotfix;
    private LuaFunction m_GetMissionCompletedProcessMission_hotfix;
    private LuaFunction m_OnSummonHeroCallBackInt32_hotfix;
    private LuaFunction m_OnConsumeEnergyCallBackGameFunctionTypeInt32_hotfix;
    private LuaFunction m_RollbackConsumeEnergyGameFunctionTypeInt32_hotfix;
    private LuaFunction m_IsFamGameFuncionTypeGameFunctionType_hotfix;
    private LuaFunction m_OnCompleteEventCallBackBoolean_hotfix;
    private LuaFunction m_OnCompleteLevelCallBackBattleTypeInt32List`1_hotfix;
    private LuaFunction m_RiftLevelAttackDiffculityCallBackInt32_hotfix;
    private LuaFunction m_AddBattleTypeLevelStatisticalDataBattleType_hotfix;
    private LuaFunction m_OnArenaConsecutiveVictoryCallBackInt32_hotfix;
    private LuaFunction m_OnArenaFightCallBack_hotfix;
    private LuaFunction m_OnCompleteScenaioCallBackInt32_hotfix;
    private LuaFunction m_OnCompleteRiftLevelCallBackInt32_hotfix;
    private LuaFunction m_OnGetRiftLevelFightAchievementCallBackInt32_hotfix;
    private LuaFunction m_OnFinishFightAchievementCallBackInt32_hotfix;
    private LuaFunction m_OnGetRiftLevelStarCallBackInt32_hotfix;
    private LuaFunction m_OnConsumeCrystalCallBackInt32_hotfix;
    private LuaFunction m_OnComsumeGoldCallBackInt32_hotfix;
    private LuaFunction m_OnUseHeroExpItemCallBackInt32_hotfix;
    private LuaFunction m_OnAllHeroAllJobLevelUpCallBack_hotfix;
    private LuaFunction m_OnAllHeroAddJobNums_hotfix;
    private LuaFunction m_OnRankJobHaveCallBack_hotfix;
    private LuaFunction m_OnAllHeroAddSkillNumsCallBackInt32_hotfix;
    private LuaFunction m_OnAllHeroAddSoliderNumsCallBackInt32_hotfix;
    private LuaFunction m_OnAllJobMasterHeroCallBackInt32_hotfix;
    private LuaFunction m_OnHeroMasterJobCallBackInt32Int32_hotfix;
    private LuaFunction m_OnLevelUpHeroStarLevelCallBackInt32_hotfix;
    private LuaFunction m_UpdateHeroRankLevelCallBackInt32_hotfix;
    private LuaFunction m_OnAddHeroNumsCallBackInt32_hotfix;
    private LuaFunction m_OnHasAboveLevelHeroNumsCallBack_hotfix;
    private LuaFunction m_OnLevelUpPlayerLevelCallBackInt32_hotfix;
    private LuaFunction m_OnUseCrystalBuyEnergyCallBackInt32_hotfix;
    private LuaFunction m_OnLoginGameCallBack_hotfix;
    private LuaFunction m_OnNewHeroJobTransferCallBackInt32Int32_hotfix;
    private LuaFunction m_OnSpecificHeroFightInt32Int32_hotfix;
    private LuaFunction m_OnSpecificHeroLevelUpInt32_hotfix;
    private LuaFunction m_OnTrainingTechToLevelCallBackTrainingTech_hotfix;
    private LuaFunction m_OnEquipmentToLevelCallBackEquipmentBagItem_hotfix;
    private LuaFunction m_OnFinishTeamBattleCallBack_hotfix;
    private LuaFunction m_OnFavorabilityToLevelCallBackHero_hotfix;
    private LuaFunction m_OnFetterToLevelCallBackHeroInt32_hotfix;
    private LuaFunction m_OnEnchantPropertiesSaveCallBackEquipmentBagItem_hotfix;
    private LuaFunction m_OnEnchantEquipmentCallBackEquipmentBagItem_hotfix;
    private LuaFunction m_OnTotalHeroJobLevelUpCallBack_hotfix;
    private LuaFunction m_OnInviteFriendCallBack_hotfix;
    private LuaFunction m_OnAssignHeroToTaskCallBack_hotfix;
    private LuaFunction m_OnBattlePracticeCallBack_hotfix;
    private LuaFunction m_OnRealTimeArenaBattleStartCallBackRealTimePVPMode_hotfix;
    private LuaFunction m_OnRealTimeArenaBattleFinishCallBackRealTimePVPModeBoolean_hotfix;
    private LuaFunction m_OnRealTimeArenaDanUpdateCallBackInt32_hotfix;
    private LuaFunction m_OnBuyGiftStoreGoodsCallBackInt32_hotfix;
    private LuaFunction m_OnBuyRechargeStoreGoodsCallBackInt32_hotfix;
    private LuaFunction m_OnDoShareCallBack_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public MissionComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveProcessingMissionById(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMonthCardVaildEveryDayFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMonthCardVaildCallBack(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitProcessingMission(List<Mission> processingMissionList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetProcessingMissionByMissionType(MissionType missionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitExistMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushEverydayMissionEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ResetEverydayMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitMissionListByMissionPeriodType(MissionPeriodType missionPeriod)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsRefluxActivityHasOpening()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool CanUnlockMissionInLogicFromInit(ConfigDataMissionInfo missionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnDirectActivatedMissionLocked(ConfigDataMissionInfo missionConfig)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsNoviceMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsRefluxMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsRefluxMission(ConfigDataMissionInfo mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsNoviceMission(ConfigDataMissionInfo mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsNoviceMissionActivated(ConfigDataMissionInfo mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsNoviceMissionActivatedForRewarding(ConfigDataMissionInfo mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsNoviceMissionActivated(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsRefluxMissionActivated(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsRefluxMissionActivated(ConfigDataMissionInfo mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsRefluxMissionActivatedForRewarding(ConfigDataMissionInfo mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanGetRewarding(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool CanUnlockMissionInLogic(ConfigDataMissionInfo missionConfigInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool CanUnlockMission(ConfigDataMissionInfo missionConfigInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected DirectlyActivatedMissionSatatus CaculateDirectlyActivationMissionStatus(
      ConfigDataMissionInfo missionConfigInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool AddMission(ConfigDataMissionInfo missionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnAddProcessingDirectelyActivitedMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Mission GetProcessingMissionByMissionPeriod(
      MissionPeriodType missionPeriodType,
      int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGetProcessingDirectActivationMissionFail(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddEverydayMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void AddOneOffMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsMissionExist(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMissionFinished(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitMissionsFromConfig(List<Mission> missions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void FinishMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ConfigDataMissionInfo> GetAllEverydayMissionConfigByPreMissionId(
      int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ConfigDataMissionInfo> GetAllEverydayMissionListByPlayerLevel(
      int playerLvl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ConfigDataMissionInfo> GetAllEverydayMissionListByScenario(
      int scenario)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllProcessingRefluxMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllFinishedRefluxMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllProcessingNoviceMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllFinishedNoviceMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMissionListByMissionComplete(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMissionListByLevelUp(int playerLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMissionListByScenario(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMissionListByNewMissionConfigList(
      List<ConfigDataMissionInfo> newMissionConfigList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AddMissionProcess(Mission mission, long process)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMissionMaxProcess(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMissionProcessFinished(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanGainMissionReward(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegisterMissionCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsCompleted(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionFinishCallBack(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnKillGoblinCallBack(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentStarLevelUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJoinGuildCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GuildMassiveCombatAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteTowerFloorCallBack(int floor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectCardMissionCallBack(int cardPoolId, int selectCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionSelectCardCallBack(int cardPoolId, int selectCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void SetStatisticalData(StatisticalDataType typeId, long nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void AddStatisticalData(StatisticalDataType typeId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetMissionCompletedProcess(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSummonHeroCallBack(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConsumeEnergyCallBack(GameFunctionType gameFuncTypeId, int energyCost)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RollbackConsumeEnergy(GameFunctionType gameFuncTypeId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFamGameFuncionType(GameFunctionType gameFuncTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteEventCallBack(bool isRandomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteLevelCallBack(BattleType levelType, int levelId, List<int> fightHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelAttackDiffculityCallBack(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddBattleTypeLevelStatisticalData(BattleType levelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArenaConsecutiveVictoryCallBack(int consecutiveVictoryNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArenaFightCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteScenaioCallBack(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteRiftLevelCallBack(int diffculty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetRiftLevelFightAchievementCallBack(int achievementRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFinishFightAchievementCallBack(int achievementRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetRiftLevelStarCallBack(int starCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConsumeCrystalCallBack(int crystalCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnComsumeGoldCallBack(int goldCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnUseHeroExpItemCallBack(int itemCount = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllHeroAllJobLevelUpCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllHeroAddJobNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankJobHaveCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllHeroAddSkillNumsCallBack(int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllHeroAddSoliderNumsCallBack(int soliderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllJobMasterHeroCallBack(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroMasterJobCallBack(int heroId, int jobRelateId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelUpHeroStarLevelCallBack(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroRankLevelCallBack(int heroRank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddHeroNumsCallBack(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHasAboveLevelHeroNumsCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelUpPlayerLevelCallBack(int upLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseCrystalBuyEnergyCallBack(int buyCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnLoginGameCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNewHeroJobTransferCallBack(int heroId, int jobConnnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSpecificHeroFight(int heroId, int fightNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSpecificHeroLevelUp(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTrainingTechToLevelCallBack(TrainingTech Tech)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentToLevelCallBack(EquipmentBagItem Equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFinishTeamBattleCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFavorabilityToLevelCallBack(Hero Hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFetterToLevelCallBack(Hero Hero, int FetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantPropertiesSaveCallBack(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantEquipmentCallBack(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTotalHeroJobLevelUpCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteFriendCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAssignHeroToTaskCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattlePracticeCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRealTimeArenaBattleStartCallBack(RealTimePVPMode battleMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRealTimeArenaBattleFinishCallBack(RealTimePVPMode battleMode, bool win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRealTimeArenaDanUpdateCallBack(int dan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyGiftStoreGoodsCallBack(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyRechargeStoreGoodsCallBack(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDoShareCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public MissionComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private MissionComponentCommon m_owner;

      public LuaExportHelper(MissionComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public HashSet<ConfigDataMissionInfo> m_lockedMissionConfigsInLogic
      {
        get
        {
          return this.m_owner.m_lockedMissionConfigsInLogic;
        }
        set
        {
          this.m_owner.m_lockedMissionConfigsInLogic = value;
        }
      }

      public HashSet<int> m_existMissions
      {
        get
        {
          return this.m_owner.m_existMissions;
        }
        set
        {
          this.m_owner.m_existMissions = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public SelectCardComponentCommon m_selectCard
      {
        get
        {
          return this.m_owner.m_selectCard;
        }
        set
        {
          this.m_owner.m_selectCard = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public LevelComponentCommon m_level
      {
        get
        {
          return this.m_owner.m_level;
        }
        set
        {
          this.m_owner.m_level = value;
        }
      }

      public RiftComponentCommon m_rift
      {
        get
        {
          return this.m_owner.m_rift;
        }
        set
        {
          this.m_owner.m_rift = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public ThearchyTrialCompomentCommon m_thearchyTrial
      {
        get
        {
          return this.m_owner.m_thearchyTrial;
        }
        set
        {
          this.m_owner.m_thearchyTrial = value;
        }
      }

      public AnikiGymComponentCommon m_anikiGym
      {
        get
        {
          return this.m_owner.m_anikiGym;
        }
        set
        {
          this.m_owner.m_anikiGym = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public ArenaComponentCommon m_arena
      {
        get
        {
          return this.m_owner.m_arena;
        }
        set
        {
          this.m_owner.m_arena = value;
        }
      }

      public HeroDungeonComponentCommon m_heroDungeon
      {
        get
        {
          return this.m_owner.m_heroDungeon;
        }
        set
        {
          this.m_owner.m_heroDungeon = value;
        }
      }

      public HeroTrainningComponentCommon m_heroTrainning
      {
        get
        {
          return this.m_owner.m_heroTrainning;
        }
        set
        {
          this.m_owner.m_heroTrainning = value;
        }
      }

      public MemoryCorridorCompomentCommon m_memoryCorridor
      {
        get
        {
          return this.m_owner.m_memoryCorridor;
        }
        set
        {
          this.m_owner.m_memoryCorridor = value;
        }
      }

      public TreasureMapComponentCommon m_treasureMap
      {
        get
        {
          return this.m_owner.m_treasureMap;
        }
        set
        {
          this.m_owner.m_treasureMap = value;
        }
      }

      public HeroPhantomCompomentCommon m_heroPhantom
      {
        get
        {
          return this.m_owner.m_heroPhantom;
        }
        set
        {
          this.m_owner.m_heroPhantom = value;
        }
      }

      public CooperateBattleCompomentCommon m_cooperateBattle
      {
        get
        {
          return this.m_owner.m_cooperateBattle;
        }
        set
        {
          this.m_owner.m_cooperateBattle = value;
        }
      }

      public TrainingGroundCompomentCommon m_trainingGround
      {
        get
        {
          return this.m_owner.m_trainingGround;
        }
        set
        {
          this.m_owner.m_trainingGround = value;
        }
      }

      public TeamComponentCommon m_team
      {
        get
        {
          return this.m_owner.m_team;
        }
        set
        {
          this.m_owner.m_team = value;
        }
      }

      public NoviceComponentCommon m_novice
      {
        get
        {
          return this.m_owner.m_novice;
        }
        set
        {
          this.m_owner.m_novice = value;
        }
      }

      public RefluxComponentCommon m_reflux
      {
        get
        {
          return this.m_owner.m_reflux;
        }
        set
        {
          this.m_owner.m_reflux = value;
        }
      }

      public FriendComponentCommon m_friend
      {
        get
        {
          return this.m_owner.m_friend;
        }
        set
        {
          this.m_owner.m_friend = value;
        }
      }

      public HeroAssistantsCompomentCommon m_heroAssistants
      {
        get
        {
          return this.m_owner.m_heroAssistants;
        }
        set
        {
          this.m_owner.m_heroAssistants = value;
        }
      }

      public ResourceComponentCommon m_resource
      {
        get
        {
          return this.m_owner.m_resource;
        }
        set
        {
          this.m_owner.m_resource = value;
        }
      }

      public RealTimePVPComponentCommon m_realTimePvp
      {
        get
        {
          return this.m_owner.m_realTimePvp;
        }
        set
        {
          this.m_owner.m_realTimePvp = value;
        }
      }

      public GiftStoreComponentCommon m_giftStore
      {
        get
        {
          return this.m_owner.m_giftStore;
        }
        set
        {
          this.m_owner.m_giftStore = value;
        }
      }

      public RechargeStoreComponentCommon m_rechargeStore
      {
        get
        {
          return this.m_owner.m_rechargeStore;
        }
        set
        {
          this.m_owner.m_rechargeStore = value;
        }
      }

      public ClimbTowerComponentCommon m_climb
      {
        get
        {
          return this.m_owner.m_climb;
        }
        set
        {
          this.m_owner.m_climb = value;
        }
      }

      public GuildComponentCommon m_guild
      {
        get
        {
          return this.m_owner.m_guild;
        }
        set
        {
          this.m_owner.m_guild = value;
        }
      }

      public EternalShrineCompomentCommon m_eternalShrine
      {
        get
        {
          return this.m_owner.m_eternalShrine;
        }
        set
        {
          this.m_owner.m_eternalShrine = value;
        }
      }

      public HeroAnthemComponentCommon m_heroAnthem
      {
        get
        {
          return this.m_owner.m_heroAnthem;
        }
        set
        {
          this.m_owner.m_heroAnthem = value;
        }
      }

      public AncientCallComponentCommon m_ancientCall
      {
        get
        {
          return this.m_owner.m_ancientCall;
        }
        set
        {
          this.m_owner.m_ancientCall = value;
        }
      }

      public void OnMonthCardVaildEveryDayFlush()
      {
        this.m_owner.OnMonthCardVaildEveryDayFlush();
      }

      public void OnMonthCardVaildCallBack(int monthCardId)
      {
        this.m_owner.OnMonthCardVaildCallBack(monthCardId);
      }

      public void InitProcessingMission(List<Mission> processingMissionList)
      {
        this.m_owner.InitProcessingMission(processingMissionList);
      }

      public void InitExistMissions()
      {
        this.m_owner.InitExistMissions();
      }

      public void OnFlushEverydayMissionEvent()
      {
        this.m_owner.OnFlushEverydayMissionEvent();
      }

      public void ResetEverydayMissions()
      {
        this.m_owner.ResetEverydayMissions();
      }

      public void InitMissionListByMissionPeriodType(MissionPeriodType missionPeriod)
      {
        this.m_owner.InitMissionListByMissionPeriodType(missionPeriod);
      }

      public bool IsRefluxActivityHasOpening()
      {
        return this.m_owner.IsRefluxActivityHasOpening();
      }

      public bool CanUnlockMissionInLogicFromInit(ConfigDataMissionInfo missionInfo)
      {
        return this.m_owner.CanUnlockMissionInLogicFromInit(missionInfo);
      }

      public void OnDirectActivatedMissionLocked(ConfigDataMissionInfo missionConfig)
      {
        this.m_owner.OnDirectActivatedMissionLocked(missionConfig);
      }

      public bool IsNoviceMission(Mission mission)
      {
        return this.m_owner.IsNoviceMission(mission);
      }

      public bool IsRefluxMission(Mission mission)
      {
        return this.m_owner.IsRefluxMission(mission);
      }

      public bool IsRefluxMission(ConfigDataMissionInfo mission)
      {
        return this.m_owner.IsRefluxMission(mission);
      }

      public bool IsNoviceMission(ConfigDataMissionInfo mission)
      {
        return this.m_owner.IsNoviceMission(mission);
      }

      public bool IsNoviceMissionActivated(ConfigDataMissionInfo mission)
      {
        return this.m_owner.IsNoviceMissionActivated(mission);
      }

      public bool IsNoviceMissionActivatedForRewarding(ConfigDataMissionInfo mission)
      {
        return this.m_owner.IsNoviceMissionActivatedForRewarding(mission);
      }

      public bool IsNoviceMissionActivated(Mission mission)
      {
        return this.m_owner.IsNoviceMissionActivated(mission);
      }

      public bool IsRefluxMissionActivated(Mission mission)
      {
        return this.m_owner.IsRefluxMissionActivated(mission);
      }

      public bool IsRefluxMissionActivated(ConfigDataMissionInfo mission)
      {
        return this.m_owner.IsRefluxMissionActivated(mission);
      }

      public bool IsRefluxMissionActivatedForRewarding(ConfigDataMissionInfo mission)
      {
        return this.m_owner.IsRefluxMissionActivatedForRewarding(mission);
      }

      public bool CanUnlockMissionInLogic(ConfigDataMissionInfo missionConfigInfo)
      {
        return this.m_owner.CanUnlockMissionInLogic(missionConfigInfo);
      }

      public bool CanUnlockMission(ConfigDataMissionInfo missionConfigInfo)
      {
        return this.m_owner.CanUnlockMission(missionConfigInfo);
      }

      public DirectlyActivatedMissionSatatus CaculateDirectlyActivationMissionStatus(
        ConfigDataMissionInfo missionConfigInfo)
      {
        return this.m_owner.CaculateDirectlyActivationMissionStatus(missionConfigInfo);
      }

      public bool AddMission(ConfigDataMissionInfo missionInfo)
      {
        return this.m_owner.AddMission(missionInfo);
      }

      public void OnAddProcessingDirectelyActivitedMission(Mission mission)
      {
        this.m_owner.OnAddProcessingDirectelyActivitedMission(mission);
      }

      public Mission GetProcessingMissionByMissionPeriod(
        MissionPeriodType missionPeriodType,
        int missionId)
      {
        return this.m_owner.GetProcessingMissionByMissionPeriod(missionPeriodType, missionId);
      }

      public void OnGetProcessingDirectActivationMissionFail(Mission mission)
      {
        this.m_owner.OnGetProcessingDirectActivationMissionFail(mission);
      }

      public void AddEverydayMission(Mission mission)
      {
        this.m_owner.AddEverydayMission(mission);
      }

      public void AddOneOffMission(Mission mission)
      {
        this.m_owner.AddOneOffMission(mission);
      }

      public bool IsMissionExist(int missionId)
      {
        return this.m_owner.IsMissionExist(missionId);
      }

      public void InitMissionsFromConfig(List<Mission> missions)
      {
        this.m_owner.InitMissionsFromConfig(missions);
      }

      public void FinishMission(Mission mission)
      {
        this.m_owner.FinishMission(mission);
      }

      public List<ConfigDataMissionInfo> GetAllEverydayMissionConfigByPreMissionId(
        int missionId)
      {
        return this.m_owner.GetAllEverydayMissionConfigByPreMissionId(missionId);
      }

      public List<ConfigDataMissionInfo> GetAllEverydayMissionListByPlayerLevel(
        int playerLvl)
      {
        return this.m_owner.GetAllEverydayMissionListByPlayerLevel(playerLvl);
      }

      public List<ConfigDataMissionInfo> GetAllEverydayMissionListByScenario(
        int scenario)
      {
        return this.m_owner.GetAllEverydayMissionListByScenario(scenario);
      }

      public void UpdateMissionListByMissionComplete(int missionId)
      {
        this.m_owner.UpdateMissionListByMissionComplete(missionId);
      }

      public void UpdateMissionListByLevelUp(int playerLevel)
      {
        this.m_owner.UpdateMissionListByLevelUp(playerLevel);
      }

      public void UpdateMissionListByScenario(int scenarioId)
      {
        this.m_owner.UpdateMissionListByScenario(scenarioId);
      }

      public void UpdateMissionListByNewMissionConfigList(
        List<ConfigDataMissionInfo> newMissionConfigList)
      {
        this.m_owner.UpdateMissionListByNewMissionConfigList(newMissionConfigList);
      }

      public void AddMissionProcess(Mission mission, long process)
      {
        this.m_owner.AddMissionProcess(mission, process);
      }

      public void RegisterMissionCallBack()
      {
        this.m_owner.RegisterMissionCallBack();
      }

      public bool IsCompleted(int missionId)
      {
        return this.m_owner.IsCompleted(missionId);
      }

      public void OnMissionFinishCallBack(int missionId)
      {
        this.m_owner.OnMissionFinishCallBack(missionId);
      }

      public void OnKillGoblinCallBack(int nums)
      {
        this.m_owner.OnKillGoblinCallBack(nums);
      }

      public void OnEquipmentStarLevelUp()
      {
        this.m_owner.OnEquipmentStarLevelUp();
      }

      public void OnJoinGuildCallBack()
      {
        this.m_owner.OnJoinGuildCallBack();
      }

      public void OnCompleteTowerFloorCallBack(int floor)
      {
        this.m_owner.OnCompleteTowerFloorCallBack(floor);
      }

      public void OnSelectCardMissionCallBack(int cardPoolId, int selectCount)
      {
        this.m_owner.OnSelectCardMissionCallBack(cardPoolId, selectCount);
      }

      public void OnMissionSelectCardCallBack(int cardPoolId, int selectCount)
      {
        this.m_owner.OnMissionSelectCardCallBack(cardPoolId, selectCount);
      }

      public void SetStatisticalData(StatisticalDataType typeId, long nums)
      {
        this.m_owner.SetStatisticalData(typeId, nums);
      }

      public void AddStatisticalData(StatisticalDataType typeId, int nums)
      {
        this.m_owner.AddStatisticalData(typeId, nums);
      }

      public void OnSummonHeroCallBack(int heroId)
      {
        this.m_owner.OnSummonHeroCallBack(heroId);
      }

      public void OnConsumeEnergyCallBack(GameFunctionType gameFuncTypeId, int energyCost)
      {
        this.m_owner.OnConsumeEnergyCallBack(gameFuncTypeId, energyCost);
      }

      public bool IsFamGameFuncionType(GameFunctionType gameFuncTypeId)
      {
        return this.m_owner.IsFamGameFuncionType(gameFuncTypeId);
      }

      public void OnCompleteEventCallBack(bool isRandomEvent)
      {
        this.m_owner.OnCompleteEventCallBack(isRandomEvent);
      }

      public void OnCompleteLevelCallBack(BattleType levelType, int levelId, List<int> fightHeroes)
      {
        this.m_owner.OnCompleteLevelCallBack(levelType, levelId, fightHeroes);
      }

      public void RiftLevelAttackDiffculityCallBack(int levelId)
      {
        this.m_owner.RiftLevelAttackDiffculityCallBack(levelId);
      }

      public void AddBattleTypeLevelStatisticalData(BattleType levelType)
      {
        this.m_owner.AddBattleTypeLevelStatisticalData(levelType);
      }

      public void OnArenaConsecutiveVictoryCallBack(int consecutiveVictoryNums)
      {
        this.m_owner.OnArenaConsecutiveVictoryCallBack(consecutiveVictoryNums);
      }

      public void OnArenaFightCallBack()
      {
        this.m_owner.OnArenaFightCallBack();
      }

      public void OnCompleteScenaioCallBack(int scenarioId)
      {
        this.m_owner.OnCompleteScenaioCallBack(scenarioId);
      }

      public void OnCompleteRiftLevelCallBack(int diffculty)
      {
        this.m_owner.OnCompleteRiftLevelCallBack(diffculty);
      }

      public void OnGetRiftLevelFightAchievementCallBack(int achievementRelatedId)
      {
        this.m_owner.OnGetRiftLevelFightAchievementCallBack(achievementRelatedId);
      }

      public void OnFinishFightAchievementCallBack(int achievementRelatedId)
      {
        this.m_owner.OnFinishFightAchievementCallBack(achievementRelatedId);
      }

      public void OnGetRiftLevelStarCallBack(int starCount)
      {
        this.m_owner.OnGetRiftLevelStarCallBack(starCount);
      }

      public void OnConsumeCrystalCallBack(int crystalCount)
      {
        this.m_owner.OnConsumeCrystalCallBack(crystalCount);
      }

      public void OnComsumeGoldCallBack(int goldCount)
      {
        this.m_owner.OnComsumeGoldCallBack(goldCount);
      }

      public void OnAllHeroAllJobLevelUpCallBack()
      {
        this.m_owner.OnAllHeroAllJobLevelUpCallBack();
      }

      public void OnAllHeroAddJobNums()
      {
        this.m_owner.OnAllHeroAddJobNums();
      }

      public void OnRankJobHaveCallBack()
      {
        this.m_owner.OnRankJobHaveCallBack();
      }

      public void OnAllHeroAddSkillNumsCallBack(int skillId)
      {
        this.m_owner.OnAllHeroAddSkillNumsCallBack(skillId);
      }

      public void OnAllHeroAddSoliderNumsCallBack(int soliderId)
      {
        this.m_owner.OnAllHeroAddSoliderNumsCallBack(soliderId);
      }

      public void OnAllJobMasterHeroCallBack(int heroId)
      {
        this.m_owner.OnAllJobMasterHeroCallBack(heroId);
      }

      public void OnHeroMasterJobCallBack(int heroId, int jobRelateId)
      {
        this.m_owner.OnHeroMasterJobCallBack(heroId, jobRelateId);
      }

      public void OnLevelUpHeroStarLevelCallBack(int heroId)
      {
        this.m_owner.OnLevelUpHeroStarLevelCallBack(heroId);
      }

      public void UpdateHeroRankLevelCallBack(int heroRank)
      {
        this.m_owner.UpdateHeroRankLevelCallBack(heroRank);
      }

      public void OnAddHeroNumsCallBack(int heroId)
      {
        this.m_owner.OnAddHeroNumsCallBack(heroId);
      }

      public void OnHasAboveLevelHeroNumsCallBack()
      {
        this.m_owner.OnHasAboveLevelHeroNumsCallBack();
      }

      public void OnLevelUpPlayerLevelCallBack(int upLevel)
      {
        this.m_owner.OnLevelUpPlayerLevelCallBack(upLevel);
      }

      public void OnUseCrystalBuyEnergyCallBack(int buyCount)
      {
        this.m_owner.OnUseCrystalBuyEnergyCallBack(buyCount);
      }

      public void OnLoginGameCallBack()
      {
        this.m_owner.OnLoginGameCallBack();
      }

      public void OnNewHeroJobTransferCallBack(int heroId, int jobConnnectionId)
      {
        this.m_owner.OnNewHeroJobTransferCallBack(heroId, jobConnnectionId);
      }

      public void OnSpecificHeroFight(int heroId, int fightNums)
      {
        this.m_owner.OnSpecificHeroFight(heroId, fightNums);
      }

      public void OnSpecificHeroLevelUp(int heroId)
      {
        this.m_owner.OnSpecificHeroLevelUp(heroId);
      }

      public void OnTrainingTechToLevelCallBack(TrainingTech Tech)
      {
        this.m_owner.OnTrainingTechToLevelCallBack(Tech);
      }

      public void OnEquipmentToLevelCallBack(EquipmentBagItem Equipment)
      {
        this.m_owner.OnEquipmentToLevelCallBack(Equipment);
      }

      public void OnFinishTeamBattleCallBack()
      {
        this.m_owner.OnFinishTeamBattleCallBack();
      }

      public void OnFavorabilityToLevelCallBack(Hero Hero)
      {
        this.m_owner.OnFavorabilityToLevelCallBack(Hero);
      }

      public void OnFetterToLevelCallBack(Hero Hero, int FetterId)
      {
        this.m_owner.OnFetterToLevelCallBack(Hero, FetterId);
      }

      public void OnEnchantPropertiesSaveCallBack(EquipmentBagItem equipment)
      {
        this.m_owner.OnEnchantPropertiesSaveCallBack(equipment);
      }

      public void OnEnchantEquipmentCallBack(EquipmentBagItem equipment)
      {
        this.m_owner.OnEnchantEquipmentCallBack(equipment);
      }

      public void OnTotalHeroJobLevelUpCallBack()
      {
        this.m_owner.OnTotalHeroJobLevelUpCallBack();
      }

      public void OnInviteFriendCallBack()
      {
        this.m_owner.OnInviteFriendCallBack();
      }

      public void OnAssignHeroToTaskCallBack()
      {
        this.m_owner.OnAssignHeroToTaskCallBack();
      }

      public void OnBattlePracticeCallBack()
      {
        this.m_owner.OnBattlePracticeCallBack();
      }

      public void OnRealTimeArenaBattleStartCallBack(RealTimePVPMode battleMode)
      {
        this.m_owner.OnRealTimeArenaBattleStartCallBack(battleMode);
      }

      public void OnRealTimeArenaBattleFinishCallBack(RealTimePVPMode battleMode, bool win)
      {
        this.m_owner.OnRealTimeArenaBattleFinishCallBack(battleMode, win);
      }

      public void OnRealTimeArenaDanUpdateCallBack(int dan)
      {
        this.m_owner.OnRealTimeArenaDanUpdateCallBack(dan);
      }

      public void OnBuyGiftStoreGoodsCallBack(int goodsId)
      {
        this.m_owner.OnBuyGiftStoreGoodsCallBack(goodsId);
      }

      public void OnBuyRechargeStoreGoodsCallBack(int goodsId)
      {
        this.m_owner.OnBuyRechargeStoreGoodsCallBack(goodsId);
      }

      public void OnDoShareCallBack()
      {
        this.m_owner.OnDoShareCallBack();
      }
    }
  }
}
