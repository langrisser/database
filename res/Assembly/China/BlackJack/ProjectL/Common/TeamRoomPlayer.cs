﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TeamRoomPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class TeamRoomPlayer
  {
    public string UserId { get; set; }

    public ulong SessionId { get; set; }

    public int ChannelId { get; set; }

    public string Name { get; set; }

    public int HeadIcon { get; set; }

    public int ActiveHeroJobRelatedId { get; set; }

    public int Level { get; set; }

    public int Position { get; set; }

    public int ModenSkinId { get; set; }

    public bool Blessing { get; set; }

    public int Title { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProTeamRoomPlayer TeamRoomPlayerToPbTeamRoomPlayer(
      TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TeamRoomPlayer PbTeamRoomPlayerToTeamRoomPlayer(
      ProTeamRoomPlayer pbPlayer)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
