﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RoleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RoleInfo
  {
    public string UserId { get; set; }

    public int ServerId { get; set; }

    public int PlayerLevel { get; set; }

    public int HeadIcon { get; set; }

    public string Name { get; set; }

    public int LastLoginHours { get; set; }
  }
}
