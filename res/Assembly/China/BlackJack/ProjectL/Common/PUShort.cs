﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PUShort
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public struct PUShort
  {
    private ushort m_Value;
    private ushort m_Check;

    [MethodImpl((MethodImplOptions) 32768)]
    public PUShort(ushort v)
    {
    }

    public static implicit operator PUShort(ushort value)
    {
      return new PUShort(value);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static implicit operator ushort(PUShort p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    private static ushort Encode(ushort v)
    {
      return (ushort) ((uint) v ^ 21845U);
    }

    private static ushort Decode(ushort v)
    {
      return (ushort) ((uint) v ^ 21845U);
    }

    private static ushort Check(ushort v)
    {
      return (ushort) ((uint) v ^ 30583U);
    }
  }
}
