﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BusinessCardPeakArenaData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BusinessCardPeakArenaData
  {
    public PeakArenaStatus Status { get; set; }

    public int Score { get; set; }

    public int Rank { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BusinessCardPeakArenaData FromPB(
      ProBusinessCardPeakArenaData pbData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBusinessCardPeakArenaData ToPB(
      BusinessCardPeakArenaData data)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
