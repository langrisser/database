﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BagComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class BagComponentCommon : IComponentBase
  {
    protected DataSectionBag m_bagDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected ResourceComponentCommon m_resource;
    protected HeroComponentCommon m_hero;
    protected IConfigDataLoader m_configDataLoader;
    private int m_enhanceEquipmentMaterialExp;
    [DoNotToLua]
    private BagComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_IsGoodsEnoughList`1List`1__hotfix;
    private LuaFunction m_ConsumeGoodsList`1List`1List`1GameFunctionTypeString_hotfix;
    private LuaFunction m_CountIncreaseBagSizeList`1_hotfix;
    private LuaFunction m_CountDecreaseBagSizeList`1_hotfix;
    private LuaFunction m_IsBagFullByRandomGoodsInt32List`1List`1_hotfix;
    private LuaFunction m_IsBagFullByGoodsList`1List`1_hotfix;
    private LuaFunction m_IsBagFullByGoodsOperationList`1List`1Int32_hotfix;
    private LuaFunction m_IsBagFullByBagItemsList`1List`1Int32_hotfix;
    private LuaFunction m_IsBagFullInt32_hotfix;
    private LuaFunction m_GetBagSize_hotfix;
    private LuaFunction m_IsBagFullByCurrentSize_hotfix;
    private LuaFunction m_IsHeuristicBagFullList`1_hotfix;
    private LuaFunction m_FilterNonBagItemList`1_hotfix;
    private LuaFunction m_FilterNonBagItemsWhenAddList`1_hotfix;
    private LuaFunction m_TransformHeroGoodsInt32Int32_hotfix;
    private LuaFunction m_IsBagItemContentIdInConfigGoodsTypeInt32_hotfix;
    private LuaFunction m_RemoveAllBagItemsList`1Int32_hotfix;
    private LuaFunction m_RemoveBagItemGoodsTypeInt32Int32UInt64GameFunctionTypeString_hotfix;
    private LuaFunction m_RemoveBagItemByItemBagItemBaseInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_RemoveBagItemByTypeGoodsTypeInt32Int32GameFunctionTypeString_hotfix;
    private LuaFunction m_RemoveBagItemByInstanceIdUInt64GameFunctionTypeString_hotfix;
    private LuaFunction m_RemoveBagItemDirectlyBagItemBaseInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_FindBagItemGoodsTypeInt32UInt64_hotfix;
    private LuaFunction m_FindBagItemByInstanceIdUInt64_hotfix;
    private LuaFunction m_FindBagItemByTypeGoodsTypeInt32_hotfix;
    private LuaFunction m_FindUseableBagItemGoodsTypeInt32_hotfix;
    private LuaFunction m_GetAllBagItems_hotfix;
    private LuaFunction m_IterateAllBagItems_hotfix;
    private LuaFunction m_FindEnoughBagItemsList`1_hotfix;
    private LuaFunction m_UseBagItemGoodsTypeInt32Int32Objectbe_hotfix;
    private LuaFunction m_UseBagItemUseableBagItemInt32Objectbe_hotfix;
    private LuaFunction m_UseBagItemDirectlyUseableBagItemInt32Objectbe_hotfix;
    private LuaFunction m_OpenSelfSelectedBoxInt32GoodsTypeInt32Int32_hotfix;
    private LuaFunction m_OpenHeroSkinSelfSelectedBoxList`1GoodsTypeInt32Int32_hotfix;
    private LuaFunction m_HasEnoughBagItemBagItemBaseInt32_hotfix;
    private LuaFunction m_IsBagItemEnoughBagItemBaseInt32_hotfix;
    private LuaFunction m_IsBagItemEnoughGoodsTypeInt32Int32_hotfix;
    private LuaFunction m_GetTicketIdGameFunctionType_hotfix;
    private LuaFunction m_IsLevelTicketsMaxByUIGameFunctionTypeGoodsTypeInt32Int32_hotfix;
    private LuaFunction m_IsLevelTicketsMaxConfigDataTicketLimitInfoInt32Int32_hotfix;
    private LuaFunction m_SellBagItemUInt64Int32_hotfix;
    private LuaFunction m_CombineSameGoodsAndReplaceExistHeroToFragmentList`1_hotfix;
    private LuaFunction m_CanDecomposeBagItemsList`1_hotfix;
    private LuaFunction m_CanComposeBagItemsInt32Int32_hotfix;
    private LuaFunction m_CanComposeABagItemInt32Int32_hotfix;
    private LuaFunction m_CanDecomposeABagItemGoodsTypeInt32Int32UInt64_hotfix;
    private LuaFunction m_SetBagItemDirtyBagItemBase_hotfix;
    private LuaFunction m_IsPercentBaseBattlePropertyPropertyModifyType_hotfix;
    private LuaFunction m_CanLockAndUnLockEquipmentUInt64_hotfix;
    private LuaFunction m_LockAndUnLockEquipmentUInt64_hotfix;
    private LuaFunction m_CanEnhanceEquipmentUInt64List`1_hotfix;
    private LuaFunction m_CanWearEquipmentByEquipmentTypeBagItemBase_hotfix;
    private LuaFunction m_IsThisEquipmentTypeBagItemBaseEquipmentType_hotfix;
    private LuaFunction m_IsLevelUpEquipmentStarLevelEquipmentBagItem_hotfix;
    private LuaFunction m_CalculateEnhanceEquipmentExpList`1_hotfix;
    private LuaFunction m_CalculateEnhanceEquipmentGoldInt32_hotfix;
    private LuaFunction m_EnhanceEquipmentUInt64List`1_hotfix;
    private LuaFunction m_CanEnchantEquipmentUInt64UInt64_hotfix;
    private LuaFunction m_OutPutEqipmentEnhanceOperateLogEquipmentBagItemInt32Int32List`1_hotfix;
    private LuaFunction m_OutPutEquipmentUpgrageOperateLogEquipmentBagItemEquipmentBagItemInt32Int32List`1_hotfix;
    private LuaFunction m_AddEquipmentExpEquipmentBagItemInt32_hotfix;
    private LuaFunction m_LevelUpEquipmentEquipmentBagItem_hotfix;
    private LuaFunction m_IsEquipmentMaxExpEquipmentBagItem_hotfix;
    private LuaFunction m_CaculateEquipmentNextLevelExpInt32Int32_hotfix;
    private LuaFunction m_IsEquipmentMaxLevelEquipmentBagItem_hotfix;
    private LuaFunction m_GetEquipmentLevelLimitInt32_hotfix;
    private LuaFunction m_CanLevelUpEquipmentStarUInt64UInt64_hotfix;
    private LuaFunction m_GetLevelUpEquipmentStarItemsInt32ConfigDataEquipmentInfo_hotfix;
    private LuaFunction m_CalculateLevelUpEquipmentStarGoldEquipmentBagItem_hotfix;
    private LuaFunction m_LevelUpEquipmentStarUInt64UInt64_hotfix;
    private LuaFunction m_CalculateEquipmentTotalExpEquipmentBagItem_hotfix;
    private LuaFunction m_CalculateDecomposeEquipmentBackGoldEquipmentBagItem_hotfix;
    private LuaFunction m_LevelUpEquipmentStarEquipmentBagItem_hotfix;
    private LuaFunction m_GetEquipmentResonanceNumsByHeroUInt64Hero_hotfix;
    private LuaFunction m_OutPutItemOperationLogGoodsTypeInt32Int32GameFunctionTypeString_hotfix;
    private LuaFunction m_OnSaveEnchantSaveMissionEventEquipmentBagItem_hotfix;
    private LuaFunction m_OnEnchantMissionEventEquipmentBagItem_hotfix;
    private LuaFunction m_OnCreateBagItemEventBagItemBase_hotfix;
    private LuaFunction m_add_UseHeroExpItemMissionEventAction`1_hotfix;
    private LuaFunction m_remove_UseHeroExpItemMissionEventAction`1_hotfix;
    private LuaFunction m_set_m_bagItemFactoryBagItemFactory_hotfix;
    private LuaFunction m_get_m_bagItemFactory_hotfix;
    private LuaFunction m_add_EquipmentLevelupMissionEventAction`1_hotfix;
    private LuaFunction m_remove_EquipmentLevelupMissionEventAction`1_hotfix;
    private LuaFunction m_add_EnchantEquipmentPropertiesSaveMissionEventAction`1_hotfix;
    private LuaFunction m_remove_EnchantEquipmentPropertiesSaveMissionEventAction`1_hotfix;
    private LuaFunction m_add_EnchantEquipmentsMissionEventAction`1_hotfix;
    private LuaFunction m_remove_EnchantEquipmentsMissionEventAction`1_hotfix;
    private LuaFunction m_add_CreateBagItemEventAction`1_hotfix;
    private LuaFunction m_remove_CreateBagItemEventAction`1_hotfix;
    private LuaFunction m_add_EquipmentStarLevelUpEventAction_hotfix;
    private LuaFunction m_remove_EquipmentStarLevelUpEventAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BagComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsGoodsEnough(List<Goods> conditions, out List<BagItemBase> BagItemsInBag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ConsumeGoods(
      List<Goods> needToConsumeGoods,
      List<BagItemBase> BagItemsInBag = null,
      List<BagItemBase> changedGoods = null,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CountIncreaseBagSize(List<Goods> addBagItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CountDecreaseBagSize(List<Goods> deleteBagItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBagFullByRandomGoods(
      int addRandomRewardExpectSize,
      List<Goods> addGoods = null,
      List<Goods> deleteGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBagFullByGoods(List<Goods> addGoods = null, List<Goods> deleteGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBagFullByGoodsOperation(
      List<Goods> addGoods = null,
      List<Goods> deleteGoods = null,
      int addRandomRewardExpectSize = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBagFullByBagItems(
      List<Goods> addBagItems = null,
      List<Goods> deleteBagItems = null,
      int addRandomRewardExpectSize = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBagFull(int expectSize)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBagSize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBagFullByCurrentSize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsHeuristicBagFull(List<Goods> adds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> FilterNonBagItem(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> FilterNonBagItemsWhenAdd(List<Goods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected List<Goods> TransformHeroGoods(int heroId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsBagItemContentIdInConfig(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllBagItems(List<BagItemBase> changedBagItems = null, int removeItemMaxNums = 2147483647)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveBagItem(
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums,
      ulong instanceId,
      GameFunctionType caseId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int RemoveBagItemByItem(
      BagItemBase bagItem,
      int consumeNums,
      GameFunctionType caseId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveBagItemByType(
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums,
      GameFunctionType caseId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveBagItemByInstanceId(
      ulong instanceId,
      GameFunctionType caseId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase RemoveBagItemDirectly(
      BagItemBase bagItem,
      int consumeNums,
      GameFunctionType caseId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase FindBagItem(
      GoodsType goodsTypeId,
      int contentId,
      ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase FindBagItemByInstanceId(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase FindBagItemByType(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UseableBagItem FindUseableBagItem(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BagItemBase> GetAllBagItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<BagItemBase> IterateAllBagItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BagItemBase> FindEnoughBagItems(List<Goods> conditions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int UseBagItem(
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums,
      params object[] param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UseBagItem(UseableBagItem useableBagItem, int consumeNums, params object[] param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int UseBagItemDirectly(
      UseableBagItem useableBagItem,
      int consumeNums,
      params object[] param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int OpenSelfSelectedBox(
      int selectedIndex,
      GoodsType goodsTypeId,
      int contentId,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int OpenHeroSkinSelfSelectedBox(
      List<int> selectedIndexes,
      GoodsType goodsTypeId,
      int contentId,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HasEnoughBagItem(BagItemBase bagItem, int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsBagItemEnough(BagItemBase bagItem, int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBagItemEnough(GoodsType bagItemType, int bagItemId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTicketId(GameFunctionType causeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelTicketsMaxByUI(
      GameFunctionType causeId,
      GoodsType goodTypeId,
      int contentId,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLevelTicketsMax(
      ConfigDataTicketLimitInfo ticketLimitInfo,
      int contentId,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SellBagItem(ulong instanceId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombineSameGoodsAndReplaceExistHeroToFragment(List<Goods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanDecomposeBagItems(List<ProGoods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanComposeBagItems(int itemId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanComposeABagItem(int itemId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanDecomposeABagItem(
      GoodsType goodsTypeId,
      int contentId,
      int nums,
      ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBagItemDirty(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPercentBaseBattleProperty(PropertyModifyType id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLockAndUnLockEquipment(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int LockAndUnLockEquipment(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanEnhanceEquipment(ulong instanceId, List<ulong> materialIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanWearEquipmentByEquipmentType(BagItemBase equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsThisEquipmentType(BagItemBase equipment, EquipmentType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLevelUpEquipmentStarLevel(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateEnhanceEquipmentExp(List<BagItemBase> materials)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateEnhanceEquipmentGold(int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int EnhanceEquipment(ulong instanceId, List<ulong> materialIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanEnchantEquipment(ulong equipmentInstanceId, ulong enchantStoneInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutEqipmentEnhanceOperateLog(
      EquipmentBagItem equipment,
      int preLevel,
      int preExp,
      List<Goods> costItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutEquipmentUpgrageOperateLog(
      EquipmentBagItem equipment,
      EquipmentBagItem material,
      int preLvlLimit,
      int postLvlLimit,
      List<Goods> costItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddEquipmentExp(EquipmentBagItem equipment, int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LevelUpEquipment(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEquipmentMaxExp(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CaculateEquipmentNextLevelExp(int equipmentContentId, int equipmentLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEquipmentMaxLevel(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int GetEquipmentLevelLimit(int equipmentStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpEquipmentStar(ulong instanceId, ulong materialId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> GetLevelUpEquipmentStarItems(
      int star,
      ConfigDataEquipmentInfo equipmentInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateLevelUpEquipmentStarGold(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int LevelUpEquipmentStar(ulong instanceId, ulong materialId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateEquipmentTotalExp(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateDecomposeEquipmentBackGold(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LevelUpEquipmentStar(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int GetEquipmentResonanceNumsByHero(ulong equipmentInstanceId, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutItemOperationLog(
      GoodsType itemTypeId,
      int itemId,
      int nums,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSaveEnchantSaveMissionEvent(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEnchantMissionEvent(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCreateBagItemEvent(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> UseHeroExpItemMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BagItemFactory m_bagItemFactory
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<EquipmentBagItem> EquipmentLevelupMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<EquipmentBagItem> EnchantEquipmentPropertiesSaveMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<EquipmentBagItem> EnchantEquipmentsMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BagItemBase> CreateBagItemEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EquipmentStarLevelUpEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BagComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_UseHeroExpItemMissionEvent(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_UseHeroExpItemMissionEvent(int obj)
    {
      this.UseHeroExpItemMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EquipmentLevelupMissionEvent(EquipmentBagItem obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EquipmentLevelupMissionEvent(EquipmentBagItem obj)
    {
      this.EquipmentLevelupMissionEvent = (Action<EquipmentBagItem>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EnchantEquipmentPropertiesSaveMissionEvent(EquipmentBagItem obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EnchantEquipmentPropertiesSaveMissionEvent(EquipmentBagItem obj)
    {
      this.EnchantEquipmentPropertiesSaveMissionEvent = (Action<EquipmentBagItem>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EnchantEquipmentsMissionEvent(EquipmentBagItem obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EnchantEquipmentsMissionEvent(EquipmentBagItem obj)
    {
      this.EnchantEquipmentsMissionEvent = (Action<EquipmentBagItem>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_CreateBagItemEvent(BagItemBase obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_CreateBagItemEvent(BagItemBase obj)
    {
      this.CreateBagItemEvent = (Action<BagItemBase>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EquipmentStarLevelUpEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EquipmentStarLevelUpEvent()
    {
      this.EquipmentStarLevelUpEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BagComponentCommon m_owner;

      public LuaExportHelper(BagComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_UseHeroExpItemMissionEvent(int obj)
      {
        this.m_owner.__callDele_UseHeroExpItemMissionEvent(obj);
      }

      public void __clearDele_UseHeroExpItemMissionEvent(int obj)
      {
        this.m_owner.__clearDele_UseHeroExpItemMissionEvent(obj);
      }

      public void __callDele_EquipmentLevelupMissionEvent(EquipmentBagItem obj)
      {
        this.m_owner.__callDele_EquipmentLevelupMissionEvent(obj);
      }

      public void __clearDele_EquipmentLevelupMissionEvent(EquipmentBagItem obj)
      {
        this.m_owner.__clearDele_EquipmentLevelupMissionEvent(obj);
      }

      public void __callDele_EnchantEquipmentPropertiesSaveMissionEvent(EquipmentBagItem obj)
      {
        this.m_owner.__callDele_EnchantEquipmentPropertiesSaveMissionEvent(obj);
      }

      public void __clearDele_EnchantEquipmentPropertiesSaveMissionEvent(EquipmentBagItem obj)
      {
        this.m_owner.__clearDele_EnchantEquipmentPropertiesSaveMissionEvent(obj);
      }

      public void __callDele_EnchantEquipmentsMissionEvent(EquipmentBagItem obj)
      {
        this.m_owner.__callDele_EnchantEquipmentsMissionEvent(obj);
      }

      public void __clearDele_EnchantEquipmentsMissionEvent(EquipmentBagItem obj)
      {
        this.m_owner.__clearDele_EnchantEquipmentsMissionEvent(obj);
      }

      public void __callDele_CreateBagItemEvent(BagItemBase obj)
      {
        this.m_owner.__callDele_CreateBagItemEvent(obj);
      }

      public void __clearDele_CreateBagItemEvent(BagItemBase obj)
      {
        this.m_owner.__clearDele_CreateBagItemEvent(obj);
      }

      public void __callDele_EquipmentStarLevelUpEvent()
      {
        this.m_owner.__callDele_EquipmentStarLevelUpEvent();
      }

      public void __clearDele_EquipmentStarLevelUpEvent()
      {
        this.m_owner.__clearDele_EquipmentStarLevelUpEvent();
      }

      public DataSectionBag m_bagDS
      {
        get
        {
          return this.m_owner.m_bagDS;
        }
        set
        {
          this.m_owner.m_bagDS = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public ResourceComponentCommon m_resource
      {
        get
        {
          return this.m_owner.m_resource;
        }
        set
        {
          this.m_owner.m_resource = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public int m_enhanceEquipmentMaterialExp
      {
        get
        {
          return this.m_owner.m_enhanceEquipmentMaterialExp;
        }
        set
        {
          this.m_owner.m_enhanceEquipmentMaterialExp = value;
        }
      }

      public BagItemFactory m_bagItemFactory
      {
        set
        {
          this.m_owner.m_bagItemFactory = value;
        }
      }

      public int CountIncreaseBagSize(List<Goods> addBagItems)
      {
        return this.m_owner.CountIncreaseBagSize(addBagItems);
      }

      public int CountDecreaseBagSize(List<Goods> deleteBagItems)
      {
        return this.m_owner.CountDecreaseBagSize(deleteBagItems);
      }

      public bool IsBagFullByGoodsOperation(
        List<Goods> addGoods,
        List<Goods> deleteGoods,
        int addRandomRewardExpectSize)
      {
        return this.m_owner.IsBagFullByGoodsOperation(addGoods, deleteGoods, addRandomRewardExpectSize);
      }

      public bool IsBagFullByBagItems(
        List<Goods> addBagItems,
        List<Goods> deleteBagItems,
        int addRandomRewardExpectSize)
      {
        return this.m_owner.IsBagFullByBagItems(addBagItems, deleteBagItems, addRandomRewardExpectSize);
      }

      public bool IsHeuristicBagFull(List<Goods> adds)
      {
        return this.m_owner.IsHeuristicBagFull(adds);
      }

      public List<Goods> FilterNonBagItem(List<Goods> goods)
      {
        return this.m_owner.FilterNonBagItem(goods);
      }

      public List<Goods> FilterNonBagItemsWhenAdd(List<Goods> goodsList)
      {
        return this.m_owner.FilterNonBagItemsWhenAdd(goodsList);
      }

      public List<Goods> TransformHeroGoods(int heroId, int nums)
      {
        return this.m_owner.TransformHeroGoods(heroId, nums);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int RemoveBagItemByItem(
        BagItemBase bagItem,
        int consumeNums,
        GameFunctionType caseId,
        string location)
      {
        // ISSUE: unable to decompile the method.
      }

      public bool IsBagItemEnough(BagItemBase bagItem, int consumeNums)
      {
        return this.m_owner.IsBagItemEnough(bagItem, consumeNums);
      }

      public bool IsLevelTicketsMax(
        ConfigDataTicketLimitInfo ticketLimitInfo,
        int contentId,
        int nums)
      {
        return this.m_owner.IsLevelTicketsMax(ticketLimitInfo, contentId, nums);
      }

      public bool IsThisEquipmentType(BagItemBase equipment, EquipmentType type)
      {
        return this.m_owner.IsThisEquipmentType(equipment, type);
      }

      public bool IsLevelUpEquipmentStarLevel(EquipmentBagItem equipment)
      {
        return this.m_owner.IsLevelUpEquipmentStarLevel(equipment);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void OutPutEqipmentEnhanceOperateLog(
        EquipmentBagItem equipment,
        int preLevel,
        int preExp,
        List<Goods> costItems)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void OutPutEquipmentUpgrageOperateLog(
        EquipmentBagItem equipment,
        EquipmentBagItem material,
        int preLvlLimit,
        int postLvlLimit,
        List<Goods> costItems)
      {
        // ISSUE: unable to decompile the method.
      }

      public void AddEquipmentExp(EquipmentBagItem equipment, int exp)
      {
        this.m_owner.AddEquipmentExp(equipment, exp);
      }

      public void LevelUpEquipment(EquipmentBagItem equipment)
      {
        this.m_owner.LevelUpEquipment(equipment);
      }

      public bool IsEquipmentMaxExp(EquipmentBagItem equipment)
      {
        return this.m_owner.IsEquipmentMaxExp(equipment);
      }

      public bool IsEquipmentMaxLevel(EquipmentBagItem equipment)
      {
        return this.m_owner.IsEquipmentMaxLevel(equipment);
      }

      public int GetEquipmentLevelLimit(int equipmentStarLevel)
      {
        return this.m_owner.GetEquipmentLevelLimit(equipmentStarLevel);
      }

      public List<Goods> GetLevelUpEquipmentStarItems(
        int star,
        ConfigDataEquipmentInfo equipmentInfo)
      {
        return this.m_owner.GetLevelUpEquipmentStarItems(star, equipmentInfo);
      }

      public void LevelUpEquipmentStar(EquipmentBagItem equipment)
      {
        this.m_owner.LevelUpEquipmentStar(equipment);
      }

      public int GetEquipmentResonanceNumsByHero(ulong equipmentInstanceId, Hero hero)
      {
        return this.m_owner.GetEquipmentResonanceNumsByHero(equipmentInstanceId, hero);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void OutPutItemOperationLog(
        GoodsType itemTypeId,
        int itemId,
        int nums,
        GameFunctionType causeId,
        string location)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
