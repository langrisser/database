﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.IBPStageRule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public interface IBPStageRule
  {
    void OnBan(int playerIndex, BPStageCommand command);

    void OnPick(int playerIndex, BPStageCommand command);

    void OnTick(long deltaTicks);

    void OnGiveUp(int playerIndex);

    void OnStart();

    int CanExecuteCommand(int playerIndex, BPStageCommand command);

    bool IsFinished();
  }
}
