﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionArenaBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionArenaBattleReport : DataSection
  {
    private Dictionary<ulong, int> m_instanceId2CacheIndex;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionArenaBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitArenaBattleReport(ArenaBattleReport report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaBattleReport(int index, ArenaBattleReport report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddArenaBattleReport(ArenaBattleReport report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DirtyArenaBattleReport(ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<ArenaBattleReport> GetAllArenaBattleReports()
    {
      return this.ArenaBattleReportInfo.GetAllVaildDatas();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport FindArenaBattleReportByInstanceId(
      ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetNextBattleReportIndex(byte index)
    {
      this.NextBattleReportIndex = index;
      this.SetDirty(true);
    }

    public byte NextBattleReportIndex { get; set; }

    public int BattleReportNums
    {
      get
      {
        return this.m_instanceId2CacheIndex.Count;
      }
    }

    public ArenaBattleReportUpdateCache ArenaBattleReportInfo { get; set; }
  }
}
