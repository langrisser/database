﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BattleReport
  {
    public List<BattleCommand> Commands;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleReportHead Head { get; set; }

    public BattleType BattleType { get; set; }

    public int BattleId { get; set; }

    public int RandomSeed { get; set; }
  }
}
