﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ResourceComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ResourceComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionResource m_resourceDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;

    [MethodImpl((MethodImplOptions) 32768)]
    public ResourceComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "Resource";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void EffectValidMonthCard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<MonthCard> GetAllValidMonthCards()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddMonthCard(int monthCardId, DateTime expiredTime, string goodsId = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RemoveMonthCard(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    public MonthCard FindMonthCardById(int cardId)
    {
      return this.m_resourceDS.FindMonthCardById(cardId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMonthCardVaild(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMonthCardValid(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMonthCardInvalid(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasHeadFrameId(int headFrameId)
    {
      return this.m_resourceDS.HasHeadFrameId(headFrameId);
    }

    public virtual void AddHeadFrame(int headFrameId, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      this.m_resourceDS.AddHeadFrame(headFrameId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool AddTitle(int titileId, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddHeroSkin(int heroSkinId, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddSoldierSkin(
      int soldierSkinId,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOwn(GoodsType goodtypeId, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCreateBagItemEventCallBack(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddEquipmentId(int equipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> MonthCardValidEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> MonthCardInvalidEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
