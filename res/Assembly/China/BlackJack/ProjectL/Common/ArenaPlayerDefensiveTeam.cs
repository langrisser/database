﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaPlayerDefensiveTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ArenaPlayerDefensiveTeam
  {
    public List<ArenaPlayerDefensiveHero> Heroes;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerDefensiveTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    public byte BattleId { get; set; }

    public byte ArenaDefenderRuleId { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaPlayerDefensiveTeam PBArenaDefensiveTeamToArenaDefensiveTeam(
      ProArenaPlayerDefensiveTeam pbDefensiveTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaPlayerDefensiveTeam ArenaDefensiveTeamToPBArenaDefensiveTeam(
      ArenaPlayerDefensiveTeam defensiveTeam)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
