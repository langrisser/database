﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.SceneDummyObjectInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [ProtoContract(Name = "SceneDummyObjectInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class SceneDummyObjectInfo : IExtensible
  {
    private uint _objType;
    private int _objConfId;
    private PVector3D _location;
    private PVector3D _rotation;
    private float _scale;
    private bool _createOnSceneCreate;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public SceneDummyObjectInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "objType")]
    public uint ObjType
    {
      get
      {
        return this._objType;
      }
      set
      {
        this._objType = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "objConfId")]
    public int ObjConfId
    {
      get
      {
        return this._objConfId;
      }
      set
      {
        this._objConfId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "location")]
    public PVector3D Location
    {
      get
      {
        return this._location;
      }
      set
      {
        this._location = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "rotation")]
    public PVector3D Rotation
    {
      get
      {
        return this._rotation;
      }
      set
      {
        this._rotation = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "scale")]
    public float Scale
    {
      get
      {
        return this._scale;
      }
      set
      {
        this._scale = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "createOnSceneCreate")]
    public bool CreateOnSceneCreate
    {
      get
      {
        return this._createOnSceneCreate;
      }
      set
      {
        this._createOnSceneCreate = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
