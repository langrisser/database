﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroDungeonComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class HeroDungeonComponentCommon : IComponentBase
  {
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected RiftComponentCommon m_rift;
    protected HeroComponentCommon m_hero;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected DataSectionHeroDungeon m_heroDungeonDS;
    protected IConfigDataLoader m_configDataLoader;

    public string GetName()
    {
      return "HeroDungeon";
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushLevelChallengeNumsEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasGotAchievementRelationId(int achievementRelationId)
    {
      return this.m_heroDungeonDS.HasGotAchievementRelationId(achievementRelationId);
    }

    public bool IsLevelFirstPass(int levelId)
    {
      return !this.IsFinishedLevel(levelId);
    }

    public bool IsFinishedLevel(int levelId)
    {
      return this.m_heroDungeonDS.IsFinishedLevel(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEnoughAttackNums(ConfigDataHeroDungeonLevelInfo levelInfo, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetLevelCanChallengeMaxNums(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      return levelInfo.ChallengeCount;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyChallengeMaxNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitLevel(int chapterId, int levelId, int stars, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetLevel(int chapterId, int LevelId, int stars, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public HeroDungeonLevel FindLevel(int chapterId, int levelId)
    {
      return this.m_heroDungeonDS.FindLevel(chapterId, levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackHeroDungeonLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void SetSuccessHeroDungeonLevel(
      ConfigDataHeroDungeonLevelInfo levelInfo,
      List<int> newGotAchievementRelationInds,
      int stars,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRaidSuccessHeroDungeonLevel(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackHeroDungeonLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRaidHeroDungeonLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLevelByEnergyAndSoOn(ConfigDataHeroDungeonLevelInfo levelInfo, bool isRaid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLevelByRewards(ConfigDataHeroDungeonLevelInfo levelInfo, bool isRaid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroDungeonLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonChapterStar(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGotChapterStarReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainHeroDungeonChapterStarRewards(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected List<Goods> GetHeroDungeonChapterStarRewards(
      ConfigDataHeroInformationInfo chapterInfo,
      int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GainHeroDungeonChapterStarRewards(int chapterId, int index, bool check = false)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void GenerateHeroDungeonChapterStarRewards(int chapterId, int index)
    {
    }

    public bool HasRewardAddRelativeOperationalActivity { get; set; }

    public int OperationalActivityChanllengenumsAdd { get; set; }

    public event Action<BattleType, int, List<int>> CompleteHeroDungeonLevelMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
