﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BPStagePeakArenaRule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class BPStagePeakArenaRule : IBPStageRule
  {
    public List<long> PublicTimeSpan;
    public bool EarlyFinished;
    private BPStage m_stage;
    private IConfigDataLoader m_configDataLoader;
    private readonly long BanTimeSpan;
    private readonly long PickTimeSpan;
    private readonly List<PeakAreanaBPFlowInfo> m_flowInfos;
    private IBPStageListener m_listener;
    [DoNotToLua]
    private BPStagePeakArenaRule.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorBPStageIConfigDataLoaderIBPStageListener_hotfix;
    private LuaFunction m_OnStart_hotfix;
    private LuaFunction m_CanExecuteCommandInt32BPStageCommand_hotfix;
    private LuaFunction m_OnBanInt32BPStageCommand_hotfix;
    private LuaFunction m_OnPickInt32BPStageCommand_hotfix;
    private LuaFunction m_OnGiveUpInt32_hotfix;
    private LuaFunction m_OnTickInt64_hotfix;
    private LuaFunction m_NextTurn_hotfix;
    private LuaFunction m_GeneratePlayerCommand_hotfix;
    private LuaFunction m_IsFinished_hotfix;
    private LuaFunction m_GetCurrentFlowInfo_hotfix;
    private LuaFunction m_get_CurrentTurn_hotfix;
    private LuaFunction m_set_CurrentTurnInt32_hotfix;
    private LuaFunction m_get_CurrentPlayerIndex_hotfix;
    private LuaFunction m_set_CurrentPlayerIndexInt32_hotfix;
    private LuaFunction m_get_TurnTimeSpan_hotfix;
    private LuaFunction m_set_TurnTimeSpanInt64_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStagePeakArenaRule(
      BPStage stage,
      IConfigDataLoader configDataLoader,
      IBPStageListener listener = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanExecuteCommand(int playerIndex, BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBan(int playerIndex, BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPick(int playerIndex, BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnGiveUp(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnTick(long deltaTicks)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStageCommand GeneratePlayerCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakAreanaBPFlowInfo GetCurrentFlowInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CurrentTurn
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentPlayerIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long TurnTimeSpan
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BPStagePeakArenaRule.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BPStagePeakArenaRule m_owner;

      public LuaExportHelper(BPStagePeakArenaRule owner)
      {
        this.m_owner = owner;
      }

      public BPStage m_stage
      {
        get
        {
          return this.m_owner.m_stage;
        }
        set
        {
          this.m_owner.m_stage = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public long BanTimeSpan
      {
        get
        {
          return this.m_owner.BanTimeSpan;
        }
      }

      public long PickTimeSpan
      {
        get
        {
          return this.m_owner.PickTimeSpan;
        }
      }

      public List<PeakAreanaBPFlowInfo> m_flowInfos
      {
        get
        {
          return this.m_owner.m_flowInfos;
        }
      }

      public IBPStageListener m_listener
      {
        get
        {
          return this.m_owner.m_listener;
        }
        set
        {
          this.m_owner.m_listener = value;
        }
      }

      public void NextTurn()
      {
        this.m_owner.NextTurn();
      }
    }
  }
}
