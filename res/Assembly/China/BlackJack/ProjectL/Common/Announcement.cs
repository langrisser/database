﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.Announcement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class Announcement
  {
    public ulong InstanceId { get; set; }

    public DateTime ShowStartTime { get; set; }

    public DateTime ShowEndTime { get; set; }

    public string Title { get; set; }

    public string Content { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Announcement PBAnnouncementToAnnouncement(
      ProAnnouncement pbAnnouncement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProAnnouncement AnnouncementToPBAnnouncement(
      Announcement announcement)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
