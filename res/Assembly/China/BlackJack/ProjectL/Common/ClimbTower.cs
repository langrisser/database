﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ClimbTower
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ClimbTower
  {
    public int Floor { get; set; }

    public int HistoryFloorMax { get; set; }

    public DateTime NextFlushTime { get; set; }

    public DateTime TowerFloorUpdateTime { get; set; }

    public DateTime LastTowerFloorUpdateTime { get; set; }

    public int LastTowerFloorRank { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProClimbTower ToPb(GlobalClimbTowerInfo globalClimbTowerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProClimbTowerFloor ToPb(
      GlobalClimbTowerFloor globalClimbTowerFloor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GlobalClimbTowerInfo FromPb(ProClimbTower pbClimbTower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GlobalClimbTowerFloor FromPb(
      ProClimbTowerFloor pbClimbTowerFloor)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
