﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ChatVoiceMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  [Serializable]
  public class ChatVoiceMessage : ChatMessage
  {
    public ulong InstanceId { get; set; }

    public byte[] VoiceData { get; set; }

    public int VoiceLength { get; set; }

    public int AudioFrequency { get; set; }

    public int SampleLength { get; set; }

    public bool IsOverdued { get; set; }

    public string TranslateText { get; set; }
  }
}
