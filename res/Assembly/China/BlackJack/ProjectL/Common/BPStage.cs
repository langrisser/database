﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BPStage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BPStage
  {
    public List<BPStageCommand> ExecutedCommands;
    public List<BPStageHeroSetupInfo> m_setupInfos;
    private List<int> m_bannedActorIds;
    private List<int> m_pickedActorIds;
    public IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStage()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Init(IBPStageRule rule, IConfigDataLoader configDataLoader)
    {
      this.m_configDataLoader = configDataLoader;
      this.m_bpRule = rule;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(int seed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBPStage ToPB(BPStage stage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BPStage FromPB(ProBPStage stage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetupBPStageHeroes(int playerIndex, List<BPStageHero> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetPickedHero(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetBannedHero(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(long deltaTicks)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ExecuteCommand(int playerIndex, BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStageCommand GenerateFinishedCommand(
      int playerIndex,
      BPStageCommandType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBannedOrPicked(List<int> actorIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBannedOrPicked(int actorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(List<int> actorIds)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsBanned(int actorId)
    {
      return this.m_bannedActorIds.Contains(actorId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPicked(List<int> actorIds)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsPicked(int actorId)
    {
      return this.m_pickedActorIds.Contains(actorId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BPStageHeroSetupInfo> GetSetupInfos(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BPStageHeroSetupInfo> GetSetupInfosByPred(
      Predicate<BPStageHeroSetupInfo> match)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStageHeroSetupInfo GetSetupInfoByActorId(int actorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetBPRule<T>() where T : class, IBPStageRule
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Ban(int playerIndex, BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Pick(int playerIndex, BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<int> GetPickedActorIds()
    {
      return this.m_pickedActorIds;
    }

    public List<int> GetBannedActorIds()
    {
      return this.m_bannedActorIds;
    }

    public int m_randomSeed { get; set; }

    public RandomNumber Random { get; set; }

    private IBPStageRule m_bpRule { get; set; }
  }
}
