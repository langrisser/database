﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DanmakuComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DanmakuComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;

    public string GetName()
    {
      return "Danmaku";
    }

    public virtual void Init()
    {
    }

    public virtual void PostInit()
    {
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetLevelDanmaku(int gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanPostLevelDanmaku(
      int gameFunctionTypeId,
      int locationId,
      List<PostDanmakuEntry> entries)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string CombineLevelDanmakuKey(int gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TruncateDanmakuOrNot(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TruncatePeakArenaDanmakuOrNot(string text)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
