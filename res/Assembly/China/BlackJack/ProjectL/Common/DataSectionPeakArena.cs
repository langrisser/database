﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionPeakArena
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class DataSectionPeakArena : DataSection
  {
    public PeakArenaMatchStats FriendlyMatchStats;
    public PeakArenaMatchStats LadderMatchStats;
    public PeakArenaMatchStats FriendlyCareerMatchStats;
    public PeakArenaMatchStats LadderCareerMatchStats;
    public List<int> WinsBonusIdAcquired;
    public List<int> RegularRewardIndexAcquired;
    public int CurrentSeasonRealTimeRank;
    public bool NoticeEnterPlayOffRace;
    public PeakArenaBattleTeam BattleTeam;
    public List<PeakArenaBattleReportNameRecord> BattleReportNameRecords;
    public List<BattleReportHead> BattleReports;
    public bool BattleReportInited;
    public PeakArenaBetInfo BetInfo;
    public ulong LiveRoomId;
    public DateTime BannedTime;
    [DoNotToLua]
    private DataSectionPeakArena.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_ClearInitedData_hotfix;
    private LuaFunction m_SerializeToClient_hotfix;
    private LuaFunction m_SetNoticeEnterPlayOffRaceBoolean_hotfix;
    private LuaFunction m_WeeklyRefresh_hotfix;
    private LuaFunction m_SaveStatisticsInt32Boolean_hotfix;
    private LuaFunction m_StartNewSeasonInt32Int32_hotfix;
    private LuaFunction m_AcquireWinsBonusInt32_hotfix;
    private LuaFunction m_AcquireRegularRewardInt32_hotfix;
    private LuaFunction m_AddBattleReportNameRecordUInt64String_hotfix;
    private LuaFunction m_GetBattleReportNameUInt64_hotfix;
    private LuaFunction m_ExistBattleReportNameRecordUInt64_hotfix;
    private LuaFunction m_RemoveFirstBattleReportRecord_hotfix;
    private LuaFunction m_AddBattleReportBattleReportHead_hotfix;
    private LuaFunction m_RemoveFirstBattleReport_hotfix;
    private LuaFunction m_BetPeakArenaInt32Int32StringInt32_hotfix;
    private LuaFunction m_SettlePeakArenaMatchupBetRewardInt32_hotfix;
    private LuaFunction m_BetDailyFlush_hotfix;
    private LuaFunction m_SetLiveRoomIdUInt64_hotfix;
    private LuaFunction m_IsBannedDateTime_hotfix;
    private LuaFunction m_BanDateTime_hotfix;
    private LuaFunction m_ResetBattleReportNameRecords_hotfix;
    private LuaFunction m_get_PreRankingScore_hotfix;
    private LuaFunction m_set_PreRankingScoreInt32_hotfix;
    private LuaFunction m_get_CurrentSeason_hotfix;
    private LuaFunction m_set_CurrentSeasonInt32_hotfix;
    private LuaFunction m_get_CurrentSeasonFinalRank_hotfix;
    private LuaFunction m_set_CurrentSeasonFinalRankInt32_hotfix;
    private LuaFunction m_get_TodaysQuickLeavingCount_hotfix;
    private LuaFunction m_set_TodaysQuickLeavingCountInt32_hotfix;
    private LuaFunction m_get_MatchmakingBannedTime_hotfix;
    private LuaFunction m_set_MatchmakingBannedTimeDateTime_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionPeakArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNoticeEnterPlayOffRace(bool notice)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WeeklyRefresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveStatistics(int Type, bool Win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartNewSeason(int initScore, int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AcquireWinsBonus(int BonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AcquireRegularReward(int RewardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBattleReportNameRecord(ulong battleReportId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetBattleReportName(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExistBattleReportNameRecord(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveFirstBattleReportRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBattleReport(BattleReportHead report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveFirstBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BetPeakArena(int seasonId, int matchupId, string userId, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SettlePeakArenaMatchupBetReward(int matchupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BetDailyFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLiveRoomId(ulong liveRoomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Ban(DateTime banTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetBattleReportNameRecords()
    {
      // ISSUE: unable to decompile the method.
    }

    public int PreRankingScore
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentSeason
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentSeasonFinalRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TodaysQuickLeavingCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime MatchmakingBannedTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public DataSectionPeakArena.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_InitVersion(ushort dbVersion, ushort clientVersion)
    {
      this.InitVersion(dbVersion, clientVersion);
    }

    private void __callBase_SetDirty(bool needCommit2Client)
    {
      this.SetDirty(needCommit2Client);
    }

    private bool __callBase_NeedSyncToDB()
    {
      return this.NeedSyncToDB();
    }

    private void __callBase_OnDBSynced()
    {
      this.OnDBSynced();
    }

    private void __callBase_SetClientCommitedVersion(ushort version)
    {
      this.SetClientCommitedVersion(version);
    }

    private bool __callBase_NeedSyncToClient()
    {
      return this.NeedSyncToClient();
    }

    private void __callBase_OnClientSynced()
    {
      this.OnClientSynced();
    }

    private object __callBase_SerializeToClient()
    {
      return base.SerializeToClient();
    }

    private void __callBase_ClearInitedData()
    {
      base.ClearInitedData();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private DataSectionPeakArena m_owner;

      public LuaExportHelper(DataSectionPeakArena owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_InitVersion(ushort dbVersion, ushort clientVersion)
      {
        this.m_owner.__callBase_InitVersion(dbVersion, clientVersion);
      }

      public void __callBase_SetDirty(bool needCommit2Client)
      {
        this.m_owner.__callBase_SetDirty(needCommit2Client);
      }

      public bool __callBase_NeedSyncToDB()
      {
        return this.m_owner.__callBase_NeedSyncToDB();
      }

      public void __callBase_OnDBSynced()
      {
        this.m_owner.__callBase_OnDBSynced();
      }

      public void __callBase_SetClientCommitedVersion(ushort version)
      {
        this.m_owner.__callBase_SetClientCommitedVersion(version);
      }

      public bool __callBase_NeedSyncToClient()
      {
        return this.m_owner.__callBase_NeedSyncToClient();
      }

      public void __callBase_OnClientSynced()
      {
        this.m_owner.__callBase_OnClientSynced();
      }

      public object __callBase_SerializeToClient()
      {
        return this.m_owner.__callBase_SerializeToClient();
      }

      public void __callBase_ClearInitedData()
      {
        this.m_owner.__callBase_ClearInitedData();
      }
    }
  }
}
