﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PlayerBasicInfoComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class PlayerBasicInfoComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected LevelComponentCommon m_level;
    protected BagComponentCommon m_bag;
    protected MissionComponentCommon m_mission;
    protected RiftComponentCommon m_rift;
    protected HeroComponentCommon m_hero;
    protected ThearchyTrialCompomentCommon m_thearchyTrial;
    protected ResourceComponentCommon m_resource;
    protected GuildComponentCommon m_guild;
    protected PeakArenaComponentCommon m_peakArena;
    protected List<BagItemBase> m_changedGoods;
    public DataSectionPlayerBasicInfo m_playerBasicDS;
    [DoNotToLua]
    private PlayerBasicInfoComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_BuyJettonBoolean_hotfix;
    private LuaFunction m_OnBuyJettonGoodsSettlement_hotfix;
    private LuaFunction m_CanBuyJetton_hotfix;
    private LuaFunction m_TryUpdateSignedDays_hotfix;
    private LuaFunction m_TickTitleExpired_hotfix;
    private LuaFunction m_IsGameFunctionOpenedGameFunctionType_hotfix;
    private LuaFunction m_GetCreateTime_hotfix;
    private LuaFunction m_GetRefluxBeginTime_hotfix;
    private LuaFunction m_GetCreateTimeUtc_hotfix;
    private LuaFunction m_GetLastLogoutTime_hotfix;
    private LuaFunction m_GetLoginTime_hotfix;
    private LuaFunction m_GetPlayerName_hotfix;
    private LuaFunction m_GetUserId_hotfix;
    private LuaFunction m_IsMeString_hotfix;
    private LuaFunction m_GetCurrentLevelExp_hotfix;
    private LuaFunction m_GetRechargedCsystal_hotfix;
    private LuaFunction m_GetRechargeRMB_hotfix;
    private LuaFunction m_GetHeadIcon_hotfix;
    private LuaFunction m_GetDefaultHeadIcon_hotfix;
    private LuaFunction m_GetHeadPortrait_hotfix;
    private LuaFunction m_GetHeadFrame_hotfix;
    private LuaFunction m_IsPlayerLevelMax_hotfix;
    private LuaFunction m_CanAddPlayerExp_hotfix;
    private LuaFunction m_AddPlayerExpInt32_hotfix;
    private LuaFunction m_OnLevelChangeInt32Int32_hotfix;
    private LuaFunction m_LevelUp_hotfix;
    private LuaFunction m_AddChallengePointInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_AddFashionPointInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_AddGoldInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_IsGoldEnoughInt32_hotfix;
    private LuaFunction m_IsGoldOverFlowInt32_hotfix;
    private LuaFunction m_GetGold_hotfix;
    private LuaFunction m_AddBrillianceMithralStoneInt32_hotfix;
    private LuaFunction m_AddMithralStoneInt32_hotfix;
    private LuaFunction m_IsMithralStoneEnoughInt32_hotfix;
    private LuaFunction m_IsBrillianceMithralStoneEnoughInt32_hotfix;
    private LuaFunction m_IsCurrencyEnoughGoodsTypeInt32_hotfix;
    private LuaFunction m_AddRechargeRMBInt32DateTimeBoolean_hotfix;
    private LuaFunction m_AddCrystalInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_IsCrystalEnoughInt32_hotfix;
    private LuaFunction m_InitEnergyInt64_hotfix;
    private LuaFunction m_IsReachEnergyMaxInt64_hotfix;
    private LuaFunction m_FlushEnergy_hotfix;
    private LuaFunction m_CanFlushEnergy_hotfix;
    private LuaFunction m_CanFlushPlayerAction_hotfix;
    private LuaFunction m_ResetPlayerActionNextFlushTime_hotfix;
    private LuaFunction m_IncreamentEnergyInt64BooleanGameFunctionTypeString_hotfix;
    private LuaFunction m_DecreaseEnergyInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_IsEnergyEnoughInt32_hotfix;
    private LuaFunction m_GetEnergy_hotfix;
    private LuaFunction m_GetCurrentTime_hotfix;
    private LuaFunction m_IsSigned_hotfix;
    private LuaFunction m_CanSignToday_hotfix;
    private LuaFunction m_CanBuyEnergy_hotfix;
    private LuaFunction m_GetBuyEnergyConfig_hotfix;
    private LuaFunction m_IsBoughtNumsUsedOut_hotfix;
    private LuaFunction m_BuyEnergy_hotfix;
    private LuaFunction m_CanBuyArenaTickets_hotfix;
    private LuaFunction m_GetBuyArenaTicketConfig_hotfix;
    private LuaFunction m_BuyArenaTickets_hotfix;
    private LuaFunction m_GetNextFlushPlayerActionTime_hotfix;
    private LuaFunction m_FlushPlayerAction_hotfix;
    private LuaFunction m_OnPlayerActionFlushEvent_hotfix;
    private LuaFunction m_OnFlush_hotfix;
    private LuaFunction m_IsArenaTicketsEnoughInt32_hotfix;
    private LuaFunction m_IsArenaTicketsFull_hotfix;
    private LuaFunction m_AddArenaTicketsInt32BooleanGameFunctionTypeString_hotfix;
    private LuaFunction m_GetAreanaTicketNums_hotfix;
    private LuaFunction m_AddRechargedCrystalInt32_hotfix;
    private LuaFunction m_AddArenaHonourInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_GetArenaHonour_hotfix;
    private LuaFunction m_AddRealTimePVPHonorInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_GetRealTimePVPHonor_hotfix;
    private LuaFunction m_GetFriendshipPoints_hotfix;
    private LuaFunction m_GetSkinTickets_hotfix;
    private LuaFunction m_AddFriendshipPointsInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_AddSkinTicketsInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_AddMemoryEssenceInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_AddBrillianceMithralStoneInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_AddMithralStoneInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_AddGuildMedalInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_IsGuildMedalEnoughInt32_hotfix;
    private LuaFunction m_IsFriendshipPointsEnoughInt32_hotfix;
    private LuaFunction m_IsArenaHonourEnoughInt32_hotfix;
    private LuaFunction m_IsRealTimePVPHonorEnoughInt32_hotfix;
    private LuaFunction m_IsSkinTicketEnoughInt32_hotfix;
    private LuaFunction m_IsMemoryEssenceEnoughInt32_hotfix;
    private LuaFunction m_IsChallengePointEnoughInt32_hotfix;
    private LuaFunction m_IsFashionPointEnoughInt32_hotfix;
    private LuaFunction m_CanSetUserGuideList`1_hotfix;
    private LuaFunction m_SetUserGuideList`1_hotfix;
    private LuaFunction m_CleanUserGuideList`1_hotfix;
    private LuaFunction m_CompleteAllUserGuides_hotfix;
    private LuaFunction m_IsUserGuideCompletedInt32_hotfix;
    private LuaFunction m_GetLevel_hotfix;
    private LuaFunction m_GetRechargedCrystal_hotfix;
    private LuaFunction m_GetCrystal_hotfix;
    private LuaFunction m_CheckRankingListAddPlayerLevel_hotfix;
    private LuaFunction m_CreateRankingPlayerInfoInt32_hotfix;
    private LuaFunction m_OnRankingListPlayerInfoChange_hotfix;
    private LuaFunction m_OpenGameRating_hotfix;
    private LuaFunction m_IsOpenGameRating_hotfix;
    private LuaFunction m_SetMemoryStoreOpenStatusBoolean_hotfix;
    private LuaFunction m_IsMemoryStoreOpen_hotfix;
    private LuaFunction m_CanGainDialogRewardInt32_hotfix;
    private LuaFunction m_GainDialogRewardInt32_hotfix;
    private LuaFunction m_GenerateDialogRewardInt32_hotfix;
    private LuaFunction m_GetLevelUpAddEnergyFromConfig_hotfix;
    private LuaFunction m_GetNextLevelExpFromConfig_hotfix;
    private LuaFunction m_CanSetHeadPortraitAndHeadFrameInt32Int32_hotfix;
    private LuaFunction m_SetHeadPortraitAndHeadFrameInt32Int32_hotfix;
    private LuaFunction m_CanSetHeadPortraitInt32_hotfix;
    private LuaFunction m_SetHeadPortraitInt32_hotfix;
    private LuaFunction m_get_Title_hotfix;
    private LuaFunction m_set_TitleInt32_hotfix;
    private LuaFunction m_HasTitle_hotfix;
    private LuaFunction m_CanSetHeadFrameInt32_hotfix;
    private LuaFunction m_SetHeadFrameInt32_hotfix;
    private LuaFunction m_OnHeadIconChange_hotfix;
    private LuaFunction m_CanChangePlayerNameString_hotfix;
    private LuaFunction m_ChangePlayerNameString_hotfix;
    private LuaFunction m_TryChangePlayerNameString_hotfix;
    private LuaFunction m_CheckPlayerNameString_hotfix;
    private LuaFunction m_DoShare_hotfix;
    private LuaFunction m_add_LevelUpPlayerLevelEventAction`1_hotfix;
    private LuaFunction m_remove_LevelUpPlayerLevelEventAction`1_hotfix;
    private LuaFunction m_add_ConsumeEnergyMissionEventAction`2_hotfix;
    private LuaFunction m_remove_ConsumeEnergyMissionEventAction`2_hotfix;
    private LuaFunction m_add_ConsumeGoldMissionEventAction`1_hotfix;
    private LuaFunction m_remove_ConsumeGoldMissionEventAction`1_hotfix;
    private LuaFunction m_add_ConsumeCrystalMissionEventAction`1_hotfix;
    private LuaFunction m_remove_ConsumeCrystalMissionEventAction`1_hotfix;
    private LuaFunction m_add_LevelUpPlayerLevelMissionEventAction`1_hotfix;
    private LuaFunction m_remove_LevelUpPlayerLevelMissionEventAction`1_hotfix;
    private LuaFunction m_add_AddBuyEnergyMissionEventAction`1_hotfix;
    private LuaFunction m_remove_AddBuyEnergyMissionEventAction`1_hotfix;
    private LuaFunction m_add_PlayerActionFlushEventAction_hotfix;
    private LuaFunction m_remove_PlayerActionFlushEventAction_hotfix;
    private LuaFunction m_add_ConsumeFriendshipPointsEventAction`1_hotfix;
    private LuaFunction m_remove_ConsumeFriendshipPointsEventAction`1_hotfix;
    private LuaFunction m_add_SetHeadIconEventAction`1_hotfix;
    private LuaFunction m_remove_SetHeadIconEventAction`1_hotfix;
    private LuaFunction m_add_AddRechargeRMBEventAction`2_hotfix;
    private LuaFunction m_remove_AddRechargeRMBEventAction`2_hotfix;
    private LuaFunction m_add_DoShareEventAction_hotfix;
    private LuaFunction m_remove_DoShareEventAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerBasicInfoComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int BuyJetton(bool check = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnBuyJettonGoodsSettlement()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBuyJetton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TryUpdateSignedDays()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickTitleExpired()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGameFunctionOpened(GameFunctionType gameFunctionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCreateTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetRefluxBeginTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCreateTimeUtc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetLastLogoutTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetLoginTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPlayerName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetUserId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMe(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentLevelExp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRechargedCsystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetRechargeRMB()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeadIcon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDefaultHeadIcon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeadPortrait()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeadFrame()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerLevelMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanAddPlayerExp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddPlayerExp(int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLevelChange(int upLevel, int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LevelUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddChallengePoint(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddFashionPoint(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddGold(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGoldEnough(int useGoldCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGoldOverFlow(int addNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddBrillianceMithralStone(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddMithralStone(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMithralStoneEnough(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBrillianceMithralStoneEnough(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsCurrencyEnough(GoodsType currencyType, int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual long AddRechargeRMB(int nums, DateTime rechargeTime, bool needSync2Client = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddCrystal(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCrystalEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitEnergy(long secondPast)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsReachEnergyMax(long currentEnergy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FlushEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanFlushEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanFlushPlayerAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void ResetPlayerActionNextFlushTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int IncreamentEnergy(
      long energyAddNums,
      bool canAboveMaxEnergy,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int DecreaseEnergy(int energyCost, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnergyEnough(int consumeEnergy = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual DateTime GetCurrentTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSigned()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSignToday()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBuyEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataBuyEnergyInfo GetBuyEnergyConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBoughtNumsUsedOut()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int BuyEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBuyArenaTickets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataBuyArenaTicketInfo GetBuyArenaTicketConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int BuyArenaTickets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetNextFlushPlayerActionTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FlushPlayerAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnPlayerActionFlushEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaTicketsEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaTicketsFull()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddArenaTickets(
      int nums,
      bool arenaGiven = true,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAreanaTicketNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddRechargedCrystal(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddArenaHonour(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaHonour()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddRealTimePVPHonor(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRealTimePVPHonor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFriendshipPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSkinTickets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddFriendshipPoints(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddSkinTickets(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddMemoryEssence(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddBrillianceMithralStone(
      int nums,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddMithralStone(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddGuildMedal(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGuildMedalEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFriendshipPointsEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaHonourEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRealTimePVPHonorEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSkinTicketEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMemoryEssenceEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengePointEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFashionPointEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetUserGuide(List<int> completeStepIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int SetUserGuide(List<int> completeStepIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CleanUserGuide(List<int> completeStepIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CompleteAllUserGuides()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUserGuideCompleted(int stepId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRechargedCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckRankingListAddPlayerLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingPlayerInfo CreateRankingPlayerInfo(int championHeroId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnRankingListPlayerInfoChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenGameRating()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpenGameRating()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMemoryStoreOpenStatus(bool open)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMemoryStoreOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainDialogReward(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GainDialogReward(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateDialogReward(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLevelUpAddEnergyFromConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextLevelExpFromConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetHeadPortraitAndHeadFrame(int headPortraitId, int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SetHeadPortraitAndHeadFrame(int headPortraitId, int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetHeadPortrait(int headPortraitId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SetHeadPortrait(int headPortraitId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Title
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasTitle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetHeadFrame(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SetHeadFrame(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnHeadIconChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanChangePlayerName(string newName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ChangePlayerName(string newName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int TryChangePlayerName(string newName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CheckPlayerName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoShare()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> LevelUpPlayerLevelEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GameFunctionType, int> ConsumeEnergyMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> ConsumeGoldMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> ConsumeCrystalMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> LevelUpPlayerLevelMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> AddBuyEnergyMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action PlayerActionFlushEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> ConsumeFriendshipPointsEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> SetHeadIconEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, DateTime> AddRechargeRMBEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action DoShareEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PlayerBasicInfoComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_LevelUpPlayerLevelEvent(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_LevelUpPlayerLevelEvent(int obj)
    {
      this.LevelUpPlayerLevelEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_ConsumeEnergyMissionEvent(GameFunctionType arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_ConsumeEnergyMissionEvent(GameFunctionType arg1, int arg2)
    {
      this.ConsumeEnergyMissionEvent = (Action<GameFunctionType, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_ConsumeGoldMissionEvent(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_ConsumeGoldMissionEvent(int obj)
    {
      this.ConsumeGoldMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_ConsumeCrystalMissionEvent(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_ConsumeCrystalMissionEvent(int obj)
    {
      this.ConsumeCrystalMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_LevelUpPlayerLevelMissionEvent(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_LevelUpPlayerLevelMissionEvent(int obj)
    {
      this.LevelUpPlayerLevelMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_AddBuyEnergyMissionEvent(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_AddBuyEnergyMissionEvent(int obj)
    {
      this.AddBuyEnergyMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_PlayerActionFlushEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_PlayerActionFlushEvent()
    {
      this.PlayerActionFlushEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_ConsumeFriendshipPointsEvent(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_ConsumeFriendshipPointsEvent(int obj)
    {
      this.ConsumeFriendshipPointsEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_SetHeadIconEvent(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_SetHeadIconEvent(int obj)
    {
      this.SetHeadIconEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_AddRechargeRMBEvent(int arg1, DateTime arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_AddRechargeRMBEvent(int arg1, DateTime arg2)
    {
      this.AddRechargeRMBEvent = (Action<int, DateTime>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_DoShareEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_DoShareEvent()
    {
      this.DoShareEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PlayerBasicInfoComponentCommon m_owner;

      public LuaExportHelper(PlayerBasicInfoComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_LevelUpPlayerLevelEvent(int obj)
      {
        this.m_owner.__callDele_LevelUpPlayerLevelEvent(obj);
      }

      public void __clearDele_LevelUpPlayerLevelEvent(int obj)
      {
        this.m_owner.__clearDele_LevelUpPlayerLevelEvent(obj);
      }

      public void __callDele_ConsumeEnergyMissionEvent(GameFunctionType arg1, int arg2)
      {
        this.m_owner.__callDele_ConsumeEnergyMissionEvent(arg1, arg2);
      }

      public void __clearDele_ConsumeEnergyMissionEvent(GameFunctionType arg1, int arg2)
      {
        this.m_owner.__clearDele_ConsumeEnergyMissionEvent(arg1, arg2);
      }

      public void __callDele_ConsumeGoldMissionEvent(int obj)
      {
        this.m_owner.__callDele_ConsumeGoldMissionEvent(obj);
      }

      public void __clearDele_ConsumeGoldMissionEvent(int obj)
      {
        this.m_owner.__clearDele_ConsumeGoldMissionEvent(obj);
      }

      public void __callDele_ConsumeCrystalMissionEvent(int obj)
      {
        this.m_owner.__callDele_ConsumeCrystalMissionEvent(obj);
      }

      public void __clearDele_ConsumeCrystalMissionEvent(int obj)
      {
        this.m_owner.__clearDele_ConsumeCrystalMissionEvent(obj);
      }

      public void __callDele_LevelUpPlayerLevelMissionEvent(int obj)
      {
        this.m_owner.__callDele_LevelUpPlayerLevelMissionEvent(obj);
      }

      public void __clearDele_LevelUpPlayerLevelMissionEvent(int obj)
      {
        this.m_owner.__clearDele_LevelUpPlayerLevelMissionEvent(obj);
      }

      public void __callDele_AddBuyEnergyMissionEvent(int obj)
      {
        this.m_owner.__callDele_AddBuyEnergyMissionEvent(obj);
      }

      public void __clearDele_AddBuyEnergyMissionEvent(int obj)
      {
        this.m_owner.__clearDele_AddBuyEnergyMissionEvent(obj);
      }

      public void __callDele_PlayerActionFlushEvent()
      {
        this.m_owner.__callDele_PlayerActionFlushEvent();
      }

      public void __clearDele_PlayerActionFlushEvent()
      {
        this.m_owner.__clearDele_PlayerActionFlushEvent();
      }

      public void __callDele_ConsumeFriendshipPointsEvent(int obj)
      {
        this.m_owner.__callDele_ConsumeFriendshipPointsEvent(obj);
      }

      public void __clearDele_ConsumeFriendshipPointsEvent(int obj)
      {
        this.m_owner.__clearDele_ConsumeFriendshipPointsEvent(obj);
      }

      public void __callDele_SetHeadIconEvent(int obj)
      {
        this.m_owner.__callDele_SetHeadIconEvent(obj);
      }

      public void __clearDele_SetHeadIconEvent(int obj)
      {
        this.m_owner.__clearDele_SetHeadIconEvent(obj);
      }

      public void __callDele_AddRechargeRMBEvent(int arg1, DateTime arg2)
      {
        this.m_owner.__callDele_AddRechargeRMBEvent(arg1, arg2);
      }

      public void __clearDele_AddRechargeRMBEvent(int arg1, DateTime arg2)
      {
        this.m_owner.__clearDele_AddRechargeRMBEvent(arg1, arg2);
      }

      public void __callDele_DoShareEvent()
      {
        this.m_owner.__callDele_DoShareEvent();
      }

      public void __clearDele_DoShareEvent()
      {
        this.m_owner.__clearDele_DoShareEvent();
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public LevelComponentCommon m_level
      {
        get
        {
          return this.m_owner.m_level;
        }
        set
        {
          this.m_owner.m_level = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public MissionComponentCommon m_mission
      {
        get
        {
          return this.m_owner.m_mission;
        }
        set
        {
          this.m_owner.m_mission = value;
        }
      }

      public RiftComponentCommon m_rift
      {
        get
        {
          return this.m_owner.m_rift;
        }
        set
        {
          this.m_owner.m_rift = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public ThearchyTrialCompomentCommon m_thearchyTrial
      {
        get
        {
          return this.m_owner.m_thearchyTrial;
        }
        set
        {
          this.m_owner.m_thearchyTrial = value;
        }
      }

      public ResourceComponentCommon m_resource
      {
        get
        {
          return this.m_owner.m_resource;
        }
        set
        {
          this.m_owner.m_resource = value;
        }
      }

      public GuildComponentCommon m_guild
      {
        get
        {
          return this.m_owner.m_guild;
        }
        set
        {
          this.m_owner.m_guild = value;
        }
      }

      public PeakArenaComponentCommon m_peakArena
      {
        get
        {
          return this.m_owner.m_peakArena;
        }
        set
        {
          this.m_owner.m_peakArena = value;
        }
      }

      public List<BagItemBase> m_changedGoods
      {
        get
        {
          return this.m_owner.m_changedGoods;
        }
        set
        {
          this.m_owner.m_changedGoods = value;
        }
      }

      public void OnBuyJettonGoodsSettlement()
      {
        this.m_owner.OnBuyJettonGoodsSettlement();
      }

      public void TryUpdateSignedDays()
      {
        this.m_owner.TryUpdateSignedDays();
      }

      public void TickTitleExpired()
      {
        this.m_owner.TickTitleExpired();
      }

      public bool CanAddPlayerExp()
      {
        return this.m_owner.CanAddPlayerExp();
      }

      public void OnLevelChange(int upLevel, int addExp)
      {
        this.m_owner.OnLevelChange(upLevel, addExp);
      }

      public void LevelUp()
      {
        this.m_owner.LevelUp();
      }

      public void InitEnergy(long secondPast)
      {
        this.m_owner.InitEnergy(secondPast);
      }

      public ConfigDataBuyEnergyInfo GetBuyEnergyConfig()
      {
        return this.m_owner.GetBuyEnergyConfig();
      }

      public bool IsBoughtNumsUsedOut()
      {
        return this.m_owner.IsBoughtNumsUsedOut();
      }

      public ConfigDataBuyArenaTicketInfo GetBuyArenaTicketConfig()
      {
        return this.m_owner.GetBuyArenaTicketConfig();
      }

      public void OnPlayerActionFlushEvent()
      {
        this.m_owner.OnPlayerActionFlushEvent();
      }

      public void OnFlush()
      {
        this.m_owner.OnFlush();
      }

      public void GenerateDialogReward(int dialogId)
      {
        this.m_owner.GenerateDialogReward(dialogId);
      }

      public void OnHeadIconChange()
      {
        this.m_owner.OnHeadIconChange();
      }

      public int TryChangePlayerName(string newName)
      {
        return this.m_owner.TryChangePlayerName(newName);
      }
    }
  }
}
