﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AddHeroFavorabilityUseableBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class AddHeroFavorabilityUseableBagItem : UseableBagItem
  {
    public const int SpecificHeroAddFavorabilityExpBasicValue = 10000;
    public List<int> SpecificHeroes;

    [MethodImpl((MethodImplOptions) 32768)]
    public AddHeroFavorabilityUseableBagItem(
      GoodsType goodsTypeId,
      int contentId,
      int nums,
      ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int HaveEffect(IComponentOwner owner, params object[] param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateAddFavorabilityExp(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    private int HeroId { get; set; }

    public int NormalAddExp { get; set; }

    public int SpecificHeroAddFavorabilityExpMultipleValue { get; set; }
  }
}
