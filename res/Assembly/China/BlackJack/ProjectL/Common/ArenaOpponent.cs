﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaOpponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ArenaOpponent
  {
    public string UserId { get; set; }

    public string Name { get; set; }

    public int Level { get; set; }

    public ushort ArenaPoints { get; set; }

    public int HeadIcon { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaOpponent ArenaOpponentToPBArenaOpponent(
      ArenaOpponent arenaOpponent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaOpponent PBArenaOpponentToArenaOpponent(
      ProArenaOpponent pbArenaOpponent)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
