﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionOperationalActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionOperationalActivity : DataSection
  {
    private List<OperationalActivityBase> m_operationalActivities;
    private List<AdvertisementFlowLayout> m_layouts;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionOperationalActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityById(
      ulong operationalActivityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> FindOperationalActivitiesByType(
      OperationalActivityType activityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitOperationalActivities(
      List<OperationalActivityBase> operationalActivities)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOperationalActivities(
      List<OperationalActivityBase> operationalActivities)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddAccumulateLoginTime(
      AccumulateLoginOperationalActivity accumulateLoginOperationalActivity,
      DateTime loginTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddRechargeRMB(AccumulateRechargeOperationalActivity activity, int nums)
    {
      activity.AddRechargeRMB(nums);
      this.SetDirty(true);
    }

    public void AddConsumeCrystal(
      AccumulateConsumeCrystalOperationalActivity activity,
      int nums)
    {
      activity.AddConsumeCrystal(nums);
      this.SetDirty(true);
    }

    public void SetPlayerLevel(
      PlayerLevelUpOperationalActivity playerLevelUpOperationalActivity,
      int playerLevel)
    {
      playerLevelUpOperationalActivity.PlayerLevel = playerLevel;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSpecificLoginTimes(
      SpecificDaysLoginOperationalActivity specificDaysLoginOperationalActivity,
      long time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ExchangeItemGroup(
      LimitedTimeExchangeOperationActivity limitedTimeExchangeOperationActivity,
      int itemGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void GainReward(AwardOperationalActivityBase operationalActivity, int rewardIndex)
    {
      operationalActivity.GainReward(rewardIndex);
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearOperationalActivities()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddNewOperationalActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateOperationalActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveExpiredOperationalActivity(OperationalActivityBase operatinoalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddActivityEventId(EventOperationalActivity activity, int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ClearAdvertisementLayout()
    {
      this.m_layouts.Clear();
    }

    public void InitAdvertisementLayout(AdvertisementFlowLayout layout)
    {
      this.m_layouts.Add(layout);
    }

    public List<AdvertisementFlowLayout> GetAllAdvertisementFlowLayouts()
    {
      return this.m_layouts;
    }

    public List<OperationalActivityBase> AllOperationalActivities
    {
      get
      {
        return this.m_operationalActivities;
      }
    }
  }
}
