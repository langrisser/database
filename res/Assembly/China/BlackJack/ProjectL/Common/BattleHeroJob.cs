﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleHeroJob
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BattleHeroJob
  {
    private int m_jobRelatedId;
    public List<HeroJobRefineryProperty> RefineryProperties;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroJob()
    {
      // ISSUE: unable to decompile the method.
    }

    public int JobRelatedId
    {
      set
      {
        this.m_jobRelatedId = value;
        this.UpdateJobConnectionInfo();
      }
      get
      {
        return this.m_jobRelatedId;
      }
    }

    public int JobLevel { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroJob BattleHeroJobToHeroJob(BattleHeroJob battleHeroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHeroJob HeroJobToBattleHeroJob(HeroJob heroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHeroJob PBBattleHeroJobToBattleHeroJob(
      ProBattleHeroJob pbBattleHeroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleHeroJob BattleHeroJobToPBBattleHeroJob(
      BattleHeroJob battleHeroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateJobConnectionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo { private set; get; }

    public bool IsLevelMax()
    {
      return this.IsLevelMax(this.JobLevel);
    }

    public bool IsLevelMax(int jobLevel)
    {
      return this.JobConnectionInfo.IsJobLevelMax(jobLevel);
    }
  }
}
