﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RealTimePVPBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RealTimePVPBattleReport : BattleReport
  {
    public RealTimePVPBattleReportPlayerData[] PlayerDatas;
    public bool Win;
    public bool IsCancel;

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPBattleReport(BattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPBattleReport DeepCopy()
    {
      // ISSUE: unable to decompile the method.
    }

    public RealTimePVPBattleReportType ReportType { get; set; }

    public BattleRoomBPRule BPRule { get; set; }

    public DateTime CreateTime { get; set; }
  }
}
