﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AccumulateRechargeOperationalActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class AccumulateRechargeOperationalActivity : AwardOperationalActivityBase
  {
    public AccumulateRechargeOperationalActivity()
    {
      this.AccumulateRechargeRMB = 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AccumulateRechargeOperationalActivity(
      ulong instanceId,
      int operationalActivityId,
      OperationalActivityType operationalActivityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeserializeFromPB(
      ProAccumulateRechargeOperationalActivity pbOperationalActivity,
      ConfigDataOperationalActivityInfo config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ToPBNtf(DSOperationalActivityNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAccumulateRechargeOperationalActivity SerializeToPB()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int CanGainRewardByIndex(int rewardIndex, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddRechargeRMB(int addNums)
    {
      this.AccumulateRechargeRMB += addNums;
    }

    public int AccumulateRechargeRMB { get; set; }
  }
}
