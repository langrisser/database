﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaMatchStats
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class PeakArenaMatchStats
  {
    public int Matches;
    public int Wins;
    public int ConsecutiveWins;
    public int ConsecutiveLosses;
    public int WinsThisWeek;
    public List<bool> RecentMatchResults;
    [DoNotToLua]
    private PeakArenaMatchStats.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_AddWins_hotfix;
    private LuaFunction m_AddLosses_hotfix;
    private LuaFunction m_DeepDuplicate_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaMatchStats()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddWins()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLosses()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaMatchStats DeepDuplicate()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public PeakArenaMatchStats.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaMatchStats m_owner;

      public LuaExportHelper(PeakArenaMatchStats owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
