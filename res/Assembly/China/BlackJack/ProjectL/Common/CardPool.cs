﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CardPool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class CardPool
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool(int poolId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int PoolId { get; set; }

    public bool SummonedRareHero { get; set; }

    public int SelectCardCount { get; set; }

    public int DisconnectCount { get; set; }

    public bool IsFirstSignleSelect { get; set; }

    public bool IsFirstTenSelect { get; set; }

    public ulong ActivityInstanceId { get; set; }

    public int GuaranteedEquipmentSuitSelectCardCount { get; set; }

    public int GuaranteedEquipmentSuitSelectCardMaxCount { get; set; }

    public ConfigDataCardPoolInfo Config { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsReachEquipmentSuitGuaranteedValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static CardPool PBCardPoolToCardPool(ProCardPool pbCardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<CardPool> PBActivityCardPoolsToCardPools(
      List<ProCardPool> pbCardPools)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProCardPool CardPoolToPBCardPool(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProCardPool> CardPoolsToPBActivityCardPools(
      List<CardPool> cardPools)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
