﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TrainingGroundCompomentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class TrainingGroundCompomentCommon : IComponentBase
  {
    private IConfigDataLoader _configDataLoader;
    public DataSectionTrainingGround m_trainingGroundDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    protected HeroComponentCommon m_hero;
    protected AnikiGymComponentCommon m_anikiGym;

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingGroundCompomentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "TrainingGround";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTechMaxLevel(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTechLocked(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanLevelup(int TechId, int DeltaLevel = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTechResourceRequirements GetResourceRequirementsByLevel(
      int TechId,
      int Level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTechResourceRequirements GetTechLevelupResourceRequirements(
      int TechId,
      int DeltaLevel = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckTechLevelup(int TechId, int DeltaLevel = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int TechLevelup(int TechId, int DeltaLevel = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddTechLevel(int TechId, int DeltaLevel, bool NoCheckAndCost = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ApplyTrainingTechToHero(Hero hero, TrainingTech tech)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ApplyTrainingTechToHero(
      Hero hero,
      TrainingTech tech,
      IConfigDataLoader config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ApplyTrainingTechToHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ApplyTrainingTechToHero(
      Hero hero,
      TrainingGroundCompomentCommon trainingGround,
      IConfigDataLoader config)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OutPutTeachnologyTreeOperateLog(
      int courseId,
      int techId,
      int currentLvl,
      List<Goods> itemGot,
      List<Goods> itemCost)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TrainingTech> GetAvailableTechs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<TrainingTechInfo> IterateAvailableTechInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<TrainingTech> IterateAvailableTechs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTechLevel(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    public TrainingTech GetTech(int TechId)
    {
      return this.m_trainingGroundDS.GetTech(TechId);
    }

    public TrainingRoom GetRoom(int RoomId)
    {
      return this.m_trainingGroundDS.GetRoom(RoomId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierSkillLevelBySoldierId(int SoldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader m_configDataLoader
    {
      get
      {
        return this._configDataLoader;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TrainingTech> TrainingTechLevelupMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
