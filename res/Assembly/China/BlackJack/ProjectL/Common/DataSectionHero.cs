﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionHero
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionHero : DataSection
  {
    private int m_heroInteractNums;
    private Dictionary<int, int> m_id2CacheIndex;
    private int m_protagonistId;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroFightNums(Hero hero, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero AddHero(int heroId, int starLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ReplaceHero(Hero oldHero, Hero newHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UpdateCache<Hero> FindCache(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int FindHeroIndex(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero FindHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedAddAchievement(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroDirty(Hero hero)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroJob(Hero hero, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob FindHeroJob(Hero hero, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnlockHeroJob(Hero hero, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroJobAchievements(HeroJob heroJob, List<int> achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ActiveHeroJob(Hero hero, int jobRelatedId)
    {
      hero.ActiveHeroJobRelatedId = jobRelatedId;
      this.SetHeroDirty(hero);
    }

    public void SetHeroExp(Hero hero, int exp)
    {
      hero.Exp = exp;
      this.SetHeroDirty(hero);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroLevel(Hero hero, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroStarLevel(Hero hero, int starLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SelectSkills(Hero hero, List<int> skills)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SelectSolider(Hero hero, int soliderId)
    {
      hero.SelectedSoldierId = soliderId;
      this.SetHeroDirty(hero);
    }

    public void AddSkill(Hero hero, int skillId)
    {
      hero.SkillIds.Add(skillId);
    }

    public void AddSoldier(Hero hero, int soldierId)
    {
      hero.SoldierIds.Add(soldierId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LevelUpHeroJobLevel(Hero hero, HeroJob heroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<Hero> GetAllHeroes()
    {
      return this.Heroes.GetAllVaildDatas();
    }

    public void InitProtagonist(int protagonist)
    {
      this.m_protagonistId = protagonist;
    }

    public void SetProtagonist(int protagonistId)
    {
      this.m_protagonistId = protagonistId;
      this.SetDirty(true);
    }

    public void SetHeroFavorabilityLevel(Hero hero, int level)
    {
      hero.FavorabilityLevel = level;
      this.SetHeroDirty(hero);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroFavorabilityLevel(Hero hero, int addLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetHeroFavorabilityExp(Hero hero, int exp)
    {
      hero.FavorabilityExp = exp;
      this.SetHeroDirty(hero);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnlockHeroFetter(Hero hero, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnlockHeroHeartFetter(Hero hero, DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LevelUpHeroHeartFetter(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetHeroHeartFetterLevelMaxTime(Hero hero, DateTime time)
    {
      hero.HeroHeartFetterLevelMaxTime = time;
      this.SetHeroDirty(hero);
    }

    public void ConfessHero(Hero hero)
    {
      hero.Confessed = true;
      this.SetHeroDirty(hero);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LevelUpHeroFetter(Hero hero, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool CanInteractHero()
    {
      return this.m_heroInteractNums > 0;
    }

    public void InitHeroInteractNums(int nums)
    {
      this.m_heroInteractNums = nums;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAllHeroBattlePower(
      int power,
      DateTime updateTime,
      int lastRank,
      DateTime lastUpdateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTopHeroBattlePower(
      int power,
      DateTime updateTime,
      int powerThreshold,
      int lastRank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTopFifteenHeroBattlePower(
      int power,
      DateTime updateTime,
      int powerThreshold,
      int lastRank)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTopFifteenHero(int power, DateTime updateTime)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitChampionHeroBattlePower(
      int power,
      DateTime updateTime,
      int heroId,
      int lastRank)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetHeroInteractNums(int nums)
    {
      this.m_heroInteractNums = nums;
      this.SetDirty(true);
    }

    public int HeroInteractNums
    {
      get
      {
        return this.m_heroInteractNums;
      }
    }

    public void SetHeroInteractNumsFlushTime(DateTime time)
    {
      this.HeroInteractNumsFlushTime = time;
      this.SetDirty(true);
    }

    public void InitHeroInteractNumsFlushTime(DateTime time)
    {
      this.HeroInteractNumsFlushTime = time;
    }

    private static int BattlePowerCompare(Hero x, Hero y)
    {
      return y.BattlePower - x.BattlePower;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnHeroBattlePowerChange(DateTime time, Hero hero = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime HeroInteractNumsFlushTime { get; set; }

    public HashSet<int> GainHeroIds { get; set; }

    public HeroUpdateCache Heroes { get; set; }

    public int ProtagonistID
    {
      get
      {
        return this.m_protagonistId;
      }
    }

    public int AllHeroBattlePower { get; private set; }

    public DateTime AllHeroBattlePowerUpdateTime { get; set; }

    public int LastAllHeroRank { get; set; }

    public int TopHeroBattlePower { get; private set; }

    public DateTime TopHeroBattlePowerUpdateTime { get; set; }

    public int LastTopHeroRank { get; set; }

    public int TopFifteenHeroBattlePower { get; private set; }

    public DateTime TopFifteenHeroBattlePowerUpdateTime { get; set; }

    public int LastTopFifteenHeroRank { get; set; }

    public int ChampionHeroBattlePower { get; private set; }

    public int ChampionHeroId { get; private set; }

    public DateTime ChampionHeroBattlePowerUpdateTime { get; set; }

    public int LastChampionHeroRank { get; set; }

    public DateTime LastHeroRankUpdateTime { get; set; }

    public int TopBattlePowerThreshold { get; private set; }
  }
}
