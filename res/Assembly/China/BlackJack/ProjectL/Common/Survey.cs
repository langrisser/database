﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.Survey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class Survey
  {
    public Survey()
    {
      this.Status = SurveyStatus.Locking;
    }

    public int Id { get; set; }

    public SurveyStatus Status { get; set; }

    public DateTime StartTime { get; set; }

    public DateTime EndTime { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProSurvey ToPBSurvey(Survey survey)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Survey ToSurvey(ProSurvey pbSurvey)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
