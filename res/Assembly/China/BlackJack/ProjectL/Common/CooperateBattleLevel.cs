﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CooperateBattleLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class CooperateBattleLevel
  {
    public DateTime FirstClear;
    public CooperateBattle WhichCooperateBattle;

    [MethodImpl((MethodImplOptions) 32768)]
    public CooperateBattleLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ID { get; set; }

    public string Name
    {
      get
      {
        return this.Config.Name;
      }
    }

    public int PlayerLevelRequirement
    {
      get
      {
        return this.Config.PlayerLevelRequired;
      }
    }

    public int EnergySuccess
    {
      get
      {
        return this.Config.EnergySuccess;
      }
    }

    public int EnergyFail
    {
      get
      {
        return this.Config.EnergyFail;
      }
    }

    public int MonsterLevel
    {
      get
      {
        return this.Config.MonsterLevel;
      }
    }

    public int BattleID
    {
      get
      {
        return this.Config.Battle_ID;
      }
    }

    public int UserExp
    {
      get
      {
        return this.Config.PlayerExp;
      }
    }

    public int HeroExp
    {
      get
      {
        return this.Config.HeroExp;
      }
    }

    public int GoldBonus
    {
      get
      {
        return this.Config.Gold;
      }
    }

    public int RandomDropID
    {
      get
      {
        return this.Config.DropID;
      }
    }

    public int TeamRandomDropID
    {
      get
      {
        return this.Config.TeamDrop_ID;
      }
    }

    public int ItemDropCountDisplay
    {
      get
      {
        return this.Config.ItemDropCountDisplay;
      }
    }

    public int DayBonusDropID
    {
      get
      {
        return this.Config.DayBonusDrop_ID;
      }
    }

    public int DayBonusExtraDropID
    {
      get
      {
        return this.Config.DayBonusExtraDrop_ID;
      }
    }

    public bool Cleared
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAvailableForChallenge
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader { get; set; }

    public ConfigDataCooperateBattleLevelInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
