﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.MailComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class MailComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected List<BagItemBase> m_changedGoods;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    protected DataSectionMail m_mailDS;

    [MethodImpl((MethodImplOptions) 32768)]
    public MailComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "Mail";
    }

    public virtual void Init()
    {
      this.m_changedGoods = new List<BagItemBase>();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mail> GetMails()
    {
      // ISSUE: unable to decompile the method.
    }

    public Mail Find(ulong instanceId)
    {
      return this.m_mailDS.FindMail(instanceId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsMailInitCompleted(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTemplateMailInitCompleted(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual Mail GetRealMail(Mail mail)
    {
      return mail;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual string GetContent(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual Mail InitTemplateMail(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsTemplateMail(Mail mail)
    {
      return mail.TemplateId != 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddMail(Mail newMail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveEarliestMail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMailBoxFull()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveMail(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ReadMail(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMailAttachments(ulong instanceId, bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected List<Mail> GetAllCanGetAttachmentMails()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AutoGetMailAttachment(bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetMailAttachment(ulong missioId, bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnGetMailAttachments(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void GenerateAttachmentRewards(Mail mail)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMailExpired(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan GetMailExpiredTimeSpan(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int RemoveExpiredMails()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsMailReaded(Mail mail)
    {
      return this.m_mailDS.IsMailReaded(mail);
    }

    public bool HasGotMailAttachments(Mail mail)
    {
      return mail.Status == 2;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistMailAttacments(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
