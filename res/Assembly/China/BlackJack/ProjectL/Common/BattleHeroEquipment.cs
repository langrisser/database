﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleHeroEquipment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BattleHeroEquipment
  {
    private int m_id;
    public List<CommonBattleProperty> EnchantProperties;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroEquipment()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Id
    {
      set
      {
        this.m_id = value;
        this.UpdateEquipmentInfo();
      }
      get
      {
        return this.m_id;
      }
    }

    public int Exp { get; set; }

    public int Level { get; set; }

    public int StarLevel { get; set; }

    public int ResonanceId { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleHeroEquipment BattleHeroEquipmentToPBBattleHeroEquipment(
      BattleHeroEquipment equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHeroEquipment PBBattleHeroEquipmentToBattleHeroEquipment(
      ProBattleHeroEquipment pbEquipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateEquipmentInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataEquipmentInfo EquipmentInfo { private set; get; }
  }
}
