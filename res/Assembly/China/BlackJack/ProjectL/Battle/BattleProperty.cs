﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleProperty
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class BattleProperty
  {
    public int HealthPointMax;
    public int Attack;
    public int Defense;
    public int Magic;
    public int MagicDefense;
    public int Dexterity;
    public int Buff_PhysicalDamageMul;
    public int Buff_PhysicalDamageReceiveMul;
    public int Buff_SuperPhysicalDamageReceiveMul;
    public int Buff_HealMul;
    public int Buff_HealReceiveMul;
    public int Buff_MagicalDamageMul;
    public int Buff_MagicalDamageReceiveMul;
    public int Buff_SuperMagicalDamageReceiveMul;
    public int Buff_IgnoreDFMul;
    public int Buff_SkillDamageMul;
    public int Buff_BFSkillDamageMul;
    public int Buff_RangeDamageReceiveMul;
    public int Buff_ReceiveCriticalRateAdd;
    public int Buff_TrueDamageMul;
    public int Buff_ReceiveTrueDamageMul;
    public int Buff_ReceiveBFSkillDamageMul;
    public int Cmd_HealthPoint;
    public int Cmd_Attack;
    public int Cmd_Defense;
    public int Cmd_MagicDefense;
    public int CriticalDamage;
    public int CriticalRate;
    public int AttackDistanceAdd;
    public int MagicSkillDistanceAdd;
    public int PhysicalSkillDistanceAdd;
    public int SupportSkillDistanceAdd;
    public int MagicSkillRangeAdd;
    public int PhysicalSkillRangeAdd;
    public int MovePointAdd;
    [DoNotToLua]
    private BattleProperty.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Clear_hotfix;
    private LuaFunction m_CopyToBattleProperty_hotfix;
    private LuaFunction m_ComputeHeroPropertiesIConfigDataLoaderConfigDataHeroInfoConfigDataJobConnectionInfoInt32Int32Int32BattlePropertyModifier_hotfix;
    private LuaFunction m_ApplyExchangePropertyBattlePropertyModifier_hotfix;
    private LuaFunction m_ApplySummonDataBattleActorSummonData_hotfix;
    private LuaFunction m_ComputeSoldierPropertiesIConfigDataLoaderConfigDataSoldierInfoConfigDataHeroInfoInt32BattlePropertyModifier_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleProperty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CopyTo(BattleProperty p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroProperties(
      IConfigDataLoader configDataLoader,
      ConfigDataHeroInfo heroInfo,
      ConfigDataJobConnectionInfo jobConnectionInfo,
      int jobLevel,
      int heroLevel,
      int heroStar,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    private static int ClampNegativeValue(int value)
    {
      if (value < 0)
        return 0;
      return value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ApplyExchangeProperty(BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ApplySummonData(BattleActorSummonData summonData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeSoldierProperties(
      IConfigDataLoader configDataLoader,
      ConfigDataSoldierInfo soldierInfo,
      ConfigDataHeroInfo heroInfo,
      int heroLevel,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleProperty.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleProperty m_owner;

      public LuaExportHelper(BattleProperty owner)
      {
        this.m_owner = owner;
      }

      public static int ClampNegativeValue(int value)
      {
        return BattleProperty.ClampNegativeValue(value);
      }
    }
  }
}
