﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.DelayHit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;

namespace BlackJack.ProjectL.Battle
{
  public struct DelayHit
  {
    public int m_frame;
    public Vector2i m_position;
    public CombatActor m_targetActor;
  }
}
