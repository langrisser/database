﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BuffState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class BuffState
  {
    public int m_id;
    public int m_displayOrder;
    public ConfigDataBuffInfo m_buffInfo;
    public BuffSourceType m_sourceType;
    public ConfigDataSkillInfo m_sourceSkillInfo;
    public BuffState m_sourceBuffState;
    public int m_time;
    public int m_effectTimes;
    public bool m_isEffective;
    public bool m_hasExtraTime;
    public bool m_checkSelfDie;
    public BattleActor m_applyer;
    public BuffState m_parent;
    [DoNotToLua]
    private BuffState.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorInt32ConfigDataBuffInfoBattleActorBuffSourceTypeConfigDataSkillInfoBuffStateBoolean_hotfix;
    private LuaFunction m_CanChangeTime_hotfix;
    private LuaFunction m_IsApplyerTeamInt32_hotfix;
    private LuaFunction m_GetRootSourceType_hotfix;
    private LuaFunction m_GetRootSourceSkillInfo_hotfix;
    private LuaFunction m_IsChild_hotfix;
    private LuaFunction m_CanNotDispel_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffState(
      int id,
      ConfigDataBuffInfo buffInfo,
      BattleActor applyer,
      BuffSourceType sourceType,
      ConfigDataSkillInfo sourceSkillInfo,
      BuffState sourceBuffState,
      bool checkSelfDie)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanChangeTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsApplyerTeam(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffSourceType GetRootSourceType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetRootSourceSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanNotDispel()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BuffState.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BuffState m_owner;

      public LuaExportHelper(BuffState owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
