﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BehaviorTarget
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class BehaviorTarget
  {
    private GridPosition m_pos;
    private BattleActor m_actor;
    [DoNotToLua]
    private BehaviorTarget.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorBattleActor_hotfix;
    private LuaFunction m_ctorGridPosition_hotfix;
    private LuaFunction m_get_Position_hotfix;
    private LuaFunction m_get_Actor_hotfix;
    private LuaFunction m_IsEmpty_hotfix;
    private LuaFunction m_ToString_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BehaviorTarget(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BehaviorTarget(GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    public GridPosition Position
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleActor Actor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEmpty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BehaviorTarget.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BehaviorTarget m_owner;

      public LuaExportHelper(BehaviorTarget owner)
      {
        this.m_owner = owner;
      }

      public GridPosition m_pos
      {
        get
        {
          return this.m_owner.m_pos;
        }
        set
        {
          this.m_owner.m_pos = value;
        }
      }

      public BattleActor m_actor
      {
        get
        {
          return this.m_owner.m_actor;
        }
        set
        {
          this.m_owner.m_actor = value;
        }
      }
    }
  }
}
