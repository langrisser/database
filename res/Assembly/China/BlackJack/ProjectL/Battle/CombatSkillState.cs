﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatSkillState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class CombatSkillState
  {
    public ConfigDataSkillInfo m_skillInfo;
    public ushort m_hitId;
    public int m_hitCount;
    public int m_preAttackHitCount;
    public List<DelayHit> m_delayHits;
    public bool m_isToHeroCritical;
    public bool m_isToSoldierCritical;

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatSkillState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDelayHit(int frame, Vector2i pos, CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDelayHitFrameMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCritical(bool targetIsHero)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
