﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.AfterCombatApplyBuff
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;

namespace BlackJack.ProjectL.Battle
{
  public class AfterCombatApplyBuff
  {
    public BattleActor m_applyer;
    public BattleActor m_target;
    public ConfigDataBuffInfo m_buffInfo;
    public BuffState m_sourceBuffState;
  }
}
