﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.Entity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class Entity
  {
    private bool m_isDeleteMe;
    private bool m_isPaused;
    protected int m_id;

    public virtual void Dispose()
    {
    }

    public virtual void Tick()
    {
    }

    public virtual void TickGraphic(float dt)
    {
    }

    public virtual void Draw()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DoPause(bool pause)
    {
    }

    public int Id
    {
      get
      {
        return this.m_id;
      }
    }

    public void DeleteMe()
    {
      this.m_isDeleteMe = true;
    }

    public bool IsDeleteMe
    {
      get
      {
        return this.m_isDeleteMe;
      }
    }

    public bool IsPaused
    {
      get
      {
        return this.m_isPaused;
      }
    }
  }
}
