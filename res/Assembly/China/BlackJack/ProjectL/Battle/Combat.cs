﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.Combat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class Combat
  {
    private BattleBase m_battle;
    private RandomNumber m_randomNumber;
    private CombatState m_state;
    private bool m_isPaused;
    private int m_entityIdCounter;
    private ushort m_hitIdCounter;
    private int m_combatGridDistance;
    private int m_frameCount;
    private int m_startCountdown;
    private int m_endCountdown;
    private int m_cutscenePauseCountdown;
    private CombatTeam[] m_teams;

    [MethodImpl((MethodImplOptions) 32768)]
    public Combat(BattleBase battle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(
      BattleActor actor0,
      BattleActor actor1,
      ConfigDataSkillInfo heroSkillInfo0,
      ConfigDataSkillInfo heroSkillInfo1,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Stop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupTeam(
      int teamNumber,
      BattleActor battleActor,
      ConfigDataSkillInfo heroSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    public CombatTeam GetTeam(int teamNumber)
    {
      return this.m_teams[teamNumber];
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextEntityId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetNextHitId()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetCombatGridDistance()
    {
      return this.m_combatGridDistance;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorCastSkill(CombatActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorCastPassiveSkill(CombatActor a, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorDie(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsPlay()
    {
      return this.m_state == CombatState.Play;
    }

    public bool IsPaused()
    {
      return this.m_isPaused;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProbabilitySatisfied(int rate)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetFrameCount()
    {
      return this.m_frameCount;
    }

    public static int FrameToMillisecond(int frame)
    {
      return frame * 1000 / 30;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame1(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleBase Battle
    {
      get
      {
        return this.m_battle;
      }
    }

    public RandomNumber RandomNumber
    {
      get
      {
        return this.m_randomNumber;
      }
    }

    public IBattleListener Listener
    {
      get
      {
        return this.m_battle.Listener;
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      get
      {
        return this.m_battle.ConfigDataLoader;
      }
    }

    public CombatState State
    {
      get
      {
        return this.m_state;
      }
    }
  }
}
