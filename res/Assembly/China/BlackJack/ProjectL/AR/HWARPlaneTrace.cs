﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.AR.HWARPlaneTrace
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using HuaweiARUnitySDK;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.AR
{
  [CustomLuaClass]
  public class HWARPlaneTrace : ARPlaneTrace
  {
    private ARConfigBase m_config;
    private bool m_installRequested;
    private bool m_isSessionCreated;
    private List<ARAnchor> m_addedAnchors;
    private const int m_maxAnchorCount = 16;
    private bool isCachedOrientation;
    private ScreenOrientation orientation;
    private bool autorotateToLandscapeLeft;
    private bool autorotateToLandscapeRight;
    private bool autorotateToPortrait;
    private bool autorotateToPortraitUpsideDown;

    [MethodImpl((MethodImplOptions) 32768)]
    public HWARPlaneTrace()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void AlwaysUpdate()
    {
      AsyncTask.Update();
      ARSession.Update();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool Raycast(Vector3 screenPos, out Vector3 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void OnStart()
    {
      this.HandleStartOrResume();
    }

    public override void OnStop()
    {
      this.HandleStopOrPause();
    }

    public override void OnResume()
    {
      this.HandleStartOrResume();
    }

    public override void OnPause()
    {
      this.HandleStopOrPause();
    }

    private void HandleStartOrResume()
    {
    }

    private void HandleStopOrPause()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Connect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ConnectToService()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
