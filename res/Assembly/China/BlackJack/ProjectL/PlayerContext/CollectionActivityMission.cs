﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CollectionActivityMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class CollectionActivityMission
  {
    public ulong activityUid;
    public int exchangeRid;
    public List<Goods> rewardList;
    public List<CollectionActivityItem> itemList;
    public int curCount;
    public int maxCount;
    public bool canExchange;
    public bool isExchangeMax;
    [DoNotToLua]
    private CollectionActivityMission.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_UpdateState_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int OnSort(CollectionActivityMission x, CollectionActivityMission y)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public CollectionActivityMission.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityMission m_owner;

      public LuaExportHelper(CollectionActivityMission owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
