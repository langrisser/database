﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PeakArenaSeasonInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class PeakArenaSeasonInfo
  {
    public int Season;
    public long Version;
    public long LiveRoomVersion;
    public List<PeakArenaPlayOffMatchupInfo> MatchupInfos;
    public Dictionary<int, PeakArenaLiveRoomInfo> liveRoomInfoDic;

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateMatchupInfoGroupId()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
