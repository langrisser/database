﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PlayerBasicInfoComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class PlayerBasicInfoComponent : PlayerBasicInfoComponentCommon
  {
    private ProjectLPlayerContext m_playerContext;
    public int m_vipLevel;
    [DoNotToLua]
    private PlayerBasicInfoComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_GetCurrentTime_hotfix;
    private LuaFunction m_DeSerializePlayerInfoInitAck_hotfix;
    private LuaFunction m_DeSerializeDSPlayerBasicNtf_hotfix;
    private LuaFunction m_EveryDaySignInt32Int64_hotfix;
    private LuaFunction m_CheckPlayerNameString_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_GetPlayerBasicInfo_hotfix;
    private LuaFunction m_GetEnergyReachMaxTime_hotfix;
    private LuaFunction m_DecreaseEnergyInt32GameFunctionTypeString_hotfix;
    private LuaFunction m_IncreamentEnergyInt64BooleanGameFunctionTypeString_hotfix;
    private LuaFunction m_AddArenaTicketsInt32BooleanGameFunctionTypeString_hotfix;
    private LuaFunction m_OnLevelChangeInt32Int32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerBasicInfoComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override DateTime GetCurrentTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(PlayerInfoInitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSPlayerBasicNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EveryDaySign(int signDays, long lastSignTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int CheckPlayerName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionPlayerBasicInfo GetPlayerBasicInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetEnergyReachMaxTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int DecreaseEnergy(int energyCost, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int IncreamentEnergy(
      long energyAddNums,
      bool canAboveMaxEnergy,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int AddArenaTickets(
      int nums,
      bool arenaGiven = true,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnLevelChange(int upLevel, int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public PlayerBasicInfoComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private int __callBase_BuyJetton(bool check)
    {
      return this.BuyJetton(check);
    }

    private void __callBase_OnBuyJettonGoodsSettlement()
    {
      this.OnBuyJettonGoodsSettlement();
    }

    private int __callBase_CanBuyJetton()
    {
      return this.CanBuyJetton();
    }

    private void __callBase_TryUpdateSignedDays()
    {
      this.TryUpdateSignedDays();
    }

    private bool __callBase_IsGameFunctionOpened(GameFunctionType gameFunctionType)
    {
      return this.IsGameFunctionOpened(gameFunctionType);
    }

    private DateTime __callBase_GetCreateTime()
    {
      return this.GetCreateTime();
    }

    private DateTime __callBase_GetRefluxBeginTime()
    {
      return this.GetRefluxBeginTime();
    }

    private DateTime __callBase_GetCreateTimeUtc()
    {
      return this.GetCreateTimeUtc();
    }

    private DateTime __callBase_GetLastLogoutTime()
    {
      return this.GetLastLogoutTime();
    }

    private DateTime __callBase_GetLoginTime()
    {
      return this.GetLoginTime();
    }

    private string __callBase_GetPlayerName()
    {
      return this.GetPlayerName();
    }

    private string __callBase_GetUserId()
    {
      return this.GetUserId();
    }

    private bool __callBase_IsMe(string userId)
    {
      return this.IsMe(userId);
    }

    private int __callBase_GetCurrentLevelExp()
    {
      return this.GetCurrentLevelExp();
    }

    private int __callBase_GetRechargedCsystal()
    {
      return this.GetRechargedCsystal();
    }

    private long __callBase_GetRechargeRMB()
    {
      return this.GetRechargeRMB();
    }

    private int __callBase_GetHeadIcon()
    {
      return this.GetHeadIcon();
    }

    private int __callBase_GetDefaultHeadIcon()
    {
      return this.GetDefaultHeadIcon();
    }

    private int __callBase_GetHeadPortrait()
    {
      return this.GetHeadPortrait();
    }

    private int __callBase_GetHeadFrame()
    {
      return this.GetHeadFrame();
    }

    private bool __callBase_IsPlayerLevelMax()
    {
      return this.IsPlayerLevelMax();
    }

    private int __callBase_AddPlayerExp(int exp)
    {
      return this.AddPlayerExp(exp);
    }

    private void __callBase_OnLevelChange(int upLevel, int addExp)
    {
      base.OnLevelChange(upLevel, addExp);
    }

    private int __callBase_AddChallengePoint(int nums, GameFunctionType causeId, string location)
    {
      return this.AddChallengePoint(nums, causeId, location);
    }

    private int __callBase_AddFashionPoint(int nums, GameFunctionType causeId, string location)
    {
      return this.AddFashionPoint(nums, causeId, location);
    }

    private int __callBase_AddGold(int nums, GameFunctionType causeId, string location)
    {
      return this.AddGold(nums, causeId, location);
    }

    private bool __callBase_IsGoldEnough(int useGoldCount)
    {
      return this.IsGoldEnough(useGoldCount);
    }

    private bool __callBase_IsGoldOverFlow(int addNums)
    {
      return this.IsGoldOverFlow(addNums);
    }

    private int __callBase_GetGold()
    {
      return this.GetGold();
    }

    private int __callBase_AddBrillianceMithralStone(int nums)
    {
      return this.AddBrillianceMithralStone(nums);
    }

    private int __callBase_AddMithralStone(int nums)
    {
      return this.AddMithralStone(nums);
    }

    private bool __callBase_IsMithralStoneEnough(int nums)
    {
      return this.IsMithralStoneEnough(nums);
    }

    private bool __callBase_IsBrillianceMithralStoneEnough(int nums)
    {
      return this.IsBrillianceMithralStoneEnough(nums);
    }

    private int __callBase_IsCurrencyEnough(GoodsType currencyType, int consumeNums)
    {
      return this.IsCurrencyEnough(currencyType, consumeNums);
    }

    private long __callBase_AddRechargeRMB(int nums, DateTime rechargeTime, bool needSync2Client)
    {
      return this.AddRechargeRMB(nums, rechargeTime, needSync2Client);
    }

    private int __callBase_AddCrystal(int nums, GameFunctionType causeId, string location)
    {
      return this.AddCrystal(nums, causeId, location);
    }

    private bool __callBase_IsCrystalEnough(int consumeNums)
    {
      return this.IsCrystalEnough(consumeNums);
    }

    private void __callBase_InitEnergy(long secondPast)
    {
      this.InitEnergy(secondPast);
    }

    private bool __callBase_IsReachEnergyMax(long currentEnergy)
    {
      return this.IsReachEnergyMax(currentEnergy);
    }

    private void __callBase_FlushEnergy()
    {
      this.FlushEnergy();
    }

    private bool __callBase_CanFlushEnergy()
    {
      return this.CanFlushEnergy();
    }

    private bool __callBase_CanFlushPlayerAction()
    {
      return this.CanFlushPlayerAction();
    }

    private void __callBase_ResetPlayerActionNextFlushTime()
    {
      this.ResetPlayerActionNextFlushTime();
    }

    private int __callBase_IncreamentEnergy(
      long energyAddNums,
      bool canAboveMaxEnergy,
      GameFunctionType causeId,
      string location)
    {
      return base.IncreamentEnergy(energyAddNums, canAboveMaxEnergy, causeId, location);
    }

    private int __callBase_DecreaseEnergy(
      int energyCost,
      GameFunctionType causeId,
      string location)
    {
      return base.DecreaseEnergy(energyCost, causeId, location);
    }

    private bool __callBase_IsEnergyEnough(int consumeEnergy)
    {
      return this.IsEnergyEnough(consumeEnergy);
    }

    private int __callBase_GetEnergy()
    {
      return this.GetEnergy();
    }

    private DateTime __callBase_GetCurrentTime()
    {
      return base.GetCurrentTime();
    }

    private bool __callBase_IsSigned()
    {
      return this.IsSigned();
    }

    private int __callBase_CanSignToday()
    {
      return this.CanSignToday();
    }

    private int __callBase_CanBuyEnergy()
    {
      return this.CanBuyEnergy();
    }

    private int __callBase_BuyEnergy()
    {
      return this.BuyEnergy();
    }

    private int __callBase_CanBuyArenaTickets()
    {
      return this.CanBuyArenaTickets();
    }

    private int __callBase_BuyArenaTickets()
    {
      return this.BuyArenaTickets();
    }

    private DateTime __callBase_GetNextFlushPlayerActionTime()
    {
      return this.GetNextFlushPlayerActionTime();
    }

    private bool __callBase_FlushPlayerAction()
    {
      return this.FlushPlayerAction();
    }

    private void __callBase_OnPlayerActionFlushEvent()
    {
      this.OnPlayerActionFlushEvent();
    }

    private bool __callBase_IsArenaTicketsEnough(int consumeNums)
    {
      return this.IsArenaTicketsEnough(consumeNums);
    }

    private bool __callBase_IsArenaTicketsFull()
    {
      return this.IsArenaTicketsFull();
    }

    private int __callBase_AddArenaTickets(
      int nums,
      bool arenaGiven,
      GameFunctionType causeId,
      string location)
    {
      return base.AddArenaTickets(nums, arenaGiven, causeId, location);
    }

    private int __callBase_GetAreanaTicketNums()
    {
      return this.GetAreanaTicketNums();
    }

    private int __callBase_AddRechargedCrystal(int nums)
    {
      return this.AddRechargedCrystal(nums);
    }

    private int __callBase_AddArenaHonour(int nums, GameFunctionType causeId, string location)
    {
      return this.AddArenaHonour(nums, causeId, location);
    }

    private int __callBase_GetArenaHonour()
    {
      return this.GetArenaHonour();
    }

    private int __callBase_AddRealTimePVPHonor(int nums, GameFunctionType causeId, string location)
    {
      return this.AddRealTimePVPHonor(nums, causeId, location);
    }

    private int __callBase_GetRealTimePVPHonor()
    {
      return this.GetRealTimePVPHonor();
    }

    private int __callBase_GetFriendshipPoints()
    {
      return this.GetFriendshipPoints();
    }

    private int __callBase_GetSkinTickets()
    {
      return this.GetSkinTickets();
    }

    private int __callBase_AddFriendshipPoints(int nums, GameFunctionType causeId, string location)
    {
      return this.AddFriendshipPoints(nums, causeId, location);
    }

    private int __callBase_AddSkinTickets(int nums, GameFunctionType causeId, string location)
    {
      return this.AddSkinTickets(nums, causeId, location);
    }

    private int __callBase_AddMemoryEssence(int nums, GameFunctionType causeId, string location)
    {
      return this.AddMemoryEssence(nums, causeId, location);
    }

    private int __callBase_AddBrillianceMithralStone(
      int nums,
      GameFunctionType causeId,
      string location)
    {
      return this.AddBrillianceMithralStone(nums, causeId, location);
    }

    private int __callBase_AddMithralStone(int nums, GameFunctionType causeId, string location)
    {
      return this.AddMithralStone(nums, causeId, location);
    }

    private int __callBase_AddGuildMedal(int nums, GameFunctionType causeId, string location)
    {
      return this.AddGuildMedal(nums, causeId, location);
    }

    private bool __callBase_IsGuildMedalEnough(int consumeNums)
    {
      return this.IsGuildMedalEnough(consumeNums);
    }

    private bool __callBase_IsFriendshipPointsEnough(int consumeNums)
    {
      return this.IsFriendshipPointsEnough(consumeNums);
    }

    private bool __callBase_IsArenaHonourEnough(int consumeNums)
    {
      return this.IsArenaHonourEnough(consumeNums);
    }

    private bool __callBase_IsRealTimePVPHonorEnough(int consumeNums)
    {
      return this.IsRealTimePVPHonorEnough(consumeNums);
    }

    private bool __callBase_IsSkinTicketEnough(int consumeNums)
    {
      return this.IsSkinTicketEnough(consumeNums);
    }

    private bool __callBase_IsMemoryEssenceEnough(int consumeNums)
    {
      return this.IsMemoryEssenceEnough(consumeNums);
    }

    private bool __callBase_IsChallengePointEnough(int consumeNums)
    {
      return this.IsChallengePointEnough(consumeNums);
    }

    private bool __callBase_IsFashionPointEnough(int consumeNums)
    {
      return this.IsFashionPointEnough(consumeNums);
    }

    private int __callBase_CanSetUserGuide(List<int> completeStepIds)
    {
      return this.CanSetUserGuide(completeStepIds);
    }

    private int __callBase_SetUserGuide(List<int> completeStepIds)
    {
      return this.SetUserGuide(completeStepIds);
    }

    private void __callBase_CleanUserGuide(List<int> completeStepIds)
    {
      this.CleanUserGuide(completeStepIds);
    }

    private void __callBase_CompleteAllUserGuides()
    {
      this.CompleteAllUserGuides();
    }

    private bool __callBase_IsUserGuideCompleted(int stepId)
    {
      return this.IsUserGuideCompleted(stepId);
    }

    private int __callBase_GetLevel()
    {
      return this.GetLevel();
    }

    private int __callBase_GetRechargedCrystal()
    {
      return this.GetRechargedCrystal();
    }

    private int __callBase_GetCrystal()
    {
      return this.GetCrystal();
    }

    private bool __callBase_CheckRankingListAddPlayerLevel()
    {
      return this.CheckRankingListAddPlayerLevel();
    }

    private RankingPlayerInfo __callBase_CreateRankingPlayerInfo(int championHeroId)
    {
      return this.CreateRankingPlayerInfo(championHeroId);
    }

    private void __callBase_OnRankingListPlayerInfoChange()
    {
      this.OnRankingListPlayerInfoChange();
    }

    private void __callBase_OpenGameRating()
    {
      this.OpenGameRating();
    }

    private bool __callBase_IsOpenGameRating()
    {
      return this.IsOpenGameRating();
    }

    private void __callBase_SetMemoryStoreOpenStatus(bool open)
    {
      this.SetMemoryStoreOpenStatus(open);
    }

    private bool __callBase_IsMemoryStoreOpen()
    {
      return this.IsMemoryStoreOpen();
    }

    private int __callBase_CanGainDialogReward(int dialogId)
    {
      return this.CanGainDialogReward(dialogId);
    }

    private int __callBase_GainDialogReward(int dialogId)
    {
      return this.GainDialogReward(dialogId);
    }

    private void __callBase_GenerateDialogReward(int dialogId)
    {
      this.GenerateDialogReward(dialogId);
    }

    private int __callBase_GetLevelUpAddEnergyFromConfig()
    {
      return this.GetLevelUpAddEnergyFromConfig();
    }

    private int __callBase_GetNextLevelExpFromConfig()
    {
      return this.GetNextLevelExpFromConfig();
    }

    private int __callBase_CanSetHeadPortraitAndHeadFrame(int headPortraitId, int headFrameId)
    {
      return this.CanSetHeadPortraitAndHeadFrame(headPortraitId, headFrameId);
    }

    private int __callBase_SetHeadPortraitAndHeadFrame(int headPortraitId, int headFrameId)
    {
      return this.SetHeadPortraitAndHeadFrame(headPortraitId, headFrameId);
    }

    private int __callBase_CanSetHeadPortrait(int headPortraitId)
    {
      return this.CanSetHeadPortrait(headPortraitId);
    }

    private int __callBase_SetHeadPortrait(int headPortraitId)
    {
      return this.SetHeadPortrait(headPortraitId);
    }

    private bool __callBase_HasTitle()
    {
      return this.HasTitle();
    }

    private int __callBase_CanSetHeadFrame(int headFrameId)
    {
      return this.CanSetHeadFrame(headFrameId);
    }

    private int __callBase_SetHeadFrame(int headFrameId)
    {
      return this.SetHeadFrame(headFrameId);
    }

    private void __callBase_OnHeadIconChange()
    {
      this.OnHeadIconChange();
    }

    private int __callBase_CanChangePlayerName(string newName)
    {
      return this.CanChangePlayerName(newName);
    }

    private int __callBase_ChangePlayerName(string newName)
    {
      return this.ChangePlayerName(newName);
    }

    private int __callBase_TryChangePlayerName(string newName)
    {
      return this.TryChangePlayerName(newName);
    }

    private int __callBase_CheckPlayerName(string name)
    {
      return base.CheckPlayerName(name);
    }

    private void __callBase_DoShare()
    {
      this.DoShare();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PlayerBasicInfoComponent m_owner;

      public LuaExportHelper(PlayerBasicInfoComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public int __callBase_BuyJetton(bool check)
      {
        return this.m_owner.__callBase_BuyJetton(check);
      }

      public void __callBase_OnBuyJettonGoodsSettlement()
      {
        this.m_owner.__callBase_OnBuyJettonGoodsSettlement();
      }

      public int __callBase_CanBuyJetton()
      {
        return this.m_owner.__callBase_CanBuyJetton();
      }

      public void __callBase_TryUpdateSignedDays()
      {
        this.m_owner.__callBase_TryUpdateSignedDays();
      }

      public bool __callBase_IsGameFunctionOpened(GameFunctionType gameFunctionType)
      {
        return this.m_owner.__callBase_IsGameFunctionOpened(gameFunctionType);
      }

      public DateTime __callBase_GetCreateTime()
      {
        return this.m_owner.__callBase_GetCreateTime();
      }

      public DateTime __callBase_GetRefluxBeginTime()
      {
        return this.m_owner.__callBase_GetRefluxBeginTime();
      }

      public DateTime __callBase_GetCreateTimeUtc()
      {
        return this.m_owner.__callBase_GetCreateTimeUtc();
      }

      public DateTime __callBase_GetLastLogoutTime()
      {
        return this.m_owner.__callBase_GetLastLogoutTime();
      }

      public DateTime __callBase_GetLoginTime()
      {
        return this.m_owner.__callBase_GetLoginTime();
      }

      public string __callBase_GetPlayerName()
      {
        return this.m_owner.__callBase_GetPlayerName();
      }

      public string __callBase_GetUserId()
      {
        return this.m_owner.__callBase_GetUserId();
      }

      public bool __callBase_IsMe(string userId)
      {
        return this.m_owner.__callBase_IsMe(userId);
      }

      public int __callBase_GetCurrentLevelExp()
      {
        return this.m_owner.__callBase_GetCurrentLevelExp();
      }

      public int __callBase_GetRechargedCsystal()
      {
        return this.m_owner.__callBase_GetRechargedCsystal();
      }

      public long __callBase_GetRechargeRMB()
      {
        return this.m_owner.__callBase_GetRechargeRMB();
      }

      public int __callBase_GetHeadIcon()
      {
        return this.m_owner.__callBase_GetHeadIcon();
      }

      public int __callBase_GetDefaultHeadIcon()
      {
        return this.m_owner.__callBase_GetDefaultHeadIcon();
      }

      public int __callBase_GetHeadPortrait()
      {
        return this.m_owner.__callBase_GetHeadPortrait();
      }

      public int __callBase_GetHeadFrame()
      {
        return this.m_owner.__callBase_GetHeadFrame();
      }

      public bool __callBase_IsPlayerLevelMax()
      {
        return this.m_owner.__callBase_IsPlayerLevelMax();
      }

      public int __callBase_AddPlayerExp(int exp)
      {
        return this.m_owner.__callBase_AddPlayerExp(exp);
      }

      public void __callBase_OnLevelChange(int upLevel, int addExp)
      {
        this.m_owner.__callBase_OnLevelChange(upLevel, addExp);
      }

      public int __callBase_AddChallengePoint(int nums, GameFunctionType causeId, string location)
      {
        return this.m_owner.__callBase_AddChallengePoint(nums, causeId, location);
      }

      public int __callBase_AddFashionPoint(int nums, GameFunctionType causeId, string location)
      {
        return this.m_owner.__callBase_AddFashionPoint(nums, causeId, location);
      }

      public int __callBase_AddGold(int nums, GameFunctionType causeId, string location)
      {
        return this.m_owner.__callBase_AddGold(nums, causeId, location);
      }

      public bool __callBase_IsGoldEnough(int useGoldCount)
      {
        return this.m_owner.__callBase_IsGoldEnough(useGoldCount);
      }

      public bool __callBase_IsGoldOverFlow(int addNums)
      {
        return this.m_owner.__callBase_IsGoldOverFlow(addNums);
      }

      public int __callBase_GetGold()
      {
        return this.m_owner.__callBase_GetGold();
      }

      public int __callBase_AddBrillianceMithralStone(int nums)
      {
        return this.m_owner.__callBase_AddBrillianceMithralStone(nums);
      }

      public int __callBase_AddMithralStone(int nums)
      {
        return this.m_owner.__callBase_AddMithralStone(nums);
      }

      public bool __callBase_IsMithralStoneEnough(int nums)
      {
        return this.m_owner.__callBase_IsMithralStoneEnough(nums);
      }

      public bool __callBase_IsBrillianceMithralStoneEnough(int nums)
      {
        return this.m_owner.__callBase_IsBrillianceMithralStoneEnough(nums);
      }

      public int __callBase_IsCurrencyEnough(GoodsType currencyType, int consumeNums)
      {
        return this.m_owner.__callBase_IsCurrencyEnough(currencyType, consumeNums);
      }

      public long __callBase_AddRechargeRMB(int nums, DateTime rechargeTime, bool needSync2Client)
      {
        return this.m_owner.__callBase_AddRechargeRMB(nums, rechargeTime, needSync2Client);
      }

      public int __callBase_AddCrystal(int nums, GameFunctionType causeId, string location)
      {
        return this.m_owner.__callBase_AddCrystal(nums, causeId, location);
      }

      public bool __callBase_IsCrystalEnough(int consumeNums)
      {
        return this.m_owner.__callBase_IsCrystalEnough(consumeNums);
      }

      public void __callBase_InitEnergy(long secondPast)
      {
        this.m_owner.__callBase_InitEnergy(secondPast);
      }

      public bool __callBase_IsReachEnergyMax(long currentEnergy)
      {
        return this.m_owner.__callBase_IsReachEnergyMax(currentEnergy);
      }

      public void __callBase_FlushEnergy()
      {
        this.m_owner.__callBase_FlushEnergy();
      }

      public bool __callBase_CanFlushEnergy()
      {
        return this.m_owner.__callBase_CanFlushEnergy();
      }

      public bool __callBase_CanFlushPlayerAction()
      {
        return this.m_owner.__callBase_CanFlushPlayerAction();
      }

      public void __callBase_ResetPlayerActionNextFlushTime()
      {
        this.m_owner.__callBase_ResetPlayerActionNextFlushTime();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int __callBase_IncreamentEnergy(
        long energyAddNums,
        bool canAboveMaxEnergy,
        GameFunctionType causeId,
        string location)
      {
        // ISSUE: unable to decompile the method.
      }

      public int __callBase_DecreaseEnergy(
        int energyCost,
        GameFunctionType causeId,
        string location)
      {
        return this.m_owner.__callBase_DecreaseEnergy(energyCost, causeId, location);
      }

      public bool __callBase_IsEnergyEnough(int consumeEnergy)
      {
        return this.m_owner.__callBase_IsEnergyEnough(consumeEnergy);
      }

      public int __callBase_GetEnergy()
      {
        return this.m_owner.__callBase_GetEnergy();
      }

      public DateTime __callBase_GetCurrentTime()
      {
        return this.m_owner.__callBase_GetCurrentTime();
      }

      public bool __callBase_IsSigned()
      {
        return this.m_owner.__callBase_IsSigned();
      }

      public int __callBase_CanSignToday()
      {
        return this.m_owner.__callBase_CanSignToday();
      }

      public int __callBase_CanBuyEnergy()
      {
        return this.m_owner.__callBase_CanBuyEnergy();
      }

      public int __callBase_BuyEnergy()
      {
        return this.m_owner.__callBase_BuyEnergy();
      }

      public int __callBase_CanBuyArenaTickets()
      {
        return this.m_owner.__callBase_CanBuyArenaTickets();
      }

      public int __callBase_BuyArenaTickets()
      {
        return this.m_owner.__callBase_BuyArenaTickets();
      }

      public DateTime __callBase_GetNextFlushPlayerActionTime()
      {
        return this.m_owner.__callBase_GetNextFlushPlayerActionTime();
      }

      public bool __callBase_FlushPlayerAction()
      {
        return this.m_owner.__callBase_FlushPlayerAction();
      }

      public void __callBase_OnPlayerActionFlushEvent()
      {
        this.m_owner.__callBase_OnPlayerActionFlushEvent();
      }

      public bool __callBase_IsArenaTicketsEnough(int consumeNums)
      {
        return this.m_owner.__callBase_IsArenaTicketsEnough(consumeNums);
      }

      public bool __callBase_IsArenaTicketsFull()
      {
        return this.m_owner.__callBase_IsArenaTicketsFull();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int __callBase_AddArenaTickets(
        int nums,
        bool arenaGiven,
        GameFunctionType causeId,
        string location)
      {
        // ISSUE: unable to decompile the method.
      }

      public int __callBase_GetAreanaTicketNums()
      {
        return this.m_owner.__callBase_GetAreanaTicketNums();
      }

      public int __callBase_AddRechargedCrystal(int nums)
      {
        return this.m_owner.__callBase_AddRechargedCrystal(nums);
      }

      public int __callBase_AddArenaHonour(int nums, GameFunctionType causeId, string location)
      {
        return this.m_owner.__callBase_AddArenaHonour(nums, causeId, location);
      }

      public int __callBase_GetArenaHonour()
      {
        return this.m_owner.__callBase_GetArenaHonour();
      }

      public int __callBase_AddRealTimePVPHonor(
        int nums,
        GameFunctionType causeId,
        string location)
      {
        return this.m_owner.__callBase_AddRealTimePVPHonor(nums, causeId, location);
      }

      public int __callBase_GetRealTimePVPHonor()
      {
        return this.m_owner.__callBase_GetRealTimePVPHonor();
      }

      public int __callBase_GetFriendshipPoints()
      {
        return this.m_owner.__callBase_GetFriendshipPoints();
      }

      public int __callBase_GetSkinTickets()
      {
        return this.m_owner.__callBase_GetSkinTickets();
      }

      public int __callBase_AddFriendshipPoints(
        int nums,
        GameFunctionType causeId,
        string location)
      {
        return this.m_owner.__callBase_AddFriendshipPoints(nums, causeId, location);
      }

      public int __callBase_AddSkinTickets(int nums, GameFunctionType causeId, string location)
      {
        return this.m_owner.__callBase_AddSkinTickets(nums, causeId, location);
      }

      public int __callBase_AddMemoryEssence(int nums, GameFunctionType causeId, string location)
      {
        return this.m_owner.__callBase_AddMemoryEssence(nums, causeId, location);
      }

      public int __callBase_AddBrillianceMithralStone(
        int nums,
        GameFunctionType causeId,
        string location)
      {
        return this.m_owner.__callBase_AddBrillianceMithralStone(nums, causeId, location);
      }

      public int __callBase_AddMithralStone(int nums, GameFunctionType causeId, string location)
      {
        return this.m_owner.__callBase_AddMithralStone(nums, causeId, location);
      }

      public int __callBase_AddGuildMedal(int nums, GameFunctionType causeId, string location)
      {
        return this.m_owner.__callBase_AddGuildMedal(nums, causeId, location);
      }

      public bool __callBase_IsGuildMedalEnough(int consumeNums)
      {
        return this.m_owner.__callBase_IsGuildMedalEnough(consumeNums);
      }

      public bool __callBase_IsFriendshipPointsEnough(int consumeNums)
      {
        return this.m_owner.__callBase_IsFriendshipPointsEnough(consumeNums);
      }

      public bool __callBase_IsArenaHonourEnough(int consumeNums)
      {
        return this.m_owner.__callBase_IsArenaHonourEnough(consumeNums);
      }

      public bool __callBase_IsRealTimePVPHonorEnough(int consumeNums)
      {
        return this.m_owner.__callBase_IsRealTimePVPHonorEnough(consumeNums);
      }

      public bool __callBase_IsSkinTicketEnough(int consumeNums)
      {
        return this.m_owner.__callBase_IsSkinTicketEnough(consumeNums);
      }

      public bool __callBase_IsMemoryEssenceEnough(int consumeNums)
      {
        return this.m_owner.__callBase_IsMemoryEssenceEnough(consumeNums);
      }

      public bool __callBase_IsChallengePointEnough(int consumeNums)
      {
        return this.m_owner.__callBase_IsChallengePointEnough(consumeNums);
      }

      public bool __callBase_IsFashionPointEnough(int consumeNums)
      {
        return this.m_owner.__callBase_IsFashionPointEnough(consumeNums);
      }

      public int __callBase_CanSetUserGuide(List<int> completeStepIds)
      {
        return this.m_owner.__callBase_CanSetUserGuide(completeStepIds);
      }

      public int __callBase_SetUserGuide(List<int> completeStepIds)
      {
        return this.m_owner.__callBase_SetUserGuide(completeStepIds);
      }

      public void __callBase_CleanUserGuide(List<int> completeStepIds)
      {
        this.m_owner.__callBase_CleanUserGuide(completeStepIds);
      }

      public void __callBase_CompleteAllUserGuides()
      {
        this.m_owner.__callBase_CompleteAllUserGuides();
      }

      public bool __callBase_IsUserGuideCompleted(int stepId)
      {
        return this.m_owner.__callBase_IsUserGuideCompleted(stepId);
      }

      public int __callBase_GetLevel()
      {
        return this.m_owner.__callBase_GetLevel();
      }

      public int __callBase_GetRechargedCrystal()
      {
        return this.m_owner.__callBase_GetRechargedCrystal();
      }

      public int __callBase_GetCrystal()
      {
        return this.m_owner.__callBase_GetCrystal();
      }

      public bool __callBase_CheckRankingListAddPlayerLevel()
      {
        return this.m_owner.__callBase_CheckRankingListAddPlayerLevel();
      }

      public RankingPlayerInfo __callBase_CreateRankingPlayerInfo(
        int championHeroId)
      {
        return this.m_owner.__callBase_CreateRankingPlayerInfo(championHeroId);
      }

      public void __callBase_OnRankingListPlayerInfoChange()
      {
        this.m_owner.__callBase_OnRankingListPlayerInfoChange();
      }

      public void __callBase_OpenGameRating()
      {
        this.m_owner.__callBase_OpenGameRating();
      }

      public bool __callBase_IsOpenGameRating()
      {
        return this.m_owner.__callBase_IsOpenGameRating();
      }

      public void __callBase_SetMemoryStoreOpenStatus(bool open)
      {
        this.m_owner.__callBase_SetMemoryStoreOpenStatus(open);
      }

      public bool __callBase_IsMemoryStoreOpen()
      {
        return this.m_owner.__callBase_IsMemoryStoreOpen();
      }

      public int __callBase_CanGainDialogReward(int dialogId)
      {
        return this.m_owner.__callBase_CanGainDialogReward(dialogId);
      }

      public int __callBase_GainDialogReward(int dialogId)
      {
        return this.m_owner.__callBase_GainDialogReward(dialogId);
      }

      public void __callBase_GenerateDialogReward(int dialogId)
      {
        this.m_owner.__callBase_GenerateDialogReward(dialogId);
      }

      public int __callBase_GetLevelUpAddEnergyFromConfig()
      {
        return this.m_owner.__callBase_GetLevelUpAddEnergyFromConfig();
      }

      public int __callBase_GetNextLevelExpFromConfig()
      {
        return this.m_owner.__callBase_GetNextLevelExpFromConfig();
      }

      public int __callBase_CanSetHeadPortraitAndHeadFrame(int headPortraitId, int headFrameId)
      {
        return this.m_owner.__callBase_CanSetHeadPortraitAndHeadFrame(headPortraitId, headFrameId);
      }

      public int __callBase_SetHeadPortraitAndHeadFrame(int headPortraitId, int headFrameId)
      {
        return this.m_owner.__callBase_SetHeadPortraitAndHeadFrame(headPortraitId, headFrameId);
      }

      public int __callBase_CanSetHeadPortrait(int headPortraitId)
      {
        return this.m_owner.__callBase_CanSetHeadPortrait(headPortraitId);
      }

      public int __callBase_SetHeadPortrait(int headPortraitId)
      {
        return this.m_owner.__callBase_SetHeadPortrait(headPortraitId);
      }

      public bool __callBase_HasTitle()
      {
        return this.m_owner.__callBase_HasTitle();
      }

      public int __callBase_CanSetHeadFrame(int headFrameId)
      {
        return this.m_owner.__callBase_CanSetHeadFrame(headFrameId);
      }

      public int __callBase_SetHeadFrame(int headFrameId)
      {
        return this.m_owner.__callBase_SetHeadFrame(headFrameId);
      }

      public void __callBase_OnHeadIconChange()
      {
        this.m_owner.__callBase_OnHeadIconChange();
      }

      public int __callBase_CanChangePlayerName(string newName)
      {
        return this.m_owner.__callBase_CanChangePlayerName(newName);
      }

      public int __callBase_ChangePlayerName(string newName)
      {
        return this.m_owner.__callBase_ChangePlayerName(newName);
      }

      public int __callBase_TryChangePlayerName(string newName)
      {
        return this.m_owner.__callBase_TryChangePlayerName(newName);
      }

      public int __callBase_CheckPlayerName(string name)
      {
        return this.m_owner.__callBase_CheckPlayerName(name);
      }

      public void __callBase_DoShare()
      {
        this.m_owner.__callBase_DoShare();
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public void OnLevelChange(int upLevel, int addExp)
      {
        this.m_owner.OnLevelChange(upLevel, addExp);
      }
    }
  }
}
