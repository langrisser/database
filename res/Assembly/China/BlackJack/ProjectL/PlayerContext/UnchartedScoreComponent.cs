﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.UnchartedScoreComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class UnchartedScoreComponent : UnchartedScoreComponentCommon
  {
    [DoNotToLua]
    private UnchartedScoreComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_DeSerializeDSUnchartedScoreNtf_hotfix;
    private LuaFunction m_IsUnchartedScoreOpenedInt32_hotfix;
    private LuaFunction m_GetUnchartedScoreOpenTimeInt32DateTime_DateTime__hotfix;
    private LuaFunction m_IsUnchartedScoreDisplayInt32_hotfix;
    private LuaFunction m_IsUnchartedScoreLevelTimeUnlockInt32Int32_hotfix;
    private LuaFunction m_GetChallengeLevelTimeUnlockDayInt32Int32_hotfix;
    private LuaFunction m_IsScoreLevelCompleteInt32Int32_hotfix;
    private LuaFunction m_IsChallengeLevelCompleteInt32Int32_hotfix;
    private LuaFunction m_IsChallengePrevLevelCompleteInt32Int32_hotfix;
    private LuaFunction m_IsRewardGotInt32Int32_hotfix;
    private LuaFunction m_GetScoreInt32_hotfix;
    private LuaFunction m_GetDailyRewardRestCountInt32_hotfix;
    private LuaFunction m_FinishedChallengeLevelInt32Int32BooleanList`1_hotfix;
    private LuaFunction m_FinishedScoreLevelInt32Int32BooleanList`1_hotfix;
    private LuaFunction m_SetSuccessChallengeLevelInt32ConfigDataChallengeLevelInfoList`1List`1Boolean_hotfix;
    private LuaFunction m_SetSuccessScoreLevelInt32ConfigDataScoreLevelInfoList`1List`1List`1Boolean_hotfix;
    private LuaFunction m_GetMaxFinishedScoreLevelIdInt32_hotfix;
    private LuaFunction m_GetMaxFinishedChallengeLevelIdInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedScoreComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSUnchartedScoreNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedScoreOpened(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetUnchartedScoreOpenTime(
      int unchartedScoreId,
      out DateTime startTime,
      out DateTime endTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedScoreDisplay(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsUnchartedScoreLevelTimeUnlock(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChallengeLevelTimeUnlockDay(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScoreLevelComplete(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelComplete(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengePrevLevelComplete(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRewardGot(int unchartedScoreId, int unchartedScoreRewardGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetScore(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyRewardRestCount(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedChallengeLevel(
      int unchartedScoreId,
      int levelId,
      bool isWin,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedScoreLevel(
      int unchartedScoreId,
      int levelId,
      bool isWin,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessChallengeLevel(
      int unchartedScoreId,
      ConfigDataChallengeLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessScoreLevel(
      int unchartedScoreId,
      ConfigDataScoreLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      List<int> allHeroes,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedScoreLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedChallengeLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public UnchartedScoreComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      this.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private int __callBase_IsUnchartedScoreOnActivityTime(int UnchartedScoreId)
    {
      return this.IsUnchartedScoreOnActivityTime(UnchartedScoreId);
    }

    private List<int> __callBase_GetAllOpenActivityUnchartedScore()
    {
      return this.GetAllOpenActivityUnchartedScore();
    }

    private OperationalActivityBase __callBase_FindOperationalActivityByUnchartedScoreId(
      int UnchartedScoreId)
    {
      return this.FindOperationalActivityByUnchartedScoreId(UnchartedScoreId);
    }

    private bool __callBase_IsChallengeLevelContains(UnchartedScore unchartedScore, int levelId)
    {
      return this.IsChallengeLevelContains(unchartedScore, levelId);
    }

    private bool __callBase_IsChallengeLevelExist(UnchartedScore unchartedScore, int levelId)
    {
      return this.IsChallengeLevelExist(unchartedScore, levelId);
    }

    private bool __callBase_IsChallengeLevelTimeUnlock(
      OperationalActivityBase operationalActivity,
      ConfigDataChallengeLevelInfo levelInfo,
      DateTime curDateTime)
    {
      return this.IsChallengeLevelTimeUnlock(operationalActivity, levelInfo, curDateTime);
    }

    private bool __callBase_IsChallengePrevLevelComplete(
      UnchartedScore unchartedScore,
      ConfigDataChallengeLevelInfo levelInfo)
    {
      return this.IsChallengePrevLevelComplete(unchartedScore, levelInfo);
    }

    private bool __callBase_IsScoreLevelContains(UnchartedScore unchartedScore, int levelId)
    {
      return this.IsScoreLevelContains(unchartedScore, levelId);
    }

    private bool __callBase_IsScoreLevelExist(UnchartedScore unchartedScore, int levelId)
    {
      return this.IsScoreLevelExist(unchartedScore, levelId);
    }

    private bool __callBase_IsScoreLevelTimeUnlock(
      OperationalActivityBase operationalActivity,
      ConfigDataScoreLevelInfo levelInfo,
      DateTime curDateTime)
    {
      return this.IsScoreLevelTimeUnlock(operationalActivity, levelInfo, curDateTime);
    }

    private bool __callBase_IsPlayerLevelVaild(ConfigDataScoreLevelInfo levelInfo)
    {
      return this.IsPlayerLevelVaild(levelInfo);
    }

    private int __callBase_CalcScoreLevelScore(
      UnchartedScore unchartedScore,
      ConfigDataScoreLevelInfo levelInfo,
      List<int> heroIdList,
      bool levelBonus)
    {
      return this.CalcScoreLevelScore(unchartedScore, levelInfo, heroIdList, levelBonus);
    }

    private int __callBase_CalcChallengeLevelScore(
      UnchartedScore unchartedScore,
      ConfigDataChallengeLevelInfo levelInfo,
      List<int> heroIdList)
    {
      return this.CalcChallengeLevelScore(unchartedScore, levelInfo, heroIdList);
    }

    private int __callBase_CalcAllHeroBonus(UnchartedScore unchartedScore, List<int> heroIdList)
    {
      return this.CalcAllHeroBonus(unchartedScore, heroIdList);
    }

    private bool __callBase_IsChallengeLevelUnlock(
      UnchartedScore unchartedScore,
      int levelId,
      DateTime curDateTime)
    {
      return this.IsChallengeLevelUnlock(unchartedScore, levelId, curDateTime);
    }

    private bool __callBase_IsScoreLevelUnlock(
      UnchartedScore unchartedScore,
      int levelId,
      DateTime curDateTime)
    {
      return this.IsScoreLevelUnlock(unchartedScore, levelId, curDateTime);
    }

    private bool __callBase_IsScoreLevelUnlock(int levelId)
    {
      return this.IsScoreLevelUnlock(levelId);
    }

    private List<int> __callBase_GetAllUnlockedScoreLevels()
    {
      return this.GetAllUnlockedScoreLevels();
    }

    private bool __callBase_IsChallengeLevelComplete(UnchartedScore unchartedScore, int levelId)
    {
      return this.IsChallengeLevelComplete(unchartedScore, levelId);
    }

    private bool __callBase_IsScoreLevelComplete(UnchartedScore unchartedScore, int levelId)
    {
      return this.IsScoreLevelComplete(unchartedScore, levelId);
    }

    private int __callBase_IsUnchartedScoreExist(int unchartedScoreId)
    {
      return this.IsUnchartedScoreExist(unchartedScoreId);
    }

    private List<Goods> __callBase_GetReward(int unchartedScoreId, int score)
    {
      return this.GetReward(unchartedScoreId, score);
    }

    private int __callBase_CanRewardGain(int unchartedScoreId, int score)
    {
      return this.CanRewardGain(unchartedScoreId, score);
    }

    private void __callBase_RemoveUnchartedScore(int unchartedScoreId)
    {
      this.RemoveUnchartedScore(unchartedScoreId);
    }

    private int __callBase_CheckChallengeLevelEnergy(ConfigDataChallengeLevelInfo levelInfo)
    {
      return this.CheckChallengeLevelEnergy(levelInfo);
    }

    private int __callBase_CheckScoreLevelEnergy(
      ConfigDataScoreLevelInfo levelInfo,
      bool isTeamBattle)
    {
      return this.CheckScoreLevelEnergy(levelInfo, isTeamBattle);
    }

    private int __callBase_CanAttackChallengeLevel(int unchartedScoreId, int levelId)
    {
      return this.CanAttackChallengeLevel(unchartedScoreId, levelId);
    }

    private int __callBase_AttackChallengeLevel(int unchartedScoreId, int levelId)
    {
      return this.AttackChallengeLevel(unchartedScoreId, levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callBase_SetCommonSuccessChallengeLevel(
      UnchartedScore unchartedScore,
      ConfigDataChallengeLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      int energyCost,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callBase_SetCommonSuccessScoreLevel(
      UnchartedScore unchartedScore,
      ConfigDataScoreLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      List<int> allHeroes,
      int energyCost,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    private int __callBase_CanAttackScoreLevel(
      int unchartedScoreId,
      int levelId,
      bool isTeamBattle)
    {
      return this.CanAttackScoreLevel(unchartedScoreId, levelId, isTeamBattle);
    }

    private int __callBase_AttackScoreLevel(int unchartedScoreId, int levelId)
    {
      return this.AttackScoreLevel(unchartedScoreId, levelId);
    }

    private void __callBase_CheckScoreReward(
      UnchartedScore unchartedScore,
      GameFunctionType causeId)
    {
      this.CheckScoreReward(unchartedScore, causeId);
    }

    private void __callBase_FlushBounsCount()
    {
      this.FlushBounsCount();
    }

    private void __callBase_SetScoreRewardGain(UnchartedScore unchartedScore, int score)
    {
      this.SetScoreRewardGain(unchartedScore, score);
    }

    private UnchartedScore __callBase_GetUnchartedScore(int id)
    {
      return this.GetUnchartedScore(id);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private UnchartedScoreComponent m_owner;

      public LuaExportHelper(UnchartedScoreComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public int __callBase_IsUnchartedScoreOnActivityTime(int UnchartedScoreId)
      {
        return this.m_owner.__callBase_IsUnchartedScoreOnActivityTime(UnchartedScoreId);
      }

      public List<int> __callBase_GetAllOpenActivityUnchartedScore()
      {
        return this.m_owner.__callBase_GetAllOpenActivityUnchartedScore();
      }

      public OperationalActivityBase __callBase_FindOperationalActivityByUnchartedScoreId(
        int UnchartedScoreId)
      {
        return this.m_owner.__callBase_FindOperationalActivityByUnchartedScoreId(UnchartedScoreId);
      }

      public bool __callBase_IsChallengeLevelContains(UnchartedScore unchartedScore, int levelId)
      {
        return this.m_owner.__callBase_IsChallengeLevelContains(unchartedScore, levelId);
      }

      public bool __callBase_IsChallengeLevelExist(UnchartedScore unchartedScore, int levelId)
      {
        return this.m_owner.__callBase_IsChallengeLevelExist(unchartedScore, levelId);
      }

      public bool __callBase_IsChallengeLevelTimeUnlock(
        OperationalActivityBase operationalActivity,
        ConfigDataChallengeLevelInfo levelInfo,
        DateTime curDateTime)
      {
        return this.m_owner.__callBase_IsChallengeLevelTimeUnlock(operationalActivity, levelInfo, curDateTime);
      }

      public bool __callBase_IsChallengePrevLevelComplete(
        UnchartedScore unchartedScore,
        ConfigDataChallengeLevelInfo levelInfo)
      {
        return this.m_owner.__callBase_IsChallengePrevLevelComplete(unchartedScore, levelInfo);
      }

      public bool __callBase_IsScoreLevelContains(UnchartedScore unchartedScore, int levelId)
      {
        return this.m_owner.__callBase_IsScoreLevelContains(unchartedScore, levelId);
      }

      public bool __callBase_IsScoreLevelExist(UnchartedScore unchartedScore, int levelId)
      {
        return this.m_owner.__callBase_IsScoreLevelExist(unchartedScore, levelId);
      }

      public bool __callBase_IsScoreLevelTimeUnlock(
        OperationalActivityBase operationalActivity,
        ConfigDataScoreLevelInfo levelInfo,
        DateTime curDateTime)
      {
        return this.m_owner.__callBase_IsScoreLevelTimeUnlock(operationalActivity, levelInfo, curDateTime);
      }

      public bool __callBase_IsPlayerLevelVaild(ConfigDataScoreLevelInfo levelInfo)
      {
        return this.m_owner.__callBase_IsPlayerLevelVaild(levelInfo);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int __callBase_CalcScoreLevelScore(
        UnchartedScore unchartedScore,
        ConfigDataScoreLevelInfo levelInfo,
        List<int> heroIdList,
        bool levelBonus)
      {
        // ISSUE: unable to decompile the method.
      }

      public int __callBase_CalcChallengeLevelScore(
        UnchartedScore unchartedScore,
        ConfigDataChallengeLevelInfo levelInfo,
        List<int> heroIdList)
      {
        return this.m_owner.__callBase_CalcChallengeLevelScore(unchartedScore, levelInfo, heroIdList);
      }

      public int __callBase_CalcAllHeroBonus(UnchartedScore unchartedScore, List<int> heroIdList)
      {
        return this.m_owner.__callBase_CalcAllHeroBonus(unchartedScore, heroIdList);
      }

      public bool __callBase_IsChallengeLevelUnlock(
        UnchartedScore unchartedScore,
        int levelId,
        DateTime curDateTime)
      {
        return this.m_owner.__callBase_IsChallengeLevelUnlock(unchartedScore, levelId, curDateTime);
      }

      public bool __callBase_IsScoreLevelUnlock(
        UnchartedScore unchartedScore,
        int levelId,
        DateTime curDateTime)
      {
        return this.m_owner.__callBase_IsScoreLevelUnlock(unchartedScore, levelId, curDateTime);
      }

      public bool __callBase_IsScoreLevelUnlock(int levelId)
      {
        return this.m_owner.__callBase_IsScoreLevelUnlock(levelId);
      }

      public List<int> __callBase_GetAllUnlockedScoreLevels()
      {
        return this.m_owner.__callBase_GetAllUnlockedScoreLevels();
      }

      public bool __callBase_IsChallengeLevelComplete(UnchartedScore unchartedScore, int levelId)
      {
        return this.m_owner.__callBase_IsChallengeLevelComplete(unchartedScore, levelId);
      }

      public bool __callBase_IsScoreLevelComplete(UnchartedScore unchartedScore, int levelId)
      {
        return this.m_owner.__callBase_IsScoreLevelComplete(unchartedScore, levelId);
      }

      public int __callBase_IsUnchartedScoreExist(int unchartedScoreId)
      {
        return this.m_owner.__callBase_IsUnchartedScoreExist(unchartedScoreId);
      }

      public List<Goods> __callBase_GetReward(int unchartedScoreId, int score)
      {
        return this.m_owner.__callBase_GetReward(unchartedScoreId, score);
      }

      public int __callBase_CanRewardGain(int unchartedScoreId, int score)
      {
        return this.m_owner.__callBase_CanRewardGain(unchartedScoreId, score);
      }

      public void __callBase_RemoveUnchartedScore(int unchartedScoreId)
      {
        this.m_owner.__callBase_RemoveUnchartedScore(unchartedScoreId);
      }

      public int __callBase_CheckChallengeLevelEnergy(ConfigDataChallengeLevelInfo levelInfo)
      {
        return this.m_owner.__callBase_CheckChallengeLevelEnergy(levelInfo);
      }

      public int __callBase_CheckScoreLevelEnergy(
        ConfigDataScoreLevelInfo levelInfo,
        bool isTeamBattle)
      {
        return this.m_owner.__callBase_CheckScoreLevelEnergy(levelInfo, isTeamBattle);
      }

      public int __callBase_CanAttackChallengeLevel(int unchartedScoreId, int levelId)
      {
        return this.m_owner.__callBase_CanAttackChallengeLevel(unchartedScoreId, levelId);
      }

      public int __callBase_AttackChallengeLevel(int unchartedScoreId, int levelId)
      {
        return this.m_owner.__callBase_AttackChallengeLevel(unchartedScoreId, levelId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_SetCommonSuccessChallengeLevel(
        UnchartedScore unchartedScore,
        ConfigDataChallengeLevelInfo levelInfo,
        List<int> battleTreasures,
        List<int> heroes,
        int energyCost,
        bool isBattleTeam)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_SetCommonSuccessScoreLevel(
        UnchartedScore unchartedScore,
        ConfigDataScoreLevelInfo levelInfo,
        List<int> battleTreasures,
        List<int> heroes,
        List<int> allHeroes,
        int energyCost,
        bool isBattleTeam)
      {
        // ISSUE: unable to decompile the method.
      }

      public int __callBase_CanAttackScoreLevel(
        int unchartedScoreId,
        int levelId,
        bool isTeamBattle)
      {
        return this.m_owner.__callBase_CanAttackScoreLevel(unchartedScoreId, levelId, isTeamBattle);
      }

      public int __callBase_AttackScoreLevel(int unchartedScoreId, int levelId)
      {
        return this.m_owner.__callBase_AttackScoreLevel(unchartedScoreId, levelId);
      }

      public void __callBase_CheckScoreReward(
        UnchartedScore unchartedScore,
        GameFunctionType causeId)
      {
        this.m_owner.__callBase_CheckScoreReward(unchartedScore, causeId);
      }

      public void __callBase_FlushBounsCount()
      {
        this.m_owner.__callBase_FlushBounsCount();
      }

      public void __callBase_SetScoreRewardGain(UnchartedScore unchartedScore, int score)
      {
        this.m_owner.__callBase_SetScoreRewardGain(unchartedScore, score);
      }

      public UnchartedScore __callBase_GetUnchartedScore(int id)
      {
        return this.m_owner.__callBase_GetUnchartedScore(id);
      }
    }
  }
}
