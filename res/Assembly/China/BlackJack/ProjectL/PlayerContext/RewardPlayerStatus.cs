﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.RewardPlayerStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class RewardPlayerStatus
  {
    public int Level;
    public int Exp;
    public int NextLevelExp;
    public int Energy;
    public int ArenaVictoryPoints;
    public int ArenaLevelID;
    public int RealTimePVPScore;
    public int RealTimePVPDan;
    public bool RealTimePVPIsPromotion;
    public RealTimePVPMode RealtTimePVPMode;
    public int PeakArenaScore;
    public int PeakArenaDan;
    public bool PeakArenaIsGrading;
    public bool PeakArenaIsPromotion;
    public RealTimePVPMode PeakArenaMode;
    public int ClimbTowerFinishedFloorId;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
