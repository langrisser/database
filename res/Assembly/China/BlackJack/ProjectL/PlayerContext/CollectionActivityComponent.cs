﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CollectionActivityComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class CollectionActivityComponent : CollectionComponentCommon
  {
    private List<ConfigDataCollectionActivityScenarioLevelInfo> m_tempScenarioLevelInfos;
    protected HeroComponentCommon m_hero;
    protected CollectionActivityExchangeModel m_exchangeModel;
    [DoNotToLua]
    private CollectionActivityComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSCollectionNtf_hotfix;
    private LuaFunction m_GetCollectionActivityList_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_IsCollectionActivityOpenedInt32_hotfix;
    private LuaFunction m_IsCollectionActivityDisplayInt32_hotfix;
    private LuaFunction m_IsCollectionActivityExchangeTimeInt32_hotfix;
    private LuaFunction m_GetCollectionActivityOpenTimeInt32DateTime_DateTime__hotfix;
    private LuaFunction m_GetCollectionActivityExchangeEndTimeInt32_hotfix;
    private LuaFunction m_FindActivityInstanceIdByCollectionActivityIdInt32_hotfix;
    private LuaFunction m_FindCollectionActivityByCollectionActivityIdInt32_hotfix;
    private LuaFunction m_GetNextScenarioLevelIdInt32_hotfix;
    private LuaFunction m_GetCurrentWaypointIdInt32_hotfix;
    private LuaFunction m_GetFinishedScenarioLevelInfosInt32List`1_hotfix;
    private LuaFunction m_GetWaypointStateInt32CollectionActivityWaypointStateType_String__hotfix;
    private LuaFunction m_GetPlayerGraphicResourceInt32_hotfix;
    private LuaFunction m_GetWaypointRedPointCountInt32_hotfix;
    private LuaFunction m_GetLevelUnlockDayInt32Int32_hotfix;
    private LuaFunction m_GetScenarioLevelUnlockDayInt32_hotfix;
    private LuaFunction m_GetChallengeLevelUnlockDayInt32_hotfix;
    private LuaFunction m_GetLootLevelUnlockDayInt32_hotfix;
    private LuaFunction m_IsScenarioLevelFinishedInt32_hotfix;
    private LuaFunction m_IsChallengeLevelFinishedInt32_hotfix;
    private LuaFunction m_IsLootLevelFinishedInt32_hotfix;
    private LuaFunction m_IsChallengePreLevelFinishedInt32_hotfix;
    private LuaFunction m_IsChallengeLevelPlayerLevelVaildInt32Int32__hotfix;
    private LuaFunction m_IsLootPreLevelFinishedInt32_hotfix;
    private LuaFunction m_IsLootLevelPlayerLevelVaildInt32Int32__hotfix;
    private LuaFunction m_FinishedLevelGameFunctionTypeInt32Boolean_hotfix;
    private LuaFunction m_SetSuccessLevelGameFunctionTypeInt32List`1List`1Boolean_hotfix;
    private LuaFunction m_HandleScenarioInt32_hotfix;
    private LuaFunction m_GetLevelNameCollectionActivityLevelTypeInt32_hotfix;
    private LuaFunction m_GetPageLockMessageCollectionActivityPage_hotfix;
    private LuaFunction m_IsLevelFinishedLevelInfo_hotfix;
    private LuaFunction m_GetScoreInt32_hotfix;
    private LuaFunction m_IsRewardGotInt32Int32_hotfix;
    private LuaFunction m_GetMaxFinishedLootLevelIdInt32_hotfix;
    private LuaFunction m_get_ExchangeModel_hotfix;
    private LuaFunction m_FinishEventUInt64Int32Int32BooleanList`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSCollectionNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionActivity> GetCollectionActivityList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityOpened(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityDisplay(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityExchangeTime(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetCollectionActivityOpenTime(
      int collectionActivityId,
      out DateTime startTime,
      out DateTime endTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCollectionActivityExchangeEndTime(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong FindActivityInstanceIdByCollectionActivityId(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivity FindCollectionActivityByCollectionActivityId(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextScenarioLevelId(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentWaypointId(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetFinishedScenarioLevelInfos(
      int collectionActivityId,
      List<ConfigDataCollectionActivityScenarioLevelInfo> scenarioLevelInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetWaypointState(
      int waypointId,
      out CollectionActivityWaypointStateType waypointLockState,
      out string waypointGraphicState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPlayerGraphicResource(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetWaypointRedPointCount(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetLevelUnlockDay(int activityId, int daysBeforeActivate)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetScenarioLevelUnlockDay(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChallengeLevelUnlockDay(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLootLevelUnlockDay(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengePreLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelPlayerLevelVaild(int levelId, out int playerLevelRequired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootPreLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootLevelPlayerLevelVaild(int levelId, out int playerLevelRequired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedLevel(GameFunctionType levelType, int levelId, bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessLevel(
      GameFunctionType levelType,
      int levelId,
      List<int> heroes,
      List<int> allHeroes,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HandleScenario(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetLevelName(CollectionActivityLevelType type, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPageLockMessage(CollectionActivityPage page)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelFinished(LevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetScore(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRewardGot(int collectionActivityId, int collectionActvityScoreRewardGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedLootLevelId(int collectionWaypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public CollectionActivityExchangeModel ExchangeModel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishEvent(
      ulong activityId,
      int wayPointId,
      int EventId,
      bool isWin,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public CollectionActivityComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private int __callBase_HandleTresureEvent(int wayPointId, ConfigDataEventInfo eventInfo)
    {
      return this.HandleTresureEvent(wayPointId, eventInfo);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int __callBase_HandleDialogEvent(
      ConfigDataCollectionActivityWaypointInfo wayPointInfo,
      List<Goods> itemRewards,
      int expReward,
      int goldReward,
      int energyCost,
      int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    private List<Goods> __callBase_GetEventRewards(ConfigDataEventInfo eventInfo)
    {
      return this.GetEventRewards(eventInfo);
    }

    private void __callBase_RemoveEvent(List<CollectionEvent> Events, ulong activityId)
    {
      this.RemoveEvent(Events, activityId);
    }

    private bool __callBase_IsEventTimeOut(CollectionEvent Event)
    {
      return this.IsEventTimeOut(Event);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private IEnumerable<ConfigDataCollectionActivityExchangeTableInfo> __callBase_IterateAvailableExchangeItems()
    {
      return this.IterateAvailableExchangeItems();
    }

    private int __callBase_CanExchangeItem(ulong activityInstanceId, int exchangeItemID)
    {
      return this.CanExchangeItem(activityInstanceId, exchangeItemID);
    }

    private int __callBase_ExchangeItem(ulong activityInstanceId, int exchangeItemID)
    {
      return this.ExchangeItem(activityInstanceId, exchangeItemID);
    }

    private ConfigDataCollectionActivityInfo __callBase_GetCollectionActivityInfo(
      ulong activityInstanceId)
    {
      return this.GetCollectionActivityInfo(activityInstanceId);
    }

    private ulong __callBase_GetActivityInstanceId(GameFunctionType typeId, int levelId)
    {
      return this.GetActivityInstanceId(typeId, levelId);
    }

    private ConfigDataCollectionActivityInfo __callBase_GetCollectionActivityInfo(
      GameFunctionType typeId,
      int levelId)
    {
      return this.GetCollectionActivityInfo(typeId, levelId);
    }

    private long __callBase_GetEventExpiredTime(int EventinfoId)
    {
      return this.GetEventExpiredTime(EventinfoId);
    }

    private CollectionActivity __callBase_GetCollectionActivity(
      GameFunctionType typeId,
      int levelId)
    {
      return this.GetCollectionActivity(typeId, levelId);
    }

    private int __callBase_CanMoveToWayPoint(int waypointId)
    {
      return this.CanMoveToWayPoint(waypointId);
    }

    private int __callBase_MoveToWayPoint(int waypointId)
    {
      return this.MoveToWayPoint(waypointId);
    }

    private int __callBase_CanHandleScenario(int levelId)
    {
      return this.CanHandleScenario(levelId);
    }

    private int __callBase_IsNextScenarioId(ulong activityInstanceId, int levelId)
    {
      return this.IsNextScenarioId(activityInstanceId, levelId);
    }

    private int __callBase_GetNextScenarioId(ulong activityInstanceId)
    {
      return this.GetNextScenarioId(activityInstanceId);
    }

    private int __callBase_CanAttackLevel(GameFunctionType typeId, int levelId, bool team)
    {
      return this.CanAttackLevel(typeId, levelId, team);
    }

    private bool __callBase_IsPrevLevelUnlock(
      CollectionActivity activity,
      List<LevelInfo> PrevLevels)
    {
      return this.IsPrevLevelUnlock(activity, PrevLevels);
    }

    private bool __callBase_IsScenarioFinished(
      CollectionActivity collectionActivity,
      int scenarioId)
    {
      return this.IsScenarioFinished(collectionActivity, scenarioId);
    }

    private bool __callBase_IsChallengeLevelFinished(
      CollectionActivity collectionActivity,
      int levelId)
    {
      return this.IsChallengeLevelFinished(collectionActivity, levelId);
    }

    private int __callBase_AttackLevel(GameFunctionType typeId, int levelId)
    {
      return this.AttackLevel(typeId, levelId);
    }

    private List<int> __callBase_GetAllUnlockedLootLevels()
    {
      return this.GetAllUnlockedLootLevels();
    }

    private bool __callBase_IsLootLevelUnlock(int levelId)
    {
      return this.IsLootLevelUnlock(levelId);
    }

    private void __callBase_AddFinishedLootLevel(CollectionActivity collectionActivity, int levelId)
    {
      this.AddFinishedLootLevel(collectionActivity, levelId);
    }

    private List<CollectionEvent> __callBase_GetAllEventsByInstanceId(
      ulong ActivityInstanceId)
    {
      return this.GetAllEventsByInstanceId(ActivityInstanceId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callBase_SetCommonSuccessLevel(
      CollectionActivity collectionActivity,
      GameFunctionType typeId,
      int levelId,
      List<int> heroes,
      List<int> allHeroes,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_SendAddEvent(List<CollectionEvent> events, ulong activityInstanceId)
    {
      this.SendAddEvent(events, activityInstanceId);
    }

    private void __callBase_SendDeleteEvent(List<CollectionEvent> events, ulong activityInstanceId)
    {
      this.SendDeleteEvent(events, activityInstanceId);
    }

    private int __callBase_CollectionEventHandle(ulong activityId, int EventId, int wayPointId)
    {
      return this.CollectionEventHandle(activityId, EventId, wayPointId);
    }

    private int __callBase_CanHandleCollectionEvent(ulong activityId, int wayPointId, int EventId)
    {
      return this.CanHandleCollectionEvent(activityId, wayPointId, EventId);
    }

    private int __callBase_HandleCollectionEvent(
      ulong activityId,
      int EventId,
      ConfigDataEventInfo eventInfo,
      ConfigDataCollectionActivityWaypointInfo wayPointInfo)
    {
      return this.HandleCollectionEvent(activityId, EventId, eventInfo, wayPointInfo);
    }

    private int __callBase_AttackEvent(ulong activityId, int wayPointId, int EventId)
    {
      return this.AttackEvent(activityId, wayPointId, EventId);
    }

    private int __callBase_CanAttackEvent(ulong activityId, int wayPointId, int EventId)
    {
      return this.CanAttackEvent(activityId, wayPointId, EventId);
    }

    private int __callBase_CanAttackCollectionEvent(ConfigDataEventInfo eventInfo)
    {
      return this.CanAttackCollectionEvent(eventInfo);
    }

    private void __callBase_AddCollectionEvent(List<CollectionEvent> events, ulong ActivityId)
    {
      this.AddCollectionEvent(events, ActivityId);
    }

    private void __callBase_DeleteCollectionEvent(List<CollectionEvent> events, ulong ActivityId)
    {
      this.DeleteCollectionEvent(events, ActivityId);
    }

    private void __callBase_OnEventComplete(
      ulong activityId,
      int EventId,
      int wayPointId,
      ConfigDataEventInfo eventInfo)
    {
      this.OnEventComplete(activityId, EventId, wayPointId, eventInfo);
    }

    private void __callBase_SetBattleEventSuccessful(
      ConfigDataCollectionActivityWaypointInfo wayPointInfo,
      List<int> battleTreasures)
    {
      this.SetBattleEventSuccessful(wayPointInfo, battleTreasures);
    }

    private void __callBase_SortByExpiredTimeAscend(List<CollectionEvent> events)
    {
      this.SortByExpiredTimeAscend(events);
    }

    private List<CollectionEvent> __callBase_TryDeleteCompleteTime(
      CollectionActivity collectionActivity,
      int EventId,
      int wayPointId)
    {
      return this.TryDeleteCompleteTime(collectionActivity, EventId, wayPointId);
    }

    private List<CollectionEvent> __callBase_TryHandleCollectionEvent(
      CollectionActivity collectionActivity,
      int levelId)
    {
      return this.TryHandleCollectionEvent(collectionActivity, levelId);
    }

    private List<CollectionEvent> __callBase_TryDeleteCollectionEventChallengeLevel(
      CollectionActivity collectionActivity,
      int levelId)
    {
      return this.TryDeleteCollectionEventChallengeLevel(collectionActivity, levelId);
    }

    private List<CollectionEvent> __callBase_TryProduceCollectionEvent(
      CollectionActivity collectionActivity,
      int levelId)
    {
      return this.TryProduceCollectionEvent(collectionActivity, levelId);
    }

    private CollectionActivityWaypointStateType __callBase_GetCollectionActivityWayPointState(
      CollectionActivity collectionActivity,
      int waypointId)
    {
      return this.GetCollectionActivityWayPointState(collectionActivity, waypointId);
    }

    private bool __callBase_IsNewCollectionActivityOpened(int collectionActivityId)
    {
      return this.IsNewCollectionActivityOpened(collectionActivityId);
    }

    private List<ConfigDataCollectionActivityScenarioLevelInfo> __callBase_GetNewFinishedScenarioLevelInfos(
      CollectionActivity collectionActivity)
    {
      return this.GetNewFinishedScenarioLevelInfos(collectionActivity);
    }

    private List<CollectionEvent> __callBase_TryDeleteCollectionEventScenario(
      CollectionActivity collectionActivity,
      int levelId)
    {
      return this.TryDeleteCollectionEventScenario(collectionActivity, levelId);
    }

    private void __callBase_AddScore(
      CollectionActivity collectionActivity,
      int scoreBase,
      List<int> allHeroes,
      GameFunctionType causeId)
    {
      this.AddScore(collectionActivity, scoreBase, allHeroes, causeId);
    }

    private int __callBase_CalculateScore(
      ConfigDataCollectionActivityInfo info,
      int scoreBase,
      List<int> allHeroes)
    {
      return this.CalculateScore(info, scoreBase, allHeroes);
    }

    private int __callBase_CalculateAllHeroBonus(
      ConfigDataCollectionActivityInfo info,
      List<int> allHeroes)
    {
      return this.CalculateAllHeroBonus(info, allHeroes);
    }

    private CollectionActivity __callBase_FindCollectionActivity(ulong instanceId)
    {
      return this.FindCollectionActivity(instanceId);
    }

    private void __callBase_RemoveCollectionActivity(OperationalActivityBase operationalActivity)
    {
      this.RemoveCollectionActivity(operationalActivity);
    }

    private void __callBase_AddCollectionActivity(OperationalActivityBase operationalActivity)
    {
      this.AddCollectionActivity(operationalActivity);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityComponent m_owner;

      public LuaExportHelper(CollectionActivityComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public int __callBase_HandleTresureEvent(int wayPointId, ConfigDataEventInfo eventInfo)
      {
        return this.m_owner.__callBase_HandleTresureEvent(wayPointId, eventInfo);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int __callBase_HandleDialogEvent(
        ConfigDataCollectionActivityWaypointInfo wayPointInfo,
        List<Goods> itemRewards,
        int expReward,
        int goldReward,
        int energyCost,
        int scenarioId)
      {
        // ISSUE: unable to decompile the method.
      }

      public List<Goods> __callBase_GetEventRewards(ConfigDataEventInfo eventInfo)
      {
        return this.m_owner.__callBase_GetEventRewards(eventInfo);
      }

      public void __callBase_RemoveEvent(List<CollectionEvent> Events, ulong activityId)
      {
        this.m_owner.__callBase_RemoveEvent(Events, activityId);
      }

      public bool __callBase_IsEventTimeOut(CollectionEvent Event)
      {
        return this.m_owner.__callBase_IsEventTimeOut(Event);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public IEnumerable<ConfigDataCollectionActivityExchangeTableInfo> __callBase_IterateAvailableExchangeItems()
      {
        return this.m_owner.__callBase_IterateAvailableExchangeItems();
      }

      public int __callBase_CanExchangeItem(ulong activityInstanceId, int exchangeItemID)
      {
        return this.m_owner.__callBase_CanExchangeItem(activityInstanceId, exchangeItemID);
      }

      public int __callBase_ExchangeItem(ulong activityInstanceId, int exchangeItemID)
      {
        return this.m_owner.__callBase_ExchangeItem(activityInstanceId, exchangeItemID);
      }

      public ConfigDataCollectionActivityInfo __callBase_GetCollectionActivityInfo(
        ulong activityInstanceId)
      {
        return this.m_owner.__callBase_GetCollectionActivityInfo(activityInstanceId);
      }

      public ulong __callBase_GetActivityInstanceId(GameFunctionType typeId, int levelId)
      {
        return this.m_owner.__callBase_GetActivityInstanceId(typeId, levelId);
      }

      public ConfigDataCollectionActivityInfo __callBase_GetCollectionActivityInfo(
        GameFunctionType typeId,
        int levelId)
      {
        return this.m_owner.__callBase_GetCollectionActivityInfo(typeId, levelId);
      }

      public long __callBase_GetEventExpiredTime(int EventinfoId)
      {
        return this.m_owner.__callBase_GetEventExpiredTime(EventinfoId);
      }

      public CollectionActivity __callBase_GetCollectionActivity(
        GameFunctionType typeId,
        int levelId)
      {
        return this.m_owner.__callBase_GetCollectionActivity(typeId, levelId);
      }

      public int __callBase_CanMoveToWayPoint(int waypointId)
      {
        return this.m_owner.__callBase_CanMoveToWayPoint(waypointId);
      }

      public int __callBase_MoveToWayPoint(int waypointId)
      {
        return this.m_owner.__callBase_MoveToWayPoint(waypointId);
      }

      public int __callBase_CanHandleScenario(int levelId)
      {
        return this.m_owner.__callBase_CanHandleScenario(levelId);
      }

      public int __callBase_IsNextScenarioId(ulong activityInstanceId, int levelId)
      {
        return this.m_owner.__callBase_IsNextScenarioId(activityInstanceId, levelId);
      }

      public int __callBase_GetNextScenarioId(ulong activityInstanceId)
      {
        return this.m_owner.__callBase_GetNextScenarioId(activityInstanceId);
      }

      public int __callBase_CanAttackLevel(GameFunctionType typeId, int levelId, bool team)
      {
        return this.m_owner.__callBase_CanAttackLevel(typeId, levelId, team);
      }

      public bool __callBase_IsPrevLevelUnlock(
        CollectionActivity activity,
        List<LevelInfo> PrevLevels)
      {
        return this.m_owner.__callBase_IsPrevLevelUnlock(activity, PrevLevels);
      }

      public bool __callBase_IsScenarioFinished(
        CollectionActivity collectionActivity,
        int scenarioId)
      {
        return this.m_owner.__callBase_IsScenarioFinished(collectionActivity, scenarioId);
      }

      public bool __callBase_IsChallengeLevelFinished(
        CollectionActivity collectionActivity,
        int levelId)
      {
        return this.m_owner.__callBase_IsChallengeLevelFinished(collectionActivity, levelId);
      }

      public int __callBase_AttackLevel(GameFunctionType typeId, int levelId)
      {
        return this.m_owner.__callBase_AttackLevel(typeId, levelId);
      }

      public List<int> __callBase_GetAllUnlockedLootLevels()
      {
        return this.m_owner.__callBase_GetAllUnlockedLootLevels();
      }

      public bool __callBase_IsLootLevelUnlock(int levelId)
      {
        return this.m_owner.__callBase_IsLootLevelUnlock(levelId);
      }

      public void __callBase_AddFinishedLootLevel(
        CollectionActivity collectionActivity,
        int levelId)
      {
        this.m_owner.__callBase_AddFinishedLootLevel(collectionActivity, levelId);
      }

      public List<CollectionEvent> __callBase_GetAllEventsByInstanceId(
        ulong ActivityInstanceId)
      {
        return this.m_owner.__callBase_GetAllEventsByInstanceId(ActivityInstanceId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_SetCommonSuccessLevel(
        CollectionActivity collectionActivity,
        GameFunctionType typeId,
        int levelId,
        List<int> heroes,
        List<int> allHeroes,
        bool isBattleTeam)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_SendAddEvent(List<CollectionEvent> events, ulong activityInstanceId)
      {
        this.m_owner.__callBase_SendAddEvent(events, activityInstanceId);
      }

      public void __callBase_SendDeleteEvent(List<CollectionEvent> events, ulong activityInstanceId)
      {
        this.m_owner.__callBase_SendDeleteEvent(events, activityInstanceId);
      }

      public int __callBase_CollectionEventHandle(ulong activityId, int EventId, int wayPointId)
      {
        return this.m_owner.__callBase_CollectionEventHandle(activityId, EventId, wayPointId);
      }

      public int __callBase_CanHandleCollectionEvent(ulong activityId, int wayPointId, int EventId)
      {
        return this.m_owner.__callBase_CanHandleCollectionEvent(activityId, wayPointId, EventId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public int __callBase_HandleCollectionEvent(
        ulong activityId,
        int EventId,
        ConfigDataEventInfo eventInfo,
        ConfigDataCollectionActivityWaypointInfo wayPointInfo)
      {
        // ISSUE: unable to decompile the method.
      }

      public int __callBase_AttackEvent(ulong activityId, int wayPointId, int EventId)
      {
        return this.m_owner.__callBase_AttackEvent(activityId, wayPointId, EventId);
      }

      public int __callBase_CanAttackEvent(ulong activityId, int wayPointId, int EventId)
      {
        return this.m_owner.__callBase_CanAttackEvent(activityId, wayPointId, EventId);
      }

      public int __callBase_CanAttackCollectionEvent(ConfigDataEventInfo eventInfo)
      {
        return this.m_owner.__callBase_CanAttackCollectionEvent(eventInfo);
      }

      public void __callBase_AddCollectionEvent(List<CollectionEvent> events, ulong ActivityId)
      {
        this.m_owner.__callBase_AddCollectionEvent(events, ActivityId);
      }

      public void __callBase_DeleteCollectionEvent(List<CollectionEvent> events, ulong ActivityId)
      {
        this.m_owner.__callBase_DeleteCollectionEvent(events, ActivityId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_OnEventComplete(
        ulong activityId,
        int EventId,
        int wayPointId,
        ConfigDataEventInfo eventInfo)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_SetBattleEventSuccessful(
        ConfigDataCollectionActivityWaypointInfo wayPointInfo,
        List<int> battleTreasures)
      {
        this.m_owner.__callBase_SetBattleEventSuccessful(wayPointInfo, battleTreasures);
      }

      public void __callBase_SortByExpiredTimeAscend(List<CollectionEvent> events)
      {
        this.m_owner.__callBase_SortByExpiredTimeAscend(events);
      }

      public List<CollectionEvent> __callBase_TryDeleteCompleteTime(
        CollectionActivity collectionActivity,
        int EventId,
        int wayPointId)
      {
        return this.m_owner.__callBase_TryDeleteCompleteTime(collectionActivity, EventId, wayPointId);
      }

      public List<CollectionEvent> __callBase_TryHandleCollectionEvent(
        CollectionActivity collectionActivity,
        int levelId)
      {
        return this.m_owner.__callBase_TryHandleCollectionEvent(collectionActivity, levelId);
      }

      public List<CollectionEvent> __callBase_TryDeleteCollectionEventChallengeLevel(
        CollectionActivity collectionActivity,
        int levelId)
      {
        return this.m_owner.__callBase_TryDeleteCollectionEventChallengeLevel(collectionActivity, levelId);
      }

      public List<CollectionEvent> __callBase_TryProduceCollectionEvent(
        CollectionActivity collectionActivity,
        int levelId)
      {
        return this.m_owner.__callBase_TryProduceCollectionEvent(collectionActivity, levelId);
      }

      public CollectionActivityWaypointStateType __callBase_GetCollectionActivityWayPointState(
        CollectionActivity collectionActivity,
        int waypointId)
      {
        return this.m_owner.__callBase_GetCollectionActivityWayPointState(collectionActivity, waypointId);
      }

      public bool __callBase_IsNewCollectionActivityOpened(int collectionActivityId)
      {
        return this.m_owner.__callBase_IsNewCollectionActivityOpened(collectionActivityId);
      }

      public List<ConfigDataCollectionActivityScenarioLevelInfo> __callBase_GetNewFinishedScenarioLevelInfos(
        CollectionActivity collectionActivity)
      {
        return this.m_owner.__callBase_GetNewFinishedScenarioLevelInfos(collectionActivity);
      }

      public List<CollectionEvent> __callBase_TryDeleteCollectionEventScenario(
        CollectionActivity collectionActivity,
        int levelId)
      {
        return this.m_owner.__callBase_TryDeleteCollectionEventScenario(collectionActivity, levelId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_AddScore(
        CollectionActivity collectionActivity,
        int scoreBase,
        List<int> allHeroes,
        GameFunctionType causeId)
      {
        // ISSUE: unable to decompile the method.
      }

      public int __callBase_CalculateScore(
        ConfigDataCollectionActivityInfo info,
        int scoreBase,
        List<int> allHeroes)
      {
        return this.m_owner.__callBase_CalculateScore(info, scoreBase, allHeroes);
      }

      public int __callBase_CalculateAllHeroBonus(
        ConfigDataCollectionActivityInfo info,
        List<int> allHeroes)
      {
        return this.m_owner.__callBase_CalculateAllHeroBonus(info, allHeroes);
      }

      public CollectionActivity __callBase_FindCollectionActivity(ulong instanceId)
      {
        return this.m_owner.__callBase_FindCollectionActivity(instanceId);
      }

      public void __callBase_RemoveCollectionActivity(OperationalActivityBase operationalActivity)
      {
        this.m_owner.__callBase_RemoveCollectionActivity(operationalActivity);
      }

      public void __callBase_AddCollectionActivity(OperationalActivityBase operationalActivity)
      {
        this.m_owner.__callBase_AddCollectionActivity(operationalActivity);
      }

      public List<ConfigDataCollectionActivityScenarioLevelInfo> m_tempScenarioLevelInfos
      {
        get
        {
          return this.m_owner.m_tempScenarioLevelInfos;
        }
        set
        {
          this.m_owner.m_tempScenarioLevelInfos = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public CollectionActivityExchangeModel m_exchangeModel
      {
        get
        {
          return this.m_owner.m_exchangeModel;
        }
        set
        {
          this.m_owner.m_exchangeModel = value;
        }
      }

      public CollectionActivity FindCollectionActivityByCollectionActivityId(
        int collectionActivityId)
      {
        return this.m_owner.FindCollectionActivityByCollectionActivityId(collectionActivityId);
      }

      public void GetFinishedScenarioLevelInfos(
        int collectionActivityId,
        List<ConfigDataCollectionActivityScenarioLevelInfo> scenarioLevelInfos)
      {
        this.m_owner.GetFinishedScenarioLevelInfos(collectionActivityId, scenarioLevelInfos);
      }

      public int GetLevelUnlockDay(int activityId, int daysBeforeActivate)
      {
        return this.m_owner.GetLevelUnlockDay(activityId, daysBeforeActivate);
      }
    }
  }
}
