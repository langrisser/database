﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.HeroTrainningComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class HeroTrainningComponent : HeroTrainningComponentCommon
  {
    [DoNotToLua]
    private HeroTrainningComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_DeSerializeDSHeroTrainningNtf_hotfix;
    private LuaFunction m_GetCurrentTicketNums_hotfix;
    private LuaFunction m_FinishedHeroTrainningLevelInt32BooleanList`1_hotfix;
    private LuaFunction m_SetSuccessHeroTrainningLevelConfigDataHeroTrainningLevelInfoList`1List`1Boolean_hotfix;
    private LuaFunction m_IsHeroTrainningOpenedInt32_hotfix;
    private LuaFunction m_IsHeroTrainningLevelOpenedInt32_hotfix;
    private LuaFunction m_GetMaxFinishedLevelIdInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroTrainningComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSHeroTrainningNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentTicketNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedHeroTrainningLevel(int levelId, bool isWin, List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessHeroTrainningLevel(
      ConfigDataHeroTrainningLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroTrainningOpened(int heroTrainningId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroTrainningLevelOpened(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public HeroTrainningComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private int __callBase_AttackHeroTrainningLevel(int levelId)
    {
      return this.AttackHeroTrainningLevel(levelId);
    }

    private int __callBase_CanAttackHeroTrainningLevel(int levelId, bool isTeamBattle)
    {
      return this.CanAttackHeroTrainningLevel(levelId, isTeamBattle);
    }

    private bool __callBase_IsLevelFinished(int levelId)
    {
      return this.IsLevelFinished(levelId);
    }

    private void __callBase_SetCommonSuccessHeroTrainningLevel(
      ConfigDataHeroTrainningLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      int energyCost,
      bool isBattleTeam)
    {
      this.SetCommonSuccessHeroTrainningLevel(levelInfo, battleTreasures, heroes, energyCost, isBattleTeam);
    }

    private HashSet<int> __callBase_GetAllFinishedLevels()
    {
      return this.GetAllFinishedLevels();
    }

    private bool __callBase_IsBlessing(int levelId)
    {
      return this.IsBlessing(levelId);
    }

    private bool __callBase_IsDailyChallenge()
    {
      return this.IsDailyChallenge();
    }

    private int __callBase_GetDailyChallengNums()
    {
      return this.GetDailyChallengNums();
    }

    private void __callBase_AddChallengedNums(int nums)
    {
      this.AddChallengedNums(nums);
    }

    private List<int> __callBase_GetAllUnlockedLevels()
    {
      return this.GetAllUnlockedLevels();
    }

    private bool __callBase_IsLevelUnlocked(int levelId)
    {
      return this.IsLevelUnlocked(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroTrainningComponent m_owner;

      public LuaExportHelper(HeroTrainningComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public int __callBase_AttackHeroTrainningLevel(int levelId)
      {
        return this.m_owner.__callBase_AttackHeroTrainningLevel(levelId);
      }

      public int __callBase_CanAttackHeroTrainningLevel(int levelId, bool isTeamBattle)
      {
        return this.m_owner.__callBase_CanAttackHeroTrainningLevel(levelId, isTeamBattle);
      }

      public bool __callBase_IsLevelFinished(int levelId)
      {
        return this.m_owner.__callBase_IsLevelFinished(levelId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_SetCommonSuccessHeroTrainningLevel(
        ConfigDataHeroTrainningLevelInfo levelInfo,
        List<int> battleTreasures,
        List<int> heroes,
        int energyCost,
        bool isBattleTeam)
      {
        // ISSUE: unable to decompile the method.
      }

      public HashSet<int> __callBase_GetAllFinishedLevels()
      {
        return this.m_owner.__callBase_GetAllFinishedLevels();
      }

      public bool __callBase_IsBlessing(int levelId)
      {
        return this.m_owner.__callBase_IsBlessing(levelId);
      }

      public bool __callBase_IsDailyChallenge()
      {
        return this.m_owner.__callBase_IsDailyChallenge();
      }

      public int __callBase_GetDailyChallengNums()
      {
        return this.m_owner.__callBase_GetDailyChallengNums();
      }

      public void __callBase_AddChallengedNums(int nums)
      {
        this.m_owner.__callBase_AddChallengedNums(nums);
      }

      public List<int> __callBase_GetAllUnlockedLevels()
      {
        return this.m_owner.__callBase_GetAllUnlockedLevels();
      }

      public bool __callBase_IsLevelUnlocked(int levelId)
      {
        return this.m_owner.__callBase_IsLevelUnlocked(levelId);
      }
    }
  }
}
