﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.GmCommand
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class GmCommand
  {
    public const string AddItem = "ADD_ITEM";
    public const string RemoveItem = "REMOVE_ITEM";
    public const string ClearBag = "CLEAR_BAG";
    public const string AddHero = "ADD_HERO";
    public const string SendTemplateMail = "POST_TEMPLATEMAIL";
    public const string CleanUserGuide = "CLEAN_USER_GUIDE";
    public const string AddActivity = "POST_GLOBALOPERATIONALACTIVITY";
    public const string BattleCheat = "BATTLE_CHEAT";
    public const string BattleCheatBuff = "BATTLE_CHEAT_BUFF";
    public const string MaxTrainingTechs = "SET_MAXTRAININGTECHS";
    public const string LevelUpHeroFetter = "LevelUpHeroFetter";
    public const string LevelUpHeroHeartFetter = "Set_HeroHeartFetter";
  }
}
