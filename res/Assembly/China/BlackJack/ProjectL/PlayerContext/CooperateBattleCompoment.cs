﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CooperateBattleCompoment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class CooperateBattleCompoment : CooperateBattleCompomentCommon
  {
    [DoNotToLua]
    private CooperateBattleCompoment.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSCooperateBattleNtf_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_SetSuccessCooperateBattleLevelConfigDataCooperateBattleLevelInfoList`1List`1_hotfix;
    private LuaFunction m_GetMaxFinishedLevelIdInt32_hotfix;
    private LuaFunction m_GetMaxUnlockedLevelIdInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CooperateBattleCompoment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSCooperateBattleNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessCooperateBattleLevel(
      ConfigDataCooperateBattleLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxUnlockedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public CooperateBattleCompoment.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private bool __callBase_CheckCooperateBattleAvailable(int CooperateBattleID, ref int Err)
    {
      return this.CheckCooperateBattleAvailable(CooperateBattleID, ref Err);
    }

    private bool __callBase_CheckCooperateBattleDisplayable(int CooperateBattleID)
    {
      return this.CheckCooperateBattleDisplayable(CooperateBattleID);
    }

    private bool __callBase_IsLevelUnlocked(int LevelId)
    {
      return this.IsLevelUnlocked(LevelId);
    }

    private bool __callBase_IsLevelFinished(int LevelId)
    {
      return this.IsLevelFinished(LevelId);
    }

    private List<int> __callBase_GetAllUnlockedLevels()
    {
      return this.GetAllUnlockedLevels();
    }

    private List<int> __callBase_GetAllFinishedLevels()
    {
      return this.GetAllFinishedLevels();
    }

    private int __callBase_GetDailyChallengeNums(int BattleId)
    {
      return this.GetDailyChallengeNums(BattleId);
    }

    private bool __callBase_CheckCooperateBattleLevelAvailable(
      int BattleId,
      int LevelId,
      ref int Err)
    {
      return this.CheckCooperateBattleLevelAvailable(BattleId, LevelId, ref Err);
    }

    private bool __callBase_CheckPlayerOutOfBattle(ref int Err)
    {
      return this.CheckPlayerOutOfBattle(ref Err);
    }

    private bool __callBase_CheckEnergy(int BattleId, int LevelId, ref int Err)
    {
      return this.CheckEnergy(BattleId, LevelId, ref Err);
    }

    private bool __callBase_CheckBag(int CooperateBattleId, int LevelId, ref int Err)
    {
      return this.CheckBag(CooperateBattleId, LevelId, ref Err);
    }

    private int __callBase_CanAttackCooperateBattleLevel(int BattleId, int LevelId)
    {
      return this.CanAttackCooperateBattleLevel(BattleId, LevelId);
    }

    private int __callBase_CanAttackCooperateBattleLevel(int LevelId)
    {
      return this.CanAttackCooperateBattleLevel(LevelId);
    }

    private void __callBase_SetCommonSuccessCooperateBattleLevel(
      ConfigDataCooperateBattleLevelInfo Level,
      List<int> Heroes,
      List<int> BattleTreasures)
    {
      this.SetCommonSuccessCooperateBattleLevel(Level, Heroes, BattleTreasures);
    }

    private void __callBase_FinishedCooperateBattleLevel(
      CooperateBattleLevel Level,
      List<int> heroes)
    {
      this.FinishedCooperateBattleLevel(Level, heroes);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CooperateBattleCompoment m_owner;

      public LuaExportHelper(CooperateBattleCompoment owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public bool __callBase_CheckCooperateBattleAvailable(int CooperateBattleID, ref int Err)
      {
        return this.m_owner.__callBase_CheckCooperateBattleAvailable(CooperateBattleID, ref Err);
      }

      public bool __callBase_CheckCooperateBattleDisplayable(int CooperateBattleID)
      {
        return this.m_owner.__callBase_CheckCooperateBattleDisplayable(CooperateBattleID);
      }

      public bool __callBase_IsLevelUnlocked(int LevelId)
      {
        return this.m_owner.__callBase_IsLevelUnlocked(LevelId);
      }

      public bool __callBase_IsLevelFinished(int LevelId)
      {
        return this.m_owner.__callBase_IsLevelFinished(LevelId);
      }

      public List<int> __callBase_GetAllUnlockedLevels()
      {
        return this.m_owner.__callBase_GetAllUnlockedLevels();
      }

      public List<int> __callBase_GetAllFinishedLevels()
      {
        return this.m_owner.__callBase_GetAllFinishedLevels();
      }

      public int __callBase_GetDailyChallengeNums(int BattleId)
      {
        return this.m_owner.__callBase_GetDailyChallengeNums(BattleId);
      }

      public bool __callBase_CheckCooperateBattleLevelAvailable(
        int BattleId,
        int LevelId,
        ref int Err)
      {
        return this.m_owner.__callBase_CheckCooperateBattleLevelAvailable(BattleId, LevelId, ref Err);
      }

      public bool __callBase_CheckPlayerOutOfBattle(ref int Err)
      {
        return this.m_owner.__callBase_CheckPlayerOutOfBattle(ref Err);
      }

      public bool __callBase_CheckEnergy(int BattleId, int LevelId, ref int Err)
      {
        return this.m_owner.__callBase_CheckEnergy(BattleId, LevelId, ref Err);
      }

      public bool __callBase_CheckBag(int CooperateBattleId, int LevelId, ref int Err)
      {
        return this.m_owner.__callBase_CheckBag(CooperateBattleId, LevelId, ref Err);
      }

      public int __callBase_CanAttackCooperateBattleLevel(int BattleId, int LevelId)
      {
        return this.m_owner.__callBase_CanAttackCooperateBattleLevel(BattleId, LevelId);
      }

      public int __callBase_CanAttackCooperateBattleLevel(int LevelId)
      {
        return this.m_owner.__callBase_CanAttackCooperateBattleLevel(LevelId);
      }

      public void __callBase_SetCommonSuccessCooperateBattleLevel(
        ConfigDataCooperateBattleLevelInfo Level,
        List<int> Heroes,
        List<int> BattleTreasures)
      {
        this.m_owner.__callBase_SetCommonSuccessCooperateBattleLevel(Level, Heroes, BattleTreasures);
      }

      public void __callBase_FinishedCooperateBattleLevel(
        CooperateBattleLevel Level,
        List<int> heroes)
      {
        this.m_owner.__callBase_FinishedCooperateBattleLevel(Level, heroes);
      }
    }
  }
}
