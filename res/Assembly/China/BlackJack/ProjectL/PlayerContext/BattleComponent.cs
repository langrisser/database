﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.BattleComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class BattleComponent : BattleComponentCommon
  {
    private BattleRoom m_battleRoom;
    private BattleReward m_battleReward;
    [DoNotToLua]
    private BattleComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSBattleNtf_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_SetBattleBaseBattleBase_hotfix;
    private LuaFunction m_GetBattleBase_hotfix;
    private LuaFunction m_IsRiftBattling_hotfix;
    private LuaFunction m_IsTreasureMapBattling_hotfix;
    private LuaFunction m_GetBattleTeamInt32_hotfix;
    private LuaFunction m_ClearFighting_hotfix;
    private LuaFunction m_SetBattleRandomSeedInt32_hotfix;
    private LuaFunction m_SetArenaBattleRandomSeedInt32_hotfix;
    private LuaFunction m_GetArenaBattleStatus_hotfix;
    private LuaFunction m_GetArenaBattleId_hotfix;
    private LuaFunction m_GetGotBattleTreasureIds_hotfix;
    private LuaFunction m_JoinTeamBattleRoomUInt64Int32GameFunctionTypeInt32List`1DateTime_hotfix;
    private LuaFunction m_JoinGuildMassiveCombatBattleRoomUInt64Int32Int32List`1DateTimeList`1UInt64_hotfix;
    private LuaFunction m_JoinPVPBattleRoomUInt64Int32List`1DateTime_hotfix;
    private LuaFunction m_JoinRealtimePVPBattleRoomUInt64Int32List`1DateTimeBattleRoomType_hotfix;
    private LuaFunction m_JoinPeakArenaBattleRoomUInt64Int32List`1DateTimeRealTimePVPModeInt32Int32Int32UInt64Int32_hotfix;
    private LuaFunction m_JoinPeakArenaBattleLiveRoomUInt64Int32ProPeakArenaBattleLiveRoomInfo_hotfix;
    private LuaFunction m_QuitBattleRoom_hotfix;
    private LuaFunction m_SetRealtimePVPBattleRoomDataBattleRoomDataChangeNtf_hotfix;
    private LuaFunction m_SetBattleRoomPlayerStatusInt32PlayerBattleStatusBoolean_hotfix;
    private LuaFunction m_InitBattleRoomPlayerTechsInt32List`1_hotfix;
    private LuaFunction m_BattleRoomHeroSetupProBattleHeroSetupInfo_hotfix;
    private LuaFunction m_BattleRoomHeroSetupAllList`1_hotfix;
    private LuaFunction m_BattleRoomBattleStart_hotfix;
    private LuaFunction m_AppendBattleRoomCommandsList`1_hotfix;
    private LuaFunction m_InitPeakArenaBPStageHerosInt32List`1_hotfix;
    private LuaFunction m_SetPeakAreanBPStageDataProBattleRoomBPStageInt32_hotfix;
    private LuaFunction m_SetPeakArenaBPStageTimeSpansInt64List`1_hotfix;
    private LuaFunction m_SetPeakArenaBattleActionTimeSpansInt64List`1_hotfix;
    private LuaFunction m_PeakArenaBPStageExecuteCommandInt32ProBPStageCommand_hotfix;
    private LuaFunction m_IsInBattleRoom_hotfix;
    private LuaFunction m_GetBattleRoom_hotfix;
    private LuaFunction m_GetBattleReward_hotfix;
    private LuaFunction m_GetChapterIdFromLevelIdGameFunctionTypeInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSBattleNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleBase(BattleBase battleBase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleBase GetBattleBase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRiftBattling()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTreasureMapBattling()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetBattleTeam(int teamType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearFighting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleRandomSeed(int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaBattleRandomSeed(int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleStatus GetArenaBattleStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaBattleId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetGotBattleTreasureIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinTeamBattleRoom(
      ulong roomId,
      int battleId,
      GameFunctionType gameFunctionType,
      int locationId,
      List<ProBattleRoomPlayer> players,
      DateTime readyTimeOut)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinGuildMassiveCombatBattleRoom(
      ulong roomId,
      int battleId,
      int locationId,
      List<ProBattleRoomPlayer> players,
      DateTime readyTimeOut,
      List<int> preferredHeroTagIds,
      ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinPVPBattleRoom(
      ulong roomId,
      int battleId,
      List<ProBattleRoomPlayer> players,
      DateTime readyTimeOut)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinRealtimePVPBattleRoom(
      ulong roomId,
      int battleId,
      List<ProBattleRoomPlayer> players,
      DateTime readyTimeOut,
      BattleRoomType roomType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinPeakArenaBattleRoom(
      ulong roomId,
      int battleId,
      List<ProBattleRoomPlayer> players,
      DateTime readyTimeOut,
      RealTimePVPMode mode,
      int round,
      int boRound,
      int randomSeed,
      ulong instanceId,
      int matchupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinPeakArenaBattleLiveRoom(
      ulong roomId,
      int matchupId,
      ProPeakArenaBattleLiveRoomInfo roomInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int QuitBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRealtimePVPBattleRoomData(BattleRoomDataChangeNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleRoomPlayerStatus(
      int playerIndex,
      PlayerBattleStatus status,
      bool isOffLine)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBattleRoomPlayerTechs(int playerIndex, List<ProTrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BattleRoomHeroSetup(ProBattleHeroSetupInfo heroSetup)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BattleRoomHeroSetupAll(List<ProBattleHeroSetupInfo> heroSetups)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BattleRoomBattleStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AppendBattleRoomCommands(List<ProBattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPeakArenaBPStageHeros(int playerIndex, List<ProBPStageHero> proHeros)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakAreanBPStageData(ProBattleRoomBPStage proBattleRoomBPStage, int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaBPStageTimeSpans(long turnTimeSpan, List<long> publicTimeSpans)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaBattleActionTimeSpans(
      long turnActionTimeSpan,
      List<long> publicTimeSpans)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PeakArenaBPStageExecuteCommand(int playerIndex, ProBPStageCommand proCommand)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoom GetBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReward GetBattleReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChapterIdFromLevelId(GameFunctionType gameFunctionType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private bool __callBase_IsGameFunctionOpenByMonthCard(GameFunctionType gameFuncTypeId)
    {
      return this.IsGameFunctionOpenByMonthCard(gameFuncTypeId);
    }

    private int __callBase_GetDailyBonusMaxNums(GameFunctionType gameFuncTypeId)
    {
      return this.GetDailyBonusMaxNums(gameFuncTypeId);
    }

    private int __callBase_GetSinglePveBattleFightEnergyCost()
    {
      return this.GetSinglePveBattleFightEnergyCost();
    }

    private int __callBase_GetSinglePveBattleFailEnergyCostByMonthCard()
    {
      return this.GetSinglePveBattleFailEnergyCostByMonthCard();
    }

    private int __callBase_GetTeamPveBattleFailEnergyCost(GameFunctionType typeId, int locationId)
    {
      return this.GetTeamPveBattleFailEnergyCost(typeId, locationId);
    }

    private int __callBase_GetTeamPveBattleFailEnergyCostByMonthCard(
      GameFunctionType typeId,
      int locationId)
    {
      return this.GetTeamPveBattleFailEnergyCostByMonthCard(typeId, locationId);
    }

    private void __callBase_AddTeamPveFightFailCompensationEnergyByMonthCard(
      GameFunctionType typeId,
      int locationId)
    {
      this.AddTeamPveFightFailCompensationEnergyByMonthCard(typeId, locationId);
    }

    private bool __callBase_IsWaypointBattling()
    {
      return this.IsWaypointBattling();
    }

    private bool __callBase_IsCollectionWaypointBattling()
    {
      return this.IsCollectionWaypointBattling();
    }

    private bool __callBase_IsBattleWin()
    {
      return this.IsBattleWin();
    }

    private int __callBase_CancelBattle()
    {
      return this.CancelBattle();
    }

    private bool __callBase_IsFighting()
    {
      return this.IsFighting();
    }

    private bool __callBase_CanSetupMineTeam()
    {
      return this.CanSetupMineTeam();
    }

    private void __callBase_FightFinished(GameFunctionStatus status, bool win, bool needBatlleLog)
    {
      this.FightFinished(status, win, needBatlleLog);
    }

    private bool __callBase_IsWayPointFightExist(int wayPointId)
    {
      return this.IsWayPointFightExist(wayPointId);
    }

    private void __callBase_WinPveBattle(int battleId)
    {
      this.WinPveBattle(battleId);
    }

    private void __callBase_FinishedArenaFight()
    {
      this.FinishedArenaFight();
    }

    private ProcessingBattle __callBase_GetProcessingBattle()
    {
      return this.GetProcessingBattle();
    }

    private void __callBase_SetProcessingBattleInfo(BattleType type, int typeId)
    {
      this.SetProcessingBattleInfo(type, typeId);
    }

    private void __callBase_SetBattleArmyRandomSeed(int armyRandomSeed)
    {
      this.SetBattleArmyRandomSeed(armyRandomSeed);
    }

    private int __callBase_GetBattleArmyRandomSeed()
    {
      return this.GetBattleArmyRandomSeed();
    }

    private void __callBase_SetArenaBattleInfo(int arenaBattleId)
    {
      this.SetArenaBattleInfo(arenaBattleId);
    }

    private bool __callBase_IsAttackingPveLevel(BattleType battleType, int levelId)
    {
      return this.IsAttackingPveLevel(battleType, levelId);
    }

    private bool __callBase_IsAttackingArenaOpponent()
    {
      return this.IsAttackingArenaOpponent();
    }

    private bool __callBase_IsArenaBattleInReady()
    {
      return this.IsArenaBattleInReady();
    }

    private bool __callBase_IsAttackingInBattleServer()
    {
      return this.IsAttackingInBattleServer();
    }

    private void __callBase_FinishBattleInBattleServer()
    {
      this.FinishBattleInBattleServer();
    }

    private int __callBase_CanCreateBattleRoom(BattleRoomType battleRoomType)
    {
      return this.CanCreateBattleRoom(battleRoomType);
    }

    private int __callBase_CanCreateTeamBattleRoom()
    {
      return this.CanCreateTeamBattleRoom();
    }

    private int __callBase_CanChangePlayerBattleStatus(PlayerBattleStatus status)
    {
      return this.CanChangePlayerBattleStatus(status);
    }

    private void __callBase_SetArenaBattleFighting()
    {
      this.SetArenaBattleFighting();
    }

    private int __callBase_GetBattleId()
    {
      return this.GetBattleId();
    }

    private int __callBase_GetBattleId(GameFunctionType typeId, int loctionId)
    {
      return this.GetBattleId(typeId, loctionId);
    }

    private int __callBase_GetMonsterLevel()
    {
      return this.GetMonsterLevel();
    }

    private void __callBase_AddFightHeroFightNumsAndExp(List<int> heroes, int exp)
    {
      this.AddFightHeroFightNumsAndExp(heroes, exp);
    }

    private List<int> __callBase_GetPveTeam()
    {
      return this.GetPveTeam();
    }

    private int __callBase_GetSubGameTypeId(BattleType battleType, int processingBattleInfoTypeId)
    {
      return this.GetSubGameTypeId(battleType, processingBattleInfoTypeId);
    }

    private int __callBase_IsMineTeamSetValid(
      int battleId,
      int gamePlayTeamTypeId,
      List<int> teamHeroes)
    {
      return this.IsMineTeamSetValid(battleId, gamePlayTeamTypeId, teamHeroes);
    }

    private int __callBase_IsArenaDefensiveTeamSetValid(int battleId, int teamCount)
    {
      return this.IsArenaDefensiveTeamSetValid(battleId, teamCount);
    }

    private bool __callBase_IsActionPositionIndexValid(
      ConfigDataArenaBattleInfo battleInfo,
      int actionPositionIndex)
    {
      return this.IsActionPositionIndexValid(battleInfo, actionPositionIndex);
    }

    private bool __callBase_IsActionValueValid(
      ConfigDataArenaBattleInfo battleInfo,
      int actionValue)
    {
      return this.IsActionValueValid(battleInfo, actionValue);
    }

    private int __callBase_SetMineTeam(int battleId, int gamePlayTeamTypeId, List<int> team)
    {
      return this.SetMineTeam(battleId, gamePlayTeamTypeId, team);
    }

    private void __callBase_SetTeamById(int gamePlayTeamTypeId, List<int> team)
    {
      this.SetTeamById(gamePlayTeamTypeId, team);
    }

    private int __callBase_GetGamePlayTeamTypeId(BattleType battleType, int subGameTypeId)
    {
      return this.GetGamePlayTeamTypeId(battleType, subGameTypeId);
    }

    private ConfigDataGamePlayTeamTypeInfo __callBase_FindGamePlayTeamTypeInfo(
      BattleType battleType,
      int subGameTypeId)
    {
      return this.FindGamePlayTeamTypeInfo(battleType, subGameTypeId);
    }

    private List<int> __callBase_GetTeam(BattleType battleType, int subGameTypeId)
    {
      return this.GetTeam(battleType, subGameTypeId);
    }

    private List<int> __callBase_GetTeamById(int id)
    {
      return this.GetTeamById(id);
    }

    private List<BattleHeroEquipment> __callBase_CreateMineBattleHeroEquipments(
      ulong[] equipmentIds)
    {
      return this.CreateMineBattleHeroEquipments(equipmentIds);
    }

    private BattleHero __callBase_MineHeroToBattleHero(Hero hero)
    {
      return this.MineHeroToBattleHero(hero);
    }

    private int __callBase_ComputeBattlePowerFromBattleHeroes(
      List<BattleHero> heroes,
      List<TrainingTech> techs)
    {
      return this.ComputeBattlePowerFromBattleHeroes(heroes, techs);
    }

    private int __callBase_ComputeBattlePower(Hero hero)
    {
      return this.ComputeBattlePower(hero);
    }

    private int __callBase_ComputeBattlePower(Hero hero, ulong[] equipmentIds)
    {
      return this.ComputeBattlePower(hero, equipmentIds);
    }

    private int __callBase_ComputeBattlePower(Hero hero, List<EquipmentBagItem> equipments)
    {
      return this.ComputeBattlePower(hero, equipments);
    }

    private int __callBase_ComputeBattlePower(BattleHero hero, List<TrainingTech> techs)
    {
      return this.ComputeBattlePower(hero, techs);
    }

    private int __callBase_ComputeEquipiemntBattlePower(Hero hero, ulong[] equipmentIds)
    {
      return this.ComputeEquipiemntBattlePower(hero, equipmentIds);
    }

    private void __callBase_InitGainBattleTreasures()
    {
      this.InitGainBattleTreasures();
    }

    private void __callBase_AddGotBattleTreasuresInThisBattle()
    {
      this.AddGotBattleTreasuresInThisBattle();
    }

    private void __callBase_AddBattleTreasures(List<int> battleTreasures)
    {
      this.AddBattleTreasures(battleTreasures);
    }

    private List<int> __callBase_GetGainBattleTreasuresInThisBattle()
    {
      return this.GetGainBattleTreasuresInThisBattle();
    }

    private void __callBase_OnBattlePracticeMissionEvent()
    {
      this.OnBattlePracticeMissionEvent();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleComponent m_owner;

      public LuaExportHelper(BattleComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public bool __callBase_IsGameFunctionOpenByMonthCard(GameFunctionType gameFuncTypeId)
      {
        return this.m_owner.__callBase_IsGameFunctionOpenByMonthCard(gameFuncTypeId);
      }

      public int __callBase_GetDailyBonusMaxNums(GameFunctionType gameFuncTypeId)
      {
        return this.m_owner.__callBase_GetDailyBonusMaxNums(gameFuncTypeId);
      }

      public int __callBase_GetSinglePveBattleFightEnergyCost()
      {
        return this.m_owner.__callBase_GetSinglePveBattleFightEnergyCost();
      }

      public int __callBase_GetSinglePveBattleFailEnergyCostByMonthCard()
      {
        return this.m_owner.__callBase_GetSinglePveBattleFailEnergyCostByMonthCard();
      }

      public int __callBase_GetTeamPveBattleFailEnergyCost(GameFunctionType typeId, int locationId)
      {
        return this.m_owner.__callBase_GetTeamPveBattleFailEnergyCost(typeId, locationId);
      }

      public int __callBase_GetTeamPveBattleFailEnergyCostByMonthCard(
        GameFunctionType typeId,
        int locationId)
      {
        return this.m_owner.__callBase_GetTeamPveBattleFailEnergyCostByMonthCard(typeId, locationId);
      }

      public void __callBase_AddTeamPveFightFailCompensationEnergyByMonthCard(
        GameFunctionType typeId,
        int locationId)
      {
        this.m_owner.__callBase_AddTeamPveFightFailCompensationEnergyByMonthCard(typeId, locationId);
      }

      public bool __callBase_IsWaypointBattling()
      {
        return this.m_owner.__callBase_IsWaypointBattling();
      }

      public bool __callBase_IsCollectionWaypointBattling()
      {
        return this.m_owner.__callBase_IsCollectionWaypointBattling();
      }

      public bool __callBase_IsBattleWin()
      {
        return this.m_owner.__callBase_IsBattleWin();
      }

      public int __callBase_CancelBattle()
      {
        return this.m_owner.__callBase_CancelBattle();
      }

      public bool __callBase_IsFighting()
      {
        return this.m_owner.__callBase_IsFighting();
      }

      public bool __callBase_CanSetupMineTeam()
      {
        return this.m_owner.__callBase_CanSetupMineTeam();
      }

      public void __callBase_FightFinished(GameFunctionStatus status, bool win, bool needBatlleLog)
      {
        this.m_owner.__callBase_FightFinished(status, win, needBatlleLog);
      }

      public bool __callBase_IsWayPointFightExist(int wayPointId)
      {
        return this.m_owner.__callBase_IsWayPointFightExist(wayPointId);
      }

      public void __callBase_WinPveBattle(int battleId)
      {
        this.m_owner.__callBase_WinPveBattle(battleId);
      }

      public void __callBase_FinishedArenaFight()
      {
        this.m_owner.__callBase_FinishedArenaFight();
      }

      public ProcessingBattle __callBase_GetProcessingBattle()
      {
        return this.m_owner.__callBase_GetProcessingBattle();
      }

      public void __callBase_SetProcessingBattleInfo(BattleType type, int typeId)
      {
        this.m_owner.__callBase_SetProcessingBattleInfo(type, typeId);
      }

      public void __callBase_SetBattleArmyRandomSeed(int armyRandomSeed)
      {
        this.m_owner.__callBase_SetBattleArmyRandomSeed(armyRandomSeed);
      }

      public int __callBase_GetBattleArmyRandomSeed()
      {
        return this.m_owner.__callBase_GetBattleArmyRandomSeed();
      }

      public void __callBase_SetArenaBattleInfo(int arenaBattleId)
      {
        this.m_owner.__callBase_SetArenaBattleInfo(arenaBattleId);
      }

      public bool __callBase_IsAttackingPveLevel(BattleType battleType, int levelId)
      {
        return this.m_owner.__callBase_IsAttackingPveLevel(battleType, levelId);
      }

      public bool __callBase_IsAttackingArenaOpponent()
      {
        return this.m_owner.__callBase_IsAttackingArenaOpponent();
      }

      public bool __callBase_IsArenaBattleInReady()
      {
        return this.m_owner.__callBase_IsArenaBattleInReady();
      }

      public bool __callBase_IsAttackingInBattleServer()
      {
        return this.m_owner.__callBase_IsAttackingInBattleServer();
      }

      public void __callBase_FinishBattleInBattleServer()
      {
        this.m_owner.__callBase_FinishBattleInBattleServer();
      }

      public int __callBase_CanCreateBattleRoom(BattleRoomType battleRoomType)
      {
        return this.m_owner.__callBase_CanCreateBattleRoom(battleRoomType);
      }

      public int __callBase_CanCreateTeamBattleRoom()
      {
        return this.m_owner.__callBase_CanCreateTeamBattleRoom();
      }

      public int __callBase_CanChangePlayerBattleStatus(PlayerBattleStatus status)
      {
        return this.m_owner.__callBase_CanChangePlayerBattleStatus(status);
      }

      public void __callBase_SetArenaBattleFighting()
      {
        this.m_owner.__callBase_SetArenaBattleFighting();
      }

      public int __callBase_GetBattleId()
      {
        return this.m_owner.__callBase_GetBattleId();
      }

      public int __callBase_GetBattleId(GameFunctionType typeId, int loctionId)
      {
        return this.m_owner.__callBase_GetBattleId(typeId, loctionId);
      }

      public int __callBase_GetMonsterLevel()
      {
        return this.m_owner.__callBase_GetMonsterLevel();
      }

      public void __callBase_AddFightHeroFightNumsAndExp(List<int> heroes, int exp)
      {
        this.m_owner.__callBase_AddFightHeroFightNumsAndExp(heroes, exp);
      }

      public List<int> __callBase_GetPveTeam()
      {
        return this.m_owner.__callBase_GetPveTeam();
      }

      public int __callBase_GetSubGameTypeId(BattleType battleType, int processingBattleInfoTypeId)
      {
        return this.m_owner.__callBase_GetSubGameTypeId(battleType, processingBattleInfoTypeId);
      }

      public int __callBase_IsMineTeamSetValid(
        int battleId,
        int gamePlayTeamTypeId,
        List<int> teamHeroes)
      {
        return this.m_owner.__callBase_IsMineTeamSetValid(battleId, gamePlayTeamTypeId, teamHeroes);
      }

      public int __callBase_IsArenaDefensiveTeamSetValid(int battleId, int teamCount)
      {
        return this.m_owner.__callBase_IsArenaDefensiveTeamSetValid(battleId, teamCount);
      }

      public bool __callBase_IsActionPositionIndexValid(
        ConfigDataArenaBattleInfo battleInfo,
        int actionPositionIndex)
      {
        return this.m_owner.__callBase_IsActionPositionIndexValid(battleInfo, actionPositionIndex);
      }

      public bool __callBase_IsActionValueValid(
        ConfigDataArenaBattleInfo battleInfo,
        int actionValue)
      {
        return this.m_owner.__callBase_IsActionValueValid(battleInfo, actionValue);
      }

      public int __callBase_SetMineTeam(int battleId, int gamePlayTeamTypeId, List<int> team)
      {
        return this.m_owner.__callBase_SetMineTeam(battleId, gamePlayTeamTypeId, team);
      }

      public void __callBase_SetTeamById(int gamePlayTeamTypeId, List<int> team)
      {
        this.m_owner.__callBase_SetTeamById(gamePlayTeamTypeId, team);
      }

      public int __callBase_GetGamePlayTeamTypeId(BattleType battleType, int subGameTypeId)
      {
        return this.m_owner.__callBase_GetGamePlayTeamTypeId(battleType, subGameTypeId);
      }

      public ConfigDataGamePlayTeamTypeInfo __callBase_FindGamePlayTeamTypeInfo(
        BattleType battleType,
        int subGameTypeId)
      {
        return this.m_owner.__callBase_FindGamePlayTeamTypeInfo(battleType, subGameTypeId);
      }

      public List<int> __callBase_GetTeam(BattleType battleType, int subGameTypeId)
      {
        return this.m_owner.__callBase_GetTeam(battleType, subGameTypeId);
      }

      public List<int> __callBase_GetTeamById(int id)
      {
        return this.m_owner.__callBase_GetTeamById(id);
      }

      public List<BattleHeroEquipment> __callBase_CreateMineBattleHeroEquipments(
        ulong[] equipmentIds)
      {
        return this.m_owner.__callBase_CreateMineBattleHeroEquipments(equipmentIds);
      }

      public BattleHero __callBase_MineHeroToBattleHero(Hero hero)
      {
        return this.m_owner.__callBase_MineHeroToBattleHero(hero);
      }

      public int __callBase_ComputeBattlePowerFromBattleHeroes(
        List<BattleHero> heroes,
        List<TrainingTech> techs)
      {
        return this.m_owner.__callBase_ComputeBattlePowerFromBattleHeroes(heroes, techs);
      }

      public int __callBase_ComputeBattlePower(Hero hero)
      {
        return this.m_owner.__callBase_ComputeBattlePower(hero);
      }

      public int __callBase_ComputeBattlePower(Hero hero, ulong[] equipmentIds)
      {
        return this.m_owner.__callBase_ComputeBattlePower(hero, equipmentIds);
      }

      public int __callBase_ComputeBattlePower(Hero hero, List<EquipmentBagItem> equipments)
      {
        return this.m_owner.__callBase_ComputeBattlePower(hero, equipments);
      }

      public int __callBase_ComputeBattlePower(BattleHero hero, List<TrainingTech> techs)
      {
        return this.m_owner.__callBase_ComputeBattlePower(hero, techs);
      }

      public int __callBase_ComputeEquipiemntBattlePower(Hero hero, ulong[] equipmentIds)
      {
        return this.m_owner.__callBase_ComputeEquipiemntBattlePower(hero, equipmentIds);
      }

      public void __callBase_InitGainBattleTreasures()
      {
        this.m_owner.__callBase_InitGainBattleTreasures();
      }

      public void __callBase_AddGotBattleTreasuresInThisBattle()
      {
        this.m_owner.__callBase_AddGotBattleTreasuresInThisBattle();
      }

      public void __callBase_AddBattleTreasures(List<int> battleTreasures)
      {
        this.m_owner.__callBase_AddBattleTreasures(battleTreasures);
      }

      public List<int> __callBase_GetGainBattleTreasuresInThisBattle()
      {
        return this.m_owner.__callBase_GetGainBattleTreasuresInThisBattle();
      }

      public void __callBase_OnBattlePracticeMissionEvent()
      {
        this.m_owner.__callBase_OnBattlePracticeMissionEvent();
      }

      public BattleRoom m_battleRoom
      {
        get
        {
          return this.m_owner.m_battleRoom;
        }
        set
        {
          this.m_owner.m_battleRoom = value;
        }
      }

      public BattleReward m_battleReward
      {
        get
        {
          return this.m_owner.m_battleReward;
        }
        set
        {
          this.m_owner.m_battleReward = value;
        }
      }
    }
  }
}
