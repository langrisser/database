﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PVPInviteInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using SLua;
using System;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class PVPInviteInfo
  {
    public UserSummary Inviter;
    public PracticeMode PracticeMode;
    public DateTime Timeout;
  }
}
