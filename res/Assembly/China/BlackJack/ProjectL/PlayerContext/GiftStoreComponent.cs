﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.GiftStoreComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class GiftStoreComponent : GiftStoreComponentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    public ushort GetDSVersion()
    {
      return this.m_giftStoreDS.ClientVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGiftStoreItemSellOut(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSGiftStoreNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(GiftStoreOperationalGoodsUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuyGoods(int goodsId, string registerId, int boughtNums, long nextFlushTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreItem GetBoughtItem(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
