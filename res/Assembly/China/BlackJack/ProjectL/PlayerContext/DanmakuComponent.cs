﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.DanmakuComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class DanmakuComponent : DanmakuComponentCommon
  {
    private List<PostDanmakuEntry> m_postDanmakuEntries;
    private LevelDanmaku m_levelDanmaku;
    private List<int> m_newSendDanmakuTurnList;

    [MethodImpl((MethodImplOptions) 32768)]
    public DanmakuComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLevelDanmaku(LevelDanmaku levelDanmaku)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FilterSensitiveWords()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private LevelDanmaku BuildLevelDanmakuOrderByTime(LevelDanmaku source)
    {
      // ISSUE: unable to decompile the method.
    }

    public LevelDanmaku GetLevelDanmaku()
    {
      return this.m_levelDanmaku;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostLevelDanmaku(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPostDanmakuEntryToLocalLevelDanmaku(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddTurnDanmakuToLevelDanmaku(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private DanmakuEntry CreateDanmakuEntry(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearLevelDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<PostDanmakuEntry> GetPostedLevelDanmaku()
    {
      this.ClearNewSendDanmakuTurnList();
      return this.m_postDanmakuEntries;
    }

    public void ClearNewSendDanmakuTurnList()
    {
      this.m_newSendDanmakuTurnList.Clear();
    }
  }
}
