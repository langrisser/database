﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.RiftComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class RiftComponent : RiftComponentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSRiftNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitChapterInfos(List<ProRiftChapter> proChapters)
    {
      // ISSUE: unable to decompile the method.
    }

    public ushort GetDSVersion()
    {
      return this.m_riftDS.ClientVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GainChapterReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void SetSuccessRiftLevel(
      ConfigDataRiftLevelInfo riftLevelInfo,
      List<int> newAchievementIds,
      int stars,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void ComplteRiftLevel(int riftLevelId)
    {
      base.ComplteRiftLevel(riftLevelId);
    }

    public override void CompleteAchievement(int achievementId)
    {
      base.CompleteAchievement(achievementId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftLevelChallengeNum(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftLevelCanChallengeNums(ConfigDataRiftLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RaidRiftLevel(int riftlevelId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRiftLevelChallenged(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftLevelStars(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishBattleRiftLevel(
      int riftLevelId,
      List<int> gotAchievementIds,
      int star,
      List<int> battleTreasureIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainChapterStarReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRiftChapterUnlockConditionCompleted(
      int chapterId,
      RiftChapterUnlockConditionType condition,
      int param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRiftLevelUnlockConditionCompleted(
      int levelId,
      RiftLevelUnlockConditionType condition,
      int param)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
