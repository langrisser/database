﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.WorldPathNode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class WorldPathNode
  {
    public int m_waypointId;
    public Vector2 m_position;

    public bool IsSameState(WorldPathNode rhs)
    {
      return this.m_waypointId == rhs.m_waypointId;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetGoalHeuristic(WorldPathfinder pathfinder)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetCost(WorldPathfinder pathfinder, WorldPathNode successor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetSuccessors(WorldPathfinder pathfinder, WorldPathNode parentNode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddSuccessor(WorldPathfinder pathfinder, int id, WorldPathNode parentNode)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
