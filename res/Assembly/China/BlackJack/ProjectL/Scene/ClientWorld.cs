﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorld
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Art;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.UI;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorld : IFxEventListener
  {
    private ClientWorldState m_state;
    private int m_entityIdCounter;
    private int m_frameCount;
    private float m_worldTickTime;
    private float m_timeScale;
    private float m_globalTimeScale;
    private bool m_isPaused;
    private bool m_enableDebugDraw;
    private RandomNumber m_randomNumber;
    private ProjectLPlayerContext m_playerContext;
    private List<ClientWorldPlayerActor> m_playerActors;
    private List<ClientWorldEventActor> m_eventActors;
    private List<ClientWorldWaypoint> m_waypoints;
    private List<ClientWorldRegion> m_regions;
    private IConfigDataLoader m_configDataLoader;
    private IClientWorldListener m_clientWorldListener;
    private WorldCamera m_worldCamera;
    private WorldPathfinder m_pathfinder;
    private GameObject m_worldRoot;
    private GameObject m_graphicRoot;
    private GameObject m_mapRoot;
    private GameObject m_background;
    private GameObject m_backgroundWaypointsRoot;
    private GameObject m_backgroundRegionsRoot;
    private GameObject m_worldUIRoot;
    private GameObject m_waypointUIRoot;
    private GameObject m_eventUIRoot;
    private GameObject m_playerUIRoot;
    private GameObject m_waypointUIPrefab;
    private GameObject m_waypoint2UIPrefab;
    private GameObject m_eventUIPrefab;
    private GameObject m_playerUIPrefab;
    private WorldScenarioUIController m_worldScenarioUIController;
    private GraphicPool m_graphicPool;
    private GraphicPool m_fxPool;
    private FxPlayer m_fxPlayer;
    private Quaternion m_faceCameraRotation;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(IClientWorldListener clientWorldListener, GameObject worldRoot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaypointUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlayerIconUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    private void SetTimeScale(float scale)
    {
      this.m_timeScale = scale;
      this.UpdateFinalTimeScale();
    }

    public void SetGlobalTimeScale(float scale)
    {
      this.m_globalTimeScale = scale;
      this.UpdateFinalTimeScale();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFinalTimeScale()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(ConfigDataWorldMapInfo mapInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Stop()
    {
      this.m_state = ClientWorldState.Stop;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWorld(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientWorldRegion CreateRegion(ConfigDataRegionInfo regionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientWorldWaypoint CreateWaypoint(
      ConfigDataWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientWorldPlayerActor CreatePlayerActor(
      ConfigDataWaypointInfo waypointInfo,
      int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientWorldEventActor CreateEventActor(
      ConfigDataEventInfo eventInfo,
      ConfigDataWaypointInfo waypointInfo,
      int dir,
      RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRegionState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateWaypointState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePlayerLocation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateEventActorState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateActiveScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldRegion GetRegion(int regionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldWaypoint GetWaypoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldEventActor GetEventActorAt(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldEventActor GetEventActoryByEventId(int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<ClientWorldEventActor> GetEventActors()
    {
      return this.m_eventActors;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldPlayerActor GetPlayerActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaypointClick(ClientWorldWaypoint wp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEventActorClick(ClientWorldEventActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic CreateGraphic(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    public void DestroyGraphic(GenericGraphic graphic)
    {
      this.m_graphicPool.Destroy(graphic);
    }

    private CameraBase GetCamera()
    {
      return (CameraBase) this.m_worldCamera;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetCameraPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 ComputeActorPosition(Vector2 pos, float zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsCulled(Vector2 p, bool isCombat)
    {
      return this.m_worldCamera.IsCulled(p);
    }

    public bool IsCulled(Vector2 bmin, Vector2 bmax, bool isCombat)
    {
      return this.m_worldCamera.IsCulled(bmin, bmax);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextEntityId()
    {
      // ISSUE: unable to decompile the method.
    }

    public void PlayScreenEffect(string name)
    {
      this.m_clientWorldListener.OnScreenEffect(name);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateBackground(ConfigDataWorldMapInfo worldMapInfo, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearBackground()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBackgroundChild(string childName, bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    public void PlaySound(string name)
    {
      AudioUtility.PlaySound(name);
    }

    public void PlaySound(SoundTableId id)
    {
      AudioUtility.PlaySound(id);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(int start, int goal, List<int> path, bool checkWaypointStatus)
    {
      // ISSUE: unable to decompile the method.
    }

    public static int FrameToMillisecond(int frame)
    {
      return frame * 1000 / 30;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame1(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    public WorldCamera WorldCamera
    {
      get
      {
        return this.m_worldCamera;
      }
    }

    public ClientWorldState State
    {
      get
      {
        return this.m_state;
      }
    }

    public bool EnableDebugDraw
    {
      set
      {
        this.m_enableDebugDraw = value;
      }
      get
      {
        return this.m_enableDebugDraw;
      }
    }

    public GameObject GraphicRoot
    {
      get
      {
        return this.m_graphicRoot;
      }
    }

    public GameObject WorldUIRoot
    {
      get
      {
        return this.m_worldUIRoot;
      }
    }

    public GameObject WaypointUIRoot
    {
      get
      {
        return this.m_waypointUIRoot;
      }
    }

    public GameObject EventUIRoot
    {
      get
      {
        return this.m_eventUIRoot;
      }
    }

    public GameObject PlayerUIRoot
    {
      get
      {
        return this.m_playerUIRoot;
      }
    }

    public GameObject WaypointUIPrefab
    {
      get
      {
        return this.m_waypointUIPrefab;
      }
    }

    public GameObject Waypoint2UIPrefab
    {
      get
      {
        return this.m_waypoint2UIPrefab;
      }
    }

    public GameObject EventUIPrefab
    {
      get
      {
        return this.m_eventUIPrefab;
      }
    }

    public GameObject PlayerUIPrefab
    {
      get
      {
        return this.m_playerUIPrefab;
      }
    }

    public Quaternion FaceCameraRotation
    {
      get
      {
        return this.m_faceCameraRotation;
      }
    }

    public FxPlayer FxPlayer
    {
      get
      {
        return this.m_fxPlayer;
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      get
      {
        return this.m_configDataLoader;
      }
    }

    public IClientWorldListener ClientWorldListener
    {
      get
      {
        return this.m_clientWorldListener;
      }
    }

    public void OnAudio(FxEvent e, AudioClip a)
    {
      AudioUtility.PlaySound(a);
    }

    public void OnSound(FxEvent e, string name)
    {
      AudioUtility.PlaySound(name);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCameraEffect(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnScreenEffect(FxEvent e, string name)
    {
      this.PlayScreenEffect(name);
    }

    public void OnGeneral(FxEvent e, string name)
    {
    }
  }
}
