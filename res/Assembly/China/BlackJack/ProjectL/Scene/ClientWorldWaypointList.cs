﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldWaypointList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldWaypointList
  {
    public static void RemoveAll(List<ClientWorldWaypoint> list)
    {
      EntityList.RemoveAll<ClientWorldWaypoint>(list);
    }

    public static void RemoveDeleted(List<ClientWorldWaypoint> list)
    {
      EntityList.RemoveDeleted<ClientWorldWaypoint>(list);
    }

    public static void Tick(List<ClientWorldWaypoint> list)
    {
      EntityList.Tick<ClientWorldWaypoint>(list);
    }

    public static void TickGraphic(List<ClientWorldWaypoint> list, float dt)
    {
      EntityList.TickGraphic<ClientWorldWaypoint>(list, dt);
    }

    public static void Draw(List<ClientWorldWaypoint> list)
    {
      EntityList.Draw<ClientWorldWaypoint>(list);
    }

    public static void Pause(List<ClientWorldWaypoint> list, bool pause)
    {
      EntityList.Pause<ClientWorldWaypoint>(list, pause);
    }
  }
}
