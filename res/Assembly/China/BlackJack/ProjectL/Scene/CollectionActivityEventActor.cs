﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.CollectionActivityEventActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.UI;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class CollectionActivityEventActor : Entity
  {
    private CollectionActivityWorld m_collectionActivityWorld;
    private ConfigDataCollectionEventInfo m_collectionEventInfo;
    private ConfigDataCollectionActivityWaypointInfo m_locateWaypointInfo;
    private GenericGraphic m_graphic;
    private Vector2 m_position;
    private int m_direction;
    private bool m_isVisible;
    private WorldEventActorUIController m_uiController;
    private Vector3 m_uiInitScale;
    private float m_graphicInitScale;
    private bool m_isPointerDown;
    private float m_scale;
    private float m_appearCountdown;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityEventActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(
      CollectionActivityWorld world,
      ConfigDataCollectionEventInfo collectionEventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void Tick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void Draw()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(Vector2 p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(
      ConfigDataCollectionActivityWaypointInfo waypointInfo,
      int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanClick(bool canClick)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAppearFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFx(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFx(string name, Vector3 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicEffect(GraphicEffect e, float param1 = 0.0f, float param2 = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGraphicEffect(GraphicEffect e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsVisible()
    {
      return this.m_isVisible;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUISiblingIndex(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnUIPointerDown()
    {
      this.m_isPointerDown = true;
    }

    private void OnUIPointerUp()
    {
      this.m_isPointerDown = false;
    }

    private void OnUIClick()
    {
      this.m_collectionActivityWorld.OnEventActorClick(this);
    }

    public Vector2 Position
    {
      get
      {
        return this.m_position;
      }
    }

    public int Direction
    {
      get
      {
        return this.m_direction;
      }
    }

    public ConfigDataCollectionActivityWaypointInfo LocateWaypointInfo
    {
      get
      {
        return this.m_locateWaypointInfo;
      }
    }

    public ConfigDataCollectionEventInfo EventInfo
    {
      get
      {
        return this.m_collectionEventInfo;
      }
    }
  }
}
