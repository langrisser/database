﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.FastCombatActorInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Battle;
using SLua;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class FastCombatActorInfo
  {
    public BattleActor m_battleActor;
    public int m_heroHpMax;
    public int m_soldierHpMax;
    public int m_beforeHeroHp;
    public int m_beforeSoldierHp;
    public int m_afterHeroHp;
    public int m_afterSoldierHp;
    public int m_heroDamage;
    public int m_soldierDamage;
    public bool m_isReceiveCriticalAttack;
  }
}
