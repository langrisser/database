﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.IGenericGraphicContainer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using FixMath.NET;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public interface IGenericGraphicContainer
  {
    Vector3 GetCameraPosition();

    Vector3 CombatPositionToWorldPosition(Vector2i p, Fix64 z, bool computeZOffset);

    bool IsCulled(Vector2 bmin, Vector2 bmax, bool isCombat);

    bool IsCombatGraphicMirrorX();
  }
}
