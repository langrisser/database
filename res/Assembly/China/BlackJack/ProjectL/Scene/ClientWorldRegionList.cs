﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldRegionList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldRegionList
  {
    public static void RemoveAll(List<ClientWorldRegion> list)
    {
      EntityList.RemoveAll<ClientWorldRegion>(list);
    }

    public static void RemoveDeleted(List<ClientWorldRegion> list)
    {
      EntityList.RemoveDeleted<ClientWorldRegion>(list);
    }

    public static void Tick(List<ClientWorldRegion> list)
    {
      EntityList.Tick<ClientWorldRegion>(list);
    }

    public static void TickGraphic(List<ClientWorldRegion> list, float dt)
    {
      EntityList.TickGraphic<ClientWorldRegion>(list, dt);
    }

    public static void Draw(List<ClientWorldRegion> list)
    {
      EntityList.Draw<ClientWorldRegion>(list);
    }

    public static void Pause(List<ClientWorldRegion> list, bool pause)
    {
      EntityList.Pause<ClientWorldRegion>(list, pause);
    }
  }
}
