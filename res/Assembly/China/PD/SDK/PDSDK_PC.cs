﻿// Decompiled with JetBrains decompiler
// Type: PD.SDK.PDSDK_PC
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace PD.SDK
{
  public class PDSDK_PC
  {
    private static int webViewEnable = 1;
    private static string gameInfo = string.Empty;
    private static string userToken = string.Empty;
    private static string testUserId = string.Empty;
    private static string qqhAppkey = "1448278612076";
    private static string hxsyAppkey = "1480042163929";
    private static string _login_customparams = string.Empty;
    private static string _webviewAction = string.Empty;
    private static string SEPARATOR = Encoding.ASCII.GetString(new byte[1]
    {
      (byte) 1
    });
    private static string INNER_SEPARATOR = Encoding.ASCII.GetString(new byte[1]
    {
      (byte) 2
    });
    private static string mbi_version = "1.0";
    private static string mbi_active = "active.log";
    private static string mbi_gameevent = "gameevent.log";
    private static string mbi_login = "login.log";
    private static string mbi_online = "online.log";
    private static string mbi_start = "start.log";
    private static string mbi_path = Application.dataPath + "/mbi/";
    private static string mbi_zip_path = Application.dataPath + "/mbi_zip/";
    private static long lastSentTicks = DateTime.Now.Ticks;
    private static string config_path = Application.dataPath + "/StreamingAssets/commonProperty";
    private static string config_path_d = Application.dataPath + "/StreamingAssets/commonPropertyD";
    private static List<string> m_EUCpuntryCodeList = new List<string>()
    {
      "AT",
      "BE",
      "BG",
      "CH",
      "CZ",
      "DE",
      "DK",
      "EE",
      "ES",
      "FI",
      "FR",
      "GB",
      "GR",
      "HR",
      "HU",
      "IE",
      "IT",
      "IS",
      "LT",
      "LV",
      "MC",
      "MD",
      "MT",
      "NL",
      "NO",
      "PL",
      "PT",
      "RO",
      "RU",
      "SE",
      "SK",
      "SM",
      "UA",
      "UK",
      "VA",
      "YU"
    };
    private static JsonData commonPropertyJson;
    private static string deviceId;
    private static string _goodsName;
    private static int _goodsNumber;
    private static string _goodsId;
    private static string _goodsRegisterId;
    private static double _goodsPrice;
    private static string _customparams;
    private static string _goodsDes;
    private static string _orderId;
    private const string kSDKLanguage = "kSDKLanguage";
    private const string m_SDKDefaultLanguage = "en";

    public static string SDKLanguage { get; private set; }

    private static string Platform_account_url
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static string LoginUrl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static string UsercenterUrl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static string Platform_webview_url
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static string Platform_pay_forbidden_url
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static string GetProductsListUrl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static string BillingCreateOrderUrl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static string OrderPayUrl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static string OrderPayGamebeansUrl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static string GetNewVersionUrl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        string str1 = string.Empty;
        if (PDSDK_PC.getCommonProperty("app_key", string.Empty) != PDSDK_PC.qqhAppkey)
          str1 = "/" + PDSDK_PC.getCommonProperty("app_key", string.Empty);
        string str2 = "/sdk-service" + str1 + "/version/getNewVersion.json";
        if (PDSDK_PC.getCommonProperty("debug_mode", "0") == "1")
          return PDSDK_PC.getCommonProperty("debug_billing_url", string.Empty) + str2;
        return PDSDK_PC.getCommonProperty("platform_billing_url", string.Empty) + str2;
      }
    }

    private static string WxCreateOrderUrl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static string WxQrcodeUrl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OpenWebBrowser(string url, int w, int h)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void EncryptCommonProperty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Init()
    {
      byte[] bytes = (byte[]) null;
      try
      {
        bytes = File.ReadAllBytes(PDSDK_PC.config_path_d);
      }
      catch (FileNotFoundException ex1)
      {
        try
        {
          bytes = File.ReadAllBytes(PDSDK_PC.config_path);
          for (int index = 0; index <= bytes.Length - 1; ++index)
          {
            if (index % 2 == 0)
              bytes[index] ^= (byte) 100;
            else
              bytes[index] ^= (byte) 55;
          }
        }
        catch (FileNotFoundException ex2)
        {
          bytes = (byte[]) null;
        }
        finally
        {
        }
      }
      finally
      {
      }
      if (bytes == null)
        return;
      PDSDK_PC.commonPropertyJson = JsonMapper.ToObject(Encoding.UTF8.GetString(bytes));
      PDSDK_PC.PrintActiveLog();
      PDSDK.DoCoroutine(PDSDK_PC.GET_NEWVERSION(PDSDK_PC.GetNewVersionUrl), (Action) null);
      JsonData jsonData = new JsonData();
      jsonData["state_code"] = (JsonData) "400";
      jsonData["message"] = (JsonData) "pcinitsuccess";
      jsonData["data"] = (JsonData) string.Empty;
      PDSDK_PC.SDKLanguage = !PlayerPrefs.HasKey("kSDKLanguage") ? "en" : (PlayerPrefs.GetString("kSDKLanguage").Length <= 0 ? "en" : PlayerPrefs.GetString("kSDKLanguage"));
      PDSDK.Instance.onInitSuccess(jsonData.ToJson());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void setCommonProperty(string pKey, string pValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string getCommonProperty(string pKey, string defaultValue = "")
    {
      if (PDSDK_PC.commonPropertyJson != null && ((IDictionary) PDSDK_PC.commonPropertyJson).Contains((object) pKey))
        return (string) PDSDK_PC.commonPropertyJson[pKey];
      return defaultValue;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Login(string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Login()
    {
      PDSDK_PC.Login(PDSDK_PC._login_customparams);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Logout(string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartGame(string gameparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void GetProductsList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DoPay(
      string goodsName,
      int goodsNumber,
      string goodsId,
      string goodsRegisterId,
      double goodsPrice,
      string customparams,
      string goodsDes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void UserCenter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Exit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetDeviceID()
    {
      if (PDSDK_PC.deviceId == null)
        PDSDK_PC.deviceId = "UNITYPC" + PDSDK_PC.INNER_SEPARATOR + SystemInfo.deviceUniqueIdentifier;
      return PDSDK_PC.deviceId;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void WebInvestigation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CallWebView(
      string title,
      int fullscreen_flag,
      int title_flag,
      string action,
      string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void callCustomerServiceWeb()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSysLangue()
    {
      // ISSUE: unable to decompile the method.
    }

    public static string GetSysCountry()
    {
      return "US";
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetSDKLanguage(string languageCode, Action<string> callback = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsEuCountry()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DoOpenFBFanPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetECID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void NoticeCenterState(int mode, string noticeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PrintGameEventLog(string eventID, string remark)
    {
      Debug.Log("PDSDK.PC.PrintGameEventLog ");
      JsonData jsonData = JsonMapper.ToObject(PDSDK_PC.gameInfo);
      string str1 = string.Empty;
      if (((IDictionary) jsonData).Contains((object) "GameUid"))
      {
        str1 = (string) jsonData["GameUid"];
        if (PDSDK_PC.testUserId != string.Empty)
          str1 = PDSDK_PC.testUserId;
      }
      string empty = string.Empty;
      if (((IDictionary) jsonData).Contains((object) "ServerId"))
        empty = (string) jsonData["ServerId"];
      string str2 = DateTime.Now.ToString("yyyyMMddHHmmss");
      string content = string.Empty + PDSDK_PC.getCommonProperty("app_key", string.Empty) + PDSDK_PC.SEPARATOR + PDSDK_PC.mbi_version + PDSDK_PC.SEPARATOR + "win" + PDSDK_PC.SEPARATOR + empty + PDSDK_PC.SEPARATOR + PDSDK_PC.getCommonProperty("cmbi_channel_id", string.Empty) + PDSDK_PC.SEPARATOR + PDSDK_PC.GetDeviceID() + PDSDK_PC.SEPARATOR + str1 + PDSDK_PC.SEPARATOR + "V" + PDSDK_PC.mbi_version + PDSDK_PC.SEPARATOR + str2 + PDSDK_PC.SEPARATOR + eventID + PDSDK_PC.SEPARATOR + "winpc" + PDSDK_PC.SEPARATOR + remark;
      if (PDSDK_PC.getCommonProperty("platform", "Zlongame") != "Zlongame")
        content = content + PDSDK_PC.SEPARATOR + "0";
      PDSDK_PC.PrintLineToFile(PDSDK_PC.mbi_gameevent, content);
      if (DateTime.Now.Ticks <= PDSDK_PC.lastSentTicks + 600000000L)
        return;
      PDSDK_PC.MakeLogZipAndSend();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PrintActiveLog()
    {
      Debug.Log("PDSDK.PC.PrintActiveLog ");
      string str = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
      string content = string.Empty + str + PDSDK_PC.SEPARATOR + PDSDK_PC.GetDeviceID() + PDSDK_PC.SEPARATOR + PDSDK_PC.getCommonProperty("cmbi_channel_id", string.Empty) + PDSDK_PC.SEPARATOR + PDSDK_PC.mbi_version + PDSDK_PC.SEPARATOR + "0" + PDSDK_PC.SEPARATOR + "0" + PDSDK_PC.SEPARATOR + "0" + PDSDK_PC.SEPARATOR + "800*600" + PDSDK_PC.SEPARATOR + "LAN" + PDSDK_PC.SEPARATOR + "0" + PDSDK_PC.SEPARATOR + "1001" + PDSDK_PC.SEPARATOR + PDSDK_PC.mbi_version;
      PDSDK_PC.PrintLineToFile(PDSDK_PC.mbi_active, content);
      PDSDK_PC.MakeLogZipAndSend();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PrintLoginLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PrintOnlineLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PrintStartLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PrintLineToFile(string fileName, string content)
    {
      try
      {
        Debug.Log("PDSDK.PC.PrintLineToFile : " + fileName + " " + content);
        Directory.CreateDirectory(PDSDK_PC.mbi_path);
        FileInfo fileInfo = new FileInfo(PDSDK_PC.mbi_path + fileName);
        StreamWriter streamWriter = fileInfo.Exists ? fileInfo.AppendText() : fileInfo.CreateText();
        streamWriter.WriteLine(content);
        streamWriter.Close();
        streamWriter.Dispose();
      }
      catch (ArgumentException ex)
      {
        Debug.LogError("PDSDK.PC.PrintLineToFile ArgumentException: ");
      }
      catch (IOException ex)
      {
        Debug.LogError("PDSDK.PC.PrintLineToFile IOException: ");
      }
      catch (NullReferenceException ex)
      {
        Debug.LogError("PDSDK.PC.PrintLineToFile NullReferenceException: ");
      }
      finally
      {
        Debug.Log("PDSDK.PC.PrintLineToFile finally: ");
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool MakeLogZipAndSend()
    {
      try
      {
        PDSDK_PC.lastSentTicks = DateTime.Now.Ticks;
        Directory.CreateDirectory(PDSDK_PC.mbi_zip_path);
        if (Directory.Exists(PDSDK_PC.mbi_path))
        {
          string str1 = DateTime.Now.ToString("yyyyMMddHHmm");
          string str2 = "MBI001_" + PDSDK_PC.getCommonProperty("cmbi_app_key", string.Empty) + "_" + str1 + "_v1.4.zip";
          string str3 = PDSDK_PC.mbi_zip_path + str2;
          if (!File.Exists(str3))
          {
            ZipFile zipFile = new ZipFile(str3);
            zipFile.AddDirectory(PDSDK_PC.mbi_path, string.Empty);
            zipFile.Save();
            Directory.Delete(PDSDK_PC.mbi_path, true);
          }
        }
      }
      catch (ArgumentException ex)
      {
        Debug.LogError("PDSDK.PC.MakeLogZipAndSend ArgumentException: ");
      }
      catch (IOException ex)
      {
        Debug.LogError("PDSDK.PC.MakeLogZipAndSend IOException: ");
      }
      catch (NullReferenceException ex)
      {
        Debug.LogError("PDSDK.PC.MakeLogZipAndSend NullReferenceException: ");
      }
      finally
      {
        Debug.Log("PDSDK.PC.MakeLogZipAndSend finally: ");
      }
      PDSDK_PC.SendZipFile();
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SendZipFile()
    {
      try
      {
        int num = 3;
        foreach (string fileSystemEntry in Directory.GetFileSystemEntries(PDSDK_PC.mbi_zip_path))
        {
          if (num <= 0)
            break;
          if (File.Exists(fileSystemEntry) && fileSystemEntry.IndexOf("MBI001") >= 0)
          {
            --num;
            PDSDK.DoCoroutine(PDSDK_PC.POST_SENDZIPFILE(PDSDK_PC.getCommonProperty("platformBIBaseUrl", string.Empty) + "/pd-bi-log-service/UploadFileServlet", fileSystemEntry.Substring(fileSystemEntry.LastIndexOf("/") + 1, fileSystemEntry.Length - fileSystemEntry.LastIndexOf("/") - 1)), (Action) null);
          }
        }
      }
      catch (ArgumentException ex)
      {
        Debug.LogError("PDSDK.PC.SendZipFile ArgumentException: ");
      }
      catch (IOException ex)
      {
        Debug.LogError("PDSDK.PC.SendZipFile IOException: ");
      }
      catch (NullReferenceException ex)
      {
        Debug.LogError("PDSDK.PC.SendZipFile NullReferenceException: ");
      }
      finally
      {
        Debug.Log("PDSDK.PC.SendZipFile finally: ");
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator POST_SENDZIPFILE(string url, string zipFileNameRaw)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new PDSDK_PC.\u003CPOST_SENDZIPFILE\u003Ec__Iterator0()
      {
        url = url,
        zipFileNameRaw = zipFileNameRaw
      };
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void verifyToken(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void onLoginSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void onLoginFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void onLoginCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void onCallWebviewSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void onCallWebviewCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void onPcPaySuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void onPcPayFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void onPcPayCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void onNoticeCenterStateSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void onNoticeCenterStateFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string Encode_DES(string str, string key)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void OnLangueChangeSuccess(string Langue)
    {
    }

    public static void OnLangueChangeFailed(string Langue)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string Encrypt_MD5(string strPwd)
    {
      MD5 md5 = (MD5) new MD5CryptoServiceProvider();
      byte[] bytes = Encoding.Default.GetBytes(strPwd);
      byte[] hash = md5.ComputeHash(bytes);
      md5.Clear();
      string empty = string.Empty;
      for (int index = 0; index < hash.Length; ++index)
        empty += hash[index].ToString("x").PadLeft(2, '0');
      return empty;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator GET_SUPERME(
      string url,
      string data,
      string opcode,
      string channelid)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator GET_WXORDER(string url)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator GET_CREATORDER(string url)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator GET_GOODSLIST(string url)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator GET_NEWVERSION(string url)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new PDSDK_PC.\u003CGET_NEWVERSION\u003Ec__Iterator5()
      {
        url = url
      };
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator GET_NOTICECENTER(string url, string nodeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string decode(string src, byte key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string DefaultCallbackMessage(string code, string message)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static PDSDK_PC()
    {
    }
  }
}
