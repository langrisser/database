﻿// Decompiled with JetBrains decompiler
// Type: SetFontSizeCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Runtime.CompilerServices;

public class SetFontSizeCmd : IDebugCmd
{
  private const string _NAME = "sfs";
  private const string _DESC = "sfs [int]: set the font size of debug console.";
  private const string _SCHEMA = "i";

  [MethodImpl((MethodImplOptions) 32768)]
  public void Execute(string strParams)
  {
    // ISSUE: unable to decompile the method.
  }

  public string GetHelpDesc()
  {
    return "sfs [int]: set the font size of debug console.";
  }

  public string GetName()
  {
    return "sfs";
  }
}
