﻿// Decompiled with JetBrains decompiler
// Type: SLua.UnityExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace SLua
{
  public static class UnityExtension
  {
    public static void StartCoroutine(this MonoBehaviour mb, LuaFunction func)
    {
      mb.StartCoroutine(UnityExtension.LuaCoroutine(func));
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    internal static IEnumerator LuaCoroutine(LuaFunction func)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
