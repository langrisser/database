﻿// Decompiled with JetBrains decompiler
// Type: ReloginCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.UI;

public class ReloginCmd : IDebugCmd
{
  public void Execute(string strParams)
  {
    LoginUITask.Relogin();
  }

  public string GetHelpDesc()
  {
    return "Relogin : 重新登陆.";
  }

  public string GetName()
  {
    return "Relogin";
  }
}
