﻿// Decompiled with JetBrains decompiler
// Type: IJsonWrapper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Collections;
using System.Collections.Specialized;

public interface IJsonWrapper : IList, IOrderedDictionary, ICollection, IEnumerable, IDictionary
{
  bool IsArray { get; }

  bool IsBoolean { get; }

  bool IsDouble { get; }

  bool IsInt { get; }

  bool IsLong { get; }

  bool IsObject { get; }

  bool IsString { get; }

  bool GetBoolean();

  double GetDouble();

  int GetInt();

  JsonType GetJsonType();

  long GetLong();

  string GetString();

  void SetBoolean(bool val);

  void SetDouble(double val);

  void SetInt(int val);

  void SetJsonType(JsonType type);

  void SetLong(long val);

  void SetString(string val);

  string ToJson();

  void ToJson(JsonWriter writer);
}
