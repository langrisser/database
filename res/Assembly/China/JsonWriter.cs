﻿// Decompiled with JetBrains decompiler
// Type: JsonWriter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

public class JsonWriter
{
  private static NumberFormatInfo number_format = NumberFormatInfo.InvariantInfo;
  private WriterContext context;
  private Stack<WriterContext> ctx_stack;
  private bool has_reached_end;
  private char[] hex_seq;
  private int indentation;
  private int indent_value;
  private StringBuilder inst_string_builder;
  private bool pretty_print;
  private bool validate;
  private TextWriter writer;

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonWriter()
  {
    this.inst_string_builder = new StringBuilder();
    this.writer = (TextWriter) new StringWriter(this.inst_string_builder);
    this.Init();
  }

  public JsonWriter(StringBuilder sb)
    : this((TextWriter) new StringWriter(sb))
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonWriter(TextWriter writer)
  {
    if (writer == null)
      throw new ArgumentNullException(nameof (writer));
    this.writer = writer;
    this.Init();
  }

  public int IndentValue
  {
    get
    {
      return this.indent_value;
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public bool PrettyPrint
  {
    get
    {
      return this.pretty_print;
    }
    set
    {
      this.pretty_print = value;
    }
  }

  public TextWriter TextWriter
  {
    get
    {
      return this.writer;
    }
  }

  public bool Validate
  {
    get
    {
      return this.validate;
    }
    set
    {
      this.validate = value;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void DoValidation(Condition cond)
  {
    if (!this.context.ExpectingValue)
      ++this.context.Count;
    if (!this.validate)
      return;
    if (this.has_reached_end)
      throw new JsonException("A complete JSON symbol has already been written");
    switch (cond)
    {
      case Condition.InArray:
        if (this.context.InArray)
          break;
        throw new JsonException("Can't close an array here");
      case Condition.InObject:
        if (this.context.InObject && !this.context.ExpectingValue)
          break;
        throw new JsonException("Can't close an object here");
      case Condition.NotAProperty:
        if (!this.context.InObject || this.context.ExpectingValue)
          break;
        throw new JsonException("Expected a property");
      case Condition.Property:
        if (this.context.InObject && !this.context.ExpectingValue)
          break;
        throw new JsonException("Can't add a property here");
      case Condition.Value:
        if (this.context.InArray || this.context.InObject && this.context.ExpectingValue)
          break;
        throw new JsonException("Can't add a value here");
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Init()
  {
    this.has_reached_end = false;
    this.hex_seq = new char[4];
    this.indentation = 0;
    this.indent_value = 4;
    this.pretty_print = false;
    this.validate = true;
    this.ctx_stack = new Stack<WriterContext>();
    this.context = new WriterContext();
    this.ctx_stack.Push(this.context);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void IntToHex(int n, char[] hex)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Indent()
  {
    if (!this.pretty_print)
      return;
    this.indentation += this.indent_value;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Put(string str)
  {
    if (this.pretty_print && !this.context.ExpectingValue)
    {
      for (int index = 0; index < this.indentation; ++index)
        this.writer.Write(' ');
    }
    this.writer.Write(str);
  }

  private void PutNewline()
  {
    this.PutNewline(true);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void PutNewline(bool add_comma)
  {
    if (add_comma && !this.context.ExpectingValue && this.context.Count > 1)
      this.writer.Write(',');
    if (!this.pretty_print || this.context.ExpectingValue)
      return;
    this.writer.Write('\n');
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void PutString(string str)
  {
    this.Put(string.Empty);
    this.writer.Write('"');
    int length = str.Length;
    for (int index = 0; index < length; ++index)
    {
      char ch = str[index];
      switch (ch)
      {
        case '\b':
          this.writer.Write("\\b");
          break;
        case '\t':
          this.writer.Write("\\t");
          break;
        case '\n':
          this.writer.Write("\\n");
          break;
        case '\f':
          this.writer.Write("\\f");
          break;
        case '\r':
          this.writer.Write("\\r");
          break;
        default:
          if (ch == '"' || ch == '\\')
          {
            this.writer.Write('\\');
            this.writer.Write(str[index]);
            break;
          }
          if (str[index] >= ' ' && str[index] <= '~')
          {
            this.writer.Write(str[index]);
            break;
          }
          JsonWriter.IntToHex((int) str[index], this.hex_seq);
          this.writer.Write("\\u");
          this.writer.Write(this.hex_seq);
          break;
      }
    }
    this.writer.Write('"');
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Unindent()
  {
    if (!this.pretty_print)
      return;
    this.indentation -= this.indent_value;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override string ToString()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Reset()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(bool boolean)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(Decimal number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(double number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(int number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(long number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(string str)
  {
    this.DoValidation(Condition.Value);
    this.PutNewline();
    if (str == null)
      this.Put("null");
    else
      this.PutString(str);
    this.context.ExpectingValue = false;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(ulong number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void WriteArrayEnd()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void WriteArrayStart()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void WriteObjectEnd()
  {
    this.DoValidation(Condition.InObject);
    this.PutNewline(false);
    this.ctx_stack.Pop();
    if (this.ctx_stack.Count == 1)
    {
      this.has_reached_end = true;
    }
    else
    {
      this.context = this.ctx_stack.Peek();
      this.context.ExpectingValue = false;
    }
    this.Unindent();
    this.Put("}");
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void WriteObjectStart()
  {
    this.DoValidation(Condition.NotAProperty);
    this.PutNewline();
    this.Put("{");
    this.context = new WriterContext();
    this.context.InObject = true;
    this.ctx_stack.Push(this.context);
    this.Indent();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void WritePropertyName(string property_name)
  {
    this.DoValidation(Condition.Property);
    this.PutNewline();
    this.PutString(property_name);
    if (this.pretty_print)
    {
      if (property_name.Length > this.context.Padding)
        this.context.Padding = property_name.Length;
      for (int index = this.context.Padding - property_name.Length; index >= 0; --index)
        this.writer.Write(' ');
      this.writer.Write(": ");
    }
    else
      this.writer.Write(':');
    this.context.ExpectingValue = true;
  }
}
