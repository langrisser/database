﻿// Decompiled with JetBrains decompiler
// Type: EnableUserGuide
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.UI;

public class EnableUserGuide : IDebugCmd
{
  public void Execute(string strParams)
  {
    UserGuideUITask.Enable = bool.Parse(strParams);
  }

  public string GetHelpDesc()
  {
    return "eug true : 启用新手引导；eug false : 关闭新手引导；";
  }

  public string GetName()
  {
    return "eug";
  }
}
