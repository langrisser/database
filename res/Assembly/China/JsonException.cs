﻿// Decompiled with JetBrains decompiler
// Type: JsonException
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Runtime.CompilerServices;

public class JsonException : ApplicationException
{
  public JsonException()
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  internal JsonException(ParserToken token)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  internal JsonException(ParserToken token, Exception inner_exception)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  internal JsonException(int c)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  internal JsonException(int c, Exception inner_exception)
  {
    // ISSUE: unable to decompile the method.
  }

  public JsonException(string message)
    : base(message)
  {
  }

  public JsonException(string message, Exception inner_exception)
    : base(message, inner_exception)
  {
  }
}
