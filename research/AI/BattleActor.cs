using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.UtilityTools;
using SLua;
using UnityEngine.Scripting;

// Token: 0x0200118F RID: 4495
[Preserve]
public class Lua_BlackJack_ProjectL_Battle_BattleActor : LuaObject
{
	public static int constructor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor o = new BattleActor();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int Dispose(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.Dispose();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int Initialize(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleTeam team;
			LuaObject.checkType<BattleTeam>(l, 2, out team);
			ConfigDataHeroInfo heroInfo;
			LuaObject.checkType<ConfigDataHeroInfo>(l, 3, out heroInfo);
			ConfigDataJobConnectionInfo jobConnectionInfo;
			LuaObject.checkType<ConfigDataJobConnectionInfo>(l, 4, out jobConnectionInfo);
			ConfigDataSoldierInfo soldierInfo;
			LuaObject.checkType<ConfigDataSoldierInfo>(l, 5, out soldierInfo);
			ConfigDataSkillInfo[] skillInfos;
			LuaObject.checkArray<ConfigDataSkillInfo>(l, 6, out skillInfos);
			ConfigDataJobInfo[] masterJobInfos;
			LuaObject.checkArray<ConfigDataJobInfo>(l, 7, out masterJobInfos);
			BattleActorEquipment[] equipments;
			LuaObject.checkArray<BattleActorEquipment>(l, 8, out equipments);
			ConfigDataSkillInfo[] resonanceSkillInfos;
			LuaObject.checkArray<ConfigDataSkillInfo>(l, 9, out resonanceSkillInfos);
			ConfigDataSkillInfo[] fetterSkillInfos;
			LuaObject.checkArray<ConfigDataSkillInfo>(l, 10, out fetterSkillInfos);
			int heroLevel;
			LuaObject.checkType(l, 11, out heroLevel);
			int heroStar;
			LuaObject.checkType(l, 12, out heroStar);
			int jobLevel;
			LuaObject.checkType(l, 13, out jobLevel);
			int soldierCount;
			LuaObject.checkType(l, 14, out soldierCount);
			GridPosition pos;
			LuaObject.checkValueType<GridPosition>(l, 15, out pos);
			int dir;
			LuaObject.checkType(l, 16, out dir);
			bool isNpc;
			LuaObject.checkType(l, 17, out isNpc);
			int actionValue;
			LuaObject.checkType(l, 18, out actionValue);
			int behaviorId;
			LuaObject.checkType(l, 19, out behaviorId);
			int groupId;
			LuaObject.checkType(l, 20, out groupId);
			BattleActorSourceType sourceType;
			LuaObject.checkEnum<BattleActorSourceType>(l, 21, out sourceType);
			int playerIndex;
			LuaObject.checkType(l, 22, out playerIndex);
			battleActor.Initialize(team, heroInfo, jobConnectionInfo, soldierInfo, skillInfos, masterJobInfos, equipments, resonanceSkillInfos, fetterSkillInfos, heroLevel, heroStar, jobLevel, soldierCount, pos, dir, isNpc, actionValue, behaviorId, groupId, sourceType, playerIndex);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int InitializeSkin(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataCharImageSkinResourceInfo heroCharImageSkinInfo;
			LuaObject.checkType<ConfigDataCharImageSkinResourceInfo>(l, 2, out heroCharImageSkinInfo);
			ConfigDataModelSkinResourceInfo heroModelSkinInfo;
			LuaObject.checkType<ConfigDataModelSkinResourceInfo>(l, 3, out heroModelSkinInfo);
			ConfigDataModelSkinResourceInfo soldierModelSkinInfo;
			LuaObject.checkType<ConfigDataModelSkinResourceInfo>(l, 4, out soldierModelSkinInfo);
			battleActor.InitializeSkin(heroCharImageSkinInfo, heroModelSkinInfo, soldierModelSkinInfo);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int InitializeExtraPassiveSkillAndTalent(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<ConfigDataSkillInfo> skillInfos;
			LuaObject.checkType<List<ConfigDataSkillInfo>>(l, 2, out skillInfos);
			ConfigDataSkillInfo talentSkillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 3, out talentSkillInfo);
			battleActor.InitializeExtraPassiveSkillAndTalent(skillInfos, talentSkillInfo);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int InitializeEnd(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool visible;
			LuaObject.checkType(l, 2, out visible);
			int heroHp;
			LuaObject.checkType(l, 3, out heroHp);
			int soldierHp;
			LuaObject.checkType(l, 4, out soldierHp);
			battleActor.InitializeEnd(visible, heroHp, soldierHp);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int UpdateBattleProperties(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.UpdateBattleProperties();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int UpdateBattlePropertiesInCombat(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor other;
			LuaObject.checkType<BattleActor>(l, 2, out other);
			bool isAttacker;
			LuaObject.checkType(l, 3, out isAttacker);
			int distance;
			LuaObject.checkType(l, 4, out distance);
			battleActor.UpdateBattlePropertiesInCombat(other, isAttacker, distance);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CollectJobMasterPropertyModifiers(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattlePropertyModifier pm;
			LuaObject.checkType<BattlePropertyModifier>(l, 2, out pm);
			battleActor.CollectJobMasterPropertyModifiers(pm);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CollectEquipmentPropertyModifiers(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattlePropertyModifier pm;
			LuaObject.checkType<BattlePropertyModifier>(l, 2, out pm);
			battleActor.CollectEquipmentPropertyModifiers(pm);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetDirection(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int direction;
			LuaObject.checkType(l, 2, out direction);
			battleActor.SetDirection(direction);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FaceTo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition p;
			LuaObject.checkValueType<GridPosition>(l, 2, out p);
			battleActor.FaceTo(p);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GuardMove(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition p;
			LuaObject.checkValueType<GridPosition>(l, 2, out p);
			battleActor.GuardMove(p);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int UnguardMove(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.UnguardMove();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetTerrain(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataTerrainInfo terrain = battleActor.GetTerrain();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, terrain);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetBuffEffectedTerrain(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataTerrainInfo buffEffectedTerrain = battleActor.GetBuffEffectedTerrain();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, buffEffectedTerrain);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ChangeTeam(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int team;
			LuaObject.checkType(l, 2, out team);
			bool isNpc;
			LuaObject.checkType(l, 3, out isNpc);
			battleActor.ChangeTeam(team, isNpc);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CreateBattleCommand(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleCommandType type;
			LuaObject.checkEnum<BattleCommandType>(l, 2, out type);
			BattleCommand o = battleActor.CreateBattleCommand(type);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int StartBattle(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.StartBattle();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int StopBattle(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool win;
			LuaObject.checkType(l, 2, out win);
			battleActor.StopBattle(win);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int NextTurn(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.NextTurn();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ActionBegin(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.ActionBegin();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ActionEnd(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.ActionEnd();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsActionFinished(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsActionFinished();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CanAction(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.CanAction();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindPath(IntPtr l)
	{
		int result;
		try
		{
			int total = LuaDLL.lua_gettop(l);
			if (LuaObject.matchType(l, total, 2, typeof(GridPosition), typeof(GridPosition), typeof(int), typeof(int), typeof(int), typeof(List<GridPosition>)))
			{
				BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
				GridPosition start;
				LuaObject.checkValueType<GridPosition>(l, 2, out start);
				GridPosition goal;
				LuaObject.checkValueType<GridPosition>(l, 3, out goal);
				int movePoint;
				LuaObject.checkType(l, 4, out movePoint);
				int ignoreTeamNumber;
				LuaObject.checkType(l, 5, out ignoreTeamNumber);
				int inRegion;
				LuaObject.checkType(l, 6, out inRegion);
				List<GridPosition> path;
				LuaObject.checkType<List<GridPosition>>(l, 7, out path);
				bool b = battleActor.FindPath(start, goal, movePoint, ignoreTeamNumber, inRegion, path);
				LuaObject.pushValue(l, true);
				LuaObject.pushValue(l, b);
				result = 2;
			}
			else if (LuaObject.matchType(l, total, 2, typeof(GridPosition), typeof(GridPosition), typeof(int), typeof(int), typeof(List<GridPosition>), typeof(bool)))
			{
				BattleActor battleActor2 = (BattleActor)LuaObject.checkSelf(l);
				GridPosition start2;
				LuaObject.checkValueType<GridPosition>(l, 2, out start2);
				GridPosition goal2;
				LuaObject.checkValueType<GridPosition>(l, 3, out goal2);
				int movePoint2;
				LuaObject.checkType(l, 4, out movePoint2);
				int inRegion2;
				LuaObject.checkType(l, 5, out inRegion2);
				List<GridPosition> path2;
				LuaObject.checkType<List<GridPosition>>(l, 6, out path2);
				bool findNearestIfFail;
				LuaObject.checkType(l, 7, out findNearestIfFail);
				bool b2 = battleActor2.FindPath(start2, goal2, movePoint2, inRegion2, path2, findNearestIfFail);
				LuaObject.pushValue(l, true);
				LuaObject.pushValue(l, b2);
				result = 2;
			}
			else
			{
				LuaObject.pushValue(l, false);
				LuaDLL.lua_pushstring(l, "No matched override function FindPath to call");
				result = 2;
			}
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindMoveRegion(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition start;
			LuaObject.checkValueType<GridPosition>(l, 2, out start);
			List<GridPosition> region;
			LuaObject.checkType<List<GridPosition>>(l, 3, out region);
			battleActor.FindMoveRegion(start, region);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int HasExecutedCommandType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleCommandType cmdType;
			LuaObject.checkEnum<BattleCommandType>(l, 2, out cmdType);
			bool b = battleActor.HasExecutedCommandType(cmdType);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ExecuteMoveCommand(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition p;
			LuaObject.checkValueType<GridPosition>(l, 2, out p);
			bool b = battleActor.ExecuteMoveCommand(p);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ExecutePerformMoveCommand(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition p;
			LuaObject.checkValueType<GridPosition>(l, 2, out p);
			bool cameraFollow;
			LuaObject.checkType(l, 3, out cameraFollow);
			bool b = battleActor.ExecutePerformMoveCommand(p, cameraFollow);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ExecuteCombatCommand(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 2, out target);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 3, out skillInfo);
			bool isPerform;
			LuaObject.checkType(l, 4, out isPerform);
			bool b = battleActor.ExecuteCombatCommand(target, skillInfo, isPerform);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ExecuteSkillCommand(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int skillIndex;
			LuaObject.checkType(l, 2, out skillIndex);
			GridPosition p;
			LuaObject.checkValueType<GridPosition>(l, 3, out p);
			GridPosition p2;
			LuaObject.checkValueType<GridPosition>(l, 4, out p2);
			bool b = battleActor.ExecuteSkillCommand(skillIndex, p, p2);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ExecutePerformSkillCommand(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			GridPosition p;
			LuaObject.checkValueType<GridPosition>(l, 3, out p);
			GridPosition p2;
			LuaObject.checkValueType<GridPosition>(l, 4, out p2);
			bool b = battleActor.ExecutePerformSkillCommand(skillInfo, p, p2);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ExecuteDoneCommand(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.ExecuteDoneCommand();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsDead(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsDead();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsRetreat(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsRetreat();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsDeadOrRetreat(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsDeadOrRetreat();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetVisible(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool visible;
			LuaObject.checkType(l, 2, out visible);
			battleActor.SetVisible(visible);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsVisible(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsVisible();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsInvincible(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsInvincible();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsSummoned(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsSummoned();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsNpc(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsNpc();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsAINpc(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsAINpc();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsPlayerNpc(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsPlayerNpc();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsPlayerActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsPlayerActor();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsAIActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsAIActor();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CanBeTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.CanBeTarget();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetHeroHealthPoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int heroHealthPoint;
			LuaObject.checkType(l, 2, out heroHealthPoint);
			battleActor.SetHeroHealthPoint(heroHealthPoint);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetSoldierTotalHealthPoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int soldierTotalHealthPoint;
			LuaObject.checkType(l, 2, out soldierTotalHealthPoint);
			battleActor.SetSoldierTotalHealthPoint(soldierTotalHealthPoint);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CheckDie(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor attacker;
			LuaObject.checkType<BattleActor>(l, 2, out attacker);
			battleActor.CheckDie(attacker);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int Retreat(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int effectType;
			LuaObject.checkType(l, 2, out effectType);
			string fxName;
			LuaObject.checkType(l, 3, out fxName);
			bool notifyListener;
			LuaObject.checkType(l, 4, out notifyListener);
			battleActor.Retreat(effectType, fxName, notifyListener);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetSoldierAttackDistance(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int soldierAttackDistance = battleActor.GetSoldierAttackDistance();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, soldierAttackDistance);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetMaxAttackDistance(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int maxAttackDistance = battleActor.GetMaxAttackDistance();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, maxAttackDistance);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetTalentSkillInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo talentSkillInfo = battleActor.GetTalentSkillInfo();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, talentSkillInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetSkillDistance(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			int skillDistance = battleActor.GetSkillDistance(skillInfo);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, skillDistance);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetMovePoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int movePoint = battleActor.GetMovePoint();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, movePoint);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetMoveType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			MoveType moveType = battleActor.GetMoveType();
			LuaObject.pushValue(l, true);
			LuaObject.pushEnum(l, (int)moveType);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetTotalHealthPoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int totalHealthPoint = battleActor.GetTotalHealthPoint();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, totalHealthPoint);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetTotalHealthPointMax(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int totalHealthPointMax = battleActor.GetTotalHealthPointMax();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, totalHealthPointMax);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetSoldierCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int soldierCount = battleActor.GetSoldierCount();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, soldierCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetLastDamageBySkill(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo lastDamageBySkill;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out lastDamageBySkill);
			battleActor.SetLastDamageBySkill(lastDamageBySkill);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetLastDamageBySkill(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo lastDamageBySkill = battleActor.GetLastDamageBySkill();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, lastDamageBySkill);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetFirstDamageTurn(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int firstDamageTurn;
			LuaObject.checkType(l, 2, out firstDamageTurn);
			battleActor.SetFirstDamageTurn(firstDamageTurn);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetFirstDamageTurn(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int firstDamageTurn = battleActor.GetFirstDamageTurn();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, firstDamageTurn);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetSetisfyCondition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int setisfyCondition;
			LuaObject.checkType(l, 2, out setisfyCondition);
			battleActor.SetSetisfyCondition(setisfyCondition);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsSatisfyCondition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int conditionId;
			LuaObject.checkType(l, 2, out conditionId);
			bool b = battleActor.IsSatisfyCondition(conditionId);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetDeathAnimType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int deathAnimType = battleActor.GetDeathAnimType();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, deathAnimType);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetBeCriticalAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool beCriticalAttack;
			LuaObject.checkType(l, 2, out beCriticalAttack);
			battleActor.SetBeCriticalAttack(beCriticalAttack);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetBeKillAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool beKillAttack;
			LuaObject.checkType(l, 2, out beKillAttack);
			battleActor.SetBeKillAttack(beKillAttack);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetActionCriticalAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.SetActionCriticalAttack();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetActionKillActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.SetActionKillActor();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetActionDamageActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.SetActionDamageActor();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsExtraAction(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsExtraAction();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsExtraActionMoving(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.IsExtraActionMoving();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetActionCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			short actionCount = battleActor.GetActionCount();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, actionCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IncreaseCombatAttackCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.IncreaseCombatAttackCount();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetCombatAttackCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			short combatAttackCount = battleActor.GetCombatAttackCount();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, combatAttackCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IncreaseBeCombatAttackCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.IncreaseBeCombatAttackCount();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetBeCombatAttackCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			short beCombatAttackCount = battleActor.GetBeCombatAttackCount();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, beCombatAttackCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IncreaseUseSkillCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.IncreaseUseSkillCount();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetUseSkillCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			short useSkillCount = battleActor.GetUseSkillCount();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, useSkillCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IncreaseKillActorCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.IncreaseKillActorCount();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetKillActorCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int killActorCount = battleActor.GetKillActorCount();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, killActorCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetKillerActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor killerActor = battleActor.GetKillerActor();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, killerActor);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetDieTurn(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int dieTurn = battleActor.GetDieTurn();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, dieTurn);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetSourceType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActorSourceType sourceType = battleActor.GetSourceType();
			LuaObject.pushValue(l, true);
			LuaObject.pushEnum(l, (int)sourceType);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IncreaseBeAttackedCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.IncreaseBeAttackedCount();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetBehaviorId(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int behaviorId = battleActor.GetBehaviorId();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, behaviorId);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetBehavior(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int behavior;
			LuaObject.checkType(l, 2, out behavior);
			battleActor.SetBehavior(behavior);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CheckBehaviorCondition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BehaviorCondition condition;
			LuaObject.checkEnum<BehaviorCondition>(l, 2, out condition);
			ConfigDataBehavior.ParamData param;
			LuaObject.checkType<ConfigDataBehavior.ParamData>(l, 3, out param);
			bool b = battleActor.CheckBehaviorCondition(condition, param);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindEmptyGridInCanAttackAndTouchRange(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition startPoint;
			LuaObject.checkValueType<GridPosition>(l, 2, out startPoint);
			int attackDistance;
			LuaObject.checkType(l, 3, out attackDistance);
			int shape;
			LuaObject.checkType(l, 4, out shape);
			GridPosition gridPosition = battleActor.FindEmptyGridInCanAttackAndTouchRange(startPoint, attackDistance, shape);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, gridPosition);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindActorsInCanAttackAndTouchRange(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> destActors;
			LuaObject.checkType<List<BattleActor>>(l, 2, out destActors);
			GridPosition startPoint;
			LuaObject.checkValueType<GridPosition>(l, 3, out startPoint);
			int attackDistance;
			LuaObject.checkType(l, 4, out attackDistance);
			int shape;
			LuaObject.checkType(l, 5, out shape);
			bool excludeSelf;
			LuaObject.checkType(l, 6, out excludeSelf);
			List<BattleActor> o = battleActor.FindActorsInCanAttackAndTouchRange(destActors, startPoint, attackDistance, shape, excludeSelf);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GenerateAIBattleCommand(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.GenerateAIBattleCommand();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetBuffStates(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BuffState> buffStates = battleActor.GetBuffStates();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, buffStates);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AttachBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataBuffInfo buffInfo;
			LuaObject.checkType<ConfigDataBuffInfo>(l, 2, out buffInfo);
			BattleActor applyer;
			LuaObject.checkType<BattleActor>(l, 3, out applyer);
			BuffSourceType sourceType;
			LuaObject.checkEnum<BuffSourceType>(l, 4, out sourceType);
			ConfigDataSkillInfo sourceSkillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 5, out sourceSkillInfo);
			BuffState sourceBuffState;
			LuaObject.checkType<BuffState>(l, 6, out sourceBuffState);
			bool b = battleActor.AttachBuff(buffInfo, applyer, sourceType, sourceSkillInfo, sourceBuffState);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int RemoveBuffList(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<int> buffIds;
			LuaObject.checkType<List<int>>(l, 2, out buffIds);
			int i = battleActor.RemoveBuffList(buffIds);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, i);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SkillDispelBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			battleActor.SkillDispelBuff(skillInfo);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CollectBuffPropertyModifiersAndFightTags(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattlePropertyModifier pm;
			LuaObject.checkType<BattlePropertyModifier>(l, 2, out pm);
			uint o;
			LuaObject.checkType(l, 3, out o);
			bool collectStatic;
			LuaObject.checkType(l, 4, out collectStatic);
			bool collectDynamic;
			LuaObject.checkType(l, 5, out collectDynamic);
			battleActor.CollectBuffPropertyModifiersAndFightTags(pm, ref o, collectStatic, collectDynamic);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CollectBuffPropertyExchange(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleProperty battleProperty;
			LuaObject.checkType<BattleProperty>(l, 2, out battleProperty);
			BattlePropertyModifier pm;
			LuaObject.checkType<BattlePropertyModifier>(l, 3, out pm);
			battleActor.CollectBuffPropertyExchange(battleProperty, pm);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CollectBuffPropertyReplace(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.CollectBuffPropertyReplace();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int UpdateAllAuras(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.UpdateAllAuras();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int HasFightTag(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			FightTag ft;
			LuaObject.checkEnum<FightTag>(l, 2, out ft);
			bool b = battleActor.HasFightTag(ft);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int HasBuffType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BuffType bt;
			LuaObject.checkEnum<BuffType>(l, 2, out bt);
			bool b = battleActor.HasBuffType(bt);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsBuffEffective(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BuffState bs;
			LuaObject.checkType<BuffState>(l, 2, out bs);
			bool b = battleActor.IsBuffEffective(bs);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ComputeBuffReboundDamage(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			int damage;
			LuaObject.checkType(l, 3, out damage);
			int i = battleActor.ComputeBuffReboundDamage(skillInfo, damage);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, i);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetBuffOverrideMovePointCost(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int buffOverrideMovePointCost = battleActor.GetBuffOverrideMovePointCost();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, buffOverrideMovePointCost);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetBuffOverrideTerrain(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataTerrainInfo buffOverrideTerrain = battleActor.GetBuffOverrideTerrain();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, buffOverrideTerrain);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AddBuffArmyRelationAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int targetArmyId;
			LuaObject.checkType(l, 2, out targetArmyId);
			bool targetIsHero;
			LuaObject.checkType(l, 3, out targetIsHero);
			ArmyRelationData armyRelationData;
			LuaObject.checkValueType<ArmyRelationData>(l, 4, out armyRelationData);
			battleActor.AddBuffArmyRelationAttack(targetArmyId, targetIsHero, ref armyRelationData);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, armyRelationData);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AddBuffArmyRelationDefend(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int targetArmyId;
			LuaObject.checkType(l, 2, out targetArmyId);
			bool targetIsHero;
			LuaObject.checkType(l, 3, out targetIsHero);
			ArmyRelationData armyRelationData;
			LuaObject.checkValueType<ArmyRelationData>(l, 4, out armyRelationData);
			battleActor.AddBuffArmyRelationDefend(targetArmyId, targetIsHero, ref armyRelationData);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, armyRelationData);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AddBuffArmyRelationModifyAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int restrainValue;
			LuaObject.checkType(l, 2, out restrainValue);
			bool targetIsHero;
			LuaObject.checkType(l, 3, out targetIsHero);
			ArmyRelationData armyRelationData;
			LuaObject.checkValueType<ArmyRelationData>(l, 4, out armyRelationData);
			battleActor.AddBuffArmyRelationModifyAttack(restrainValue, targetIsHero, ref armyRelationData);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, armyRelationData);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AddBuffArmyRelationModifyDefend(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int restrainValue;
			LuaObject.checkType(l, 2, out restrainValue);
			bool targetIsHero;
			LuaObject.checkType(l, 3, out targetIsHero);
			ArmyRelationData armyRelationData;
			LuaObject.checkValueType<ArmyRelationData>(l, 4, out armyRelationData);
			battleActor.AddBuffArmyRelationModifyDefend(restrainValue, targetIsHero, ref armyRelationData);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, armyRelationData);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsBuffForceMagicDamage(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool attackerIsHero;
			LuaObject.checkType(l, 2, out attackerIsHero);
			bool b = battleActor.IsBuffForceMagicDamage(attackerIsHero);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ActionEndBuffDoubleMove(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isKill;
			LuaObject.checkType(l, 2, out isKill);
			bool isCritical;
			LuaObject.checkType(l, 3, out isCritical);
			battleActor.ActionEndBuffDoubleMove(isKill, isCritical);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CombatBuffHeal(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 2, out target);
			bool beforeCombat;
			LuaObject.checkType(l, 3, out beforeCombat);
			bool isAttacker;
			LuaObject.checkType(l, 4, out isAttacker);
			int distance;
			LuaObject.checkType(l, 5, out distance);
			bool isCritical;
			LuaObject.checkType(l, 6, out isCritical);
			int heroDamage;
			LuaObject.checkType(l, 7, out heroDamage);
			int soldierDamage;
			LuaObject.checkType(l, 8, out soldierDamage);
			battleActor.CombatBuffHeal(target, beforeCombat, isAttacker, distance, isCritical, heroDamage, soldierDamage);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CombatBuffHealOther(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 2, out target);
			bool beforeCombat;
			LuaObject.checkType(l, 3, out beforeCombat);
			bool isAttacker;
			LuaObject.checkType(l, 4, out isAttacker);
			int soldierDamage;
			LuaObject.checkType(l, 5, out soldierDamage);
			bool isCritical;
			LuaObject.checkType(l, 6, out isCritical);
			int distance;
			LuaObject.checkType(l, 7, out distance);
			int heroDamage;
			LuaObject.checkType(l, 8, out heroDamage);
			battleActor.CombatBuffHealOther(target, beforeCombat, isAttacker, soldierDamage, isCritical, distance, heroDamage);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CombatBuffDamage(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 2, out target);
			bool beforeCombat;
			LuaObject.checkType(l, 3, out beforeCombat);
			bool isAttacker;
			LuaObject.checkType(l, 4, out isAttacker);
			int distance;
			LuaObject.checkType(l, 5, out distance);
			bool isCritical;
			LuaObject.checkType(l, 6, out isCritical);
			battleActor.CombatBuffDamage(target, beforeCombat, isAttacker, distance, isCritical);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CombatApplyBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 2, out target);
			bool beforeCombat;
			LuaObject.checkType(l, 3, out beforeCombat);
			bool isAttacker;
			LuaObject.checkType(l, 4, out isAttacker);
			int distance;
			LuaObject.checkType(l, 5, out distance);
			bool isCritical;
			LuaObject.checkType(l, 6, out isCritical);
			battleActor.CombatApplyBuff(target, beforeCombat, isAttacker, distance, isCritical);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AttackApplyBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> targets;
			LuaObject.checkType<List<BattleActor>>(l, 2, out targets);
			battleActor.AttackApplyBuff(targets);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AttackBuffDamage(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> targets;
			LuaObject.checkType<List<BattleActor>>(l, 2, out targets);
			bool beforeCombat;
			LuaObject.checkType(l, 3, out beforeCombat);
			bool isAttacker;
			LuaObject.checkType(l, 4, out isAttacker);
			int combatDistance;
			LuaObject.checkType(l, 5, out combatDistance);
			battleActor.AttackBuffDamage(targets, beforeCombat, isAttacker, combatDistance);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AttackRemoveBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> targets;
			LuaObject.checkType<List<BattleActor>>(l, 2, out targets);
			bool beforeCombat;
			LuaObject.checkType(l, 3, out beforeCombat);
			bool isAttacker;
			LuaObject.checkType(l, 4, out isAttacker);
			int combatDistance;
			LuaObject.checkType(l, 5, out combatDistance);
			battleActor.AttackRemoveBuff(targets, beforeCombat, isAttacker, combatDistance);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AttackRemoveSkillCooldown(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			bool isCritical;
			LuaObject.checkType(l, 3, out isCritical);
			bool isKill;
			LuaObject.checkType(l, 4, out isKill);
			battleActor.AttackRemoveSkillCooldown(skillInfo, isCritical, isKill);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int BattleFieldSkillApplyBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> targets;
			LuaObject.checkType<List<BattleActor>>(l, 2, out targets);
			battleActor.BattleFieldSkillApplyBuff(targets);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AttackBuffPunch(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 2, out target);
			battleActor.AttackBuffPunch(target);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AttackBuffDrag(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 2, out target);
			battleActor.AttackBuffDrag(target);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AttackBuffExchangePosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 2, out target);
			bool isTargetGurad;
			LuaObject.checkType(l, 3, out isTargetGurad);
			battleActor.AttackBuffExchangePosition(target, isTargetGurad);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CombatArmyChange(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 2, out target);
			battleActor.CombatArmyChange(target);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int BuffDoubleAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BuffState o = battleActor.BuffDoubleAttack();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int BuffUndead(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.BuffUndead();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetGuardActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			BattleActor guardActor = battleActor.GetGuardActor(skillInfo);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, guardActor);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AfterCombatDetachBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.AfterCombatDetachBuff();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetSkillState(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int index;
			LuaObject.checkType(l, 2, out index);
			BattleSkillState skillState = battleActor.GetSkillState(index);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, skillState);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetSkillStateById(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int skillID;
			LuaObject.checkType(l, 2, out skillID);
			BattleSkillState skillStateById = battleActor.GetSkillStateById(skillID);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, skillStateById);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetSkillStates(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleSkillState> skillStates = battleActor.GetSkillStates();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, skillStates);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CanUseSkillOnTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 3, out target);
			bool b = battleActor.CanUseSkillOnTarget(skillInfo, target);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsBattlefiledSkillApplyTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 3, out target);
			bool b = battleActor.IsBattlefiledSkillApplyTarget(skillInfo, target);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CombatBy(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor attacker;
			LuaObject.checkType<BattleActor>(l, 2, out attacker);
			battleActor.CombatBy(attacker);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetHeroAttackSkillInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			MoveType targetMoveType;
			LuaObject.checkEnum<MoveType>(l, 2, out targetMoveType);
			int distance;
			LuaObject.checkType(l, 3, out distance);
			ConfigDataSkillInfo heroAttackSkillInfo = battleActor.GetHeroAttackSkillInfo(targetMoveType, distance);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, heroAttackSkillInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetSoldierAttackSkillInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			MoveType targetMoveType;
			LuaObject.checkEnum<MoveType>(l, 2, out targetMoveType);
			int distance;
			LuaObject.checkType(l, 3, out distance);
			ConfigDataSkillInfo soldierAttackSkillInfo = battleActor.GetSoldierAttackSkillInfo(targetMoveType, distance);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, soldierAttackSkillInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ComputeMoveType_s(IntPtr l)
	{
		int result;
		try
		{
			ConfigDataJobInfo heroJobInfo;
			LuaObject.checkType<ConfigDataJobInfo>(l, 1, out heroJobInfo);
			ConfigDataSoldierInfo soldierInfo;
			LuaObject.checkType<ConfigDataSoldierInfo>(l, 2, out soldierInfo);
			MoveType e = BattleActor.ComputeMoveType(heroJobInfo, soldierInfo);
			LuaObject.pushValue(l, true);
			LuaObject.pushEnum(l, (int)e);
			result = 2;
		}
		catch (Exception e2)
		{
			result = LuaObject.error(l, e2);
		}
		return result;
	}

	public static int ComputeDefaultMoveType_s(IntPtr l)
	{
		int result;
		try
		{
			ConfigDataHeroInfo heroInfo;
			LuaObject.checkType<ConfigDataHeroInfo>(l, 1, out heroInfo);
			MoveType e = BattleActor.ComputeDefaultMoveType(heroInfo);
			LuaObject.pushValue(l, true);
			LuaObject.pushEnum(l, (int)e);
			result = 2;
		}
		catch (Exception e2)
		{
			result = LuaObject.error(l, e2);
		}
		return result;
	}

	public static int SelectMinHPPercentActor_s(IntPtr l)
	{
		int result;
		try
		{
			List<BattleActor> actors;
			LuaObject.checkType<List<BattleActor>>(l, 1, out actors);
			BattleActor o = BattleActor.SelectMinHPPercentActor(actors);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindActorsAlive_s(IntPtr l)
	{
		int result;
		try
		{
			List<BattleActor> actors;
			LuaObject.checkType<List<BattleActor>>(l, 1, out actors);
			List<BattleActor> o = BattleActor.FindActorsAlive(actors);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ComputeBattleProperties(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattlePropertyModifier pm;
			LuaObject.checkType<BattlePropertyModifier>(l, 2, out pm);
			battleActor.m_luaExportHelper.ComputeBattleProperties(pm);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetPosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition position;
			LuaObject.checkValueType<GridPosition>(l, 2, out position);
			battleActor.m_luaExportHelper.SetPosition(position);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int MoveTo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition p;
			LuaObject.checkValueType<GridPosition>(l, 2, out p);
			battleActor.m_luaExportHelper.MoveTo(p);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ClearMapActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.ClearMapActor();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int PostActionTerrainDamage(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.PostActionTerrainDamage();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ShouldLog(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.m_luaExportHelper.ShouldLog();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AddExecutedCommandType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleCommandType cmdType;
			LuaObject.checkEnum<BattleCommandType>(l, 2, out cmdType);
			battleActor.m_luaExportHelper.AddExecutedCommandType(cmdType);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsExecutedCommandType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleCommandType cmdType;
			LuaObject.checkEnum<BattleCommandType>(l, 2, out cmdType);
			bool b = battleActor.m_luaExportHelper.IsExecutedCommandType(cmdType);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindMoveAndAttackRegion(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int distance;
			LuaObject.checkType(l, 2, out distance);
			int shape;
			LuaObject.checkType(l, 3, out shape);
			battleActor.m_luaExportHelper.FindMoveAndAttackRegion(distance, shape);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindRandomEmptyPosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int attackDistance;
			LuaObject.checkType(l, 2, out attackDistance);
			int shape;
			LuaObject.checkType(l, 3, out shape);
			GridPosition gridPosition;
			LuaObject.checkValueType<GridPosition>(l, 4, out gridPosition);
			bool b = battleActor.m_luaExportHelper.FindRandomEmptyPosition(attackDistance, shape, ref gridPosition);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			LuaObject.pushValue(l, gridPosition);
			result = 3;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindAttackPosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int attackDistance;
			LuaObject.checkType(l, 2, out attackDistance);
			int shape;
			LuaObject.checkType(l, 3, out shape);
			GridPosition targetPos;
			LuaObject.checkValueType<GridPosition>(l, 4, out targetPos);
			bool checkMoveRegion;
			LuaObject.checkType(l, 5, out checkMoveRegion);
			bool farAway;
			LuaObject.checkType(l, 6, out farAway);
			GridPosition gridPosition = battleActor.m_luaExportHelper.FindAttackPosition(attackDistance, shape, targetPos, checkMoveRegion, farAway);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, gridPosition);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ComputeActorScoreBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor a;
			LuaObject.checkType<BattleActor>(l, 2, out a);
			int param;
			LuaObject.checkType(l, 3, out param);
			int i = battleActor.m_luaExportHelper.ComputeActorScoreBuff(a, param);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, i);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SelectNearestTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleTeam team;
			LuaObject.checkType<BattleTeam>(l, 2, out team);
			BattleActor o = battleActor.m_luaExportHelper.SelectNearestTarget(team);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetAIRandomNumber(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			RandomNumber airandomNumber = battleActor.m_luaExportHelper.GetAIRandomNumber();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, airandomNumber);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetSkillTargetTeam(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			BattleTeam skillTargetTeam = battleActor.m_luaExportHelper.GetSkillTargetTeam(skillInfo);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, skillTargetTeam);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CanAttackOrUseSkill(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.m_luaExportHelper.CanAttackOrUseSkill();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetBehaviorState(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor.BehaviorState behaviorState;
			LuaObject.checkEnum<BattleActor.BehaviorState>(l, 2, out behaviorState);
			battleActor.m_luaExportHelper.SetBehaviorState(behaviorState);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int DoBehaviorChangeRules(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.DoBehaviorChangeRules();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindFarthestPosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<GridPosition> positions;
			LuaObject.checkType<List<GridPosition>>(l, 2, out positions);
			GridPosition startPos;
			LuaObject.checkValueType<GridPosition>(l, 3, out startPos);
			GridPosition gridPosition = battleActor.m_luaExportHelper.FindFarthestPosition(positions, startPos);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, gridPosition);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindNearestPosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<GridPosition> positions;
			LuaObject.checkType<List<GridPosition>>(l, 2, out positions);
			GridPosition startPos;
			LuaObject.checkValueType<GridPosition>(l, 3, out startPos);
			GridPosition gridPosition = battleActor.m_luaExportHelper.FindNearestPosition(positions, startPos);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, gridPosition);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindNearestActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> actors;
			LuaObject.checkType<List<BattleActor>>(l, 2, out actors);
			GridPosition startPos;
			LuaObject.checkValueType<GridPosition>(l, 3, out startPos);
			BattleActor o = battleActor.m_luaExportHelper.FindNearestActor(actors, startPos);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int DoSelectTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			SelectTarget st;
			LuaObject.checkEnum<SelectTarget>(l, 2, out st);
			ConfigDataBehavior.ParamData param;
			LuaObject.checkType<ConfigDataBehavior.ParamData>(l, 3, out param);
			BehaviorTarget o = battleActor.m_luaExportHelper.DoSelectTarget(st, param);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindActorsByIDFilter(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> actors;
			LuaObject.checkType<List<BattleActor>>(l, 2, out actors);
			int[] priorIDs;
			LuaObject.checkArray<int>(l, 3, out priorIDs);
			int[] excludeIDs;
			LuaObject.checkArray<int>(l, 4, out excludeIDs);
			List<BattleActor> o = battleActor.m_luaExportHelper.FindActorsByIDFilter(actors, priorIDs, excludeIDs);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SelectMoveTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.SelectMoveTarget();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GenerateCommandOfMove(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition target;
			LuaObject.checkValueType<GridPosition>(l, 2, out target);
			battleActor.m_luaExportHelper.GenerateCommandOfMove(target);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindPositionToMoveToTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition target;
			LuaObject.checkValueType<GridPosition>(l, 2, out target);
			GridPosition gridPosition = battleActor.m_luaExportHelper.FindPositionToMoveToTarget(target);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, gridPosition);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ComputeRestrictScore(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor srcActor;
			LuaObject.checkType<BattleActor>(l, 2, out srcActor);
			BattleActor destActor;
			LuaObject.checkType<BattleActor>(l, 3, out destActor);
			int i = battleActor.m_luaExportHelper.ComputeRestrictScore(srcActor, destActor);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, i);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GetArmyRistrictScore(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ArmyTag a;
			LuaObject.checkEnum<ArmyTag>(l, 2, out a);
			ArmyTag b;
			LuaObject.checkEnum<ArmyTag>(l, 3, out b);
			int armyRistrictScore = battleActor.m_luaExportHelper.GetArmyRistrictScore(a, b);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, armyRistrictScore);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int DefaultSelectDamageSkillTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> actors;
			LuaObject.checkType<List<BattleActor>>(l, 2, out actors);
			BattleActor o = battleActor.m_luaExportHelper.DefaultSelectDamageSkillTarget(actors);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsSelectRangeSkill(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.m_luaExportHelper.IsSelectRangeSkill();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindActorsInGrids(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> actors;
			LuaObject.checkType<List<BattleActor>>(l, 2, out actors);
			List<GridPosition> grids;
			LuaObject.checkType<List<GridPosition>>(l, 3, out grids);
			List<BattleActor> o = battleActor.m_luaExportHelper.FindActorsInGrids(actors, grids);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindMaxAoeSkillCoverPosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo si;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out si);
			List<GridPosition> asCenterPositions;
			LuaObject.checkType<List<GridPosition>>(l, 3, out asCenterPositions);
			List<BattleActor> beCoveredActors;
			LuaObject.checkType<List<BattleActor>>(l, 4, out beCoveredActors);
			ClassValue<int> maxCoverActorsCount;
			LuaObject.checkType<ClassValue<int>>(l, 5, out maxCoverActorsCount);
			GridPosition gridPosition = battleActor.m_luaExportHelper.FindMaxAoeSkillCoverPosition(si, asCenterPositions, beCoveredActors, maxCoverActorsCount);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, gridPosition);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindMaxAoeSkillCoverActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo si;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out si);
			List<BattleActor> asCenterActors;
			LuaObject.checkType<List<BattleActor>>(l, 3, out asCenterActors);
			List<BattleActor> beCoveredActors;
			LuaObject.checkType<List<BattleActor>>(l, 4, out beCoveredActors);
			ClassValue<int> maxCoverActorsCount;
			LuaObject.checkType<ClassValue<int>>(l, 5, out maxCoverActorsCount);
			BattleActor o = battleActor.m_luaExportHelper.FindMaxAoeSkillCoverActor(si, asCenterActors, beCoveredActors, maxCoverActorsCount);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int DefaultSelectAttackTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.DefaultSelectAttackTarget();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindGridsLessEqualDistance(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition startPos;
			LuaObject.checkValueType<GridPosition>(l, 2, out startPos);
			int dist;
			LuaObject.checkType(l, 3, out dist);
			List<GridPosition> o = battleActor.m_luaExportHelper.FindGridsLessEqualDistance(startPos, dist);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindActorsLessEqualDistance(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> actors;
			LuaObject.checkType<List<BattleActor>>(l, 2, out actors);
			GridPosition startPos;
			LuaObject.checkValueType<GridPosition>(l, 3, out startPos);
			int dist;
			LuaObject.checkType(l, 4, out dist);
			List<BattleActor> o = battleActor.m_luaExportHelper.FindActorsLessEqualDistance(actors, startPos, dist);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int DefaultSelectHealSkillTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> actors;
			LuaObject.checkType<List<BattleActor>>(l, 2, out actors);
			BattleActor o = battleActor.m_luaExportHelper.DefaultSelectHealSkillTarget(actors);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int DefaultSelectBuffSkillTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> actors;
			LuaObject.checkType<List<BattleActor>>(l, 2, out actors);
			ConfigDataSkillInfo skill;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 3, out skill);
			BattleActor o = battleActor.m_luaExportHelper.DefaultSelectBuffSkillTarget(actors, skill);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SelectAttackTargetInSkillRange(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.SelectAttackTargetInSkillRange();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindActorsWithBuffN(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> actors;
			LuaObject.checkType<List<BattleActor>>(l, 2, out actors);
			int buffID;
			LuaObject.checkType(l, 3, out buffID);
			List<BattleActor> o = battleActor.m_luaExportHelper.FindActorsWithBuffN(actors, buffID);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SelectAttackTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.SelectAttackTarget();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SelectSkill(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.SelectSkill();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SelectSkillDirectReachTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.SelectSkillDirectReachTarget();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int DefaultSelectSkill(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int[] includeSkillIDs;
			LuaObject.checkArray<int>(l, 2, out includeSkillIDs);
			int[] excludeSkillIDs;
			LuaObject.checkArray<int>(l, 3, out excludeSkillIDs);
			battleActor.m_luaExportHelper.DefaultSelectSkill(includeSkillIDs, excludeSkillIDs);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindActorsInCanNormalAttackAndTouchRange(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleActor> destActors;
			LuaObject.checkType<List<BattleActor>>(l, 2, out destActors);
			List<BattleActor> srcActors;
			LuaObject.checkType<List<BattleActor>>(l, 3, out srcActors);
			List<BattleActor> o = battleActor.m_luaExportHelper.FindActorsInCanNormalAttackAndTouchRange(destActors, srcActors);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsSkillAGoodAISelection(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo si;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out si);
			bool b = battleActor.m_luaExportHelper.IsSkillAGoodAISelection(si);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindCastSkillPosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo si;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out si);
			BehaviorTarget t;
			LuaObject.checkType<BehaviorTarget>(l, 3, out t);
			GridPosition gridPosition = battleActor.m_luaExportHelper.FindCastSkillPosition(si, t);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, gridPosition);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindAttackPositions(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int attackDistance;
			LuaObject.checkType(l, 2, out attackDistance);
			int shape;
			LuaObject.checkType(l, 3, out shape);
			GridPosition targetPos;
			LuaObject.checkValueType<GridPosition>(l, 4, out targetPos);
			List<GridPosition> positions;
			LuaObject.checkType<List<GridPosition>>(l, 5, out positions);
			battleActor.m_luaExportHelper.FindAttackPositions(attackDistance, shape, targetPos, positions);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int GenerateCommandOfAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.GenerateCommandOfAttack();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int DoBehaviorMove(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.DoBehaviorMove();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsAttackTargetStillValid(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool b = battleActor.m_luaExportHelper.IsAttackTargetStillValid();
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int DoBehaviorAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.DoBehaviorAttack();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AICreateBattleCommand(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleCommandType type;
			LuaObject.checkEnum<BattleCommandType>(l, 2, out type);
			BattleCommand o = battleActor.m_luaExportHelper.AICreateBattleCommand(type);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SelectAttackRegionTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleTeam team;
			LuaObject.checkType<BattleTeam>(l, 2, out team);
			BattleActor.ComputeActorScoreFunc computeScore;
			LuaObject.checkDelegate<BattleActor.ComputeActorScoreFunc>(l, 3, out computeScore);
			int computeScoreParam;
			LuaObject.checkType(l, 4, out computeScoreParam);
			BattleActor o = battleActor.m_luaExportHelper.SelectAttackRegionTarget(team, computeScore, computeScoreParam);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int InitializeBuffs(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.InitializeBuffs();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AttachPassiveSkillBuffs(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			BuffSourceType sourceType;
			LuaObject.checkEnum<BuffSourceType>(l, 3, out sourceType);
			battleActor.m_luaExportHelper.AttachPassiveSkillBuffs(skillInfo, sourceType);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int RemoveBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BuffState bs;
			LuaObject.checkType<BuffState>(l, 2, out bs);
			bool b = battleActor.m_luaExportHelper.RemoveBuff(bs);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int buffId;
			LuaObject.checkType(l, 2, out buffId);
			BattleActor applyer;
			LuaObject.checkType<BattleActor>(l, 3, out applyer);
			BuffState o = battleActor.m_luaExportHelper.FindBuff(buffId, applyer);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int RemoveAllBuffs(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.RemoveAllBuffs();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int HasBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int buffId;
			LuaObject.checkType(l, 2, out buffId);
			bool b = battleActor.m_luaExportHelper.HasBuff(buffId);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CollectPropertyModifier(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattlePropertyModifier pm;
			LuaObject.checkType<BattlePropertyModifier>(l, 2, out pm);
			PropertyModifyType modifyType;
			LuaObject.checkEnum<PropertyModifyType>(l, 3, out modifyType);
			int value;
			LuaObject.checkType(l, 4, out value);
			bool collectStatic;
			LuaObject.checkType(l, 5, out collectStatic);
			bool collectDynamic;
			LuaObject.checkType(l, 6, out collectDynamic);
			battleActor.m_luaExportHelper.CollectPropertyModifier(pm, modifyType, value, collectStatic, collectDynamic);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CollectOtherActorBuffPropertyModifiersAndFightTags(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattlePropertyModifier pm;
			LuaObject.checkType<BattlePropertyModifier>(l, 2, out pm);
			uint o;
			LuaObject.checkType(l, 3, out o);
			battleActor.m_luaExportHelper.CollectOtherActorBuffPropertyModifiersAndFightTags(pm, ref o);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CollectBuffPropertyModifiersAndFightTagsInCombat(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattlePropertyModifier pm;
			LuaObject.checkType<BattlePropertyModifier>(l, 2, out pm);
			uint o;
			LuaObject.checkType(l, 3, out o);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 4, out target);
			bool isAttacker;
			LuaObject.checkType(l, 5, out isAttacker);
			int distance;
			LuaObject.checkType(l, 6, out distance);
			battleActor.m_luaExportHelper.CollectBuffPropertyModifiersAndFightTagsInCombat(pm, ref o, target, isAttacker, distance);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, o);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int RemoveAuraAppliedBuffs(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataBuffInfo auraBuffInfo;
			LuaObject.checkType<ConfigDataBuffInfo>(l, 2, out auraBuffInfo);
			battleActor.m_luaExportHelper.RemoveAuraAppliedBuffs(auraBuffInfo);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int RemovePackChildBuffs(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BuffState packBuff;
			LuaObject.checkType<BuffState>(l, 2, out packBuff);
			battleActor.m_luaExportHelper.RemovePackChildBuffs(packBuff);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int OnBuffHit(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BuffState buffState;
			LuaObject.checkType<BuffState>(l, 2, out buffState);
			int heroHpModify;
			LuaObject.checkType(l, 3, out heroHpModify);
			int soldierHpModify;
			LuaObject.checkType(l, 4, out soldierHpModify);
			DamageNumberType damageNumberType;
			LuaObject.checkEnum<DamageNumberType>(l, 5, out damageNumberType);
			battleActor.m_luaExportHelper.OnBuffHit(buffState, heroHpModify, soldierHpModify, damageNumberType);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int UpdateBuffTypes(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.UpdateBuffTypes();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SetBuffType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BuffType bt;
			LuaObject.checkEnum<BuffType>(l, 2, out bt);
			bool on;
			LuaObject.checkType(l, 3, out on);
			battleActor.m_luaExportHelper.SetBuffType(bt, on);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsBuffHpConditionSatisfied(IntPtr l)
	{
		int result;
		try
		{
			int num = LuaDLL.lua_gettop(l);
			if (num == 2)
			{
				BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
				ConfigDataBuffInfo buffInfo;
				LuaObject.checkType<ConfigDataBuffInfo>(l, 2, out buffInfo);
				bool b = battleActor.m_luaExportHelper.IsBuffHpConditionSatisfied(buffInfo);
				LuaObject.pushValue(l, true);
				LuaObject.pushValue(l, b);
				result = 2;
			}
			else if (num == 4)
			{
				BattleActor battleActor2 = (BattleActor)LuaObject.checkSelf(l);
				int operatorType;
				LuaObject.checkType(l, 2, out operatorType);
				int value;
				LuaObject.checkType(l, 3, out value);
				int targetType;
				LuaObject.checkType(l, 4, out targetType);
				bool b2 = battleActor2.m_luaExportHelper.IsBuffHpConditionSatisfied(operatorType, value, targetType);
				LuaObject.pushValue(l, true);
				LuaObject.pushValue(l, b2);
				result = 2;
			}
			else
			{
				LuaObject.pushValue(l, false);
				LuaDLL.lua_pushstring(l, "No matched override function IsBuffHpConditionSatisfied to call");
				result = 2;
			}
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsBuffEffectiveConditionSatisfied(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataBuffInfo buffInfo;
			LuaObject.checkType<ConfigDataBuffInfo>(l, 2, out buffInfo);
			bool b = battleActor.m_luaExportHelper.IsBuffEffectiveConditionSatisfied(buffInfo);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsBuffCombatConditionSatisfied(IntPtr l)
	{
		int result;
		try
		{
			int total = LuaDLL.lua_gettop(l);
			if (LuaObject.matchType(l, total, 2, typeof(BattleActor), typeof(bool), typeof(bool), typeof(int), typeof(bool), typeof(List<int>)))
			{
				BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
				BattleActor target;
				LuaObject.checkType<BattleActor>(l, 2, out target);
				bool beforeCombat;
				LuaObject.checkType(l, 3, out beforeCombat);
				bool isAttacker;
				LuaObject.checkType(l, 4, out isAttacker);
				int distance;
				LuaObject.checkType(l, 5, out distance);
				bool isCritical;
				LuaObject.checkType(l, 6, out isCritical);
				List<int> paramList;
				LuaObject.checkType<List<int>>(l, 7, out paramList);
				bool b = battleActor.m_luaExportHelper.IsBuffCombatConditionSatisfied(target, beforeCombat, isAttacker, distance, isCritical, paramList);
				LuaObject.pushValue(l, true);
				LuaObject.pushValue(l, b);
				result = 2;
			}
			else if (LuaObject.matchType(l, total, 2, typeof(BattleActor), typeof(bool), typeof(int), typeof(int), typeof(int), typeof(int)))
			{
				BattleActor battleActor2 = (BattleActor)LuaObject.checkSelf(l);
				BattleActor target2;
				LuaObject.checkType<BattleActor>(l, 2, out target2);
				bool isAttacker2;
				LuaObject.checkType(l, 3, out isAttacker2);
				int distance2;
				LuaObject.checkType(l, 4, out distance2);
				int param;
				LuaObject.checkType(l, 5, out param);
				int param2;
				LuaObject.checkType(l, 6, out param2);
				int param3;
				LuaObject.checkType(l, 7, out param3);
				bool b2 = battleActor2.m_luaExportHelper.IsBuffCombatConditionSatisfied(target2, isAttacker2, distance2, param, param2, param3);
				LuaObject.pushValue(l, true);
				LuaObject.pushValue(l, b2);
				result = 2;
			}
			else
			{
				LuaObject.pushValue(l, false);
				LuaDLL.lua_pushstring(l, "No matched override function IsBuffCombatConditionSatisfied to call");
				result = 2;
			}
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ComputeNeighborAliveActorCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int teamType;
			LuaObject.checkType(l, 2, out teamType);
			int distance;
			LuaObject.checkType(l, 3, out distance);
			int i = battleActor.m_luaExportHelper.ComputeNeighborAliveActorCount(teamType, distance);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, i);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsBuffCooldown(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BuffState bs;
			LuaObject.checkType<BuffState>(l, 2, out bs);
			bool b = battleActor.m_luaExportHelper.IsBuffCooldown(bs);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int StartBuffCooldown(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BuffState bs;
			LuaObject.checkType<BuffState>(l, 2, out bs);
			battleActor.m_luaExportHelper.StartBuffCooldown(bs);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ComputeBuffCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int buffId;
			LuaObject.checkType(l, 2, out buffId);
			int i = battleActor.m_luaExportHelper.ComputeBuffCount(buffId);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, i);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ComputeEnhanceOrDebuffCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int enhanceDebuffType;
			LuaObject.checkType(l, 2, out enhanceDebuffType);
			int i = battleActor.m_luaExportHelper.ComputeEnhanceOrDebuffCount(enhanceDebuffType);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, i);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsImmuneBuffSubType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int subType;
			LuaObject.checkType(l, 2, out subType);
			bool b = battleActor.m_luaExportHelper.IsImmuneBuffSubType(subType);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ActionEndBuffEffect(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.ActionEndBuffEffect();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int UpdateBuffTime(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.UpdateBuffTime();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ActionEndBuffHealOverTime(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isDamage;
			LuaObject.checkType(l, 2, out isDamage);
			battleActor.m_luaExportHelper.ActionEndBuffHealOverTime(isDamage);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ActionEndBuffDamageOverTime(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.ActionEndBuffDamageOverTime();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ActionEndAddBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.ActionEndAddBuff();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ActionEndAddBuffSuper(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isKill;
			LuaObject.checkType(l, 2, out isKill);
			bool isCritical;
			LuaObject.checkType(l, 3, out isCritical);
			battleActor.m_luaExportHelper.ActionEndAddBuffSuper(isKill, isCritical);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ActionEndRemoveDebuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.ActionEndRemoveDebuff();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ActionEndRemoveEnhanceBuff(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isKill;
			LuaObject.checkType(l, 2, out isKill);
			bool isCritical;
			LuaObject.checkType(l, 3, out isCritical);
			bool isDamage;
			LuaObject.checkType(l, 4, out isDamage);
			battleActor.m_luaExportHelper.ActionEndRemoveEnhanceBuff(isKill, isCritical, isDamage);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ActionEndBuffBattlefieldSkill(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isKill;
			LuaObject.checkType(l, 2, out isKill);
			bool isCritical;
			LuaObject.checkType(l, 3, out isCritical);
			bool isDamage;
			LuaObject.checkType(l, 4, out isDamage);
			battleActor.m_luaExportHelper.ActionEndBuffBattlefieldSkill(isKill, isCritical, isDamage);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ActionEndBuffNewTurn(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isKill;
			LuaObject.checkType(l, 2, out isKill);
			bool isCritical;
			LuaObject.checkType(l, 3, out isCritical);
			battleActor.m_luaExportHelper.ActionEndBuffNewTurn(isKill, isCritical);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CanBuffGuard(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 2, out target);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 3, out skillInfo);
			bool b = battleActor.m_luaExportHelper.CanBuffGuard(target, skillInfo);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int IsSkillUseable(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int skillIndex;
			LuaObject.checkType(l, 2, out skillIndex);
			bool b = battleActor.m_luaExportHelper.IsSkillUseable(skillIndex);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int FindBattlefieldSkillApplyTargets(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			GridPosition targetPos;
			LuaObject.checkValueType<GridPosition>(l, 3, out targetPos);
			List<BattleActor> targetActors;
			LuaObject.checkType<List<BattleActor>>(l, 4, out targetActors);
			battleActor.m_luaExportHelper.FindBattlefieldSkillApplyTargets(skillInfo, targetPos, targetActors);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ExecuteBattlefieldSkill(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			GridPosition p;
			LuaObject.checkValueType<GridPosition>(l, 3, out p);
			GridPosition p2;
			LuaObject.checkValueType<GridPosition>(l, 4, out p2);
			bool b = battleActor.m_luaExportHelper.ExecuteBattlefieldSkill(skillInfo, p, p2);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SkillAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 3, out target);
			bool b = battleActor.m_luaExportHelper.SkillAttack(skillInfo, target);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SkillSummon(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			GridPosition p;
			LuaObject.checkValueType<GridPosition>(l, 3, out p);
			bool b = battleActor.m_luaExportHelper.SkillSummon(skillInfo, p);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SkillTeleport(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			BattleActor target;
			LuaObject.checkType<BattleActor>(l, 3, out target);
			GridPosition teleportPos;
			LuaObject.checkValueType<GridPosition>(l, 4, out teleportPos);
			bool b = battleActor.m_luaExportHelper.SkillTeleport(skillInfo, target, teleportPos);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int AttackBy(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor attacker;
			LuaObject.checkType<BattleActor>(l, 2, out attacker);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 3, out skillInfo);
			battleActor.m_luaExportHelper.AttackBy(attacker, skillInfo);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SkillAttackEnd(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out skillInfo);
			List<BattleActor> targets;
			LuaObject.checkType<List<BattleActor>>(l, 3, out targets);
			battleActor.m_luaExportHelper.SkillAttackEnd(skillInfo, targets);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int TeleportBy(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor attacker;
			LuaObject.checkType<BattleActor>(l, 2, out attacker);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 3, out skillInfo);
			GridPosition p;
			LuaObject.checkValueType<GridPosition>(l, 4, out p);
			battleActor.m_luaExportHelper.TeleportBy(attacker, skillInfo, p);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int SummonBy(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor attacker;
			LuaObject.checkType<BattleActor>(l, 2, out attacker);
			ConfigDataSkillInfo skillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 3, out skillInfo);
			GridPosition p;
			LuaObject.checkValueType<GridPosition>(l, 4, out p);
			battleActor.m_luaExportHelper.SummonBy(attacker, skillInfo, p);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int __callBase_Dispose(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.__callBase_Dispose();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int __callBase_Tick(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.__callBase_Tick();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int __callBase_TickGraphic(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			float dt;
			LuaObject.checkType(l, 2, out dt);
			battleActor.m_luaExportHelper.__callBase_TickGraphic(dt);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int __callBase_Draw(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.__callBase_Draw();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int __callBase_Pause(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool pause;
			LuaObject.checkType(l, 2, out pause);
			battleActor.m_luaExportHelper.__callBase_Pause(pause);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int __callBase_DoPause(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool pause;
			LuaObject.checkType(l, 2, out pause);
			battleActor.m_luaExportHelper.__callBase_DoPause(pause);
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int __callBase_DeleteMe(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			battleActor.m_luaExportHelper.__callBase_DeleteMe();
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ComputeActorScoreDamage_s(IntPtr l)
	{
		int result;
		try
		{
			BattleActor a;
			LuaObject.checkType<BattleActor>(l, 1, out a);
			int param;
			LuaObject.checkType(l, 2, out param);
			int i = BattleActor.LuaExportHelper.ComputeActorScoreDamage(a, param);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, i);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int ComputeActorScoreHeal_s(IntPtr l)
	{
		int result;
		try
		{
			BattleActor a;
			LuaObject.checkType<BattleActor>(l, 1, out a);
			int param;
			LuaObject.checkType(l, 2, out param);
			int i = BattleActor.LuaExportHelper.ComputeActorScoreHeal(a, param);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, i);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int Contains_s(IntPtr l)
	{
		int result;
		try
		{
			int[] target;
			LuaObject.checkArray<int>(l, 1, out target);
			int nr;
			LuaObject.checkType(l, 2, out nr);
			bool b = BattleActor.LuaExportHelper.Contains(target, nr);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int HasInt_s(IntPtr l)
	{
		int result;
		try
		{
			int value;
			LuaObject.checkType(l, 1, out value);
			int[] arr;
			LuaObject.checkArray<int>(l, 2, out arr);
			bool b = BattleActor.LuaExportHelper.HasInt(value, arr);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, b);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CompareBuffOrder_s(IntPtr l)
	{
		int result;
		try
		{
			BuffState b;
			LuaObject.checkType<BuffState>(l, 1, out b);
			BuffState b2;
			LuaObject.checkType<BuffState>(l, 2, out b2);
			int i = BattleActor.LuaExportHelper.CompareBuffOrder(b, b2);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, i);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int CompareHealActor_s(IntPtr l)
	{
		int result;
		try
		{
			BattleActor a;
			LuaObject.checkType<BattleActor>(l, 1, out a);
			BattleActor a2;
			LuaObject.checkType<BattleActor>(l, 2, out a2);
			int i = BattleActor.LuaExportHelper.CompareHealActor(a, a2);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, i);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_team(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_team);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_team(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleTeam team;
			LuaObject.checkType<BattleTeam>(l, 2, out team);
			battleActor.m_luaExportHelper.m_team = team;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_position(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_position);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_position(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition position;
			LuaObject.checkValueType<GridPosition>(l, 2, out position);
			battleActor.m_luaExportHelper.m_position = position;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_direction(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_direction);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_direction(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int direction;
			LuaObject.checkType(l, 2, out direction);
			battleActor.m_luaExportHelper.m_direction = direction;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_initPosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_initPosition);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_initPosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition initPosition;
			LuaObject.checkValueType<GridPosition>(l, 2, out initPosition);
			battleActor.m_luaExportHelper.m_initPosition = initPosition;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_beforeGuardPosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_beforeGuardPosition);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_beforeGuardPosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			GridPosition beforeGuardPosition;
			LuaObject.checkValueType<GridPosition>(l, 2, out beforeGuardPosition);
			battleActor.m_luaExportHelper.m_beforeGuardPosition = beforeGuardPosition;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_heroBattleProperty(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_heroBattleProperty);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_heroBattleProperty(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleProperty heroBattleProperty;
			LuaObject.checkType<BattleProperty>(l, 2, out heroBattleProperty);
			battleActor.m_luaExportHelper.m_heroBattleProperty = heroBattleProperty;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_soldierBattleProperty(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_soldierBattleProperty);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_soldierBattleProperty(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleProperty soldierBattleProperty;
			LuaObject.checkType<BattleProperty>(l, 2, out soldierBattleProperty);
			battleActor.m_luaExportHelper.m_soldierBattleProperty = soldierBattleProperty;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_heroHealthPoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_heroHealthPoint);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_heroHealthPoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int heroHealthPoint;
			LuaObject.checkType(l, 2, out heroHealthPoint);
			battleActor.m_luaExportHelper.m_heroHealthPoint = heroHealthPoint;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_soldierTotalHealthPoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_soldierTotalHealthPoint);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_soldierTotalHealthPoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int soldierTotalHealthPoint;
			LuaObject.checkType(l, 2, out soldierTotalHealthPoint);
			battleActor.m_luaExportHelper.m_soldierTotalHealthPoint = soldierTotalHealthPoint;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_soldierSingleHealthPointMax(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_soldierSingleHealthPointMax);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_soldierSingleHealthPointMax(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int soldierSingleHealthPointMax;
			LuaObject.checkType(l, 2, out soldierSingleHealthPointMax);
			battleActor.m_luaExportHelper.m_soldierSingleHealthPointMax = soldierSingleHealthPointMax;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_initSoldierCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_initSoldierCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_initSoldierCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int initSoldierCount;
			LuaObject.checkType(l, 2, out initSoldierCount);
			battleActor.m_luaExportHelper.m_initSoldierCount = initSoldierCount;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_heroLevel(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_heroLevel);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_heroLevel(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int heroLevel;
			LuaObject.checkType(l, 2, out heroLevel);
			battleActor.m_luaExportHelper.m_heroLevel = heroLevel;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_heroStar(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_heroStar);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_heroStar(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int heroStar;
			LuaObject.checkType(l, 2, out heroStar);
			battleActor.m_luaExportHelper.m_heroStar = heroStar;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_jobLevel(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_jobLevel);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_jobLevel(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int jobLevel;
			LuaObject.checkType(l, 2, out jobLevel);
			battleActor.m_luaExportHelper.m_jobLevel = jobLevel;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_masterJobInfos(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_masterJobInfos);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_masterJobInfos(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataJobInfo[] masterJobInfos;
			LuaObject.checkArray<ConfigDataJobInfo>(l, 2, out masterJobInfos);
			battleActor.m_luaExportHelper.m_masterJobInfos = masterJobInfos;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_equipments(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_equipments);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_equipments(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActorEquipment[] equipments;
			LuaObject.checkArray<BattleActorEquipment>(l, 2, out equipments);
			battleActor.m_luaExportHelper.m_equipments = equipments;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_resonanceSkillInfos(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_resonanceSkillInfos);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_resonanceSkillInfos(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo[] resonanceSkillInfos;
			LuaObject.checkArray<ConfigDataSkillInfo>(l, 2, out resonanceSkillInfos);
			battleActor.m_luaExportHelper.m_resonanceSkillInfos = resonanceSkillInfos;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_fetterSkillInfos(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_fetterSkillInfos);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_fetterSkillInfos(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo[] fetterSkillInfos;
			LuaObject.checkArray<ConfigDataSkillInfo>(l, 2, out fetterSkillInfos);
			battleActor.m_luaExportHelper.m_fetterSkillInfos = fetterSkillInfos;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_actionValue(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_actionValue);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_actionValue(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int actionValue;
			LuaObject.checkType(l, 2, out actionValue);
			battleActor.m_luaExportHelper.m_actionValue = actionValue;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_moveType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushEnum(l, (int)battleActor.m_luaExportHelper.m_moveType);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_moveType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			MoveType moveType;
			LuaObject.checkEnum<MoveType>(l, 2, out moveType);
			battleActor.m_luaExportHelper.m_moveType = moveType;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_isActionFinished(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_isActionFinished);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_isActionFinished(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isActionFinished;
			LuaObject.checkType(l, 2, out isActionFinished);
			battleActor.m_luaExportHelper.m_isActionFinished = isActionFinished;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_hasExtraAction(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_hasExtraAction);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_hasExtraAction(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool hasExtraAction;
			LuaObject.checkType(l, 2, out hasExtraAction);
			battleActor.m_luaExportHelper.m_hasExtraAction = hasExtraAction;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_hasExtraActionCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_hasExtraActionCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_hasExtraActionCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int hasExtraActionCount;
			LuaObject.checkType(l, 2, out hasExtraActionCount);
			battleActor.m_luaExportHelper.m_hasExtraActionCount = hasExtraActionCount;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_hasExtraMovePoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_hasExtraMovePoint);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_hasExtraMovePoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int hasExtraMovePoint;
			LuaObject.checkType(l, 2, out hasExtraMovePoint);
			battleActor.m_luaExportHelper.m_hasExtraMovePoint = hasExtraMovePoint;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_isExtraAction(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_isExtraAction);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_isExtraAction(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isExtraAction;
			LuaObject.checkType(l, 2, out isExtraAction);
			battleActor.m_luaExportHelper.m_isExtraAction = isExtraAction;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_extraActionMovePoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_extraActionMovePoint);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_extraActionMovePoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int extraActionMovePoint;
			LuaObject.checkType(l, 2, out extraActionMovePoint);
			battleActor.m_luaExportHelper.m_extraActionMovePoint = extraActionMovePoint;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_isVisible(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_isVisible);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_isVisible(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isVisible;
			LuaObject.checkType(l, 2, out isVisible);
			battleActor.m_luaExportHelper.m_isVisible = isVisible;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_isDie(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_isDie);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_isDie(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isDie;
			LuaObject.checkType(l, 2, out isDie);
			battleActor.m_luaExportHelper.m_isDie = isDie;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_isRetreat(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_isRetreat);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_isRetreat(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isRetreat;
			LuaObject.checkType(l, 2, out isRetreat);
			battleActor.m_luaExportHelper.m_isRetreat = isRetreat;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_skillStates(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_skillStates);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_skillStates(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BattleSkillState> skillStates;
			LuaObject.checkType<List<BattleSkillState>>(l, 2, out skillStates);
			battleActor.m_luaExportHelper.m_skillStates = skillStates;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_buffStates(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_buffStates);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_buffStates(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			List<BuffState> buffStates;
			LuaObject.checkType<List<BuffState>>(l, 2, out buffStates);
			battleActor.m_luaExportHelper.m_buffStates = buffStates;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_buffIdCounter(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_buffIdCounter);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_buffIdCounter(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int buffIdCounter;
			LuaObject.checkType(l, 2, out buffIdCounter);
			battleActor.m_luaExportHelper.m_buffIdCounter = buffIdCounter;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_buffTypes(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_buffTypes);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_buffTypes(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ulong buffTypes;
			LuaObject.checkType(l, 2, out buffTypes);
			battleActor.m_luaExportHelper.m_buffTypes = buffTypes;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_fightTags(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_fightTags);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_fightTags(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			uint fightTags;
			LuaObject.checkType(l, 2, out fightTags);
			battleActor.m_luaExportHelper.m_fightTags = fightTags;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_summoner(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_summoner);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_summoner(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor summoner;
			LuaObject.checkType<BattleActor>(l, 2, out summoner);
			battleActor.m_luaExportHelper.m_summoner = summoner;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_killerActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_killerActor);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_killerActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor killerActor;
			LuaObject.checkType<BattleActor>(l, 2, out killerActor);
			battleActor.m_luaExportHelper.m_killerActor = killerActor;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_isNpc(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_isNpc);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_isNpc(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isNpc;
			LuaObject.checkType(l, 2, out isNpc);
			battleActor.m_luaExportHelper.m_isNpc = isNpc;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_isPlayerNpc(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_isPlayerNpc);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_isPlayerNpc(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isPlayerNpc;
			LuaObject.checkType(l, 2, out isPlayerNpc);
			battleActor.m_luaExportHelper.m_isPlayerNpc = isPlayerNpc;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_sourceType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushEnum(l, (int)battleActor.m_luaExportHelper.m_sourceType);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_sourceType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActorSourceType sourceType;
			LuaObject.checkEnum<BattleActorSourceType>(l, 2, out sourceType);
			battleActor.m_luaExportHelper.m_sourceType = sourceType;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_heroInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_heroInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_heroInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataHeroInfo heroInfo;
			LuaObject.checkType<ConfigDataHeroInfo>(l, 2, out heroInfo);
			battleActor.m_luaExportHelper.m_heroInfo = heroInfo;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_jobConnectionInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_jobConnectionInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_jobConnectionInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataJobConnectionInfo jobConnectionInfo;
			LuaObject.checkType<ConfigDataJobConnectionInfo>(l, 2, out jobConnectionInfo);
			battleActor.m_luaExportHelper.m_jobConnectionInfo = jobConnectionInfo;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_jobInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_jobInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_jobInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataJobInfo jobInfo;
			LuaObject.checkType<ConfigDataJobInfo>(l, 2, out jobInfo);
			battleActor.m_luaExportHelper.m_jobInfo = jobInfo;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_heroArmyInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_heroArmyInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_heroArmyInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataArmyInfo heroArmyInfo;
			LuaObject.checkType<ConfigDataArmyInfo>(l, 2, out heroArmyInfo);
			battleActor.m_luaExportHelper.m_heroArmyInfo = heroArmyInfo;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_soldierInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_soldierInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_soldierInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSoldierInfo soldierInfo;
			LuaObject.checkType<ConfigDataSoldierInfo>(l, 2, out soldierInfo);
			battleActor.m_luaExportHelper.m_soldierInfo = soldierInfo;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_soldierArmyInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_soldierArmyInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_soldierArmyInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataArmyInfo soldierArmyInfo;
			LuaObject.checkType<ConfigDataArmyInfo>(l, 2, out soldierArmyInfo);
			battleActor.m_luaExportHelper.m_soldierArmyInfo = soldierArmyInfo;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_heroCharImageSkinResourceInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_heroCharImageSkinResourceInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_heroCharImageSkinResourceInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataCharImageSkinResourceInfo heroCharImageSkinResourceInfo;
			LuaObject.checkType<ConfigDataCharImageSkinResourceInfo>(l, 2, out heroCharImageSkinResourceInfo);
			battleActor.m_luaExportHelper.m_heroCharImageSkinResourceInfo = heroCharImageSkinResourceInfo;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_heroModelSkinResourceInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_heroModelSkinResourceInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_heroModelSkinResourceInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataModelSkinResourceInfo heroModelSkinResourceInfo;
			LuaObject.checkType<ConfigDataModelSkinResourceInfo>(l, 2, out heroModelSkinResourceInfo);
			battleActor.m_luaExportHelper.m_heroModelSkinResourceInfo = heroModelSkinResourceInfo;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_soldierModelSkinResourceInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_soldierModelSkinResourceInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_soldierModelSkinResourceInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataModelSkinResourceInfo soldierModelSkinResourceInfo;
			LuaObject.checkType<ConfigDataModelSkinResourceInfo>(l, 2, out soldierModelSkinResourceInfo);
			battleActor.m_luaExportHelper.m_soldierModelSkinResourceInfo = soldierModelSkinResourceInfo;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_extraPassiveSkillInfos(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_extraPassiveSkillInfos);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_extraPassiveSkillInfos(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo[] extraPassiveSkillInfos;
			LuaObject.checkArray<ConfigDataSkillInfo>(l, 2, out extraPassiveSkillInfos);
			battleActor.m_luaExportHelper.m_extraPassiveSkillInfos = extraPassiveSkillInfos;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_extraTalentSkillInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_extraTalentSkillInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_extraTalentSkillInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo extraTalentSkillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out extraTalentSkillInfo);
			battleActor.m_luaExportHelper.m_extraTalentSkillInfo = extraTalentSkillInfo;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_isActionCriticalAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_isActionCriticalAttack);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_isActionCriticalAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isActionCriticalAttack;
			LuaObject.checkType(l, 2, out isActionCriticalAttack);
			battleActor.m_luaExportHelper.m_isActionCriticalAttack = isActionCriticalAttack;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_isActionKillActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_isActionKillActor);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_isActionKillActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isActionKillActor;
			LuaObject.checkType(l, 2, out isActionKillActor);
			battleActor.m_luaExportHelper.m_isActionKillActor = isActionKillActor;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_isActionDamageActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_isActionDamageActor);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_isActionDamageActor(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isActionDamageActor;
			LuaObject.checkType(l, 2, out isActionDamageActor);
			battleActor.m_luaExportHelper.m_isActionDamageActor = isActionDamageActor;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_isBeCriticalAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_isBeCriticalAttack);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_isBeCriticalAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isBeCriticalAttack;
			LuaObject.checkType(l, 2, out isBeCriticalAttack);
			battleActor.m_luaExportHelper.m_isBeCriticalAttack = isBeCriticalAttack;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_isBeKillAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_isBeKillAttack);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_isBeKillAttack(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			bool isBeKillAttack;
			LuaObject.checkType(l, 2, out isBeKillAttack);
			battleActor.m_luaExportHelper.m_isBeKillAttack = isBeKillAttack;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_actionMoveGrids(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_actionMoveGrids);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_actionMoveGrids(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int actionMoveGrids;
			LuaObject.checkType(l, 2, out actionMoveGrids);
			battleActor.m_luaExportHelper.m_actionMoveGrids = actionMoveGrids;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_actionRemainMovePoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_actionRemainMovePoint);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_actionRemainMovePoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int actionRemainMovePoint;
			LuaObject.checkType(l, 2, out actionRemainMovePoint);
			battleActor.m_luaExportHelper.m_actionRemainMovePoint = actionRemainMovePoint;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_buffReplaceMovePoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_buffReplaceMovePoint);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_buffReplaceMovePoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int buffReplaceMovePoint;
			LuaObject.checkType(l, 2, out buffReplaceMovePoint);
			battleActor.m_luaExportHelper.m_buffReplaceMovePoint = buffReplaceMovePoint;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_lastDamageBySkillInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_lastDamageBySkillInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_lastDamageBySkillInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo lastDamageBySkillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out lastDamageBySkillInfo);
			battleActor.m_luaExportHelper.m_lastDamageBySkillInfo = lastDamageBySkillInfo;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_firstDamageTurn(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_firstDamageTurn);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_firstDamageTurn(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int firstDamageTurn;
			LuaObject.checkType(l, 2, out firstDamageTurn);
			battleActor.m_luaExportHelper.m_firstDamageTurn = firstDamageTurn;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_satisfyConditions(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_satisfyConditions);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_satisfyConditions(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ulong satisfyConditions;
			LuaObject.checkType(l, 2, out satisfyConditions);
			battleActor.m_luaExportHelper.m_satisfyConditions = satisfyConditions;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_actionCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_actionCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_actionCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			short actionCount;
			LuaObject.checkType(l, 2, out actionCount);
			battleActor.m_luaExportHelper.m_actionCount = actionCount;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_combatAttackCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_combatAttackCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_combatAttackCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			short combatAttackCount;
			LuaObject.checkType(l, 2, out combatAttackCount);
			battleActor.m_luaExportHelper.m_combatAttackCount = combatAttackCount;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_beCombatAttackCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_beCombatAttackCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_beCombatAttackCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			short beCombatAttackCount;
			LuaObject.checkType(l, 2, out beCombatAttackCount);
			battleActor.m_luaExportHelper.m_beCombatAttackCount = beCombatAttackCount;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_useSkillCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_useSkillCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_useSkillCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			short useSkillCount;
			LuaObject.checkType(l, 2, out useSkillCount);
			battleActor.m_luaExportHelper.m_useSkillCount = useSkillCount;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_killActorCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_killActorCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_killActorCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			short killActorCount;
			LuaObject.checkType(l, 2, out killActorCount);
			battleActor.m_luaExportHelper.m_killActorCount = killActorCount;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_dieTurn(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_dieTurn);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_dieTurn(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int dieTurn;
			LuaObject.checkType(l, 2, out dieTurn);
			battleActor.m_luaExportHelper.m_dieTurn = dieTurn;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_deathAnimType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_deathAnimType);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_deathAnimType(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int deathAnimType;
			LuaObject.checkType(l, 2, out deathAnimType);
			battleActor.m_luaExportHelper.m_deathAnimType = deathAnimType;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_executedCommandTypes(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_executedCommandTypes);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_executedCommandTypes(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			uint executedCommandTypes;
			LuaObject.checkType(l, 2, out executedCommandTypes);
			battleActor.m_luaExportHelper.m_executedCommandTypes = executedCommandTypes;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_executedSkillInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_executedSkillInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_executedSkillInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataSkillInfo executedSkillInfo;
			LuaObject.checkType<ConfigDataSkillInfo>(l, 2, out executedSkillInfo);
			battleActor.m_luaExportHelper.m_executedSkillInfo = executedSkillInfo;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_playerIndex(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_playerIndex);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_playerIndex(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int playerIndex;
			LuaObject.checkType(l, 2, out playerIndex);
			battleActor.m_luaExportHelper.m_playerIndex = playerIndex;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_curBehaviorCfg(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_curBehaviorCfg);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_curBehaviorCfg(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			ConfigDataBehavior curBehaviorCfg;
			LuaObject.checkType<ConfigDataBehavior>(l, 2, out curBehaviorCfg);
			battleActor.m_luaExportHelper.m_curBehaviorCfg = curBehaviorCfg;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_curBehaviorState(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushEnum(l, (int)battleActor.m_luaExportHelper.m_curBehaviorState);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_curBehaviorState(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BattleActor.BehaviorState curBehaviorState;
			LuaObject.checkEnum<BattleActor.BehaviorState>(l, 2, out curBehaviorState);
			battleActor.m_luaExportHelper.m_curBehaviorState = curBehaviorState;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_moveTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_moveTarget);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_moveTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BehaviorTarget moveTarget;
			LuaObject.checkType<BehaviorTarget>(l, 2, out moveTarget);
			battleActor.m_luaExportHelper.m_moveTarget = moveTarget;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_attackTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_attackTarget);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_attackTarget(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BehaviorTarget attackTarget;
			LuaObject.checkType<BehaviorTarget>(l, 2, out attackTarget);
			battleActor.m_luaExportHelper.m_attackTarget = attackTarget;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_attackSkillIndex(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_attackSkillIndex);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_attackSkillIndex(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int attackSkillIndex;
			LuaObject.checkType(l, 2, out attackSkillIndex);
			battleActor.m_luaExportHelper.m_attackSkillIndex = attackSkillIndex;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_beAttackedCountOfTurns(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_beAttackedCountOfTurns);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_beAttackedCountOfTurns(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int[] beAttackedCountOfTurns;
			LuaObject.checkArray<int>(l, 2, out beAttackedCountOfTurns);
			battleActor.m_luaExportHelper.m_beAttackedCountOfTurns = beAttackedCountOfTurns;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_groupId(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_groupId);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_groupId(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int groupId;
			LuaObject.checkType(l, 2, out groupId);
			battleActor.m_luaExportHelper.m_groupId = groupId;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_aiCreateBattleCommandCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.m_aiCreateBattleCommandCount);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_m_aiCreateBattleCommandCount(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int aiCreateBattleCommandCount;
			LuaObject.checkType(l, 2, out aiCreateBattleCommandCount);
			battleActor.m_luaExportHelper.m_aiCreateBattleCommandCount = aiCreateBattleCommandCount;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_m_healSkillTargetHPThresh(IntPtr l)
	{
		int result;
		try
		{
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, BattleActor.LuaExportHelper.m_healSkillTargetHPThresh);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_Position(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.Position);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_InitPosition(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.InitPosition);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_Direction(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.Direction);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_Battle(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.Battle);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_Team(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.Team);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_TeamNumber(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.TeamNumber);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_HeroBattleProperty(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.HeroBattleProperty);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_SoldierBattleProperty(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.SoldierBattleProperty);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_HeroHealthPoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.HeroHealthPoint);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_SoldierTotalHealthPoint(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.SoldierTotalHealthPoint);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_SoldierSingleHealthPointMax(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.SoldierSingleHealthPointMax);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_FightTags(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.FightTags);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_HeroLevel(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.HeroLevel);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_HeroStar(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.HeroStar);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_JobLevel(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.JobLevel);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_ActionValue(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.ActionValue);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_ActionValue(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			int actionValue;
			LuaObject.checkType(l, 2, out actionValue);
			battleActor.ActionValue = actionValue;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_HeroInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.HeroInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_HeroId(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.HeroId);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_HeroArmyInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.HeroArmyInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_HeroArmyId(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.HeroArmyId);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_JobConnectionInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.JobConnectionInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_JobInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.JobInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_SoldierInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.SoldierInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_SoldierArmyInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.SoldierArmyInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_HeroCharImageSkinResourceInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.HeroCharImageSkinResourceInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_HeroModelSkinResourceInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.HeroModelSkinResourceInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_SoldierModelSkinResourceInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.SoldierModelSkinResourceInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_PlayerIndex(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.PlayerIndex);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_Group(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.Group);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int set_Group(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			BehaviorGroup group;
			LuaObject.checkType<BehaviorGroup>(l, 2, out group);
			battleActor.Group = group;
			LuaObject.pushValue(l, true);
			result = 1;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_GroupId(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.GroupId);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_IsAttackedByEnemy(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.IsAttackedByEnemy);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_IsAttackedByEnemyInLastTrun(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.IsAttackedByEnemyInLastTrun);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_InstanceID(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.InstanceID);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_NextBehaviorByChangeRules(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.NextBehaviorByChangeRules);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_TotalHPPercent(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.TotalHPPercent);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}

	public static int get_BehaviorSelectSkillInfo(IntPtr l)
	{
		int result;
		try
		{
			BattleActor battleActor = (BattleActor)LuaObject.checkSelf(l);
			LuaObject.pushValue(l, true);
			LuaObject.pushValue(l, battleActor.m_luaExportHelper.BehaviorSelectSkillInfo);
			result = 2;
		}
		catch (Exception e)
		{
			result = LuaObject.error(l, e);
		}
		return result;
	}
}